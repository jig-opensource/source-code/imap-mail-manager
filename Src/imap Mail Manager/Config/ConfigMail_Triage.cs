using System.Collections.Generic;


namespace ImapMailManager.Config;

/// <summary>
/// Konfiguration für die Triage der E-Mails
/// </summary>
public class ConfigMail_Triage {

   #region Triage HashTags

   /// <summary>
   /// Struktur der Text Mail Body Triage HashTags:
   /// 
   ///   Triage-HashTags:
   ///   #Seelsorge
   ///   #Glaubensleben 
   /// 
   /// </summary>
   readonly static public string Triage_HashTag_TextHeader = "\n\nTriage-HashTags:";

   // <summary>
   // Die E-Mail Triage fehlt, ist unbekannt oder wurde nicht gemacht
   // ‼ Wir speichern den Zustand "nicht Triagiert" nicht
   // Dieser state ergibt sich aus der Tatsache, dass die E-Mail keine HashTags hat 
   // </summary>
   // readonly static public string Triage_HashTag_NichtKlassifiziert = "#TriageFehlt";

   #endregion Triage HashTags

   #region Mailbox Triage: Ordner-Klassifikation

   /// <summary>
   /// Name des Hauptordners im Archiv, in dem die triagierten E-Mails abgelegt werden
   /// Aus diesem Namen wird dieses HashTag generiert:: #Triagiert 
   /// </summary>
   public static readonly string TriageArchivOrdnerName = "Triagiert";

   /// <summary>
   /// Ordner mit offenen Fragem, die nicht exakt triagiert sind
   /// </summary>
   public static readonly List<string> Kat_Triage_TriageUnbekannt_Rgx
      = new List<string>(new[] { @"\d+\s+Redaktion", "Parkierte Nachrichten", "INBOX" });

   /// <summary>
   /// Der Hauptordner, in dem die E-Mails triagiert werden
   /// und der deshalb nicht Teil der eigentlichen Inhalts-Klassifizierung ist 
   /// </summary>
   public static readonly List<string> Kat_Triage_Hauptordner_Rgx
      = new List<string>(new[] { "Kontaktformular.*" });

   #endregion Mailbox Triage: Ordner-Klassifikation

}
