using System.Collections.Generic;
using PCRE;


namespace ImapMailManager.Config;

/// <summary>
/// Grundkonfigurationen der App
/// für alles rund um die E-Mails
/// </summary>
public abstract class ConfigMail_Basics {

   /// <summary>
   /// Der Betreff von Doc E-Mails 
   /// </summary>
   public static readonly string AppSpecMail_Doc_SubjectStart = "✅ Dok:";

   public static readonly List<string> Kat_FromAddr_Fragen =
      new List<string>
         (new[] {
            "KontaktFormular@rogerliebi.ch", "freundesbrief@rogerliebi.ch"
         });

   #region Bekannte E-Mail Adressen
   
   #region Regex Personliche Team Antwort-Adresse
   
   /// <summary>
   /// Die persönliche Team Mitglied Antwort Adresse
   /// </summary>
   public static readonly string sRgx_EMailAdress_PersönlicheTeamAntwortAdresse = @"
                                       (?inxm-s:
                                         (?<EMailAddress>
                                           (?<Prefix>Team)
                                           (?<Sprache>[a-zA-Z]{2})
                                           \.
                                           (?<MAKuerzel>[a-zA-Z]{4,5})
                                           @
                                           (?<Domain>rogerliebi\.ch)
                                         )
                                       )";

   static readonly PcreOptions RgxPcre2Opts_EMailAdress_PersönlicheTeamAntwortAdresse = PcreOptions.Compiled
                                                                                             | PcreOptions.IgnoreCase
                                                                                             | PcreOptions.ExplicitCapture
                                                                                             | PcreOptions.Caseless
                                                                                             | PcreOptions.IgnorePatternWhitespace
                                                                                             | PcreOptions.Unicode;

   public static PcreRegex ORgx_EMailAdress_PersönlicheTeamAntwortAdresse = new PcreRegex(sRgx_EMailAdress_PersönlicheTeamAntwortAdresse, RgxPcre2Opts_EMailAdress_PersönlicheTeamAntwortAdresse);
   
   #endregion Regex Personliche Team Antwort-Adresse

   /// <summary>
   /// Die from: Adresse des Kontaktformulars
   /// </summary>
   public static readonly string EMailAdress_Kontaktformular = "KontaktFormular@rogerliebi.ch";

   /// <summary>
   /// Die To: Adresse des Team-Postfachs
   /// </summary>
   public static readonly string EMailAdress_TeamPostfach = "Team@rogerliebi.ch";

   /// <summary>
   /// Die to: Adresse, an die Daten ans Team@RL.ch geschickt werden können
   /// </summary>
   public static readonly string EMailAdress_ZugeschickteDaten = "Team-Daten@rogerliebi.ch";

   /// <summary>
   /// E-Mail Adressen mit unseren Antworten
   /// </summary>
   public static readonly List<string> Kat_FromAddr_Antworten
      = new List<string>(new[] { "team@rogerliebi.ch", "RogerLiebi@RogerLiebi.ch", "RogerLiebi@swissonline.ch" });

   /// <summary>
   /// E-Mail Domänen mit unseren Antworten
   /// </summary>
   public static readonly List<string> Kat_FromAddr_DomainsAreProbablyAntworten
      = new List<string>(new[] { "RogerLiebi.ch" });

   #endregion Bekannte E-Mail Adressen

   public static readonly List<string> Kat_Folder_MailsSpam
      = new List<string>(new[] { "Junk-E-Mail", "Spam" });

   public static readonly List<string> Kat_Folder_MailsDeleted
      = new List<string>(new[] { "Trash", "Gelöschte" });

   public static readonly List<string> Kat_Folder_MailsInArbeit
      = new List<string>(new[] { "Entwürfe", "Drafts", "10 Prozess" });

   public static readonly List<string> Kat_Folder_GeschickteAntworten
      = new List<string>(new[] { "Gesendet", "Sent" });

   public static readonly List<string> Kat_Folder_ErledigteFragen =
      new List<string>(new[] { "90 Erledigt", "Archive" });

}
