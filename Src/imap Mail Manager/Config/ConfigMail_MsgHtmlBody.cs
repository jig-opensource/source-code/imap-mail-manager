namespace ImapMailManager.Config;


/// <summary>
/// Konfig-Daten für den *ganzen* E-Mail HTML Body,
/// also inkl. HTML Head Elemente
/// </summary>
public class ConfigMail_MsgHtmlBody {

   // Die HTML Element ID, die in den E-Mails genützt wird 
   readonly static public string MailHtmlElementID = "jigData";

   /// <summary>
   /// Definiert das standard Grundgerüst eines HTML Dokuments
   /// </summary>
   readonly static public string Default_HtmlDoc_Structure = @"
         <!doctype html>
         <html>
           <head>
           </head>
           <body>
           </body>
         </html>
         ";
   
   /// <summary>
   /// Definiert die CSS-Klassen, die in den veränderten E-Mails genützt werden
   /// </summary>
   readonly static public string MailHtmlHeaderStyleCss =
      @"<style type=""text/css"">
         /* CSS Klass jigID dient nur als unique ID, */
         /* damit sie per code gefunden werden kann */
   		.jigID {  --jigDummy-style: foo; }

         /* CSS Klassen für die Mail-Formatierung */
         
         /* Obsolete  */
         /* CSS für eine HTML ID */
         #jigData {
	         font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;
	         font-size: 13px;
         }

         /* CSS für eine HTML ID */
         /* ID für den Bereich mit den Daten für den User */
         /* Ist im prod. Betrieb wohl immer sichtbar */
         #jigDataUser {
	         font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;
	         font-size: 13px;
         }

         /* CSS für eine HTML ID */
         /* ID für den Bereich mit den App-Daten */
         /* Ist im prod. Betrieb wohl immer versteckt */
         #jigDataApp {
	         font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;
	         font-size: 13px;
            color: green;
            /* display:none; */
         }

         /* CSS Klassen  */
         .jigTagUser { color: blue; }
         .jigTagApp { color: green; }
         .jigHidden { display:none; }

         #jigDataUser ul,
         #jigDataApp ul {
	         list-style-type: none;
	         margin-left: -23px;
	         padding-right: 8px;
	         content: '-';
         }

          </style>
      ";

}
