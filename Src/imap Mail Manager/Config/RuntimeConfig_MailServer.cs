﻿using System.Linq;
using ImapMailManager.Properties;
using jig.Tools;
using jig.Tools.String;


namespace ImapMailManager.Config;

/// <summary>
/// Stellt die Runtime-Config des aktiven Mail-Servers für die App
/// </summary>
public class RuntimeConfig_MailServer {

   #region Definition des aktiven Mail Servers

   public static Sensitive_AppSettings.CfgImapServer CfgImapServerRl => App.SensitiveAppSettings.ImapSrcCfg!;

   /*
      public static Sensitive_AppSettings.CfgImapServerCred CfgImapServerCredAktiv =>
         App.SensitiveAppSettings.ImapSrcCfg!
            .Creds.FirstOrDefault(x => x.CredKey.ØEqualsIgnoreCase("Prod-Mailbox"))!;

      public static Sensitive_AppSettings.CfgImapServerCred CfgImapServerCredAktiv =>
         App.SensitiveAppSettings.ImapSrcCfg!
            .Creds.FirstOrDefault(x => x.CredKey.ØEqualsIgnoreCase("Prod-Mailbox-Archiv"))!;

      public static Sensitive_AppSettings.CfgImapServerCred CfgImapServerCredAktiv =>
         App.SensitiveAppSettings.ImapSrcCfg!
            .Creds.FirstOrDefault(x => x.CredKey.ØEqualsIgnoreCase("Test-TomSWE"))!;
            
   public static Sensitive_AppSettings.CfgImapServerCred CfgImapServerCredAktiv =>
      App.SensitiveAppSettings.ImapSrcCfg!
         .Creds.FirstOrDefault(x => x.CredKey.ØEqualsIgnoreCase("Prod-Mailbox-Archiv"))!;

   */

   private static Sensitive_AppSettings.CfgImapServerCred? _cfgImapServerCredAktiv = null;
   
   public static Sensitive_AppSettings.CfgImapServerCred CfgImapServerCredAktiv {
      get {
         if (_cfgImapServerCredAktiv != null) {
            return _cfgImapServerCredAktiv;
         }
         return App.SensitiveAppSettings.ImapSrcCfg!
                   .Creds.FirstOrDefault(x => x.CredKey.ØEqualsIgnoreCase("Test-Team"))!;
      }
      set => _cfgImapServerCredAktiv = value;
   }

   #endregion Definition des aktiven Mail Servers

}
