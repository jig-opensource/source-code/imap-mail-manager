using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ImapMailManager.Properties;
using jig.Tools;
using jig.Tools.IComparable;


namespace ImapMailManager.Config;

#region MailKategorieComparer

/// <summary>
/// Vergleicht MailKategorie
/// </summary>
class MailKategorieComparer : IComparer<ConfigMail_MsgHeader.MailKategorie?> {

   public int Compare(ConfigMail_MsgHeader.MailKategorie? x, ConfigMail_MsgHeader.MailKategorie? y) {
      if (x == null && y == null) { return 0; }
      if (x == null) { return 1; }
      if (y == null) { return -1; }

      return x.ToNumber().CompareTo(y.ToNumber());
   }

}

#endregion MailKategorieComparer


/// <summary>
/// Definiert den Umgang mit den Mail-Headern
/// </summary>
public class ConfigMail_MsgHeader {

   //+ Die E-Mail Header, die ich selber setze
   //!KH https://stackoverflow.com/questions/39167153/rfc5322-what-are-the-line-limits

   /// <summary>
   /// Der Comparer für MailKategorie
   /// </summary>

   // public static EnumOrNullComparer<MailKategorie?> MailKategorieComparer = EnumOrNullComparer<MailKategorie?>.Instance;
   // var comparer = EnumComparer❮DayOfWeek❯.Instance;
   public static EnumComparable<MailKategorie> MailKategorieComparable = EnumComparable<MailKategorie>.Instance;


   /// <summary>
   /// Klassifiziert eine E-Mail
   /// </summary>
   [SuppressMessage("ReSharper", "MissingXmlDoc")]
   public enum MailKategorie {

      Unknown,
      /// <summary>
      /// Eine E-Mail im Postfach-Ordner, der etwas dokumentiert 
      /// </summary>
      AppSpezifischeMailDocMail,
      /// <summary>
      /// Die Mail kommt direkt ovm Kontaktformular, sie beinhaltet also die ursprüngliche Frage 
      /// </summary>
      FrageVomKontaktformular,
      /// <summary>
      /// Die Mail wurde direkt ans Team-Postfach geschickt, ohne RefNr
      /// </summary>
      MailDirektAnsTeamPostfach, 
      
      ZugeschickteDatei,
      
      /// <summary>
      /// Wenn AntwortVomTeam,
      /// - Wenn der Autor identifiziert werden konnte,
      ///   dann haben wir diesen Header:
      ///   <see cref="ConfigMail_MsgHeader.AppMailHeaderType.Author_AntwortVomTeam"/>
      /// - Sonst fehlt der Header Author_AntwortVomTeam
      /// </summary>
      AntwortVomTeam,
      
      /// <summary>
      /// Folgefrage: Jemand hat auf eine Antwort vom Team reagiert
      /// </summary>
      ReaktionAufAntwortVomTeam,

      // Das Team hat eine Frage ohne Refnr beantwortet
      // und der Fragesteller hat reagiert
      // FrageOhneRefNr_WahrscheinlichReaktionAufAntwortVomTeam

      // ‼ Fehlt, weil knifflig zum Erkennen:
      // Diskussionen, die in der Mailbox mit privaten Absendern geführt werden 
      // 
      // From: Andreas Kuhs <andreas.kuhs@outlook.com>
      // To: RogerLiebi@RogerLiebi.ch
      //     svenho@gmx.ch
      // CC: dietlind@ad-kuhs.de
      // Subject: WG: Kontaktformular rogerliebi.ch: Biblische Frage 

   }


   /// <summary>
   /// Mit diesem PW werden Daten verschlüsselt 
   /// </summary>
   public static readonly string EncryptionPW = Sensitive_AppSettings.Cfg_RecFragesteller_EncryptRecord_PW;

   /// <summary>
   /// Msg Header Wert für FragestellerPersonalDatenNotFound
   /// wenn im E-Mail Body keine Formulardaten des Fragestellers gefunden wurden 
   /// </summary>
   public static readonly string NoFormDataValue = "Keine Formulardaten gefunden";

   /// <summary>
   /// Die Standard-Header, die wir immer lesen
   ///
   /// Ev. zusätzlich interessante Header: 
   /// { "X-Mailer", "X-Priority", "X-Loop" }
   /// 
   /// </summary>
   readonly static public string[] Default_Common_EmailHeaders = {
      "Message-ID", "Thread-Topic", "Thread-Index", "References"
      , "In-Reply-To"
   };


   /// <summary>
   /// Die Liste der E-Mail Header, die ich nütze
   /// </summary>
   public enum AppMailHeaderType {

      /// <summary>
      /// Der Ordner nach der Triage:
      /// 
      ///   Kontaktformular/2b Frage Allgemein/Vorschläge für Vorträge/
      ///   Kontaktformular/2b Frage Allgemein/Zeitgeschehen/
      ///   Kontaktformular/2c Freundesbrief/
      ///   Kontaktformular/6 Suche Gemeinde/
      ///   Kontaktformular/4 Feedback Webseite/
      /// 
      /// </summary>
      TriageOrdner,
      /// <summary>
      /// Die Referenz-Nummer, e.g. #29-5858 
      /// </summary>
      MailRefNr,
      /// <summary>
      /// Header mit den HashTags, Separiert durch |
      /// </summary>
      TriageHashtags,
      /// <summary>
      /// Informationen des Fragestellers, verschlüsselt!
      /// Besteht aus Daten aus dem Header (E-Mail Adresse und Displayname der Mail-Adresse)
      /// und, wenn vorhanden, aus den Kontaktformulardaten
      /// </summary>
      FragestellerPersoenlicheDaten,
      /// <summary>
      /// Bei der Analyse des EMail Body wurde das Kontaktformular mit Daten gefunden 
      /// </summary>
      MailKontaktformularDatenFound,
      /// <summary>
      /// Bei der Analyse des EMail Body wurden keine Kontaktformular Daten gefunden 
      /// </summary>
      MailKontaktformularDatenMissing,
      /// <summary>
      /// Das vom Fragesteller gewählte Thema
      /// </summary>
      MailKontaktformularThema,
      /// <summary>
      /// Die E-Mail wurde anonymisiert
      /// </summary>
      MailInhaltAnonymisiert,
      /// <summary>
      /// Sprache Mailinhalt
      /// </summary>
      SpracheMailInhalt,

      /// <summary>
      /// Wird bei der Bereinigung des Mail-Betreffs mit dem aktuellen Datum belegt,
      /// weil dieser Msg Header ja dem Fragesteller geschickt wird,
      /// und mit seiner Antwort der Betreff wieder 'verschmutzt' werden kann 
      /// </summary>
      MailBetreffWurdeBereinigtDate,

      /// <summary>
      /// Wenn der Mail-Betreff berienigt wurde <see cref="MailBetreffWurdeBereinigtDate"/>,
      /// dann hat dieser Header eine Kopie des Original-Betreffs
      /// Dient als Sicherheit für den Fall, dass die Bereinigungslogik Fehler hat 
      /// </summary>
      OriMailBetreffBackup,

      /// <summary>
      /// Die Analyse klassifiziert die E-Mails
      /// <see cref="ConfigMail_MsgHeader.MailKategorie"/>
      /// </summary>
      MailKategorie,

      /// <summary>
      /// Der Autor der Antwort vom Team
      /// Muster:
      ///   Kürzel des Autors - Zuverlässigkeit der Erkennung in %, 0-100
      /// 
      /// Zuverlässigkeit der Erkennung
      ///   100
      ///      Teammitglied in der ReplyTo-Adresse erkannt 
      ///      Teammitglied im ReplyTo-Anzeigenamen erkannt
      ///   80
      ///      Die E-Mail Signatur wurde erkannt und darin der Name des Teammitglieds
      /// </summary>
      Author_AntwortVomTeam

   }


   /// <summary>
   /// Alle Mail-Header, die auf andere Mails im Mail-Thread übertragen werden
   /// Prozess
   /// 
   /// - Beim Mail-Versand
   ///   - Im Gesendet-Ordner werden die E-Mail Header aktualisiert,
   ///     für den Fall dass die Mail mit der Frage gelöscht wird und so z.B. Triage-Infos verloren gehen
   ///     Übertragen: Alle Header gem. dieser Liste
   /// 
   /// - Beim Mail-Empfang
   ///     Übertragen: Alle Header gem. dieser Liste
   /// 
   /// </summary>
   public static readonly List<AppMailHeaderType> TranferHdrToOtherMailsInThread = new List<AppMailHeaderType>() {
      AppMailHeaderType.MailRefNr, AppMailHeaderType.TriageOrdner, AppMailHeaderType.TriageHashtags
      , AppMailHeaderType.FragestellerPersoenlicheDaten, AppMailHeaderType.MailKontaktformularDatenFound
      , AppMailHeaderType.MailKontaktformularDatenMissing, AppMailHeaderType.SpracheMailInhalt

      // Ignoriert:
      // AppMailHeaderType.MailInhaltAnonymisiert
      // AppMailHeaderType.MailBetreffWurdeBereinigt
      // AppMailHeaderType.OriMailBetreffBackup
   };

   /// <summary>
   /// Die Liste der E-Mail Header Namen, die ich nütze
   /// </summary>
   public static readonly Dictionary<AppMailHeaderType, string> AppEMailHeader_HdrType_Name_Map
      = new Dictionary<AppMailHeaderType, string>() {
         { AppMailHeaderType.MailRefNr, "jigMailRefNr" }
         , { AppMailHeaderType.TriageOrdner, "jigTriageOrdner" }
         , { AppMailHeaderType.TriageHashtags, "jigTriageHashtags" }

         //+ Informationen des Fragestellers, verschlüsselt!
         // äöü sind nicht erlaubt für Msg Header Feldnamen
         , { AppMailHeaderType.FragestellerPersoenlicheDaten, "jigFragestellerPersoenlicheDaten" }
         , { AppMailHeaderType.MailKontaktformularDatenFound, "jigMailKontaktformularDatenFound" }
         , { AppMailHeaderType.MailKontaktformularThema, "jigMailKontaktformularThema" }
         , { AppMailHeaderType.MailKontaktformularDatenMissing, "jigMailKontaktformularDatenMissing" }
         , { AppMailHeaderType.MailInhaltAnonymisiert, "jigMailInhaltAnonymisiert" }
         , { AppMailHeaderType.SpracheMailInhalt, "jigSpracheMailInhalt" }
         , { AppMailHeaderType.MailBetreffWurdeBereinigtDate, "jigMailBetreffWurdeBereinigtDate" }
         , { AppMailHeaderType.OriMailBetreffBackup, "jigOriMailBetreffBackup" }
         , { AppMailHeaderType.MailKategorie, "jigMailKategorie" }
         , { AppMailHeaderType.Author_AntwortVomTeam, "jigAuthorAntwortVomTeam" }
      };

   /// <summary>
   /// Die Liste aller App Header 
   /// </summary>
   public static List<string> AppEMailHeader_Names = AppEMailHeader_HdrType_Name_Map.Values.ToList();


   /// <summary>
   /// Holt für eMailHeader den Namen des E-Mail Header Properties
   /// </summary>
   /// <param name="emailHeaderType"></param>
   /// <returns></returns>
   public static string GetHeadername(AppMailHeaderType emailHeaderType) {
      return AppEMailHeader_HdrType_Name_Map[emailHeaderType];
   }


   /// <summary>
   /// Das Array mit den E-Mail Header Namen
   /// </summary>
   public static HashSet<string> Get_AppEMailHeader_Names() { return AppEMailHeader_HdrType_Name_Map.Values.ToHashSet(); }


   /// <summary>
   /// Liefert alle Header, die wir immer einlesen
   /// Kombiniert die Standard Mail Header
   /// mit den App Mail Headern
   /// </summary>
   /// <returns></returns>
   public static HashSet<string> Get_DefaultEMailHeader_Names() {
      //+ Die Standard-Header
      //! mit den Mail Headern der App kombinieren
      return Default_Common_EmailHeaders.Concat(Get_AppEMailHeader_Names()).ToHashSet();
   }

}
