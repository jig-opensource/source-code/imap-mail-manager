using System;
using System.Linq;


namespace ImapMailManager.Config; 

public class ConfigTel_Basics {

   /// <summary>
   /// Predicate, das sicherstellt, dass erkannte TelNr mind. 8 Digits haben
   /// </summary>
   public static readonly Func<string, bool>? TelNrMatch_Predicate = test => (test.Count(char.IsDigit) >= 8);

}
