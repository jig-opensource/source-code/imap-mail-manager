using System;
using System.Collections.Generic;
using Hierarchy;
using ImapMailManager.Config;
using ImapMailManager.Mail;
using ImapMailManager.Mail.Hierarchy;
using ImapMailManager.Mail.Stats;


namespace ImapMailManager;

/// <summary>
/// Speichert die runtime App Config,
/// vor allem mit singletons, in denen Daten einmal berechnet werden
/// </summary>
public class AppCfgRuntime {

   /// <summary>
   /// Singleton ImapMbxDataRootNode
   /// 230223 Neu mit Lazy≺T≻
   /// !Q https://www.infoworld.com/article/3227207/how-to-perform-lazy-initialization-in-c.html
   /// </summary>

   // private static readonly Lazy<StateManager> obj = new Lazy<StateManager>(() => new StateManager());

   // private static readonly HierarchyNode<ImapHrchyNde>? _imapMbxDataRootNode = null;
   private static readonly Lazy<HierarchyNode<ImapHrchyNde>?> _imapMbxDataRootNode
      = new
         (() => ReadImapMailbox.ReadMailbox_CreateHierarchy
             (RuntimeConfig_MailServer
                .CfgImapServerRl
              , RuntimeConfig_MailServer
                .CfgImapServerCredAktiv
              , false
              , null), true);

   /// <summary>
   /// Singleton ImapMbxDataRootNode
   /// 230223 Neu mit Lazy≺T≻
   /// !Q https://www.infoworld.com/article/3227207/how-to-perform-lazy-initialization-in-c.html
   /// </summary>
   // private static List<MailboxStatisticsProKW>? _antwortenVsNeueFragen_Stats = null;
   private static Lazy<List<MailboxStatisticsProKW>?> _antwortenVsNeueFragen_Stats = new
      (() => CalcImapStats
         .Calc_AntwortenVsNeueFragen_Stats
             (
              RuntimeConfig_MailServer
                .CfgImapServerRl
              , RuntimeConfig_MailServer
                .CfgImapServerCredAktiv
              , false
              , null
              , ImapMbxDataRootNode), true);

   /// <summary>
   /// Das eingelesene IMAP Postfach in einer hierarchischen Struktur 
   /// 
   /// Singleton Logik, Thread-Safe!
   /// !Q https://csharpindepth.com/Articles/Singleton#conclusion
   /// 230223 Neu mit Lazy≺T≻
   /// !Q https://www.infoworld.com/article/3227207/how-to-perform-lazy-initialization-in-c.html
   /// </summary>
   public static HierarchyNode<ImapHrchyNde>? ImapMbxDataRootNode => _imapMbxDataRootNode.Value;

   /// <summary>
   /// Singleton Die statistischen Daten der Mailbox für den Vergleich
   /// Antworten vs neue Fragen 
   /// 230223 Neu mit Lazy≺T≻
   /// !Q https://www.infoworld.com/article/3227207/how-to-perform-lazy-initialization-in-c.html
   /// </summary>
   public static List<MailboxStatisticsProKW>? AntwortenVsNeueFragen_Stats => _antwortenVsNeueFragen_Stats.Value;

}
