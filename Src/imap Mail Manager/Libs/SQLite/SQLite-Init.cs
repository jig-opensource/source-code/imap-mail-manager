﻿using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using jig.Tools;
using jig.Tools.String;


namespace ImapMailManager.Libs.SQLite {

	/// <summary>
	/// Initialisiert eine SQLite DB oder lädt die DB
	/// !M https://zetcode.com/csharp/sqlite/
	/// </summary>
	public class SQLite_Init {

      #region Fields

      // Die DB im RAM
      // public readonly string DbSrc = "Data Source=:memory:";
      // Die DB in einem File
      public readonly string DbSrc =
         @"URI=file:c:\GitWork\GitLab.com\jig-Opensource\Source-Code\imap-Mail-Manager\imap-Mail-Manager.sqlite";

      protected readonly SQLiteConnection Con;

      private string _sqLiteVersion;

      #endregion


      #region Constructors and Destructors

      /// <summary>
      /// Constructor
      /// </summary>
      public SQLite_Init() {
         // Verbindung öffnen
         Con = new SQLiteConnection(DbSrc);
         Con.Open();

         // Die Version lesen
         Debug.WriteLine($"SQLite version: {SqLiteVersion}");

         var dddd = SqLiteTables;

         // Allenfalls die DB initialisieren
         if (SqLiteTables.Count == 0) { InitDB(); }
      }

      #endregion


      #region Public Properties

      /// <summary>
      /// Liefert die Tabellen der SQLIte DB
      /// </summary>
      public List<string> SqLiteTables {
         get {
            var qry = @"SELECT name
				  FROM sqlite_schema
				  WHERE type ='table'
						  AND name NOT LIKE 'sqlite_%';
				";

            using var cmd        = new SQLiteCommand(qry, Con);
            var       dataReader = cmd.ExecuteReader();

            List<string> tables = (
                                     from IDataRecord r in dataReader
                                     select r.GetString(0)
                                  ).ToList();

            return tables;
         }
      }

      /// <summary>
      /// Liefert die SQLite version der DB
      /// </summary>
      public string SqLiteVersion {
         get {
            // Query für die Version
            var QryVersion = "SELECT SQLITE_VERSION()";

            if (!_sqLiteVersion.ØHasValue()) {
               using var cmd = new SQLiteCommand(QryVersion, Con);
               _sqLiteVersion = cmd.ExecuteScalar().ToString() ?? "(unknown)";
            }

            return _sqLiteVersion;
         }
      }

      #endregion


      #region Public Methods and Operators

      /// <summary>
      /// Die Tabellen initialisieren
      /// </summary>
      public void InitDB() {
         using var dbCmd = new SQLiteCommand(Con);

         dbCmd.CommandText = "DROP TABLE IF EXISTS MailboxMails";
         dbCmd.ExecuteNonQuery();

         // Datentypen
         // !M https://www.sqlite.org/datatype3.html
         dbCmd.CommandText = @"CREATE TABLE MailboxMails(ID INTEGER PRIMARY KEY,
            MessageId TEXT,
				Date NUMERIC,
				Sender TEXT,
				'From' TEXT
				'To' TEXT,
				Subject TEXT, 
				SubjectNormalized TEXT,
				NoLongerInMailbox NUMERIC
				);";

         dbCmd.ExecuteNonQuery();

         /*
         dbCmd.CommandText = "INSERT INTO cars(name, price) VALUES('Audi',52642)";
         dbCmd.ExecuteNonQuery();
      
         dbCmd.CommandText = "INSERT INTO cars(name, price) VALUES('Mercedes',57127)";
         dbCmd.ExecuteNonQuery();
      
         dbCmd.CommandText = "INSERT INTO cars(name, price) VALUES('Skoda',9000)";
         dbCmd.ExecuteNonQuery();
      
         dbCmd.CommandText = "INSERT INTO cars(name, price) VALUES('Volvo',29000)";
         dbCmd.ExecuteNonQuery();
      
         dbCmd.CommandText = "INSERT INTO cars(name, price) VALUES('Bentley',350000)";
         dbCmd.ExecuteNonQuery();
      
         dbCmd.CommandText = "INSERT INTO cars(name, price) VALUES('Citroen',21000)";
         dbCmd.ExecuteNonQuery();
      
         dbCmd.CommandText = "INSERT INTO cars(name, price) VALUES('Hummer',41400)";
         dbCmd.ExecuteNonQuery();
      
         dbCmd.CommandText = "INSERT INTO cars(name, price) VALUES('Volkswagen',21600)";
         dbCmd.ExecuteNonQuery();
         */

         Debug.WriteLine("Table cars created");
      }

      #endregion

   }

}
