using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using jig.Tools;
using jig.Tools.String;
using NTextCat;


namespace ImapMailManager.Libs.Text_Spracherkennung.NTextCat;

/// <summary>
/// Erkenntnisse
///   - Sprach-Profile:
///      \\basdc002\schittli$\Wissen, KH, Knowhow - Eigenes\SW - Cloud - Spracherkennung - Text\!9 NTextCat, offline-Analyse\!230122 ntextcat-master\ntextcat-master\src\LanguageModels\
///   - Wiki280.profile.xml
///      280 Sprachen, vermutlich deshalb hohe Ungenauigkeit
///   - Core14.profile.xml
///      14 Sprachen, vermutlich das beste Profil
///   - Wiki82.profile.xml
///      82 Sprachen, Warnung im XML wegen falscher Codierung
/// 
/// Strategie
///   - Wahl: Core14.profile.xml
///   - Für fehlende Sprachen aus Wiki280.profile.xml kopieren  
/// 
/// </summary>
public class NTextCat_JigTools {

   /// <summary>
   /// Der Name des Language-Modells
   /// Wiki280.profile.xml
   /// Wiki82.profile.xml
   /// </summary>
   private static readonly string LanguageModelFileName = @"Core14.profile.xml";
   public static           string? LanguageModelFullFileName { get; private set; }
   private static readonly string  LanguageModelModuleDir = @"Libs\Text_Spracherkennung\NTextCat";
   /// <summary>
   /// Singleton
   /// </summary>
   private static string? _sprachmodellDir = null;
   private static readonly object                           padlock_sprachmodellDir           = new object();
   private static          RankedLanguageIdentifierFactory? _languageIdentifierFactory        = null;
   private static readonly object                           padlock_languageIdentifierFactory = new object();
   private static          RankedLanguageIdentifier?        _languageIdentifier               = null;
   private static readonly object                           padlock_languageIdentifier        = new object();

   #region Singletons

   /// <summary>
   /// Liefert das Verzeichnis, in dem die Sprach-Modelle sind
   /// .\bin\Debug\net6.0-windows\Libs\Text_Spracherkennung\NTextCat\
   /// </summary>
   public static string SprachmodellDir {
      get {
         lock (padlock_sprachmodellDir) {
            if (_sprachmodellDir == null) {
               var thisAssemblyDir = Path.GetDirectoryName
                  (System.Reflection.Assembly.GetExecutingAssembly().Location);
               _sprachmodellDir = Path.Combine(thisAssemblyDir, LanguageModelModuleDir);
            }

            return _sprachmodellDir;
         }
      }
   }

   /// <summary>
   /// Singleton Logik, Thread-Safe!
   /// !Q https://csharpindepth.com/Articles/Singleton#conclusion
   /// </summary>
   public static RankedLanguageIdentifierFactory LanguageIdentifierFactory {
      get {
         lock (padlock_languageIdentifierFactory) {
            if (_languageIdentifierFactory == null) {
               // Don't forget to deploy a language profile (e.g. Core14.profile.xml) with your application.
               // (take a look at "content" folder inside of NTextCat nupkg and here: https://github.com/ivanakcheurov/ntextcat/tree/master/src/LanguageModels).
               _languageIdentifierFactory = new RankedLanguageIdentifierFactory();
            }

            return _languageIdentifierFactory;
         }
      }
   }

   /// <summary>
   /// Singleton Logik, Thread-Safe!
   /// Das Sprachmodell
   /// !Q https://csharpindepth.com/Articles/Singleton#conclusion
   /// </summary>
   public static RankedLanguageIdentifier RankedLanguageIdentifier {
      get {
         lock (padlock_languageIdentifier) {
            if (_languageIdentifier == null) {

               LanguageModelFullFileName = Path.Combine(SprachmodellDir, LanguageModelFileName);
               _languageIdentifier       = LanguageIdentifierFactory.Load(LanguageModelFullFileName);

            }

            return _languageIdentifier;
         }
      }
   }

   #endregion Singletons


   /// <summary>
   /// GetCultureInfo("German").NativeName » Deutsch
   /// GetCultureInfo("English").ThreeLetterISOLanguageName » eng
   ///
   /// !Ex
   ///   var cultureInfo = GetCultureInfo("German");
   ///   GetCultureInfo(threeLetterISOLanguageName: "ger")
   /// </summary>
   /// <param name="englishName"></param>
   /// <param name="threeLetterIsoCode"></param>
   /// <returns></returns>
   public static CultureInfo? GetCultureInfo(string? englishName = null, string? threeLetterIsoCode = null) {
      if (englishName.ØHasValue()) {
         foreach (CultureInfo info in CultureInfo.GetCultures(CultureTypes.AllCultures)) {
            if (info.EnglishName.ØEqualsIgnoreCase(englishName))
               return new CultureInfo(info.Name);
         }
      }

      if (threeLetterIsoCode.ØHasValue()) {
         foreach (CultureInfo info in CultureInfo.GetCultures(CultureTypes.AllCultures)) {
            if (info.ThreeLetterISOLanguageName.ØEqualsIgnoreCase(threeLetterIsoCode))
               return new CultureInfo(info.Name);
         }

      }

      return null;
   }


   /// <summary>
   /// Erkennt die Sprache des Textes
   /// </summary>
   /// <param name="text"></param>
   /// <param name="fallbackSprache"></param>
   public static CultureInfo Detect_Language(string text, CultureInfo? fallbackSprache = null) {
      //+ Wenn keine Sprache erkannt wird
      fallbackSprache ??= CultureInfo.GetCultureInfo("de");

      //+ Die Sprache erkennen
      var languages = RankedLanguageIdentifier.Identify(text);

      //+ Die wahrscheinlichste Sprache holen 
      // Das Resultat ist *aufsteigend* sortiert,
      // dh das Element mit dem tiefsten wert in Item2 ist die wahrscheinlichste Sprache
      // var sorted = languages.OrderBy(x => x.Item2);
      var mostCertainLanguage = languages.FirstOrDefault();

      //+ Konvertieren
      var dotCultureInfo = GetCultureInfo(threeLetterIsoCode: mostCertainLanguage.Item1.Iso639_3);

      return dotCultureInfo ?? fallbackSprache;

   }


   public static void Tester() {

      var thisAssemblyDir = Path.GetDirectoryName
         (System.Reflection.Assembly.GetExecutingAssembly().Location);

      var libSubDir  = @"Libs\Text_Spracherkennung\NTextCat";
      var thisLibDir = Path.Combine(thisAssemblyDir, libSubDir);

      // var languageFileName     = @"Wiki280.profile.xml";
      // var languageFileName     = @"Wiki82.profile.xml";
      var languageFileName     = @"Core14.profile.xml";
      var thisLanguageFileName = Path.Combine(thisLibDir, languageFileName);

      //+ Load XML
      XElement purchaseOrder = XElement.Load(thisLanguageFileName);

      int stopper = 1;

      // Don't forget to deploy a language profile (e.g. Core14.profile.xml) with your application.
      // (take a look at "content" folder inside of NTextCat nupkg and here: https://github.com/ivanakcheurov/ntextcat/tree/master/src/LanguageModels).
      RankedLanguageIdentifierFactory factory = new RankedLanguageIdentifierFactory();

      // var identifier          = factory.Load("Core14.profile.xml");
      RankedLanguageIdentifier? identifier          = factory.Load(thisLanguageFileName);
      var                       languages           = identifier.Identify("your text to get its language identified");
      var                       mostCertainLanguage = languages.FirstOrDefault();

      var sorted = languages.OrderByDescending(x => x.Item2);

      if (mostCertainLanguage != null)
         Console.WriteLine("The language of the text is '{0}' (ISO639-3 code)", mostCertainLanguage.Item1.Iso639_3);
      else
         Console.WriteLine("The language couldn’t be identified with an acceptable degree of certainty");

      // outputs: The language of the text is 'eng' (ISO639-3 code)

   }

}
