using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DetectLanguage;
using ImapMailManager.Properties;


namespace ImapMailManager.Libs.Text_Spracherkennung.DetectLanguage;

public class DetectLanguage_JigTools {

   //+ Config

   /// <summary>
   /// Wir suchen einen Satz mind mind. dieser Anzahl Zeichen,
   /// den wir dann zur Analyse schicken 
   /// </summary>
   const int RestApi_Satz_MinLength = 25;


   /// <summary>
   /// Liefert Details zum Konto-Status und dem Abo
   /// </summary>
   public static async Task Get_KontoStatus() {

      // Test
      // !M https://github.com/detectlanguage/detectlanguage-dotnet
      DetectLanguageClient client = new DetectLanguageClient(Sensitive_AppSettings.Cfg_Libs_DetectLanguage_ApiKey);

      //+ Der Konto-Status
      UserStatus userStatus = await client.GetUserStatusAsync();
   }


   /// <summary>
   /// Liefert die unterstützten Sprachen
   /// </summary>
   public static async Task Get_SuppoertedLanguages() {

      // Test
      // !M https://github.com/detectlanguage/detectlanguage-dotnet
      DetectLanguageClient client = new DetectLanguageClient(Sensitive_AppSettings.Cfg_Libs_DetectLanguage_ApiKey);

      //+ Suppoerted Languages
      Language[] languages = await client.GetLanguagesAsync();
   }


   public record AnfrageTextSprache(
      string   Id
      , string TextOriginal
      , string TextAnfrage
      , string ErkannteSprache) {

      public AnfrageTextSprache() : this
         (""
          , ""
          , ""
          , "") {}


      public string Id              { get; set; } = Id;
      public string TextOriginal    { get; set; } = Id;
      public string TextAnfrage     { get; set; } = Id;
      public string ErkannteSprache { get; set; } = Id;

   }


   /// <summary>
   /// Detektiert die Sprache des Texts
   /// Die Lizenz begrenzt pro Tag:
   /// - die Anz Aufrufe pro Tag
   /// - Die Anzahl Zeichen
   /// </summary>
   public static async Task<string> DetectLanguage(string text) {

      // !M https://github.com/detectlanguage/detectlanguage-dotnet
      DetectLanguageClient client = new DetectLanguageClient(Sensitive_AppSettings.Cfg_Libs_DetectLanguage_ApiKey);

      //+ Alle Sätze, der Länge nach sortiert
      IList<string> sätze = text.Split('.')
                                .Select(s => s.Trim())
                                .Where(s => s != String.Empty)
                                .OrderByDescending(s => s.Length)
                                .ToList();

      var testSatz = "";

      //+ Den ersten Satz suchen, der die Mindestlönge hat
      bool found = false;

      for (var idxSatz = 0; idxSatz < sätze.Count && !found; idxSatz++) {
         var satz = sätze[idxSatz];

         // Den ersten Satz nehmen, der kang genug ist
         if (satz.Length >= RestApi_Satz_MinLength) {
            found    = true;
            testSatz = satz;
         }
      }

      //+ Wenn nicht gefunden, dann den länsgten, dh. letzten Satz wählen
      if (!found) {
         testSatz = sätze[^1];
      }

      //+ Spracherkennung für 1 Text, gewichtete Resultat-Liste
      // DetectResult[] results1 = await client.DetectAsync(testSatz);

      //+ Spracherkennung für 1 Text, die wahrscheinlichste Sprache
      string languageCode = await client.DetectCodeAsync(testSatz);

      return languageCode;
   }


   /// <summary>
   /// Detektiert die Sprache der Texte
   /// 
   /// Die Lizenz begrenzt pro Tag:
   /// - die Anz Aufrufe pro Tag
   /// - Die Anzahl Zeichen
   /// </summary>
   public static async Task DetectLanguage(List<string> texte) {

      //+ Config
      const int testSatzMinLen = 25;

      // Weil die Rest API Lizenz auf die Anz. Zeichen zählt,
      // Wählen wird von jedem Text nur einen Satz mit 30+ Zeichen
      List<AnfrageTextSprache> anfrageTexte = new List<AnfrageTextSprache>();

      for (var idx = 0; idx < texte.Count; idx++) {
         var originalText = texte[idx];

         var rec = new AnfrageTextSprache();
         rec.TextOriginal = originalText;

         //+ Alle Sätze, der Länge nach sortiert
         IList<string> sätze = originalText.Split('.')
                                           .Select(s => s.Trim())
                                           .Where(s => s != String.Empty)
                                           .OrderByDescending(s => s.Length)
                                           .ToList();

         var testSatz = "";

         // Sie sind der Länge nach sortiert
         bool found = false;

         for (var idxSatz = 0; idxSatz < sätze.Count && !found; idxSatz++) {
            var satz = sätze[idxSatz];

            // Den ersten Satz nehmen, der kang genug ist
            if (satz.Length >= testSatzMinLen) {
               found    = true;
               testSatz = satz;
            }
         }

         //+ Wenn nicht gefunden, dann den länsgten, dh. letzten Satz wählen
         if (!found) {
            testSatz = sätze[^1];
         }

         rec.TextAnfrage = testSatz;

         anfrageTexte.Add(new AnfrageTextSprache());
      }

      List<string> texteZurAnfrage = new List<string>();

      var arrAnfrageTexte = anfrageTexte.Select(x => x.TextAnfrage).ToArray();

      // Test
      // !M https://github.com/detectlanguage/detectlanguage-dotnet
      DetectLanguageClient client = new DetectLanguageClient(Sensitive_AppSettings.Cfg_Libs_DetectLanguage_ApiKey);

      //+ Mehrere Texte per Batch erkennen, gewichtete Resultat-Liste
      string[]         texts                      = { "labas rytas", "good morning" };
      DetectResult[][] resRestApiErkannteSprachen = await client.BatchDetectAsync(arrAnfrageTexte);

      //+ Die Resultate analysieren
      List<string> resSprache = new List<string>();

      for (int idxLine = 0; idxLine < resRestApiErkannteSprachen.Length; idxLine++) {
         var resTextSprache = resRestApiErkannteSprachen[idxLine];

         string erkannteSprache = resTextSprache
                                 .OrderByDescending(x => x.reliable)
                                 .ThenByDescending(x => x.confidence).First().language;
         resSprache.Add(erkannteSprache);
      }

      var stopper = 1;

   }


   /// <summary>
   /// Eine Test-Funktion
   /// </summary>
   public static async Task Tester() {

      // Test
      // !M https://github.com/detectlanguage/detectlanguage-dotnet
      DetectLanguageClient client = new DetectLanguageClient(Sensitive_AppSettings.Cfg_Libs_DetectLanguage_ApiKey);

      //+ Spracherkennung für 1 Text, gewichtete Resultat-Liste
      DetectResult[] results1 = await client.DetectAsync("Buenos dias señor");

      //+ Spracherkennung für 1 Text, die wahrscheinlichste Sprache
      string languageCode = await client.DetectCodeAsync("Buenos dias señor");

      //+ Mehrere Texte per Batch erkennen, gewichtete Resultat-Liste
      string[]         texts                      = { "labas rytas", "good morning" };
      DetectResult[][] resRestApiErkannteSprachen = await client.BatchDetectAsync(texts);

      //+ Die Resultate analysieren
      List<string> resSprache = new List<string>();

      for (int idxLine = 0; idxLine < resRestApiErkannteSprachen.Length; idxLine++) {
         var resTextSprache = resRestApiErkannteSprachen[idxLine];

         string erkannteSprache = resTextSprache
                                 .OrderByDescending(x => x.reliable)
                                 .ThenByDescending(x => x.confidence).First().language;
         resSprache.Add(erkannteSprache);
      }

      var stopper = 1;

   }

}
