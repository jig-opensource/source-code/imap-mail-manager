using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Catalyst;
using Catalyst.Models;
using Mosaik.Core;


namespace ImapMailManager.Libs.Text_Spracherkennung.Catalyst;

public class Catalyst_JigTools {

   public static async Task Tester() {

      var testTextEN = "pre-register each language (and install the respective NuGet Packages)";
      var testTextDE = "jede Sprache vorregistrieren (und die entsprechenden NuGet-Pakete installieren)";
      var testTextFR = "pré-enregistrer chaque langue (et installer les paquets NuGet respectifs)";

      var cld2LanguageDetector = await LanguageDetector.FromStoreAsync
                                    (Language.English
                                     , Version.Latest
                                     , "");

      var doc2EN = new Document(testTextEN);
      cld2LanguageDetector.Process(doc2EN);
      Debug.WriteLine($"Actual:CLD2\t{doc2EN.Language}");

      var docDE = new Document(testTextDE);
      cld2LanguageDetector.Process(docDE);
      Debug.WriteLine($"Actual:CLD2\t{docDE.Language}");

      var docFR = new Document(testTextFR);
      cld2LanguageDetector.Process(docFR);
      Debug.WriteLine($"Actual:CLD2\t{docFR.Language}");


      //‼ aa 
      var thisAssemblyDir = Path.GetDirectoryName
         (System.Reflection.Assembly.GetExecutingAssembly().Location);

      var subDirLib  = @"Libs\Text_Spracherkennung\Catalyst";
      var thisLibDir = Path.Combine(thisAssemblyDir, subDirLib);

      var subDirSprachmodelle     = @"LanguageModels";
      var thisSubDirSprachmodelle = Path.Combine(thisLibDir, subDirSprachmodelle);

      //+ Allenfalls das Verzeichnis erstellen
      if (!Directory.Exists(thisSubDirSprachmodelle)) {
         Directory.CreateDirectory(thisSubDirSprachmodelle);
      }


      Storage.Current = new DiskStorage(thisSubDirSprachmodelle);

      var fastTextLanguageDetector = await FastTextLanguageDetector.FromStoreAsync
                                        (Language.Any
                                         , Version.Latest
                                         , "");

      var Fr2 = new Document(testTextFR);
      
      var allPredictions = fastTextLanguageDetector.Predict(Fr2);

      int stoppper = 1;
   }


   public static async Task Tester2() {
      // pre-register each language (and install the respective NuGet Packages)
      // Catalyst.Models.English.Register(); 

      Storage.Current = new DiskStorage("catalyst-models");
      var nlp = await Pipeline.ForAsync(Language.English);
      var doc = new Document("The quick brown fox jumps over the lazy dog", Language.English);
      nlp.ProcessSingle(doc);
      Debug.WriteLine(doc.ToJson());
   }


   public static async Task DownloadLanguageModels() {
      //+ Das Verzeichnis bestimmen

      var thisAssemblyDir = Path.GetDirectoryName
         (System.Reflection.Assembly.GetExecutingAssembly().Location);

      var subDirLib  = @"Libs\Text_Spracherkennung\Catalyst";
      var thisLibDir = Path.Combine(thisAssemblyDir, subDirLib);

      var subDirSprachmodelle     = @"LanguageModels";
      var thisSubDirSprachmodelle = Path.Combine(thisLibDir, subDirSprachmodelle);

      //+ Allenfalls das Verzeichnis erstellen
      if (!Directory.Exists(thisSubDirSprachmodelle)) {
         Directory.CreateDirectory(thisSubDirSprachmodelle);
      }

      List<Language> meineSprachen = new List<Language>() {
         Language.English, Language.German, Language.French, Language.Afrikaans
      };

      foreach (var sprache in meineSprachen) {
         // Creates and stores the model
         PatternSpotter isApattern = new PatternSpotter
            (sprache
             , 0
             , tag: "is-a-pattern"
             , captureTag: "IsA");

         isApattern.NewPattern
            (
             "Is+Noun"
             , mp => mp.Add
                  (
                   new PatternUnit(PatternUnitPrototype.Single().WithToken("is").WithPOS(PartOfSpeech.VERB))
                   , new PatternUnit
                      (PatternUnitPrototype.Multiple().WithPOS
                          (PartOfSpeech.NOUN
                           , PartOfSpeech.PROPN
                           , PartOfSpeech.AUX
                           , PartOfSpeech.DET
                           , PartOfSpeech.ADJ))
                  ));

         var fileName =
            Path.Combine(thisSubDirSprachmodelle, $"{sprache}.bin");

         using (var f = File.OpenWrite(fileName)) {
            await isApattern.StoreAsync(f);
         }

      }

      return;

      /*
      // Load the model back from disk
      var isApattern2 = new PatternSpotter
         (Language.English
          , 0
          , tag: "is-a-pattern"
          , captureTag: "IsA");

      using (var f = File.OpenRead("my-pattern-spotter.bin")) {
         await isApattern2.LoadAsync(f);
      }
*/

   }

}
