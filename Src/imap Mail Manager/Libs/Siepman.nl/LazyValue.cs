using System;
using System.Collections.Generic;


namespace ImapMailManager.Libs.Siepman.nl;

/// <summary>
/// !Q https://www.siepman.nl/blog/lazyt-property-caching-alternative
/// Muss Elemente nicht im Konstruktor initialisieren
///
/// Patched by TomTom
/// </summary>
public class LazyValue<T> {

   /// <summary>
   /// Patched TomTom:
   /// Nullable wollen wir auch speichern können!
   /// </summary>
   private T? _value;
   public           bool   IsInitialized { get; private set; }
   private readonly object _lock = new object();


   /// <summary>
   /// Patched TomTom:
   /// Den Wert zurücksetzen
   /// </summary>
   public void Reset() {
      if (IsInitialized) {
         // Double-checked locking pattern
         lock (_lock) {
            if (IsInitialized) {
               // Double-checked locking pattern
               IsInitialized = false;
               _value        = default(T);
            }
         }
      }
   }


   /// <summary>
   /// 
   /// </summary>
   /// <param name="producer"></param>
   /// <returns></returns>
   /// <exception cref="ArgumentNullException"></exception>
   public T? GetValue(Func<T> producer) {
      if (producer == null) {
         throw new ArgumentNullException("producer");
      }

      if (!IsInitialized) // Double-checked locking pattern
      {
         lock (_lock) {
            if (!IsInitialized) // Double-checked locking pattern
            {
               _value = ConvertToListIfNecessary(producer());

               IsInitialized = true;
            }
         }
      }

      return _value;
   }


   private T? ConvertToListIfNecessary(dynamic value) { return MaybeToList(value); }


   private LazyList<TP> MaybeToList<TP>(IEnumerable<TP> value) {
      // LazyList<T>, see other post:
      // https://www.siepman.nl/Home/Blog/363e34a3-f2a0-4be9-ada9-a6a2e6b07672
      return new LazyList<TP>(value);
   }


   private IList<TP> MaybeToList<TP>(IList<TP> value) { return value; }

   private object MaybeToList(object value) { return value; }

}
