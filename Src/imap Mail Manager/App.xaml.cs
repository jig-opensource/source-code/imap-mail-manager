﻿using System;
using System.IO;
using System.Windows;
using CliFx;
using ImapMailManager.Config;
using ImapMailManager.Mail;
using ImapMailManager.Properties;
using ImapMailManager.UI;
using Microsoft.Extensions.Configuration;


namespace ImapMailManager;

/// <summary>
/// Interaction logic for App.xaml
/// </summary>
public partial class App : Application {

   internal static Sensitive_AppSettings SensitiveAppSettings = null;
   internal static Cfg_TeamConfig        Cfg_TeamConfig       = null;
   
   #region Internal Methods

   /// <summary>
   /// Die WPF Main Funktion
   /// !TT: App.xaml
   /// Direkt ein Windows öffnen:
   /// StartupUri = "/UI/MainWindow.xaml"
   /// Zum Start eine Funktion nützen:
   /// Startup = "App_Startup"
   /// !Q https://stackoverflow.com/a/11770322/4795779
   /// </summary>
   /// <param name="sender"></param>
   /// <param name="e"></param>
   /// <exception cref="NotImplementedException"></exception>
   private async void App_Startup(object sender, StartupEventArgs e) {

      //+ ! Die sensitiven Config-Daten laden
      
      //+++ JsonConfigurationProvider 
      // !M https://learn.microsoft.com/de-de/dotnet/api/microsoft.extensions.configuration.json.jsonconfigurationprovider?view=dotnet-plat-ext-7.0
      var sensitiveAppSettings_Builder = new ConfigurationBuilder();

      //+++ Das Config-File:
      // Properties\sensitive-appsettings.json
      sensitiveAppSettings_Builder.SetBasePath(Path.Join(Directory.GetCurrentDirectory(), "Properties"))
                                  .AddJsonFile
                                      ("sensitive-appsettings.json"
                                       , optional: false
                                       , reloadOnChange: true);

      // ‼ Erweitertes Beispiel:
      // Sich ergänzende Configs,
      // zB um eine Standard-Config mit Debug / Prod-Configs zu ergänzen
      /*
      builder.SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile
                 ("appsettings.json"
                  , optional: false
                  , reloadOnChange: true)
             .AddJsonFile
                 ("appsettings.dev.json"
                  , optional: true
                  , reloadOnChange: true);
      */

      //+ Die Sensitiven Config-Daten laden
      //++ sensitive-appsettings.json
      SensitiveAppSettings = new Sensitive_AppSettings(sensitiveAppSettings_Builder.Build());

      //++ Team-Config
      Cfg_TeamConfig = Sensitive_Team_Mitglieder.ReadYaml();
      
      //+ Application is running
      
      //‼ CliFx Commandline Framework

      var res = await new CliApplicationBuilder()
           .AddCommandsFromThisAssembly()
           .Build()
           .RunAsync();
      
      // return;
      
      //++ Process command line args
      var startMinimized = false;

      for (var i = 0; i != e.Args.Length; ++i) {
         if (e.Args[i] == "/StartMinimized") { startMinimized = true; }
      }

      // Create main application window, starting minimized if specified
      var mainWindow = new MainWindow(App.SensitiveAppSettings.ImapSrcCfg, RuntimeConfig_MailServer.CfgImapServerCredAktiv);

      if (startMinimized) { mainWindow.WindowState = WindowState.Minimized; }

      mainWindow.Show();
   }

   #endregion


   private void Application_Exit(object sender, ExitEventArgs e) {
      // Die Settings speichern
      ImapMailManager.Properties.Settings.Default.Save();
   }

}
