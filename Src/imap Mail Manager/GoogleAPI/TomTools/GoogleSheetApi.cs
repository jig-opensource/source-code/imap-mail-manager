﻿using System;
using System.Collections.Generic;// .// 
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.Drawing.Charts;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper;
using ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper.StatsMbxAntwortenVsNeueFragen;
using jig.Tools;
using jig.Tools.Exception;
using jig.Tools.String;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ImapMailManager.GoogleAPI.TomTools;

/// <summary> 
/// Config für GSheets
/// 
/// !Tut9 https://dottutorials.net/google-sheets-read-write-operations-dotnet-core-tutorial/#setting-up-google-sheet
/// !Ex https://github.com/shehryarkn/Google-Sheets-Read-Write-Update-using-Dotnet-Core
/// </summary>
public class GoogleSheetApi {

   #region Public Methods and Operators

   /// <summary>
   /// Sucht in einem Excel-Range die Zeile mit dem Header
   /// Gesucht werden die Zellen,
   /// die im Notiz-Feld eine Notiz haben:
   /// ObjID:
   /// <SpaltenName>
   /// SpaltenName ist die ID der Spalte, die als Mapping dient,
   /// so dass der Text im Spaltentitel frei geändert  werden kann
   /// </summary>
   /// <param name="gridData0"></param>
   /// <exception cref="RuntimeException"></exception>
   public static Tuple<int?, IDictionary<string, int>> FindTableHeaderData(GridData gridData0) {
      /// In den Zeilen die Zeile mit dem Header suchen
      var myRegexOptions = RegexOptions.IgnoreCase
                           | RegexOptions.Multiline
                           | RegexOptions.ExplicitCapture
                           | RegexOptions.CultureInvariant
                           | RegexOptions.IgnorePatternWhitespace
                           | RegexOptions.Compiled;

      var rgxCellnoteObjID = @"(?<ObjID>ObjID\s*:\s*(?<ObjName>\w+))";
      var oRgx             = new Regex(rgxCellnoteObjID, myRegexOptions);

      IDictionary<string, int> headerCols     = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
      int?                     headerFoundRow = null;

      for (var idxRow = 0; idxRow < gridData0.RowData.Count && headerFoundRow == null; idxRow++) {
         var row = gridData0.RowData[idxRow];

         for (var idxCol = 0; idxCol < row.Values.Count; idxCol++) {
            var cellData = row.Values[idxCol];

            // finden wir in der Zell-Note das Muster
            // ObjID:<SpaltenName>?
            var match = oRgx.ØMatch(cellData.Note);

            if (match.Success) {
               // Wir haben die Header-Zeile gefunden
               headerFoundRow = idxRow;
               var objID   = match.Groups["ObjID"].Value;
               var objName = match.Groups["ObjName"].Value;

               if (headerCols.ContainsKey(objName)) {
                  throw new RuntimeException($"Die Spalte {objName} ist in den Zellen-Notes doppelt definiert!");
               }

               // Spalten-Referenz gefunden, also die ObjReferenz und die Spalte merken
               headerCols.Add(objName, idxCol);
            }
         }
      }

      return new Tuple<int?, IDictionary<string, int>>(headerFoundRow, headerCols);
   }


   /// <summary>
   /// Erzeugt aus Koordinaten in Zeilen / Spalten und Sheetnamen
   /// eine GSheet (auch Excel) A1-Notation
   /// !Sj Cell-Referenzen, Ranges, Bereich, Scope
   /// !Sj A1 notation
   /// !KH
   /// https://developers.google.com/sheets/api/guides/concepts#cell
   /// Sheet1!A1:B2				refers to the first two cells in the top two rows of Sheet1.
   /// Sheet1!A:A				refers to all the cells in the first column of Sheet1.
   /// Sheet1!1:2				refers to all the cells in the first two rows of Sheet1.
   /// Sheet1!A5:A				refers to all the cells of the first column of Sheet 1, from row 5 onward.
   /// A1:B2						refers to the first two cells in the top two rows of the first visible sheet.
   /// Sheet1					refers to all the cells in Sheet1.
   /// 'My Custom Sheet'!A:A	refers to all the cells in the first column
   /// of a sheet named "My Custom Sheet."
   /// Single quotes are required for sheet names with spaces,
   /// special characters, or an alphanumeric combination.
   /// 'My Custom Sheet'		refers to all the cells in 'My Custom Sheet'.
   /// </summary>
   /// <param name="sheetName"></param>
   /// <param name="colNoStart1Based"></param>
   /// <param name="colNoEnd1Based"></param>
   /// <param name="rowNoStart1Based"></param>
   /// <param name="rowNoEnd1Based"></param>
   /// <returns></returns>
   public static string GetA1Notion(
      string? sheetName
      , int?  colNoStart1Based
      , int?  colNoEnd1Based
      , int?  rowNoStart1Based
      , int?  rowNoEnd1Based) {
      Assert.IsTrue
         (
          !colNoStart1Based.HasValue || (colNoStart1Based.HasValue && colNoStart1Based.Value > 0)
          , "GetA1Notion(): colNoStart1Based muss > 0 sein!");

      Assert.IsTrue
         (
          !colNoEnd1Based.HasValue || (colNoEnd1Based.HasValue && colNoEnd1Based.Value > 0)
          , "GetA1Notion(): colNoEnd1Based muss > 0 sein!");

      Assert.IsTrue
         (
          !rowNoStart1Based.HasValue || (rowNoStart1Based.HasValue && rowNoStart1Based.Value > 0)
          , "GetA1Notion(): rowNoStart1Based muss > 0 sein!");

      Assert.IsTrue
         (
          !rowNoEnd1Based.HasValue || (rowNoEnd1Based.HasValue && rowNoEnd1Based.Value > 0)
          , "GetA1Notion(): rowNoEnd1Based muss > 0 sein!");

      var sb = new StringBuilder();

      if (sheetName.ØHasValue()) {
         sb.Append($"'{sheetName}'!");
      }

      if (colNoStart1Based.HasValue) {
         sb.Append(GetExcelColumnName(colNoStart1Based.Value));
      }

      if (rowNoStart1Based.HasValue) {
         sb.Append(rowNoStart1Based.Value);
      }

      sb.Append(":");

      if (colNoEnd1Based.HasValue) {
         sb.Append(GetExcelColumnName(colNoEnd1Based.Value));
      }

      if (rowNoEnd1Based.HasValue) {
         sb.Append(rowNoEnd1Based.Value);
      }

      return sb.ToString();
   }


   /// <summary>
   /// In der A1 Notation sind die Index 1 basiert!
   /// Konvertieren in die A1 Notation
   /// !M https://developers.google.com/sheets/api/guides/concepts#cell
   /// Sheet1!A1:B2			 refers to the first two cells in the top two rows of Sheet1.
   /// Sheet1!A:A				 refers to all the cells in the first column of Sheet1.
   /// Sheet1!1:2				 refers to all the cells in the first two rows of Sheet1.
   /// Sheet1!A5:A				 refers to all the cells of the first column of Sheet 1, from row 5 onward.
   /// A1:B2						 refers to the first two cells in the top two rows of the first visible sheet.
   /// Sheet1					 refers to all the cells in Sheet1.
   /// 'My Custom Sheet'!A:A refers to all the cells in the first column
   /// of a sheet named "My Custom Sheet."
   /// Single quotes are required for sheet names with spaces,
   /// special characters, or an alphanumeric combination.
   /// 'My Custom Sheet'		 refers to all the cells in 'My Custom Sheet'.
   /// 1: A
   /// 2: B
   /// 3: C
   /// 24: X
   /// 25: Y
   /// 26: Z
   /// 27: AA
   /// 28: AB
   /// 29: AC
   /// 700: ZX
   /// 701: ZY
   /// 702: ZZ
   /// 703: AAA
   /// 704: AAB
   /// 705: AAC
   /// 18276: ZZX
   /// 18277: ZZY
   /// 18278: ZZZ
   /// 18279: AAAA
   /// 18280: AAAB
   /// 18281: AAAC
   /// </summary>
   /// <param name="colNo1Based"></param>
   /// <returns></returns>
   public static string GetExcelColumnName(int colNo1Based) {
      Assert.IsTrue(colNo1Based > 0, "GetA1Notion(): colNoStart1Based muss > 0 sein!");

      var columnName = "";

      while (colNo1Based > 0) {
         var modulo = (colNo1Based - 1)%26;
         columnName  = Convert.ToChar('A' + modulo) + columnName;
         colNo1Based = (colNo1Based - modulo)/26;
      }

      return columnName;
   }


   /// <summary>
   /// Holt die SheetId, die Nr. eines Sheets aufgrund des Namens
   /// </summary>
   /// <param name="sheetName"></param>
   public static int? GetSheetId(string sheetName) {
      var range = $"'{sheetName}'";

      var request = GoogleSheetApiCfg.GoogleService.Spreadsheets.Get(GoogleSheetMbxStatistics.SpreadsheetId);

      // var request = GoogleSheetApiCfg.GoogleService.Spreadsheets.Values.Get()
      // SpreadsheetProperties  c = new SpreadsheetProperties();

      // !M https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/get#query-parameters
      request.Ranges          = range;
      request.IncludeGridData = false;
      var response = request.Execute();

      return response.Sheets[0].Properties.SheetId;
   }


   /// <summary>
   /// Holt alle Sheets mit allen Properties, ohne Daten
   /// </summary>
   /// <param name="sheetName"></param>
   public static IList<Sheet> GetSheetProps() {
      var request = GoogleSheetApiCfg.GoogleService.Spreadsheets.Get(GoogleSheetMbxStatistics.SpreadsheetId);

      // !M https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/get#query-parameters
      request.IncludeGridData = false;
      var response = request.Execute();

      return response.Sheets;
   }


   /// <summary>
   /// Setzt im Spreadsheets Obj die Regionale Sprache, wenn sie nicht schon stimmt
   /// Status: Testet & OK
   /// </summary>
   /// <param name="spreadsheetId"></param>
   /// <param name="locale"></param>
   public static void SetSpreadSheetsLocale(string spreadsheetId, string locale = "de_CH") {
      var spreadSheets = GetSpreadSheets(spreadsheetId);

      if (!spreadSheets.Properties.Locale.Equals(locale, StringComparison.OrdinalIgnoreCase)) {
         var thisRequest = new Request() {
            UpdateSpreadsheetProperties = new UpdateSpreadsheetPropertiesRequest() {
               //‼ !9 Wichtig:
               // Field ist nicht eine Zelle, sondern definiert, welche Felder / Props der Zelle aktualisiert werden sollen!
               // !M https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#google.protobuf.FieldMask
               // !M Details: https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/cells
               // e.g. Fields: "userEnteredFormat.textFormat.bold"
               Fields       = "*"
               , Properties = new SpreadsheetProperties() { Locale = locale }
            }
         };

         var batchRequest = GetBatchRequest(false);
         batchRequest.Requests.Add(thisRequest);

         //+ Den Batch Request ausführen
         var res2 = GoogleSheetApiCfg.GoogleService.Spreadsheets.BatchUpdate(batchRequest, spreadsheetId).Execute();
      }
   }


   /// <summary>
   /// Liest das Spreadsheets Objekt,
   /// das Props wie z.B. die regionale Sprache hat 
   /// Status: Testet & OK
   /// </summary>
   /// <param name="spreadsheetId"></param>
   public static Spreadsheet GetSpreadSheets(string spreadsheetId) {
      // Note !M https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/get
      var request = GoogleSheetApiCfg.GoogleService.Spreadsheets.Get(spreadsheetId);
      request.IncludeGridData = true;
      Spreadsheet? spreadsheet = request.Execute();

      return spreadsheet;
   }


   /// <summary>
   /// Vergrössert bei Bedarf das GSheets Sheet
   /// </summary>
   /// <param name="spreadsheetId"></param>
   /// <param name="sheetName"></param>
   /// <param name="minRows"></param>
   /// <param name="minCols"></param>
   public static void Assert_GSheets_sheetRowsAndCols(
      string spreadsheetId, string sheetName, int minRows, int minCols) {
      //+ Die Anz. Zeilen & Spalten bestimmen
      var (anzRows1Based, anzCols1Based) = GetSheet_GetAnz_SheetRowsAndCols(spreadsheetId, sheetName);
      
      //+ Berechnen, wie viele Zeilen / Spalten fehlen
      var deltaRows = Math.Max(0, minRows - anzRows1Based);
      var deltaCols = Math.Max(0, minCols - anzCols1Based);

      //+ Müssen wir Zeilen / Spalten ergänzen?
      if (deltaRows > 0 || deltaCols > 0) {
         GSheet_AppendRowsCols(spreadsheetId, sheetName, deltaRows, deltaCols);         
      }
   }


   /// <summary>
   /// Liest ein GSheets Sheet
   /// </summary>
   /// <param name="spreadsheetId"></param>
   /// <param name="sheetName"></param>
   /// <returns></returns>
   public static Sheet GetSheet(string spreadsheetId, string sheetName) {
      //+ Request definieren
      var request = GoogleSheetApiCfg.GoogleService.Spreadsheets.Get(spreadsheetId);

      // Das ganze Blatt holen
      // !M https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/get#query-parameters
      request.Ranges = $"'{sheetName}'";

      // Wir wollen die Daten im Sheet
      request.IncludeGridData = true;

      // Daten abholen
      var response = request.Execute();

      return response.Sheets[0];
   }


   /// <summary>
   /// Holt die Datenblöcke eines Sheets
   /// </summary>
   /// <param name="spreadsheetId"></param>
   /// <param name="sheetName"></param>
   public static IList<GridData> GetSheet_Data(string spreadsheetId, string sheetName) {
      var s = GetSheet(spreadsheetId, sheetName);

      // Alle gefundenen Bereiche mit Daten
      return s.Data;
   }


   /// <summary>
   /// Berechnet, wie viele Zeilen und Spalten in einem Sheet max. mit Daten gefüllt sind
   ///
   /// !9 Es wird nur das erste dataGrid analysiert
   /// </summary>
   /// <param name="spreadsheetId"></param>
   /// <param name="sheetName"></param>
   /// <returns>
   /// var (maxGenützteZeile1Based, maxGenützteSpalte1Based) = GetSheet_MaxUsed_RowsAndCols(spreadsheetId, sheetName)
   /// </returns>
   public static Tuple<int, int> GetSheet_MaxUsed_RowsAndCols(string spreadsheetId, string sheetName) {
      //+ Die Daten lesen
      var gridData = GetSheet_Data(spreadsheetId, sheetName);

      //+ Wie viele Zeilen haben Zellen mit Daten != null?
      //++ Für jede Zeile zählen, wie viele Spalten != null sind 
      var zeilenMitAnzUsedCells =
         (
            from row
               in gridData[0].RowData

            // Wie viele Spalten haben einen Wert != null?
            let anzNotNull = row.Values.Count(v => v.EffectiveValue != null)
            select anzNotNull).ToList();

      //++ Vom Ende her die erste Zeile suchen, die Zellen mit Daten hat 
      int foundRow0Based = zeilenMitAnzUsedCells.Count - 1;

      for (; foundRow0Based >= 0; foundRow0Based--) {
         var z = zeilenMitAnzUsedCells[foundRow0Based];

         if (z != 0) {
            break;
         }
      }

      //++ Wie viele Zeilen sind max. belegt? 
      var maxGenützteZeile0Based = foundRow0Based;

      //+ Wie viele Spalten haben Zellen mit Daten != null?
      // Array mit einer int für jede Zeile:
      // welches ist in der Zeile die max. Spalte, die Daten != null hat? 
      var mostRightUsedCellsInZeile = new int[gridData[0].RowData.Count];

      for (var idxZeile = 0; idxZeile < gridData[0].RowData.Count; idxZeile++) {
         IList<CellData> spalten = gridData[0].RowData[idxZeile].Values;

         // Von der letzten Spalte her die erste Zelle suchen, die einen Wert != null hat
         int foundSpalte0Based = spalten.Count - 1;

         for (; foundSpalte0Based >= 0; foundSpalte0Based--) {
            var cellData = spalten[foundSpalte0Based];

            // Hat die Zelle einen Wert?
            if (cellData.EffectiveValue != null) {
               break;
            }
         }

         // Merken, bis zu welcher Spalte diese Zeile Daten beinhalten
         mostRightUsedCellsInZeile[idxZeile] = foundSpalte0Based;
      }

      //++ Wie viele Spalten sind max. belegt? 
      var maxGenützteSpalte0Based = mostRightUsedCellsInZeile.Max(x => x);

      return new Tuple<int, int>(maxGenützteZeile0Based+1, maxGenützteSpalte0Based+1);
   }


   /// <summary>
   /// Holt die Anz. Zeilen und Spalten eines GSheets Sheet
   /// </summary>
   /// <param name="spreadsheetId"></param>
   /// <param name="sheetName"></param>
   /// <returns>
   /// var (anzRows1Based, anzCols1Based) = GetSheet_RowsAndCols_Count(spreadsheetId, sheetName);
   /// </returns>
   public static Tuple<int, int> GetSheet_GetAnz_SheetRowsAndCols(string spreadsheetId, string sheetName) {
      var s = GetSheet(spreadsheetId, sheetName);

      // Anz. Zeilen und Spalten zurückgeben
      return new System.Tuple<int, int>(s.Properties.GridProperties.RowCount ?? 0, s.Properties.GridProperties.ColumnCount ?? 0);
   }


   /// <summary>
   /// Ergänzt *eine* Zeile
   /// </summary>
   /// <param name="spreadsheetId"></param>
   /// <param name="sheetName"></param>
   public static void GSheet_AppendDataRow(string spreadsheetId, string sheetName) {
      // Die Blatt-ID
      var sheetId = GetSheetId(sheetName) ?? -1;
      Assert.IsTrue(sheetId >= 0, $"Im Excel kein Sheet mit dem Namen '{sheetName}' gefunden");

      //+ Der Batch-Update Request
      var batchUpdateRequest = new BatchUpdateSpreadsheetRequest() {
         IncludeSpreadsheetInResponse = false
         , Requests                   = new List<Request>()
      };

      //+ Der Request mit der eigentlichen Aufgabe
      // !M https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/request#appendcellsrequest
      // !M https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values/append
      var appendCellsRequest = new AppendCellsRequest() {
         SheetId = sheetId
         , Rows = new[] {
            new RowData { Values = new List<CellData>() { new() { UserEnteredValue = new ExtendedValue() { StringValue = "" } } } }
         }
         , Fields = "*"
      };

      //+ Das Request-Obj erzeugen
      var thisRequest = new Request() { AppendCells = appendCellsRequest };

      //+ Das Batch Update Request ergänzen
      batchUpdateRequest.Requests.Add(thisRequest);

      //+ Den Batch Request ausführen
      var res2 = GoogleSheetApiCfg.GoogleService.Spreadsheets.BatchUpdate(batchUpdateRequest, spreadsheetId).Execute();
   }


   /// <summary>
   /// Ergänzt ein GSheets Sheet um leere Zeilen / Spalten
   /// </summary>
   /// <param name="spreadsheetId"></param>
   /// <param name="sheetName"></param>
   /// <param name="appendRows"></param>
   /// <param name="appendCols"></param>
   public static void GSheet_AppendRowsCols(
      string   spreadsheetId
      , string sheetName
      , int?   appendRows
      , int?   appendCols) {
      //+ Params prüfen
      if ((appendRows == null
          || appendRows is <= 0)
          && (appendCols == null
          || appendCols is <= 0)) {
         return;
      }

      // Die Blatt-ID
      var sheetId = GetSheetId(sheetName) ?? -1;
      Assert.IsTrue(sheetId >= 0, $"Im Excel kein Sheet mit dem Namen '{sheetName}' gefunden");

      //+ Der Batch-Update Request
      var batchUpdateRequest = new BatchUpdateSpreadsheetRequest() {
         IncludeSpreadsheetInResponse = false
         , Requests                   = new List<Request>()
      };

      //+ Der Request mit der eigentlichen Aufgabe
      // !M https://developers.google.com/sheets/api/samples/rowcolumn#adjust_column_width_or_row_height
      //++ Zeilen zufügen?
      if (appendRows is > 0) {
         var appendDimensionRequest = new AppendDimensionRequest() {
            SheetId     = sheetId
            , Dimension = "ROWS"
            , Length    = appendRows
         };

         //+ Das Request-Obj erzeugen
         var thisRequest = new Request() { AppendDimension = appendDimensionRequest };

         //+ Das Batch Update Request ergänzen
         batchUpdateRequest.Requests.Add(thisRequest);
      }

      //++ Spalten zufügen?
      if (appendCols is > 0) {
         var appendDimensionRequest = new AppendDimensionRequest() {
            SheetId     = sheetId
            , Dimension = "COLUMNS"
            , Length    = appendCols
         };

         //+ Das Request-Obj erzeugen
         var thisRequest = new Request() { AppendDimension = appendDimensionRequest };

         //+ Das Batch Update Request ergänzen
         batchUpdateRequest.Requests.Add(thisRequest);
      }

      //+ Den Batch Request ausführen
      var res2 = GoogleSheetApiCfg.GoogleService.Spreadsheets.BatchUpdate(batchUpdateRequest, spreadsheetId).Execute();
   }


   /// <summary>
   /// Löscht alle Daten eines GSheets Sheet
   ///
   /// !9 Vorsicht, GSheets erlaubt nicht, dass man alle Zellen löscht,
   /// erzeugt die Exception: "You can't delete all the rows on the sheet"
   /// https://stackoverflow.com/questions/68310160/you-cant-delete-all-the-rows-on-the-sheet-google-sheets-script
   /// 
   /// Deshalb ergänzt diese Funktion bei Bedarf eine Zeile und löscht alles oberhalb 
   ///
   /// Variante: Löscht die Daten und/oder Formatierungenen aller Zellen
   ///   Sheet_Delete_CellProperties()
   /// </summary>
   /// <param name="spreadsheetId"></param>
   /// <param name="sheetName"></param>
   /// <returns> </returns>
   public static void DeleteSheetData(string spreadsheetId, string sheetName) {
      
      //+ Analyse
      // Wie viele Zeilen / Spalten hat das Sheet?
      var (anzRows1Based, anzCols1Based) = GetSheet_GetAnz_SheetRowsAndCols(spreadsheetId, sheetName);

      // Wie viele Zeilen / Spalten sind im Sheet mit Daten gefüllt?
      var (maxGenützteZeile1Based, maxGenützteSpalte1Based) = GetSheet_MaxUsed_RowsAndCols
         (spreadsheetId, sheetName);

      //+ Sind alle Zeilen mit Daten gefüllt?
      if (maxGenützteZeile1Based == anzRows1Based) {
         // Eine Datenzeile ergänzen, damit die Löschfunktion nicht in einer Exception endet
         GSheet_AppendDataRow(spreadsheetId, sheetName);
      }

      // Die Blatt-ID
      var sheetId = GetSheetId(sheetName) ?? -1;
      Assert.IsTrue(sheetId >= 0, $"Im Excel kein Sheet mit dem Namen '{sheetName}' gefunden");

      //+ Der Batch-Update Request
      var batchUpdateRequest = new BatchUpdateSpreadsheetRequest() {
         IncludeSpreadsheetInResponse = false
         , Requests                   = new List<Request>()
      };

      //+ Der Request mit der eigentlichen Aufgabe
      // !M https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/request#deletedimensionrequest
      var deleteDimensionRequest = new DeleteDimensionRequest() {
         // !M https://developers.google.com/sheets/api/reference/rest/v4/DimensionRange
         Range = new DimensionRange() {
            // ALle Zellen löschen
            SheetId = Convert.ToInt32(sheetId)
            , Dimension = "ROWS"
            // All indexes are zero-based
            // Das Ende (ausschließlich) der Spanne, oder nicht gesetzt, wenn sie nicht begrenzt ist
            , EndIndex = maxGenützteZeile1Based
         }
      };

      //+ Das Request-Obj erzeugen
      var thisRequest = new Request() { DeleteDimension = deleteDimensionRequest };

      //+ Das Batch Update Request ergänzen
      batchUpdateRequest.Requests.Add(thisRequest);

      //+ Den Batch Request ausführen
      var res2 = GoogleSheetApiCfg.GoogleService.Spreadsheets.BatchUpdate(batchUpdateRequest, spreadsheetId).Execute();
   }


   /// <summary>
   /// Löscht alle Formatierungen und/oder Daten eines GSheets Sheet
   /// </summary>
   /// <param name="spreadsheetId"></param>
   /// <param name="sheetName"></param>
   /// <param name="deletePropsFlags">
   /// e.g. GSheetsFieldMask.GSheetsFlags.DatenLöschen | GSheetsFieldMask.GSheetsFlags.FormatierungLöschen
   /// </param>
   /// <returns></returns>
   public static void Sheet_Delete_CellProperties(string spreadsheetId, string sheetName, GSheetsFieldMask.GSheetsFlags deletePropsFlags) {
      // Die Blatt-ID
      var sheetId = GetSheetId(sheetName) ?? -1;
      Assert.IsTrue(sheetId >= 0, $"Im Excel kein Sheet mit dem Namen '{sheetName}' gefunden");

      //+ Der Batch-Update Request
      var batchUpdateRequest = new BatchUpdateSpreadsheetRequest() {
         IncludeSpreadsheetInResponse = false
         , Requests                   = new List<Request>()
      };

      //+ Der Request mit der eigentlichen Aufgabe
      // !M https://stackoverflow.com/questions/45801313/remove-only-formatting-on-a-cell-range-selection-with-google-spreadsheet-api
      var updateCellsRequest = new UpdateCellsRequest() {
         Range = new GridRange() {
            // https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/other#GridRange
            SheetId = sheetId

            // All Indexs are 0-based!
         }

         //‼ !9 Wichtig:
         // Field ist nicht eine Zelle, sondern definiert, welche Felder / Props der Zelle aktualisiert werden sollen!
         // !M https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#google.protobuf.FieldMask
         // !M Details: https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/cells
         // e.g. Fields: "userEnteredFormat.textFormat.bold"
         , Fields = GSheetsFieldMask.getGSheetsFlags(deletePropsFlags)
      };

      //+ Das Request-Obj erzeugen
      var thisRequest = new Request() { UpdateCells = updateCellsRequest };

      //+ Das Batch Update Request ergänzen
      batchUpdateRequest.Requests.Add(thisRequest);

      //+ Den Batch Request ausführen
      var res2 = GoogleSheetApiCfg.GoogleService.Spreadsheets.BatchUpdate(batchUpdateRequest, spreadsheetId).Execute();
   }


   /// <summary>
   /// Initiiert AutoResize für die Spalten
   /// </summary>
   /// <param name="spreadsheetId"></param>
   /// <param name="sheetName"></param>
   public static void AutoResizeDimensions(string spreadsheetId, string sheetName) {
      // Die Blatt-ID
      var sheetId = GetSheetId(sheetName) ?? -1;
      Assert.IsTrue(sheetId >= 0, $"Im Excel kein Sheet mit dem Namen '{sheetName}' gefunden");

      //+ Der Batch-Update Request
      var batchUpdateRequest = new BatchUpdateSpreadsheetRequest();

      // Wir brauchen im Resultat keine Daten
      batchUpdateRequest.IncludeSpreadsheetInResponse = false;

      // Die Requests-Liste vorbereiten
      batchUpdateRequest.Requests = new List<Request>();

      //+ Der AutoResize Request
      var autoResizeDimensions = new AutoResizeDimensionsRequest() {
         Dimensions = new DimensionRange() {
            SheetId     = Convert.ToInt32(sheetId)
            , Dimension = "COLUMNS"

            // the start index is inclusive and the end index is exclusive
            , StartIndex = 0
            , EndIndex   = 21
         }
      };

      //+ Das Request-Obj erzeugen
      var thisRequest = new Request();

      // und den Cell-Request zuordnen
      thisRequest.AutoResizeDimensions = autoResizeDimensions;

      //+ Das Batch Update Request ergänzen
      batchUpdateRequest.Requests.Add(thisRequest);

      //+ Den Batch Request ausführen
      var res2 = GoogleSheetApiCfg.GoogleService.Spreadsheets.BatchUpdate(batchUpdateRequest, spreadsheetId).Execute();
   }


   public static void SetColumnFormat(string spreadsheetId, string sheetName, List<TomToolsGSheets.GSheetsColFormat> gSheetsColFormats) {
      // Die Blatt-ID
      var sheetId = GetSheetId(sheetName) ?? -1;
      Assert.IsTrue(sheetId >= 0, $"Im Excel kein Sheet mit dem Namen '{sheetName}' gefunden");

      BatchUpdateSpreadsheetRequest bussr = new BatchUpdateSpreadsheetRequest();
      bussr.Requests = new List<Request>();

      foreach (var gSheetsColFormat in gSheetsColFormats) {
         var updateCellsRequest = new Request() {
            RepeatCell = new RepeatCellRequest() {
               Range = new GridRange() {
                  // https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/other#GridRange
                  // All Indexs are 0-based!
                  SheetId            = sheetId
                  , StartColumnIndex = gSheetsColFormat.ColumnID1Based - 1
                  , EndColumnIndex   = gSheetsColFormat.ColumnID1Based
                  , StartRowIndex    = 1
               }
               , Cell = new CellData() {
                  // https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/cells#CellFormat
                  UserEnteredFormat = new CellFormat() {
                     // https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/cells#NumberFormat
                     NumberFormat = new NumberFormat() {
                        Type      = gSheetsColFormat.NumberFormatType
                        , Pattern = gSheetsColFormat.NumberFormatPattern
                     }
                  }
               }

               //‼ !9 Wichtig:
               // Field ist nicht eine Zelle, sondern definiert, welche Felder / Props der Zelle aktualisiert werden sollen!
               // !M https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#google.protobuf.FieldMask
               // !M Details: https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/cells
               // e.g. Fields: "userEnteredFormat.textFormat.bold"
               , Fields = "userEnteredFormat.numberFormat"
            }
         };
         bussr.Requests.Add(updateCellsRequest);
      }

      var res2 = GoogleSheetApiCfg.GoogleService.Spreadsheets.BatchUpdate(bussr, spreadsheetId).Execute();
   }


   /// <summary>
   /// Erzeugt einen BatchRequest
   /// Status: Testet & OK
   /// </summary>
   /// <param name="includeSpreadsheetInResponse"></param>
   /// <returns></returns>
   public static BatchUpdateSpreadsheetRequest GetBatchRequest(bool includeSpreadsheetInResponse) {
      //+ Der Batch-Update Request
      var batchUpdateRequest = new BatchUpdateSpreadsheetRequest();

      // Wir brauchen im Resultat keine Daten
      batchUpdateRequest.IncludeSpreadsheetInResponse = includeSpreadsheetInResponse;

      // Die Requests-Liste vorbereiten
      batchUpdateRequest.Requests = new List<Request>();

      return batchUpdateRequest;
   }


   /// <summary>
   /// </summary>
   public static void Save(string spreadsheetId, string sheetName, GridData gSheetData) {
      // Die Blatt-ID
      var sheetId = GetSheetId(sheetName) ?? -1;
      Assert.IsTrue(sheetId >= 0, $"Im Excel kein Sheet mit dem Namen '{sheetName}' gefunden");

      //+ Der Batch-Update Request
      var batchUpdateRequest = GetBatchRequest(false);

      //+ Der Cell-Update Request
      // !M https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/request#updatecellsrequest
      var updateCellsRequest = new UpdateCellsRequest();

      //‼ !9 Wichtig:
      // Field ist nicht eine Zelle, sondern definiert, welche Felder / Props der Zelle aktualisiert werden sollen!
      // !M https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#google.protobuf.FieldMask
      // !M Details: https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/cells
      // e.g. Fields: "userEnteredFormat.textFormat.bold"
      updateCellsRequest.Fields = "*";

      // Die Daten, die geschrieben werden sollen
      // updateCellsRequest.Rows = gridData[0].RowData;
      updateCellsRequest.Rows = gSheetData.RowData;

      // Start-Position
      //	 Bereich wie e.g. erzeugen eine Exception:
      //	 updateCellsRequest.Start = new GridCoordinate { SheetId = sheetId, RowIndex = 10, ColumnIndex = 10 };
      updateCellsRequest.Start = new GridCoordinate {
         SheetId       = sheetId
         , RowIndex    = 0
         , ColumnIndex = 0
      };

      //+ Das Request-Obj erzeugen
      var thisRequest = new Request();

      // und den Cell-Request zuordnen
      thisRequest.UpdateCells = updateCellsRequest;

      //+ Das Batch Update Request ergänzen
      batchUpdateRequest.Requests.Add(thisRequest);

      //+ Den Batch Request ausführen
      var res2 = GoogleSheetApiCfg.GoogleService.Spreadsheets.BatchUpdate(batchUpdateRequest, spreadsheetId).Execute();
   }


   /// <summary>
   /// Funktionen, um das API zu testen
   /// </summary>
   public static void Test_DoIt() {
      Test_ReadSheet(GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic.BlattName);
      AddRow(GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic.BlattName);
      Test_ReadSheet(GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic.BlattName);
      UpdateCell(GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic.BlattName);
      Test_ReadSheet(GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic.BlattName);
   }

   #endregion


   #region Internal Methods

   /// <summary>
   /// Ergänze einen Range um eine Zeile
   /// </summary>
   private static void AddRow(string sheetName) {
      // Der Bereich, der gelesen und danach Infos angefügt werden soll
      var range = $"'{sheetName}'!A:E";

      // Die neuen Werte
      var valueRange = new ValueRange();

      // Jahr | KW | AntwortenListe | Neue Fragen | Anz offene Fragen
      var oblist = new List<object> {
         2022
         , 5
         , 3
         , 6
         , 9
      };

      valueRange.Values = new List<IList<object>> { oblist };

      // Zum angegebenen Bereich die Daten ergänzen
      var appendRequest = GoogleSheetApiCfg.GoogleService.Spreadsheets.Values.Append
         (
          valueRange
          , GoogleSheetMbxStatistics.SpreadsheetId
          , range);

      // Die Art der Datenübergabe mitteilen
      appendRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;

      // Do it
      var appendReponse = appendRequest.Execute();
   }


   /// <summary>
   /// Akzualisiert einen Bereich im Sheet
   /// </summary>
   private static void AddUpdateRange(string sheetName) {
      // Der Bereich, der gelesen und danach Infos angefügt werden soll
      var range   = $"'{sheetName}'!A:E";
      var SheetId = GetSheetId(sheetName) ?? -1;

      if (SheetId < 0) {
         throw new RuntimeException("AddUpdateRange()");
      }

      // Die neuen Werte
      var valueRange = new ValueRange();

      // Jahr | KW | AntwortenListe | Neue Fragen | Anz offene Fragen
      var oblist = new List<object> {
         2022
         , 1
         , 1
         , 11
         , 9
         , 2022
         , 2
         , 3
         , 13
         , 11
         , 2022
         , 3
         , 5
         , 15
         , 7
         , 2022
         , 4
         , 7
         , 17
         , 8
         , 2022
         , 5
         , 9
         , 19
         , 2
      };

      valueRange.Values = new List<IList<object>> { oblist };

      var batchUpdateRequest = new BatchUpdateSpreadsheetRequest();
      batchUpdateRequest.IncludeSpreadsheetInResponse = true;

      // !M https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/request#updatecellsrequest
      var updateCellsRequest = new UpdateCellsRequest();

      //‼ !9 Wichtig:
      // Field ist nicht eine Zelle, sondern definiert, welche Felder / Props der Zelle aktualisiert werden sollen!
      // !M https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#google.protobuf.FieldMask
      // !M Details: https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/cells
      // e.g. Fields: "userEnteredFormat.textFormat.bold"
      updateCellsRequest.Fields = "*";

      updateCellsRequest.Start = new GridCoordinate {
         SheetId       = SheetId
         , RowIndex    = 0
         , ColumnIndex = 0
      };
      var rq = new Request();
      rq.UpdateCells = updateCellsRequest;

      batchUpdateRequest.Requests.Add(rq);

      // SpreadsheetsResource.ValuesResource.BatchUpdateRequest bur =
      //	new SpreadsheetsResource.ValuesResource.BatchUpdateRequest(_service, SpreadsheetId);

      // UpdateCellsRequest 
      // Zum angegebenen Bereich die Daten ergänzen
      var appendRequest = GoogleSheetApiCfg.GoogleService.Spreadsheets.Values.Append
         (
          valueRange
          , GoogleSheetMbxStatistics.SpreadsheetId
          , range);

      // 
      appendRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;

      // Do it
      var appendReponse = appendRequest.Execute();
   }


   /// <summary>
   /// Liest als Test die Daten eines Google Sheets und gibt sie in der Console aus
   /// </summary>
   private static void Test_ReadSheet(string sheetName) {
      // Specifying Column Range for reading...
      var range = $"'{sheetName}'!A:E";

      var request = GoogleSheetApiCfg.GoogleService.Spreadsheets.Values.Get(GoogleSheetMbxStatistics.SpreadsheetId, range);

      // Ecexuting Read Operation...
      var response = request.Execute();

      // Getting all records from Column A to E...
      IList<IList<object>> values = response.Values;

      // Zählt in den gelesenen Daten
      // die Bereiche mit unterschiedlich ausgefüllter Spalternanzahl
      // z.B.
      //	 <Titel>
      //	 <Leerzeile>
      //	 <Leerzeile>
      //  <HeaderSpalte> <HeaderSpalte> <HeaderSpalte> <HeaderSpalte> <HeaderSpalte> 
      //  <DatenSpalte>  <DatenSpalte>  <DatenSpalte>  <DatenSpalte>  <DatenSpalte>  
      //  <DatenSpalte>  <DatenSpalte>  <DatenSpalte>  <DatenSpalte>  <DatenSpalte>  
      //  <DatenSpalte>  <DatenSpalte>  <DatenSpalte>  <DatenSpalte>  <DatenSpalte>  
      var excelDatenspaltenGruppen = values.GroupBy(row => row.Count);

      // Gruppiert die Datenbereiche aufgrund der ausgefüllten Spalten
      // sortiert absteigend nach der Anz. ausgefüllter Spalten
      // und liefert die Anz. Spaltenzurück, die am meisten ausgefükkt wurden
      var anzDatenspalten = excelDatenspaltenGruppen.OrderByDescending(r => r.Count()).First().Key;

      var datenSpaltenGefunden = false;

      const int colWidht = 20;

      if (values != null
          && values.Count > 0) {
         foreach (var row in values) {
            if (!datenSpaltenGefunden) {
               // Die Anz. Datenspalten gefunden?
               if (row.Count == anzDatenspalten) {
                  datenSpaltenGefunden = true;
               }
            }

            if (datenSpaltenGefunden) {
               Debug.WriteLine($"{row[0],colWidht} | {row[1],colWidht} | {row[2],colWidht} | {row[3],colWidht} | {row[4],colWidht}");
            }
         }
      }
      else {
         Console.WriteLine("No data found.");
      }
   }


   private static void UpdateCell(string sheetName) {
      // Setting Cell Name...
      var range      = $"'{sheetName}'!C5";
      var valueRange = new ValueRange();

      // Setting Cell Value...
      var oblist = new List<object> { "32" };

      valueRange.Values = new List<IList<object>> { oblist };

      // Performing Update Operation...
      var updateRequest = GoogleSheetApiCfg.GoogleService.Spreadsheets.Values.Update
         (
          valueRange
          , GoogleSheetMbxStatistics.SpreadsheetId
          , range);
      updateRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.USERENTERED;
      var appendReponse = updateRequest.Execute();
   }

   #endregion

}
