using System;
using System.Collections.Generic;
using System.Text;
using jig.Tools;


namespace ImapMailManager.GoogleAPI.TomTools;

public class GSheetsFieldMask {

   /// <summary>
   ///
   /// Die GSheets FieldMasks
   /// !M https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#google.protobuf.FieldMask 
   /// </summary>
   [Flags]
   public enum GSheetsFlags {

      None                  = 1, All = 2, UserEnteredFormat = 4, UserEnteredValue = 8, NumberFormat = 16
      , FormatierungLöschen = 32, DatenLöschen = 64

   }


   /// <summary>
   /// Das Mapping zwischen GSheetsFlags
   /// und GSheets FieldMasks
   /// !M https://developers.google.com/protocol-buffers/docs/reference/google.protobuf#google.protobuf.FieldMask 
   /// </summary>
   public static readonly Dictionary<GSheetsFlags, string> GSheetsFlagsCfg = new Dictionary<GSheetsFlags, string>() {
      { GSheetsFlags.None, "" }
      , { GSheetsFlags.All, "*" }
      , { GSheetsFlags.UserEnteredFormat, "UserEnteredFormat" }
      , { GSheetsFlags.FormatierungLöschen, "UserEnteredFormat" }
      , { GSheetsFlags.UserEnteredValue, "UserEnteredValue" }
      , { GSheetsFlags.DatenLöschen, "UserEnteredValue" }
   };


   /// <summary>
   /// Erzeugt aus den GSheetsFlags
   /// den String fürs GSeets REST API
   /// </summary>
   /// <param name="gSheetsFlags"></param>
   /// <returns></returns>
   public static string getGSheetsFlags(GSheetsFlags gSheetsFlags) {
      //+ Wurde All gesetzt?
      if (gSheetsFlags.IsSet(GSheetsFlags.All)) {
         return GSheetsFlagsCfg[GSheetsFlags.All];
      }

      //+ Wurde None gesetzt?
      if (gSheetsFlags.IsSet(GSheetsFlags.None)) {
         return GSheetsFlagsCfg[GSheetsFlags.None];
      }

      //+ Sonst: Die FLags zusammensetzen
      StringBuilder sb = new StringBuilder();

      foreach (Enum flag in Enum.GetValues(typeof(GSheetsFlags))) {
         if (gSheetsFlags.HasFlag(flag)) {
            // Elemente nicht mit . trennen,
            // weil wir nicht die Hierarchie angeben, sondern die verschiedenen Elemente
            if (sb.Length > 0) {
               sb.Append(',');
            }

            sb.Append($"{GSheetsFlagsCfg[(GSheetsFlags)flag]}");
         }
      }

      return sb.ToString();
   }

}
