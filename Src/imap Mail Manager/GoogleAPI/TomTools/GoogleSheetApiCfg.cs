using System.IO;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;


namespace ImapMailManager.GoogleAPI.TomTools;

/// <summary>
/// Die Config für GSheets 
/// </summary>
internal class GoogleSheetApiCfg {

   // Der Google API *Projektname*
   // https://console.cloud.google.com/iam-admin/serviceaccounts/details/113580017850624329823;edit=true/keys?project=rluk-372400
   public static readonly string ApplicationName           = "RLUK-Srvc";
   public static readonly string ApplicationServiceAccount = "rluk-srvc@rluk-372400.iam.gserviceaccount.com";

   /// <summary>
   /// Wird fürs GSheets Login genützt, um den Arbeits-Scope zu definieren
   /// </summary>
   private static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };

   /// <summary>
   /// Singleton
   /// Der GSheets Service als Interface zum REST API
   /// Wird beim ersten Zugriff automatisch initialisiert
   /// </summary>
   private static SheetsService? _googleService = null;
   private static readonly object padlock = new object();

   /// <summary>
   /// Singleton Logik, Thread-Safe!
   /// !Q https://csharpindepth.com/Articles/Singleton#conclusion
   /// </summary>
   public static SheetsService GoogleService {
      get {
         lock (padlock) {
            if (_googleService == null) {
               var credential = GoogleSheetApiCfg.GetGoogleCredential();

               // Creating Google Sheets API service...
               _googleService = new SheetsService(
                  new BaseClientService.Initializer {
                     HttpClientInitializer = credential
                     , ApplicationName     = GoogleSheetApiCfg.ApplicationName
                  });
            }

            return _googleService;
         }
      }
   }

   /// <summary>
   /// Liest die GSheets Credentials Datei
   /// und gibtGoogleCredential zurück
   /// </summary>
   /// <returns></returns>
   public static GoogleCredential GetGoogleCredential() => GoogleCredential.FromJson(GetGoogleCredentialJson())
                                                                           .CreateScoped(GoogleSheetApiCfg.Scopes);

   /// <summary>
   /// Liest die GSheets Credentials JSON Datei und gibt den json string zurück
   /// </summary>
   /// <returns></returns>
   public static string? GetGoogleCredentialJson() {
      //Reading Credentials File...
      using var fileStream = new FileStream(@"GoogleAPI\GoogleSheets_app_client_secret.json", FileMode.Open, FileAccess.Read);

      using var streamReader = new StreamReader(fileStream);

      return streamReader.ReadToEnd();
   }

}
