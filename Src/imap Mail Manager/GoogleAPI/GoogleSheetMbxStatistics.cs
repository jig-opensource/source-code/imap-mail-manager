﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using GoogleSheetsWrapper.Utils;
using ImapMailManager.GoogleAPI.TomTools;
using ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper.StatsMbxAntwortenVsNeueFragen;
using ImapMailManager.Mail;
using ImapMailManager.Mail.Stats;
using jig.Tools;
using jig.Tools.Exception;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ImapMailManager.GoogleAPI {

   /// <summary>
   /// Die Mailbox-Statistik im Google Sheets
   /// </summary>
   internal class GoogleSheetMbxStatistics {

      #region Static Fields

      /// <summary>
      /// Google SheetName Doc ID aus der URI
      /// </summary>
      public static readonly string SpreadsheetId = "1hjPjWhNUxUr7qmiJcCZeyR2fH87X4jiZkk5DUpcMQGY";

      #endregion


      #region Public Methods and Operators

      /// <summary>
      /// Das GSheets Sheet um die Zeilen in neueDaten ergänzen
      /// </summary>
      /// <param name="neueDaten"></param>
      public static void AppendMailboxStatistics(IEnumerable<MailboxStatisticsProKW> neueDaten) {
         //+ Daten lesen, um sie analysieren zu können
         var gridData = GoogleSheetApi.GetSheet_Data(GoogleSheetMbxStatistics
                                                    .SpreadsheetId, GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic.BlattName)[0];

         //++ In Excel den Header suchen
         var (headerRowId, headerCols) = GoogleSheetApi.FindTableHeaderData(gridData);

         //++ Asserts
         Assert.IsNotNull(headerRowId, "Im Excel keine Header-Zeile gefunden");
         Assert.IsTrue(headerCols.Count > 2, "Im Excel keine Header-Zeile gefunden");

         var colNoStart = headerCols.Values.Min();
         var ColNoEnd   = headerCols.Values.Max();

         // Berechnen, in welchem Bereich unsere Daten sind
         // eg. $"'{SheetName}'!A:E"
         var meineDatenSpalten = GoogleSheetApi.GetA1Notion(GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic.BlattName, colNoStart + 1, ColNoEnd + 1, null, null);

         //++ Die Daten in neueDaten in die richtige Spaltenreihenfolge bringen

         // Die Props / Spalten entsprechend der SpaltenNr sortieren
         var headerColsSortiert = headerCols.OrderBy(x => x.Value);

         // note Wichtig: Die Daten werden als ValueRange Objekt übergeben,
         // - das eine eindimensionale (nicht zweidimensionale!) Liste entgegen nimmt
         // - Der Zeilenumbruch geschieht mir der Angabe des Spalten-Bereichs, bzw. der Anz. Spalten
         //
         // !M9 Reading & Writing Cell Values
         // https://developers.google.com/sheets/api/guides/values#writing
         // !M9 valuerange
         // https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets.values#resource-valuerange

         // Zeilen und Spalten
         var gSheetData = new List<IList<object>>();

         //+++ Alle Zeilen durchlaufen
         // Die statistischen Daten für E-Mails im Entwurf ausblenden
         // Sortieren nach Datum
         foreach (var statisticsProKw in neueDaten.Where(x => !x.IsStatsForMailsImEntwurf).OrderBy(x => x.EndDatumDerKW)) {
            var newRow = new List<object>();

            // Alle Spalten in der richtigen Rhf. durchlaufen
            foreach (var keyValuePair in headerColsSortiert) {
               var propName = keyValuePair.Key;

               // Via Reflection den Wert des Props auslesen
               if (statisticsProKw.ØObjHasProp(propName)) {
                  var propVal = statisticsProKw.ØObjGetPropVal(propName);

                  // Das Datum so formatieren, wie der User es eingeben würde
                  if (propVal is DateTime) { newRow.Add(((DateTime)propVal).ToString("dd.MM.yyyy")); }
                  else { newRow.Add(propVal); }
               }
               else { throw new RuntimeException($"Das Obj hat kei Property {propName}"); }
            }

            gSheetData.Add(newRow);
         }

         // Für GSheet das ValueRange Obj
         var valueRange = new ValueRange();

         // Unsere Daten übergeben
         // valueRange.Values = new List<IList<object>> { gSheetData };
         // valueRange.Values = new List<IList<object>> { gSheetData };
         valueRange.Values = gSheetData;

         // valueRange.Range  = meineDatenSpalten;

         // In GSheet unsere Daten ergänzen
         var appendRequest =
            GoogleSheetApiCfg.GoogleService.Spreadsheets.Values.Append(valueRange, SpreadsheetId, meineDatenSpalten);

         // Die Art der Datenübergabe mitteilen
         // !M https://developers.google.com/sheets/api/guides/values#writing
         // -> The input is parsed exactly as if it were entered into the Google Sheets UI,
         // so "Mar 1 2016" becomes a date, and "=1+2" becomes a formula.
         // Formats may also be inferred, so "$100.15" becomes a number with currency formatting.
         appendRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;

         // Do it
         var res = appendRequest.Execute();
      }


      /// <summary>
      /// Aktualisiert im Google Sheet die *bestehenden Daten* mit den Mailbox-Statistik
      /// rückwirkendeUpdatesStartDatum:
      /// Bis zu welchem Datum (rückwirkend) aktualisieren wir bereits vorhandene Daten auf GSheets?
      /// </summary>
      public static void UpdateMailboxStatistics(List<MailboxStatisticsProKW> mbxStats,
                                                 DateTime                                                   rückwirkendeUpdatesStartDatum) {
         //+ Daten lesen
         var gridData = GoogleSheetApi.GetSheet_Data(GoogleSheetMbxStatistics
                                                    .SpreadsheetId, GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic.BlattName)[0];

         //+ Daten patchen
         var patchedGSheetData = PatchExistingData(gridData, mbxStats, rückwirkendeUpdatesStartDatum);

         //+ Und die Daten zu GSheet übertragen
         GoogleSheetApi.Save(SpreadsheetId, GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic.BlattName, patchedGSheetData);
      }

      #endregion


      #region Internal Methods

      /// <summary>
      /// Patcht *bestehende* Daten im Excel-Sheet, bzw. im gridData
      /// </summary>
      /// <param name="gridData"></param>
      /// <param name="mbxStatsProKw"></param>
      /// <param name="rückwirkendeUpdatesStartDatum">
      /// Bis zu welchem Datum (rückwirkend) aktualisieren wir bereits vorhandene Daten auf GSheets?
      /// </param>
      private static GridData PatchExistingData(GridData                                                   gridData,
                                                List<MailboxStatisticsProKW> mbxStatsProKw,
                                                DateTime                                                   rückwirkendeUpdatesStartDatum) {
         //+ Via Reflection die Props der Statistik-Daten bestimmen
         // und mit den Excel-Spalten übereinbringen

         // Mapping: Object - zu Excel Spalte
         // Object.PropertyName -> Excel.Zelle.Notiz: ObjID:<PropertyName>

         // Es werden nur Daten geschrieben, die in Excel auch vorhanden sind

         //++ In Excel den Header suchen
         var (headerRowId, headerCols) = GoogleSheetApi.FindTableHeaderData(gridData);

         //++ Asserts
         Assert.IsNotNull(headerRowId, "Im Excel keine Header-Zeile gefunden");
         Assert.IsTrue(headerCols.Count > 2, "Im Excel keine Header-Zeile gefunden");

         //++ Die PropNames des DatenObj
         // Note > Die Obj Prop-Names nütze ich noch nicht,
         //      Allenfalls später nützen, um dynamische auf Änderungen in den Google Sheets Spalten reagieren zu können
         var firstStatsRec = mbxStatsProKw[0];
         var objPropNames  = firstStatsRec.GetType().GetProperties().Select(x => x.Name).ToList();

         //++ Die Spalten-IDs bestimmen
         // Note > Könnte dynamisch anhand der Propnamen im DatenObj berechnet werden
         var colIdJahr                 = headerCols["Jahr"];
         var colIdKw                   = headerCols["KW"];
         var colIdDatum                = headerCols["Datum"];
         var colIdAnzAntwortenInKW     = headerCols["AnzAntwortenInKW"];
         var colIdAnzNeueFragenInKW    = headerCols["AnzNeueFragenInKW"];
         var colIdAnzOffeneFragenBisKW = headerCols["AnzOffeneFragenBisKW"];

         // Die max. Anzal Spalten
         var maxColNo = headerCols.Values.MaxBy(x => x);

         //+ Alle Zeilen nach dem Header verarbeiten
         for (var index = (headerRowId ?? 0) + 1; index < gridData.RowData.Count; index++) {
            var row = gridData.RowData[index];

            // Hat mind. 1 Spalte Daten?
            var anyColHasData = row.Values.Any(x => x.UserEnteredValue != null);

            if (!anyColHasData) { continue; }

            // Auf GSheet nur Einträge neueren Datums aktualisieren
            var rowDate = DateTimeUtils.ConvertFromSerialNumber(row.Values[colIdDatum].UserEnteredValue?.NumberValue ?? 0);

            // Ist das Datum zu alt?
            if (rowDate < rückwirkendeUpdatesStartDatum) { continue; }

            //+++ Wenn die Zeile zu wenig Spalten hat, ergänzen
            for (var i = row.Values.Count; i <= maxColNo; i++) { row.Values.Add(new CellData()); }

            //+++ Wenn eine Spalte kein ExtendedValue hat, ergänzen
            for (var i = 0; i < row.Values.Count; i++) {
               if (row.Values[i].UserEnteredValue == null) { row.Values[i].UserEnteredValue = new ExtendedValue(); }
            }

            //+++ Haben wir aktuelle statistische Daten für diese KW?
            var thisRowYear = (int)(row.Values[colIdJahr].UserEnteredValue?.NumberValue ?? -1);
            var thisRowKW   = (int)(row.Values[colIdKw].UserEnteredValue?.NumberValue ?? -1);

            // Haben wir einen Record für diese KW in den statistichen Daten?
            var thisKWStatRec = mbxStatsProKw
               .SingleOrDefault(
                                x => x.EndDatumDerKW.Year == thisRowYear
                                     && ISOWeek.GetWeekOfYear(x.EndDatumDerKW) == thisRowKW);

            //++ Wenn wir statistische Daten haben, dann das Excel aktualisieren
            if (thisKWStatRec != null) {
               //+++ Den Export vermerken
               thisKWStatRec.IsExportedToGoogleSheets = true;

               //+++ Die statistischen Daten im Google Sheet aktualsieren
               row.Values[colIdDatum].UserEnteredValue.NumberValue =
                  DateTimeUtils.ConvertToSerialNumber(thisKWStatRec.EndDatumDerKW);
               row.Values[colIdAnzAntwortenInKW].UserEnteredValue.NumberValue     = thisKWStatRec.AnzAntwortenInKW;
               row.Values[colIdAnzNeueFragenInKW].UserEnteredValue.NumberValue    = thisKWStatRec.AnzNeueFragenInKW;
               row.Values[colIdAnzOffeneFragenBisKW].UserEnteredValue.NumberValue = thisKWStatRec.AnzOffeneFragenBisKW;
            }
         }

         // Die aktualiserten Daten zurückgeben
         return gridData;
      }

      #endregion

   }

}
