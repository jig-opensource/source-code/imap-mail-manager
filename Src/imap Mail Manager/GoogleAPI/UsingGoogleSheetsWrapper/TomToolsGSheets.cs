using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Schema;
using GoogleSheetsWrapper;
using jig.Tools;
using System.Collections.Generic;
using System.Reflection;
using DocumentFormat.OpenXml.Wordprocessing;


namespace ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper;

/// <summary>
/// Tools rund um das Google Sheets API
/// </summary>
public static class TomToolsGSheets {
   
   #region GSheets Spalten-Formatierung
   
   /// <summary>
   /// Definiert die Formatierung einer GSheets-Spalte
   /// </summary>
   /// <param name="NumberFormatType"></param>
   /// <param name="NumberFormatPattern"></param>
   /// <param name="Numb1ErFormatPattern"></param>
   public record GSheetsColFormat(
      string   SpaltenName
      , string   NumberFormatType
      , string NumberFormatPattern
      , int    ColumnID1Based) {

      // public override string ToString() => $"Personal Namespace: {Path}";

   }


   /// <summary>
   ///  Liest von einer von BaseRecord abgeleiteten Klasse die Attribute:
   ///  - TomGSheetsFormat
   ///  - SheetField
   /// Und berechnet daraus die Formatierung für das GSheet
   /// </summary>
   /// <param name="baseRecordType"></param>
   /// <returns></returns>
   public static List<GSheetsColFormat> Get_BaseRecord_GSheet_ColFormat(Type baseRecordType) {

      //+ Alle Attributte der Klasse bestimmen 
      var typeAttributtes =
         from prop in baseRecordType.GetProperties()
         from ca in prop.CustomAttributes
         where (prop.CustomAttributes.Any())
         select prop;

      // Die Formatierung für die GSheets-Spalten
      List<GSheetsColFormat> resColFormats = new List<GSheetsColFormat>();

      //+ Alle Props suchen, die diese beiden Attributte haben
      // - TomGSheetsFormat
      // - SheetField
      //
      // Mit diesen Attributten die Spalten-Formatierung bestimmen
      // ++ Gruppieren n ach dem Proeprty-Namen
      foreach (var propertyInfo in typeAttributtes.GroupBy(x => x.Name).Select(g => g.First())) {
         //++ Haben wir das SheetFieldAttribute?
         IEnumerable<CustomAttributeData> asheetFieldAttribute =
            from ca in propertyInfo.CustomAttributes
            where (ca.AttributeType.Name.Equals("SheetFieldAttribute", StringComparison.OrdinalIgnoreCase))
            select ca;

         //++ Haben wir das TomGSheetsFormatAttribute?
         IEnumerable<CustomAttributeData> aTomGSheetsFormatAttribute =
            from ca in propertyInfo.CustomAttributes
            where (ca.AttributeType.Name.Equals("TomGSheetsFormatAttribute", StringComparison.OrdinalIgnoreCase))
            select ca;

         //+ Wenn wir beide Attribute haben
         if (asheetFieldAttribute.Any()
             && aTomGSheetsFormatAttribute.Any()) {
            //+ Dann die GSheets Spalten-Formatierung auslesen
            string numberFormatType = aTomGSheetsFormatAttribute
                                      .First()
                                      .NamedArguments.Where
                                         (x => x.MemberName.Equals("NumberFormatType", StringComparison.OrdinalIgnoreCase))
                                      .Select(x => x.TypedValue.Value).First().ToString();

            string numberFormatPattern = aTomGSheetsFormatAttribute
                                         .First()
                                         .NamedArguments.Where
                                            (x => x.MemberName.Equals("NumberFormatPattern", StringComparison.OrdinalIgnoreCase))
                                         .Select(x => x.TypedValue.Value).First().ToString();

            int columnID1Based = (int)asheetFieldAttribute
                                            .First()
                                            .NamedArguments.Where(x => x.MemberName.Equals("ColumnID", StringComparison.OrdinalIgnoreCase))
                                            .Select(x => x.TypedValue.Value).First();

            resColFormats.Add(new GSheetsColFormat(propertyInfo.Name, numberFormatType, numberFormatPattern, columnID1Based));
         }
      }

      return resColFormats;
   }


   #endregion GSheets Spalten-Formatierung


   /// <summary>
   /// Liest aus <T> das Attribut mit dem Property "DisplayName" und erzeugt die Daten für die Spaltenbeschriftung
   /// </summary>
   /// <param name="s"></param>
   /// <typeparam name="T"></typeparam>
   /// <returns></returns>
   public static List<string> GetHeaderRowRecord<T>(string spaltenNamenSuffix = "")
      where T : BaseRecord {
      //+ Die Attributte lesen
      var fieldAttributes = SheetFieldAttributeUtils.GetAllSheetFieldAttributes<T>();

      //+ Die DisplayNamenn sammeln
      List<string> header = new List<string>();

      // Nach der Spaltennummer sortieren
      foreach (var kvp in fieldAttributes.OrderBy(x => x.Key.ColumnID)) {
         var attribute = kvp.Key;

         // Debug.WriteLine($"{attribute.ColumnID}: {attribute.DisplayName}");
         // header.Add(attribute.DisplayName);
         header.Add(spaltenNamenSuffix != null ? $"{attribute.DisplayName}{spaltenNamenSuffix}" : attribute.DisplayName);
      }

      return header;
   }

}
