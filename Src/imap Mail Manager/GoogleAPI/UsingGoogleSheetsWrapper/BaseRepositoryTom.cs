using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Google.Apis.Sheets.v4.Data;
using GoogleSheetsWrapper;
using ImapMailManager.GoogleAPI.TomTools;


namespace ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper;

/// <summary>
/// Korrekturen für GoogleSheetsWrapper:
/// - Überschreibt die Funktion ValidateSchema(), damit sie die Header mit Trim() vergleicht
/// - Überschreibt AddRecords(), damit sichergestellt ist, dass bei Bedarf vorher genügen Zeilen & Spalten im sheet sind
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class BaseRepositoryTom<T> : BaseRepository<T>
   where T : BaseRecord {

   public BaseRepositoryTom() : base() {}


   public BaseRepositoryTom(SheetHelper<T> sheetsHelper, bool hasHeaderRow = true) : base
      (sheetsHelper
       , hasHeaderRow) {}


   public BaseRepositoryTom(
      string   spreadsheetID
      , string serviceAccountEmail
      , string tabName
      , string jsonCredentials
      , bool   hasHeaderRow = true) : base
      (spreadsheetID
       , serviceAccountEmail
       , tabName
       , jsonCredentials
       , hasHeaderRow) {}


   /// <summary>
   /// Der Einstiegspunkt der Funktion ValidateSchema()
   /// Muss auch mit new überschrieben werden, weil sonst nicht meine Variante von:
   ///   public new SchemaValidationResult ValidateSchema(IList<object> row) {
   /// genützt wird.
   /// </summary>
   /// <returns></returns>
   /// <exception cref="ArgumentException"></exception>
   public new SchemaValidationResult ValidateSchema() {
      if (!this.HasHeaderRow)
         throw new ArgumentException
            ("ValidateSchema cannot be called when the HasHeaderRow property is set to false");

      return this.ValidateSchema(this.SheetsHelper.GetRows(this.SheetHeaderRange)[0]);
   }

   /// <summary>
   /// Schreibt die Funktion neu, damit in GSheets die Spaltentitel auch Leerzeichen beinhalten können 
   /// </summary>
   /// <param name="row"></param>
   /// <returns></returns>
   /// <exception cref="ArgumentException"></exception>
   public new SchemaValidationResult ValidateSchema(IList<object> row) {
      if (!this.HasHeaderRow)
         throw new ArgumentException
            ("ValidateSchema cannot be called when the HasHeaderRow property is set to false");

      SchemaValidationResult validationResult = new SchemaValidationResult() {
         IsValid = true, ErrorMessage = ""
      };

      foreach (KeyValuePair<SheetFieldAttribute, PropertyInfo> sheetFieldAttribute in
               SheetFieldAttributeUtils.GetAllSheetFieldAttributes<NPOI.SS.Formula.Functions.T>()) {
         SheetFieldAttribute key = sheetFieldAttribute.Key;

         if (row.Count < key.ColumnID) {
            validationResult.IsValid = false;

            validationResult.ErrorMessage += string.Format
               ("'{0}' column id: {1} is greater than the defined column count: {2}. "
                , (object)key.DisplayName
                , (object)key.ColumnID
                , (object)row.Count);
         }
         else if (row[key.ColumnID - 1]?.ToString().Trim() != key.DisplayName.Trim()) {
            validationResult.IsValid = false;

            validationResult.ErrorMessage += string.Format
               ("Expected column name: '{0}' however, current column name is: '{1}'. "
                , (object)key.DisplayName
                , row[key.ColumnID - 1]);
         }
      }

      return validationResult;
   }

   /*
   public new BatchUpdateSpreadsheetResponse AddRecord(T record) {
      // ‼ Fix für GoogleSheetsWrapper
      // AddRecords() stellt nicht sicher, dass das Sheet genut Spalten & Zeilen hat
      var zeilen  = 1;
      var spalten = ((IEnumerable<T>)record).Count();
      
      GoogleSheetApi.Assert_GSheets_sheetRowsAndCols
         (SheetsHelper.SpreadsheetID
          , SheetsHelper.TabName
          , zeilen + 2
          , spalten+ 1);

      return base.AddRecord(record);
   }


   public new BatchUpdateSpreadsheetResponse AddRecords(IList<T> records) {
      // ‼ Fix für GoogleSheetsWrapper
      // AddRecords() stellt nicht sicher, dass das Sheet genut Spalten & Zeilen hat
      var zeilen  = ((IEnumerable<T>)records).Count();
      var spalte1 = ((IEnumerable<T>)records).First();
      var spalten = ((IEnumerable<T>)spalte1).Count();

      GoogleSheetApi.Assert_GSheets_sheetRowsAndCols
         (SheetsHelper.SpreadsheetID
          , SheetsHelper.TabName
          , zeilen + 2
          , spalten + 1);

      return base.AddRecords(records);
   }
   */
   
}
