using System;


namespace ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper;

/// <summary>
/// Ein eigenes C# Class Prop-Atteribute,
/// um die Formatierung in Google Sheets zu deklarieren
/// </summary>
[AttributeUsage(AttributeTargets.All)]
public class TomGSheetsFormatAttribute : Attribute {

   /// <summary>
   /// Siehe:
   /// https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/cells#NumberFormat
   /// </summary>
   public string NumberFormatType { get; set; }

   /// <summary>
   /// Siehe:
   /// https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/cells#NumberFormat
   /// </summary>
   public string NumberFormatPattern { get; set; }

}
