using System;
using System.Collections.Generic;
using GoogleSheetsWrapper;


namespace ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper.StatsMbxFolder;

/// <summary>
/// Ein GSheets Record mit der Statistik der Mailbox-Ordner
/// </summary>
public class GSheets_MbxFldr_Stats_Rec : BaseRecord {

   
   [SheetField(
                 DisplayName = "Kategorie",
                 ColumnID = 1,
                 FieldType = SheetFieldType.String)]

   // !M https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/cells#NumberFormat
   [TomGSheetsFormat(NumberFormatType = "TEXT", NumberFormatPattern = "")]
   public string FldrCat { get; set; }

   [SheetField(
                 DisplayName = "Ordner",
                 ColumnID = 2,
                 FieldType = SheetFieldType.String)]
   [TomGSheetsFormat(NumberFormatType = "TEXT", NumberFormatPattern = "")]
   public string Fldr { get; set; }

   [SheetField(
                 DisplayName = "Anzahl Mails",
                 ColumnID = 3,
                 FieldType = SheetFieldType.Integer,
                 NumberFormatPattern = "#,##0")]
   [TomGSheetsFormat(NumberFormatType = "NUMBER", NumberFormatPattern = "#,##0")]
   public int AnzMails { get; set; }

   [SheetField(
                 DisplayName = "(ungelesen)",
                 ColumnID = 4,
                 FieldType = SheetFieldType.Integer,
                 NumberFormatPattern = "#,##0")]
   [TomGSheetsFormat(NumberFormatType = "NUMBER", NumberFormatPattern = "#,##0")]
   public int AnzMailsUnread { get; set; }

   [SheetField(
                 DisplayName = "Grösse [KB]",
                 ColumnID = 5,
                 FieldType = SheetFieldType.Integer,
                 NumberFormatPattern = "#,##0")]
   [TomGSheetsFormat(NumberFormatType = "NUMBER", NumberFormatPattern = "#,##0")]
   public int GrösseKB { get; set; }

   [SheetField(
                 DisplayName = "Älteste E-Mail",
                 ColumnID = 6,
                 FieldType = SheetFieldType.DateTime,

                 //‼ Muss dieses Format haben, GSheets und auch der GoogleSheetsWrapper
                 // kommen sonst nicht klar
                 NumberFormatPattern = "dd.MM.yy hh:mm"
              )]
   [TomGSheetsFormat(NumberFormatType = "DATE_TIME", NumberFormatPattern = "dd.MM.yy hh:mm:00")]
   public DateTime? OldestMail { get; set; }

   [SheetField(
                 DisplayName = "Neuste E-Mail",
                 ColumnID = 7,
                 FieldType = SheetFieldType.DateTime,

                 //‼ Muss dieses Format haben, GSheets und auch der GoogleSheetsWrapper
                 // kommen sonst nicht klar
                 NumberFormatPattern = "dd.MM.yy hh:mm"
              )]
   [TomGSheetsFormat(NumberFormatType = "DATE_TIME", NumberFormatPattern = "dd.MM.yy hh:mm:00")]
   public DateTime? NewestMail { get; set; }


   /// <summary>
   /// Constructor für BaseRecord 
   /// </summary>
   public GSheets_MbxFldr_Stats_Rec() {}


   /// <summary>
   /// Constructor für BaseRecord 
   /// </summary>
   /// <param name="row"></param>
   /// <param name="rowId"></param>
   /// <param name="minColumnId"></param>
   public GSheets_MbxFldr_Stats_Rec(IList<object> row, int rowId, int minColumnId = 1)
      : base(row, rowId, minColumnId) {}

}
