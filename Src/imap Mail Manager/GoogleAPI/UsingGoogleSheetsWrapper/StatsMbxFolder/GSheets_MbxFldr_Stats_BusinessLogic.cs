using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Google.Apis.Sheets.v4.Data;
using GoogleSheetsWrapper;
using Hierarchy;
using ImapMailManager.Config;
using ImapMailManager.GoogleAPI.TomTools;
using ImapMailManager.Mail;
using ImapMailManager.Mail.Hierarchy;
using ImapMailManager.Mail.Stats;
using ImapMailManager.Properties;
using jig.WPF.SerilogViewer;
using static ImapMailManager.GoogleAPI.TomTools.GoogleSheetApi;


namespace ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper.StatsMbxFolder;

/// <summary>
/// Business-Logik für die Statistik der Ordner-Struktor in der Mailbox
/// </summary>
public class GSheets_MbxFldr_Stats_BusinessLogic {

   public static readonly string BlattName = "StatistikProMailboxOrdner";


   /// <summary>
   /// Die Mailbox Order Statistik erzeugen und in GSheets publizieren
   /// </summary>
   /// <param name="cfgImapServerRl"></param>
   /// <param name="cfgImapServerCredAktiv"></param>
   /// <param name="fetchMailHeaders"></param>
   /// <param name="serilogViewer"></param>
   /// <param name="hierarchyNode"></param>
   public static void PublishStats(
      Sensitive_AppSettings.CfgImapServer               cfgImapServerRl
      , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCredAktiv
      , bool                                        fetchMailHeaders
      , SerilogViewer                               serilogViewer
      , HierarchyNode<ImapHrchyNde>?                rootNode) {
      //+ Die Mailbox-Daten lesen
      var (mbxFoldersGroupByCat, statsSortierung) = CalcImapStats.Calc_MailboxFolder_Stats
         (
          RuntimeConfig_MailServer.CfgImapServerRl
          , RuntimeConfig_MailServer.CfgImapServerCredAktiv
          , false
          , null
          , rootNode);

      //+ Die Statistik in der Konsole ausgeben
      Create_ImapMailbox_Statistics.PrintFolderStatistics(mbxFoldersGroupByCat, statsSortierung);

      //+ Die GSheets Daten erzeugen
      //++ Den Tabellen-Header berechnen
      var TableHeader = TomToolsGSheets.GetHeaderRowRecord<GSheets_MbxFldr_Stats_Rec>();

      //++ Die Daten exportieren
      ExportData(mbxFoldersGroupByCat, statsSortierung);

      //++ Die spaltenbreiten justieren
      AutoResizeDimensions
         (GoogleSheetMbxStatistics.SpreadsheetId, GSheets_MbxFldr_Stats_BusinessLogic.BlattName);
   }


   /// <summary>
   /// Exportiert die Informationen zu jedem Mailbox-Ordner zu GSheets
   /// Getestet
   /// </summary>
   /// <param name="allFolders"></param>
   /// <param name="catOrder"
   ///Sortierung der Ausgabe
   /// ></param>
   public static void ExportData(
      IEnumerable<IGrouping<CalcImapStats.eMailCategory, ImapFolderNde>> mbxFoldersGroupByCat
      , Dictionary<int, CalcImapStats.eMailCategory>                     catOrder) {
      //++ Zu GSheet verbinden
      var sheetHelper = GSheets_MbxFldr_Stats_Repo.GetSheetHelper<GSheets_MbxFldr_Stats_Rec>
         (BlattName);
      var appender = new SheetAppender(sheetHelper);

      //+ Alle Daten löschen
      // Anzahl Zeilen bestimmen
      var rows = sheetHelper.GetRows
         (new SheetRange
             (BlattName
              , 1
              , 1
              , 1));

      // und löschen
      if (rows != null
          && rows.Any()) {
         // ! GSheets erlaubt nicht, alle Zellen zu löschen:
         // sheetHelper.DeleteRows(1, rows.Count);
         // ! Diese Funktion hat zur Folge, dass GSheets meint, dass Zeile 1 Daten enthält
         DeleteSheetData(GoogleSheetMbxStatistics.SpreadsheetId, BlattName);

         // … Also müssen wir die 'Daten' noch löschen 
         Sheet_Delete_CellProperties
            (GoogleSheetMbxStatistics.SpreadsheetId
             , BlattName
             , GSheetsFieldMask.GSheetsFlags.DatenLöschen
               | GSheetsFieldMask.GSheetsFlags.FormatierungLöschen);
      }

      //+ Den Header schreiben
      // Aus den Klassen-Attributen die Metadaten lesen und die Spaltentitel generieren
      // ein Suffix ergänzen, um bei der automatischen Spaltenbreite genügend breite Spalten zu erhalten
      var headerData = TomToolsGSheets.GetHeaderRowRecord<GSheets_MbxFldr_Stats_Rec>("   ");

      // und schreiben
      appender.AppendRows(new List<List<string>>() { headerData });

      // Create a Repository for the TestRecord class
      var repository = new GSheets_MbxFldr_Stats_Repo(sheetHelper);

      //+ Alle Daten für die GSheets-Zeilen sammeln
      List<GSheets_MbxFldr_Stats_Rec> datenRecords = new List<GSheets_MbxFldr_Stats_Rec>();

      //+ Die Liste nach Category sortiert verarbeiten
      // foreach (var thisCat in catOrder.OrderBy(x => x.Key).Select(x => x.Value)) {
      foreach (var thisCat in catOrder.OrderBy(x => x.Key)) {
         //! Ex: Die Ordner der Gruppe holen 
         List<ImapFolderNde>? mailsInCat = mbxFoldersGroupByCat.FirstOrDefault
            (x => x.Key == thisCat.Value)?.ToList();

         // Die Mails der Kategorie durchlaufen
         // ‼ !TTTom C#, Linq: foreach, das auch mit null objects klappt
         // Sortieren nach dem Pfad 
         mailsInCat?.OrderBy(x => x.Path).ToList().ForEach
            (
             imapFolderNde => {
                //++ Die Daten im GSteets-Record erfassen
                datenRecords.Add
                   (
                    new GSheets_MbxFldr_Stats_Rec() {
                       FldrCat          = $"{thisCat.Key.ToString()} {thisCat.Value.ToString()}"
                       , Fldr           = imapFolderNde!.Path
                       , AnzMails       = imapFolderNde.NoOfMails
                       , AnzMailsUnread = imapFolderNde.NoOfUnread
                       , GrösseKB       = (int)(imapFolderNde.SizeBytes/1024)
                       , OldestMail     = imapFolderNde.ÄltesteMail
                       , NewestMail     = imapFolderNde.NeusteMail
                    });
             });
      }

      //+ Alle Daten Batch-Update aktualisieren
      repository.AddRecords(datenRecords);

      //+ Header Fett machen
      var anzSpalten = headerData.Count;
      var updates    = new List<BatchUpdateRequestObject>();

      updates.Add
         (
          new BatchUpdateRequestObject() {
             Range = new SheetRange
                (GetA1Notion
                    (""
                     , 1
                     , anzSpalten
                     , 1
                     , 1))
             , Data = new CellData() {
                UserEnteredFormat = new CellFormat()
                   { TextFormat = new TextFormat() { Bold = true } }
             }
          });

      // Note the field mask is for both the UserEnteredValue and the UserEnteredFormat below,
      // Nur die Formatierung, nicht die Cellen-Werte ändern!
      // sheetHelper.BatchUpdate(updates, "userEnteredValue, userEnteredFormat");
      sheetHelper.BatchUpdate(updates, "userEnteredFormat");

      //+ Die GSheet-Spalten formatieren
      var spaltenFormatierungen = TomToolsGSheets.Get_BaseRecord_GSheet_ColFormat
         (typeof(GSheets_MbxFldr_Stats_Rec));

      SetColumnFormat
         (GoogleSheetMbxStatistics.SpreadsheetId
          , BlattName
          , spaltenFormatierungen);
   }

}
