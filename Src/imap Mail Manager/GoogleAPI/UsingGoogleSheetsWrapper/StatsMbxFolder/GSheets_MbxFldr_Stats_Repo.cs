using GoogleSheetsWrapper;
using ImapMailManager.GoogleAPI.TomTools;


namespace ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper.StatsMbxFolder;

/// <summary>
/// Das Repository-Objekt für das GSteets API
/// </summary>
public class GSheets_MbxFldr_Stats_Repo : BaseRepositoryTom<GSheets_MbxFldr_Stats_Rec> {

   public GSheets_MbxFldr_Stats_Repo() {}


   public GSheets_MbxFldr_Stats_Repo(SheetHelper<GSheets_MbxFldr_Stats_Rec> sheetsHelper)
      : base(sheetsHelper) {}


   /// <summary>
   /// Verbindet zu GSheets
   /// </summary>
   public static SheetHelper<T> GetSheetHelper<T>(string sheetName)
      where T : BaseRecord {
      // Create a SheetHelper class for the specified Google Sheet and Tab name
      // var sheetHelper = new SheetHelper<GSheetsMbxFldrStatsRec>(
      var sheetHelper = new SheetHelper<T>(
                                           GoogleSheetMbxStatistics.SpreadsheetId,
                                           GoogleSheetApiCfg.ApplicationServiceAccount,
                                           sheetName);

      sheetHelper.Init(GoogleSheetApiCfg.GetGoogleCredentialJson());

      return sheetHelper;
   }

}
