﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using GoogleSheetsWrapper;
using Hierarchy;
using jig.Tools;
using jig.Tools.Exception;
using jig.Tools.String;


namespace ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper {

   public static class ExtensionMethods_GoogleSheetsWrapper {

      #region Public Methods and Operators

      /// <summary>
      /// Liest aus <T> die DisplayNamen und erzeugt die Daten für den Tabellen-Header
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <returns></returns>
      public static List<string> ØGetTableHeader<T>(this T recordType, string spaltenNamenSuffix = "")
         where T : BaseRecord {
         //+ Von allen Props alle SheetField Attributte lesen
         var fieldAttributes = SheetFieldAttributeUtils.GetAllSheetFieldAttributes<T>();

         //+ Die DisplayNamenn sammeln
         List<string> header = new List<string>();

         // Nach der Spaltennummer sortieren
         foreach (var kvp in fieldAttributes.OrderBy(x => x.Key.ColumnID)) {
            var attribute = kvp.Key;

            // Debug.WriteLine($"{attribute.ColumnID}: {attribute.DisplayName}");
            header.Add(spaltenNamenSuffix.ØHasValue() ? $"{attribute.DisplayName}{spaltenNamenSuffix}" : attribute.DisplayName);
         }

         return header;
      }

      #endregion


      #region Archiv, !KH

      /// <summary>
      ///
      /// ‼ Funktioniert leider nicht!
      /// Bei einem CustomAttributeNamedArgument kann der TyoedValue zur Laufzeit leider nicht geändert werden 
      ///
      /// Die Idee war:
      /// Liest aus <T> die DisplayNamen und erzeugt die Daten für den Tabellen-Header
      /// 
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <returns></returns>
      ///
      /// Sonderzeichen
      /// https://www.compart.com/de/unicode/scripts/Latn
      /// 
      /// Ø    Ɵ     ǀ     ǁ    ǂ     ǃ
      /// Ɂ    Ʌ     ɵ     ɸ    ʘ     ᴉ
      /// ᴓ    ᶲ      ⁱ      ⱡ
      ///  
      public static void AddAttributeSuffixǂ<T>(this T recordType, string attributeName, string suffix)
         where T : BaseRecord {
         //+ Alle Typen-Properties suchen
         var props = typeof(T).GetProperties()
                              .Where(prop => prop.IsDefined(typeof(T), false));

         //++ Alle Attribute mit dem Namen "DisplayName" suchen
         IEnumerable<CustomAttributeNamedArgument> customAttr1 = (
                                                                    from prop in props
                                                                    from ca in prop.CustomAttributes
                                                                    from namedArgument in ca.NamedArguments
                                                                    where namedArgument.MemberName == "DisplayName"
                                                                    select namedArgument);

         //+++ Bei allen gefundenen Attributen den Value um ein Suffix ergänzen
         foreach (CustomAttributeNamedArgument fsdfs in customAttr1) {
            //+ klappt nicht 
            // fsdfs.TypedValue = $"{fsdfs.TypedValue} - suffix";
            // fsdfs = $"{fsdfs.TypedValue} - suffix";
         }

         throw new RuntimeException("ØAddAttributeSuffix() die idee ist leider nicht umsetzbar");
      }

      #endregion Archiv, !KH

   }

}
