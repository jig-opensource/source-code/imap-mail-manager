using System;
using System.Collections.Generic;
using System.Reflection;
using GoogleSheetsWrapper;
using NPOI.SS.Formula.Functions;


namespace ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper.StatsMbxAntwortenVsNeueFragen;

/// <summary>
/// Ein GSheets Record mit der Statistik der Mailbox-Ordner
/// </summary>
public class GSheets_MbxAntwortenVsNeueFragen_Stats_Rec : BaseRecord {
   
   
   [SheetField
      (
         DisplayName = "Jahr"
         , ColumnID = 1
         , FieldType = SheetFieldType.Integer
         , NumberFormatPattern = "0000")]

   // !M https://developers.google.com/sheets/api/reference/rest/v4/spreadsheets/cells#NumberFormat
   [TomGSheetsFormat(NumberFormatType = "NUMBER", NumberFormatPattern = "0000")]
   public int Jahr { get; set; }

   [SheetField
      (
         DisplayName = "KW"
         , ColumnID = 2
         , FieldType = SheetFieldType.Integer
         , NumberFormatPattern = "00")]
   [TomGSheetsFormat(NumberFormatType = "NUMBER", NumberFormatPattern = "00")]
   public int KW { get; set; }

   [SheetField
      (
         DisplayName = "EndDatumDerKW"
         , ColumnID = 3
         , FieldType = SheetFieldType.DateTime
         ,

         //‼ Muss dieses Format haben, GSheets und auch der GoogleSheetsWrapper
         // kommen sonst nicht klar
         NumberFormatPattern = "dd.MM.yy hh:mm")]
   [TomGSheetsFormat(NumberFormatType = "DATE_TIME", NumberFormatPattern = "dd.MM.yy hh:mm:00")]
   public DateTime? EndDatumDerKW { get; set; }

   [SheetField
      (
         DisplayName = "Anzahl neue Fragen in KW"
         , ColumnID = 4
         , FieldType = SheetFieldType.Integer
         , NumberFormatPattern = "#,##0")]
   [TomGSheetsFormat(NumberFormatType = "NUMBER", NumberFormatPattern = "#,##0")]
   public int AnzNeueFragenInKW { get; set; }

   [SheetField
      (
         DisplayName = "Anzahl Antworten in Arbeit in KW"
         , ColumnID = 5
         , FieldType = SheetFieldType.Integer
         , NumberFormatPattern = "#,##0")]
   [TomGSheetsFormat(NumberFormatType = "NUMBER", NumberFormatPattern = "#,##0")]
   public int AnzAntwortenInArbeitInKW { get; set; }

   [SheetField
      (
         DisplayName = "Anzahl Antworten geschickt in KW"
         , ColumnID = 6
         , FieldType = SheetFieldType.Integer
         , NumberFormatPattern = "#,##0")]
   [TomGSheetsFormat(NumberFormatType = "NUMBER", NumberFormatPattern = "#,##0")]
   public int AnzAntwortenInKWGeschickt { get; set; }

   [SheetField
      (
         DisplayName = "Anzahl offene Fragen bis KW"
         , ColumnID = 7
         , FieldType = SheetFieldType.Integer
         , NumberFormatPattern = "#,##0")]
   [TomGSheetsFormat(NumberFormatType = "NUMBER", NumberFormatPattern = "#,##0")]
   public int AnzOffeneFragenBisKW { get; set; }


   /// <summary>
   /// Constructor für BaseRecord 
   /// </summary>
   public GSheets_MbxAntwortenVsNeueFragen_Stats_Rec() {}


   /// <summary>
   /// Constructor für BaseRecord 
   /// </summary>
   /// <param name="row"></param>
   /// <param name="rowId"></param>
   /// <param name="minColumnId"></param>
   public GSheets_MbxAntwortenVsNeueFragen_Stats_Rec(
      IList<object> row
      , int         rowId
      , int         minColumnId = 1)
      : base
         (row
          , rowId
          , minColumnId) {}

}
