using System;
using System.Collections.Generic;
using System.Reflection;
using GoogleSheetsWrapper;
using ImapMailManager.GoogleAPI.TomTools;
using NPOI.SS.Formula.Functions;


namespace ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper.StatsMbxAntwortenVsNeueFragen;

/// <summary>
/// Das Repository-Objekt für das GSteets API
/// </summary>

// todo aufräumen:
// public class GSheets_MbxAntwortenVsNeueFragen_Stats_Repo : BaseRepository<GSheets_MbxAntwortenVsNeueFragen_Stats_Rec> {
public class GSheets_MbxAntwortenVsNeueFragen_Stats_Repo : BaseRepositoryTom<GSheets_MbxAntwortenVsNeueFragen_Stats_Rec> {

   
   public GSheets_MbxAntwortenVsNeueFragen_Stats_Repo() {}


   public GSheets_MbxAntwortenVsNeueFragen_Stats_Repo(SheetHelper<GSheets_MbxAntwortenVsNeueFragen_Stats_Rec> sheetsHelper)
      : base(sheetsHelper) {}


   /// <summary>
   /// Verbindet zu GSheets
   /// </summary>
   public static SheetHelper<T> GetSheetHelper<T>(string sheetName)
      where T : BaseRecord {
      // Create a SheetHelper class for the specified Google Sheet and Tab name
      // var sheetHelper = new SheetHelper<GSheetsMbxFldrStatsRec>(
      var sheetHelper = new SheetHelper<T>(
                                           GoogleSheetMbxStatistics.SpreadsheetId,
                                           GoogleSheetApiCfg.ApplicationServiceAccount,
                                           sheetName);

      sheetHelper.Init(GoogleSheetApiCfg.GetGoogleCredentialJson());

      return sheetHelper;
   }

}
