using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using GoogleSheetsWrapper;
using GoogleSheetsWrapper.Utils;
using Hierarchy;
using ImapMailManager.Config;
using ImapMailManager.Excel;
using ImapMailManager.GoogleAPI.TomTools;
using ImapMailManager.Mail;
using ImapMailManager.Mail.Hierarchy;
using ImapMailManager.Mail.Stats;
using ImapMailManager.Properties;
using jig.Tools;
using jig.Tools.String;
using jig.WPF.SerilogViewer;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;


namespace ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper.StatsMbxAntwortenVsNeueFragen;

/// <summary>
/// Business-Logik für die Statistik pro KW:
/// - Anz Antwortenpro KW
/// - Anz neue Fragen pro KW
/// - Anz offene Fragen insgesamt zur Zeit der KW
/// </summary>
public class GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic {

   /// <summary>
   /// Der Name des Google Sheets
   /// </summary>
   public static readonly string BlattName = "StatistikAntwortenUndFragen";

   // Wie viele Wochen rückwirkend sollen bestehende Excel-Daten aktualisiert werden?
   private static readonly int UpdateRückwirkendAnzWeeks = 4;


   /// <summary>
   /// Die Mailbox Order Statistik erzeugen und in GSheets publizieren
   /// </summary>
   /// <param name="cfgImapServer"></param>
   /// <param name="cfgImapServerCred"></param>
   /// <param name="fetchMailHeaders"></param>
   /// <param name="serilogViewer"></param>
   /// <param name="rootNode"></param>
   public static void PublishStats(
      Sensitive_AppSettings.CfgImapServer               cfgImapServer
      , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred
      , bool                                        fetchMailHeaders
      , SerilogViewer                               serilogViewer
      , HierarchyNode<ImapHrchyNde>?                rootNode
      , string?                                     exportToExcelFilename = null) {
      //+ Die Mailbox-Daten lesen
      var antwortenVsNeueFragenStats = AppCfgRuntime.AntwortenVsNeueFragen_Stats;
      /*
         CalcImapStats.Calc_AntwortenVsNeueFragen_Stats(
                                                        cfgImapServer
                                                        , cfgImapServerCred
                                                        , fetchMailHeaders
                                                        , serilogViewer
                                                        , rootNode);
      */

      //+ Die Statistik in der Konsole ausgeben
      // Create_ImapMailbox_Statistics.PrintFolderStatistics(mbxFoldersGroupByCat, statsSortierung);

      //+ Die Daten zu Excel exportieren?
      if (exportToExcelFilename.ØHasValue()) {
         ExportData_ToExcel(antwortenVsNeueFragenStats, exportToExcelFilename);
      }

      //++ Die Daten exportieren
      ExportData_ToGSheets(antwortenVsNeueFragenStats);

      //++ Die Spaltenbreiten justieren
      GoogleSheetApi.AutoResizeDimensions(GoogleSheetMbxStatistics.SpreadsheetId, BlattName);
   }


   /// <summary>
   /// Exportiert die Informationen mit der Statistik pro KW
   /// </summary>
   /// <param name="allFolders"></param>
   public static void ExportData_ToGSheets(List<MailboxStatisticsProKW> mbxFoldersGroupByCat) {
      //+ Prepare
      // Ab welchem Datum aktualsieren wir rückwirkend bestehende Daten in GSheets?
      // Berechnung:
      // +1 Woche zusätzlich minus 1 Tag, um die gewünschten KW rückwirkend sicher abzudecken
      var rückwirkendeUpdatesStartDatum = DateTime.Now.AddDays
         ((UpdateRückwirkendAnzWeeks + 1)*7 - 1);

      //+ Die GSheets Header bestimmen, um die Anz. Spalten zu erhalten
      //‼ Das Suffix macht den Titel-Text breiter, danit die automatiusche Spaltenbreite am Ende passt 
      var headerData
         = TomToolsGSheets.GetHeaderRowRecord<GSheets_MbxAntwortenVsNeueFragen_Stats_Rec>("    ");

      //+ Zu GSheet verbinden
      var sheetHelper = GSheets_MbxAntwortenVsNeueFragen_Stats_Repo
        .GetSheetHelper<GSheets_MbxAntwortenVsNeueFragen_Stats_Rec>(BlattName);
      var appender = new SheetAppender(sheetHelper);

      //+ Create a Repository for the TestRecord class
      var repo = new GSheets_MbxAntwortenVsNeueFragen_Stats_Repo(sheetHelper);

      //+ Haben wir Daten im GSheet?
      // Die raw-Daten lesen, weil das Lesen via OR-Mapper für diese info knifflig ist 
      // Get all rows from the sheet starting with the 1st row, between the 1st and 8th columns
      // Leaving the last row blank tells Google Sheets to return all rows from the Sheet
      var rawGSheetsData = sheetHelper.GetRows
         (
          new SheetRange
             (
              BlattName
              , 1
              , 1
              , headerData.Count)
          , SpreadsheetsResource.ValuesResource.GetRequest.ValueRenderOptionEnum.FORMATTEDVALUE
          , SpreadsheetsResource.ValuesResource.GetRequest.DateTimeRenderOptionEnum
                                .FORMATTEDSTRING);

      // Die GSheet Daten via OR-Mapper
      List<GSheets_MbxAntwortenVsNeueFragen_Stats_Rec>? existingGSheetsRecords = null;

      //+ GSheets Daten aktualisieren
      //+ Müssen wir die Tabelle neu erzeugen?
      var recreateTable = false;

      //++ Haben wir Zeilen?
      if (rawGSheetsData != null
          && rawGSheetsData.Any()) {
         //+ Die GSheet Daten via OR-Mapper lesen
         try {
            existingGSheetsRecords = repo.GetAllRecords();
         } catch {
            // Im Bereich der Daten keinen Inhalt gefunden
         }

         //+ Wenn der OR-Mapper keine Daten fand
         // oder der header nicht stimmt
         var isValid = repo.ValidateSchema().IsValid;
         if (existingGSheetsRecords == null
             || !isValid) {
            //++ Alles neu erzeugen, d.h. die Daten löschen
            // sheetHelper.DeleteRows(1, rawGSheetsData.Count);
            GoogleSheetApi.DeleteSheetData(GoogleSheetMbxStatistics.SpreadsheetId, BlattName);

            recreateTable = true;
         }
      }
      else {
         // Keine Daten, also alles neu erzeugen
         recreateTable = true;
      }

      //+ Allenfalls die Tabelle neu erzeugen 
      if (recreateTable) {
         //++ Den Kopf schreiben
         appender.AppendRows(new List<List<string>>() { headerData });
      }
      else {
         //+ Die bestehenden GSheets Daten aktualisieren
         if (existingGSheetsRecords != null
             && existingGSheetsRecords.Any()) {
            //++ Bestehende Daten aktualisieren
            //+++ Alle neuen statistischen Daten verarbeiten
            foreach (var statsProKw in mbxFoldersGroupByCat) {
               //+++ Haben wir im Excel einen Datensatz für diese KW?
               var gSheetsRec = existingGSheetsRecords.FirstOrDefault
                  (x => x.Jahr == statsProKw.Jahr && x.KW == statsProKw.KW);

               if (gSheetsRec != null) {
                  // Markieren, dass der Rec nicht mehr exportiert werden muss
                  statsProKw.IsExportedToGoogleSheets = true;

                  // Ist das Datum zu alt, weil wir nicht so weit zurück Daten aändern?
                  if (statsProKw.EndDatumDerKW < rückwirkendeUpdatesStartDatum) {
                     continue;
                  }

                  //+++ Allenfalls die Excel Daten aktualisieren
                  // Das GoogleSheetsWrapper API kann nur einzelne Props aktualisieren,
                  // Es dürfte diese jedoch als Batch-Job tun 
                  if (gSheetsRec.AnzAntwortenInKWGeschickt != statsProKw.AnzAntwortenInKW) {
                     gSheetsRec.AnzAntwortenInKWGeschickt = statsProKw.AnzAntwortenInKW;
                     repo.SaveField(gSheetsRec, r => r.AnzAntwortenInKWGeschickt);
                  }

                  if (gSheetsRec.AnzNeueFragenInKW != statsProKw.AnzNeueFragenInKW) {
                     gSheetsRec.AnzNeueFragenInKW = statsProKw.AnzNeueFragenInKW;
                     repo.SaveField(gSheetsRec, r => r.AnzNeueFragenInKW);
                  }

                  if (gSheetsRec.AnzAntwortenInArbeitInKW != statsProKw.AnzAntwortenInArbeitInKW) {
                     gSheetsRec.AnzAntwortenInArbeitInKW = statsProKw.AnzAntwortenInArbeitInKW;
                     repo.SaveField(gSheetsRec, r => r.AnzAntwortenInArbeitInKW);
                  }

                  if (gSheetsRec.AnzOffeneFragenBisKW != statsProKw.AnzOffeneFragenBisKW) {
                     gSheetsRec.AnzOffeneFragenBisKW = statsProKw.AnzOffeneFragenBisKW;
                     repo.SaveField(gSheetsRec, r => r.AnzOffeneFragenBisKW);
                  }
               }
            }
         }
      }

      //+ Die neuen Daten ergänzen
      //++ Alle Daten für die GSheets-Zeilen sammeln
      List<GSheets_MbxAntwortenVsNeueFragen_Stats_Rec> newDatenRecords
         = new List<GSheets_MbxAntwortenVsNeueFragen_Stats_Rec>();

      //+++ Alle neuen statistik-Records verarbeiten
      // und zu GSheets übertragen
      foreach (var statsProKw in mbxFoldersGroupByCat) {
         // Existiert der Record bereits in GSheets?
         if (statsProKw.IsExportedToGoogleSheets) {
            continue;
         }

         //+++ E-Mails im Entwurf haben kein Datum bzw. optional ein sehr frühes Datum
         // » Übertragen wir nicht ins Excel
         if (statsProKw.EndDatumDerKW.Year < 1900) {
            continue;
         }

         newDatenRecords.Add
            (
             new GSheets_MbxAntwortenVsNeueFragen_Stats_Rec() {
                Jahr                        = statsProKw.Jahr
                , KW                        = statsProKw.KW
                , EndDatumDerKW             = statsProKw.EndDatumDerKW
                , AnzNeueFragenInKW         = statsProKw.AnzNeueFragenInKW
                , AnzAntwortenInArbeitInKW  = statsProKw.AnzAntwortenInArbeitInKW
                , AnzAntwortenInKWGeschickt = statsProKw.AnzAntwortenInKW
                , AnzOffeneFragenBisKW      = statsProKw.AnzOffeneFragenBisKW
             });
      }

      //+ Im Excel alle Daten per Batch-Update aktualisieren
      // ‼ Fix für GoogleSheetsWrapper
      // AddRecords() stellt nicht sicher, dass das Sheet genut Spalten & Zeilen hat
      GoogleSheetApi.Assert_GSheets_sheetRowsAndCols
         (GoogleSheetMbxStatistics.SpreadsheetId
          , BlattName
          , mbxFoldersGroupByCat.Count + 2
          , headerData.Count + 1);


      repo.AddRecords(newDatenRecords);

      //+ Header Fett machen
      var anzSpalten = headerData.Count;
      var updates    = new List<BatchUpdateRequestObject>();

      updates.Add
         (
          new BatchUpdateRequestObject() {
             // Ab Zeile 1 / Spalte 1 bis zu den anz. Spalten
             Range = new SheetRange
                (
                 GoogleSheetApi.GetA1Notion
                    (
                     ""
                     , 1
                     , anzSpalten
                     , 1
                     , 1))
             , Data = new CellData() {
                UserEnteredFormat = new CellFormat()
                   { TextFormat = new TextFormat() { Bold = true } }
             }
          });

      // Note the field mask is for both the UserEnteredValue and the UserEnteredFormat below,
      // Nur die Formatierung, nicht die Cellen-Werte ändern!
      // sheetHelper.BatchUpdate(updates, "userEnteredValue, userEnteredFormat");
      sheetHelper.BatchUpdate(updates, "userEnteredFormat");

      //+ Die Zellen in den GSheet-Spalten formatieren
      var spaltenFormatierungen = TomToolsGSheets.Get_BaseRecord_GSheet_ColFormat
         (typeof(GSheets_MbxAntwortenVsNeueFragen_Stats_Rec));

      GoogleSheetApi.SetColumnFormat
         (GoogleSheetMbxStatistics.SpreadsheetId
          , BlattName
          , spaltenFormatierungen);
   }


   /// <summary>
   /// Schreibt den Titel in eine Zelle
   /// </summary>
   /// <param name="row"></param>
   /// <param name="spalten"></param>
   /// <param name="spaltenName"></param>
   /// <returns></returns>
   private static ICell SetCellTitle(
      HSSFWorkbook              workbook
      , IRow                    row
      , Dictionary<string, int> spalten
      , string                  spaltenName
      , bool                    bold = true) {
      var cell = row.CreateCell(spalten[spaltenName]);
      cell.SetCellValue(spaltenName);

      if (bold) {
         MyExcelTools.SetCellBold(workbook, cell);
      }

      return cell;
   }


   /// <summary>
   /// Schreibt einen Wert in eine Zelle
   /// </summary>
   /// <param name="workbook"></param>
   /// <param name="row"></param>
   /// <param name="spaltenDef"></param>
   /// <param name="spaltenName"></param>
   /// <param name="value"></param>
   /// <typeparam name="T"></typeparam>
   private static ICell SetCellValue<T>(
      HSSFWorkbook              workbook
      , IRow                    row
      , Dictionary<string, int> spaltenDef
      , string                  spaltenName
      , T                       value
      , bool                    bold = false) {
      if (value is int number) {
         var cell = row.CreateCell(spaltenDef[spaltenName]);
         cell.SetCellValue(number);

         if (bold) {
            MyExcelTools.SetCellBold(workbook, cell);
         }

         return cell;
      }
      else if (value is double dbl) {
         var cell = row.CreateCell(spaltenDef[spaltenName]);
         cell.SetCellValue(dbl);

         if (bold) {
            MyExcelTools.SetCellBold(workbook, cell);
         }

         return cell;
      }
      else if (value is DateTime date) {
         var cell = row.CreateCell(spaltenDef[spaltenName]);

         MyExcelTools.SetCellDateTime
            (workbook
             , cell
             , date);

         if (bold) {
            MyExcelTools.SetCellBold(workbook, cell);
         }

         return cell;
      }
      else if (value is string str) {
         var cell = row.CreateCell(spaltenDef[spaltenName]);
         cell.SetCellValue(str);

         if (bold) {
            MyExcelTools.SetCellBold(workbook, cell);
         }

         return cell;
      }
      else {
         int xxx = 1;

         return null;
      }
   }


   /// <summary>
   /// Die Daten zu Excel exportieren, damit Kategorie-Zuordnungen besser analysiert werden können
   /// </summary>
   /// <param name="antwortenVsNeueFragenStats"></param>
   /// <param name="exportToExcelFilename"></param>
   /// <exception cref="NotImplementedException"></exception>
   private static void ExportData_ToExcel(
      List<MailboxStatisticsProKW> mbxFoldersGroupByCat
      , string?                    exportToExcelFilename) {
      var colWidth   = 15000;
      var maxSpalten = -1;

      Dictionary<string, int> spaltenDef = new Dictionary<string, int>() {
         // Haupt-Header
         { "Jahr", 0 }
         , { "KW", 1 }
         , { "Bericht Datum", 2 }

         // Header 2 
         , { "AnzAntwortenInKW", 2 }
         , { "AnzAntwortenInKWData", 3 }
         , { "AnzNeueFragenInKW", 2 }
         , { "AnzNeueFragenInKWData", 3 }

         // Header pro Mail-Details 
         , { "Erhalten", 2 }
         , { "Pfad", 3 }
         , { "From", 4 }
         , { "SubjectNormalized", 5 }
         , { "Subject", 6 }
         ,
      };

      //+ Excel einrichten
      var workbook = MyExcelTools.InitializeWorkbook();
      var sheet1   = workbook.CreateSheet("IMAP Stats");

      var zeileNo = 0;

      //+ Titel anzeigen
      var thisCell = sheet1.CreateRow(zeileNo++).CreateCell(0);
      thisCell.SetCellValue("Antworten vs neue Fragen Stats");
      MyExcelTools.SetCellBold(workbook, thisCell);

      //+ Spaltenbreiten initialisieren
      for (var iSpalte = 0; iSpalte < 20; iSpalte++) {
         sheet1.SetColumnWidth(iSpalte, colWidth);
      }

      //+ Header anzeigen
      zeileNo++;
      var row      = sheet1.CreateRow(zeileNo++);
      var spalteNo = 0;

      SetCellTitle
         (workbook
          , row
          , spaltenDef
          , "Jahr");

      SetCellTitle
         (workbook
          , row
          , spaltenDef
          , "KW");

      SetCellTitle
         (workbook
          , row
          , spaltenDef
          , "Bericht Datum");

      // row.CreateCell(spalteNo++).SetCellValue("Erhalten");

      //!  Sender vs From
      // Sender: "Thomas Schittli" <outlook_5631ED86E3A3F8EF@outlook.com>
      // From  : "Thomas Schittli" <Thomas.Schittli@jig.ch>
      // row.CreateCell(spalteNo++).SetCellValue("Sender");

      // row.CreateCell(spalteNo++).SetCellValue("From");
      // row.CreateCell(spalteNo++).SetCellValue("FromAnz");

      // row.CreateCell(spalteNo++).SetCellValue("==");

      // row.CreateCell(spalteNo++).SetCellValue("To");
      // row.CreateCell(spalteNo++).SetCellValue("ToAnz");
      // row.CreateCell(spalteNo++).SetCellValue("SubjectNormalized");
      // row.CreateCell(spalteNo++).SetCellValue("Subject");

      //+ Daten speichern
      var thisFolder = "";

      int lineCnt = 0;

      /*      
            
            foreach (var item in mbxFoldersGroupByCat.DescendantNodes(TraversalType.DepthFirst)) {
               if (item.Data.ImapPersonalNamespaceNde != null) {}
      
               if (item.Data.ImapFolderNde != null) {
                  //++ Ein Mailbox Ordner
                  var oImapFolderNde = item.Data.ImapFolderNde;
                  thisFolder = oImapFolderNde.Path.Replace('/', '\\').TrimEnd('\\');
               }
      
               if (item.Data.ImapMsgNde != null) {
                  //++ Wir haben eine E-Mail Node
                  var msg = item.Data.ImapMsgNde;
      
                  //++Neue Zeile erzeugen
                  row = sheet1.CreateRow(zeileNo++);
      
                  // Die Anz. bisheriger Spalten merken
                  maxSpalten = Math.Max(maxSpalten, spalteNo);
                  spalteNo   = 0;
      
                  row.CreateCell(spalteNo++).SetCellValue(thisFolder);
      
                  // Datum, wie es auch Roundcube amzeigt
                  // Variante wäre: utcDateTime
                  var empfangsDatum = row.CreateCell(spalteNo++);
                  SetCellDateTime(workbook, empfangsDatum, msg.ADate.GetValueOrDefault().LocalDateTime);
      
                  //++ Sender vs From
                  // Sender: "Thomas Schittli" <outlook_5631ED86E3A3F8EF@outlook.com>
                  // From  : "Thomas Schittli" <Thomas.Schittli@jig.ch>
                  // row.CreateCell(spalteNo++).SetCellValue(msg.Sender.ToString());
      
                  row.CreateCell(spalteNo++).SetCellValue(msg.From.ToString());
                  row.CreateCell(spalteNo++).SetCellValue(msg.From.Count);
      
                  // row.CreateCell(spalteNo++).SetCellValue(msg.Sender.ToString().ToLower().Equals(msg.From.ToString().ToLower(), StringComparison.OrdinalIgnoreCase));
      
                  row.CreateCell(spalteNo++).SetCellValue(msg.To.ToString());
                  row.CreateCell(spalteNo++).SetCellValue(msg.To.Count);
                  row.CreateCell(spalteNo++).SetCellValue(msg.SubjectNormalized);
                  row.CreateCell(spalteNo++).SetCellValue(msg.Subject);
               }
            }
      */
      foreach (var statsProKw in mbxFoldersGroupByCat) {
         if (statsProKw.Jahr < 1900) {
            continue;
         }

         //+ Ausgabe:
         // Jahr - KW
         //   Kategorie - Datum, Mail Ordner, Sender, Betreff

         //++Neue Zeile erzeugen mit Gruppen-Daten
         zeileNo++;
         row = sheet1.CreateRow(zeileNo++);

         // Die Anz. bisheriger Spalten merken
         maxSpalten = Math.Max(maxSpalten, spalteNo);
         spalteNo   = 0;

         SetCellValue
            (workbook
             , row
             , spaltenDef
             , "Jahr"
             , statsProKw.Jahr);

         SetCellValue
            (workbook
             , row
             , spaltenDef
             , "KW"
             , statsProKw.KW);

         SetCellValue
            (workbook
             , row
             , spaltenDef
             , "Bericht Datum"
             , statsProKw.EndDatumDerKW);

         // row.CreateCell(spalteNo++).SetCellValue(statsProKw.Jahr);
         // row.CreateCell(spalteNo++).SetCellValue(statsProKw.KW);
         // row.CreateCell(spalteNo++).SetCellValue(statsProKw.EndDatumDerKW);

         //++ Neue Zeile: Header + Daten Antworten
         row = sheet1.CreateRow(zeileNo++);

         SetCellTitle
            (workbook
             , row
             , spaltenDef
             , "AnzAntwortenInKW"
             , true);

         SetCellValue
            (workbook
             , row
             , spaltenDef
             , "AnzAntwortenInKWData"
             , statsProKw.AnzAntwortenInKW
             , true);

         //++Neue Zeile mit Header
         //+ E-Mails mit Antworten
         row = sheet1.CreateRow(zeileNo++);

         SetCellTitle
            (workbook
             , row
             , spaltenDef
             , "Erhalten");

         SetCellTitle
            (workbook
             , row
             , spaltenDef
             , "Pfad");

         SetCellTitle
            (workbook
             , row
             , spaltenDef
             , "From");

         SetCellTitle
            (workbook
             , row
             , spaltenDef
             , "SubjectNormalized");

         SetCellTitle
            (workbook
             , row
             , spaltenDef
             , "Subject");

         if (statsProKw.ListeAntworten.Count == 0) {
            row = sheet1.CreateRow(zeileNo++);

            SetCellValue
               (workbook
                , row
                , spaltenDef
                , "Erhalten"
                , "-");
         }
         else {
            foreach (ImapMsgNde? antwort in statsProKw.ListeAntworten) {
               //++Neue Zeile mit Daten
               row      = sheet1.CreateRow(zeileNo++);
               spalteNo = 1;

               SetCellValue
                  (workbook
                   , row
                   , spaltenDef
                   , "Erhalten"
                   , antwort.ADate.GetValueOrDefault().LocalDateTime);

               SetCellValue
                  (workbook
                   , row
                   , spaltenDef
                   , "Pfad"
                   , antwort.OrdnerPfad.Replace('/', '\\').TrimEnd('\\'));

               SetCellValue
                  (workbook
                   , row
                   , spaltenDef
                   , "From"
                   , antwort.From.ToString());

               SetCellValue
                  (workbook
                   , row
                   , spaltenDef
                   , "SubjectNormalized"
                   , antwort.SubjectNormalized);

               SetCellValue
                  (workbook
                   , row
                   , spaltenDef
                   , "Subject"
                   , antwort.Subject);
            }
         }

         //++ Neue Zeile: Header + Daten Offene Fragen
         row = sheet1.CreateRow(zeileNo++);

         SetCellTitle
            (workbook
             , row
             , spaltenDef
             , "AnzNeueFragenInKW"
             , true);

         SetCellValue
            (workbook
             , row
             , spaltenDef
             , "AnzNeueFragenInKWData"
             , statsProKw.AnzNeueFragenInKW
             , true);

         //+ E-Mails mit Fragen
         //++Neue Zeile mit Header
         row = sheet1.CreateRow(zeileNo++);

         SetCellTitle
            (workbook
             , row
             , spaltenDef
             , "Erhalten");

         SetCellTitle
            (workbook
             , row
             , spaltenDef
             , "Pfad");

         SetCellTitle
            (workbook
             , row
             , spaltenDef
             , "From");

         SetCellTitle
            (workbook
             , row
             , spaltenDef
             , "SubjectNormalized");

         SetCellTitle
            (workbook
             , row
             , spaltenDef
             , "Subject");

         if (statsProKw.ListeNeueFragen.Count == 0) {
            //++Neue Zeile erzeugen
            row = sheet1.CreateRow(zeileNo++);

            SetCellValue
               (workbook
                , row
                , spaltenDef
                , "Erhalten"
                , "-");
         }
         else {
            foreach (var antwort in statsProKw.ListeNeueFragen) {
               //++Neue Zeile erzeugen
               row      = sheet1.CreateRow(zeileNo++);
               spalteNo = 1;

               row.CreateCell(spalteNo++).SetCellValue
                  (antwort.ADate.GetValueOrDefault().LocalDateTime);

               row.CreateCell(spalteNo++).SetCellValue
                  (antwort.OrdnerPfad.Replace('/', '\\').TrimEnd('\\'));
               row.CreateCell(spalteNo++).SetCellValue(antwort.From.ToString());
               row.CreateCell(spalteNo++).SetCellValue(antwort.SubjectNormalized);
               row.CreateCell(spalteNo++).SetCellValue(antwort.Subject);
            }
         }

         // Foreach verlassen
         if (lineCnt++ >= 10) {
            break;
         }
      }

      //+ Die Spaltenbreiten anpassen
      for (var iSpalte = 0; iSpalte < maxSpalten; iSpalte++) {
         try {
            sheet1.AutoSizeColumn(iSpalte);
         } catch (InvalidCastException e) {
            // Ignorieren
         }
      }

      sheet1.SetColumnWidth(1, sheet1.GetColumnWidth(1) + 50);

      //+ Datei speichern
      workbook.ØSave(exportToExcelFilename);

      Debug.WriteLine("Fertig!");
   }

}
