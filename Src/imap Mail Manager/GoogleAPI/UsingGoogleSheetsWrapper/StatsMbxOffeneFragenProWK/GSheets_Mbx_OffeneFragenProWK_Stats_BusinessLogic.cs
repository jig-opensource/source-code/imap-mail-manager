using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using GoogleSheetsWrapper;
using Hierarchy;
using ImapMailManager.Config;
using ImapMailManager.GoogleAPI.TomTools;
using ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper.StatsMbxAntwortenVsNeueFragen;
using ImapMailManager.Mail;
using ImapMailManager.Mail.Hierarchy;
using ImapMailManager.Mail.Stats;
using ImapMailManager.Properties;
using jig.WPF.SerilogViewer;


namespace ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper.StatsMbxOffeneFragenProWK;

/// <summary>
/// Business-Logik für die Statistik pro KW:
/// - Anz offene Fragen pro KW
/// </summary>
public class GSheets_Mbx_OffeneFragenProWK_Stats_BusinessLogic {

   /// <summary>
   /// Der Name des Google Sheets
   /// </summary>
   public static readonly string BlattName = "StatistikOffeneFragenProKW";

   // Wie viele Wochen rückwirkend sollen bestehende Excel-Daten aktualisiert werden?
   private static readonly int UpdateRückwirkendAnzWeeks = 4;


   /// <summary>
   /// Berechnet die Anz. KW zwischen zwei Tagen
   /// </summary>
   /// <param name="d1"></param>
   /// <param name="d2"></param>
   /// <param name="startOfWeek"></param>
   /// <returns></returns>
   static public int CalcWeekDiff(DateTime d1, DateTime d2, DayOfWeek startOfWeek = DayOfWeek.Sunday) {
      var diff = d2.Subtract(d1);

      var weeks = (int)diff.Days/7;

      // need to check if there's an extra week to count
      var remainingDays         = diff.Days%7;
      var cal                   = CultureInfo.InvariantCulture.Calendar;
      var d1WeekNo              = cal.GetWeekOfYear(d1, CalendarWeekRule.FirstFullWeek, startOfWeek);
      var d1PlusRemainingWeekNo = cal.GetWeekOfYear(d1.AddDays(remainingDays), CalendarWeekRule.FirstFullWeek, startOfWeek);

      if (d1WeekNo != d1PlusRemainingWeekNo)
         weeks++;

      return weeks;
   }


   /// <summary>
   /// Die Mailbox Order Statistik erzeugen und in GSheets publizieren
   /// </summary>
   /// <param name="cfgImapServer"></param>
   /// <param name="cfgImapServerCred"></param>
   /// <param name="fetchMailHeaders"></param>
   /// <param name="serilogViewer"></param>
   /// <param name="hierarchyNode"></param>
   /// <param name="cfgImapServerRl"></param>
   /// <param name="cfgImapServerCredAktiv"></param>
   public static void PublishStats(
      Sensitive_AppSettings.CfgImapServer               cfgImapServer
      , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred
      , bool                                        fetchMailHeaders
      , SerilogViewer                               serilogViewer
      , HierarchyNode<ImapHrchyNde>?                rootNode) {
      
      //+ Die Mailbox-Daten lesen
      var offeneFragenStats = AppCfgRuntime.AntwortenVsNeueFragen_Stats;
      /*
         CalcImapStats.Calc_AntwortenVsNeueFragen_Stats
            (cfgImapServer
             , cfgImapServerCred
             , fetchMailHeaders
             , serilogViewer, rootNode);
      */

      //+ Die Statistik in der Konsole ausgeben
      // Create_ImapMailbox_Statistics.PrintFolderStatistics(mbxFoldersGroupByCat, statsSortierung);

      //++ Die Daten exportieren
      ExportData(offeneFragenStats);

      //++ Die Spaltenbreiten justieren
      GoogleSheetApi.AutoResizeDimensions(GoogleSheetMbxStatistics.SpreadsheetId, BlattName);
   }


   /// <summary>
   /// Exportiert die Informationen mit der Statistik pro KW
   /// </summary>
   /// <param name="allFolders"></param>
   public static void ExportData(List<MailboxStatisticsProKW> mbxFoldersGroupByCat) {
      //+ Prepare
      // Ab welchem Datum aktualsieren wir rückwirkend bestehende Daten in GSheets?
      // Berechnung:
      // +1 Woche zusätzlich minus 1 Tag, um die gewünschten KW rückwirkend sicher abzudecken
      var rückwirkendeUpdatesStartDatum = DateTime.Now.AddDays((UpdateRückwirkendAnzWeeks + 1)*7 - 1);

      //+ Die GSheets Header bestimmen, um die Anz. Spalten zu erhalten
      //‼ Das Suffix macht den Titel-Text breiter, danit die automatiusche Spaltenbreite am Ende passt 
      var headerData = TomToolsGSheets.GetHeaderRowRecord<GSheets_Mbx_OffeneFragenProWK_Stats_Rec>("    ");

      //+ Zu GSheet verbinden
      var sheetHelper = GSheets_Mbx_OffeneFragenProWK_Stats_Repo.GetSheetHelper<GSheets_Mbx_OffeneFragenProWK_Stats_Rec>(BlattName);
      var appender    = new SheetAppender(sheetHelper);

      //+ Create a Repository for the TestRecord class
      var repo = new GSheets_Mbx_OffeneFragenProWK_Stats_Repo(sheetHelper);

      //+ Haben wir Daten im GSheet?
      // Die raw-Daten lesen, weil das Lesen via OR-Mapper für diese info knifflig ist 
      // Get all rows from the sheet starting with the 1st row, between the 1st and 8th columns
      // Leaving the last row blank tells Google Sheets to return all rows from the Sheet
      var rawGSheetsData = sheetHelper.GetRows
         (
          new SheetRange
             (BlattName
              , 1
              , 1
              , headerData.Count)
          , SpreadsheetsResource.ValuesResource.GetRequest.ValueRenderOptionEnum.FORMATTEDVALUE
          , SpreadsheetsResource.ValuesResource.GetRequest.DateTimeRenderOptionEnum
                                .FORMATTEDSTRING);

      // Die GSheet Daten via OR-Mapper
      List<GSheets_Mbx_OffeneFragenProWK_Stats_Rec>? existingGSheetsRecords = null;

      //+ GSheets Daten aktualisieren
      //+ Müssen wir die Tabelle neu erzeugen?
      var recreateTable = false;

      //++ Haben wir Zeilen?
      if (rawGSheetsData != null
          && rawGSheetsData.Any()) {
         //+ Die GSheet Daten via OR-Mapper lesen
         try {
            existingGSheetsRecords = repo.GetAllRecords();
         } catch (FormatException) {
            int thisFormatException = 1;
         }
         catch {
            // Im Bereich der Daten keinen Inhalt gefunden
            int otherException = 1;
         }

         //+ Wenn der OR-Mapper keine Daten fand
         // oder der header nicht stimmt
         if (existingGSheetsRecords == null
             || !repo.ValidateSchema().IsValid) {
            //++ Alles neu erzeugen
            // sheetHelper.DeleteRows(1, rawGSheetsData.Count);
            GoogleSheetApi.DeleteSheetData(GoogleSheetMbxStatistics.SpreadsheetId, BlattName);
            /*
            GoogleSheetApi.Sheet_Delete_CellProperties
               (GoogleSheetMbxStatistics.SpreadsheetId
                , BlattName
                , GSheetsFieldMask.GSheetsFlags.DatenLöschen
                  | GSheetsFieldMask.GSheetsFlags.FormatierungLöschen);
            */
            recreateTable = true;
         }
      }
      else {
         // Keine Daten, also alles neu erzeugen
         recreateTable = true;
      }

      //+ Allenfalls die Tabelle neu erzeugen 
      if (recreateTable) {
         //++ Den Kopf schreiben
         appender.AppendRows(new List<List<string>>() { headerData });
      }
      else {
         //+ Die bestehenden GSheets Daten aktualisieren
         if (existingGSheetsRecords != null
             && existingGSheetsRecords.Any()) {
            //++ Bestehende Daten aktualisieren
            //+++ Alle neuen statistischen Daten verarbeiten
            foreach (var statsProKw in mbxFoldersGroupByCat) {
               //+++ Haben wir im Excel einen Datensatz für diese KW?
               var gSheetsRec = existingGSheetsRecords.FirstOrDefault(x => x.Jahr == statsProKw.Jahr && x.KW == statsProKw.KW);

               if (gSheetsRec != null) {
                  // Markieren, dass der Rec nicht mehr exportiert werden muss
                  statsProKw.IsExportedToGoogleSheets = true;

                  // Ist das Datum zu alt, weil wir nicht so weit zurück Daten aändern?
                  if (statsProKw.EndDatumDerKW < rückwirkendeUpdatesStartDatum) {
                     continue;
                  }

                  //+++ Allenfalls die Excel Daten aktualisieren
                  // Das GoogleSheetsWrapper API kann nur einzelne Props aktualisieren,
                  // Es dürfte diese jedoch als Batch-Job tun 
                  if (gSheetsRec.AnzOffeneFragenInKW != statsProKw.AnzNeueFragenInKW) {
                     gSheetsRec.AnzOffeneFragenInKW = statsProKw.AnzNeueFragenInKW;
                     repo.SaveField(gSheetsRec, r => r.AnzOffeneFragenInKW);
                  }
               }
            }
         }
      }

      //+ Die neuen Daten ergänzen
      //++ Alle Daten für die GSheets-Zeilen sammeln
      List<GSheets_Mbx_OffeneFragenProWK_Stats_Rec> newDatenRecords = new List<GSheets_Mbx_OffeneFragenProWK_Stats_Rec>();

      //+++ Alle neuen statistik-RecGSheets_Mbx_OffeneFragenProWK_Stats_Records verarbeiten
      // und zu GSheets übertragen
      foreach (var statsProKw in mbxFoldersGroupByCat) {
         // Existiert der Record bereits in GSheets?
         if (statsProKw.IsExportedToGoogleSheets) {
            continue;
         }

         //+++ E-Mails im Entwurf haben kein Datum bzw. optional ein sehr frühes Datum
         // » Übertragen wir nicht ins Excel
         if (statsProKw.EndDatumDerKW.Year < 1900) {
            continue;
         }

         // KW ohne offene Fragen ausblenden
         if (statsProKw.AnzNeueFragenInKW <= 00) {
            continue;
         }

         newDatenRecords.Add
            (
             new GSheets_Mbx_OffeneFragenProWK_Stats_Rec() {
                Jahr                  = statsProKw.Jahr
                , KW                  = statsProKw.KW
                , EndDatumDerKW       = statsProKw.EndDatumDerKW
                , AnzOffeneFragenInKW = statsProKw.AnzNeueFragenInKW
                , AnzKWPast           = CalcWeekDiff(DateTime.Now, statsProKw.EndDatumDerKW)
             });
      }

      // ‼ Fix für GoogleSheetsWrapper
      // AddRecords() stellt nicht sicher, dass das Sheet genut Spalten & Zeilen hat
      GoogleSheetApi.Assert_GSheets_sheetRowsAndCols
         (GoogleSheetMbxStatistics.SpreadsheetId
          , BlattName
          , mbxFoldersGroupByCat.Count + 2
          , headerData.Count + 1);
      
      //+ Im Excel alle Daten per Batch-Update aktualisieren
      repo.AddRecords(newDatenRecords);

      //+ Header Fett machen
      var anzSpalten                = headerData.Count;
      var batchUpdateRequestObjects = new List<BatchUpdateRequestObject>();

      batchUpdateRequestObjects.Add
         (
          new BatchUpdateRequestObject() {
             // Ab Zeile 1 / Spalte 1 bis zu den anz. Spalten
             Range = new SheetRange
                (GoogleSheetApi.GetA1Notion
                   (""
                    , 1
                    , anzSpalten
                    , 1
                    , 1))
             , Data = new CellData() { UserEnteredFormat = new CellFormat() { TextFormat = new TextFormat() { Bold = true } } }
          });

      // Note the field mask is for both the UserEnteredValue and the UserEnteredFormat below,
      // Nur die Formatierung, nicht die Cellen-Werte ändern!
      // sheetHelper.BatchUpdate(updates, "userEnteredValue, userEnteredFormat");
      sheetHelper.BatchUpdate(batchUpdateRequestObjects, "userEnteredFormat");

      //+ Die GSheet-Spalten formatieren
      var spaltenFormatierungen = TomToolsGSheets.Get_BaseRecord_GSheet_ColFormat(typeof(GSheets_Mbx_OffeneFragenProWK_Stats_Rec));
      GoogleSheetApi.SetColumnFormat(GoogleSheetMbxStatistics.SpreadsheetId, BlattName, spaltenFormatierungen);
   }

}