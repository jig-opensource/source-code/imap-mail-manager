
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using ImapMailManager.Config;
using ImapMailManager.Libs.Text_Spracherkennung.NTextCat;
using ImapMailManager.Mail;
using ImapMailManager.Mail.MimeKit;
using ImapMailManager.Mail.Parser;
using ImapMailManager.Mail.Parser.Kontaktformular;
using ImapMailManager.Mail.Parser.Kontaktformular.Personendaten;
using jig.Tools;
using jig.Tools.DependencySupplierToClient;
using jig.Tools.Exception;
using jig.Tools.ExpressionTree_Tools;
using jig.Tools.IComparer;
using jig.Tools.String;
using MailKit;
using MailKit.Net.Imap;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MimeKit;
using MimeKit.Utils;
using PCRE;
using Standart.Hash.xxHash;
using static jig.Tools.ExtensionMethods_DateTime;
using TeamMitglied = ImapMailManager.Properties.TeamMitglied;


namespace ImapMailManager.BO;

/// <summary>
/// Kapselt eine MailKit / MimeKit Mail
/// und gibt BO-Funktionen
/// </summary>
public class BO_Mail {

   /// <summary>
   ///  True, wenn die Mail verändert wurde
   /// </summary>
   public bool IsDirty { get; internal set; }

   #region MailKit / MimeKit-Props

   public ImapServerRuntimeConfig? ImapSrvRuntimeConfig { get; private set; }

   #region Props in Abbhängigkeit von Msg & MsgSummary

   #region Cache: CachedDependentClientData

   // » Cache: Properties, die von Cache-Supplier abhängig sind, d.h. wenn der Supplier den Wert ändert,
   // dann wird der Wert in CachedDependentClientData ungültig

   #region MailRefNr_InSubject

   /// <summary>
   /// Wird ad hoc gesucht / berechnet: Unsere E-Mail RefNr im Mail Subject
   /// ! Backing Field 
   /// </summary>
   private readonly CachedDependentClientData<string?> _mailRefNr_InSubject;

   private string? MailRefNr_InSubject {
      get => _mailRefNr_InSubject
        .GetCachedValue(() => Get_RefNr_InSubject);
      set => _mailRefNr_InSubject.SetCachedValue(value);
   }

   #endregion MailRefNr_InSubject

   #endregion MailRefNr_InMailBody

   /// <summary>
   /// Wird ad hoc gesucht / berechnet: Unsere E-Mail RefNr im Mail Body 
   /// ! Backing Field 
   /// </summary>
   private readonly CachedDependentClientData<string?> _mailRefNr_InMailBody;

   private string? MailRefNr_InMailBody {
      get => _mailRefNr_InMailBody
        .GetCachedValue( () => Get_RefNr_InMailBody);
      set => _mailRefNr_InMailBody.SetCachedValue(value);
   }

   #endregion MailRefNr_InMailBody

   #region MailFolder: MailFolder_ArchivPath

   /// <summary>
   /// Der Pfad, wo die E-Mail im Archiv abgelegt wird
   /// ‼ Werte:
   /// - Null     Der Pfad konnte nicht berechnet werden, weil MailFolder fehlt
   /// - ""       Die E-Mail kommt nicht ins Archiv und wird gelöscht, z.b. Spam
   /// - Sonst    Der Mailbox-Pfad im Archiv 
   /// ! Backing Field 
   /// </summary>
   private readonly CachedDependentClientData<string?> _mailFolder_ArchivPath;

   private string? MailFolder_ArchivPath {
      get {
         throw new NotImplementedException("Testen der Implementation. Siehe auch: Calc_TriagePath()");

         return _mailFolder_ArchivPath
           .GetCachedValue(() => GetMailFolder_ArchivPath());
      }
      set {
         throw new NotImplementedException("Testen der Implementation. Siehe auch: Calc_TriagePath()");
         _mailFolder_ArchivPath.SetCachedValue(value);
      }
   }

   #endregion MailFolder: MailFolder_ArchivPath

   #region Cache: MsgHdr_Author_AntwortVomTeam

   /// <summary>
   /// Cache für Get_MsgHdr_Author_AntwortVomTeam()
   /// ! Backing Field 
   /// </summary>
   private readonly CachedDependentClientData<BO_Mail_BO_MsgHdr_Author_AntwortVomTeam?> _bo_MsgHdr_Author_AntwortVomTeam;

   private BO_Mail_BO_MsgHdr_Author_AntwortVomTeam? Bo_MsgHdr_Author_AntwortVomTeam {
      get => _bo_MsgHdr_Author_AntwortVomTeam
        .GetCachedValue(() => Get_MsgHdr_Cached_Author_AntwortVomTeam(false));
      set => _bo_MsgHdr_Author_AntwortVomTeam.SetCachedValue(value);
   }


   /// <summary>
   /// Wenn vorhanden, wird der Cached Autor der Antwort zurückgegeben,
   /// Sonst wird er Autor analysiert
   /// </summary>
   /// <returns></returns>
   public BO_Mail_BO_MsgHdr_Author_AntwortVomTeam? Get_MsgHdr_Cached_Author_AntwortVomTeam(bool force = false) {
      //+ Wenn nicht force und der Cache die Daten schon hat, diese zurückgeben
      if (!force
          && _bo_MsgHdr_Author_AntwortVomTeam.HasValidData) { return Bo_MsgHdr_Author_AntwortVomTeam; }

      //+ Force oder wir haben _bo_MsgHdr_Author_AntwortVomTeam noch nicht instanziiert
      TeamMitglied? author_AntwortVomTeam = Analyze_And_AddOrUpdate_MsgHdr_Author_AntwortVomTeam(force);

      //+ Wenn wir dem Autor nicht bestimmen können, sind wir fertig
      if (author_AntwortVomTeam == null) { return null; }

      //+ Den Cache definieren
      Bo_MsgHdr_Author_AntwortVomTeam = new BO_Mail_BO_MsgHdr_Author_AntwortVomTeam(author_AntwortVomTeam, 0);

      return Bo_MsgHdr_Author_AntwortVomTeam;
   }

   #endregion Cache: MsgHdr_Author_AntwortVomTeam

   #region Cache: MailBetreffBackup

   /// <summary>
   /// ! Backing Field 
   /// </summary>
   private readonly CachedDependentClientData<BL_Mail_MsgHeader_StringArray?> _msgHeader_MailBetreffBackup;

   private BL_Mail_MsgHeader_StringArray? MsgHeader_MailBetreffBackup {
      get => _msgHeader_MailBetreffBackup
        .GetCachedValue(() => ObjMsgHeader_MailBetreffBackup);
      set => _msgHeader_MailBetreffBackup.SetCachedValue(value);
   }

   /// <summary>
   /// Das Obj zur Verwaltung der Liste mit den Mail Betreff Backups
   /// </summary>
   public BL_Mail_MsgHeader_StringArray? ObjMsgHeader_MailBetreffBackup {
      get {
         // Wenn wir den Header bereits gelesen haben
         if (_msgHeader_MailBetreffBackup.HasValidData) {
            return MsgHeader_MailBetreffBackup;
         }

         // Haben wir den Header?
         Header? hdr = Search_JigAppHeader_GetSingleton(ConfigMail_MsgHeader.AppMailHeaderType.OriMailBetreffBackup);

         if (hdr != null) {
            MsgHeader_MailBetreffBackup = new BL_Mail_MsgHeader_StringArray(hdr.Value);

            return MsgHeader_MailBetreffBackup;
         }

         return null;
      }
   }

   #endregion Cache: MailBetreffBackup

   #region Cache: Sprache

   /// <summary>
   /// ! Backing Field 
   /// </summary>
   private readonly CachedDependentClientData<CultureInfo?> _mailInhalt_Sprache;

   private CultureInfo? MailInhalt_Sprache {
      get => _mailInhalt_Sprache
        .GetCachedValue( Analyze_Mail_Get_MailInhalt_Sprache);
      set => _mailInhalt_Sprache.SetCachedValue(value);
   }

   #endregion Cache: Sprache

   #endregion » Cache: CachedDependentClientData

   #region Cache: SupplierDataWithTimestamp

   // » Cache: Supplier-Properties:
   // Stellt Daten zur Verfügung
   // Machen die von ihnen abhängige Cache-Daten (CachedDependentClientData) ungültig,
   // sobald der Supplier-Prop-Wert ändert

   /// <summary>
   /// ‼ Supplier mit Timestamp 
   /// </summary>
   private readonly SupplierDataWithTimestamp<IMailFolder?> _mailFolder = new();

   public IMailFolder? MailFolder {
      get => _mailFolder.Value;
      init => _mailFolder.Value = value;
   }

   /// <summary>
   /// ‼ Cached Supplier mit Timestamp 
   /// </summary>
   private readonly SupplierDataWithTimestamp<IMessageSummary?> _msgSummary = new();

   /// <summary>
   /// Wenn die Msg nicht vorhanden ist,
   /// wird versucht, sie automatisch vom Server zu laden
   ///  
   /// !M http://www.mimekit.net/docs/html/T_MailKit_IMessageSummary.htm
   /// </summary>
   public IMessageSummary? MsgSummary {
      get {
         if (!HasMsgSummary) {
            // Versuchen, die Nacchricht zu laden
            Srv_Load_Msg_SummaryData();
         }

         return _msgSummary.Value;
      }
      private set => _msgSummary.Value = value;
   }

   /// <summary>
   /// ‼ Supplier mit Timestamp 
   /// </summary>
   private readonly SupplierDataWithTimestamp<MimeMessage?> _msg = new();

   /// <summary>
   /// Wenn die Msg nicht vorhanden ist,
   /// wird versucht, sie automatisch vom Server zu laden
   ///  
   /// !M http://www.mimekit.net/docs/html/T_MimeKit_MimeMessage.htm
   /// </summary>
   public MimeMessage? Msg {
      get {
         if (!HasMsg) {
            // Versuchen, die Nachricht zu laden
            Srv_Load_Msg_Data();
         }

         // return _msg.IsInitialized ? _msg.Value : null;
         return _msg.Value;
      }
      private set => _msg.Value = value;
   }

   #endregion Cache: SupplierDataWithTimestamp

   /// <summary>
   /// Liefert die eindeutige Message-ID des Msg Headers, z.B.:
   /// Message-ID: CACKWGCLEOLT+tet+p3muwEGv5R4PjfzPNFk6sTTToow2Ujhbew@mail.gmail.com
   /// 
   /// !9 KH The message ID is also used in the header fields:
   /// In-Reply-To:
   /// References:
   ///  
   /// !M https://www.hostknox.com/tutorials/email/headers
   /// </summary>
   public string? MsgHdr_MessageId => MsgSummary?.Envelope.MessageId;

   /// <summary>
   /// In-Reply-To:
   /// this field is only present if the message is a reply to another message.
   /// It contains the Message-ID of the message to which the current email is a reply to
   ///  
   /// !M https://www.hostknox.com/tutorials/email/headers
   /// </summary>
   public string? MsgHdr_InReplyTo => MsgSummary?.Envelope.InReplyTo;

   /// <summary>
   /// The References: field list the message identifiers of the first message and all the replies.
   ///  
   /// !M https://www.hostknox.com/tutorials/email/headers
   /// </summary>
   public MessageIdList? MsgHdr_References => Msg?.References;

   /// <summary>
   /// Berechnet den Hash vom Message Body
   /// - Vom Text Body einfach die Checksumme
   /// - Vom Html Body nur von der HTML Body Node die Checksomme vom in Text konvertierten Text,
   ///   aus dem alle White-Spaces auf 1 reduziert wurde   
   /// </summary>
   public ulong MsgBody_Hash {
      get {
         var    mail_Body_TextOnly = Get_Mail_Body_TextOnly(false);
         byte[] data               = Encoding.UTF8.GetBytes(mail_Body_TextOnly);

         return xxHash64.ComputeHash(data, data.Length);
      }
   }

   #region Basis-Props

   public bool HasImapSrvRuntimeConfig => ImapSrvRuntimeConfig != null;
   public bool HasMailFolder           => MailFolder != null;
   public bool HasMsgSummary           => _msgSummary.Value != null;
   /// <summary>
   /// MsgSummary.Headers ist nicht null und ein {}
   /// </summary>
   public bool HasMsgSummaryHeaders => MsgSummary is { Headers: {} };
   public bool HasMsg => _msg.Value != null;

   #endregion Basis-Props

   #region Berechnete Basis-Props

   /// <summary>
   /// Liefert true, wenn es sich um irgend eine Art App-Spezifische E-Mail handelt
   /// Prüft aktuell nur die Doc Mails 
   /// </summary>
   public bool IsAppSpecificMail => IsAppSpecificMail_DocMail;

   /// <summary>
   /// Liefert True, wenn es eine App-spezifische E-Mail ist:
   /// Doc E-Mail
   /// </summary>
   public bool IsAppSpecificMail_DocMail => Mail_Subject.ØStartsWith
      (ConfigMail_Basics.AppSpecMail_Doc_SubjectStart, StringComparison.OrdinalIgnoreCase);

   #endregion Berechnete Basis-Props

   #region Metadaten

   #region Mail Header: Msg Hdr

   #region HasHeaderSet_...

   #region kombinierte / logische Props

   /// <summary>
   /// Wurden in dieser E-Mail die Metadaten gesetzt?
   /// Prüft Msg.Headers und MsgSummary.Headers
   /// </summary>
   /// <returns></returns>
   public bool HasHeaderSet_Metadaten =>

      // Eines muss gesetzt sein, wenn die Metadaten berechnet wurden
      HasHeaderSet_MailKontaktformularDatenFound || HasHeaderSet_MailKontaktformularDatenMissing;

   #endregion kombinierte / logische Props

   /// <summary>
   /// Hat die Mail den AppMailHeader?
   /// Prüft Msg.Headers und MsgSummary.Headers
   /// </summary>
   public bool HasHeaderSet_TriageOrdner => HasHeaderSet(ConfigMail_MsgHeader.AppMailHeaderType.TriageOrdner);

   /// <summary>
   /// Hat die Mail den AppMailHeader?
   /// Prüft Msg.Headers und MsgSummary.Headers
   /// </summary>
   public bool HasHeaderSet_MailRefNr => HasHeaderSet(ConfigMail_MsgHeader.AppMailHeaderType.MailRefNr);

   /// <summary>
   /// Hat die Mail den AppMailHeader?
   /// Prüft Msg.Headers und MsgSummary.Headers
   /// </summary>
   public bool HasHeaderSet_TriageHashtags => HasHeaderSet(ConfigMail_MsgHeader.AppMailHeaderType.TriageHashtags);

   /// <summary>
   /// Hat die Mail den AppMailHeader?
   /// Prüft Msg.Headers und MsgSummary.Headers
   /// </summary>
   public bool HasHeaderSet_FragestellerPersoenlicheDaten => HasHeaderSet
      (ConfigMail_MsgHeader.AppMailHeaderType.FragestellerPersoenlicheDaten);

   /// <summary>
   /// Hat die Mail den AppMailHeader?
   /// Prüft Msg.Headers und MsgSummary.Headers
   /// </summary>
   public bool HasHeaderSet_MailKontaktformularDatenFound => HasHeaderSet
      (ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularDatenFound);

   /// <summary>
   /// Hat die Mail den AppMailHeader?
   /// Prüft Msg.Headers und MsgSummary.Headers
   /// </summary>
   public bool HasHeaderSet_MailKontaktformularThema => HasHeaderSet
      (ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularThema);

   /// <summary>
   /// Hat die Mail den AppMailHeader?
   /// Prüft Msg.Headers und MsgSummary.Headers
   /// </summary>
   public bool HasHeaderSet_MailKontaktformularDatenMissing => HasHeaderSet
      (ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularDatenMissing);

   /// <summary>
   /// Hat die Mail den AppMailHeader?
   /// Prüft Msg.Headers und MsgSummary.Headers
   /// </summary>
   public bool HasHeaderSet_MailInhaltAnonymisiert => HasHeaderSet
      (ConfigMail_MsgHeader.AppMailHeaderType.MailInhaltAnonymisiert);

   /// <summary>
   /// Hat die Mail den AppMailHeader?
   /// Prüft Msg.Headers und MsgSummary.Headers
   /// </summary>
   public bool HasHeaderSet_SpracheMailInhalt => HasHeaderSet
      (ConfigMail_MsgHeader.AppMailHeaderType.SpracheMailInhalt);

   /// <summary>
   /// Hat die Mail den AppMailHeader?
   /// Prüft Msg.Headers und MsgSummary.Headers
   /// </summary>
   public bool HasHeaderSet_MailBetreffWurdeBereinigt => HasHeaderSet
      (ConfigMail_MsgHeader.AppMailHeaderType.MailBetreffWurdeBereinigtDate);

   /// <summary>
   /// Der Msg Header hat das Feld
   /// AppMailHeaderType.MailKategorie
   /// </summary>
   public bool HasHeaderSet_MailKategorie => HasHeaderSet
      (ConfigMail_MsgHeader.AppMailHeaderType.MailKategorie);

   #endregion HasHeaderSet_...

   #region Doc: Das Konzept

   //» Doc Konzept

   /*
    
      // Holt die Metadaten, egal, ob sie in der Mail im Header oder Body gespeichert sind
      // Ruft auf: Get_MsgHdr_xxx
      public Get_Metadata_xxx
      
      // Holt vom Msg Header / Body die Metadaten
      // Wenn die Metadaten nicht bekannt sind, dann werden sie berechnet
      // Ruft auf: Analyze_And_AddOrUpdate_MsgHdr_xxx
      internal Get_MsgHdr_xxx

      // Analysiert und aktualisiert allenfalls den MsgHdr
      // Liefert die Metadaten
      internal Analyze_And_AddOrUpdate_MsgHdr_xxx
    
    */

   #endregion Doc: Das Konzept

   #region AppMailHeaderType.MailKontaktformularThema

   /// <summary>
   /// Liest aus der Mail das Kontaktformular-Thema.
   /// Wenn es unbekannt ist, dann wird es berechnet
   /// </summary>
   /// <returns></returns>
   string? Get_Metadata_MailKontaktformularThema(bool saveOnDirty) {
      var res = Analyze_and_AddOrUpdate_MsgHdr_MailKontaktformularThema();

      if (saveOnDirty) { Save(); }

      return res;
   }

   #endregion AppMailHeaderType.MailKontaktformularThema

   #region AppMailHeaderType.Author_AntwortVomTeam

   #region Analyse des Autors einer Antwort

   /// <summary>
   /// Die tiefe der Erkennung der Authoren 
   /// </summary>
   public enum DetectAuthor_Level {

      /// <summary>
      /// Stufe 1: Analysiert den Anzeigenamen der Sender E-Mail Adresse
      /// </summary>
      Lvl1_By_SenderDisplayName = 1,

      /// <summary>
      /// Stufe 2: Analysiert die E-Mail Signatur
      /// </summary>
      Lvl2_By_RgxMailSignatur = 2,

      /// <summary>
      /// Stufe 3: Sucht via RegEx Vorname Nachname 
      /// </summary>
      Lvl3_By_RgxVornameNachname = 3

   }


   /// <summary>
   /// Alle Regex mit den Team-Mitglieder Namen,
   /// um sie in Sender: From: DisplayNamen von E-Mail Adressen zu erkennen
   /// </summary>
   private static readonly Lazy<Dictionary<string, List<PcreRegex>>> AllRgx_EMailDisplayName_TeamSender
      = new(Init_AllRgx_EMailDisplayName_TeamSender);


   /// <summary>
   /// Initialisiert das Dict AllRgx_EMailDisplayName_TeamSender
   /// </summary>
   /// <returns></returns>
   private static Dictionary<string, List<PcreRegex>> Init_AllRgx_EMailDisplayName_TeamSender() {
      // !KH9'9
      // i » PcreOptions.IgnoreCase 
      // n » PcreOptions.ExplicitCapture 
      // x » PcreOptions.IgnorePatternWhitespace 
      // m » PcreOptions.MultiLine   »  ^$ matches line breaks 
      // s » PcreOptions.Singleline  »   . matches line breaks 

      #region rgxEMailDisplayName_TeamSender

      PcreOptions rgxOpt_EMailDisplayName_TeamSender =
         PcreOptions.Compiled
         | PcreOptions.IgnoreCase
         | PcreOptions.Caseless
         | PcreOptions.IgnorePatternWhitespace
         | PcreOptions.ExplicitCapture

         // | PcreOptions.MultiLine
         // | PcreOptions.Singleline
         | PcreOptions.Unicode;

      string sRgxEMailDisplayName_TeamSender = @"
                 ## (Vorname Nachname)
                 (?<KlammerVornameNachname>\(\p{Zs}*{Vorname}[, \t]+{Nachname})
                 |
                 ## (Nachname Vorname)
                 (?<KlammerNachnameVorname>\(\p{Zs}*{Nachname}[, \t]+{Vorname})
                 |
                 ## (Vorname
                 (?<KlammerVorname>\(\p{Zs}*{Vorname})
                 |
                 ## (Nachname
                 (?<KlammerNachname>\(\p{Zs}*{Nachname})
                 |
                 ## ^Vorname Nachname
                 (?<VornameNachname>^\p{Zs}*{Vorname}[, \t]+{Nachname})
                 |
                 ## ^Nachname Vorname
                 (?<NachnameVorname>^\p{Zs}*{Nachname}[, \t]+{Vorname})";

      #endregion rgxEMailDisplayName_TeamSender

      Dictionary<string, List<PcreRegex>> resRegexes = new Dictionary<string, List<PcreRegex>>();

      foreach (var teamMitglied in App.Cfg_TeamConfig.TeamConfig.TeamMitglieder) {
         //+ Das Dict initialisieren
         if (!resRegexes.ContainsKey(teamMitglied.Kuerzel)) {
            resRegexes.Add(teamMitglied.Kuerzel, new List<PcreRegex>());
         }

         //++ Den Nachnamen aufsplitten
         var nachnameItems = teamMitglied.Nachname.Split('-');

         //+ Der sRgxEMailDisplayName_TeamSender

         #region sRgxEMailDisplayName_TeamSender

         if (nachnameItems.Length > 1) {
            foreach (var nachnameItem in nachnameItems) {
               var strRgxLoop = sRgxEMailDisplayName_TeamSender.Replace("{Nachname}", nachnameItem).Replace
                  ("{Vorname}", teamMitglied.Vorname);
               PcreRegex oRgxLoop = new PcreRegex(strRgxLoop, rgxOpt_EMailDisplayName_TeamSender);
               resRegexes[teamMitglied.Kuerzel].Add(oRgxLoop);
            }
         }

         var strRgx = sRgxEMailDisplayName_TeamSender.Replace("{Nachname}", teamMitglied.Nachname).Replace
            ("{Vorname}", teamMitglied.Vorname);
         var oRgx = new PcreRegex(strRgx, rgxOpt_EMailDisplayName_TeamSender);
         resRegexes[teamMitglied.Kuerzel].Add(oRgx);

         #endregion sRgxEMailDisplayName_TeamSender

      }

      return resRegexes;
   }


   /// <summary>
   /// Sucht im ReplyTo Msg Header nach der E-Mail Adresse eines Team-Mitglieds
   /// </summary>
   /// <returns></returns>
   public TeamMitglied? Find_Msg_TeamAntwort_Author_In_ReplyTo_Address() {
      // Alle Autoren durchlaufen
      for (var idx = 0; idx < App.Cfg_TeamConfig.TeamConfig.TeamMitglieder.Count; idx++) {
         var teamMitglied = App.Cfg_TeamConfig.TeamConfig.TeamMitglieder[idx];

         //++ Haben wir eine bekannte ReplyTo Adresse?
         if (MsgSummary.Envelope.ReplyTo.Any
                (x => ((MailboxAddress)x).Address.ØEqualsIgnoreCase(teamMitglied.AntwortEMailAddress))) {
            return teamMitglied;
         }

         foreach (var regex in AllRgx_EMailDisplayName_TeamSender.Value[teamMitglied.Kuerzel]) {
            if (MsgSummary.Envelope.ReplyTo.Any
                   (x => x.Name != null && regex.Match(x.Name).Success)) {
               return teamMitglied;
            }
         }

      }

      return null;
   }


   /// <summary>
   /// Sucht im From: und Sender: Msg Header nach dem Namen eines Team-Mitglieds
   /// </summary>
   /// <returns></returns>
   public TeamMitglied? Find_Msg_TeamAntwort_Author_In_SenderMail_Displayname() {
      // Alle Autoren durchlaufen
      for (var idx = 0; idx < App.Cfg_TeamConfig.TeamConfig.TeamMitglieder.Count; idx++) {
         var teamMitglied = App.Cfg_TeamConfig.TeamConfig.TeamMitglieder[idx];

         //++ Ist ein bekannter Name in der Namen im From AnzeigeNamen?
         foreach (var regex in AllRgx_EMailDisplayName_TeamSender.Value[teamMitglied.Kuerzel]) {
            if (MsgSummary.Envelope.From.Any
                   (x => x.Name != null && regex.Match(x.Name).Success)) {
               return teamMitglied;
            }
         }

         //+ Haben wir den Namen im Sender AnzeigeNamen?
         foreach (var regex in AllRgx_EMailDisplayName_TeamSender.Value[teamMitglied.Kuerzel]) {
            if (MsgSummary.Envelope.Sender.Any
                   (x => x.Name != null && regex.Match(x.Name).Success)) {
               return teamMitglied;
            }
         }
      }

      return null;
   }


   /// <summary>
   /// Analysiert die E-Mail um den Autor herauszufinden
   /// Speichert den gefundenen Autor im Msg Header
   /// </summary>
   /// <returns>
   /// var (erkennungsZuverlässigkeitProzent, teamMitgliedAutor?) = Analyze_MsgHdr_Author_AntwortVomTeam()
   /// teamMitgliedAutor:
   /// null    Der Autor konnte nicht erkannt werden 
   /// </returns>
   public Tuple<int, TeamMitglied?> Analyze_MsgHdr_Author_AntwortVomTeam(bool force = false) {
      // 🚩 Debug
      bool isDebug = false;

      //+ Die Mail-Kategorie bestimmen (sie wird bei Bedarf allenfalls bestimmt)
      var currMsgHdrCfg = Get_MsgHdr_MailKategorie();

      //++ Abbrechen, wenn:
      // - Wir keine Kategorie haben
      // - Wenn die Kategorie nicht AntwortVomTeam ist
      if (currMsgHdrCfg is ConfigMail_MsgHeader.MailKategorie.Unknown
          or not ConfigMail_MsgHeader.MailKategorie.AntwortVomTeam) {
         return new(0, null);
      }

      #region Debug

      if (isDebug) {
         var replyToMail = String.Join
            (", ", MsgSummary?.Envelope.ReplyTo.Select(x => ((MailboxAddress)x).Address).ToList() ?? new List<string>());

         var replyToDisplayName = String.Join
            (", ", MsgSummary?.Envelope.ReplyTo.Select(x => x.Name).ToList() ?? new List<string>());

         var fromMail = String.Join
            (", ", MsgSummary?.Envelope.From.Select(x => ((MailboxAddress)x).Address).ToList() ?? new List<string>());
         var fromDisplayName = String.Join(", ", MsgSummary?.Envelope.From.Select(x => x.Name).ToList() ?? new List<string>());

         var senderMail = String.Join
            (", ", MsgSummary?.Envelope.Sender.Select(x => ((MailboxAddress)x).Address).ToList() ?? new List<string>());

         var senderDisplayName = String.Join
            (", ", MsgSummary?.Envelope.Sender.Select(x => x.Name).ToList() ?? new List<string>());

         Debug.WriteLine($"replyToMail       : {replyToMail}");
         Debug.WriteLine($"replyToDisplayName: {replyToDisplayName}");
         Debug.WriteLine($"fromMail          : {fromMail}");
         Debug.WriteLine($"fromDisplayName   : {fromDisplayName}");
         Debug.WriteLine($"senderMail        : {senderMail}");
         Debug.WriteLine($"senderDisplayName : {senderDisplayName}");
      }

      #endregion Debug

      //+ Stufe 1:
      // - Haben wir eine persönliche Team Antwort Adresse?
      // - Oder aben wir den Autor im Anzeigenamen der Send: From: EMail Adresse?
      var teamAutor = Find_Msg_TeamAntwort_Author_In_ReplyTo_Address() ?? Find_Msg_TeamAntwort_Author_In_SenderMail_Displayname();

      if (teamAutor != null) {
         //++ Gefunden
         return new(100, teamAutor);
      }

      //+ Stufe 2: Erkennen wir den Autor im Text der EMail-Signatur?
      (teamAutor, var erkennungsZuverlässigkeitProzentLvl2)
         = BO_Mail_BL_Detect_Author_AntwortVomTeam.Detect_Author_By_RgxMailSignatur(this);

      if (teamAutor != null) {
         //+++ Gefunden
         return new(erkennungsZuverlässigkeitProzentLvl2, teamAutor);
      }

      //+ Stufe 3: Erkennen des Autors mit dem Muster Vorname Nachname
      (teamAutor, var erkennungsZuverlässigkeitProzentLvl3)
         = BO_Mail_BL_Detect_Author_AntwortVomTeam.Detect_Author_By_RgxVornameNachname(this);

      if (teamAutor != null) {
         //+++ Gefunden
         return new(erkennungsZuverlässigkeitProzentLvl3, teamAutor);
      }

      return new(0, null);
   }

   #endregion Analyse des Autors einer Antwort

   /// <summary>
   /// Der Msg Header hat das Feld
   /// AppMailHeaderType.Author_AntwortVomTeam
   /// </summary>
   public bool HasHeaderSet_Author_AntwortVomTeam => HasHeaderSet
      (ConfigMail_MsgHeader.AppMailHeaderType.Author_AntwortVomTeam);


   /// <summary>
   /// Liest aus der Mail den Autor der Antwort
   /// Wenn er unbekannt ist, dann wird es berechnet
   /// </summary>
   /// <returns></returns>
   TeamMitglied? Get_Metadata_Author_AntwortVomTeam(bool saveOnDirty) {
      var res = Analyze_And_AddOrUpdate_MsgHdr_Author_AntwortVomTeam();

      if (saveOnDirty) { Save(); }

      return res;
   }

   #endregion AppMailHeaderType.Author_AntwortVomTeam

   #region Original Mail-Betreff Backup

   /// <summary>
   /// Hat die Mail den AppMailHeader?
   /// Prüft Msg.Headers und MsgSummary.Headers
   /// </summary>
   public bool HasHeaderSet_OriMailBetreffBackup => HasHeaderSet
      (ConfigMail_MsgHeader.AppMailHeaderType.OriMailBetreffBackup);


   /// <summary>
   /// Holt vom Msg Hdr OriMailBetreffBackup die Backups des Betreffs
   /// </summary>
   /// <returns></returns>
   public ImmutableList<string> Get_MailBetreffBackup() {
      if (ObjMsgHeader_MailBetreffBackup == null) {
         return new List<string>().ToImmutableList();
      }

      return ObjMsgHeader_MailBetreffBackup.Daten.ToImmutableList();
   }


   /// <summary>
   /// Ergänzt den Msg Header um ein Backup von subject, wenn es noch nicht im Backup ist  
   /// </summary>
   /// <param name="subject"></param>
   /// <returns></returns>
   public bool Create_MailBetreffBackup(string subject) {
      if (!subject.ØHasValue()) { return false; }

      bool hasChanged = false;

      // Das Obj wurde noch nicht initialisiert
      if (ObjMsgHeader_MailBetreffBackup == null) {
         Header? hdr = Search_JigAppHeader_GetSingleton(ConfigMail_MsgHeader.AppMailHeaderType.OriMailBetreffBackup);

         // Wenn wir den Msg Hdr schon haben
         if (hdr != null) {
            // Das Obj mit ihm initialisieren
            MsgHeader_MailBetreffBackup = new BL_Mail_MsgHeader_StringArray(hdr.Value);
            hasChanged                  = MsgHeader_MailBetreffBackup.AddString(subject);
         }
         else {
            // Das Obj mit dem Subject initialisieren
            MsgHeader_MailBetreffBackup = new BL_Mail_MsgHeader_StringArray(subject);
            hasChanged                  = true;
         }
      }
      else {
         hasChanged = ObjMsgHeader_MailBetreffBackup.AddString(subject);
      }

      if (hasChanged) {
         hasChanged = MsgHdr_UpdateOrSet_JigAppHeader
            (ConfigMail_MsgHeader.AppMailHeaderType.OriMailBetreffBackup, ObjMsgHeader_MailBetreffBackup.DatenDelimted);
      }

      return hasChanged;
   }

   #endregion Original Mail-Betreff Backup


   /// <summary>
   /// Liefert True, wenn der Application Msg Header appMailHeaderType
   /// gesetzt ist
   /// </summary>
   /// <param name="appMailHeaderType"></param>
   /// <returns></returns>
   public bool HasHeaderSet(ConfigMail_MsgHeader.AppMailHeaderType appMailHeaderType) {
      Header? hdr = Search_JigAppHeader_GetSingleton(appMailHeaderType);

      return hdr != null;
   }

   #endregion Mail Header: Msg Hdr

   #region Metadaten: Msg hdr

   #region Analyze_and_AddOrUpdate_MsgHdr_...

   #region Generische Funktionen

   /// <summary>
   /// Generische Methode um Msg Header AppMailHeaderType zu aktualisieren:
   /// - Prüft, ob der Msg Hdr bereits einen Wert für AppMailHeaderType hat
   /// - Wenn ja und ohne force, dann sind wird fertig
   /// - Wenn force oder der Wert fehlt, dann wird der Wert berechnet
   /// - Wenn der Wert anders als ein allenfalls bereits gespeicherter Wert ist,
   ///   dann wird der Msg Header aktualisiert 
   /// </summary>
   /// <param name="appMailHeaderType"></param>
   /// <param name="analyzeFunc"></param>
   /// <param name="force"></param>
   /// <returns>
   /// Null    Daten nicht vorhanden
   /// Sonst   Die Daten
   /// </returns>
   private string? AddOrUpdate_MailMsgHdr_AppMetadata(
      ConfigMail_MsgHeader.AppMailHeaderType appMailHeaderType
      , Func<string?>                        analyzeFunc
      , bool                                 force = false) {

      //+ Die Msg Hdr Daten lesen
      Header? currMailMetadata = Search_JigAppHeader_GetSingleton(appMailHeaderType);

      //+ Kein force und die Daten sind vorhanden: fertig
      if (!force && currMailMetadata != null && currMailMetadata.Value.ØHasValue()) {
         return currMailMetadata.Value;
      }

      //+ Die Daten bestimmen
      var analyzedMetadata = analyzeFunc();

      //+ Wenn wir keine Daten haben, sind wir fertig 
      if (!analyzedMetadata.ØHasValue()) { return null; }

      //+ Wenn wir schon die aktuellen Daten haben, dann sind wir fertig  
      if (currMailMetadata != null && currMailMetadata.Value.ØEqualsIgnoreCase(analyzedMetadata)) {
         return currMailMetadata.Value;
      }

      //+ Den Header setzen / aktualisieren
      MsgHdr_UpdateOrSet_JigAppHeader(appMailHeaderType, analyzedMetadata!);

      return analyzedMetadata;
   }


   /// <summary>
   /// Prüft den Wert von Metadaten und aktualisiert allenfalls den Header
   /// Liefert die gefundenen Metadaten zurück oder den Default-Wert
   /// </summary>
   /// <param name="appMailHeaderType">Der Metadaten-Typ, den wir prüfen / aktualisieren</param>
   /// <param name="analyzeFunc">
   /// Die Funktion, die die Metadaten berechnet, wenn sie fehlen 
   /// </param>
   /// <param name="deserialize">
   /// Die Funktion, die den Msg Hdr String den Typ der Metadaten konvertiert
   /// </param>
   /// <param name="serialize">
   /// Die Funktion, die den Metadaten-Typ in den Msg Hdr String konvertiert
   /// </param>
   /// <param name="comparer">
   /// Der IComparer, der erlaubt, die analysierten Metadaten mit den aktuellen Metadten im Msg Hdr zu vergleichen
   /// </param>
   /// <param name="defaultValue">
   /// Wenn keine Metadaten gefunden werden: Welcher Wert soll zurückgegeben werden?
   /// </param>
   /// <param name="force">
   /// Wenn true, werden die Metadaten berechent, auch wenn der Msg Hdr diese bereits hätte
   /// </param>
   /// <typeparam name="T"></typeparam>
   /// <returns></returns> 
   private T? AddOrUpdate_MailMsgHdr_AppMetadata<T>(
         ConfigMail_MsgHeader.AppMailHeaderType appMailHeaderType
         , Func<T?>                             analyzeFunc
         , Func<string?, T?>                    deserialize
         , Func<T?, string?>                    serialize
         , IComparer<T?>                        comparer
         , T                                    defaultValue
         , bool                                 force = false)

      // Explizit / Redundant Enum angegeben, um's klar zu deklarieren
      where T : struct, System.Enum {

      //+ Die Msg Hdr Daten lesen
      Header? strCurrMailMetadata = Search_JigAppHeader_GetSingleton(appMailHeaderType);
      T?      currMailMetadata    = deserialize(strCurrMailMetadata?.Value);

      //+ Kein force und die Daten sind vorhanden: fertig
      if (!force && strCurrMailMetadata != null && strCurrMailMetadata.Value.ØHasValue()) {
         return currMailMetadata;
      }

      //+ Die Daten bestimmen
      T? analyzedMetadata = analyzeFunc();

      //+ Wenn wir keine Daten haben, sind wir fertig 
      if (analyzedMetadata == null) { return defaultValue; }

      //+ Wenn wir schon die aktuellen Daten haben, dann sind wir fertig  
      if (comparer.Compare(currMailMetadata, analyzedMetadata) == 0) {
         return currMailMetadata;
      }

      //+ Den Header setzen / aktualisieren
      MsgHdr_UpdateOrSet_JigAppHeader(appMailHeaderType, serialize(analyzedMetadata!)!);

      return analyzedMetadata;
   }

   #endregion Generische Funktionen


   /// <summary>
   /// Setzt den Msg Hdr MailBetreffWurdeBereinigtDate aufs aktuelle Datum,
   /// wenn die E-Mail nach dem Datum in MailBetreffWurdeBereinigt empfangen wurde
   /// </summary>
   /// <param name="force"></param>
   private void Analyze_And_AddOrUpdate_MsgHdr_MailBetreffWurdeBereinigtDate(bool force) {
      //+ Kein Force und nicht veralteter Wert: Wir sind fertig
      if (!force && !Is_MsgHdr_SubjectBackup_Outdated()) { return; }

      //+ Den Header setzen / aktualisieren
      // Den Zeitstempel als String
      MsgHdr_UpdateOrSet_JigAppHeader
         (ConfigMail_MsgHeader.AppMailHeaderType.MailBetreffWurdeBereinigtDate, DateUtils.FormatDate(DateTimeOffset.Now));
   }


   /// <summary>
   /// Aktualisiert, wenn nötig, den Msg Header: AppMailHeaderType.TriageHashtags
   /// Liefert die Daten zurück
   /// </summary>
   /// <returns>
   /// Null    Daten nicht vorhanden
   /// Sonst   Die Daten
   /// </returns> 
   internal List<string>? Analyze_and_AddOrUpdate_MsgHdr_TriageHashtags(bool force = false) {
      //+ Die Msg Hdr Daten lesen
      Header? strCurrMailMetadata = Search_JigAppHeader_GetSingleton(ConfigMail_MsgHeader.AppMailHeaderType.TriageHashtags);
      var     currMailMetadata    = TriageHashtags_Deserialize(strCurrMailMetadata?.Value);

      //+ Kein force und die Daten sind vorhanden: fertig
      if (!force && strCurrMailMetadata != null && strCurrMailMetadata.Value.ØHasValue()) {
         return currMailMetadata;
      }

      //+ Die Daten bestimmen
      List<string> analyzedMetadata = Analyze_TriageHashTags();

      //+ Wenn wir keine Daten haben, sind wir fertig 
      if (analyzedMetadata.Count == 0) { return new(); }

      //+ Wenn wir schon die aktuellen Daten haben, dann sind wir fertig  
      if (new ListStringComparer().Compare(currMailMetadata, analyzedMetadata) == 0) {
         return currMailMetadata;
      }

      //+ Den Header setzen / aktualisieren
      Set_MsgHdr_TriageHashtags(analyzedMetadata);

      return analyzedMetadata;
   }


   /// <summary>
   /// Aktualisiert, wenn nötig, den Msg Header: AppMailHeaderType.TriageOrdner
   /// Liefert die Daten zurück
   /// </summary>
   /// <returns>
   /// Null    Daten nicht vorhanden
   /// Sonst   Die Daten
   /// </returns>
   internal string? Analyze_and_AddOrUpdate_MsgHdr_TriageOrdner(bool force = false)
      => AddOrUpdate_MailMsgHdr_AppMetadata
         (ConfigMail_MsgHeader.AppMailHeaderType.TriageOrdner, Analyze_Mail_Get_TriagePath, force);


   /// <summary>
   /// Aktualisiert, wenn nötig, den Msg Header: AppMailHeaderType.MailRefNr
   /// Liefert die Daten zurück
   /// </summary>
   /// <returns>
   /// Null    Daten nicht vorhanden
   /// Sonst   Die Daten
   /// </returns>
   internal string? Analyze_and_AddOrUpdate_MsgHdr_MailRefnr(bool force = false)
      => AddOrUpdate_MailMsgHdr_AppMetadata(ConfigMail_MsgHeader.AppMailHeaderType.MailRefNr, Analyze_Mail_Get_MailRefNr, force);


   /// <summary>
   /// Analysiert die E-Mail und aktualisiert bei Bedarf die Personen-Daten des Fragestellers
   /// im Msg Header
   /// 
   /// Wenn die Nachricht verändert wurde, wird sie gespeichert
   /// 
   /// </summary>
   /// <param name="saveOnDirty"></param>
   /// <param name="force">
   /// Analysiert den E-Mail Body und die Mail-Props (To, Sender, …) wieder neu
   /// und setzt die gefundenen Personen-Daten wieder im Msg Header 
   /// </param>
   /// <exception cref="RuntimeException"></exception>
   internal RecFragestellerV01? Analyze_and_AddOrUpdate_MsgHdr_FragestellerPersoenlicheDaten(
      bool   saveOnDirty = false
      , bool force       = false) {

      // hier weiter

      //+ haben wir im Header die Personendaten des Fragestellers?
      var currRecFragesteller                   = Get_MsgHdr_Encrypted_Fragesteller_Daten();
      var hasCurr_Fragesteller_PersönlicheDaten = currRecFragesteller != null;

      //+ Wenn wir die Daten schon haben und sie nicht neu berechnen sollen
      if (!force && hasCurr_Fragesteller_PersönlicheDaten) {
         return currRecFragesteller;
      }

      //+ Daten fehlen oder forciert neu rechnen
      //++ Den Mail-Body analysieren  
      if (!HasMsg) {
         //++ Können wir die Daten vom Server lesen?
         if (CanGetServerData) {
            Srv_Load_Msg_Data();
         }
         else {
            throw new RuntimeException
               ("public void Analyze_and_UpdateOrSet_Fragesteller_PersönlicheDaten(bool force): Weder MsgSummary noch Msg sind vohanden");
         }
      }

      //+ Die Mail Daten holen
      var newRec_Kontaktformular_Fragesteller_Daten = Analyze_Mail_Get_FragestellerPersoenlicheDaten();

      //+ Sind die neuen Daten anders als die bestehenden?
      if (currRecFragesteller != null
          && currRecFragesteller.Get_DataAsString().Equals
             (newRec_Kontaktformular_Fragesteller_Daten.Get_DataAsString())) {
         // Wir müssen nichts ändern
         return currRecFragesteller;
      }

      //+ Daten aktualisieren und allenfalls speichern
      UpdateOrSet_Fragesteller_PersönlicheDaten
         (newRec_Kontaktformular_Fragesteller_Daten);

      if (saveOnDirty && IsDirty) { Save(); }

      return newRec_Kontaktformular_Fragesteller_Daten;

   }


   /// <summary>
   /// Aktualisiert, wenn nötig, den Msg Header: AppMailHeaderType.MailKontaktformularThema
   /// Liefert die Daten zurück
   /// </summary>
   /// <returns>
   /// Null    Daten nicht vorhanden
   /// Sonst   Die Daten
   /// </returns>
   internal string? Analyze_and_AddOrUpdate_MsgHdr_MailKontaktformularThema(bool force = false)
      => AddOrUpdate_MailMsgHdr_AppMetadata
         (ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularThema, Analyze_Mail_Get_Kontaktformular_Thema, force);


   /// <summary>
   /// Analysiert die E-Mail und sucht das Thema, das der User im Kontaktformular gewählt hat
   /// Wenn es unbekannt ist, dann wird es analysiert
   /// </summary>
   /// <returns></returns>
   private string Analyze_Mail_Get_Kontaktformular_Thema() {
      return ParseKontaktformular_Thema.Parse_Kontaktformular_Thema(this);
   }


   /// <summary>
   /// Sucht den Msg Header: AppMailHeaderType.Author_AntwortVomTeam
   /// Analysiert allenfalls die Mail
   /// Setzt den Cache
   /// Liefert die Daten zurück
   /// </summary>
   /// <returns>
   /// Null    Daten nicht vorhanden
   /// Sonst   Die Daten
   /// </returns>
   internal TeamMitglied? Analyze_And_AddOrUpdate_MsgHdr_Author_AntwortVomTeam(bool force = false) {
      //+ Die Msg Hdr Daten lesen
      BO_Mail_BO_MsgHdr_Author_AntwortVomTeam? currMailMetadata = null;
      if (!force && _bo_MsgHdr_Author_AntwortVomTeam.HasValidData) {
         currMailMetadata = Bo_MsgHdr_Author_AntwortVomTeam; 
      }

      //+ Kein force und die Daten sind vorhanden: fertig
      if (!force && currMailMetadata != null) {
         return currMailMetadata.TeamAutor;
      }

      //+ Die Daten bestimmen
      var (erkennungsZuverlässigkeitProzent, detectedTeamMitgliedAutor) = Analyze_MsgHdr_Author_AntwortVomTeam(force);

      //+ Wenn wir keine Daten haben, sind wir fertig 
      if (detectedTeamMitgliedAutor == null) { return null; }

      //+ Wenn wir schon die aktuellen Daten haben, dann sind wir fertig
      var bo_Mail_Bo_MsgHdr_Author_AntwortVomTeam = new BO_Mail_BO_MsgHdr_Author_AntwortVomTeam
         (detectedTeamMitgliedAutor, erkennungsZuverlässigkeitProzent);

      // Wenn die Daten identisch sind, haben wir nichts zu tun
      if (currMailMetadata != null
          && currMailMetadata.Serialize().ØEqualsIgnoreCase(bo_Mail_Bo_MsgHdr_Author_AntwortVomTeam.Serialize())) {
         return currMailMetadata.TeamAutor;
      }

      //+ Den Header setzen / aktualisieren
      MsgHdr_UpdateOrSet_JigAppHeader
         (ConfigMail_MsgHeader.AppMailHeaderType.Author_AntwortVomTeam, bo_Mail_Bo_MsgHdr_Author_AntwortVomTeam.Serialize());

      //+ Den Cache setzen
      Bo_MsgHdr_Author_AntwortVomTeam = bo_Mail_Bo_MsgHdr_Author_AntwortVomTeam;

      return detectedTeamMitgliedAutor;
   }


   /// <summary>
   /// Aktualisiert, wenn nötig, den Msg Header: AppMailHeaderType.SpracheMailInhalt
   /// Liefert die Daten zurück
   /// </summary>
   /// <returns>
   /// Null    Daten nicht vorhanden
   /// Sonst   Die Daten
   /// </returns>
   internal string? Analyze_And_AddOrUpdate_MsgHdr_SpracheMailInhalt(bool force = false) => AddOrUpdate_MailMsgHdr_AppMetadata
      (ConfigMail_MsgHeader.AppMailHeaderType.SpracheMailInhalt, Calc_MsgHdr_MailInhalt_Sprache, force);


   /// <summary>
   /// Aktualisiert, wenn nötig, den Msg Header: AppMailHeaderType.MailKategorie
   /// Liefert die Daten zurück
   /// </summary>
   /// <returns>
   /// Null    Daten nicht vorhanden
   /// Sonst   Die Daten
   /// </returns>
   public ConfigMail_MsgHeader.MailKategorie? Analyze_And_AddOrUpdate_MsgHdr_MailKategorie(bool force = false) {
      return AddOrUpdate_MailMsgHdr_AppMetadata
         (ConfigMail_MsgHeader.AppMailHeaderType.MailKategorie
          , Analyze_MailKategorie
          , MailKategorie_Deserialize
          , MailKategorie_Serialize
          , new MailKategorieComparer()
          , ConfigMail_MsgHeader.MailKategorie.Unknown
          , force);
   }

   #endregion Analyze_and_AddOrUpdate_MsgHdr_...

   #endregion Metadaten: Msg hdr

   #endregion Metadaten

   #region Mail Subject

   /// <summary>
   /// Speichert im Msg Hdr eine Kopie des Original-Betreffs for der Bereinigung
   /// </summary>
   /// <param name="force"></param>
   public void Backup_MailBetreff(bool force = false) {
      //+ Wenn nicht force und wenn das Subject-Backup atuell ist 
      if (!force && !Is_MsgHdr_SubjectBackup_Outdated()) { return; }

      //+ Das Subject sichern
      Create_MailBetreffBackup(Mail_Subject);

      // Das Datum des aktuellen Backups merken
      Analyze_And_AddOrUpdate_MsgHdr_MailBetreffWurdeBereinigtDate(force);
   }

   #endregion Mail Subject

   #region Mail Date - Empfangsdatum

   /// <summary>
   /// Liefert das Empfangsdatum der E-Mail
   /// ! Siehe auch IMAP Message InternalDate
   /// - MessageSummaryItems.InternalDate 
   /// </summary>
   public DateTimeOffset Date {
      get {
         //+ Wenn wir die Msg haben, dann dieses Datum nützen
         if (HasMsg) { return Msg.Date; }

         // Sonst die MsgSummary allenfalls laden und das Datum holen 
         return MsgSummary.Date;
      }
   }

   #endregion Mail Date - Empfangsdatum

   /// <summary>
   /// Getter / Setter für den den Mail Betreff
   /// </summary>
   public string Mail_Subject {
      get {
         // Wenn wir die Msg haben, nützen wir sie
         if (HasMsg) { return Msg!.Subject; }

         // Sonst nützen wir MsgSummary, das bei Bedarf geladen wird
         return MsgSummary!.Envelope.Subject;
      }
      set {
         //‼ Der Setter benötigt zwingend die Msg (und nicht nur die MsgSummary)
         if (Msg == null) {
            throw new RuntimeException("Der Mail betreff konnte nicht geladen werden!");
         }

         //+ Den Betreff setzen
         if (value.ØEquals(Msg.Subject)) { return; }

         IsDirty     = true;
         Msg.Subject = value;

         //++ Allenfalls auch die MsgSummary nachführen 
         if (HasMsgSummary) {
            MsgSummary!.Envelope.Subject = value;
         }

      }
   }

   #region Mail Body

   /// <summary>
   /// Holt vom Mail Body den Text mit CRLF
   /// - Wenn die Mail HTML ist, dann wird die Mail in Text konvertiert
   /// - sonst wird der normale Text zurückgegeben
   /// </summary>
   public string Mail_Body_TextOnly => Get_Mail_Body_TextOnly(true);


   /// <summary>
   /// Analysiert den Mail Inhalt und liefert nur den puren Text zurück
   /// - Wenn der Mail Nachrichtentext HTML ist,
   ///   dann wird die Body-Node der HTML-Nachricht in Text konvertiert
   ///   und alle mehrfachen Leerzeichen entfernt
   /// - Sonst wird der Mail Nachrichtentext als Text geholt 
   ///   und alle mehrfachen Leerzeichen entfernt
   /// </summary>
   /// <param name="keepCrLf"></param>
   /// <returns></returns>
   private string Get_Mail_Body_TextOnly(bool keepCrLf) {

      //+ Allenfalls Daten laden
      if (!HasMsg) { Srv_Load_Msg_Data(); }

      //+ HTML extrahieren
      string? htmlAsText = null;

      //+++ Variante 2
      // var htmlBody = BL_Mail_Body.Get_MsgHtmlBody_Complete(Msg, true);
      // HtmlDocument mailHtmlDoc = new HtmlDocument();
      // mailHtmlDoc.LoadHtml(htmlBody);

      //+++ Variante 1
      var (textPart, htmlPart) = MimeKit_JigTools.Get_Msg_Bodies(Msg, true);

      //+++ Wenn textPart und htmlPart null oder leer sind
      // > das Mail hat keinen Text (und ev. nur ein eingebettetes Bild?)
      if (!textPart.ØHasValue() && !htmlPart.ØHasValue()) {
         return "";
      }

      //+ Wenn wir einen HTML Body haben
      if (htmlPart.ØHasValue()) {
         HtmlDocument mailHtmlDoc = new HtmlDocument();
         mailHtmlDoc.LoadHtml(htmlPart);

         //+ Den Body in Text konvertieren
         htmlAsText = BL_Mail_Common.OHtmlToText.Convert(mailHtmlDoc).ØReduceWhiteSpaces
            (keepCrLf);

         // + Debug: Ins ClipBoard kopieren
         // ClipboardService.SetText(htmlBody);
      }

      //+ Wenn wir einen HTML Body haben, er in Text konvertiert werden konnte und der Text nicht leer ist  
      if (htmlAsText.ØHasValue()) {
         return htmlAsText;
      }

      //+ Wenn der HTML Text leer ist, den normalen Text nehmen
      return textPart.ØReduceWhiteSpaces(keepCrLf) ?? "";
   }

   #endregion Mail Body

   /// <summary>
   /// True, wenn wir Daten vom IMAP-Server abrufen können
   /// </summary>
   public bool CanGetServerData => MailFolder != null && MsgSummary != null;

   /// <summary>
   /// True, wenn wir die uniqie Msg Id haben
   /// </summary>
   public bool HasMsgUniqueId => MsgSummary is { UniqueId.IsValid: true };

   /// <summary>
   /// Die Unique Msg ID
   /// </summary>
   public UniqueId MsgUniqueId => MsgSummary?.UniqueId ?? UniqueId.Invalid;

   #region Dekonstruktor, Finalizer

   /// <summary>
   /// ‼ Klappt nicht:
   ///   Sicherstellen, dass eine veränderte E-Mail gespeichert wird,
   ///   spätestens, wenn der Garbace Collector aktiv wird
   ///
   /// Problem: Verschachtelte ImapClient-Aufrufe sind nicht erlaubt 
   /// https://github.com/jstedfast/MailKit/issues/578#issuecomment-551705245
   ///
   /// ! » Also muss sauber programmiert werden, um Objekte zu speichern 
   /// </summary>
   ~BO_Mail() {
      return;
      Save(true);
   }

   #endregion Dekonstruktor, Finalizer

   #region Konstruktoren

   /// <summary>
   /// Konstruktor
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mailFolder"></param>
   /// <param name="msgSummary"></param>
   public BO_Mail(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mailFolder
      , IMessageSummary
         msgSummary) {
      ImapSrvRuntimeConfig = imapSrvRuntimeConfig;
      MailFolder           = mailFolder;
      MsgSummary           = msgSummary;

      //+ initialisieren des CachedDependentClientDataNew Props
      _bo_MsgHdr_Author_AntwortVomTeam = new(
                                                new Accessor<long>(() => _msgSummary.TimestampDataValidSince));

      _mailRefNr_InSubject = new(
                                 new Accessor<long>(() => _msgSummary.TimestampDataValidSince));

      _mailRefNr_InMailBody = new(
                                  new Accessor<long>(() => _msg.TimestampDataValidSince));

      _mailFolder_ArchivPath = new(
                                   new Accessor<long>(() => _mailFolder.TimestampDataValidSince));

      _msgHeader_MailBetreffBackup = new(
                                         new Accessor<long>(() => _msgSummary.TimestampDataValidSince));

      _mailInhalt_Sprache = new(
                                new Accessor<long>(() => _msgSummary.TimestampDataValidSince));
      
   }


   /// <summary>
   /// Konstruktor
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mailFolder"></param>
   /// <param name="msgSummary"></param>
   public BO_Mail(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mailFolder
      , IMessageSummary
         msgSummary
      , MimeMessage msg) {
      ImapSrvRuntimeConfig = imapSrvRuntimeConfig;
      MailFolder           = mailFolder;
      MsgSummary           = msgSummary;
      Msg                  = msg;
   }


   /// <summary>
   /// Konstruktor
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mailFolder"></param>
   /// <param name="msgSummary"></param>
   public BO_Mail(IMailFolder mailFolder, IMessageSummary msgSummary) {
      MailFolder = mailFolder;
      MsgSummary = msgSummary;
   }


   /// <summary>
   /// Konstruktor
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mailFolder"></param>
   /// <param name="msgSummary"></param>
   public BO_Mail(IMailFolder mailFolder, MimeMessage msg) {
      MailFolder = mailFolder;
      Msg        = msg;
   }


   /// <summary>
   /// Konstruktor
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mailFolder"></param>
   /// <param name="msgSummary"></param>
   public BO_Mail(IMessageSummary msgSummary) { MsgSummary = msgSummary; }


   public BO_Mail(MimeMessage msg) { Msg = msg; }

   #endregion Konstruktoren

   #region BO Props

   #region BO Props: Mail FolderPath

   /// <summary>
   /// Liefert den aktuellen Triage-Pfad, d.h. den Unterordner von:
   /// Posteingang\Kontaktformular ★\...
   /// </summary>
   /// <returns>
   /// ‼ Werte:
   /// - Null     Die Mail ist nicht im Triage-Dir oder es gibt keinen MailFolder
   /// - Sonst    Der Triage Mailbox-Pfad. "" == Root-Dir von: Posteingang\Kontaktformular ★\  
   /// </returns>
   public string? Analyze_Mail_Get_TriagePath() {
      if (!HasMailFolder) { return null; }

      //+ Config
      string[] triageRootDirs = {
         @"INBOX\Kontaktformular ★", @"Posteingang\Kontaktformular ★"
      };

      //+ Ist der MailFolder in einem Triage-Verzeichnis? 
      string? triagePath = null;

      for (var idx = 0; triagePath == null && idx < triageRootDirs.Length; idx++) {
         var triageRootDir = triageRootDirs[idx];

         if (MailFolder!.FullName.StartsWith(triageRootDir, StringComparison.OrdinalIgnoreCase)) {
            //+ Die Mail ist im Triage-Verzeichnis: den Pfad merken, das führende \ entfernen
            triagePath = MailFolder!.FullName.ØSubstring(triageRootDir.Length + 1);
         }
      }

      return triagePath;
   }


   /// <summary>
   /// Berechnet für einen Mailbox-Ordner den entsprechenden Pfad im Archiv
   /// 
   /// Alte static-Methode(): Calc_MbxOrdner_ArchivPath()
   /// 
   /// Die Berechnung erfolgt nur aufgrund des Ordners
   /// Für einzelne E-Mails im \Archiv muss der Zielpfad individuell pro Mail berechnet werden
   /// 
   /// ! Wichtig: siehe return Info
   /// 
   /// </summary>
   /// <param name="force"></param>
   /// <returns>
   /// ‼ Werte:
   /// - Null     Der Pfad konnte nicht berechnet werden, weil MailFolder fehlt
   /// - ""       Die E-Mail kommt nicht ins Archiv und wird gelöscht, z.b. Spam
   /// - Sonst    Der Mailbox-Pfad im Archiv 
   /// </returns>
   public string? GetMailFolder_ArchivPath(bool force = false) {
      const string? couldNotCalcArchivPath       = null;
      const string? mailWillBeDeletedNotArchived = "";

      //+ Können wir den Pfad vom Msg Header auslesen?
      if (!force) {
         //+ Haben wir den Wert bereits?
         if (MailFolder_ArchivPath != couldNotCalcArchivPath) { return MailFolder_ArchivPath; }

         //+ Hat die E-Mail den Pfad zum Archiv im Header gespeichert?
         Header? archivPfad = Search_JigAppHeader_GetSingleton
            (ConfigMail_MsgHeader.AppMailHeaderType.TriageOrdner);

         if (archivPfad != null && archivPfad.Value.ØHasValue()) {
            MailFolder_ArchivPath = archivPfad.Value;

            return MailFolder_ArchivPath;
         }
      }

      //+ Ohne force
      //+ oder wir haben den Archiv-Pfad nicht im Msg Header

      //+ Pfad berechnen
      //++ Ohne Daten keine Berechnung
      if (!HasMailFolder) {
         MailFolder_ArchivPath = couldNotCalcArchivPath;

         return MailFolder_ArchivPath;
      }

      // 🚩 Debug
      bool isDebugActive = false;

      //+ Ist die E-Mail im Erledigt-Ordner?
      if (BL_Mail_Common.Kat_ArchivOrdner_Erledigt_Rgx.ØRgxMatchWithAnyItemInList
             (MailFolder!.FullName)) {
         // Im Standard Triage-Ordner ablegen
         MailFolder_ArchivPath = ConfigMail_Triage.TriageArchivOrdnerName;

         return MailFolder_ArchivPath;
      }

      //+ Die eindeutigen Ordner verarbeiten

      #region E-Mails, die gelöscht werden

      //++ E-Mails, die gelöscht werden

      //+++ Gelöscht
      if (BL_Mail_Common.Kat_ArchivOrdner_Gelöscht_Rgx.ØRgxMatchWithAnyItemInList
             (MailFolder.FullName)) {
         // Die E-Mail wird nicht im Archiv gespeichert sondern gelöscht
         MailFolder_ArchivPath = mailWillBeDeletedNotArchived;

         return MailFolder_ArchivPath;
      }

      //+++ Spam
      if (BL_Mail_Common.Kat_ArchivOrdner_Spam_Rgx.ØRgxMatchWithAnyItemInList
             (MailFolder.FullName)) {
         // Die E-Mail wird nicht im Archiv gespeichert sondern gelöscht 
         MailFolder_ArchivPath = mailWillBeDeletedNotArchived;

         return MailFolder_ArchivPath;
      }

      #endregion E-Mails, die gelöscht werden

      //++ E-Mails, die im Archiv abgelegt werden

      #region Alle Ordner, deren E-Mails im Archiv abgelegt werden können

      //+++ Archiv
      if (BL_Mail_Common.Kat_ArchivOrdner_Archiv_Rgx.ØRgxMatchWithAnyItemInList
             (MailFolder.FullName)) {
         MailFolder_ArchivPath = "Archiv";

         return MailFolder_ArchivPath;
      }

      //+++ Entwurf
      if (BL_Mail_Common.Kat_ArchivOrdner_Entwurf_Rgx.ØRgxMatchWithAnyItemInList
             (MailFolder.FullName)) {
         MailFolder_ArchivPath = "Entwurf";

         return MailFolder_ArchivPath;
      }

      //+++ Inbox
      if (BL_Mail_Common.Kat_ArchivOrdner_Inbox_Rgx.ØRgxMatchWithAnyItemInList
             (MailFolder.FullName)) {
         MailFolder_ArchivPath = "Inbox";

         return MailFolder_ArchivPath;
      }

      //+++ Gesendet
      if (BL_Mail_Common.Kat_ArchivOrdner_Gesendet_Rgx.ØRgxMatchWithAnyItemInList
             (MailFolder.FullName)) {
         MailFolder_ArchivPath = "Gesendet";

         return MailFolder_ArchivPath;
      }

      #endregion E-Mails, die im Archiv abgelegt werden

      //+ Alle Order des Arbeitsprozesses 

      #region Alle Order des Arbeitsprozesses: \10 Prozess, in Arbeit

      //++ Die Nummer zur Sortierung der Ordner erscheint nicht im Archiv oder Hashtag,
      // weil diese Nummerierung ändern kann und so später im Archiv nur Verwirrung stiftet
      var archivPath = BL_Mail_Common.CleanPath_RemoveNumbers(MailFolder.FullName);

      // Debug.WriteLine($"\n{mailFolder.FullName}");
      // Debug.WriteLine(archivPath);

      //+ Wenn das Ziel im Hauptordner der Triage liegt, dann diesen Verzeichnisnamen ersetzen,
      // Weil der Hauptordner nichts zur Klassifizierung der E-Mails beiträgt 
      if (ConfigMail_Triage.Kat_Triage_Hauptordner_Rgx.ØRgxMatchWithAnyItemInList
             (archivPath)) {
         //++ Den obersten Pfad ersetzen
         MailFolder_ArchivPath = archivPath.ØReplaceRootDir(ConfigMail_Triage.TriageArchivOrdnerName);

         return MailFolder_ArchivPath;
      }

      #endregion Alle Order des Arbeitsprozesses: \10 Prozess, in Arbeit

      // ‼ Default 
      MailFolder_ArchivPath = ConfigMail_Triage.TriageArchivOrdnerName;

      // ‼ 🚩 Debug
      /*
         if (isDebugActive && !allArchivePaths.Contains(archivPath)) {
            allArchivePaths.Add(archivPath);
            Debug.Write($"{MailFolder.FullName,70}  »  ");
            Debug.WriteLine(archivPath);
         }
   */
      return MailFolder_ArchivPath;
   }


   #region BO Props: Sprache Mail-Inhalt

   /// <summary>
   /// Liest aus der Mail die Sprache der E-Mail
   /// Wenn sie unbekannt ist, dann sie es berechnet
   /// </summary>
   /// <returns></returns>
   string? Get_Metadata_SpracheMailInhalt(bool saveOnDirty) {
      var res = Analyze_And_AddOrUpdate_MsgHdr_SpracheMailInhalt();

      if (saveOnDirty) { Save(); }

      return res;
   }


   /// <summary>
   /// Berechnet die Msg Header Daten für die Sprache des Mail-Inhalts
   /// </summary>
   /// <returns></returns>
   private string Calc_MsgHdr_MailInhalt_Sprache() {
      //++ Sprache analysieren
      var mailInhalt_Sprache = Analyze_Mail_Get_MailInhalt_Sprache();

      return mailInhalt_Sprache?.ThreeLetterWindowsLanguageName ?? "";
   }


   private bool Has_MailInhalt_Sprache => MailInhalt_Sprache != null;


   /// <summary>
   /// Prüft, ob der Msg Header bereits das Element
   ///   <see cref="ConfigMail_MsgHeader.AppMailHeaderType.SpracheMailInhalt"/>
   /// hat.
   /// Wenn Nein, dann wird die Sprache des Mail-Inhalts analysiert
   ///
   /// <see cref="Analyze_and_UpdateOrSet_MsgHdr_MailInhalt_Sprache"/>
   /// 
   /// </summary>
   public CultureInfo? Analyze_Mail_Get_MailInhalt_Sprache() {
      //++ Den Mail-Inhalt in puren Text konvertieren
      var mail_Body_TextOnly = Get_Mail_Body_TextOnly(false);

      if (mail_Body_TextOnly.ØHasValue()) {
         //+ Die Sprache bestimmern
         MailInhalt_Sprache = NTextCat_JigTools.Detect_Language(mail_Body_TextOnly);

         return MailInhalt_Sprache;
      }

      return null;
   }

   #endregion BO Props: Sprache Mail-Inhalt

   #region BO Props: Mail RefNr

   /// <summary>
   /// Liefert True, wenn die Mail eine Mail RefNr hat 
   /// </summary>
   public bool Has_RefNr => Has_RefNr_InSubject || Has_RefNr_InMailBody;

   /// <summary>
   /// True, wenn der Betreff eine Refnr hat
   /// </summary>
   public bool Has_RefNr_InSubject => Get_RefNr_InSubject.ØHasValue();

   /// <summary>
   /// Sucht im Betreff die RefNr
   /// </summary>
   public string? Get_RefNr_InSubject {
      get {
         //+ Haben wir die Daten bereits?
         if (_mailRefNr_InSubject.HasValidData
             && MailRefNr_InSubject.ØHasValue()) {
            return MailRefNr_InSubject;
         }

         //+ Init
         MailRefNr_InSubject = "";

         //+ Daten berechnen
         // Allenfalls Daten laden
         if (!HasMsgSummary && !HasMsg) { Srv_Load_Msg_SummaryData(); }

         //+ Den Betreff holen
         var subject = MsgSummary?.Envelope.Subject ?? Msg?.Subject;

         // ! Subject nicht gefunden
         if (!subject.ØHasValue()) { return null; }

         //+ Suche im Betreff
         var matchSubject = BL_Mail_Common.oRgxRefNr.Match(subject);

         if (matchSubject.Success) {
            MailRefNr_InSubject = matchSubject.Groups["RefNr"].Value;
         }

         return MailRefNr_InSubject;
      }
   }

   /// <summary>
   /// True, wenn der Betreff eine Refnr hat
   /// </summary>
   public bool Has_RefNr_InMailBody => Get_RefNr_InMailBody.ØHasValue();

   /// <summary>
   /// Sucht im Mail Body die RefNr
   /// </summary>
   public string? Get_RefNr_InMailBody {
      get {
         //+ Haben wir die Daten bereits?
         if (_mailRefNr_InMailBody.HasValidData
             && MailRefNr_InMailBody.ØHasValue()) {
            return MailRefNr_InMailBody;
         }

         //+ Den Mail-Inhalt in Text konvertieren
         var mail_Body_TextOnly = Get_Mail_Body_TextOnly(false);

         //++ Finden wir eine RefNr im HTML Body?
         var matchHtml = BL_Mail_Common.oRgxRefNr.Match(mail_Body_TextOnly);

         if (matchHtml.Success) {
            MailRefNr_InMailBody = matchHtml.Groups["RefNr"].Value;

            return MailRefNr_InMailBody;
         }

         //+ Nicht gefunden...
         return null;
      }
   }

   #endregion BO Props: Mail RefNr

   #endregion BO Props

   #endregion BO Props

   #region Methoden: Public, d.h. BL, Business-Logic

   #region BL, Business-Logic: Persönliche Daten der Fragesteller

   /// <summary>
   /// Analysiert die E-Mail und sucht nach den Personendaten des Fragestellers
   /// </summary>
   /// <returns></returns>
   public RecFragestellerV01 Analyze_Mail_Get_FragestellerPersoenlicheDaten() {
      //+ Die Mail Header Props analysieren (To, From, Sender, …)
      List<EMailAddress> MsgHdr_eMailAddresses = Parser_MsgProperties.Parse_MailProperties(Msg!);

      //+ Den Mail Body analysieren
      //++ Im Body alle E-Mail Adressen suchen
      List<EMailAddress> MailBody_eMailAddresses = Parse_MailBody.Find_EMail_Addresses(Msg!);

      // Die beiden Listen gefundener E-Mail Adressen zusammenfügen
      EMailAddress.AddToList(MsgHdr_eMailAddresses, MailBody_eMailAddresses, true);

      //++ Die Kontaktformular-Daten suchen
      RecFragestellerV01? newRec_Kontaktformular_Fragesteller_Daten
         = Parse_MailBody.Find_Kontaktformular_Fragesteller_Daten(Msg!);

      //+ Alle Daten im Rec zusammenführen
      if (newRec_Kontaktformular_Fragesteller_Daten == null) {
         newRec_Kontaktformular_Fragesteller_Daten = new RecFragestellerV01();
      }

      //++ Die gefundenen E-Mail Adressen erfassen
      newRec_Kontaktformular_Fragesteller_Daten.MailHeader_EMailAddresses = MsgHdr_eMailAddresses
                                                                           .Where
                                                                               (x => x.EMailAddr.ØHasValue()).Distinct().ToList();

      return newRec_Kontaktformular_Fragesteller_Daten;
   }


   /// <summary>
   /// Haben wir im Msg Header die Personendaten zum Fragesteller? 
   /// </summary>
   /// <param name="appMailHeaderType"></param>
   /// <returns></returns>
   private bool Has_Fragesteller_PersönlicheDaten() {
      var recFragesteller = Get_MsgHdr_Encrypted_Fragesteller_Daten();

      return recFragesteller != null;
   }


   /// <summary>
   /// Liest aus dem Msg Header die verschlüsselten Personendaten des Fragestellers  
   /// </summary>
   /// <param name="appMailHeaderType"></param>
   /// <returns>
   /// var (mimekitObjHeaders, foundAppheaders) = 
   /// </returns>
   internal RecFragestellerV01? Get_MsgHdr_Encrypted_Fragesteller_Daten() {
      //+ Den Msg Header mit den persönlichen Daten suchen 
      var (_, _, foundMsgSummaryHdrs, foundMsgHdrs) = Search_JigAppHeader
         (ConfigMail_MsgHeader.AppMailHeaderType.FragestellerPersoenlicheDaten);

      //++ Nicht gefunden
      if (foundMsgSummaryHdrs.Count == 0 && foundMsgHdrs.Count == 0) { return null; }

      //++ Gefunden: je max 1 Record!, weil die Personendaten nur einmal gespeichert werden sollen
      Assert.IsTrue(foundMsgSummaryHdrs.Count <= 1);
      Assert.IsTrue(foundMsgHdrs.Count <= 1);

      //++ Die Daten entschlüsseln
      if (foundMsgSummaryHdrs.Count > 0) {
         return RecFragestellerV01.DecryptRecord
            (ConfigMail_MsgHeader.EncryptionPW, foundMsgSummaryHdrs[0].Value);
      }

      if (foundMsgHdrs.Count > 0) {
         return RecFragestellerV01.DecryptRecord
            (ConfigMail_MsgHeader.EncryptionPW, foundMsgHdrs[0].Value);
      }

      //Q Nichts gefunden
      return null;
   }


   #region E-Mail Adressen Tests

   /// <summary>
   /// Liefert true, wenn die internetAddressList
   /// eine persönliche Team Antwort E-Mail Adresse enthält 
   /// </summary>
   /// <param name="internetAddressList"></param>
   /// <returns></returns>
   public bool HasMail_PersönlicheTeamAntwortAdresse(InternetAddressList? internetAddressList) {
      // Ignorieren: MimeKit.GroupAddress
      // !M http://www.mimekit.net/docs/html/T_MimeKit_GroupAddress.htm
      return internetAddressList != null
             && internetAddressList.Where(ial => ial.GetType() != typeof(GroupAddress))
                                   .Any
                                       (m =>
                                           ConfigMail_Basics.ORgx_EMailAdress_PersönlicheTeamAntwortAdresse.Match
                                              (((MailboxAddress)m).Address).Success);
   }


   /// <summary>
   /// Liefert true, wenn internetAddressList eine Adresse von unserer AntwortDomain hat
   /// </summary>
   /// <param name="internetAddressList"></param>
   /// <returns></returns>
   public bool HasMail_From_AntwortDomain(InternetAddressList? internetAddressList) {
      // Ignorieren: MimeKit.GroupAddress
      // !M http://www.mimekit.net/docs/html/T_MimeKit_GroupAddress.htm
      return internetAddressList != null
             && internetAddressList.Where(ial => ial.GetType() != typeof(GroupAddress))
                                   .Any
                                       (m => ConfigMail_Basics.Kat_FromAddr_DomainsAreProbablyAntworten
                                                              .ØEqualsWithAnyItemInList
                                                                  (((MailboxAddress)m).Domain));
   }


   /// <summary>
   /// Liefert true, wenn internetAddressList eine unserer Antwortadressen beinhaltet
   /// </summary>
   /// <param name="internetAddressList"></param>
   /// <returns></returns>
   public bool HasMail_From_AntwortAddress(InternetAddressList? internetAddressList) {
      // Ignorieren: MimeKit.GroupAddress
      // !M http://www.mimekit.net/docs/html/T_MimeKit_GroupAddress.htm
      return internetAddressList != null
             && internetAddressList.Where(ial => ial.GetType() != typeof(GroupAddress))
                                   .Any
                                       (x => ConfigMail_Basics.Kat_FromAddr_Antworten
                                                              .ØStartsWithAnyItemInList
                                                                  (
                                                                   ((MailboxAddress)x).Address));
   }


   /// <summary>
   /// Liefert true, wenn internetAddressList die Kontaktformular-Absenderadresse hat 
   /// </summary>
   /// <param name="internetAddressList"></param>
   /// <returns></returns>
   public bool HasMail_From_Kontaktformular(InternetAddressList? internetAddressList) {
      // Ignorieren: MimeKit.GroupAddress
      // !M http://www.mimekit.net/docs/html/T_MimeKit_GroupAddress.htm
      return internetAddressList != null
             && internetAddressList.Where(ial => ial.GetType() != typeof(GroupAddress))
                                   .Any
                                       (m => ((MailboxAddress)m).Address.ØEqualsIgnoreCase
                                           (ConfigMail_Basics.EMailAdress_Kontaktformular));
   }


   /// <summary>
   /// Liefert true, wenn internetAddressList die TeamMailbox Address drin hat 
   /// </summary>
   /// <param name="internetAddressList"></param>
   /// <returns></returns>
   public bool HasMail_TeamMailboxAddress(InternetAddressList? internetAddressList) {
      // Ignorieren: MimeKit.GroupAddress
      // !M http://www.mimekit.net/docs/html/T_MimeKit_GroupAddress.htm
      return internetAddressList != null
             && internetAddressList.Where(ial => ial.GetType() != typeof(GroupAddress))
                                   .Any
                                       (m => ((MailboxAddress)m).Address.ØEqualsIgnoreCase
                                           (ConfigMail_Basics.EMailAdress_TeamPostfach));
   }


   /// <summary>
   /// Liefert true, wenn internetAddressList die E-Mail Address für zugeschickte Daten drin hat 
   /// </summary>
   /// <param name="internetAddressList"></param>
   /// <returns></returns>
   public bool HasMail_ZugeschickteDateiAddress(InternetAddressList? internetAddressList) {
      // Ignorieren: MimeKit.GroupAddress
      // !M http://www.mimekit.net/docs/html/T_MimeKit_GroupAddress.htm
      return internetAddressList != null
             && internetAddressList.Where(ial => ial.GetType() != typeof(GroupAddress))
                                   .Any
                                       (m => ((MailboxAddress)m).Address.ØEqualsIgnoreCase
                                           (ConfigMail_Basics.EMailAdress_ZugeschickteDaten));
   }

   #endregion E-Mail Adressen Tests

   #region ConfigMail_MsgHeader.MailKategorie

   /// <summary>
   /// Liest aus der Mail die Mail-Kategorie der E-Mail
   /// Wenn sie unbekannt ist, dann sie es berechnet
   /// </summary>
   /// <returns></returns>
   ConfigMail_MsgHeader.MailKategorie? Get_Metadata_MailKategorie(bool saveOnDirty) {
      var res = Analyze_And_AddOrUpdate_MsgHdr_MailKategorie();

      if (saveOnDirty) { Save(); }

      return res;
   }


   #region Analyze_And_AddOrUpdate_MsgHdr_MailKategorie

   /// <summary>
   /// Konvertiert MailKategorie » String
   /// </summary>
   /// <param name="mailKategorie"></param>
   /// <returns></returns>
   private string? MailKategorie_Serialize(ConfigMail_MsgHeader.MailKategorie? mailKategorie) {
      if (mailKategorie == null) { return null; }

      return mailKategorie.ToString();
   }


   /// <summary>
   /// Konvertiert String » MailKategorie  
   /// </summary>
   /// <param name="triageHashtags"></param>
   /// <returns></returns>
   private ConfigMail_MsgHeader.MailKategorie? MailKategorie_Deserialize(string? triageHashtags) {
      if (!triageHashtags.ØHasValue()) { return null; }

      if (Enum.TryParse(triageHashtags, out ConfigMail_MsgHeader.MailKategorie parsed)) {
         return parsed;
      }

      return null;
   }

   #endregion Analyze_And_AddOrUpdate_MsgHdr_MailKategorie


   /// <summary>
   /// Analysiert die E-Mail und bestimmt die MailKategorie
   /// </summary>
   /// <returns></returns>
   private ConfigMail_MsgHeader.MailKategorie? Analyze_MailKategorie() {
      //» Sicherstellen, dass die Metadaten vorhanden sind
      Analyze_and_AddOrUpdate_MsgHdr_MailRefnr();

      //» Allgemeine Analysen der E-Mail Adressen
      //+ From: -Adressen
      //++ From stammt von einer unserer Domain (RogerLiebi.ch) 
      bool isEMail_FromAntwortDomain = HasMail_From_AntwortDomain(MsgSummary?.Envelope.From);

      //++ From stammt von einer unserer Absender-Adressem 
      var isEMail_FromAntwortAddress = HasMail_From_AntwortAddress(MsgSummary?.Envelope.From);

      //++ From ist das Kontaktformular
      var isEMail_FromKontaktformular = HasMail_From_Kontaktformular(MsgSummary?.Envelope.From);

      //+ TO: -Adressen
      //++ To direkt ans Team-Postfach
      var isEMail_ToTeamPostfach = HasMail_TeamMailboxAddress(MsgSummary?.Envelope.To);

      //++ To: eine zugeschickte Datei
      var isEMail_ToZugeschickteDaten = HasMail_ZugeschickteDateiAddress(MsgSummary?.Envelope.To);

      //+ To: an eine persönliche Team Antwort Adresse
      var isEMail_ToPersönlicheTeamAntwortAdresse = HasMail_PersönlicheTeamAntwortAdresse(MsgSummary?.Envelope.To);

      //» Analyse der E-Mail
      //+ Die Mail analysieren
      ConfigMail_MsgHeader.MailKategorie analyzedNewMailKategorie = ConfigMail_MsgHeader.MailKategorie.Unknown;

      if (IsAppSpecificMail_DocMail) {
         //++ Haben wir eine Doc-Mail?
         analyzedNewMailKategorie = ConfigMail_MsgHeader.MailKategorie.AppSpezifischeMailDocMail;
      }
      else if (isEMail_FromKontaktformular) {
         //++ Ist die Mail vom Kontaktformular?
         analyzedNewMailKategorie = ConfigMail_MsgHeader.MailKategorie.FrageVomKontaktformular;
      }
      else if (isEMail_ToZugeschickteDaten) {
         //++ Ist die Mail eine zugeschickte Datei?
         analyzedNewMailKategorie = ConfigMail_MsgHeader.MailKategorie.ZugeschickteDatei;
      }
      else if (Has_RefNr
               && (isEMail_ToPersönlicheTeamAntwortAdresse || isEMail_ToTeamPostfach)) {
         //++ Ist die Mail eine Reaktion auf eine unserer Antworten?
         analyzedNewMailKategorie = ConfigMail_MsgHeader.MailKategorie.ReaktionAufAntwortVomTeam;
      }
      else {
         //++ Wurde die Mail direkt ans Team-Postfach
         // oder an ein Team-Mitglied geschickt?
         bool isDirektZugeschickt = !Has_RefNr
                                    && !isEMail_FromAntwortDomain
                                    && !isEMail_FromAntwortAddress
                                    && !isEMail_FromKontaktformular
                                    && !isEMail_ToZugeschickteDaten
                                    && (isEMail_ToTeamPostfach
                                        || isEMail_ToPersönlicheTeamAntwortAdresse);

         if (isDirektZugeschickt) {
            analyzedNewMailKategorie = ConfigMail_MsgHeader.MailKategorie.MailDirektAnsTeamPostfach;
         }
         else if (isEMail_FromAntwortDomain || isEMail_FromAntwortAddress) {
            analyzedNewMailKategorie = ConfigMail_MsgHeader.MailKategorie.AntwortVomTeam;
         }
         else {
            analyzedNewMailKategorie = ConfigMail_MsgHeader.MailKategorie.Unknown;
         }
      }

      return analyzedNewMailKategorie;
   }


   /// <summary>
   /// Liest den Msg Header AppMailHeaderType.MailKategorie
   /// </summary>
   /// <returns>
   /// - Die Mail-Kategorie im Header
   /// - oder MailKategorie.Unknown 
   /// </returns>
   internal ConfigMail_MsgHeader.MailKategorie Get_MsgHdr_MailKategorie() {
      //+ Den Msg Header mit den persönlichen Daten suchen 
      var (_, _, foundMsgSummaryHdrs, foundMsgHdrs) = Search_JigAppHeader
         (ConfigMail_MsgHeader.AppMailHeaderType.MailKategorie);

      //++ Nicht gefunden
      if (foundMsgSummaryHdrs.Count == 0 && foundMsgHdrs.Count == 0) { return ConfigMail_MsgHeader.MailKategorie.Unknown; }

      //++ Gefunden: je max 1 Record!, weil die Personendaten nur einmal gespeichert werden sollen
      Assert.IsTrue(foundMsgSummaryHdrs.Count <= 1);
      Assert.IsTrue(foundMsgHdrs.Count <= 1);

      //++ Die Daten entschlüsseln
      if (foundMsgSummaryHdrs.Count > 0) {
         return foundMsgSummaryHdrs[0].Value.ØParseToEnum<ConfigMail_MsgHeader.MailKategorie>
            (ConfigMail_MsgHeader.MailKategorie.Unknown);
      }

      if (foundMsgHdrs.Count > 0) {
         return foundMsgHdrs[0].Value.ØParseToEnum<ConfigMail_MsgHeader.MailKategorie>
            (ConfigMail_MsgHeader.MailKategorie.Unknown);
      }

      return ConfigMail_MsgHeader.MailKategorie.Unknown;
   }


   /// <summary>
   /// Setzt den Msg Hdr AppMailHeaderType.MailKategorie
   /// </summary>
   /// <param name="mailKategorie"></param>
   /// <param name="saveOnDirty"></param>
   internal void UpdateOrSet_MailKategorie(
      ConfigMail_MsgHeader.MailKategorie? mailKategorie
      , bool                              saveOnDirty = false) {
      if (mailKategorie == null) { return; }

      //+ Wenn der Wert nicht geändert hat, sind wir fertig
      var currentSetting = Get_MsgHdr_MailKategorie();

      if (currentSetting == mailKategorie) { return; }

      //+ Daten aktualisieren und allenfalls speichern
      Set_MsgHdr_MailKategorie(mailKategorie);
      if (saveOnDirty && IsDirty) { Save(); }
   }


   /// <summary>
   /// Konvertiert MailKategorie für den Msg Header und setzt den Msg Hdr
   /// </summary>
   private void Set_MsgHdr_MailKategorie(ConfigMail_MsgHeader.MailKategorie? mailKategorie) {
      MsgHdr_UpdateOrSet_JigAppHeader
         (ConfigMail_MsgHeader.AppMailHeaderType.MailKategorie, mailKategorie.ToString()!);
   }

   #endregion ConfigMail_MsgHeader.MailKategorie


   /// <summary>
   /// Liest aus der Mail die persönlichen Daten des Fragestellers
   /// Wenn sie unbekannt sind, dann werden sie berechnet
   /// </summary>
   /// <returns></returns>
   RecFragestellerV01? Get_Metadata_FragestellerPersoenlicheDaten(bool saveOnDirty) {
      var res = Analyze_and_AddOrUpdate_MsgHdr_FragestellerPersoenlicheDaten();

      if (saveOnDirty) { Save(); }

      return res;
   }


   /// <summary>
   /// Aktualisiert oder setzt den Msg Header auf die Daten des Fragestellers
   /// Markiert dieses Objekt als dirty, wenn Daten verändert werden
   /// </summary>
   /// <param name="newRecFragesteller"></param>
   /// <param name="hasKontakformularDaten">
   /// True, wenn newRecFragesteller Daten vom Kontaktformular hat
   /// </param>
   private void UpdateOrSet_Fragesteller_PersönlicheDaten(RecFragestellerV01? newRecFragesteller) {

      //+ Haben wir bereits Daten im Header?
      var currentRecFragesteller = Get_MsgHdr_Encrypted_Fragesteller_Daten();

      //+ Wenn die Msg Hdr Daten haben und keine neuen Daten vorhanden sind: fertig
      if (currentRecFragesteller != null
          && currentRecFragesteller.HasData()
          && (newRecFragesteller == null || !newRecFragesteller.HasData())) {
         return;
      }

      //+ Hat der Header bereits aktuelle Daten?
      var headerDataIsUptodate = newRecFragesteller != null
                                 && currentRecFragesteller != null
                                 && currentRecFragesteller.Get_DataAsString().Equals
                                    (newRecFragesteller.Get_DataAsString());

      //+ Wenn die daten aktuell sind: fertig
      if (headerDataIsUptodate) {
         return;
      }

      //+ Wenn wir keinen neuen Rec haben (und keine alten bestehen)
      if (newRecFragesteller == null) {
         //++ um sicher zu sein: Msg Hdr entfernen: FragestellerPersonalDaten 
         Remove_JigAppHeader(ConfigMail_MsgHeader.AppMailHeaderType.FragestellerPersoenlicheDaten);
      }
      else {
         //+ Wir haben neue Daten: den Msg Hdr setzen
         Set_MsgHdr_FragestellerPersoenlicheDaten(newRecFragesteller);
      }

   }


   /// <summary>
   /// Konvertiert die RecFragestellerV01 für den Msg Header und setzt den Msg Hdr
   /// </summary>
   /// <param name="recFragestellerV01"></param>
   private void Set_MsgHdr_FragestellerPersoenlicheDaten(RecFragestellerV01 recFragestellerV01) {
      //++ Daten verschlüsseln
      var encryptedRecord = recFragestellerV01.EncryptRecord(ConfigMail_MsgHeader.EncryptionPW);

      //++ Den neuen Header setzen
      MsgHdr_UpdateOrSet_JigAppHeader
         (ConfigMail_MsgHeader.AppMailHeaderType.FragestellerPersoenlicheDaten, encryptedRecord);

      //++ Erfassen, ob wir Kontaktformulardaten gefunden haben
      Set_MsgHeader_MailKontaktformularDatenFound(recFragestellerV01.HasKontaktformData);
   }


   /// <summary>
   /// Setzt im Msg Header die Info, ob im Mail Body die Kontaktformulardaten gefunden wurden 
   /// </summary>
   /// <param name="kontaktformularDatenFound"></param>
   private void Set_MsgHeader_MailKontaktformularDatenFound(bool kontaktformularDatenFound) {
      if (kontaktformularDatenFound) {
         //++ Wir haben Kontaktformular Daten gefunden
         MsgHdr_UpdateOrSet_JigAppHeader(ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularDatenFound, "");
         Remove_JigAppHeader(ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularDatenMissing);
      }
      else {
         //++ Wir haben keine KontaktformulardatenDen msg hdr setzen: FragestellerPersonalDatenNotFound
         MsgHdr_UpdateOrSet_JigAppHeader(ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularDatenMissing, "");
         Remove_JigAppHeader(ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularDatenFound);
      }
   }

   #endregion BL, Business-Logic: Persönliche Daten der Fragesteller

   #region BL, Business-Logic: Mail

   /// <summary>
   /// Aktualisiert, wenn nötig:
   ///   Mail Msg Header
   ///   - <see cref="ConfigMail_MsgHeader.AppMailHeaderType.MailRefNr"/> 
   ///   - <see cref="ConfigMail_MsgHeader.AppMailHeaderType.TriageOrdner"/> 
   ///   - <see cref="ConfigMail_MsgHeader.AppMailHeaderType.TriageHashtags"/> 
   ///   - <see cref="ConfigMail_MsgHeader.AppMailHeaderType.FragestellerPersoenlicheDaten"/> 
   ///   - <see cref="ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularDatenFound"/> 
   ///   - <see cref="ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularDatenMissing"/> 
   ///   - <see cref="ConfigMail_MsgHeader.AppMailHeaderType.SpracheMailInhalt"/> 
   /// 
   ///   Mail Body ergänzen:
   ///   - Html Head: Style setzen
   ///   - Triage-Hashtags
   /// 
   /// </summary>
   [Obsolete("Diese Funktion ist obsolete » Update_Mail_MetaDaten()", false)]
   public void Update_Mail_MetaDaten_001(bool saveOnDirty = true, bool force = false) {
      //+ Ohne Mailserver können wir keine Daten aktualisieren       
      if (!HasImapSrvRuntimeConfig) { return; }

      //+ Stoppen, wenn wir in der Archiv-Mailbox sind
      // if (ImapSrvRuntimeConfig!.IsArchiveMailbox) { return; }

      //+ Alle Msg Header aktualisieren
      Update_Mail_Header_MetaData(force: force);

      //+ Den Msg Mail Body aktualisieren
      Update_Mail_Body(force: force);

      if (saveOnDirty && IsDirty) { Save(); }

   }


   /// <summary>
   /// Ergänz in der E-Mail die Metadaten
   /// </summary>
   /// <param name="saveOnDirty"></param>
   /// <param name="force"></param>
   public void Update_Mail_MetaDaten(bool saveOnDirty = true, bool force = false) {
      //+ Ohne Mailserver können wir keine Daten aktualisieren       
      if (!HasImapSrvRuntimeConfig) { return; }

      //+ Alle Msg Header aktualisieren
      var bo_Mail_Bl_UpdateOrSet_Metadata_MsgHdr = new BO_Mail_BL_UpdateOrSet_Metadata_MsgHdr(this);
      bo_Mail_Bl_UpdateOrSet_Metadata_MsgHdr.UpdateOrSet_Mail_Metadata(false, force);

      //+ Den Msg Mail Body aktualisieren
      var bO_Mail_BL_UpdateOrSet_Metadata_MsgBody = new BO_Mail_BL_UpdateOrSet_Metadata_MsgBody(this);
      bO_Mail_BL_UpdateOrSet_Metadata_MsgBody.UpdateOrSet_Mail_Metadata(false, force);

      if (saveOnDirty && IsDirty) { Save(); }
   }

   #endregion BL, Business-Logic: Mail

   #region BL, Business-Logic: Msg Subject

   /// <summary>
   /// Bereinigt den E-Mail Betreff:
   /// - Setzt im Header
   ///   - eine Kopie des Original-Betreffs
   ///   - das Flag dass der Header berienigt wurde
   /// - Bereinigt die Subject-Präfixe RE: FWD, etc.
   /// - Setzt bei fremdsprachigen Mails das Sprachkürzel, e.g. ⭕EN
   /// 
   /// !Doc: Siehe: .\BO-Mail-Cleanup-Subject.adoc
   /// 
   /// </summary>
   /// <param name="addForeinLanguageInfo">
   /// True: das Sprachkürzel wird am Anfang des Subjects zugefügt
   /// </param>
   /// <param name="addFirstSubjectPrefix">
   /// True: Wenn vorhanden, dann wird das erste Betreff-Präfix der Mail zugefügt 
   /// </param>
   /// <param name="addLastSubjectPrefix">
   /// True: Wenn vorhanden, dann wird das letzte gefundene Betreff-Präfix der Mail zugefügt 
   /// </param>
   /// <param name="saveOnDirty"></param>
   /// <param name="force"></param>
   public void Cleanup_Subject(
      bool   addForeinLanguageInfo
      , bool addFirstSubjectPrefix
      , bool addLastSubjectPrefix
      , bool saveOnDirty = true
      , bool force       = false) {
      //+ App-Spezifische E-Mails von Imap-Mail-Manager berieigen wir nie
      if (IsAppSpecificMail) { return; }

      //+ Den Betreff bereinigten Betreff berechnen
      //++ Analysieren
      var (resSpracheIso3, resEMailTypFirst, resEMailTypLast, resNewSubject) =
         BL_MailSubject.Split_Subject_SprachCode_Präfixes_Subject(Mail_Subject);

      //+ Den neuen Betreff erzeugen
      StringBuilder newSubject = new StringBuilder();

      //+ Das Shrachkürzel ergänzen
      if (addForeinLanguageInfo) {
         string sprachKürzel = "";

         if (resSpracheIso3.ØHasValue()) {
            sprachKürzel = resSpracheIso3;
         }
         else {
            // Allenfalls die Sprache erkennen
            if (!Has_MailInhalt_Sprache) { Analyze_And_AddOrUpdate_MsgHdr_SpracheMailInhalt(); }

            if (Has_MailInhalt_Sprache) {
               sprachKürzel = Analyze_Mail_Get_MailInhalt_Sprache()!.ThreeLetterISOLanguageName.ToUpper();
            }
         }

         // Wenn wir die Sprache kennen und sie nicht DEU ist
         if (!sprachKürzel.ØEqualsIgnoreCase("DEU")) {
            newSubject.Append($"{BL_MailSubject.SubjectLanguageMarker}{sprachKürzel} ");
         }
      }

      // Das erste Präfix ergänzen
      if (addFirstSubjectPrefix && resEMailTypFirst != BL_MailSubject.EmpfangenerEMailTyp.Unknown) {
         newSubject.Append
            ($"{BL_MailSubject.Get_StandardizedPrefix_Str(resEMailTypFirst)} ");
      }

      // Das letzte Präfix ergänzen
      if (addLastSubjectPrefix
          && resEMailTypLast != BL_MailSubject.EmpfangenerEMailTyp.Unknown
          && resEMailTypLast != resEMailTypFirst) {
         newSubject.Append
            ($"{BL_MailSubject.Get_StandardizedPrefix_Str(resEMailTypLast)} ");
      }

      // Den eigentlichen Betreff-Text ergänzen 
      newSubject.Append(resNewSubject);

      //+ Ist der neue Betreff anders als der aktuelle?
      var strNewSubject = newSubject.ToString().ØTrimAndReduce();

      if (force || !Mail_Subject.ØEquals(strNewSubject)) {
         //+ Den Betreff im Msg Hdr sichern 
         Backup_MailBetreff();
         Mail_Subject = strNewSubject;
         IsDirty      = true;
      }

      //+ Speichern wenn verändert
      if (saveOnDirty && IsDirty) { Save(); }

   }

   #endregion BL, Business-Logic: Msg Subject

   #region BL, Business-Logic: Msg Header

   /// <summary>
   /// Berechnet die Metadaten und aktualisiert, wenn nötig, die Msg Header
   /// </summary>
   private void Update_Mail_Header_MetaData(bool saveOnDirty = false, bool force = false) {
      //+ Die Mail Msg Header
      //++ Die Kat der Mail berechnen
      Analyze_And_AddOrUpdate_MsgHdr_MailKategorie(force);

      // Das Thema des Kontaktformulars bestimmen
      Analyze_and_AddOrUpdate_MsgHdr_MailKontaktformularThema(force);

      //++ AppMailHeaderType.MailRefNr
      Analyze_and_AddOrUpdate_MsgHdr_MailRefnr();

      //++ AppMailHeaderType.TriageOrdner
      Analyze_and_AddOrUpdate_MsgHdr_TriageOrdner(false);

      //++ AppMailHeaderType.TriageHashtags
      Analyze_and_AddOrUpdate_MsgHdr_TriageHashtags();

      //++ AppMailHeaderType.FragestellerPersonalDaten / FragestellerPersonalDatenNotFound
      Analyze_and_AddOrUpdate_MsgHdr_FragestellerPersoenlicheDaten(force: force);

      //++ AppMailHeaderType.SpracheMailInhalt
      Analyze_And_AddOrUpdate_MsgHdr_SpracheMailInhalt(force);

      if (saveOnDirty && IsDirty) { Save(); }
   }


   #region BL, Business-Logic: Msg Header: MailBetreffWurdeBereinigt

   /// <summary>
   /// Liefert True, wenn das Backup vom Mail Subject im Header veraltet ist
   /// </summary>
   /// <returns></returns>
   private bool Is_MsgHdr_SubjectBackup_Outdated() {
      // Das Datum holen, wann der Betreff bereinigt wurde
      var MsgHdr_MailBetreffWurdeBereinigtAm = Get_MsgHdr_MailBetreffWurdeBereinigt();

      //+ Ist der MsgHdr MailBetreffWurdeBereinigt veraltet?
      //++ Ja, wenn kein Backup existiert
      //++ Ja, wenn das Backup vor dem Empfang der E-Mail erstellt wurde
      var isMsgSubjectBereinigungVeraltet = MsgHdr_MailBetreffWurdeBereinigtAm == null
                                            || MsgHdr_MailBetreffWurdeBereinigtAm < Date;

      return isMsgSubjectBereinigungVeraltet;
   }


   /// <summary>
   /// Holt den Msg Hdr MailBetreffWurdeBereinigt
   /// und liefert das DateTimeOffset 
   /// </summary>
   /// <returns></returns>
   private DateTimeOffset? Get_MsgHdr_MailBetreffWurdeBereinigt() {
      // Die Msg Hdr Daten lesen
      Header? mailBetreffWurdeBereinigtAm = Search_JigAppHeader_GetSingleton
         (ConfigMail_MsgHeader.AppMailHeaderType.MailBetreffWurdeBereinigtDate);

      if (mailBetreffWurdeBereinigtAm != null && mailBetreffWurdeBereinigtAm.Value.ØHasValue()) {
         DateTimeOffset dateTimeOffset;

         DateUtils.TryParse
            (Encoding.UTF8.GetBytes(mailBetreffWurdeBereinigtAm.Value)
             , 0
             , mailBetreffWurdeBereinigtAm.Value.Length
             , out dateTimeOffset);

         return dateTimeOffset;
      }

      return null;
   }

   #endregion BL, Business-Logic: Msg Header: MailBetreffWurdeBereinigt

   #region BL, Business-Logic: Msg Header: TriageHashtags

   /// <summary>
   /// Konvertiert TriageHashtags » String
   /// </summary>
   /// <param name="triageHashtags"></param>
   /// <returns></returns>
   private string? TriageHashtags_Serialize(List<string>? triageHashtags) {
      if (triageHashtags == null) { return null; }

      return String.Join('|', triageHashtags);
   }


   /// <summary>
   /// Konvertiert String » TriageHashtags  
   /// </summary>
   /// <param name="triageHashtags"></param>
   /// <returns></returns>
   private List<string> TriageHashtags_Deserialize(string? triageHashtags) {
      if (!triageHashtags.ØHasValue()) { return new(); }

      return triageHashtags.Split('|').ToList();
   }


   /// <summary>
   /// Liest aus der Mail die Triage HashTags
   /// Wenn sie unbekannt sind, dann werden sie berechnet
   /// </summary>
   /// <returns></returns>
   List<string> Get_Metadata_TriageHashtags_Archiv(bool saveOnDirty) {
      var res = Analyze_and_AddOrUpdate_MsgHdr_TriageHashtags();

      // Analyze_Mail_Get_TriageHashTags
      // Calc_MbxOrdner_TriageHashTags

      if (saveOnDirty) { Save(); }

      return res;
   }


   /// <summary>
   /// Konvertiert die TriageHashTags für den Msg Header und setzt den Msg Hdr
   /// </summary>
   /// <param name="triageHashtags"></param>
   private void Set_MsgHdr_TriageHashtags(List<string> triageHashtags) {
      MsgHdr_UpdateOrSet_JigAppHeader
         (ConfigMail_MsgHeader.AppMailHeaderType.TriageHashtags, TriageHashtags_Serialize(triageHashtags)!);
   }


   /// <summary>
   /// Berechnet aufgrund des Mail-Ordners die Triage-HashTags
   /// </summary>
   /// <returns></returns>
   internal List<string> Analyze_TriageHashTags() {
      //+ Die HashTags berechnen
      var triageFolderHashTags = BL_Mail_Common.Analyze_TriageHashTags(MailFolder!);

      // Das oberste Element "#Triagiert" löschen, hat nur wenig Nutzen 
      triageFolderHashTags.ØRemoveAt(0);

      return triageFolderHashTags;
   }


   /// <summary>
   /// Sucht nicht explizit die Triage-HashTags, sondern alle HashTags in der E-Mail:
   /// - im Betreff
   /// - im Mail Header 
   /// - im Mail Body
   /// 
   /// Aktualisiert, wenn nötig, den Msg Header mit:
   /// AppMailHeaderType.TriageHashtags
   ///
   /// Alter Code: AddOrUpdate_MailMsgHdr_HashTags()
   /// 
   /// </summary>
   private List<string> Analyze_and_UpdateOrSet_MsgHdr_AllMailHashtags() {

      //+ Die HashTags der E-Mail lesen
      var (hashtags_InHeader, hashtags_InSubject, hashtags_InBody)
         = BL_Mail_Body.FindMail_Hashtags(Msg!);

      //+ Alle HashTags zusammenfassen
      var allHashtags = hashtags_InHeader.Concat(hashtags_InSubject).Concat(hashtags_InBody).Distinct().ToList();

      //+ Prüfen, ob die HashTags geändert haben
      var hashTagsHaveNotBeenChanged = new HashSet<string>(hashtags_InHeader).SetEquals
         (allHashtags);

      //++ Die HashTags wurden nicht verändert
      if (hashTagsHaveNotBeenChanged) { return hashtags_InHeader; }

      //++ Die HashTags wurden verändert
      var joinedAllHashtags = string.Join("|", allHashtags);

      //+ Den Header setzen / aktualisieren
      MsgHdr_UpdateOrSet_JigAppHeader(ConfigMail_MsgHeader.AppMailHeaderType.TriageOrdner, joinedAllHashtags);

      return allHashtags;
   }

   #endregion BL, Business-Logic: Msg Header: TriageHashtags

   #region BL, Business-Logic: Msg Header: TriageOrdner

   /// <summary>
   /// Liest aus der Mail die Triage HashTags
   /// Wenn sie unbekannt sind, dann werden sie berechnet
   /// </summary>
   /// <returns></returns>
   List<string>? Get_Metadata_TriageHashtags(bool saveOnDirty) {
      var res = Analyze_and_AddOrUpdate_MsgHdr_TriageHashtags();
      if (saveOnDirty) { Save(); }

      return res;
   }


   /// <summary>
   /// Liest aus der Mail die MailRefNr
   /// Wenn sie unbekannt ist, dann wird sie berechnet
   /// </summary>
   /// <returns></returns>
   string? Get_Metadata_MailRefNr(bool saveOnDirty) {
      var res = Analyze_and_AddOrUpdate_MsgHdr_MailRefnr();
      if (saveOnDirty) { Save(); }

      return res;
   }


   /// <summary>
   /// Liest aus der Mail den Triage-Ordner
   /// Wenn er unbekannt ist, dann wird er berechnet
   /// </summary>
   /// <returns></returns>
   string? Get_Metadata_TriageOrdner(bool saveOnDirty) {
      var res = Analyze_and_AddOrUpdate_MsgHdr_TriageOrdner();

      if (saveOnDirty) { Save(); }

      return res;
   }


   #region Metadaten: Kopieren von anderen Mails

   #region Metadaten aus Strings

   /// <summary>
   /// Kopiert Metadaten: AppMailHeaderType.TriageOrdner
   /// </summary>
   /// <param name="dst"></param>
   /// <returns>
   /// false: Die Metadaten konnten nicht übertragen werden
   /// </returns>
   internal bool Copy_MsgHdr_TriageOrdner(BO_Mail dst) {
      if (!HasHeaderSet_TriageOrdner) { return false; }
      var srcTriageOrdner = Get_Metadata_TriageOrdner(false);

      if (srcTriageOrdner.ØHasValue()) {
         //+ Den Header setzen / aktualisieren
         dst.MsgHdr_UpdateOrSet_JigAppHeader(ConfigMail_MsgHeader.AppMailHeaderType.TriageOrdner, srcTriageOrdner!);

         return true;
      }

      return false;
   }


   /// <summary>
   /// Kopiert Metadaten: AppMailHeaderType.MailRefNr
   /// </summary>
   /// <param name="dst"></param>
   /// <returns>
   /// false: Die Metadaten konnten nicht übertragen werden
   /// </returns>
   internal bool Copy_MsgHdr_MailRefNr(BO_Mail dst) {
      if (!HasHeaderSet_MailRefNr) { return false; }
      var srcMailRefNr = Get_Metadata_MailRefNr(false);

      if (srcMailRefNr.ØHasValue()) {
         //+ Den Header setzen / aktualisieren
         dst.MsgHdr_UpdateOrSet_JigAppHeader(ConfigMail_MsgHeader.AppMailHeaderType.MailRefNr, srcMailRefNr!);

         return true;
      }

      return false;
   }


   /// <summary>
   /// Kopiert Metadaten: AppMailHeaderType.MailKontaktformularThema
   /// </summary>
   /// <param name="dst"></param>
   /// <returns>
   /// false: Die Metadaten konnten nicht übertragen werden
   /// </returns>
   internal bool Copy_MsgHdr_MailKontaktformularThema(BO_Mail dst) {
      if (!HasHeaderSet_MailKontaktformularThema) { return false; }
      var kontaktformularThema = Get_Metadata_MailKontaktformularThema(false);

      if (kontaktformularThema.ØHasValue()) {
         //+ Den Header setzen / aktualisieren
         dst.MsgHdr_UpdateOrSet_JigAppHeader
            (ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularThema, kontaktformularThema!);

         return true;
      }

      return false;
   }


   /// <summary>
   /// Kopiert Metadaten: AppMailHeaderType.SpracheMailInhalt
   /// </summary>
   /// <param name="dst"></param>
   /// <returns>
   /// false: Die Metadaten konnten nicht übertragen werden
   /// </returns>
   internal bool Copy_MsgHdr_SpracheMailInhalt(BO_Mail dst) {
      if (!HasHeaderSet_SpracheMailInhalt) { return false; }
      var spracheMailInhalt = Get_Metadata_SpracheMailInhalt(false);

      if (spracheMailInhalt.ØHasValue()) {
         //+ Den Header setzen / aktualisieren
         dst.MsgHdr_UpdateOrSet_JigAppHeader(ConfigMail_MsgHeader.AppMailHeaderType.SpracheMailInhalt, spracheMailInhalt!);

         return true;
      }

      return false;
   }

   #endregion Metadaten aus Strings

   #region Metadaten aus Objects

   /// <summary>
   /// Kopiert Metadaten: AppMailHeaderType.TriageHashtags
   /// </summary>
   /// <param name="dst"></param>
   /// <returns>
   /// false: Die Metadaten konnten nicht übertragen werden
   /// </returns>
   internal bool Copy_MsgHdr_TriageHashtags(BO_Mail dst) {
      if (!HasHeaderSet_TriageHashtags) { return false; }
      var metadata_TriageHashtags = Get_Metadata_TriageHashtags(false);

      if (metadata_TriageHashtags is { Count: > 0 }) {
         //+ Den Header setzen / aktualisieren
         dst.Set_MsgHdr_TriageHashtags(metadata_TriageHashtags);

         return true;
      }

      return false;
   }


   /// <summary>
   /// Kopiert Metadaten: AppMailHeaderType.FragestellerPersoenlicheDaten
   /// </summary>
   /// <param name="dst"></param>
   /// <returns>
   /// false: Die Metadaten konnten nicht übertragen werden
   /// </returns>
   internal bool Copy_MsgHdr_FragestellerPersoenlicheDaten(BO_Mail dst) {
      if (!HasHeaderSet_FragestellerPersoenlicheDaten) { return false; }
      var recFragestellerV01 = Get_Metadata_FragestellerPersoenlicheDaten(false);

      if (recFragestellerV01 != null) {
         //+ Den Header setzen / aktualisieren
         dst.Set_MsgHdr_FragestellerPersoenlicheDaten(recFragestellerV01);

         return true;
      }

      return false;
   }


   /// <summary>
   /// Kopiert Metadaten: AppMailHeaderType.MailKategorie
   /// </summary>
   /// <param name="dst"></param>
   /// <returns>
   /// false: Die Metadaten konnten nicht übertragen werden
   /// </returns>
   internal bool Copy_MsgHdr_MailKategorie(BO_Mail dst) {
      if (!HasHeaderSet_MailKategorie) { return false; }
      var mailKategorie = Get_Metadata_MailKategorie(false);

      if (mailKategorie != null) {
         //+ Den Header setzen / aktualisieren
         dst.Set_MsgHdr_MailKategorie(mailKategorie);

         return true;
      }

      return false;
   }

   #endregion Metadaten aus Objects

   #endregion Metadaten: Kopieren von anderen Mails

   #endregion BL, Business-Logic: Msg Header: TriageOrdner

   #region BL, Business-Logic: Msg Header: Mail RefNr

   /// <summary>
   /// Liest aus der Mail die MailRefnr
   /// Wenn sie unbekannt ist, dann wird sie berechnet
   /// </summary>
   /// <returns></returns>
   string? Get_MsgHdr_MailRefnr(bool saveOnDirty) {
      var res = Analyze_and_AddOrUpdate_MsgHdr_MailRefnr();

      if (saveOnDirty) { Save(); }

      return res;
   }


   /// <summary>
   /// Sucht die Mail RefNr
   /// Wenn Subject und Mail Body unterschiedliche RefNr haben,
   /// dann wird eine Exception erzeugt
   /// </summary>
   public string? Analyze_Mail_Get_MailRefNr() {
      var refNr_InSubject  = Get_RefNr_InSubject;
      var refNr_InMailBody = Get_RefNr_InMailBody;

      //+ Wenn Subject & Body eine unterschiedliche RefNr haben
      if (refNr_InSubject.ØHasValue()
          && refNr_InMailBody.ØHasValue()
          && !refNr_InSubject.ØEqualsIgnoreCase(refNr_InMailBody)
         ) {

         // throw new RuntimeException
         //   ($"BO_Mail.Get_RefNr: Unterschiedliche Refnr in Subject / Body: {refNr_InSubject} / {refNr_InMailBody}");

         // ToDo 🟥 Es passiert tatsächlich, dass Body & Subject unterschiedliche RefNr haben
         // Lösungsvarianten:
         // Einfach eine zurückgeben
         // Den Msg Header ändern und eine Liste von Refnr speichern
         return refNr_InSubject;
      }

      //+ Das Resultat zurückgeben
      return refNr_InSubject.ØHasValue() ? refNr_InSubject : refNr_InMailBody;

   }

   #endregion BL, Business-Logic: Msg Header: Mail RefNr

   #endregion BL, Business-Logic: Msg Header

   #region BL, Business-Logic: Msg Body

   /// <summary>
   /// Aktualisiert, wenn nötig, die Msg Header
   /// </summary>
   [Obsolete("Diese Funktion ist obsolete » BO_Mail_BL_UpdateOrSet_Metadata_MsgBody", false)]
   public void Update_Mail_Body(bool saveOnDirty = false, bool force = false) {

      //+ Bei HTML Mails im Html Header demn Style aktualisieren
      // AddOrUpdate_MailMsgBody_Style();

      //+ Im Mail Body die HashTags aktualisieren
      // Update_Mail_Body_HashTags(force: force);

      //+ Im Mail Body das Autor #HashTag setzen 
      // Update_Mail_Body_Autor_HashTags(force: force);

      if (saveOnDirty && IsDirty) { Save(); }
   }

   #endregion BL, Business-Logic: Msg Body

   #region BL, Business-Logic: Mail HTML Body

   /// <summary>
   /// Liefert True, wenn Msg einen HTML Body hat
   /// </summary>
   /// <returns></returns>
   internal bool Is_MsgBody_Html() {
      // Allenfalls versuchen, die Nacchricht zu laden
      if (!HasMsg) { Srv_Load_Msg_Data(); }

      var (_, htmlPart) = MimeKit_JigTools.Get_Msg_Bodies(Msg!, true);

      return htmlPart.ØHasValue();
   }

   #endregion BL, Business-Logic: Mail HTML Body

   #region BL, Business-Logic: HashTags / Hash-Tagging Body & Msg Header

   #endregion BL, Business-Logic: HashTags / Hash-Tagging Body & Msg Header

   #region BL, Business-Logic: Anonymisieren

   /// <summary>
   /// Anonymisiert die E-Mail:
   /// - HTML Mail Body
   /// - Text Mail Body
   /// - Mail Header
   ///
   /// Die Mail wird gespeichert, wenn sie verändert wurde
   /// 
   /// </summary>
   public void Anonymize_Mail(bool saveOnDirty = true, bool force = false) {
      //+ App-Spezifische E-Mails anonymisieren wir nie
      if (IsAppSpecificMail) { return; }

      //+ Hat die Msg den Header bereits?
      if (!force && Has_MsgHdr_Anonymized()) { return; }

      //+ Anonymisieren
      BL_Anonymize_Mail.Anonymize_Mail(this);

      //+ Im Header die Anonymisierung markieren
      Set_MsgHdr_Anonymized();

      //+ Speichern wenn verändert
      if (saveOnDirty && IsDirty) { Save(); }
   }


   /// <summary>
   /// Setzt den Msg Header Anonymisiert 
   /// </summary>
   /// <returns>
   /// True, wenn die Msg verändert wurde
   /// </returns>
   private void Set_MsgHdr_Anonymized() {
      //+ Hat die Msg den Header bereits?
      if (Has_MsgHdr_Anonymized()) { return; }

      //+ Den Header setzen
      MsgHdr_UpdateOrSet_JigAppHeader(ConfigMail_MsgHeader.AppMailHeaderType.MailInhaltAnonymisiert, "");
   }


   /// <summary>
   /// Liefert true, wenn die Msg den Header hat:
   /// AppMailHeaderType.MailInhaltAnonymisiert
   /// </summary>
   /// <returns></returns>
   internal bool Has_MsgHdr_Anonymized() {
      //+ Den Msg Header suchen 
      var (_, _, foundMsgSummaryHdrs, foundMsgHdrs) = Search_JigAppHeader
         (ConfigMail_MsgHeader.AppMailHeaderType.MailInhaltAnonymisiert);

      return foundMsgSummaryHdrs.Count > 0 || foundMsgHdrs.Count > 0;
   }

   #endregion BL, Business-Logic: Anonymisieren

   #region !To Msg Header der Applikation / AppMailHeaderType

   #region Msg Header Basis-Funktionen

   /// <summary>
   /// Liefert alle Msg Header mit dem angegebenen Namen
   /// Geprüft werden diese Header:
   ///   - MsgSummary!.Headers
   ///   - Msg!.Headers
   /// </summary>
   /// <param name="hdrName"></param>
   /// <returns>
   /// Gibt zurück, wenn gefunden:
   /// - mimekitObjHeaders:
   ///   Die Quelle mit den Headern
   ///   (MsgSummary!.Headers oder Msg!.Headers)
   /// - foundAppheaders:
   ///   Die Liste der passenden App-Header
   /// 
   /// var (MsgSummaryHdrs, MsgHdrs, foundMsgSummaryHdrs, foundMsgHdrs) = MsgHdr_Search()
   ///  
   /// </returns>
   /// <exception cref="RuntimeException"></exception>
   private Tuple<HeaderList?, HeaderList?, List<Header>, List<Header>> MsgHdr_Search(
      string hdrName) {
      //+ Allenfalls die Daten laden
      if (!HasMsgSummaryHeaders && !HasMsg) {
         //++ Können wir die Server-Daten lesen? 
         if (CanGetServerData) {
            Srv_Load_Msg_SummaryData();
         }
         else {
            throw new RuntimeException
               ("public bool Set_JigAppHeader(): Weder MsgSummary noch Msg sind vohanden");
         }
      }

      HeaderList? allMsgSummaryHdrs = null;
      HeaderList? allMsgHdrs        = null;

      List<Header> foundMsgSummaryHdrs = new List<Header>();
      List<Header> foundMsgHdrs        = new List<Header>();

      //+ Zuerst MsgSummary prüfen
      if (HasMsgSummaryHeaders) {
         allMsgSummaryHdrs = MsgSummary!.Headers;

         //++ Finden wir den App header?
         foundMsgSummaryHdrs = MsgSummary!.Headers.Where
            (h => h.Field == hdrName).ToList();
      }

      //+ Sonst Msg prüfen
      if (HasMsg) {
         allMsgHdrs = Msg!.Headers;

         //++ Finden wir den App header?
         foundMsgHdrs = Msg!.Headers.Where
            (h => h.Field == hdrName).ToList();
      }

      return new Tuple<HeaderList?, HeaderList?, List<Header>, List<Header>>
         (
          allMsgSummaryHdrs
          , allMsgHdrs
          , foundMsgSummaryHdrs
          , foundMsgHdrs);

   }


   /// <summary>
   /// Aktualisiert oder setzt einen Msg Header
   /// </summary>
   /// <param name="hdrName"></param>
   /// <param name="newValue"></param>
   /// <returns></returns>
   /// <exception cref="RuntimeException"></exception>
   public bool MsgHdr_UpdateOrSet(
      string   hdrName
      , string newValue) {

      //» Die Msg zwingend laden, weil beim Speichern nur sie zum Server übertragen wird
      if (!HasMsg) {
         //++ Können wir die Server-Daten lesen? 
         if (CanGetServerData) {
            Srv_Load_Msg_Data();
         }
         else {
            throw new RuntimeException
               ("public bool Set_JigAppHeader(): Weder MsgSummary noch Msg sind vohanden");
         }
      }

      //+ Prüfen, ob wir bereits einen Header mit diesem Wert haben
      var (MsgSummaryHdrs, MsgHdrs, foundMsgSummaryHdrs, foundMsgHdrs) = MsgHdr_Search(hdrName);

      // 0 bis 1 (und max 1) App Header haben wir gefunden
      // Man könnte mit dem gleichen Namen mehrere Msg Header haben, aber diese App erwartet immer nur einen 
      Assert.IsTrue(foundMsgSummaryHdrs is { Count: <= 1 });
      Assert.IsTrue(foundMsgHdrs is { Count       : <= 1 });

      //+ Param testen
      if (!newValue.ØHasValue()) { newValue = ""; }

      //+ Wenn wir den App Header schon haben, stimmt der Wert bereits?
      var foundMsgSummaryHdrsHasItems = foundMsgSummaryHdrs.ØHasItems();
      var foundMsgHdrsHasItems        = foundMsgHdrs.ØHasItems();
      var msgSummaryHeaderData_IsOk   = foundMsgSummaryHdrs.Any(x => x.Value.ØEqualsIgnoreCase(newValue));
      var msgHeaderData_IsOk          = foundMsgHdrs.Any(x => x.Value.ØEqualsIgnoreCase(newValue));

      //++ Ist der Msg Summary Header i.O.?
      bool isMsgSummaryHdr_IsOk
         = (foundMsgSummaryHdrsHasItems, MsgSummaryHeaderData_IsOK: msgSummaryHeaderData_IsOk) switch {
            // Der Header existiert und stimmt bereit
            { foundMsgSummaryHdrsHasItems: true, MsgSummaryHeaderData_IsOK: true } => true

            // Der Header existiert und ist falsch
            , { foundMsgSummaryHdrsHasItems: true, MsgSummaryHeaderData_IsOK: false } => false

            // Der Header fehlt: wenn der andere Header existiert, ist alles OK (wird im nächsten Schritt getestet)
            , _ => true
         };

      bool isMsgHdr_IsOk
         = (foundMsgHdrsHasItems, MsgHeaderData_IsOK: msgHeaderData_IsOk) switch {
            // Der Header existiert und stimmt bereit
            { foundMsgHdrsHasItems: true, MsgHeaderData_IsOK: true } => true

            // Der Header existiert und ist falsch
            , { foundMsgHdrsHasItems: true, MsgHeaderData_IsOK: false } => true

            // Der Header fehlt: wenn der andere Header existiert, ist alles OK (wird im nächsten Schritt getestet)
            , _ => true
         };

      //+ Stimmen die Daten im Header bereits?
      //++ Mind. einer der Header muss existieren
      if (foundMsgSummaryHdrsHasItems || foundMsgHdrsHasItems) {
         //++ Alle existierenden Header haben die richtigen Daten 
         if (isMsgSummaryHdr_IsOk && isMsgHdr_IsOk) {
            // nichts geändert, alle existierenden Header haben bereits die richtigen Werte
            return false;
         }
      }

      //+ Den Header aktualisieren
      IsDirty = true;

      //++ Den MsgSummary Header aktualisieren
      if (foundMsgSummaryHdrsHasItems && !msgSummaryHeaderData_IsOk) {
         //+++ Den Wert aktualisieren
         foundMsgSummaryHdrs.ForEach(x => x.Value = newValue);
      }
      else {
         //+++ In den Msg Header die App-Header erzeugen
         // ‼ Wenn wir beide Header haben (Msk Summary und Msg selber), dann müssen beide aktualisiert werden
         // » Wenn nur der Msg Summary hdr aktualisert wird, dann kriegt die Mail beim Speichern die Ändeurng nicht mit,
         //   ev. würde es umgekehrt klappen.
         // Aber so ist sicher:
         MsgSummaryHdrs?.Add(hdrName, newValue);
      }

      //++ Den Msg Header aktualisieren
      if (foundMsgSummaryHdrsHasItems && !msgSummaryHeaderData_IsOk) {
         //+++ Den Wert aktualisieren
         foundMsgHdrs.ForEach(x => x.Value = newValue);
      }
      else {
         //+++ In den Msg Header die App-Header erzeugen
         // ‼ Wenn wir beide Header haben (Msk Summary und Msg selber), dann müssen beide aktualisiert werden
         // » Wenn nur der Msg Summary hdr aktualisert wird, dann kriegt die Mail beim Speichern die Ändeurng nicht mit,
         //   ev. würde es umgekehrt klappen.
         // Aber so ist sicher:
         MsgHdrs?.Add(hdrName, newValue);
      }

      return true;

   }

   #endregion Msg Header Basis-Funktionen

   #region Jig Header

   /// <summary>
   /// Liefert True, wenn wir den gesuchten App-Header in der Mail haben
   /// </summary>
   /// <returns></returns>
   private bool Has_JigAppHeader(ConfigMail_MsgHeader.AppMailHeaderType appMailHeaderType) {
      var (MsgSummaryHdrs, MsgHdrs, foundMsgSummaryHdrs, foundMsgHdrs) = Search_JigAppHeader(appMailHeaderType);

      return foundMsgSummaryHdrs.Count > 0 || foundMsgHdrs.Count > 0;
   }


   /// <summary>
   /// Liefert den ersten Header zurück, der passt.
   /// Stellt sicher, dass vom AppMailHeaderType max 1 Header existiert
   /// </summary>
   /// <param name="appMailHeaderType"></param>
   /// <returns></returns>
   public Header? Search_JigAppHeader_GetSingleton(ConfigMail_MsgHeader.AppMailHeaderType appMailHeaderType) {
      var (_, _, foundMsgSummaryHdrs, foundMsgHdrs) = Search_JigAppHeader(appMailHeaderType);

      // Sicherstellen, dass max. 1 Header gefunden wurde
      Assert.IsTrue(foundMsgSummaryHdrs.Count <= 1);
      Assert.IsTrue(foundMsgHdrs.Count <= 1);

      //+ Den ersten gefundenen Header zurückgebem
      //++ Wenn vorhanden, dann den Msg.Header zurückgeben, weil er writeable ist
      if (foundMsgHdrs.Count > 0) { return foundMsgHdrs[0]; }

      //++ Sonst den MsgSummary Hdr 
      if (foundMsgSummaryHdrs.Count > 0) { return foundMsgSummaryHdrs[0]; }

      //++ Nicht gefunden
      return null;
   }


   /// <summary>
   /// Liefert alle passenden App-Header der Mail
   /// Geprüft werden diese Header:
   ///   - MsgSummary!.Headers
   ///   - Msg!.Headers
   /// 
   /// </summary>
   /// <param name="appMailHeaderType"></param>
   /// <returns>
   /// Gibt zurück, wenn gefunden:
   /// - mimekitObjHeaders:
   ///   Die Quelle mit den Headern
   ///   (MsgSummary!.Headers oder Msg!.Headers)
   /// - foundAppheaders:
   ///   Die Liste der passenden App-Header
   /// 
   /// var (MsgSummaryHdrs, MsgHdrs, foundMsgSummaryHdrs, foundMsgHdrs) = Search_JigAppHeader()
   ///  
   /// </returns>
   private Tuple<HeaderList?, HeaderList?, List<Header>, List<Header>> Search_JigAppHeader(
      ConfigMail_MsgHeader.AppMailHeaderType appMailHeaderType) {
      return MsgHdr_Search(ConfigMail_MsgHeader.GetHeadername(appMailHeaderType));
   }


   /// <summary>
   /// Entfernt alle Msg Hdr vom appMailHeaderType
   /// </summary>
   /// <param name="appMailHeaderType"></param>
   public void Remove_JigAppHeader(
      ConfigMail_MsgHeader.AppMailHeaderType appMailHeaderType) {
      //» Unbedingt die Msg laden, weil beim Speichern nur sie zum Server übertragen wird 
      if (!HasMsg) {
         //++ Können wir die Server-Daten lesen? 
         if (CanGetServerData) {
            Srv_Load_Msg_Data();
         }
         else {
            throw new RuntimeException
               ("public bool Set_JigAppHeader(): Weder MsgSummary noch Msg sind vohanden");
         }
      }

      //+ Den Header anpassen
      var hdrName = ConfigMail_MsgHeader.GetHeadername(appMailHeaderType);

      if (HasMsgSummaryHeaders) {
         if (MsgSummary!.Headers.Contains(hdrName)) {
            IsDirty = true;
            MsgSummary!.Headers.RemoveAll(hdrName);
         }
      }

      if (Msg!.Headers.Contains(hdrName)) {
         IsDirty = true;
         Msg!.Headers.RemoveAll(hdrName);
      }
   }


   /// <summary>
   /// Setzt oder aktualisiert den App-Header im Mail Header
   /// Aktualisiert, wenn vorhanden:
   /// - MsgSummary.Headers
   /// - Msg.Headers
   ///
   /// ‼ Markiert dieses Objekt als dirty, wenn Daten verändert werden
   /// </summary>
   /// <param name="appMailHeaderType"></param>
   /// <param name="newValue"></param>
   /// <exception cref="RuntimeException"></exception>
   /// <returns>
   /// True: Daten wurden aktualisiert
   /// </returns>
   private bool MsgHdr_UpdateOrSet_JigAppHeader(
      ConfigMail_MsgHeader.AppMailHeaderType appMailHeaderType
      , string                               newValue) {

      //‼ Unbedingt die Msg und nicht nur die MsgSummary aktualisieren, weil beim Speichern die Msg zum Server übertragen wird
      if (!HasMsg) {
         //++ Können wir die Server-Daten lesen? 
         if (CanGetServerData) { Srv_Load_Msg_Data(); }
         else {
            throw new RuntimeException
               ("public bool Set_JigAppHeader(): Weder MsgSummary noch Msg sind vohanden");
         }
      }

      //+ Prüfen, ob wir bereits in der Msg(!) ein Prop mit diesem Wert haben
      var (MsgSummaryHdrs, MsgHdrs, foundMsgSummaryHdrs, foundMsgHdrs) = Search_JigAppHeader
         (appMailHeaderType);

      // 0 bis 1 (und max 1) App Header haben wir gefunden
      // Man könnte mit dem gleichen Namen mehrere Msg Header haben, aber diese App erwartet immer nur einen 
      Assert.IsTrue(foundMsgSummaryHdrs is { Count: <= 1 });
      Assert.IsTrue(foundMsgHdrs is { Count       : <= 1 });

      //+ Param testen
      if (!newValue.ØHasValue()) { newValue = ""; }

      //+ Stimmen existierende Header?
      var foundMsgHdrsHasItems        = foundMsgHdrs.ØHasItems();
      var foundMsgSummaryHdrsHasItems = foundMsgSummaryHdrs.ØHasItems();

      var msgHeaderData_IsOk        = foundMsgHdrs.Any(x => x.Value.ØEqualsIgnoreCase(newValue));
      var msgSummaryHeaderData_IsOk = foundMsgSummaryHdrs.Any(x => x.Value.ØEqualsIgnoreCase(newValue));

      //++ Ist der Msg Header i.O.?
      bool isMsgHdr_IsOk
         = (foundMsgHdrsHasItems, MsgHeaderData_IsOK: msgHeaderData_IsOk) switch {
            // Der Header existiert und stimmt bereit
            { foundMsgHdrsHasItems: true, MsgHeaderData_IsOK: true } => true

            // Der Header existiert und ist falsch
            , { foundMsgHdrsHasItems: true, MsgHeaderData_IsOK: false } => true

            // Der Header fehlt: wenn der andere Header existiert, ist alles OK (wird im nächsten Schritt getestet)
            , _ => true
         };

      //+ Stimmen die Daten im Header bereits?
      if (foundMsgHdrsHasItems && isMsgHdr_IsOk) {
         // nichts geändert, alle existierenden Header haben bereits die richtigen Werte
         return false;
      }

      //+ Den Header aktualisieren
      IsDirty = true;

      //++ Auch den MsgSummary Header aktualisieren, damit er die gleichen Daten hat
      if (foundMsgSummaryHdrsHasItems && !msgSummaryHeaderData_IsOk) {
         //+++ Den Wert aktualisieren
         foundMsgSummaryHdrs.ForEach(x => x.Value = newValue);
      }
      else {
         //+++ In den Msg Header die App-Header erzeugen
         MsgSummaryHdrs?.Add
            (ConfigMail_MsgHeader.GetHeadername
                (appMailHeaderType)
             , newValue);
      }

      //++ Den Msg Header aktualisieren
      if (foundMsgSummaryHdrsHasItems && !msgSummaryHeaderData_IsOk) {
         //+++ Den Wert aktualisieren
         foundMsgHdrs.ForEach(x => x.Value = newValue);
      }
      else {
         //+++ In den Msg Header die App-Header erzeugen
         //   ev. würde es umgekehrt klappen.
         // Aber so ist sicher:
         MsgHdrs?.Add
            (ConfigMail_MsgHeader.GetHeadername
                (appMailHeaderType)
             , newValue);
      }

      return true;
   }

   #endregion Jig Header

   #endregion !To Msg Header der Applikation / AppMailHeaderType

   #endregion Methoden: Public, d.h. BL, Business-Logic

   #region Methoden: Hilfsfuktionen Mail Body

   #endregion Methoden: Hilfsfuktionen Mail Body

   #region Methoden: Hilfsfuktionen allgemein

   #region Save

   /// <summary>
   /// Speichert die E-Mail ein eine EML-Datei
   /// und speichert allenfalls die Anhänge separat
   /// </summary>
   /// <param name="basePath"></param>
   /// <param name="addTimeStamp"></param>
   /// <param name="addMbxOrdnerPhath"></param>
   public void Save(
      string basePath
      , bool addTimeStamp
      , bool addMbxOrdnerPhath
      , bool saveAttachments) {
      // Allenfalls die Nachricht zu laden
      if (!HasMsg) { Srv_Load_Msg_Data(); }

      //! Das Muster des Mail-Dateinamen:
      // Nummer mit zwei Digits und führender 0, kann auch e.g. D5 sein
      const string dateiNrMuster            = "{0:D2}";
      const string dateiNrMusterPlatzhalter = "᙭MusterDateinummer᙭";

      //+ Parameter bereinigen
      //++ Der übergebene Pfad bereinigen  
      basePath = basePath.ØStringToFilename();

      //++ Den Mail TimeStamp berechnen  
      var mailTimeStamp = "";

      if (addTimeStamp) {
         if (Msg.Date != DateTimeOffset.MinValue) {
            mailTimeStamp = $"{Msg.Date.ØGet_TimeStamp()} • ";
         }
         else {
            mailTimeStamp = $"{Get_ActualTimeStamp()} • ";
         }
      }

      //++ Das Unterverzeichnis der Mbx Ordners berechnen
      var mbxSubDir = "";

      if (addMbxOrdnerPhath) {
         mbxSubDir = MailFolder!.FullName;

         // Pfad bereinigen
         mbxSubDir = mbxSubDir.ØStringToFilename();
      }

      //++ Das Zielverzeichnis erstellen
      var zielDir  = Path.Join(basePath, mbxSubDir);
      var oZielDir = Directory.CreateDirectory(zielDir);

      //+ Den Basis-Dateinamen berechnen
      string fileBaseName = Mail_Subject.ØHasValue() ? Mail_Subject : "Mail";
      fileBaseName = $"{mailTimeStamp}{fileBaseName}".ØStringToFilename();

      //+ Das Muster mit der optionalen Nummerierung ergänzen
      fileBaseName = $"{fileBaseName} {dateiNrMusterPlatzhalter}"
        .Replace(dateiNrMusterPlatzhalter, dateiNrMuster);

      var    mailFileName = $"{fileBaseName}.eml";
      string zielDatei    = Path.Join(zielDir, mailFileName);

      //++ Sicherstellen, dass die Datei eindeutig ist
      zielDatei = ExtensionMethods_IO_Path.GetUniqueNumberedFilename(zielDatei);

      //++ Speichern
      Msg!.WriteTo(zielDatei);

      //+ Die Anhänge speichern
      if (saveAttachments) {

         //! !KH9: Why do attachments with unicode filenames appear as "ATT0####.dat" in Outlook?
         // http://www.mimekit.net/docs/html/Frequently-Asked-Questions.htm#UntitledAttachments

         //++ Den fileBaseName für die Anhänge berechnen
         var fileBaseNameAttachments = Path.Join(Path.GetDirectoryName(zielDatei), Path.GetFileNameWithoutExtension(zielDatei));
         int fileNo                  = 1;

         foreach (var attachment in Msg!.Attachments) {
            if (attachment is MessagePart) {
               //+ MessagePart Anhang
               var fileName = attachment.ContentDisposition?.FileName;
               var rfc822   = (MessagePart)attachment;

               if (!fileName.ØHasValue()) {
                  fileName = $"{fileBaseNameAttachments} - Anhang {fileNo++:00}.eml";
               }

               // Die Dateinummer einbauen
               fileName = $"{Path.GetDirectoryName(fileName)} {dateiNrMusterPlatzhalter}{Path.GetFileName(fileName)}"
                 .Replace(dateiNrMusterPlatzhalter, dateiNrMuster);
               zielDatei = $"{fileBaseNameAttachments} - {fileName}";

               //++ Sicherstellen, dass die Datei eindeutig ist
               zielDatei = ExtensionMethods_IO_Path.GetUniqueNumberedFilename(zielDatei);

               using var stream = File.Create(zielDatei);
               rfc822.Message.WriteTo(stream);
            }
            else {
               //+ MimePart Anhang
               var part     = (MimePart)attachment;
               var fileName = part.FileName;

               // Die Dateinummer einbauen
               fileName = $"{Path.GetDirectoryName(fileName)} {dateiNrMusterPlatzhalter}{Path.GetFileName(fileName)}"
                 .Replace(dateiNrMusterPlatzhalter, dateiNrMuster);

               zielDatei = $"{fileBaseNameAttachments} - {fileName}";

               //++ Sicherstellen, dass die Datei eindeutig ist
               zielDatei = ExtensionMethods_IO_Path.GetUniqueNumberedFilename(zielDatei);
               using var stream = File.Create(zielDatei);
               part.Content.DecodeTo(stream);
            }
         }

      }

   }


   /// <summary>
   /// Wenn die Mail verändert wurde, dann speichern
   /// Überträgt auf Wunsch das read-Flag auf die neue Msg 
   /// </summary>
   /// <param name="copyMarkReadFlag"></param>
   /// <param name="forceSave">
   /// Wenn das Objekt unabhöngig von den Klassen-Props & Funktionen verändert wird,
   /// kann via forceSave das speichern erzwungen werden
   /// </param>
   /// <param name="internalDate">
   /// Vermutlich das interne "Erhalten"-Datum der Mail
   /// Siehe: INTERNALDATE in:
   /// https://www.rfc-editor.org/rfc/rfc3501#page-57
   /// </param>
   /// <returns>
   /// Die neue UniqueID
   /// </returns>
   public UniqueId? Save(bool copyMarkReadFlag = true, bool forceSave = false, DateTimeOffset? internalDate = null) {
      //+ Nichts verändert
      if (!forceSave && !IsDirty) return MsgUniqueId;

      var oriMsgUniqueID = MsgUniqueId;

      //+ Die Mailbox zum Schreiben vorbereiten
      MailFolder!.Open(FolderAccess.ReadWrite);

      //+ Die Msg dem Ordner zufügen
      UniqueId? newUid;

      if (internalDate.HasValue) {
         if (!HasMsgSummary) {
            Srv_Load_Msg_SummaryData();
         }

         Assert.IsNotNull(MsgSummary.Flags);
         newUid = MailFolder.Append(Msg, MsgSummary.Flags.Value, internalDate.Value);
      }
      else {
         newUid = MailFolder.Append(Msg);
      }

      //+ Das Read-Flag kopieren?
      if (newUid.HasValue
          && copyMarkReadFlag
          && MimeKit_JigTools.Is_Msg_Marked_AsRead(MsgSummary!)) {
         MailFolder.AddFlags
            (newUid.Value
             , MessageFlags.Seen
             , true);
      }

      //+ Im Ordner die alte Nachricht als gelöscht markieren
      MailFolder.AddFlags
         (oriMsgUniqueID
          , MessageFlags.Deleted
          , true);

      // Wenn möglich, die alte Message endgültig aus dem Ordner löschen
      if (ImapSrvRuntimeConfig!.ImapClient.Capabilities.HasFlag
             (ImapCapabilities.UidPlus)) { MailFolder.Expunge(new[] { oriMsgUniqueID }); }

      // ‼ Den Ordner schliessen, speichert vermutlich alle Daten
      // und Expunge ausführen
      // !KH http://www.mimekit.net/docs/html/M_MailKit_IMailFolder_Close.htm
      // > Closes the folder, optionally expunging the messages marked for deletion.
      MailFolder!.Close(true);

      // Alles gespeichert
      IsDirty = false;

      // ‼ Die Daten neu laden, weil sie geändert haben
      Srv_Load_Msg_SummaryData(newUid, true);

      // Die Msg wird bei Bedarf neu geladen
      Msg = null;

      // 🚩 Debug
      // Msg.WriteTo(@"c:\Temp\Mimekit-Test-Mail.eml");

      return newUid;
   }

   #endregion Save


   /// <summary>
   /// Vorsicht, löscht die E-Mail!
   ///
   /// Wichtiges Konzept:
   /// https://stackoverflow.com/a/43521939/4795779
   /// </summary>
   public void Delete() {

      // ! Nur in Testumgebungen löschen
      if (ImapSrvRuntimeConfig is { MailboxIsTest: true }) {
         if (HasMsgUniqueId) {

            //+ Die Mailbox zum Schreiben vorbereiten
            MailFolder!.Open(FolderAccess.ReadWrite);

            MailFolder.AddFlags
               (MsgUniqueId
                , MessageFlags.Deleted
                , true);

            // Wenn möglich, die alte Message endgültig aus dem Ordner löschen
            if (ImapSrvRuntimeConfig!.ImapClient.Capabilities.HasFlag
                   (ImapCapabilities.UidPlus)) { MailFolder.Expunge(new[] { MsgUniqueId }); }

         }
      }
   }


   /// <summary>
   /// Aktualsiert die MessageSummary Daten 
   /// </summary>
   /// <param name="newUniqueId">
   /// Wenn definiert, wird die MsgSummary der neuen ID geladen,
   /// sonst jene von der aktuellen ID
   /// </param>
   /// <param name="force"></param>
   internal void Srv_Load_Msg_SummaryData(UniqueId? newUniqueId = null, bool force = false) {
      if (!force && HasMsgSummary) { return; }

      // Den Ordner öffnen
      MailFolder!.Open(FolderAccess.ReadOnly);

      //+ Die MessageSummary Daten für diese E-Mail neu vom Server lesen
      // ‼ Vorsicht, langsam für einzelne E-Mails!
      // !M http://www.mimekit.net/docs/html/T_MailKit_IMailFolder.htm
      IList<IMessageSummary>? messageSummariesSrvData = MailFolder!.ØFetch

         // Wenn definiert, die neue newUniqueId nützen, sonst die bisherige
         (new List<UniqueId>() { newUniqueId ?? MsgUniqueId }
          , MessageSummaryItems.Envelope
            | MessageSummaryItems.BodyStructure
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Size
            | MessageSummaryItems.Headers
            | MessageSummaryItems.Flags

          //+++ Wichtige Header auch lesen
          , ConfigMail_MsgHeader.Get_DefaultEMailHeader_Names());

      Assert.IsTrue(messageSummariesSrvData.Count == 1);

      // Die gelesenen Daten übernehmen
      MsgSummary = messageSummariesSrvData[0];
   }


   /// <summary>
   /// Aktualsiert die Message Daten 
   /// </summary>
   internal void Srv_Load_Msg_Data(bool force = false) {
      if (force == false && HasMsg == true) {
         return;
      }

      if (!CanGetServerData) {
         throw new RuntimeException
            ("Srv_Read_Msg_Data(): Keine Server-Verbindung möglich");
      }

      //+ Die Message Daten vom Server lesen
      MimeMessage? msgSrvData = MailFolder!.GetMessage(this.MsgUniqueId);
      Assert.IsTrue(msgSrvData != null);

      // Die gelesenen Daten übernehmen
      Msg = msgSrvData;
   }

   #endregion Methoden: Hilfsfuktionen allgemein

}
