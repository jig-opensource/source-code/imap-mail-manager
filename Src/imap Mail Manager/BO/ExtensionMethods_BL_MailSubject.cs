using System;
using System.Text;
using jig.Tools;
using jig.Tools.String;


namespace ImapMailManager.BO;

/// <summary>
/// Funktionen zum E-Mail Subject
/// - Normalisieren des betreffs, d.h. Re: WG: etc. bereinigen
/// </summary>
public static class ExtensionMethods_BL_MailSubject {


   /// <summary>
   /// 
   /// </summary>
   /// <param name="subject"></param>
   /// <param name="prependErstesStandardsiertesSubjectPräfix">
   /// Soll im bereinigten Betreff das erste gefundene Präfix standardisiert und dann wieder zugefügt werden?
   /// </param>
   /// <param name="prependLetztesStandardsiertesSubjectPräfix">
   /// Soll im bereinigten Betreff das letzte gefundene Präfix standardisiert und dann wieder zugefügt werden?
   /// Es wird nur zugefügt, wenn es unterschiedlich zum ersten Präfix ist
   /// </param>
   /// <returns></returns>
   public static string ØCleanEmailSubjectPrefix(
      this string? subject
      , bool       prependErstesStandardsiertesSubjectPräfix  = true
      , bool       prependLetztesStandardsiertesSubjectPräfix = true) {

      if (!subject.ØHasValue()) { return ""; }

      return BL_MailSubject.ØCleanEmailSubjectPrefix
         (subject
          , prependErstesStandardsiertesSubjectPräfix
          , prependLetztesStandardsiertesSubjectPräfix);
      
   }


   /// <summary>
   /// Entfernt von einem EMail Betreff RE: FWD: etc.
   /// und erkennt verschachtelte Präfixes
   /// 
   /// !Ex
   ///   Aw: Re: #F2-1442 • Biblische Frage: Calvinismus
   ///   #F2-1442 • Biblische Frage: Calvinismus
   /// 
   ///   Re: Antwort: #81-1736 . RL Formular: Biblische Frage
   ///   #81-1736 . RL Formular: Biblische Frage
   /// 
   /// </summary>
   /// <param name="str"></param>
   /// <param name="prependStandardsiertesSubjectPräfix">
   /// Wenn true, dann wird versucht, aufgrund des Betreffs zu erkennen,
   /// ob die E-Mail eine Antwort oder eine weitergeleitete E-Mail ist.
   /// 
   /// Diese Information wird dann im bereinigten Betreff
   /// wieder mit standardisierten Präfixes zugefügt
   /// </param>
   /// <returns></returns>
   public static string ØCleanEmailSubjectPrefix_Nested(
      this string? subject
      , bool       prependStandardsiertesSubjectPräfix = true) {

      // Todo 🟥 !Ex dokumentieren!
      if (subject.ØHasValue()) {
         // Bereinigen
         subject = subject.Trim();

         //+ Wenn wir das erste Präfix standardisiert ergänzen, dann es erkennen
         BL_MailSubject.EmpfangenerEMailTyp erstesPräfixStandardisiert = BL_MailSubject.EmpfangenerEMailTyp.Unknown;

         if (prependStandardsiertesSubjectPräfix) {
            erstesPräfixStandardisiert = BL_MailSubject.Get_EmpfangenerEMailTyp(subject);
         }

         // Regex verarbeiten, bis keiner mehr passt
         string startSubject;

         do {
            startSubject = subject;
            var matches4 = BL_MailSubject.ORgxMailSubject_NestedEckigeKlammerPrefixDetector.Match(subject);

            if (matches4.Success) {
               var sb = new StringBuilder();

               if (matches4.Groups["Pre"].Value.ØHasValue()) {
                  sb.Append(matches4.Groups["Pre"].Value);
               }

               if (matches4.Groups["InnerScriptBlock"].Value.ØHasValue()) {
                  sb.Append(matches4.Groups["InnerScriptBlock"].Value);
               }

               if (matches4.Groups["Post"].Value.ØHasValue()) {
                  sb.Append(matches4.Groups["Post"].Value);
               }

               subject = sb.ToString();
            }
         }
         while (!startSubject.Equals(subject, StringComparison.InvariantCultureIgnoreCase));

         // Die restlichen 3 Regex verarbeiten, bis keiner mehr passt
         do {
            startSubject = subject;
            var matches1 = BL_MailSubject.ORgxMailSubject_SimplePrefixDetector.Match(subject);

            if (matches1.Success) {
               subject = matches1.Groups["Subj"].Value;
            }

            var matches2 = BL_MailSubject.ORgxMailSubject_EckigeKlammerPrefixDetector.Match(subject);

            if (matches2.Success) {
               subject = matches2.Groups["Subj"].Value;
            }

            var matches3 = BL_MailSubject.ORgxMailSubject_RundeKlammerPrefixDetector.Match(subject);

            if (matches3.Success) {
               // subject = matches3.Groups["Subj"].Value;
               var sb = new StringBuilder();

               if (matches3.Groups["Pre"].Value.ØHasValue()) {
                  sb.Append(matches3.Groups["Pre"].Value);
               }

               if (matches3.Groups["Post"].Value.ØHasValue()) {
                  sb.Append(matches3.Groups["Post"].Value);
               }

               subject = sb.ToString();
            }
         }
         while (!startSubject.Equals(subject, StringComparison.InvariantCultureIgnoreCase));

         subject = subject.Trim();

         //+ Wenn wir das standardisierte Subject Präfix ergänzen
         if (prependStandardsiertesSubjectPräfix) {
            subject = BL_MailSubject.Prepend_Subject_With_StandardizedPrefix(subject, erstesPräfixStandardisiert);
         }

         return subject.ØTrimAndReduce();
      }

      return "";
   }

}
