using System;
using ImapMailManager.Config;
using ImapMailManager.Properties;
using jig.Tools.Exception;
using jig.Tools.String;
using NPOI.SS.Formula.Functions;


namespace ImapMailManager.BO;

/// <summary>
/// Bildet im Msg Hdr 
/// die Daten mit den Infos des Autors einer Antwort vom Team am 
/// </summary>
public class BO_Mail_BO_MsgHdr_Author_AntwortVomTeam {

   #region Config

   /// <summary>
   /// ‼ Neu nicht mehr das Unicdode-Zeichen • als Delimiter, damit der Header Klartext bleibt
   /// </summary>
   private static char   SerializeDeserializeDelimiter_Old = '•';
   private static string SerializeDeserializeDelimiter     = " # ";

   #endregion Config

   #region Props

   public TeamMitglied? TeamAutor { get; private set; }

   private string _autorKürzel;

   public string AutorKürzel {
      get => _autorKürzel;
      set {
         if (!value.ØHasValue()) {
            throw new RuntimeException("BO_MsgHdr_Author_AntwortVomTeam: AutorKürzel darf nicht null oder leer sein!");
         }

         _autorKürzel = value;
      }
   }

   private int _erkennungsZuverlässigkeitProzent;

   /// <summary>
   /// Zuverlässigkeit der Erkennung
   ///   100
   ///      Teammitglied in der ReplyTo-Adresse erkannt 
   ///      Teammitglied im ReplyTo-Anzeigenamen erkannt
   ///   80
   ///      Die E-Mail Signatur wurde erkannt und darin der Name des Teammitglieds
   /// </summary>
   public int ErkennungsZuverlässigkeitProzent {
      get => _erkennungsZuverlässigkeitProzent;
      set {
         if (value is < 0 or > 100) {
            throw new RuntimeException
               ("BO_MsgHdr_Author_AntwortVomTeam: ErkennungsZuverlässigkeitProzent muss zwischen 0 und 100 sein!");
         }
         _erkennungsZuverlässigkeitProzent = value;
      }
   }

   #endregion Props

   #region Konstruktor

   /// <summary>
   /// Der Konstrultor mit dem Kürzel
   /// </summary>
   /// <param name="autorKürzel"></param>
   /// <param name="erkennungsZuverlässigkeitProzent"></param>
   public BO_Mail_BO_MsgHdr_Author_AntwortVomTeam(string autorKürzel, int erkennungsZuverlässigkeitProzent) {
      AutorKürzel = autorKürzel.Trim();
      TeamAutor = TeamMitglied.Get_TeamMitglied(App.Cfg_TeamConfig.TeamConfig.TeamMitglieder, autorKürzel);
      _erkennungsZuverlässigkeitProzent = erkennungsZuverlässigkeitProzent;
   }

   /// <summary>
   /// Der Konstrultor mit dem Kürzel
   /// </summary>
   /// <param name="autorKürzel"></param>
   /// <param name="erkennungsZuverlässigkeitProzent"></param>
   public BO_Mail_BO_MsgHdr_Author_AntwortVomTeam(TeamMitglied autor, int erkennungsZuverlässigkeitProzent) {
      AutorKürzel                       = autor.Kuerzel;
      TeamAutor                         = autor;
      _erkennungsZuverlässigkeitProzent = erkennungsZuverlässigkeitProzent;
   }

   #endregion Konstruktor

   #region Serialize / Deserialize

   /// <summary>
   /// Serialisiert das Objekt in einen String
   /// </summary>
   /// <returns></returns>
   public string Serialize() { return $"{AutorKürzel}{SerializeDeserializeDelimiter}{ErkennungsZuverlässigkeitProzent}"; }


   /// <summary>
   /// De-Serialisiert einen String in das Obj
   /// </summary>
   /// <param name="serialized"></param>
   /// <returns></returns>
   public static BO_Mail_BO_MsgHdr_Author_AntwortVomTeam Deserialize(string serialized) {
      var itemsOld = serialized.Split(SerializeDeserializeDelimiter_Old);
      var items    = serialized.Split(SerializeDeserializeDelimiter);

      // ? Haben wir die neue oder alte Codierung?
      string autorKürzel;
      string sErkennungsZuverlässigkeitProzent;
      if (itemsOld.Length >= 2) {
         autorKürzel                       = itemsOld.Length >= 1 ? itemsOld[0] : "";
         sErkennungsZuverlässigkeitProzent = itemsOld.Length >= 2 ? itemsOld[1] : "0";
      }
      else {
         autorKürzel                       = items.Length >= 1 ? items[0] : "";
         sErkennungsZuverlässigkeitProzent = items.Length >= 2 ? items[1] : "0";
      }

      if (!Int32.TryParse(sErkennungsZuverlässigkeitProzent, out int iErkennungsZuverlässigkeitProzent)) {
         iErkennungsZuverlässigkeitProzent = 0;
      }

      return new BO_Mail_BO_MsgHdr_Author_AntwortVomTeam(autorKürzel, iErkennungsZuverlässigkeitProzent);
   }

   #endregion Serialize / Deserialize

}
