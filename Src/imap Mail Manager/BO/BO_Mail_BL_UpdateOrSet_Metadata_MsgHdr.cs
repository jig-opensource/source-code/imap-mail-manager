using System;
using System.Collections.Generic;
using System.Diagnostics;
using Fluid.Ast;
using ImapMailManager.Config;
using ImapMailManager.Mail.MimeKit;
using ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator;
using jig.Tools;
using jig.Tools.Exception;


namespace ImapMailManager.BO;

/// <summary>
/// BL für das Setzen und Aktualisieren von Metadaten im Msg Hdr
/// </summary>
public class BO_Mail_BL_UpdateOrSet_Metadata_MsgHdr {

   #region Props

   private readonly BO_Mail _boMail;

   #endregion Props

   #region Konstruktoren

   /// <summary>
   /// Initialisiert die Klasse mit der BO_Mail
   /// </summary>
   /// <param name="boMail"></param>
   public BO_Mail_BL_UpdateOrSet_Metadata_MsgHdr(BO_Mail boMail) => _boMail = boMail;

   #endregion Konstruktoren

   #region Config

   #endregion Config

   #region BL

   /// <summary>
   /// Wrapper für: UpdateOrSet_Mail_Metadata_()
   /// damit saveOnDirty einfacher handhabbar ist
   /// 
   /// Setzt oder aktualisiert die Metadaten einer E-Mail
   /// aufgrund des Ablageorts und der Mail-Kategoerie 
   /// </summary>
   public void UpdateOrSet_Mail_Metadata(
      bool   saveOnDirty = false
      , bool force       = false

      // Zum Debuggen: Annahme, dass der Aufruf für eine Archiv-Mailbox gilt
      , bool forceProcessingAsArchiveMailbox = false

      // Zum Debuggen: Annahme, dass der Aufruf für die Team-Mailbox gilt
      , bool forceProcessingAsTeamMailbox = false) {
      UpdateOrSet_Mail_Metadata_
         (force
          , forceProcessingAsArchiveMailbox
          , forceProcessingAsTeamMailbox);

      if (saveOnDirty) { _boMail.Save(); }
   }


   /// <summary>
   /// Setzt oder aktualisiert die Metadaten einer E-Mail
   /// aufgrund des Ablageorts und der Mail-Kategoerie 
   /// </summary>
   private void UpdateOrSet_Mail_Metadata_(
      bool force = false

      // Zum Debuggen: Annahme, dass der Aufruf für eine Archiv-Mailbox gilt
      , bool forceProcessingAsArchiveMailbox = false

      // Zum Debuggen: Annahme, dass der Aufruf für die Team-Mailbox gilt
      , bool forceProcessingAsTeamMailbox = false) {
      //+ Ohne Mailserver können wir keine Daten aktualisieren       
      if (!_boMail.HasImapSrvRuntimeConfig) { return; }

      //» Abbruch, wenn die Mail in einem Ordner ist, in dem keine Metadaten berechnet werden
      if (BO_Mail_BL_UpdateOrSet_Metadata_Common.FolderFilter_AllMetadata_Blacklist.StopFolderProcessing_BlackWhiteList_(_boMail.MailFolder)) {
         Debug.WriteLine($"  » skipped: {_boMail.MailFolder}");
         return;
      }

      //» Die Regeln für die Archiv-Mailbox 
      if (forceProcessingAsArchiveMailbox || _boMail.ImapSrvRuntimeConfig!.IsArchiveMailbox) {
         return;
      }

      //» Die Regeln für die Team-Mailbox (ist keine Archiv-Mailbox) 
      if (forceProcessingAsTeamMailbox || !_boMail.ImapSrvRuntimeConfig!.IsArchiveMailbox) {
         //+ Metadaten für alle Mbx Ordner
         ConfigMail_MsgHeader.AppMailHeaderType[] metadatenAllgemein = {
            ConfigMail_MsgHeader.AppMailHeaderType.MailRefNr
            , ConfigMail_MsgHeader.AppMailHeaderType.MailKategorie
         };
         UpdateOrSet_Mail_Metadata(metadatenAllgemein);

         //+ Ordner: Erhaltene Dokumente
         if (!BO_Mail_BL_UpdateOrSet_Metadata_Common.FolderFilter_Metadaten_Kat_ErhalteneDokumente.StopFolderProcessing_BlackWhiteList_(_boMail.MailFolder)) {
            // Nichts besonderes zu tun
            return;
         }

         //+ Ordner: Gesendet
         if (!BO_Mail_BL_UpdateOrSet_Metadata_Common.FolderFilter_Metadaten_Kat_ErhalteneDokumente.StopFolderProcessing_BlackWhiteList_
                (_boMail.MailFolder)) {

            // Den Autor detektieren
            _boMail.Analyze_And_AddOrUpdate_MsgHdr_Author_AntwortVomTeam(force);

            // 1. Die Metadaten vom Mail-Thread kopieren
            ConfigMail_MsgHeader.AppMailHeaderType[] metadaten = {
               ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularThema
               , ConfigMail_MsgHeader.AppMailHeaderType.TriageOrdner
               , ConfigMail_MsgHeader.AppMailHeaderType.TriageHashtags
               , ConfigMail_MsgHeader.AppMailHeaderType.FragestellerPersoenlicheDaten
               , ConfigMail_MsgHeader.AppMailHeaderType.SpracheMailInhalt
            };
            var metadataMissing = MailKit_JigTools_MessageThread.UpdateMetadata_FromMsgThread(metadaten, _boMail);

            // 2. Wenn wir nicht alle Metadaten übertragen konnten, dann die restlichen Metadaten berechnen
            if (metadataMissing) {
               UpdateOrSet_Mail_Metadata(metadaten);
            }

            return;
         }

         //+ Triage-Ordner:
         //+ Ordner mit Mails direkt vom Kontaktformular
         //+ Kontaktformular ★
         if (!BO_Mail_BL_UpdateOrSet_Metadata_Common.FolderFilter_Metadaten_Kat_Kontaktformular.StopFolderProcessing_BlackWhiteList_
                (_boMail.MailFolder)) {
            ConfigMail_MsgHeader.AppMailHeaderType[] metadaten = {
               ConfigMail_MsgHeader.AppMailHeaderType.SpracheMailInhalt
               , ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularThema
               , ConfigMail_MsgHeader.AppMailHeaderType.FragestellerPersoenlicheDaten
            };
            UpdateOrSet_Mail_Metadata(metadaten);

            //‼ ToDo 🟥 Metadaten periodisch berechnen, damit die Triage nachgeführt wird
            //! Diese Metadaten forciert berechnen, um sie nachzuführen 
            ConfigMail_MsgHeader.AppMailHeaderType[] metadatenForce = {
               ConfigMail_MsgHeader.AppMailHeaderType.TriageOrdner
               , ConfigMail_MsgHeader.AppMailHeaderType.TriageHashtags
            };
            UpdateOrSet_Mail_Metadata(metadatenForce, true);
            return;
         }

         //+ Ordner: 10 Prozess, in Arbeit ★
         if (!BO_Mail_BL_UpdateOrSet_Metadata_Common.FolderFilter_Metadaten_Kat_MailsInArbeit.StopFolderProcessing_BlackWhiteList_
                (_boMail.MailFolder)) {
            // ! Optional könnten wir hier periodisch den Autor erfassen,
            // der zuletzt an der Ausarbeitung der Antwort gearbeitet hat
            return;
         }

         //+ Ordner: 3 Redaktion ★
         if (!BO_Mail_BL_UpdateOrSet_Metadata_Common.FolderFilter_Metadaten_Kat_3_Redaktion.StopFolderProcessing_BlackWhiteList_(_boMail.MailFolder)) {
            // zZ nichts besonderes zu tun
            return;
         }

         //+ Ordner: 90 Erledigt ★
         if (!BO_Mail_BL_UpdateOrSet_Metadata_Common.FolderFilter_Metadaten_Kat_90_Erledigt.StopFolderProcessing_BlackWhiteList_(_boMail.MailFolder)) {
            ConfigMail_MsgHeader.AppMailHeaderType[] metadaten = {
               ConfigMail_MsgHeader.AppMailHeaderType.Author_AntwortVomTeam
            };
            UpdateOrSet_Mail_Metadata(metadaten);
            return;
         }

         //+ Ordner: Folgefragen  ★
         if (!BO_Mail_BL_UpdateOrSet_Metadata_Common.FolderFilter_Metadaten_Kat_Folgefragen.StopFolderProcessing_BlackWhiteList_(_boMail.MailFolder)) {
            ConfigMail_MsgHeader.AppMailHeaderType[] metadaten = {
               ConfigMail_MsgHeader.AppMailHeaderType.MailKategorie
            };
            UpdateOrSet_Mail_Metadata(metadaten);
            return;
         }
      }

   }


   /// <summary>
   /// Versucht, die angegebenen Metadaten zu berechnen
   /// </summary>
   /// <param name="appMailHeaderTypes"></param>
   public void UpdateOrSet_Mail_Metadata(ConfigMail_MsgHeader.AppMailHeaderType[] appMailHeaderTypes, bool force = false) {
      //+ Alle zu berechnenden Metadaten durchlaufen und berechnen
      foreach (var mailHeaderType in appMailHeaderTypes) {
         switch (mailHeaderType) {

            case ConfigMail_MsgHeader.AppMailHeaderType.TriageOrdner:
               if (force || !_boMail.HasHeaderSet_TriageOrdner) {
                  _boMail.Analyze_and_AddOrUpdate_MsgHdr_TriageOrdner(force);
               }

               break;

            case ConfigMail_MsgHeader.AppMailHeaderType.MailRefNr:
               if (force || !_boMail.HasHeaderSet_MailRefNr) {
                  _boMail.Analyze_and_AddOrUpdate_MsgHdr_MailRefnr(force);
               }

               break;

            case ConfigMail_MsgHeader.AppMailHeaderType.TriageHashtags:
               if (force || !_boMail.HasHeaderSet_TriageHashtags) {
                  _boMail.Analyze_and_AddOrUpdate_MsgHdr_TriageHashtags(force);
               }

               break;

            case ConfigMail_MsgHeader.AppMailHeaderType.FragestellerPersoenlicheDaten:
               if (force || !_boMail.HasHeaderSet_FragestellerPersoenlicheDaten) {
                  _boMail.Analyze_and_AddOrUpdate_MsgHdr_FragestellerPersoenlicheDaten(force);
               }

               break;

            case ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularThema:
               if (force || !_boMail.HasHeaderSet_MailKontaktformularThema) {
                  _boMail.Analyze_and_AddOrUpdate_MsgHdr_MailKontaktformularThema(force);
               }

               break;

            case ConfigMail_MsgHeader.AppMailHeaderType.SpracheMailInhalt:
               if (force || !_boMail.HasHeaderSet_SpracheMailInhalt) {
                  _boMail.Analyze_And_AddOrUpdate_MsgHdr_SpracheMailInhalt(force);
               }

               break;

            case ConfigMail_MsgHeader.AppMailHeaderType.MailKategorie:
               if (force || !_boMail.HasHeaderSet_MailKategorie) {
                  _boMail.Analyze_And_AddOrUpdate_MsgHdr_MailKategorie(force);
               }

               break;

            case ConfigMail_MsgHeader.AppMailHeaderType.Author_AntwortVomTeam:
               if (force || !_boMail.HasHeaderSet_Author_AntwortVomTeam) {
                  _boMail.Analyze_And_AddOrUpdate_MsgHdr_Author_AntwortVomTeam(force);
               }

               break;

            case ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularDatenFound:
            case ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularDatenMissing:
            case ConfigMail_MsgHeader.AppMailHeaderType.MailInhaltAnonymisiert:
            case ConfigMail_MsgHeader.AppMailHeaderType.MailBetreffWurdeBereinigtDate:
            case ConfigMail_MsgHeader.AppMailHeaderType.OriMailBetreffBackup:
               throw new RuntimeException
                  ($"UpdateMsgMetadata(): Es macht keinen Sinn, diese Metadaten zu kopieren: {mailHeaderType}");

            default:
               throw new ArgumentOutOfRangeException();
         }
      }
   }

   #endregion BL

   #region Hilfsfunktionen

   /// <summary>
   /// Überträgt die Metadaten von src auf dst,
   /// wobei bestehende Metadaten in dst nicht überschrieben werden 
   /// </summary>
   /// <param name="src"></param>
   /// <param name="dst"></param>
   /// <returns>
   /// true: Alle Metadaten sind jetzt vorhanden
   /// </returns>
   public static bool UpdateMsgMetadata(ConfigMail_MsgHeader.AppMailHeaderType[] appMailHeaderTypes, BO_Mail src, BO_Mail dst) {

      var metadataMissing = false;

      //+ Alle zu kopierenden Attribute durchlaufen und kopieren
      //++ Merken, wenn Daten fehlen
      foreach (var mailHeaderType in appMailHeaderTypes) {
         switch (mailHeaderType) {
            case ConfigMail_MsgHeader.AppMailHeaderType.TriageOrdner: {
               if (!dst.HasHeaderSet_TriageOrdner && src.HasHeaderSet_TriageOrdner) {
                  src.Copy_MsgHdr_TriageOrdner(dst);
               }
               else {
                  metadataMissing = true;
               }

               break;
            }

            case ConfigMail_MsgHeader.AppMailHeaderType.MailRefNr:
               if (!dst.HasHeaderSet_MailRefNr && src.HasHeaderSet_MailRefNr) {
                  src.Copy_MsgHdr_MailRefNr(dst);
               }
               else {
                  metadataMissing = true;
               }

               break;

            case ConfigMail_MsgHeader.AppMailHeaderType.TriageHashtags:
               if (!dst.HasHeaderSet_TriageHashtags && src.HasHeaderSet_TriageHashtags) {
                  src.Copy_MsgHdr_TriageHashtags(dst);
               }
               else {
                  metadataMissing = true;
               }

               break;

            case ConfigMail_MsgHeader.AppMailHeaderType.FragestellerPersoenlicheDaten:
               if (!dst.HasHeaderSet_FragestellerPersoenlicheDaten && src.HasHeaderSet_FragestellerPersoenlicheDaten) {
                  src.Copy_MsgHdr_FragestellerPersoenlicheDaten(dst);
               }
               else {
                  metadataMissing = true;
               }

               break;

            case ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularThema:
               if (!dst.HasHeaderSet_MailKontaktformularThema && src.HasHeaderSet_MailKontaktformularThema) {
                  src.Copy_MsgHdr_MailKontaktformularThema(dst);
               }
               else {
                  metadataMissing = true;
               }

               break;

            case ConfigMail_MsgHeader.AppMailHeaderType.SpracheMailInhalt:
               if (!dst.HasHeaderSet_SpracheMailInhalt && src.HasHeaderSet_SpracheMailInhalt) {
                  src.Copy_MsgHdr_SpracheMailInhalt(dst);
               }
               else {
                  metadataMissing = true;
               }

               break;

            case ConfigMail_MsgHeader.AppMailHeaderType.MailKategorie:
               if (!dst.HasHeaderSet_MailKategorie && src.HasHeaderSet_MailKategorie) {
                  src.Copy_MsgHdr_MailKategorie(dst);
               }
               else {
                  metadataMissing = true;
               }

               break;

            case ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularDatenFound:
            case ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularDatenMissing:
            case ConfigMail_MsgHeader.AppMailHeaderType.MailInhaltAnonymisiert:
            case ConfigMail_MsgHeader.AppMailHeaderType.OriMailBetreffBackup:
            case ConfigMail_MsgHeader.AppMailHeaderType.MailBetreffWurdeBereinigtDate:
            case ConfigMail_MsgHeader.AppMailHeaderType.Author_AntwortVomTeam:
               throw new RuntimeException
                  ($"UpdateMsgMetadata(): Es macht keinen Sinn, diese Metadaten zu kopieren: {mailHeaderType}");

            default:
               throw new ArgumentOutOfRangeException();
         }
      }

      return metadataMissing == false;
   }

   #endregion Hilfsfunktionen

}
