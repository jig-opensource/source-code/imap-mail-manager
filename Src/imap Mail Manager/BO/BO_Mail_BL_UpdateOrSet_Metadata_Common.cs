using System.Collections.Generic;
using ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator;
using jig.Tools;


namespace ImapMailManager.BO; 


/// <summary>
/// Gemeinsamer Code für die Metadaten-Generierung
/// </summary>
public class BO_Mail_BL_UpdateOrSet_Metadata_Common {

   #region Config

   #region Mbx Ordner Filter

   /// <summary>
   /// Blackliste, die für alle Metadaten-Erzeugung gilt
   /// </summary>
   public static readonly FolderFilter FolderFilter_AllMetadata_Blacklist = new() {
      MbxFolder_Blacklist = new
         (IMailbox_FolderFilter.FolderCompare.RgxMatch
          , new List<string> {
             ExtensionMethods_RegEx.ØWildcardToRegex(@"Drafts".Replace('\\', '/'))
             , ExtensionMethods_RegEx.ØWildcardToRegex(@"Entwürfe".Replace('\\', '/'))
             , ExtensionMethods_RegEx.ØWildcardToRegex(@"Trash".Replace('\\', '/'))
             , ExtensionMethods_RegEx.ØWildcardToRegex(@"Gelöschte Elemente".Replace('\\', '/'))
             , ExtensionMethods_RegEx.ØWildcardToRegex(@"Spam".Replace('\\', '/'))
             , ExtensionMethods_RegEx.ØWildcardToRegex(@"Junk-E-Mail".Replace('\\', '/'))
          })
   };

   /// <summary>
   /// Ordner, in dem wir die erhaltenen Dokumente empfangen
   /// </summary>
   public static readonly FolderFilter FolderFilter_Metadaten_Kat_ErhalteneDokumente = new() {
      MbxFolder_Whitelist = new
         (IMailbox_FolderFilter.FolderCompare.RgxMatch
          , new List<string> {
             ExtensionMethods_RegEx.ØWildcardToRegex(@"INBOX/Erhaltene Dokumente ★*".Replace('\\', '/'))
             , ExtensionMethods_RegEx.ØWildcardToRegex(@"Posteingang/Erhaltene Dokumente ★*".Replace('\\', '/'))
          })
   };

   /// <summary>
   /// Ordner mit Mails direkt vom Kontaktformular
   /// </summary>
   public static readonly FolderFilter FolderFilter_Metadaten_Kat_Kontaktformular = new() {
      MbxFolder_Whitelist = new
         (IMailbox_FolderFilter.FolderCompare.RgxMatch
          , new List<string> {
             ExtensionMethods_RegEx.ØWildcardToRegex(@"INBOX/Erhaltene Dokumente ★*".Replace('\\', '/'))
             , ExtensionMethods_RegEx.ØWildcardToRegex(@"Posteingang/Erhaltene Dokumente ★*".Replace('\\', '/'))
          })
   };

   /// <summary>
   /// Ordner mit Mails, die in Arbeit sind
   /// </summary>
   public static readonly FolderFilter FolderFilter_Metadaten_Kat_MailsInArbeit = new() {
      MbxFolder_Whitelist = new
         (IMailbox_FolderFilter.FolderCompare.RgxMatch
          , new List<string> {
             ExtensionMethods_RegEx.ØWildcardToRegex(@"10 Prozess, in Arbeit ★*".Replace('\\', '/'))
          })
   };

   /// <summary>
   /// Ordner: 3 Redaktion ★
   /// </summary>
   public static readonly FolderFilter FolderFilter_Metadaten_Kat_3_Redaktion = new() {
      MbxFolder_Whitelist = new
         (IMailbox_FolderFilter.FolderCompare.RgxMatch
          , new List<string> {
             ExtensionMethods_RegEx.ØWildcardToRegex(@"3 Redaktion ★*".Replace('\\', '/'))
          })
   };

   /// <summary>
   /// Ordner: 90 Erledigt ★
   /// </summary>
   public static readonly FolderFilter FolderFilter_Metadaten_Kat_90_Erledigt = new() {
      MbxFolder_Whitelist = new
         (IMailbox_FolderFilter.FolderCompare.RgxMatch
          , new List<string> {
             ExtensionMethods_RegEx.ØWildcardToRegex(@"90 Erledigt ★*".Replace('\\', '/'))
          })
   };

   /// <summary>
   /// Ordner: Folgefragen  ★
   /// </summary>
   public static readonly FolderFilter FolderFilter_Metadaten_Kat_Folgefragen = new() {
      MbxFolder_Whitelist = new
         (IMailbox_FolderFilter.FolderCompare.RgxMatch
          , new List<string> {
             ExtensionMethods_RegEx.ØWildcardToRegex(@"Posteingang\Folgefragen ★*".Replace('\\', '/'))
             , ExtensionMethods_RegEx.ØWildcardToRegex(@"INBOX\Folgefragen ★*".Replace('\\', '/'))
          })
   };

   #endregion Mbx Ordner Filter

   #endregion Config
   
   
   

}
