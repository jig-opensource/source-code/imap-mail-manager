using System;
using System.Collections.Generic;
using System.Linq;
using ImapMailManager.Properties;
using jig.Tools.String;
using PCRE;


namespace ImapMailManager.BO;

/// <summary>
/// BL für das Erkennen der Team-Antwort Autoren
/// </summary>
public class BO_Mail_BL_Detect_Author_AntwortVomTeam {

   private static readonly Dictionary<string, List<PcreRegex>> AllRgx_MailSignatur    = new();
   private static readonly Dictionary<string, List<PcreRegex>> AllRgx_VornameNachname = new();

   #region Konstruktor

   static BO_Mail_BL_Detect_Author_AntwortVomTeam() { CalcRegex(); }

   #endregion Konstruktor

   #region BL

   /// <summary>
   /// Versucht den Autor zu erkennen, indem die bekannte E-Mail Signatur gesucht wird 
   /// </summary>
   /// <returns>
   /// var (autor, erkennungsZuverlässigkeitProzent) = … 
   /// </returns>
   public static Tuple<TeamMitglied?, int> Detect_Author_By_RgxMailSignatur(BO_Mail boMail) {
      var mailBodytextWithCrLf = boMail.Mail_Body_TextOnly;

      // Alle Autoren sammeln, die für diese E-Mail in Frage kommen
      List<TeamMitglied?> thisMailAutors = new List<TeamMitglied?>();

      //+ Von jedem Teammitglied seine RegEx prüfen 
      foreach (KeyValuePair<string, List<PcreRegex>> thisTeamMember_AllRgx_Grussformel in AllRgx_MailSignatur) {
         //+ Alle RegEx prüfen
         foreach (PcreRegex this_AllRgx_Grussformel in thisTeamMember_AllRgx_Grussformel.Value) {
            var match = this_AllRgx_Grussformel.Match(mailBodytextWithCrLf);

            if (match.Success) {
               var teamInfo = match.Groups["TeamInfo"].Value;

               //+ Haben wir einen Autor?
               //++ Autor vor der Fusszeile?
               var authorOben = match.Groups["AuthorOben"].Value ?? "";

               //++ Autor nach der Fusszeile?
               var authorUnten = match.Groups["AuthorUnten"].Value ?? "";
               var author      = authorOben.ØHasValue() ? authorOben : authorUnten;

               if (author.ØHasValue()) {
                  //+++ Den Autor sammeln
                  thisMailAutors.Add
                     (TeamMitglied.Get_TeamMitglied
                         (App.Cfg_TeamConfig.TeamConfig.TeamMitglieder, thisTeamMember_AllRgx_Grussformel.Key)!
                     );
               }
            }
         }
      }

      // Distinct
      var thisMailAutorsDistinct = thisMailAutors.DistinctBy(x => x.Kuerzel).ToList();

      //+ Wir haben nur einen Autor gefunden?
      if (thisMailAutorsDistinct.Count == 1) {
         // 90%
         return new Tuple<TeamMitglied?, int>(thisMailAutorsDistinct[0], 70);
      }
      else if (thisMailAutorsDistinct.Count > 1) {
         // Mehrere gefunden, den ersten zurückliefern
         // 80%
         return new Tuple<TeamMitglied?, int>(thisMailAutorsDistinct[0], 60);
      }

      return new Tuple<TeamMitglied?, int>(null, 0);
   }

   /// <summary>
   /// Sucht den ersten vollen Vornamen Nachnamen per Regex
   /// </summary>
   /// <param name="boMail"></param>
   /// <returns></returns>
   public static Tuple<TeamMitglied?, int> Detect_Author_By_RgxVornameNachname(BO_Mail boMail) {
      var MailBodytextWithCrLf = boMail.Mail_Body_TextOnly;

      // Alle Autoren sammeln, die für diese E-Mail in Frage kommen
      List<TeamMitglied?> thisMailAutors = new List<TeamMitglied?>();

      //+ Von jedem Teammitglied seine RegEx prüfen 
      foreach (KeyValuePair<string, List<PcreRegex>> thisTeamMember_AllRgx_Grussformel in AllRgx_VornameNachname) {
         //+ Alle RegEx prüfen
         foreach (PcreRegex this_AllRgx_Grussformel in thisTeamMember_AllRgx_Grussformel.Value) {
            var match = this_AllRgx_Grussformel.Match(MailBodytextWithCrLf);

            if (match.Success) {
               //+ Haben wir einen Autor?
               var authorVornameNachname = match.Groups["AutorVornameNachname"].Value ?? "";

               if (authorVornameNachname.ØHasValue()) {
                  //+++ Den Autor sammeln
                  thisMailAutors.Add
                     (TeamMitglied.Get_TeamMitglied
                         (App.Cfg_TeamConfig.TeamConfig.TeamMitglieder, thisTeamMember_AllRgx_Grussformel.Key)!
                     );
               }
            }
         }
      }

      // Distinct
      var thisMailAutorsDistinct = thisMailAutors.DistinctBy(x => x.Kuerzel).ToList();

      //+ Wir haben nur einen Autor gefunden?
      if (thisMailAutorsDistinct.Count == 1) {
         // 90%
         return new Tuple<TeamMitglied?, int>(thisMailAutorsDistinct[0], 90);
      }
      else if (thisMailAutorsDistinct.Count > 1) {
         // Mehrere gefunden, den ersten zurückliefern
         // 80%
         return new Tuple<TeamMitglied?, int>(thisMailAutorsDistinct[0], 80);
      }

      return new Tuple<TeamMitglied?, int>(null, 0);
   }

   #endregion BL

   #region Hilfsfunktionen

   private static void CalcRegex() {

      // !KH9'9
      // i » PcreOptions.IgnoreCase 
      // n » PcreOptions.ExplicitCapture 
      // x » PcreOptions.IgnorePatternWhitespace 
      // m » PcreOptions.MultiLine   »  ^$ matches line breaks 
      // s » PcreOptions.Singleline  »   . matches line breaks 

      #region RgxMailSignatur

      PcreOptions RgxPcre2Optionss_rgxMailSignatur =
         PcreOptions.Compiled
         | PcreOptions.IgnoreCase
         | PcreOptions.Caseless
         | PcreOptions.IgnorePatternWhitespace
         | PcreOptions.ExplicitCapture
         | PcreOptions.MultiLine

         // | PcreOptions.Singleline
         | PcreOptions.Ungreedy
         | PcreOptions.Unicode;

      string rgxMailSignatur_AutorOben = @"
                        ## Der Author über der Team-Info
                        (?:(?<AuthorOben>
                            # nicht \p{Zs}+ sondern:
                            # \s+     Weil die Infos auf zwei Zeilen sein können
                            (?:{Nachname}\s+{Vorname}|{Vorname}\s+{Nachname}|{Vorname}|{Nachname})
                        ).*$)
                        (?<EmptyChars>\s+)
                        ## Die Team-Info
                        (?<TeamInfo>Team\s+Roger\s+Liebi)";

      string rgxMailSignatur_AutorUnten = @"
                       ## Die Team-Info
                       (?<TeamInfo>Team\s+Roger\s+Liebi|Grüße?|Grüsse|Gruss)
                       (?<EmptyChars>\s+)
                       (?:.*(?<AuthorUnten>
                          # nicht \p{Zs}+ sondern:
                          # \s+     Weil die Infos auf zwei Zeilen sein können
                         (?:{Nachname}\s+{Vorname}|{Vorname}\s+{Nachname}|{Vorname}|{Nachname})
                       ).*$)";

      #endregion RgxMailSignatur

      #region Voller Vorname und Name

      PcreOptions RgxPcre2Optionss_FullVornameNachname =
         PcreOptions.Compiled
         | PcreOptions.IgnoreCase
         | PcreOptions.Caseless
         | PcreOptions.IgnorePatternWhitespace
         | PcreOptions.ExplicitCapture
         | PcreOptions.MultiLine

         // | PcreOptions.Singleline
         | PcreOptions.Ungreedy
         | PcreOptions.Unicode;

      string rgxGruss_FullVornameNachname = @"
                        ## Die Autor-Info
                        (?:.*(?<AutorVornameNachname>
                          # nicht \p{Zs}+ sondern:
                          # \s+     Weil die Infos auf zwei Zeilen sein können
                         (?:{Vorname}\s+{Nachname})
                        ).*$)";

      #endregion Voller Vorname und Name

      foreach (var teamMitglied in App.Cfg_TeamConfig.TeamConfig.TeamMitglieder) {
         //+ Das Dict initialisieren
         if (!AllRgx_MailSignatur.ContainsKey(teamMitglied.Kuerzel)) {
            AllRgx_MailSignatur.Add(teamMitglied.Kuerzel, new List<PcreRegex>());
         }

         if (!AllRgx_VornameNachname.ContainsKey(teamMitglied.Kuerzel)) {
            AllRgx_VornameNachname.Add(teamMitglied.Kuerzel, new List<PcreRegex>());
         }

         //++ Den Nachnamen aufsplitten
         var nachnameItems = teamMitglied.Nachname.Split('-');

         //+ rgxGruss_FullVornameNachname

         #region rgxGruss_FullVornameNachname

         if (nachnameItems.Length > 1) {
            //+ Wir haben mehrere Nachnamen
            foreach (var nachnameItem in nachnameItems) {
               var strRgxLoop = rgxGruss_FullVornameNachname
                               .Replace("{Vorname}",  teamMitglied.Vorname)
                               .Replace("{Nachname}", nachnameItem);
               PcreRegex oRgxLoop = new PcreRegex(strRgxLoop, RgxPcre2Optionss_rgxMailSignatur);
               AllRgx_VornameNachname[teamMitglied.Kuerzel].Add(oRgxLoop);
            }
         }
         else {
            //+ Wir haben nur einen Nachnamen
            var strRgxOben = rgxGruss_FullVornameNachname.Replace("{Vorname}", teamMitglied.Vorname)
                                                         .Replace("{Nachname}", teamMitglied.Nachname);
            var oRgxOben = new PcreRegex(strRgxOben, RgxPcre2Optionss_rgxMailSignatur);
            AllRgx_VornameNachname[teamMitglied.Kuerzel].Add(oRgxOben);
         }

         #endregion rgxGruss_FullVornameNachname

         //+ rgxMailSignatur_AutorOben

         #region rgxMailSignatur_AutorOben

         if (nachnameItems.Length > 1) {
            //+ Wir haben mehrere Nachnamen
            foreach (var nachnameItem in nachnameItems) {
               var strRgxLoop = rgxMailSignatur_AutorOben
                               .Replace("{Vorname}",  teamMitglied.Vorname)
                               .Replace("{Nachname}", nachnameItem);
               PcreRegex oRgxLoop = new PcreRegex(strRgxLoop, RgxPcre2Optionss_rgxMailSignatur);
               AllRgx_MailSignatur[teamMitglied.Kuerzel].Add(oRgxLoop);
            }
         }
         else {
            //+ Wir haben nur einen Nachnamen
            var strRgxOben = rgxMailSignatur_AutorOben.Replace("{Vorname}", teamMitglied.Vorname)
                                                      .Replace("{Nachname}", teamMitglied.Nachname);
            var oRgxOben = new PcreRegex(strRgxOben, RgxPcre2Optionss_rgxMailSignatur);
            AllRgx_MailSignatur[teamMitglied.Kuerzel].Add(oRgxOben);
         }

         #endregion rgxMailSignatur_AutorOben

         //+ rgxMailSignatur_AutorUnten

         #region rgxMailSignatur_AutorUnten

         if (nachnameItems.Length > 1) {
            //+ Wir haben mehrere Nachnamen
            foreach (var nachnameItem in nachnameItems) {
               var strRgxLoop = rgxMailSignatur_AutorUnten.Replace("{Vorname}", teamMitglied.Vorname)
                                                          .Replace("{Nachname}", teamMitglied.Nachname);
               PcreRegex oRgxLoop = new PcreRegex(strRgxLoop, RgxPcre2Optionss_rgxMailSignatur);
               AllRgx_MailSignatur[teamMitglied.Kuerzel].Add(oRgxLoop);
            }
         }
         else {
            //+ Wir haben nur einen Nachnamen
            var strRgxOben = rgxMailSignatur_AutorUnten.Replace("{Vorname}", teamMitglied.Vorname)
                                                       .Replace("{Nachname}", teamMitglied.Nachname);
            var oRgxOben = new PcreRegex(strRgxOben, RgxPcre2Optionss_rgxMailSignatur);
            AllRgx_MailSignatur[teamMitglied.Kuerzel].Add(oRgxOben);
         }

         #endregion rgxMailSignatur_AutorUnten

      }

   }

   #endregion Hilfsfunktionen

}
