using System.Collections.Generic;
using System.Linq;
using ImapMailManager.Config;
using ImapMailManager.Mail;
using ImapMailManager.Mail.MimeKit;
using ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator;
using ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;
using ImapMailManager.Properties;
using jig.Tools;
using jig.Tools.String;
using MailKit;
using MailKit.Search;


namespace ImapMailManager.BO;

/// <summary>
/// Verwaltet von der Mailbox alle Mail-Threads:
/// - Liest alle Mail-Threads
/// - Aktualisiert einzelne Elemente im Mail-Thread
/// </summary>
public class BO_Mail_BL_MailThread_Mgr {

   /// <summary>
   /// Für jede MailBox die Message Threads
   /// </summary>
   private static Dictionary<string, Process_AllMails_Get_MessageThreads> mbxThreads = new();


   /// <summary>
   /// Liefert vom Mailserver alle Mail-Threads
   /// Wenn sie fehlen, werden sie gelesen 
   /// </summary>
   /// <param name="cfgImapServerCred"></param>
   /// <returns></returns>
   public static Process_AllMails_Get_MessageThreads Get_Mailbox_MailThreadsData(
      Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred) {
      if (!mbxThreads.ContainsKey(cfgImapServerCred.CredKey)) {
         Read_Mbx_MailThreads(cfgImapServerCred);
      }

      return mbxThreads[cfgImapServerCred.CredKey];
   }


   /// <summary>
   /// Liest für die Mailbox alle Threads neu vom Server ein
   /// </summary>
   /// <param name="cfgImapServerCred"></param>
   private static void Read_Mbx_MailThreads(Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred) {

      //+ Die Mail-Threads abrufen
      //++ Definieren, welche Mbx Ordner wir verarbeiten
      var folderFilter = new FolderFilter {
         MbxFolder_Blacklist = new
            (IMailbox_FolderFilter.FolderCompare.RgxMatch
             , BL_Mail_Common.FolderFilter_Gelöscht
                             .ØMerge(BL_Mail_Common.FolderFilter_Spam)
                             .ØMerge(BL_Mail_Common.FolderFilter_Draft)
            )
      };

      //++ Den Iterator initialisieren
      Process_AllMails_Get_MessageThreads mbxThreadsIterator = new Process_AllMails_Get_MessageThreads(folderFilter) {
         MailboxIterator_Dont_ProcessDone = true
      };

      //++ Iterator starten
      Main_MailboxIterator.IterateMailbopxFolders
         (RuntimeConfig_MailServer.CfgImapServerRl
          , RuntimeConfig_MailServer.CfgImapServerCredAktiv
          , mbxThreadsIterator
          , true
          , null);

      //+ Die Daten speichern
      mbxThreads.ØAddorSetData(cfgImapServerCred.CredKey, mbxThreadsIterator);
   }


   /// <summary>
   /// Liest für die Mailbox alle Threads neu vom Server ein
   /// </summary>
   /// <param name="cfgImapServerCred"></param>
   public void Update_Mbx_MailThreads(Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred) {
      Read_Mbx_MailThreads(cfgImapServerCred);
   }


   /// <summary>
   /// Sucht in den Mail-Threads mbxThreads die Node, die unsere bo_Mail beinhaltet
   /// liest von den Mails dieser Node alle MsgSummaries neu vom Server
   /// und aktualisiert dann unseren Cache wieder
   /// </summary>
   /// <param name="cfgImapServer"></param>
   /// <param name="bo_Mail"></param>
   public void Update_Mbx_MailThread_Node(Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred, BO_Mail bo_Mail) {

      //+ Wenn wir noch keine Daten haben, die Mailbox lesen
      if (!mbxThreads.ContainsKey(cfgImapServerCred.CredKey)) {
         Read_Mbx_MailThreads(cfgImapServerCred);

         // Die Daten sind aktualisiert, also sind wird fertig
         return;
      }

      //+ Alle Mbx Threads holen
      Process_AllMails_Get_MessageThreads mbxData = mbxThreads[cfgImapServerCred.CredKey];

      //+ Für unsere Mail den Thread suchen
      var myMsgThread = MailKit_JigTools_MessageThread.SearchMsgThread_forMsg(mbxData.AllMsgThreads, bo_Mail.MsgHdr_MessageId);

      //+ Die neuen Daten sammeln
      List<IMessageSummary> newMsgSummaries = new List<IMessageSummary>();

      // Die Liste aller Message-IDs der Mails unseres Threads
      List<string> myMailThreadMailsMessageIDs = new List<string>();

      if (myMsgThread.Any()) {
         //+ Vom Thread alle MsgSummaries abrufen
         var myMsgThreadMsgSummaries = MailKit_JigTools_MessageThread.GetMessageThread_Messages_Recursive(myMsgThread[0]);
         myMailThreadMailsMessageIDs = myMsgThreadMsgSummaries.Select(x => x.Envelope.MessageId).ToList();

         //+ Aus jedem Ordner die Mails wieder holen
         //++ Die Mails nach ihren Ordnern gruppieren
         IEnumerable<IGrouping<IMailFolder, IMessageSummary>> mbxOrdner = myMsgThreadMsgSummaries.GroupBy(x => x.Folder);

         foreach (var ordnerGrp in mbxOrdner) {
            //++ Das Header-Query mit den Message-ID zusamenstellen 
            var                msgIDs            = ordnerGrp.Select(x => x.Envelope.MessageId);
            HeaderSearchQuery? headerSearchQuery = null;

            foreach (var msgID in msgIDs.Where(x => x.ØHasValue())) {
               if (headerSearchQuery == null) {
                  headerSearchQuery = SearchQuery.HeaderContains("Message-Id", msgID);
               }
               else {
                  headerSearchQuery.Or(SearchQuery.HeaderContains("Message-Id", msgID));
               }
            }

            // Mit der Qry die IDs suchen
            var uids = ordnerGrp.Key.Search(headerSearchQuery);

            // Mit den IDs die Daten abrufen
            newMsgSummaries.AddRange
               (ordnerGrp.Key.Fetch
                   (uids
                    , MessageSummaryItems.Envelope
                      | MessageSummaryItems.BodyStructure
                      | MessageSummaryItems.UniqueId
                      | MessageSummaryItems.Size
                      | MessageSummaryItems.Headers

                      // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
                      | MessageSummaryItems.InternalDate
                      | MessageSummaryItems.References
                      | MessageSummaryItems.Flags));
         }

      }

      //+ Die neue Liste zusammenstellen

      var DebugStart = mbxData.AllMsgSummary[^5];

      //++ Die Msg Summaries ohne die neu abgerufenen Mails
      var newMsgSummary = mbxData.AllMsgSummary.Where(x => myMailThreadMailsMessageIDs.Contains(x.Envelope.MessageId)).ToList();

      //++ Die neu abgerufenen Mails ergänzen
      newMsgSummary.AddRange(newMsgSummaries);

      //++ Die mbx Daten neu setzen
      mbxData.AllMsgSummary = newMsgSummary;

      //++ Abgeleitete Daten zurücksetzen
      mbxData.Reset_Abgeleitete_Daten();

      // ‼ Testen:
      // Wird mbxThreads[cfgImapServerCred.CredKey] automatisch aktualisiert? 
      var DebugEnde1 = mbxData.AllMsgSummary[^5];

      Process_AllMails_Get_MessageThreads mbxDataTest = mbxThreads[cfgImapServerCred.CredKey];
      var                                 DebugEnde2  = mbxDataTest.AllMsgSummary[^5];

      var stopper = 1;

   }

}
