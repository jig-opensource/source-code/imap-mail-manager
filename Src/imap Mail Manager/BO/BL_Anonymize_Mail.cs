using System;
using System.Linq;
using System.Windows.Interop;
using HtmlAgilityPack;
using ImapMailManager.Config;
using ImapMailManager.Mail;
using ImapMailManager.Mail.MimeKit;
using jig.Tools;
using jig.Tools.HTML;
using jig.Tools.Parser;
using jig.Tools.String;


namespace ImapMailManager.BO;

/// <summary>
/// Anonymisiert eine E-Mail
/// </summary>
public class BL_Anonymize_Mail {

   /// <summary>
   /// Im Mail Text werden anonymisierte Daten mit diesem Begriff ersetzt
   /// </summary>
   private const string AnonymizedData_TextmarkerMsgHdr = "Anonymisiert";

   // private const string AnonymizedData_TextmarkerTxt = "<Anonymisiert>";

   // Unicode-Zeichen
   // !Q https://www.compart.com/de/unicode/bidiclass/ON
   // U+301C   〜    Rot, https://www.compart.com/de/unicode/U+301C
   // U+3030   〰    Rot, https://www.compart.com/de/unicode/U+3030
   // U+2307   ⌇     Sw, https://www.fileformat.info/info/unicode/char/2307/index.htm 
   // U+2300   ⌀     Sw, https://www.fileformat.info/info/unicode/char/2300/index.htm
   // U+166D   ᙭     Sw, https://www.fileformat.info/info/unicode/char/166d/index.htm
   // U+1392   ᎒     Sw,  https://www.fileformat.info/info/unicode/char/1392/index.htm
   // U+2588   █     Sw, https://www.fileformat.info/info/unicode/char/2588/index.htm
   // U+1801   ᠁    Rot, https://www.compart.com/de/unicode/U+1801
   // U+220E   ∎     Rot, https://www.compart.com/de/unicode/U+220E
   // U+23F9   ⏹    Rot, https://www.compart.com/de/unicode/U+23F9
   // U+23FA   ⏺    Rot, https://www.compart.com/de/unicode/U+23FA
   // U+2421   ␡     Rot, https://www.compart.com/de/unicode/U+2421
   // U+2508   ┈     Rot, https://www.compart.com/de/unicode/U+2508
   // U+2509   ┉     Rot, https://www.compart.com/de/unicode/U+2509
   // U+2505   ┅     Rot, https://www.compart.com/de/unicode/U+2505
   // U+2588   █     Rot, https://www.compart.com/de/unicode/U+2588
   // U+25AC   ▬    Rot, https://www.compart.com/de/unicode/U+25AC
   // U+2672   ♲    Rot, https://www.compart.com/de/unicode/U+2672
   // U+267A   ♺    Rot, https://www.compart.com/de/unicode/U+267A
   // U+267B   ♻    Rot, https://www.compart.com/de/unicode/U+267B
   // U+26AB   ⚫   Gu, https://www.compart.com/de/unicode/U+26AB
   // U+26D4   ⛔   Rot, https://www.compart.com/de/unicode/U+26D4
   // U+2718   ✘    Rot, https://www.compart.com/de/unicode/U+2718
   // U+2716   ✖    Rot, https://www.compart.com/de/unicode/U+2716
   // U+2717   ✗    Rot, https://www.compart.com/de/unicode/U+2717
   // U+2731   ✱   Rot, https://www.compart.com/de/unicode/U+2731
   // U+2981   ⦁    Rot, https://www.compart.com/de/unicode/U+2981
   // U+29BE   ⦾   Rot, https://www.compart.com/de/unicode/U+29BE
   // U+29BF   ⦿   Rot, https://www.compart.com/de/unicode/U+29BF
   // U+29D8   ⧘    Rot, https://www.compart.com/de/unicode/U+29D8
   // U+29D9   ⧙    Rot, https://www.compart.com/de/unicode/U+29D9
   // U+29DA   ⧚    Rot, https://www.compart.com/de/unicode/U+29DA
   // U+29DB   ⧛    Rot, https://www.compart.com/de/unicode/U+29DB
   // U+2A33   ⨳   Rot, https://www.compart.com/de/unicode/U+2A33
   // U+2B1B   ⬛  Sw, https://www.compart.com/de/unicode/U+2B1B
   // U+2B24   ⬤   Rot, https://www.compart.com/de/unicode/U+2B24
   // U+2B2E   ⬮   Rot, https://www.compart.com/de/unicode/U+2B2E
   // U+2B55   ⭕   Rot, https://www.compart.com/de/unicode/U+2B55
   // U+2E3A   ⸺   Sw, https://www.compart.com/de/unicode/U+2E3A
   // U+2E3B   ⸻   Sw, https://www.compart.com/de/unicode/U+2E3B
   // U+2E3D    ⸽     Rot, https://www.compart.com/de/unicode/U+2E3D
   // U+2E3E    ⸾     Rot, https://www.compart.com/de/unicode/U+2E3E
   // U+301C    〜    Rot, https://www.compart.com/de/unicode/U+301C
   // U+3030    〰    Rot, https://www.compart.com/de/unicode/U+3030
   // U+FE4F    ﹏    Rot, https://www.compart.com/de/unicode/U+FE4F
   // U+FE4D    ﹍    Rot, https://www.compart.com/de/unicode/U+FE4D
   // U+FE4E    ﹎    Rot, https://www.compart.com/de/unicode/U+FE4E
   // U+FFED    ￭     Rot, https://www.compart.com/de/unicode/U+FFED
   // U+FFEE    ￮     Rot, https://www.compart.com/de/unicode/U+FFEE
   // U+11054   𑁔     Rot, https://www.compart.com/de/unicode/U+11054
   // U+1D300   𝌀    Rot, https://www.compart.com/de/unicode/U+1D300
   // U+1F534   🔴   Rot, https://www.compart.com/de/unicode/U+1F534
   // U+1F535   🔵   Bu, https://www.compart.com/de/unicode/U+1F535
   // U+1F536   🔶   Or, https://www.compart.com/de/unicode/U+1F536
   // U+1F537   🔷   Bu, https://www.compart.com/de/unicode/U+1F537
   // U+1F538   🔸    Or, https://www.compart.com/de/unicode/U+1F538
   // U+1F539   🔹    Bu, https://www.compart.com/de/unicode/U+1F539
   // U+1F7B5   🞵    Rot, https://www.compart.com/de/unicode/U+1F7B5
   // U+1F7B6   🞶    Rot, https://www.compart.com/de/unicode/U+1F7B6
   // U+1F7BB   🞻    Rot, https://www.compart.com/de/unicode/U+1F7BB
   // U+1F7BC   🞼    Rot, https://www.compart.com/de/unicode/U+1F7BC
   // U+1F7BD   🞽    Rot, https://www.compart.com/de/unicode/U+1F7BD
   // U+1F7BE   🞾    Rot, https://www.compart.com/de/unicode/U+1F7BE
   // U+1F7E0   🟠   Or, https://www.compart.com/de/unicode/U+1F7E0
   // U+1F7E1   🟡   Gb, https://www.compart.com/de/unicode/U+1F7E1
   // U+1F7E2   🟢   Gn, https://www.compart.com/de/unicode/U+1F7E2
   // U+1F7E3   🟣   Vio, https://www.compart.com/de/unicode/U+1F7E3
   // U+1F7E4   🟤   Bn, https://www.compart.com/de/unicode/U+1F7E4
   // U+1F7E5   🟥   Rot, https://www.compart.com/de/unicode/U+1F7E5
   // U+1F7E6   🟦   Bu, https://www.compart.com/de/unicode/U+1F7E6
   // U+1F7E7   🟧   Or, https://www.compart.com/de/unicode/U+1F7E7
   // U+1F7E8   🟨   Gb, https://www.compart.com/de/unicode/U+1F7E8
   // U+1F7E9   🟩   Gn, https://www.compart.com/de/unicode/U+1F7E9
   // U+1F7EA   🟪   Vio, https://www.compart.com/de/unicode/U+1F7EA
   // U+1F7EB   🟫   Bn, https://www.compart.com/de/unicode/U+1F7EB

   //+ Ersatz-Text für sensible Personendaten
   private const string AnonymizedData_TextmarkerTxt = "⛔";

   // private const string AnonymizedData_TextmarkerHtml = @"<span style=""color:red"">&lt;Anonymisiert&gt;</span>";
   private const string AnonymizedData_TextmarkerHtml = @"⛔";

   //+ Ersatz-Text für Tel.-Nr
   private const string AnonymizedTelNr_TextmarkerTxt  = "(⛔ TelNr)";
   private const string AnonymizedTelNr_TextmarkerHtml = @"(⛔ TelNr)";

   /// <summary>
   /// Liste von Msg Header, die bei der Anonymisierung gelöscht werden
   /// </summary>
   private static string[] HeadersToDelete = {
      "From", "Return-Path", "Authentication-Results", "Received-SPF"
   };


   /// <summary>
   /// Anonymisiert eine E-Mail
   /// Wenn die Personendaten noch nicht parsed wurden, dann wird das nachgeholt
   /// </summary>
   /// <param name="boMail"></param>
   /// <param name="recFragesteller">
   /// Wenn nicht vorhanden, dann werden nur die bekannten Msg Header gelöscht,
   /// die persönliche Daten haben 
   /// </param>
   internal static void Anonymize_Mail(BO_Mail boMail) {

      //+ Die verschlüsselten, persönlichen Daten aus dem Msg Header holen
      var currRecFragesteller = boMail.Get_MsgHdr_Encrypted_Fragesteller_Daten();

      //+ Wenn wir keine Daten haben, dann bestimmen wir sie
      if (currRecFragesteller == null) {
         currRecFragesteller = boMail.Analyze_and_AddOrUpdate_MsgHdr_FragestellerPersoenlicheDaten();
      }

      //+ Den Header anonymisieren
      Anonymize_Mail_MsgHeader_ReplaceText(boMail, currRecFragesteller);

      //+ Den Msg Body anonymisieren
      Anonymize_Mail_Body(boMail, currRecFragesteller);
   }


   /// <summary>
   /// Anonymisiert die Mail-Header, indem die Worte ersetzt werden
   ///
   /// Alternative:
   /// <seealso cref="ImapMailManager.BO.BL_Anonymize_Mail.Anonymize_Mail_MsgHeader_DeleteHdr"/>
   /// 
   /// </summary>
   /// <param name="thisMsg"></param>
   private static void Anonymize_Mail_MsgHeader_ReplaceText(
      BO_Mail               boMail
      , RecFragestellerV01? recFragesteller) {

      //+ Allenfalls alle Header lesen
      // !M http://www.mimekit.net/docs/html/T_MailKit_MessageSummaryItems.htm
      if (!boMail.HasMsgSummaryHeaders) {
         boMail.Srv_Load_Msg_SummaryData();
      }

      // 🚩 Debug: Alle Header ausgeben
      /*
      foreach (var mailHeader in allMailHeaders) {
         Debug.WriteLine($"\n{mailHeader.Field}:");
         Debug.WriteLine($"{mailHeader.Value}");
      }
      */

      //+ Wenn wir im Header individuelle Personendaten haben, diese anonymisieren, nicht löschen
      if (recFragesteller != null && recFragesteller.HasData()) {
         //++ Jedes Wort mit persönlichen Daten löschen
         foreach (var wort in recFragesteller.Get_PersönlicheDaten_AlsWorte()) {
            //+++ In allen MsgSummary-Headern die persönlichen Daten löschen
            if (boMail.MsgSummary?.Headers != null) {
               foreach (var msgSumHdr in boMail.MsgSummary.Headers
                                               .Where
                                                   (msgSumHdr =>

                                                       // Die App Msg Header nicht anonymisieren
                                                       !ConfigMail_MsgHeader.AppEMailHeader_Names.Contains
                                                          (msgSumHdr.Field)
                                                       && msgSumHdr.Value.Contains
                                                          (wort, StringComparison.OrdinalIgnoreCase))) {
                  boMail.IsDirty = true;

                  msgSumHdr.Value = msgSumHdr.Value.Replace
                     (wort
                      , AnonymizedData_TextmarkerMsgHdr
                      , StringComparison.OrdinalIgnoreCase);
               }
            }

            //+++ In allen Msg-Headern die persönlichen Daten löschen
            if (boMail.Msg != null) {
               foreach (var msgHdr in boMail.Msg.Headers
                                            .Where
                                                (
                                                 msgHdr =>

                                                    // Die App Msg Header nicht anonymisieren
                                                    !ConfigMail_MsgHeader.AppEMailHeader_Names.Contains(msgHdr.Field)
                                                    && msgHdr.Value.Contains
                                                       (wort, StringComparison.OrdinalIgnoreCase))) {
                  boMail.IsDirty = true;

                  msgHdr.Value = msgHdr.Value.Replace
                     (wort
                      , AnonymizedData_TextmarkerMsgHdr
                      , StringComparison.OrdinalIgnoreCase);
               }
            }
         }
      }
   }


   /// <summary>
   /// Anonymisiert die Mail-Header, indem sie gelöscht werden
   ///
   /// Alternative:
   /// <seealso cref="ImapMailManager.BO.BL_Anonymize_Mail.Anonymize_Mail_MsgHeader_ReplaceText"/>
   /// 
   /// </summary>
   /// <param name="thisMsg"></param>
   private static void Anonymize_Mail_MsgHeader_DeleteHdr(
      BO_Mail               boMail
      , RecFragestellerV01? recFragesteller) {

      //+ Allenfalls alle Header lesen
      // !M http://www.mimekit.net/docs/html/T_MailKit_MessageSummaryItems.htm
      if (!boMail.HasMsgSummaryHeaders) {
         boMail.Srv_Load_Msg_SummaryData();
      }

      // 🚩 Debug: Alle Header ausgeben
      /*
      foreach (var mailHeader in allMailHeaders) {
         Debug.WriteLine($"\n{mailHeader.Field}:");
         Debug.WriteLine($"{mailHeader.Value}");
      }
      */

      //+ Alle Header löschen, die immer Personendaten haben
      foreach (var headerToDelete in HeadersToDelete) {
         //++ Alle passenden Header löschen 
         //+++ Msg Summary Headers bereinigen
         boMail.MsgSummary?
               .Headers
               .Where
                   (hdr => hdr.Field.ØEqualsIgnoreCase
                       (headerToDelete)).ToList()
               .ForEach
                   (hdr => {
                       boMail.IsDirty = true;
                       boMail.MsgSummary.Headers.Remove(hdr);
                    });

         //+++ Msg Headers bereinigen
         boMail.Msg?
               .Headers
               .Where
                   (hdr => hdr.Field.ØEqualsIgnoreCase
                       (headerToDelete)).ToList()
               .ForEach
                   (hdr => {
                       boMail.IsDirty = true;
                       boMail.Msg.Headers.Remove(hdr);
                    });
      }

      //+ Wenn wir individuelle Personendaten haben, diese löschen
      if (recFragesteller != null && recFragesteller.HasData()) {

         var persönlicheDaten_Worte = recFragesteller.Get_PersönlicheDaten_AlsWorte();

         //++ Alle Header löschen, die Begriffe haben, die zu den persönlichen Daten gehören 
         //+++ Msg Summary Headers bereinigen
         if (boMail.MsgSummary != null)
            for (var idx = boMail.MsgSummary.Headers.Count - 1; idx >= 0; idx--) {
               var msgHeader = boMail.MsgSummary.Headers[idx];

               //+++ Wenn im Header das Wort vorkommt, dann den Header löschen
               if (persönlicheDaten_Worte.ØEqualsWithAnyItemInList(msgHeader.Value)) {
                  boMail.IsDirty = true;
                  boMail.MsgSummary!.Headers.Remove(msgHeader);
               }
            }

         //+++ Msg Headers bereinigen
         if (boMail.Msg != null)
            for (var idx = boMail.Msg.Headers.Count - 1; idx >= 0; idx--) {
               var msgHeader = boMail.Msg.Headers[idx];

               //+++ Wenn im Header das Wort vorkommt, dann den Header löschen
               if (persönlicheDaten_Worte.ØEqualsWithAnyItemInList(msgHeader.Value)) {
                  boMail.IsDirty = true;
                  boMail.Msg!.Headers.Remove(msgHeader);
               }
            }
      }

   }


   ///  <summary>
   ///  Löscht im Mail-Body alle persönlichen Daten
   ///  </summary>
   ///  <param name="boMail"></param>
   ///  <param name="recFragesteller"></param>
   ///  <returns>
   /// True, wenn der Msg Inhalt geändert wurde
   ///  </returns>
   private static void Anonymize_Mail_Body(
      BO_Mail               boMail
      , RecFragestellerV01? recFragesteller) {

      //+ Wenn wir keine Informationen zum Fragesteller haben, dann sind wir fertig
      if (recFragesteller == null) { return; }

      //+ Die Nachricht lesen
      if (!boMail.HasMsg) {
         boMail.Srv_Load_Msg_Data();
      }

      //+ Die Nachrichtentexte holen
      var (textBodyText, htmlBodyText) = MimeKit_JigTools.Get_Msg_Bodies(boMail.Msg!, true);

      //+ Den HTML Body anonymisieren
      if (htmlBodyText.ØHasValue()) {
         var msgHtmlDoc = new HtmlDocument();
         msgHtmlDoc.LoadHtml(htmlBodyText);

         //++ Den Html Body-Block holen
         HtmlNode? bodyHtml = msgHtmlDoc!.DocumentNode.SelectSingleNode("//body");

         if (bodyHtml != null) {
            var (textHasChanged, newText) = AnonymizeText
               (bodyHtml.InnerHtml
                , true
                , recFragesteller);

            //++ Wenn der Body-Block geändert hat, diesen in der Nachricht ersetzen
            if (textHasChanged) {
               boMail.IsDirty = true;

               // Im HTML die Node mit dem anonymisierten Text ersetzen
               bodyHtml.InnerHtml = newText;
               // In ganzen HtmlDoc die Body-Bode ersetzen
               HtmlTools.Replace_BodyNode(msgHtmlDoc, bodyHtml.OuterHtml);

               //+ Wenn die Mail einen Text Body hat, den HTML-Block in Text konvertieren
               string? newTextBody = null;
               if (textBodyText.ØHasValue()) {
                  // Den anonymisierten HTML Text in normalen Text konvertieren
                  newTextBody = BL_Mail_Common.OHtmlToText.Convert(msgHtmlDoc.DocumentNode.OuterHtml);
               }

               // In der E-Mail die anonymisierten Bodies setzen  
               MimeKit_JigTools.Update_Msg_Bodies(boMail.Msg!, newTextBody, msgHtmlDoc.DocumentNode.OuterHtml);
            }
         }
      }
      else if (textBodyText.ØHasValue()) {
         //+ Wenn die Mail nur einen Text Body hat, alle Personaldaten löschen
         // Wir haben keinen html Body, also den Text selber anonymisieren
         var (textHasChanged, newText) = AnonymizeText
            (textBodyText, false, recFragesteller);

         //++ Wenn der Text geändert hat, diesen in der Nachricht ersetzen
         if (textHasChanged) {
            boMail.IsDirty = true;
            MimeKit_JigTools.Update_Msg_Bodies(boMail.Msg!, newText, null);
         }
      }
   }


   /// <summary>
   /// Anonymisiert text, indem die Daten aller Props in recFragesteller
   /// gesucht und mit nichts ersetzt werden
   /// </summary>
   /// <param name="text">
   /// In diesem Text wird der Inhalt ersetzt
   /// </param>
   /// <param name="textIsHtml"></param>
   /// <param name="recFragesteller"></param>
   /// <param name="markReplacedText">
   /// True: Die ersetzten Personendaten mit einem Ersatztext markieren, z.Z.: ⛔
   /// False: Die ersetzten Personendaten löschen   
   /// </param>
   /// <returns>
   /// var (textHasChanged, newText) = …  
   /// </returns>
   private static Tuple<bool, string> AnonymizeText(
      string?               text
      , bool                textIsHtml
      , RecFragestellerV01? recFragesteller
      , bool                markReplacedText = true) {

      //+ Keine Daten erhalten - fertig
      if (!text.ØHasValue() || recFragesteller == null) {
         return new Tuple<bool, string>(false, "");
      }

      //+ Berechnen, mit welchem Ersatztext ersetzte Personendaten und TelNr markiert werden
      var (anonymizedText_ReplacementText, anonymizedTelNr_ReplacementText) = (markReplacedText, textIsHtml) switch {
         { markReplacedText: true, textIsHtml: true } =>
            (AnonymizedData_TextmarkerHtml, AnonymizedTelNr_TextmarkerHtml)
         , { markReplacedText: true, textIsHtml: false } => (AnonymizedData_TextmarkerTxt
                                                             , AnonymizedTelNr_TextmarkerTxt)

         // den eretzten Text nicht markieren, sondern durch einen leeren String ersetzen
         , _ => ("", "")
      };

      //+ Die sensiblen Worte holen 
      var persönlicheDaten_Worte  = recFragesteller.Get_PersönlicheDaten_AlsWorte();
      var bodyInnerHtmlHasChanged = false;

      //+++ Die Worte ersetzen
      foreach (var wort in persönlicheDaten_Worte) {
         //+++ Haben wir den puren Text?
         if (text!.Contains(wort, StringComparison.OrdinalIgnoreCase)) {
            // Dann das Wort ersetzen
            bodyInnerHtmlHasChanged = true;

            text = text!.Replace
               (wort
                , anonymizedText_ReplacementText
                , StringComparison.OrdinalIgnoreCase);
         }
      }

      //++ Die Tel.-Nummern ersetzen
      (bool hasMatch, text) = Parse_Telefonnummer.Replace_Telefonnummer_Pcre2
         (text
          , anonymizedTelNr_ReplacementText
          , ConfigTel_Basics
            .TelNrMatch_Predicate);

      if (hasMatch) { bodyInnerHtmlHasChanged = true; }

      return new Tuple<bool, string>(bodyInnerHtmlHasChanged, text!);
   }

}
