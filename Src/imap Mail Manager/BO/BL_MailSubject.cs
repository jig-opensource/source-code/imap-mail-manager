using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using jig.Tools;
using jig.Tools.String;
using MimeKit;


namespace ImapMailManager.BO;

/// <summary>
/// Struktur des E-Mail Betreffs
/// [Sprachkürel] [Präfixe] [Betreff]  
/// 
/// ⭕DE Re: Fw: Betreff
/// 
/// </summary>
public class BL_MailSubject {

   /// <summary>
   ///+ Dient zum erkennen vom E-Mail-Typ:
   ///+ - AW / Antwort
   ///+ - FW / Weitergeleitet
   /// </summary>
   public enum EmpfangenerEMailTyp {

      Antwort, Weitergeleitet
      , Unknown

   }


   /// <summary>
   /// Markiert das Sprach-Kürtel im Betreff, e.g.
   /// ¤DE ¤EN ¤FR
   /// ⭕DE ⭕EN ⭕FR
   /// </summary>
   public static readonly string SubjectLanguageMarker = "⭕";

   #region Regex Sprach-Kürzel im Betreff

   private static readonly RegexOptions RegexOptionSprachkürzel = RegexOptions.IgnoreCase
                                                                  | RegexOptions.Multiline
                                                                  | RegexOptions.ExplicitCapture
                                                                  | RegexOptions.CultureInvariant
                                                                  | RegexOptions.IgnorePatternWhitespace
                                                                  | RegexOptions.Compiled;

   private static readonly string RgxSprachKürzelImBetreff = @"
                                             (?<AlleSprachkürzel>
                                               (?:
                                                 (?<SprachkürzelVoll>
                                                   ⭕
                                                   (?<Sprachkürzel>[a-zA-Z]{2,3})
                                                 )
                                                 (?:[ \t]+|$)
                                               )+
                                             )";

   private static readonly Regex ORgxSprachKürzelImBetreff
      = new(RgxSprachKürzelImBetreff, RegexOptionSprachkürzel);

   #endregion Regex Sprach-Kürzel im Betreff

   #region Regex Präfix im Betreff einfacher und kompletter Regex

   private static readonly RegexOptions MyRegexOptions = RegexOptions.IgnoreCase
                                                         | RegexOptions.Multiline
                                                         | RegexOptions.ExplicitCapture
                                                         | RegexOptions.CultureInvariant
                                                         | RegexOptions.IgnorePatternWhitespace
                                                         | RegexOptions.Compiled;

   /// <summary>
   /// Erkennt alle Arten von Weiterleitungs- und Antworten-Präfixes
   /// Sie werden mit Named Groups Separat erkannt, damit sie gezielt standardisiert werden können  
   /// </summary>
   private static readonly string RgxPrefixesComplete = @"
                                       (?<Any>
                                          (?<PrefixAntworten>
                                            # Re|Res|…..|P.S.|...
                                            \b(?:R|Re|Res|AW|Antwort|P\.[ ]*S\.)
                                            # Gefolgt von mind. 1 Doppelpunkt oder Space
                                            (?:;|:|[ ])+
                                          )
                                          |
                                          (?<PrefixWeiterleitungen>
                                            # Re|Res|…..|P.S.|...
                                            \b(?:WG|Fwd|Fw)
                                            # Gefolgt von mind. 1 Doppelpunkt oder Space
                                            (?:;|:|[ ])+
                                          )
                                       )*
                                          ";

   private static readonly Regex ORgxMailSubject_PrefixesComplete
      = new($"^(\\s*{RgxPrefixesComplete})+", MyRegexOptions);

   #endregion Regex Präfix im Betreff einfacher und kompletter Regex

   #region Regex E-Mail Betreff nested Präfix-Klassen

   /// <summary>
   /// Klassifiziert Antwort-Präfixes
   /// </summary>
   private static readonly string RgxPrefixesSimpleRe = "(RE?S?|AW|Antwort)";

   private static readonly Regex ORgxMailSubject_SimplePrefixRe
      = new($"^(\\s*{RgxPrefixesSimpleRe}[ ;:]*[;:])+", MyRegexOptions);

   /// <summary>
   /// Klassifiziert Weiterleitungs-Präfixes
   /// </summary>
   private static readonly string RgxPrefixesSimpleFwd = "(FYI|WG|FWD?)";
   private static readonly Regex ORgxMailSubject_SimplePrefixFwd
      = new($"^(\\s*{RgxPrefixesSimpleFwd}[ ;:]*[;:])+", MyRegexOptions);

   /// <summary>
   /// Findet E-Mail Betreff Präfixe wie RE: FWD:
   /// </summary>
   private static readonly string RgxPrefixesSimple = "(RE?S?|FYI|AW|Antwort|WG|FWD?)";

   /// <summary>
   /// Behandelt normale Präfixe
   /// 
   /// Trennt den E-Mail Betreff:
   /// Normale Präfixe und den eigentlichen Betreff
   /// 
   /// Der Regex-String
   /// </summary>
   private static readonly string SRgxMailSubject_SimplePrefixDetector
      = $"^(\\s*{RgxPrefixesSimple}[ ;:]*[;:])*(?<Subj>[^\\r\\n]*)";
   internal static readonly Regex ORgxMailSubject_SimplePrefixDetector
      = new(SRgxMailSubject_SimplePrefixDetector, MyRegexOptions);

   #endregion Regex E-Mail Betreff nested Präfix-Klassen

   #region Rgx E-Mail Betreff mit Präfixes in eckigen klammern

   /// <summary>
   /// Behandelt Präfixe mit eckigen Klammern
   /// 
   /// Trennt den E-Mail Betreff:
   /// Präfixe mit eckigen Klammern und den eigentlichen Betreff
   /// 
   /// Der Regex-String
   /// 
   /// ^(\s*\[(RE?S?|FYI|AW|WG|FWD?)[ ;:\]]+)*(?<Subj>[^\r\n]*)
   /// </summary>
   private static readonly string SRgxMailSubject_EckigeKlammerPrefixDetector
      = $"^(\\s*\\[{RgxPrefixesSimple}[ ;:\\]]+)*(?<Subj>[^\\r\\n]*)";
   internal static readonly Regex ORgxMailSubject_EckigeKlammerPrefixDetector = new Regex
      (SRgxMailSubject_EckigeKlammerPrefixDetector, MyRegexOptions);

   #endregion Rgx E-Mail Betreff mit Präfixes in eckigen klammern

   #region Rgx E-Mail Betreff mit Präfixes in runden Klammern

   /// <summary>
   /// Behandelt Präfixe mit runden Klammern
   /// 
   /// Trennt den E-Mail Betreff:
   /// Präfixe mit eckigen Klammern und den eigentlichen Betreff
   /// 
   /// Der Regex-String
   /// ^(\s*\[(RE?S?|FYI|AW|WG|FWD?)[ ;:\]]+)*(?<Subj>[^\r\n]*) 
   /// </summary>
   private static readonly string SRgxMailSubject_RundeKlammerPrefixDetector
      = $"^\\s*(?<Pre>[^\\(]*)\\s+(\\({RgxPrefixesSimple}\\s*\\))+(?<Post>[^\\r\\n]*)";
   internal static readonly Regex ORgxMailSubject_RundeKlammerPrefixDetector
      = new(SRgxMailSubject_RundeKlammerPrefixDetector, MyRegexOptions);

   #endregion Rgx E-Mail Betreff mit Präfixes in runden Klammern

   #region Rgx E-Mail Betreff mit Präfixes in verschachtelten eckige Klammern

   /// <summary>
   /// Behandelt Präfixe in verschachtelten eckige Klammern
   /// 
   /// Trennt den E-Mail Betreff:
   /// Präfixe mit eckigen Klammern und den eigentlichen Betreff
   /// 
   /// Der Regex-String
   /// ^(\s*\[(RE?S?|FYI|AW|WG|FWD?)[ ;:\]]+)*(?<Subj>[^\r\n]*) 
   /// </summary>
   private static readonly string SRgxMailSubject_NestedEckigeKlammerPrefixDetector = @"
		  # Verschachtelte eckige Klammern
		  (?<Pre>.*)
		  (?<OuterScriptBlock>
			 \[                   # Match {
			 (\s*(RE?S?|FYI|AW|WG|FWD?)[ ;:]+)
				(?<InnerScriptBlock>
				  [^\[\]]+             # all chars except {}
				  | (?<Level>\[)     # or if { then Level += 1
				  | (?<-Level>\])    # or if } then Level -= 1
				)+                  # Repeat (to go from inside to outside)
			 (?(Level)(?!))       # zero-width negative lookahead assertion
			 \]
		  )
		  (?<Post>[^\r\n]*)
	  ";
   internal static readonly Regex ORgxMailSubject_NestedEckigeKlammerPrefixDetector
      = new(SRgxMailSubject_NestedEckigeKlammerPrefixDetector, MyRegexOptions);

   #endregion Rgx E-Mail Betreff mit Präfixes in verschachtelten eckige Klammern

   private string OriSubject;

   /// <summary>
   /// Das erste gefundene Präfix
   /// </summary>
   EmpfangenerEMailTyp _firstPrefix
      = EmpfangenerEMailTyp.Unknown;
   /// <summary>
   /// Das letzte gefundene Präfix
   /// </summary>
   EmpfangenerEMailTyp _lastPrefix
      = EmpfangenerEMailTyp.Unknown;

   private readonly string textDavor;
   private readonly string textDanach;
   private readonly string sprachKürzel;


   /// <summary>
   /// Konstruktor
   /// </summary>
   /// <param name="subject"></param>
   public BL_MailSubject(string subject) {
      OriSubject = subject;

      if (!subject.ØHasValue()) {
         textDavor = "";

         // Default DE
         sprachKürzel = "DE";
         textDanach   = "";
      }
      else {
         (textDavor, sprachKürzel, textDanach) = Split_Subject_Sprachkürzel(subject);
      }
   }


   /// <summary>
   /// Das Sprachkürzel mit dem Präfix
   /// </summary>
   public string sprachKürzelVoll => @"{SubjectLanguageMarker}{sprachKürzel}";

   /// <summary>
   /// Der ganze Betreff
   /// </summary>
   public string Subject => $"{textDavor}{sprachKürzel}{textDanach}";


   /// <summary>
   /// Split dews Subjects in:
   /// - Text vor dem Sprachkürzel 
   /// - Das Sprachkürzel ⭕XX 
   /// - Text nach dem Sprachkürzel 
   /// </summary>
   /// <param name="subject"></param>
   /// <returns>
   /// Das Sprachkürzel ist ohne Präfix ⭕ 
   /// var (textDavor, sprachKürzel, textDanach) = … 
   /// </returns>
   public static Tuple<string, string, string> Split_Subject_Sprachkürzel(string subject) {
      if (!subject.ØHasValue()) {
         return new Tuple<string, string, string>
            (subject
             , ""
             , "");
      }

      // Das Sprachkürzel suchen
      var match = ORgxSprachKürzelImBetreff.Match(subject);

      if (match.Success) {
         var    sprachKürzel     = match.Groups["Sprachkürzel"].Value;
         var    alleSprachkürzel = match.Groups["AlleSprachkürzel"];
         // var    sprachKürzelVoll = match.Groups["SprachkürzelVoll"];
         string textDavor        = subject.ØSubstring(0, alleSprachkürzel.Index);
         string textDanach       = subject.ØSubstring(alleSprachkürzel.Index + alleSprachkürzel.Length);

         return new Tuple<string, string, string>
            (textDavor
             , sprachKürzel
             , textDanach);
      }

      return new Tuple<string, string, string>
         (subject
          , ""
          , "");
   }


   /// <summary>
   /// 
   /// </summary>
   /// <param name="subject"></param>
   /// <param name="prependErstesStandardsiertesSubjectPräfix">
   /// Soll im bereinigten Betreff das erste gefundene Präfix standardisiert und dann wieder zugefügt werden?
   /// </param>
   /// <param name="prependLetztesStandardsiertesSubjectPräfix">
   /// Soll im bereinigten Betreff das letzte gefundene Präfix standardisiert und dann wieder zugefügt werden?
   /// Es wird nur zugefügt, wenn es unterschiedlich zum ersten Präfix ist
   /// </param>
   /// <returns></returns>
   public static string ØCleanEmailSubjectPrefix(
      string? subject
      , bool  prependErstesStandardsiertesSubjectPräfix  = true
      , bool  prependLetztesStandardsiertesSubjectPräfix = true) {

      if (!subject.ØHasValue()) { return ""; }

      //+ Das erste und letzte Subject Präfix merken wir und in der Standardisierten variante
      EmpfangenerEMailTyp firstPrefix = Get_EmpfangenerEMailTyp(subject!);
      EmpfangenerEMailTyp lastPrefix  = EmpfangenerEMailTyp.Unknown;

      // Die nested Präfixe bereinigen
      var subjectCleaned1 = subject.ØCleanEmailSubjectPrefix_Nested(false);

      // Alle anderen Präfixe bereinigen
      MatchCollection matchCollection;
      int             lastEndofMatchPos = 0;
      StringBuilder   resCleanedSubject = new StringBuilder();

      bool hadMatch;

      do {
         // Alle Präfixes suchen
         matchCollection = ORgxMailSubject_PrefixesComplete.Matches(subjectCleaned1, lastEndofMatchPos);

         hadMatch = false;

         // Die "Any" Regex-Gruppe verarbeiten und alle Präfixe entfernen
         foreach (Match match in matchCollection) {
            var matchAny = match.Groups["Any"];

            // Alle Any Elemente entfernen
            foreach (Capture matchAnyCapture in matchAny.Captures) {
               hadMatch = true;

               // Das Prfäix erkennen / standardisieren
               lastPrefix = Get_EmpfangenerEMailTyp(matchAnyCapture.Value);

               // Den String vor dem Match
               string strBeforeMatch = subjectCleaned1.ØSubstring
                  (lastEndofMatchPos, matchAnyCapture.Index - lastEndofMatchPos);

               // Den String erfassen
               resCleanedSubject.Append(strBeforeMatch);

               // Die neue End-Position
               lastEndofMatchPos = matchAnyCapture.Index + matchAnyCapture.Length;
            }
         }
      }
      while (hadMatch && matchCollection.Count > 0);

      // Vom letzten Präfix bis zum Ende von subject die Zeichen übernehmen
      string lastSubStr = subjectCleaned1.ØSubstring(lastEndofMatchPos, subjectCleaned1.Length - lastEndofMatchPos);
      resCleanedSubject.Append(lastSubStr);

      // Das Resultat bereinigen
      var newSubject = resCleanedSubject.ToString().ØTrimAndReduce();

      // Debug.WriteLine(res.ToString());

      // Allenfalls das letzte Präfix standardisiert ergänzen
      if (prependLetztesStandardsiertesSubjectPräfix && lastPrefix != EmpfangenerEMailTyp.Unknown) {
         newSubject = Prepend_Subject_With_StandardizedPrefix(newSubject, lastPrefix);
      }

      // Allenfalls das letzte Präfix standardisiert ergänzen
      if (prependErstesStandardsiertesSubjectPräfix
          && firstPrefix != EmpfangenerEMailTyp.Unknown) {

         // Nur, wenn das letzte Suffix nicht zugefügt wurde
         // Oder, wenn es zugefügt wurde, nur, wenn es ungleich zum ersten Suffix ist
         if (prependLetztesStandardsiertesSubjectPräfix == false
             || (firstPrefix != lastPrefix)) {
            newSubject = Prepend_Subject_With_StandardizedPrefix(newSubject, firstPrefix);
         }
      }

      return newSubject;
   }


   /// <summary>
   /// Trennt subject:
   /// - Erstes Subject Präfix 
   /// - Letztes Subject Präfix 
   /// - Den Rest des Subjects
   /// </summary>
   /// <param name="subject"></param>
   /// <returns>
   /// var (firstPrefix, lastPrefix, restSubject) = … 
   /// </returns>
   public static Tuple<EmpfangenerEMailTyp, EmpfangenerEMailTyp, string> Get_FirstAndLast_Subject_Prefix(
      string? subject) {
      if (!subject.ØHasValue()) {
         return new Tuple<EmpfangenerEMailTyp, EmpfangenerEMailTyp, string>
            (EmpfangenerEMailTyp.Unknown
             , EmpfangenerEMailTyp.Unknown
             , "");
      }

      // Das erste Präfix holen
      EmpfangenerEMailTyp firstPrefix = Get_EmpfangenerEMailTyp(subject!);
      EmpfangenerEMailTyp lastPrefix  = EmpfangenerEMailTyp.Unknown;

      // Das letzte Präfix suchen
      // Alle anderen Präfixe bereinigen
      MatchCollection matchCollection;
      int             lastEndofMatchPos = 0;
      StringBuilder   resCleanedSubject = new StringBuilder();

      bool hadMatch;

      do {
         // Alle Präfixes suchen
         matchCollection = ORgxMailSubject_PrefixesComplete.Matches(subject, lastEndofMatchPos);

         hadMatch = false;

         // Die "Any" Regex-Gruppe verarbeiten und alle Präfixe entfernen
         foreach (Match match in matchCollection) {
            var matchAny = match.Groups["Any"];

            // Alle Any Elemente entfernen
            foreach (Capture matchAnyCapture in matchAny.Captures) {
               hadMatch = true;

               // Das Prfäix erkennen / standardisieren
               lastPrefix = Get_EmpfangenerEMailTyp(matchAnyCapture.Value);

               // Den String vor dem Match
               string strBeforeMatch = subject.ØSubstring
                  (lastEndofMatchPos, matchAnyCapture.Index - lastEndofMatchPos);

               // Den String erfassen
               resCleanedSubject.Append(strBeforeMatch);

               // Die neue End-Position
               lastEndofMatchPos = matchAnyCapture.Index + matchAnyCapture.Length;
            }
         }
      }
      while (hadMatch && matchCollection.Count > 0);

      // Vom letzten Präfix bis zum Ende von subject die Zeichen übernehmen
      string lastSubStr = subject.ØSubstring(lastEndofMatchPos, subject.Length - lastEndofMatchPos);
      resCleanedSubject.Append(lastSubStr);

      // Das Resultat bereinigen
      var newSubject = resCleanedSubject.ToString().ØTrimAndReduce();

      // Wenn das lastPrefix definiert ist
      // uns es dem firstPrefix entpsorcht, dann dieses auf Unknown setzen
      if (lastPrefix != EmpfangenerEMailTyp.Unknown) {
         if (firstPrefix == lastPrefix) {
            lastPrefix = EmpfangenerEMailTyp.Unknown;
         }
      }

      return new Tuple<EmpfangenerEMailTyp, EmpfangenerEMailTyp, string>
         (firstPrefix
          , lastPrefix
          , newSubject);
   }


   /// <summary>
   /// Splittet den Betreff in:
   /// - Sprachcode
   /// - Das erste gefundene Präfix
   /// - Das letzte gefundene Präfix
   /// - Den Rest des E-Mail Betreffs
   /// </summary>
   /// <param name="subject"></param>
   /// <returns>
   /// var (spracheIso2, eMailTypFirst, eMailTypLast, newSubject) = … 
   /// </returns>
   public static Tuple<string, EmpfangenerEMailTyp, EmpfangenerEMailTyp, string>
      Split_Subject_SprachCode_Präfixes_Subject(string? subject) {

      if (!subject.ØHasValue()) {
         return new Tuple<string, EmpfangenerEMailTyp, EmpfangenerEMailTyp, string>
            (""
             , EmpfangenerEMailTyp.Unknown
             , EmpfangenerEMailTyp.Unknown
             , "");
      }

      // Prepare
      var newSubject  = "";
      var firstPrefix = EmpfangenerEMailTyp.Unknown;
      var lastPrefix  = EmpfangenerEMailTyp.Unknown;

      //+ Zuerst den Sprachcode suchen
      var (textDavor, sprachKürzel, textDanach) = Split_Subject_Sprachkürzel(subject!);

      //+ Das erste und letzte Subject Präfix im Text vor dem Sprachcode suchen
      var (firstPrefixDavor, lastPrefixDavor, restSubjectDavor) = Get_FirstAndLast_Subject_Prefix(textDavor);

      //+ Das erste und letzte Subject Präfix merken wir und in der Standardisierten variante
      var (firstPrefixDanach, lastPrefixDanach, restSubjectDanach) = Get_FirstAndLast_Subject_Prefix(textDanach);

      // Wenn vor dem Sprachkürzel *kein* Prefix definiert war
      // Dann sind die Präfixe nach dem Sprachkürzel nützen
      if (firstPrefixDavor == EmpfangenerEMailTyp.Unknown) {
         if (firstPrefixDanach == EmpfangenerEMailTyp.Unknown) {
            // Kein Prefix gefunden
            firstPrefix = EmpfangenerEMailTyp.Unknown;
            lastPrefix  = EmpfangenerEMailTyp.Unknown;
            newSubject  = $"{restSubjectDavor.Trim()} {restSubjectDanach.Trim()}".ØTrimAndReduce();
         }
         else {
            // Danach ein Prefix gefunden
            firstPrefix = firstPrefixDanach;
            lastPrefix  = lastPrefixDanach;
            newSubject  = $"{restSubjectDanach}".ØTrimAndReduce();
         }
      }
      else {
         // Vor dem Sprachkürzel war ein Prefix definiert
         firstPrefix = firstPrefixDavor;

         // Ist das lastPrefixDanach definiert und ungleich des firstPrefix? 
         if (lastPrefixDanach != EmpfangenerEMailTyp.Unknown
             && lastPrefixDanach != firstPrefix) {
            // Das last Prefix übernehmen
            lastPrefix = lastPrefixDanach;
            newSubject = $"{restSubjectDanach}".ØTrimAndReduce();
         }

         // Ist das firstPrefixDanach definiert und ungleich des firstPrefix? 
         else if (firstPrefixDanach != EmpfangenerEMailTyp.Unknown
                  && firstPrefixDanach != firstPrefix) {
            // Das last Prefix übernehmen
            lastPrefix = firstPrefixDanach;
            newSubject = $"{restSubjectDanach}".ØTrimAndReduce();
         }
         else {
            // Sonst arbeiten wir mit dem lastPrefixDavor  
            lastPrefix = lastPrefixDavor;
            newSubject = $"{restSubjectDavor.Trim()} {restSubjectDanach.Trim()}".ØTrimAndReduce();
         }

      }

      // Wenn das lastPrefix definiert ist und identisch mit firstPrefix ist,
      // dann setzten wir das lastPrefix auf Unknown
      if (lastPrefix != EmpfangenerEMailTyp.Unknown
          && firstPrefix == lastPrefix) {
         lastPrefix = EmpfangenerEMailTyp.Unknown;
      }

      return new Tuple<string, EmpfangenerEMailTyp, EmpfangenerEMailTyp, string>
         (sprachKürzel
          , firstPrefix
          , lastPrefix
          , newSubject);
   }


   #region Hilfsfunktionen zum Subject

   /// <summary>
   /// Liefert True, wenn Subject ein Präfix hat
   /// </summary>
   /// <param name="subject"></param>
   /// <returns></returns>
   public static bool SubjectHasPrefix(string subject) {
      EmpfangenerEMailTyp firstPrefix = Get_EmpfangenerEMailTyp(subject!);

      return firstPrefix != EmpfangenerEMailTyp.Unknown;
   }


   /// <summary>
   /// Ergänzt ein Subject um das Standard-Präfix
   /// </summary>
   /// <param name="subject"></param>
   /// <param name="präfix"></param>
   /// <returns></returns>
   public static string Prepend_Subject_With_StandardizedPrefix(string subject, EmpfangenerEMailTyp präfix) {
      if (!subject.ØHasValue()) { return ""; }
      return $"{Get_StandardizedPrefix_Str(präfix)} {subject}";
   }

   /// <summary>
   /// Berechnet aufgrund vom präfix das Kürzel im Betreff:
   /// - Re: 
   /// - Fw: 
   /// </summary>
   /// <param name="präfix"></param>
   /// <returns>
   /// - "" 
   /// - "Re:" 
   /// - "Fw:" 
   /// </returns>
   public static string Get_StandardizedPrefix_Str(EmpfangenerEMailTyp präfix) {
      return präfix switch {
         EmpfangenerEMailTyp.Antwort          => $"Re:"
         , EmpfangenerEMailTyp.Weitergeleitet => $"Fw:"
         , _                                  => ""
      };
   }


   /// <summary>
   /// Erkennt aufgrund der E-Mail Betreff präfixes, ob es eine Antwort
   /// oder eine Weiterleitung ist
   /// </summary>
   /// <param name="subject"></param>
   public static EmpfangenerEMailTyp Get_EmpfangenerEMailTyp(string subject) {
      if (!subject.ØHasValue()) {
         return EmpfangenerEMailTyp.Unknown;
      }

      var matches = ORgxMailSubject_PrefixesComplete.Matches(subject).ØOrEmptyIfNull().ToArray();

      // Kein Subject Prefix gefunden
      if (!matches.Any()) {
         return EmpfangenerEMailTyp.Unknown;
      }

      // Prüfen, welche der beiden Präfixes wir haben
      var matchPrefixAntworten       = matches[0].Groups["PrefixAntworten"];
      var matchPrefixWeiterleitungen = matches[0].Groups["PrefixWeiterleitungen"];

      // Das zuerst gefundene Element definiert den E-Mail Typ (FW oder RE)
      int posPrefixAntworten = matchPrefixAntworten is { Success: true } ? matchPrefixAntworten.Index : 9999;

      int posPrefixWeiterleitungen
         = matchPrefixWeiterleitungen is { Success: true } ? matchPrefixWeiterleitungen.Index : 9999;

      // Zuerst RE gefunden?
      if (matchPrefixAntworten is { Success: true }) {
         if (posPrefixAntworten < posPrefixWeiterleitungen) {
            return EmpfangenerEMailTyp.Antwort;
         }
      }

      // Zuerst FW gefunden?
      if (matchPrefixWeiterleitungen is { Success: true }) {
         if (posPrefixWeiterleitungen < posPrefixAntworten) {
            return EmpfangenerEMailTyp.Weitergeleitet;
         }
      }

      return EmpfangenerEMailTyp.Unknown;

   }

   #endregion Hilfsfunktionen zum Subject

}
