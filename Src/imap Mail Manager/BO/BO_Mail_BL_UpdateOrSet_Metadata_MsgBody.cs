using System;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using ImapMailManager.Config;
using ImapMailManager.Mail;
using ImapMailManager.Mail.MimeKit;
using jig.Tools.HTML;
using jig.Tools.String;


namespace ImapMailManager.BO;

/// <summary>
/// BL für das Setzen und Aktualisieren von Metadaten im Msg Body
/// </summary>
public class BO_Mail_BL_UpdateOrSet_Metadata_MsgBody {

   #region Props

   private readonly BO_Mail _boMail;

   #endregion Props

   #region Konstruktoren

   /// <summary>
   /// Initialisiert die Klasse mit der BO_Mail
   /// </summary>
   /// <param name="boMail"></param>
   public BO_Mail_BL_UpdateOrSet_Metadata_MsgBody(BO_Mail boMail) => _boMail = boMail;

   #endregion Konstruktoren

   #region BO Methoden

   /// <summary>
   /// Wrapper für: UpdateOrSet_Mail_Metadata_()
   /// damit saveOnDirty einfacher handhabbar ist
   /// 
   /// Setzt oder aktualisiert die Metadaten einer E-Mail
   /// aufgrund des Ablageorts und der Mail-Kategoerie
   /// </summary>
   public void UpdateOrSet_Mail_Metadata(
      bool   saveOnDirty = false
      , bool force       = false

      // Zum Debuggen: Annahme, dass der Aufruf für eine Archiv-Mailbox gilt
      , bool forceProcessingAsArchiveMailbox = false

      // Zum Debuggen: Annahme, dass der Aufruf für die Team-Mailbox gilt
      , bool forceProcessingAsTeamMailbox = false) {
      UpdateOrSet_Mail_Metadata_
         (force
          , forceProcessingAsArchiveMailbox
          , forceProcessingAsTeamMailbox);

      if (saveOnDirty) { _boMail.Save(); }
   }


   /// <summary>
   /// Setzt oder aktualisiert die Metadaten einer E-Mail
   /// aufgrund des Ablageorts und der Mail-Kategoerie 
   /// </summary>
   private void UpdateOrSet_Mail_Metadata_(
      bool force = false
      // Zum Debuggen: Annahme, dass der Aufruf für eine Archiv-Mailbox gilt
      , bool forceProcessingAsArchiveMailbox = false
      // Zum Debuggen: Annahme, dass der Aufruf für die Team-Mailbox gilt
      , bool forceProcessingAsTeamMailbox = false) {

      // Alte Funktion:
      // Update_Mail_Body(force: force);      
      
      //+ Ohne Mailserver können wir keine Daten aktualisieren       
      if (!_boMail.HasImapSrvRuntimeConfig) { return; }

      //+ Bei HTML Mails im Html Header demn Style aktualisieren
      AddOrUpdate_MailMsgBody_Style();

      //» Abbruch, wenn die Mail in einem Ordner ist, in dem keine Metadaten berechnet werden
      if (BO_Mail_BL_UpdateOrSet_Metadata_Common.FolderFilter_AllMetadata_Blacklist.StopFolderProcessing_BlackWhiteList_
             (_boMail.MailFolder)) {
         return;
      }

      //» Die Regeln für die Archiv-Mailbox 
      if (forceProcessingAsArchiveMailbox || _boMail.ImapSrvRuntimeConfig!.IsArchiveMailbox) {
         // ToDo now 🟥 Am Anfang im E-Mail Body zufügen:
         // - Autor
         // » Update_Mail_Body_Autor_HashTags()
         // - Triage-Info
         // » Update_Mail_Body_HashTags()
         return;
      }

      //» Die Regeln für die Team-Mailbox (ist keine Archiv-Mailbox) 
      if (forceProcessingAsTeamMailbox || !_boMail.ImapSrvRuntimeConfig!.IsArchiveMailbox) {
         return;
      }
   }

   #endregion BO Methoden
   
   #region Hilfsfunktionen

   /// <summary>
   /// Setzt im Mail Body den Autor-Hashtag
   /// </summary>
   public void Update_Mail_Body_Autor_HashTags(bool saveOnDirty = false, bool force = false) {

      //+ Den Mail Body ergänzen
      if (_boMail.Is_MsgBody_Html()) {
         // ToDo Now 🟥 Autor_HashTags
         AddOrUpdate_MailMsg_HtmlBody_Autor_HashTags();
      }
      else {
         // ToDo Now 🟥 Autor_HashTags
         AddOrUpdate_MailMsg_TextBody_Autor_HashTags();
      }

      if (saveOnDirty && _boMail.IsDirty) { _boMail.Save(); }

   }


   /// <summary>
   /// Setzt / aktualisiert im HTML Body das Autor-Hashtag
   /// </summary>
   private void AddOrUpdate_MailMsg_TextBody_Autor_HashTags() {
      // ToDo Now 🟥 Autor_HashTags
      return;

      //+ Die HashTags berechnen
      var triageFolderHashTags = _boMail.Analyze_TriageHashTags();

      //++ Keine HashTags: Fertig
      if (triageFolderHashTags.Count == 0) { return; }

      //+ Aus den HashTags den Text-Inhalt erzeugen
      StringBuilder sbTriageHashTags = new StringBuilder();
      sbTriageHashTags.AppendLine(ConfigMail_Triage.Triage_HashTag_TextHeader);

      foreach (var triageFolderHashTag in triageFolderHashTags) {
         sbTriageHashTags.AppendLine(triageFolderHashTag);
      }

      var newTriageHashTags = sbTriageHashTags.ToString();

      //+ Den Mail text Body holen
      var (mailText, _) = MimeKit_JigTools.Get_Msg_Bodies(_boMail.Msg!, true);

      //+ Haben wir im Mail Text schon einen solchen Block?
      Match match = BL_Mail_HashTag.ORgxOptions_HashTagBlock_InTextMailBody.Match(mailText);

      if (match.Success) {
         var currentTriagehashTags = match.Groups["TriageHashTags"].Value;

         //++ Sind die alten und neuen HashTags identisch?
         if (currentTriagehashTags.ØCompareStringWithouWhiteChars
                (newTriageHashTags, StringComparison.OrdinalIgnoreCase)) {
            // Nichts zu tun
            return;
         }

         //++ Den alten Block mit dem neuen ersetzen
         mailText = BL_Mail_HashTag.ORgxOptions_HashTagBlock_InTextMailBody.Replace
            (mailText, newTriageHashTags);
      }
      else {
         //++ Den neuen Block ergänzen
         mailText += newTriageHashTags;
      }

      //+ Den Mail Body Textblock aktualisieren
      MimeKit_JigTools.Update_Msg_Bodies(_boMail.Msg!, mailText, null);
      _boMail.IsDirty = true;
   }
   
   
   /// <summary>
   /// Setzt / aktualisiert im HTML Body das Autor-Hashtag
   /// </summary>
   /// <param name="mailFolder"></param>
   /// <param name="msg"></param>
   /// <returns></returns>
   private void AddOrUpdate_MailMsg_HtmlBody_Autor_HashTags() {
      // ToDo Now 🟥 Autor_HashTags

      return;

      //+ Die HashTags berechnen
      var triageFolderHashTags = _boMail.Analyze_TriageHashTags();

      //++ Keine HashTags: Fertig
      if (triageFolderHashTags.Count == 0) { return; }

      //+ Aus den HashTags den HTML-Inhalt erzeugen
      //++ Erzeugen: Das Html-Element mit den HashTags:
      // 
      // <div id="jigDataApp" class="jigID">
      //    <ul class ="jigTagApp" >
      //       <li >#SehrDringend </li >
      //    </ul >
      // </div >

      HtmlDocument htmlTriageHashTagsUlDoc = new HtmlDocument();

      // Aus dem String ein Div-Element erzeugen
      htmlTriageHashTagsUlDoc.LoadHtml(@"<div id=""jigDataApp"" class=""jigID""></div>");

      // Die erzeugte Div-Node laden
      var divNode = htmlTriageHashTagsUlDoc.DocumentNode.SelectSingleNode("//div");

      //++ Der Div-Node die CSS Klasse zuordnen
      // HtmlNode jigDataAppNode = htmlTriageHashTagsUlDoc.CreateElement(@"<div id=""jigDataApp""");
      divNode.AddClass("jigID");

      //++ Die <ul> Liste erzeugen
      HtmlNode ulHashTagNode = htmlTriageHashTagsUlDoc.CreateElement("ul");
      ulHashTagNode.AddClass("jigTagApp");

      //+++ Alle HashTags als <li> ergänzen
      foreach (var triageFolderHashTag in triageFolderHashTags) {
         ulHashTagNode.ChildNodes.Add
            (HtmlNode.CreateNode($"<li>{HtmlDocument.HtmlEncode(triageFolderHashTag)}</li>"));
      }

      //++ Der Div die UL zufügen
      divNode.AppendChild(ulHashTagNode);

      //‼ 🚩 Debug: Ins ClipBoard kopieren
      // ClipboardService.SetText(outerHtml);

      //+ Den HTML Body lesen
      var htmlBody = BL_Mail_Body.Get_MsgHtmlBody_Complete(_boMail.Msg!, true);

      //++ Aus dem HTML Mail ein htmlDoc erzeugen
      var htmlDoc = new HtmlDocument();
      htmlDoc.LoadHtml(htmlBody);

      //+ Im HtmlDoc die Node ergänzen / aktualisieren 
      var (isHtmlUpdated, newHtmlDoc) = HtmlTools.AddOrUpdate_HtmlElementByID
         (htmlDoc
          , "jigDataApp"
          , htmlTriageHashTagsUlDoc.DocumentNode);

      //+ Musste die Node geändert werden?
      if (isHtmlUpdated) {
         var (textPart, htmlPart) = MimeKit_JigTools.Get_Msg_Bodies(_boMail.Msg, true);

         //++ Allenfalls auch den Text ersetzen
         string? newTextBody = null;

         if (textPart.ØHasValue()) {
            // Den anonymisierten HTML Text in normalen Text konvertieren
            newTextBody = BL_Mail_Common.OHtmlToText.Convert(newHtmlDoc!.DocumentNode.OuterHtml);
         }

         // In der E-Mail die anonymisierten Bodies setzen  
         MimeKit_JigTools.Update_Msg_Bodies(_boMail.Msg!, newTextBody, newHtmlDoc!.DocumentNode.OuterHtml);

         _boMail.IsDirty = true;
      }
   }

   /// <summary>
   /// Analysiert den Speicherort der E-Mail und setzt die Triage-Hash-Tags:
   /// - Im Msg Header
   /// - Im Msg Body
   /// </summary>
   public void Update_Mail_Body_HashTags(bool saveOnDirty = false, bool force = false) {

      //+ Den Mail Body ergänzen
      if (_boMail.Is_MsgBody_Html()) {
         //++ Den HTML Body um die Triage-#HashTag Infos ergänzen
         AddOrUpdate_MailMsg_HtmlBody_TriageHashTags();
      }
      else {
         //++ Den Text Body um die Triage-#HashTag Infos ergänzen
         AddOrUpdate_MailMsg_TextBody_TriageHashTags();
      }

      if (saveOnDirty && _boMail.IsDirty) { _boMail.Save(); }

   }
   
   /// <summary>
   /// Setzt / aktualisiert im HTML Body Footer die #HashTags mit den Triage-Infos
   /// </summary>
   /// <param name="mailFolder"></param>
   /// <param name="msg"></param>
   /// <returns></returns>
   private void AddOrUpdate_MailMsg_HtmlBody_TriageHashTags() {
      //+ Die HashTags berechnen
      var triageFolderHashTags = _boMail.Analyze_TriageHashTags();

      //++ Keine HashTags: Fertig
      if (triageFolderHashTags.Count == 0) { return; }

      //+ Aus den HashTags den HTML-Inhalt erzeugen
      //++ Erzeugen: Das Html-Element mit den HashTags:
      // 
      // <div id="jigDataApp" class="jigID">
      //    <ul class ="jigTagApp" >
      //       <li >#SehrDringend </li >
      //    </ul >
      // </div >

      HtmlDocument htmlTriageHashTagsUlDoc = new HtmlDocument();

      // Aus dem String ein Div-Element erzeugen
      htmlTriageHashTagsUlDoc.LoadHtml(@"<div id=""jigDataApp"" class=""jigID""></div>");

      // Die erzeugte Div-Node laden
      var divNode = htmlTriageHashTagsUlDoc.DocumentNode.SelectSingleNode("//div");

      //++ Der Div-Node die CSS Klasse zuordnen
      // HtmlNode jigDataAppNode = htmlTriageHashTagsUlDoc.CreateElement(@"<div id=""jigDataApp""");
      divNode.AddClass("jigID");

      //++ Die <ul> Liste erzeugen
      HtmlNode ulHashTagNode = htmlTriageHashTagsUlDoc.CreateElement("ul");
      ulHashTagNode.AddClass("jigTagApp");

      //+++ Alle HashTags als <li> ergänzen
      foreach (var triageFolderHashTag in triageFolderHashTags) {
         ulHashTagNode.ChildNodes.Add
            (HtmlNode.CreateNode($"<li>{HtmlDocument.HtmlEncode(triageFolderHashTag)}</li>"));
      }

      //++ Der Div die UL zufügen
      divNode.AppendChild(ulHashTagNode);

      //‼ 🚩 Debug: Ins ClipBoard kopieren
      // ClipboardService.SetText(outerHtml);

      //+ Den HTML Body lesen
      var htmlBody = BL_Mail_Body.Get_MsgHtmlBody_Complete(_boMail.Msg!, true);

      //++ Aus dem HTML Mail ein htmlDoc erzeugen
      var htmlDoc = new HtmlDocument();
      htmlDoc.LoadHtml(htmlBody);

      //+ Im HtmlDoc die Node ergänzen / aktualisieren 
      var (isHtmlUpdated, newHtmlDoc) = HtmlTools.AddOrUpdate_HtmlElementByID
         (htmlDoc
          , "jigDataApp"
          , htmlTriageHashTagsUlDoc.DocumentNode);

      //+ Musste die Node geändert werden?
      if (isHtmlUpdated) {
         var (textPart, htmlPart) = MimeKit_JigTools.Get_Msg_Bodies(_boMail.Msg, true);

         //++ Allenfalls auch den Text ersetzen
         string? newTextBody = null;

         if (textPart.ØHasValue()) {
            // Den anonymisierten HTML Text in normalen Text konvertieren
            newTextBody = BL_Mail_Common.OHtmlToText.Convert(newHtmlDoc!.DocumentNode.OuterHtml);
         }

         // In der E-Mail die anonymisierten Bodies setzen  
         MimeKit_JigTools.Update_Msg_Bodies(_boMail.Msg!, newTextBody, newHtmlDoc!.DocumentNode.OuterHtml);

         _boMail.IsDirty = true;
      }
   }


   /// <summary>
   /// Setzt / aktualisiert im Text Body die HashTags
   /// </summary>
   private void AddOrUpdate_MailMsg_TextBody_TriageHashTags() {
      //+ Die HashTags berechnen
      var triageFolderHashTags = _boMail.Analyze_TriageHashTags();

      //++ Keine HashTags: Fertig
      if (triageFolderHashTags.Count == 0) { return; }

      //+ Aus den HashTags den Text-Inhalt erzeugen
      StringBuilder sbTriageHashTags = new StringBuilder();
      sbTriageHashTags.AppendLine(ConfigMail_Triage.Triage_HashTag_TextHeader);

      foreach (var triageFolderHashTag in triageFolderHashTags) {
         sbTriageHashTags.AppendLine(triageFolderHashTag);
      }

      var newTriageHashTags = sbTriageHashTags.ToString();

      //+ Den Mail text Body holen
      var (mailText, _) = MimeKit_JigTools.Get_Msg_Bodies(_boMail.Msg!, true);

      //+ Haben wir im Mail Text schon einen solchen Block?
      Match match = BL_Mail_HashTag.ORgxOptions_HashTagBlock_InTextMailBody.Match(mailText);

      if (match.Success) {
         var currentTriagehashTags = match.Groups["TriageHashTags"].Value;

         //++ Sind die alten und neuen HashTags identisch?
         if (currentTriagehashTags.ØCompareStringWithouWhiteChars
                (newTriageHashTags, StringComparison.OrdinalIgnoreCase)) {
            // Nichts zu tun
            return;
         }

         //++ Den alten Block mit dem neuen ersetzen
         mailText = BL_Mail_HashTag.ORgxOptions_HashTagBlock_InTextMailBody.Replace
            (mailText, newTriageHashTags);
      }
      else {
         //++ Den neuen Block ergänzen
         mailText += newTriageHashTags;
      }

      //+ Den Mail Body Textblock aktualisieren
      MimeKit_JigTools.Update_Msg_Bodies(_boMail.Msg!, mailText, null);
      _boMail.IsDirty = true;
   }

   
   /// <summary>
   /// Aktualisiert oder setzt den Html\Head\Style
   /// </summary>
   /// <param name="mailFolder"></param>
   /// <param name="msg"></param>
   /// <returns></returns>
   private void AddOrUpdate_MailMsgBody_Style() {

      //++ Ohne HTML sind wir fertig
      if (!_boMail.Is_MsgBody_Html()) { return; }

      //+ HTML extrahieren
      var (msgTextPart, msgHtmlPart) = MimeKit_JigTools.Get_Msg_Bodies(_boMail.Msg!, true);

      // 🚩 Debug: Ins ClipBoard kopieren
      // ClipboardService.SetText(htmlBody);

      //+ Haben wir einen HTML-Body?
      if (msgHtmlPart.ØHasValue()) {
         //++ Der Body als HTMLDoc
         var htmlDoc = new HtmlDocument();
         htmlDoc.LoadHtml(msgHtmlPart);

         //+ Html\head\Stylye aktualisieren
         var (htmlChanged, newHtmlDoc) = HtmlTools.AddOrUpdate_HtmlHeadStyle
            (htmlDoc
             , ConfigMail_MsgHtmlBody.MailHtmlHeaderStyleCss
             , ".jigID");

         //+ Wenn der Inhalt änderte, die E-Mail aktualisieren
         if (htmlChanged) {
            // ToDo 🟥 Debug test

            //++ Allenfalls auch den Text ersetzen
            string? newTextBody = null;

            if (msgTextPart.ØHasValue()) {
               // Den anonymisierten HTML Text in normalen Text konvertieren
               newTextBody = BL_Mail_Common.OHtmlToText.Convert(newHtmlDoc.DocumentNode.OuterHtml);
            }

            // In der E-Mail die anonymisierten Bodies setzen  
            MimeKit_JigTools.Update_Msg_Bodies(_boMail.Msg!, newTextBody, newHtmlDoc.DocumentNode.OuterHtml);

            _boMail.IsDirty = true;
         }
      }
   }
   
   #endregion Hilfsfunktionen
   
   

}
