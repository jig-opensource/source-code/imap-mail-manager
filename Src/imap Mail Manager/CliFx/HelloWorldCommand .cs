using System.Diagnostics;
using System.Threading.Tasks;
using CliFx;
using CliFx.Attributes;
using CliFx.Infrastructure;


namespace ImapMailManager.CliFx;

[Command]
public class HelloWorldCommand : ICommand {

   public ValueTask ExecuteAsync(IConsole console) {
      Debug.WriteLine("Debug.WriteLine(): Message from HelloWorldCommand");
      console.Output.WriteLine("console.Output.WriteLine(): Message from HelloWorldCommand");
      Trace.WriteLine("Trace.WriteLine(): Message from HelloWorldCommand");
      // Return default task if the command is not asynchronous
      return default;
   }

}
