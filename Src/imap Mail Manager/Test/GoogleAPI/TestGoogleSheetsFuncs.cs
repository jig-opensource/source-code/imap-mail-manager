using System.Collections.Generic;
using GoogleSheetsWrapper;
using ImapMailManager.GoogleAPI;
using ImapMailManager.GoogleAPI.TomTools;


namespace ImapMailManager.Test.GoogleAPI;

/// <summary>
/// Test-Funktionen fürs GSheets API
/// </summary>
internal class TestGoogleSheetsWrapper {

   /// <summary>
   /// Setzt die Regionale Sprache
   /// </summary>
   public static void TesterSetSheetsLocale() {
      GoogleSheetApi.SetSpreadSheetsLocale(GoogleSheetMbxStatistics.SpreadsheetId);
   }

   #region Public Methods and Operators

   /// <summary>
   /// Testet den GoogleSheetsWrapper
   /// </summary>
   public static void Tester() {
      // Create a SheetHelper class
      var sheetHelper = new SheetHelper(
         GoogleSheetMbxStatistics.SpreadsheetId,
         "rluk-srvc@rluk-372400.iam.gserviceaccount.com",
         "Tester");

      sheetHelper.Init(GoogleSheetApiCfg.GetGoogleCredentialJson());

      // sheetHelper.TabName = "Tester";

      // Get the total row count for the existing sheet
      var sheetRange = new SheetRange("Tester", 1, 1, 1);
      var rows       = sheetHelper.GetRows(sheetRange);
      
      var rows2       = sheetHelper.GetRows(sheetRange);
      
      var rows3       = sheetHelper.TabName;
      var rows4       = sheetHelper.Scopes;
      var rows5       = sheetHelper.GetAllTabNames();
      // var rows6       = sheetHelper.DeleteRow(1);

      // Create the SheetAppender class
      var appender = new SheetAppender(sheetHelper);

      // Delete all of the rows
      if (rows != null) {
         var res = sheetHelper.DeleteRows(1, rows.Count);
      }

      List<List<string>> data = new List<List<string>> {
         new List<string>(){"a", "b", "c"},
         new List<string>(){1.ToString(), 11.ToString(), 111.ToString()},
         new List<string>(){2.ToString(), 22.ToString(), 222.ToString()}
      };
      
      appender.AppendRows(data);
      // appender.AppendCsv();
      
      int xxxx = 1;
   }

   #endregion

}
