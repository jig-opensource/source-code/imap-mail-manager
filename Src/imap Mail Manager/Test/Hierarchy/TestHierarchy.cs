using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Hierarchy;


namespace ImapMailManager.Test.Hierarchy;

static internal class TestHierarchy {

   public static void Test() {
      List<Person> flatList = new() {
         new() { Id   = 1, ParentId  = 0, Name = "CEO" }, new() { Id                    = 2, ParentId  = 1, Name = "North America Operaror" }
         , new() { Id = 3, ParentId  = 1, Name = "South America Operator" }, new() { Id = 12, ParentId = 3, Name = "Brazil Operator" }
         , new() { Id = 10, ParentId = 1, Name = "Europe Operator" }, new() { Id        = 11, ParentId = 1, Name = "Africa Operator" },
      };

      List<HierarchyNode<Person>>? hierarchyList = flatList.OrderBy(f => f.Id).ToHierarchy(t => t.Id, t => t.ParentId);
      Debug.WriteLine("We convert the flat list to a hierarchy");
      Debug.WriteLine(hierarchyList.PrintTree());

      // Person 1: CEO um ein Element ergänzen
      var Test = hierarchyList[0];

      Person TestPerson = new() { Id                       = 4, ParentId = 0, Name = "CFO" };
      var    TestNode1  = new HierarchyNode<Person> { Data = TestPerson };
      Test.AddChild(TestPerson);
      Debug.WriteLine("Ausgabe 2:");
      Debug.WriteLine(hierarchyList.PrintTree());

      // Root node um ein Element ergänzen
      hierarchyList.Add(TestNode1);
      Debug.WriteLine("Ausgabe 3:");
      Debug.WriteLine(hierarchyList.PrintTree());
   }

}