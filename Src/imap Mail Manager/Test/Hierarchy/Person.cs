namespace ImapMailManager.Test.Hierarchy;

public class Person {

   #region Public Properties

   public int    Id       { get; set; }
   public string Name     { get; set; } = "";
   public int    ParentId { get; set; }

   #endregion


   #region Public Methods and Operators

   public override string ToString() => $"Person: {Id} '{Name}'";

   #endregion

}
