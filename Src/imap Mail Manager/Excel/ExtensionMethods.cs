﻿using System;
using System.IO;
using jig.Tools.Exception;
using NPOI.HSSF.UserModel;


namespace ImapMailManager.Excel;

public static class ExtensionMethods {

   #region Public Methods and Operators

   /// <summary>
   /// Speichert ein HSSFWorkbook vom NPOI Office Framework
   /// </summary>
   /// <param name="workbook"></param>
   /// <param name="zielDateiName"></param>
   /// <exception cref="RuntimeException"></exception>
   public static void ØSave(this HSSFWorkbook? workbook, string zielDateiName) {
      if (workbook == null) { return; }

      // Sicherstellen, dass wir mit dem NPOI nicht plötzlich xlsx files schreiben,
      // dann würde Excel Fehler beim laden anzeigen
      var ext = Path.GetExtension(zielDateiName);

      if (ext.ToLower().Equals(".xlsx".ToLower(), StringComparison.OrdinalIgnoreCase)) {
         throw new RuntimeException(".xls als Dateiname verwenden, .xlsx wird nicht unterstützt");
      }

      // Datei speichern
      // !Q https://stackoverflow.com/a/12000257/4795779

      FileStream? fs = null;

      try {
         fs = new FileStream(
            zielDateiName,
            FileMode.OpenOrCreate,
            FileAccess.ReadWrite,
            FileShare.None);
         workbook.Write(fs);
         fs.Close();
      } finally {
         if (fs != null)
            fs.Dispose();
      }
   }

   #endregion

}
