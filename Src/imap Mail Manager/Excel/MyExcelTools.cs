﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hierarchy;
using ImapMailManager.Mail;
using ImapMailManager.Mail.Hierarchy;
using ImapMailManager.Mail.Stats;
using jig.Tools;
using jig.Tools.String;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;


namespace ImapMailManager.Excel;

internal class MyExcelTools {

   #region Static Fields

   private static IFont? FontBold; 
   
   private static readonly IDictionary<string, Tuple<ICellStyle, IDataFormat>> ZellenFormate
      = new Dictionary<string, Tuple<ICellStyle, IDataFormat>>();

   #endregion


   #region Public Methods and Operators

   public static void ExportMailboxItemsToExcel(HierarchyNode<ImapHrchyNde>? rootNode, string? exportToExcelFilename) {
      var zielFile   = @"c:\Temp\tester.xls";
      var colWidth   = 15000;
      var maxSpalten = -1;

      if (exportToExcelFilename.ØHasValue()) {
         zielFile = exportToExcelFilename;
      }
      
      //+ Excel einrichten
      var workbook = InitializeWorkbook();
      var sheet1   = workbook.CreateSheet("IMAP-Inhalt");

      var zeileNo = 0;

      //+ Titel anzeigen
      sheet1.CreateRow(zeileNo++).CreateCell(0).SetCellValue("Mailbox-Inhalt");

      //+ Spalten initialisieren
      for (var iSpalte = 0; iSpalte < 20; iSpalte++) { sheet1.SetColumnWidth(iSpalte, colWidth); }

      //+ Header anzeigen
      zeileNo++;
      var row      = sheet1.CreateRow(zeileNo++);
      var spalteNo = 0;
      row.CreateCell(spalteNo++).SetCellValue("Ordner");
      row.CreateCell(spalteNo++).SetCellValue("Erhalten");

      //+ Sender vs From
      // Sender: "Thomas Schittli" <outlook_5631ED86E3A3F8EF@outlook.com>
      // From  : "Thomas Schittli" <Thomas.Schittli@jig.ch>
      // row.CreateCell(spalteNo++).SetCellValue("Sender");

      row.CreateCell(spalteNo++).SetCellValue("From");
      row.CreateCell(spalteNo++).SetCellValue("FromAnz");

      // row.CreateCell(spalteNo++).SetCellValue("==");

      row.CreateCell(spalteNo++).SetCellValue("To");
      row.CreateCell(spalteNo++).SetCellValue("ToAnz");
      row.CreateCell(spalteNo++).SetCellValue("SubjectNormalized");
      row.CreateCell(spalteNo++).SetCellValue("Subject");

      //+ Daten speichern
      var thisFolder = "";
      foreach (var item in rootNode.DescendantNodes(TraversalType.DepthFirst)) {
         if (item.Data.ImapPersonalNamespaceNde != null) {}

         if (item.Data.ImapFolderNde != null) {
            //++ Ein Mailbox Ordner
            var oImapFolderNde = item.Data.ImapFolderNde;
            thisFolder = oImapFolderNde.Path.Replace('/', '\\').TrimEnd('\\');
         }

         if (item.Data.ImapMsgNde != null) {
            //++ Wir haben eine E-Mail Node
            var msg = item.Data.ImapMsgNde;

            //++Neue Zeile erzeugen
            row = sheet1.CreateRow(zeileNo++);

            // Die Anz. bisheriger Spalten merken
            maxSpalten = Math.Max(maxSpalten, spalteNo);
            spalteNo   = 0;

            row.CreateCell(spalteNo++).SetCellValue(thisFolder);

            // Datum, wie es auch Roundcube amzeigt
            // Variante wäre: utcDateTime
            var empfangsDatum = row.CreateCell(spalteNo++);
            SetCellDateTime(workbook, empfangsDatum, msg.ADate.GetValueOrDefault().LocalDateTime);

            //++ Sender vs From
            // Sender: "Thomas Schittli" <outlook_5631ED86E3A3F8EF@outlook.com>
            // From  : "Thomas Schittli" <Thomas.Schittli@jig.ch>
            // row.CreateCell(spalteNo++).SetCellValue(msg.Sender.ToString());

            row.CreateCell(spalteNo++).SetCellValue(msg.From.ToString());
            row.CreateCell(spalteNo++).SetCellValue(msg.From.Count);

            // row.CreateCell(spalteNo++).SetCellValue(msg.Sender.ToString().ToLower().Equals(msg.From.ToString().ToLower(), StringComparison.OrdinalIgnoreCase));

            row.CreateCell(spalteNo++).SetCellValue(msg.To.ToString());
            row.CreateCell(spalteNo++).SetCellValue(msg.To.Count);
            row.CreateCell(spalteNo++).SetCellValue(msg.SubjectNormalized);
            row.CreateCell(spalteNo++).SetCellValue(msg.Subject);
         }
      }

      //+ Die Spaltenbreiten anpassen
      for (var iSpalte = 0; iSpalte < maxSpalten; iSpalte++) {
         try { sheet1.AutoSizeColumn(iSpalte); } catch (InvalidCastException e) {
            // Ignorieren
         }
      }

      sheet1.SetColumnWidth(1, sheet1.GetColumnWidth(1) + 50);

      //+ Datei speichern
      workbook.ØSave(zielFile);
   }


   /// <summary>
   /// </summary>
   /// <param name="mailboxStatisticsProKws"></param>
   /// <param name="b"></param>
   /// <param name="mailboxHierarchy"></param>
   public static void ExportMailboxStatisticsProKW(
      List<MailboxStatisticsProKW> mailboxStatisticsProKws,
      bool                                                     showDbgDetails) {
      var zielFile   = @"c:\Temp\MailboxStatisticsProKW.xls";
      var colWidth   = 15000;
      var maxSpalten = -1;

      var workbook = InitializeWorkbook();
      var sheet1   = workbook.CreateSheet("IMAP-Stats");

      var zeileNo = 0;

      /// Titel anzeigen
      sheet1.CreateRow(zeileNo++).CreateCell(0).SetCellValue("Mailbox-Statistik");

      /// Spalten initialisieren
      for (var iSpalte = 0; iSpalte < 20; iSpalte++) { sheet1.SetColumnWidth(iSpalte, colWidth); }

      /// Header anzeigen
      zeileNo++;
      var row      = sheet1.CreateRow(zeileNo++);
      var spalteNo = 0;
      row.CreateCell(spalteNo++).SetCellValue("Datum");
      row.CreateCell(spalteNo++).SetCellValue(showDbgDetails ? "AnzAntwortenInKW / Kat" : "AnzAntwortenInKW");
      row.CreateCell(spalteNo++).SetCellValue("AnzNeueFragenInKW");
      row.CreateCell(spalteNo++).SetCellValue("AnzOffeneFragen");

      /// Daten im Excel abfüllen
      foreach (var stats in mailboxStatisticsProKws) {
         // Wenn das Datum im Jahr 1 ist,
         // dan ist die E-Mail ist erst im Entwurf
         // Diese E-Mails erscheinen nicht in der Statistik
         if (showDbgDetails == false
             && stats.EndDatumDerKW.Year < 1900) { continue; }

         /// Neue Zeile erzeugen
         row      = sheet1.CreateRow(zeileNo++);
         spalteNo = 0;

         // Das Datum
         var statDate = row.CreateCell(spalteNo++);
         SetCellDateTime(workbook, statDate, stats.EndDatumDerKW, "dd.MM.yy");

         row.CreateCell(spalteNo++).SetCellValue(stats.AnzAntwortenInKW);
         row.CreateCell(spalteNo++).SetCellValue(stats.AnzNeueFragenInKW);
         row.CreateCell(spalteNo++).SetCellValue(stats.AnzOffeneFragenBisKW);
         row.CreateCell(spalteNo++).SetCellValue(stats.AnzAntwortenInArbeitInKW);

         if (showDbgDetails) {
            foreach (var neueFrage in stats.ListeNeueFragen) {
               row      = sheet1.CreateRow(zeileNo++);
               spalteNo = 1;
               row.CreateCell(spalteNo++).SetCellValue("Neue Frage");
               row.CreateCell(spalteNo++).SetCellValue(neueFrage.OImapFolder.FullName);

               var frageDate = row.CreateCell(spalteNo++);
               SetCellDateTime(workbook, frageDate, neueFrage.ADate.GetValueOrDefault().LocalDateTime);

               row.CreateCell(spalteNo++).SetCellValue(neueFrage.SubjectNormalized);
            }

            foreach (var neueAntwort in stats.ListeAntworten) {
               row      = sheet1.CreateRow(zeileNo++);
               spalteNo = 1;
               row.CreateCell(spalteNo++).SetCellValue("Neue Antwort");
               row.CreateCell(spalteNo++).SetCellValue(neueAntwort.OImapFolder.FullName);

               var frageDate = row.CreateCell(spalteNo++);
               SetCellDateTime(workbook, frageDate, neueAntwort.ADate.GetValueOrDefault().LocalDateTime);

               row.CreateCell(spalteNo++).SetCellValue(neueAntwort.SubjectNormalized);
            }
         }
      }

      /// Die Spaltenbreiten anpassen
      for (var iSpalte = 0; iSpalte < 20; iSpalte++) {
         try { sheet1.AutoSizeColumn(iSpalte); } catch (InvalidCastException e) {
            // Ignorieren
         }
      }

      // sheet1.SetColumnWidth(1, sheet1.GetColumnWidth(1) + 50);

      /// Datei speichern
      workbook.ØSave(zielFile);
   }

   #endregion


   #region Internal Methods

   /// <summary>
   /// Initialisiert ein Excel Objekt
   /// </summary>
   /// <returns></returns>
   public static HSSFWorkbook InitializeWorkbook() {
      var hssfworkbook = new HSSFWorkbook();

      //Create a entry of DocumentSummaryInformation
      var dsi = PropertySetFactory.CreateDocumentSummaryInformation();
      dsi.Company                             = "Thomas Schittli";
      hssfworkbook.DocumentSummaryInformation = dsi;

      //Create a entry of SummaryInformation
      var si = PropertySetFactory.CreateSummaryInformation();
      si.Subject                      = "IMAP";
      hssfworkbook.SummaryInformation = si;

      return hssfworkbook;
   }


   /// <summary>
   /// Setzt für die Excel Zelle ein DateTime und definiert das Anzeigeformat
   /// </summary>
   /// <param name="workbook"></param>
   /// <param name="cell"></param>
   /// <param name="dt"></param>
   /// <param name="formatStr"></param>
   public static void SetCellDateTime(HSSFWorkbook workbook, ICell cell, DateTime dt, string formatStr = "dd.MM.yyyy HH:mm") {
      // Den Wert setzen
      cell.SetCellValue(dt);

      //+ Das Zellenformat setzen
      if (ZellenFormate.ContainsKey(formatStr)) {
         // Bestehendes Format nützen
         cell.CellStyle = ZellenFormate[formatStr].Item1;
      }
      else {
         // Neues Format erzeugen
         var dtCellStyle  = workbook.CreateCellStyle();
         var dtCellFormat = workbook.CreateDataFormat();
         dtCellStyle.DataFormat = dtCellFormat.GetFormat(formatStr);
         ZellenFormate.Add(formatStr, new Tuple<ICellStyle, IDataFormat>(dtCellStyle, dtCellFormat));
         cell.CellStyle = dtCellStyle;
      }
   }


   /// <summary>
   /// Macht eine Zelle fett
   /// </summary>
   /// <param name="workbook"></param>
   /// <param name="cell"></param>
   public static void SetCellBold(
      HSSFWorkbook workbook
      , ICell      cell
      ) {

      //+ Config
      string cellformatBoldkey = "BoldFont";

      // Die fette Schrift
      if (FontBold == null) {
         FontBold            = workbook.CreateFont();
         FontBold.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
      }

      // Der CellStyle
      if (ZellenFormate.ContainsKey(cellformatBoldkey)) {
         // Bestehendes Format nützen
         cell.CellStyle = ZellenFormate[cellformatBoldkey].Item1;
      }
      else {
         // Neues Format erzeugen
         var boldCellStyle  = workbook.CreateCellStyle();
         boldCellStyle.SetFont(FontBold);
         ZellenFormate.Add(cellformatBoldkey, new Tuple<ICellStyle, IDataFormat>(boldCellStyle, null));
         cell.CellStyle = boldCellStyle;
      }
   }

   #endregion

}
