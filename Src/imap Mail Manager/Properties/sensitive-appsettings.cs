using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ImapMailManager.Properties;

// ToDo 🟥 Allenfalls Config umstellen auf:
// https://github.com/RickStrahl/Westwind.ApplicationConfiguration
// ‼ » Strongly typed, code-first configuration classes for .NET applications


/// <summary>
/// Speichert die Sensitiven Config-Daten
/// Nützt den Json ConfigurationProvider
///   !M https://learn.microsoft.com/de-de/dotnet/core/extensions/configuration-providers
///   !M https://learn.microsoft.com/de-de/dotnet/api/microsoft.extensions.configuration.json.jsonconfigurationprovider?view=dotnet-plat-ext-7.0
///
/// Das Config-File:
/// Properties\sensitive-appsettings.json
/// 
/// </summary>
public class Sensitive_AppSettings {

   public CfgImapServer? ImapSrcCfg { get; private set; }

   /// <summary>
   /// Der API Key für den REST-Service
   /// </summary>
   public static string Cfg_Libs_DetectLanguage_ApiKey { get; private set; }

   /// <summary>
   /// Das Password, mit dem die persönlichen Daten in jeder Mail im Msg Header verschlüsselt wird 
   /// </summary>
   public static string Cfg_RecFragesteller_EncryptRecord_PW { get; private set; }

   private IConfiguration Config { get; }


   /// <summary>
   /// Konstruktor: Einlesen:
   /// Properties\sensitive-appsettings.json 
   /// </summary>
   /// <param name="sensitiveAppSettings_Config"></param>
   public Sensitive_AppSettings(IConfigurationRoot sensitiveAppSettings_Config) {
      Config = sensitiveAppSettings_Config;

      //+ Die Libs:DetectLanguage Konfig
      // !Ex
      //    string connectionString = config["ConnectionString:SqlServer"];
      //    string connectionString = config.GetSection("ConnectionString").GetSection("SqlServer").Value;

      var cfg_Libs_DetectLanguage = Config.GetSection("Libs:DetectLanguage");
      Cfg_Libs_DetectLanguage_ApiKey = cfg_Libs_DetectLanguage["ApiKey"];

      //+ Die Sensitive-AppConfig
      var cfg_Sensitive_AppConfig = Config.GetSection("Sensitive-AppConfig");
      Cfg_RecFragesteller_EncryptRecord_PW = cfg_Sensitive_AppConfig["RecFragesteller_EncryptRecord_PW"];

      //+ Die IMAP Sektion holen
      IConfigurationSection cfgImap = Config.GetSection("IMAP-Server");

      //+ Die IMAP Config laden
      foreach (var child in cfgImap.GetChildren()) {
         var imapSrvCfgRoot = Config.GetSection(child.Path);
         var imapSrvCfg     = imapSrvCfgRoot.GetSection("ServerCfg");

         //++ Die Server Config laden
         var srvConfigName = imapSrvCfg["SrvConfigName"];
         var srvHostname   = imapSrvCfg["Hostname"];
         var srvPort       = imapSrvCfg["Port"];
         var srvUseSsl     = imapSrvCfg["UseSSL"];

         //+++ Und instanziieren
         ImapSrcCfg = new CfgImapServer
            (srvConfigName ?? string.Empty
             , srvHostname ?? string.Empty
             , Convert.ToInt32(srvPort)
             , Convert.ToBoolean(srvUseSsl)
             , new List<CfgImapServerCred>());

         //++ Die Credentials laden
         var credentialsSection = imapSrvCfgRoot.GetSection("Credentials");

         foreach (var credential in credentialsSection.GetChildren()) {
            //+++ Die Credentials einlesen
            var credName              = credential.Key;
            var credDetails           = Config.GetSection(credential.Path);
            var userName              = credDetails["UserName"];
            var password              = credDetails["Password"];
            var isProductiveMailbox   = credDetails["IsProductiveMailbox"];
            var isTestMailbox         = credDetails["IsTestMailbox"];
            var isMailboxTypeFrontend = credDetails["IsMailboxTypeFrontend"];
            var isMailboxTypeArchive  = credDetails["IsMailboxTypeArchive"];

            //+++ Die Credentials instanziieren
            CfgImapServerCred thisCred = new CfgImapServerCred
               (credName
                , ImapSrcCfg
                , userName ?? string.Empty
                , password ?? string.Empty
                , Convert.ToBoolean(isProductiveMailbox)
                , Convert.ToBoolean(isTestMailbox)
                , Convert.ToBoolean(isMailboxTypeFrontend)
                , Convert.ToBoolean(isMailboxTypeArchive));

            //+++ Die Creds speichern
            ImapSrcCfg.Creds.Add(thisCred);
         }
      }

   }


   /// <summary>
   /// Konfiguriert einen IMAP Server
   /// </summary>
   /// <param name="Hostname"></param>
   /// <param name="Port"></param>
   /// <param name="UseSsl"></param>
   public record CfgImapServer(
      string                    SrvConfigName
      , string                  Hostname
      , int                     Port
      , bool                    UseSsl
      , List<CfgImapServerCred> Creds) {}


   /// <summary>
   /// Konfiguriert die Zugangsdaten für einen IMAP-Server
   /// </summary>
   /// <param name="UserName"></param>
   /// <param name="Pw"></param>
   /// <param name="MailboxIsProductive"></param>
   public record CfgImapServerCred {

      /// <summary>Initializes a new instance of the <see cref="T:System.Object" /> class.</summary>
      public CfgImapServerCred(
         string          credKey
         , CfgImapServer cfgImapServer
         , string        userName
         , string        pw
         , bool          isProductiveMailbox
         , bool          isTestMailbox
         , bool          isMailboxTypeFrontend
         , bool          isMailboxTypeArchive) {
         CredKey  = credKey;
         UserName = userName;
         Pw       = pw;

         //+ Die Mailbox kann nicht Prod und Test gleichzeitig sein
         Assert.IsTrue(isProductiveMailbox != isTestMailbox);
         IsProductiveMailbox = isProductiveMailbox;
         IsTestMailbox       = isTestMailbox;

         //+ Die Mailbox kann nicht Frontend und Archiv gleichzeitig sein
         Assert.IsTrue(isMailboxTypeFrontend != isMailboxTypeArchive);
         IsMailboxTypeFrontend = isMailboxTypeFrontend;
         IsMailboxTypeArchive  = isMailboxTypeArchive;
      }


      /// <summary>
      /// In dieser Mailbox arbeiten wir, d.h. bearbeiten E-Mails
      /// </summary>
      public bool IsMailboxTypeFrontend { get; set; }
      /// <summary>
      /// In dieser Mailbox haben wir das Archiv
      /// </summary>
      public bool IsMailboxTypeArchive { get; set; }

      /// <summary>
      /// Diese Mailbox ist nur für Tests
      /// </summary>
      public bool IsTestMailbox { get; set; }

      /// <summary>
      /// Diese Mailbox ist produktiv!
      /// </summary>
      public bool IsProductiveMailbox { get; set; }

      public string Pw { get; set; }

      public string UserName { get; set; }

      public string CredKey { get; set; }

   }

}
