using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using jig.Tools;
using jig.Tools.String;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;


namespace ImapMailManager.Properties;

[SuppressMessage("ReSharper", "MissingXmlDoc")]
public record class Cfg_TeamConfig {

   public TeamConfig TeamConfig { get; set; }

}


[SuppressMessage("ReSharper", "MissingXmlDoc")]
public record class TeamConfig {

   public string?            ReplyToAddressPattern { get; set; }
   public List<TeamMitglied> TeamMitglieder        { get; set; }


   /// <summary>
   /// Die Antwort-Adresse berechnen
   /// </summary>
   /// <param name="teamConfig"></param>
   public static void Calculate_AntwortEMailAddress(TeamConfig teamConfig) {
      foreach (var teamMitglied in teamConfig.TeamMitglieder) {
         teamMitglied.AntwortEMailAddress = teamMitglied.ØInterpolate(teamConfig.ReplyToAddressPattern, "TeamMitglied");
      }
   }


   /// <summary>
   /// Gibt alle Antwortadressen aus
   /// </summary>
   /// <param name="teamConfig"></param>
   public static void Print_AntwortEMailAddresses(TeamConfig teamConfig) {
      foreach (var teamMitglied in teamConfig.TeamMitglieder.OrderBy(x => x.Vorname).ThenBy(x => x.Nachname)) {
         Debug.WriteLine(teamMitglied.AntwortEMailAddress);
      }
   }


   /// <summary>
   /// Gibt alle Antwortadressen aus
   /// </summary>
   /// <param name="teamConfig"></param>
   public static void Print_UserDetais(TeamConfig teamConfig) {
      foreach (var teamMitglied in teamConfig.TeamMitglieder.OrderBy(x => x.Vorname).ThenBy(x => x.Nachname)) {
         Debug.WriteLine
            ($"{teamMitglied.Kuerzel} • {teamMitglied.Vorname} {teamMitglied.Nachname}, {teamMitglied.AntwortEMailAddress}");
      }
   }

}


[SuppressMessage("ReSharper", "MissingXmlDoc")]
public record class TeamMitglied {

   #region Props

   /// <summary>
   /// Heisst im Yaml 'Kürzel', aber Fluid kommt nicht mit Umlauten zurecht
   /// </summary>
   [YamlMember(Alias = "Kürzel", ApplyNamingConventions = false)]
   public string? Kuerzel { get;             set; }
   public string? IstAktiv            { get; set; }
   public string? Vorname             { get; set; }
   public string? Nachname            { get; set; }
   public string? AntwortEMailAddress { get; set; }

   #endregion Props

   #region Methoden

   /// <summary>
   /// Sucht ein TeamMitglied aufgrund des Kürzels 
   /// </summary>
   /// <param name="teamMitglieder"></param>
   /// <param name="kürzel"></param>
   /// <returns></returns>
   public static TeamMitglied? Get_TeamMitglied(List<TeamMitglied> teamMitglieder, string kürzel) {
      return teamMitglieder.FirstOrDefault(x => x.Kuerzel.ØEqualsIgnoreCase(kürzel));
   }

   public string GetVornameNachname() {
      return $"{Vorname} {Nachname}";
   }
   
   #endregion Methoden

}


/// <summary>
/// Abbildung der Team-Mitglieder Konfig
/// </summary>
public class Sensitive_Team_Mitglieder {

   /// <summary>
   /// Deserialisiert die Yaml-Datei
   /// </summary>
   public static Cfg_TeamConfig ReadYaml() {

      // Der Klassen-Name, _ mit -  ersetzen, um den Dateinamen zu erhalten
      var className = nameof(Sensitive_Team_Mitglieder).Replace('_', '-');

      // Die yaml-Datei
      var yamlFileName = $"{className}.yaml";

      // Den Pfad zum speicherort dieser Klasse
      var clasFileDir = Path.Join(Directory.GetCurrentDirectory(), "Properties");

      // Die yaml-Datei
      var yamlFile = Path.Join(clasFileDir, yamlFileName);

      // Die yaml-Datei lesen
      var yamlCfg = File.ReadAllText(yamlFile);

      // Die yaml-Datei parsen und unbekannte Objekte in der yaml-Datei ignorieren
      var deserializer = new DeserializerBuilder()
                        .WithNamingConvention(NullNamingConvention.Instance)

                         // .IgnoreUnmatchedProperties()
                        .Build();

      // Das Objekt erzeugen
      var teamConfiguration = deserializer.Deserialize<Cfg_TeamConfig>(yamlCfg);

      //+ Die berechneten Props setzen
      TeamConfig.Calculate_AntwortEMailAddress(teamConfiguration.TeamConfig);

      // TeamConfig.Print_AntwortEMailAddresses(teamConfiguration.TeamConfig);
      // TeamConfig.Print_UserDetais(teamConfiguration.TeamConfig);

      return teamConfiguration;
   }

}
