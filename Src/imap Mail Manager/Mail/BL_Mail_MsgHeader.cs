using System;
using System.Collections.Generic;
using System.Linq;
using ImapMailManager.Config;
using jig.Tools;
using MailKit;
using MimeKit;


namespace ImapMailManager.Mail {

   /// <summary>
   /// Business-Logik E-Mail Message-Header (nicht den HTML Body\Head)
   /// - Setzt / aktualisiert Message Header
   /// </summary>
   public class BL_Mail_MsgHeader {

      /// <summary>
      /// Aktualisiert diese Mail Message Header,
      /// nur, wenn der Inhalt geändert hat:
      /// 
      /// - MailRefNr:     jigMailRefNr
      /// - Triage:        jigTriage
      /// - Hashtags:      jigHashtags
      /// - Fragesteller:  jigQ
      /// </summary>
      public static bool Update_MailMsg_Headers(IMailFolder mailFolder, MimeMessage msg) {
         bool msgChanged = false;

         // ToDo Now 🟥 Übertragen
         
         //+ Die Msg Header aktualisieren und merken, ob die Msg geändert wurde
         AddOrUpdate_MailMsgHdr_RefNr(msg).IfTrue
            (() => msgChanged = true);

         AddOrUpdate_MailMsgHdr_HashTags(msg).IfTrue
            (() => msgChanged = true);

         // eMailHeader.Q
         AddOrUpdate_MailMsgHdr_Fragesteller(msg).IfTrue
            (() => msgChanged = true);

         AddOrUpdate_MailMsgHdr_TriageOrdner(mailFolder, msg).IfTrue
            (() => msgChanged = true);

         return msgChanged;
      }


      /// <summary>
      /// Speichert in Msg Header den Ort, wo diese E-Mail im Archiv abgelegt werden wird
      /// </summary>
      /// <param name="mailFolder"></param>
      /// <param name="msg"></param>
      /// <returns></returns>
      [Obsolete("Diese Funktion ist obsolete » obj BO_Mail", false)]
      private static bool AddOrUpdate_MailMsgHdr_TriageOrdner(
         IMailFolder   mailFolder
         , MimeMessage msg) {
         //+ Den Archiv-Ordner berechnen
         var archivOrdner = BL_Mail_Common.Calc_MbxOrdner_ArchivPath(mailFolder);

         if (archivOrdner == null) {
            //+ E-Mails dieses Ordners werden im Archiv gespeichert 
            // Nichts verändert
            return false;
         }

         //+ Den Header setzen / aktualisieren
         return AddOrUpdate_MsgHeader
            (msg
             , ConfigMail_MsgHeader.AppMailHeaderType.TriageOrdner
             , archivOrdner);
      }


      /// <summary>
      /// Liest aus der E-Mail die Koordinaten des Fragestellers,
      /// verschlüsselt sie und speichert sie im Msg Header
      /// </summary>
      /// <param name="msg"></param>
      /// <returns></returns>
      [Obsolete("Diese Funktion ist obsolete » obj BO_Mail", false)]
      private static bool AddOrUpdate_MailMsgHdr_Fragesteller(MimeMessage msg) {
         //+ Versuchen, den Fragesteller zu identifizieren
         var fragesteller = BL_Mail_Body.TryGet_Msg_FragestellerDaten(msg);

         //++ Gefunden?
         if (fragesteller != null) {
            //+ Den Record verschlüsseln
            var encryptedRecord = fragesteller.EncryptRecord(ConfigMail_MsgHeader.EncryptionPW);

            //+ Den Header setzen / aktualisieren
            return AddOrUpdate_MsgHeader
               (msg
                , ConfigMail_MsgHeader.AppMailHeaderType.FragestellerPersoenlicheDaten
                , encryptedRecord);
         }

         // Nichts verändert
         return false;
      }


      /// <summary>
      /// Sucht in der Message nach allen HashTags, sammelt sie und aktualisiert den Msg Hdr 
      /// </summary>
      /// <param name="msg"></param>
      /// <returns></returns>
      /// <exception cref="NotImplementedException"></exception>
      [Obsolete("Diese Funktion ist obsolete » obj BO_Mail", false)]
      private static bool AddOrUpdate_MailMsgHdr_HashTags(MimeMessage msg) {
         //+ Die HashTags lesen
         var (hashtags_InHeader, hashtags_InSubject, hashtags_InBody)
            = BL_Mail_Body.FindMail_Hashtags
               (msg);

         //+ Alle HashTags zusammenfassen
         var allHashtags = hashtags_InHeader.Concat(hashtags_InSubject).Concat(hashtags_InBody);

         //+ Vergleichen, die HashTags geändert haben
         var hashTagsHaveNotBeenChanged = new HashSet<string>(hashtags_InHeader).SetEquals
            (allHashtags);

         //++ Die HashTags wurden nicht verändert
         if (hashTagsHaveNotBeenChanged) {
            // Msg nicht verändert
            return false;
         }

         //++ Die HashTags wurden verändert
         var joinedAllHashtags = String.Join("|", allHashtags);

         //+ Den Header setzen / aktualisieren
         return AddOrUpdate_MsgHeader
            (msg
             , ConfigMail_MsgHeader.AppMailHeaderType.TriageHashtags
             , joinedAllHashtags);
      }


      /// <summary>
      /// Ergänzt / aktualisiert im Mail Header die Mail RefNr
      /// </summary>
      /// <param name="msg"></param>
      /// <returns></returns>
      [Obsolete("Diese Funktion ist obsolete » obj BO_Mail", false)]
      private static bool AddOrUpdate_MailMsgHdr_RefNr(MimeMessage msg) {
         //+ Die aktuelle Refnr im Header
         // den Header abrufen, wenn vorhanden
         var currRefNrHdr = Get_MsgHeader(msg, ConfigMail_MsgHeader.AppMailHeaderType.MailRefNr);
         /* Alternative
       
            // Haben wir eine Refnr im Hdr?
            var hdridx = msg.Headers.IndexOf
               (ConfigMailHeader.GetHeadername
                   (ConfigMailHeader.eMailHeader.MailRefNr));

            Header? currRefNrHdr = null;
            if (hdridx >= 0) {
               currRefNrHdr = msg.Headers[hdridx];
            }
      
         */

         //+ Die Refnr aus dem Mail Body/Subject
         var mailContentRefNr = BL_Mail_Body.FindMail_RefNr(msg);

         //+ Analyse
         //++ Der hdr existiert nicht
         if (currRefNrHdr == null) {
            // Den Header ergänzen
            msg.Headers.Add
               (ConfigMail_MsgHeader.GetHeadername
                   (ConfigMail_MsgHeader.AppMailHeaderType.MailRefNr)
                , mailContentRefNr);

            // Die Msg wurde geändert
            return true;
         }

         //++ Die RefNr ist identisch
         if (currRefNrHdr.Value.Equals(mailContentRefNr, StringComparison.OrdinalIgnoreCase)) {
            // Msg nicht verändert
            return false;
         }

         //++ Der Hdr hat eine Refnr,
         // Der Mail-Inhalt nicht: nichts ändern
         if (mailContentRefNr.Equals
                (BL_Mail_Common.NoRefNrFound, StringComparison.OrdinalIgnoreCase)) {
            // Msg nicht verändert
            return false;
         }

         //+ Der Mail-Inhalt hat eine andere Refnr als der Header
         // Den Mail Header aktualisieren
         currRefNrHdr.Value = mailContentRefNr;

         // Die Msg wurde geändert
         return true;
      }


      /// <summary>
      /// Versucht, den msg Header vom Typ eMailHeader zu holen 
      /// </summary>
      /// <param name="msg"></param>
      /// <param name="appMailHeaderType"></param>
      /// <returns></returns>
      public static Header? Get_MsgHeader(
         MimeMessage                              msg
         , ConfigMail_MsgHeader.AppMailHeaderType appMailHeaderType) {
         Header? currRefNrHdr = msg.Headers.FirstOrDefault
            (h => h.Field
                  == ConfigMail_MsgHeader.GetHeadername(appMailHeaderType));

         return currRefNrHdr;
      }


      /// <summary>
      ///  Aktualisiert oder ergänzt den Msg Header
      /// </summary>
      /// <param name="msg"></param>
      /// <param name="appMailHeaderType"></param>
      /// <param name="newValue"></param>
      /// <returns></returns>
      [Obsolete("Diese Funktion ist obsolete » obj BO_Mail", false)]
      private static bool AddOrUpdate_MsgHeader(
         MimeMessage                              msg
         , ConfigMail_MsgHeader.AppMailHeaderType appMailHeaderType
         , string                                 newValue) {
         Header? thisHdr = msg.Headers.FirstOrDefault
            // ! h.Fiels === der Name des Headers
            (h => h.Field
                  == ConfigMail_MsgHeader.GetHeadername(appMailHeaderType));

         if (thisHdr == null) {
            // Den Header ergänzen
            msg.Headers.Add
               (ConfigMail_MsgHeader.GetHeadername
                   (appMailHeaderType)
                , newValue);

            // verändert
            return false;
         }
         else {
            if (!thisHdr.Value.Equals(newValue)) {
               thisHdr.Value = newValue;

               // verändert
               return false;
            }
            else {
               // Nichts verändert
               return false;
            }
         }
      }

   }

}
