using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ImapMailManager.Mail.Anonymisierer;
using ImapMailManager.Mail.Parser;
using jig.Tools;
using jig.Tools.Crypto;
using jig.Tools.String;


namespace ImapMailManager.Mail;

/// <summary>
/// Definiert einen Fragesteller
/// Record Version 01, damit er allenfalls später erweitert werden kann
/// </summary>
public class RecFragestellerV01 {

   //+ Config

   #region Config

   private const char DelimiterClassProps = '|';
   private const char DelimiterForLists   = '•';

   // Dient zum ent- und verschlüsseln
   private static readonly string[] PropList = {
      "Kontaktformular_Nachname", "Kontaktformular_Vorname", "Kontaktformular_EMailAddress"
      , "Kontaktformular_EMailDisplayName", "Kontaktformular_EMailAddressInvalidText"
      , "Serialize, Deserialize MailHeader_EMailAddresses"
   };
   public static readonly string EncryptedRecDelimiter = "|";

   /// <summary>
   /// Version dieses Records
   /// Wird im E-Mail Header mitgespeichert, damit das Deserialisieren klappt
   /// </summary>
   public static readonly string RecordVersion = "V01";

   #endregion Config

   #region Props

   #region Daten vom Kontaktformular

   /// <summary>
   /// Wenn die E-Mail Adresse nicht geparst werden konnte,
   /// könnte es ein Tippfehler im Formular sein, deshalb speichern wir diese falsche E-Mail Adresse
   /// </summary>
   public string? Kontaktformular_EMailAddressInvalidText { get; set; }

   /// <summary>
   /// Die E-Mail Adresse, die via Parser im Kontaktformular des Mail Bodies gefunden wurde
   /// </summary>
   public string? Kontaktformular_EMailAddress { get; set; }

   /// <summary>
   /// Der Angezeige-Name der E-Mail Adresse
   /// "Thomas Schittli" Thomas.Schittli@jig.ch
   /// </summary>
   public string? Kontaktformular_EMailDisplayName { get; set; }

   /// <summary>
   /// Der Nachname des Fragestellers
   /// </summary>
   public string? Kontaktformular_Nachname { get; set; }

   /// <summary>
   /// Der Vorname des Fragestellers
   /// </summary>
   public string? Kontaktformular_Vorname { get; set; }

   #endregion Daten vom Kontaktformular

   #region Daten vom Mail Header

   /// <summary>
   /// Die E-Mail Adressen, die wir aus dem Mail Header Sender:, From, … erhalten haben
   /// </summary>
   public List<EMailAddress> MailHeader_EMailAddresses { get; set; }

   #region Berechnete Daten

   /// <summary>
   /// Serialisiert die MailHeader_EMailAddresses
   /// </summary>
   /// <returns></returns>
   public string Serialize_MailHeader_EMailAddresses() {
      StringBuilder sb = new StringBuilder();

      foreach (var mailHeader_EMailAddress in MailHeader_EMailAddresses) {
         sb.Append(mailHeader_EMailAddress.Serialize()).Append(DelimiterForLists);
      }

      return sb.ToString();
   }


   /// <summary>
   /// Deserialisiert die MailHeader_EMailAddresses
   /// </summary>
   /// <param name="serialized"></param>
   /// <returns></returns>
   public static List<EMailAddress> Deserialize_MailHeader_EMailAddresses(string serialized) {
      var                mailHeader_EMailAddresses_Items = serialized.Split(DelimiterForLists, StringSplitOptions.RemoveEmptyEntries);
      List<EMailAddress> deserialized_EMailAddresses     = new List<EMailAddress>();

      foreach (var mailHeader_EMailAddresses_Item in mailHeader_EMailAddresses_Items) {
         deserialized_EMailAddresses.Add(EMailAddress.Deserialize(mailHeader_EMailAddresses_Item));
      }

      return deserialized_EMailAddresses;
   }


   /// <summary>
   /// Von den Mail Headern die E-Mail Adressen als delimited String
   /// </summary>
   public string MailHdr_EMailAddresses_Delimited {
      get {
         // Sicherstellen, dass die E-Mail Adresse vom Formular nicht mit drin ist
         var tmp = MailHeader_EMailAddresses.Where
                                             (x => !x.EMailAddr.Equals
                                                      (Kontaktformular_EMailAddress
                                                       , StringComparison
                                                         .OrdinalIgnoreCase))
                                            .Where(x => x.EMailAddr.ØHasValue())
                                            .Distinct()
                                            .Select(x => x.EMailAddr.Trim())
                                            .OrderBy(x => x);

         return string.Join
            (DelimiterForLists, tmp);
      }
   }

   private List<EMailAddress_WithWordsForAnonymize> EMailAddress_WithWordsForAnonymize
      => Parser.EMailAddress_WithWordsForAnonymize.Create(MailHeader_EMailAddresses);

   /// <summary>
   /// Die Wortliste als delimited String
   /// </summary>
   private string MailHdr_Worte_Delimited => string.Join
      (DelimiterForLists
       , EMailAddress_WithWordsForAnonymize.ØGetForeignWords().ToList());

   /// <summary>
   /// Die Wort-Liste der Mail Header Adressen
   /// Ist immer sortiert, damit der Obj-Vergleich einfacher ist
   /// </summary>
   public List<string> MailHdr_Worte => EMailAddress_WithWordsForAnonymize.ØGetForeignWords().ToList();

   #endregion Berechnete Daten

   #endregion Daten vom Mail Header

   #endregion Props

   #region Calculated basic Props

   /// <summary>
   /// Liefert true, wenn der Record Daten vom Kontaktformular hat
   /// </summary>
   public bool HasKontaktformData => Kontaktformular_Nachname.ØHasValue()
                                     || Kontaktformular_Vorname.ØHasValue()
                                     || Kontaktformular_EMailAddress.ØHasValue()
                                     || Kontaktformular_EMailDisplayName.ØHasValue();

   #endregion Calculated basic Props

   #region Konstruktoren

   /// <summary>Initializes a new instance of the <see cref="T:System.Object" /> class.</summary>
   public RecFragestellerV01(
      string?              kontaktformular_Nachname
      , string?            kontaktformular_Vorname
      , string?            kontaktformular_EMailadress
      , string?            kontaktformular_EMailDisplayName
      , string?            kontaktformular_EMailAddressInvalidText
      , List<EMailAddress> mailHeader_EMailAdresses) {
      Kontaktformular_Nachname                = kontaktformular_Nachname;
      Kontaktformular_Vorname                 = kontaktformular_Vorname;
      Kontaktformular_EMailAddress            = kontaktformular_EMailadress;
      Kontaktformular_EMailDisplayName        = kontaktformular_EMailDisplayName;
      Kontaktformular_EMailAddressInvalidText = kontaktformular_EMailAddressInvalidText;
      MailHeader_EMailAddresses               = mailHeader_EMailAdresses;
   }


   public RecFragestellerV01() => MailHeader_EMailAddresses = new List<EMailAddress>();

   #endregion Konstruktoren

   #region Funktionen

   /// <summary>
   /// Liefert alle persänlichen Daten als String-Liste
   /// </summary>
   /// <returns></returns>
   public List<string> Get_PersönlicheDaten_AlsWorte() {
      List<string> persönlicheDaten_Worte = new List<string>();

      // Die Worte der Mail Hdr erfassen
      persönlicheDaten_Worte.AddRange(MailHdr_Worte);

      // Vom Kontaktformular alle Worte ergänzen
      persönlicheDaten_Worte.ØAppendItem_IfMissing(Kontaktformular_EMailAddress);
      persönlicheDaten_Worte.ØAppendItem_IfMissing(Kontaktformular_EMailAddressInvalidText);

      // 230215 Alt
      // persönlicheDaten_Worte.ØAppendItemIfMissing(Kontaktformular_EMailDisplayName);
      // persönlicheDaten_Worte.ØAppendItemIfMissing(Kontaktformular_Nachname);
      // persönlicheDaten_Worte.ØAppendItemIfMissing(Kontaktformular_Vorname);

      // 230215 Neu
      // Die Worte im DisplayName ergönzen
      foreach (var word in Anonymizer_Tools.TokenizeString_ForAnonymizer
                  (Kontaktformular_EMailDisplayName, Parser.EMailAddress_WithWordsForAnonymize.Cfg_MinWordLen).ØOrEmptyIfNull()) {
         persönlicheDaten_Worte.ØAppendItem_IfMissing(word);
      }

      // Die Worte im Nachnamen ergönzen
      foreach (var word in Anonymizer_Tools.TokenizeString_ForAnonymizer
                  (Kontaktformular_Nachname, Parser.EMailAddress_WithWordsForAnonymize.Cfg_MinWordLen).ØOrEmptyIfNull()) {
         persönlicheDaten_Worte.ØAppendItem_IfMissing(word);
      }
 
      // Die Worte im Vornamen ergönzen
      foreach (var word in Anonymizer_Tools.TokenizeString_ForAnonymizer
                  (Kontaktformular_Vorname, Parser.EMailAddress_WithWordsForAnonymize.Cfg_MinWordLen).ØOrEmptyIfNull()) {
         persönlicheDaten_Worte.ØAppendItem_IfMissing(word);
      }

      return persönlicheDaten_Worte.Where(x => x.ØHasValue())
                                   .Distinct()
                                   .OrderByDescending(x => x.Length).ToList();
   }


   /// <summary>
   ///  Liefert True, wenn irgend ein Prop einen Wert hat 
   /// </summary>
   /// <returns></returns>
   public string Get_DataAsString() {
      //+ Alle Props sammeln
      StringBuilder sb = new StringBuilder();

      //+++ Normale Props
      Kontaktformular_Nachname.ØHasValue().IfTrue
         (() => sb.Append(Kontaktformular_Nachname!.Trim()).Append(DelimiterClassProps));

      Kontaktformular_Vorname.ØHasValue().IfTrue
         (() => sb.Append(Kontaktformular_Vorname!.Trim()).Append(DelimiterClassProps));

      Kontaktformular_EMailAddress.ØHasValue().IfTrue
         (() => sb.Append(Kontaktformular_EMailAddress!.Trim()).Append(DelimiterClassProps));

      //+++ List<> - Prop
      MailHdr_EMailAddresses_Delimited.ØHasValue().IfTrue
         (() => sb.Append(MailHdr_EMailAddresses_Delimited!.Trim()).Append
             (DelimiterClassProps));

      //+++ Normale Props
      Kontaktformular_EMailDisplayName.ØHasValue().IfTrue
         (() => sb.Append(Kontaktformular_EMailDisplayName!.Trim()).Append(DelimiterClassProps));

      Kontaktformular_EMailAddressInvalidText.ØHasValue().IfTrue
         (() => sb.Append(Kontaktformular_EMailAddressInvalidText!.Trim()).Append
             (DelimiterClassProps));

      //+++ List<> - Prop
      MailHdr_Worte_Delimited.ØHasValue().IfTrue
         (() => sb.Append(MailHdr_Worte_Delimited!.Trim()));

      var res = sb.ToString().Trim().TrimEnd(DelimiterClassProps);

      return res;
   }


   /// <summary>
   /// Liefert true, wenn der Record mind. ein Prop gesetzt hat 
   /// </summary>
   /// <returns></returns>
   public bool HasData() { return Get_DataAsString().ØHasValue(); }


   /// <summary>Returns a string that represents the current object.</summary>
   /// <returns>A string that represents the current object.</returns>
   public override string ToString() {
      return
         $"{Kontaktformular_Vorname}, {Kontaktformular_Nachname}, {Kontaktformular_EMailAddress} ({Kontaktformular_EMailDisplayName}), {MailHdr_EMailAddresses_Delimited} / {MailHdr_Worte_Delimited}";
   }


   #region Encryption / Decryption

   /// <summary>
   /// Verschlüsselt den Record
   /// 
   /// Variante via Serialisierung klappt nicht, weil im Mail Header keine binären Daten sein können:
   /// https://gist.github.com/gauteh/669901/9928f254983e816a4ccbc02d0f81e026941affd0
   /// 
   /// </summary>
   /// <param name="key"></param>
   /// <returns></returns>
   public string EncryptRecord(string key) {
      StringBuilder sb = new StringBuilder();

      //+ Als erstes die VersionsNr
      sb.Append($"{RecordVersion}{EncryptedRecDelimiter}");

      //+ Alle Props durchlaufen und verschlüsseln 

      foreach (var propName in PropList) {
         string? propVal = "";

         switch (propName) {
            case "Serialize, Deserialize MailHeader_EMailAddresses": {
               // Die delimited Liste nützen und verschlüsseln
               propVal = (string)this.Serialize_MailHeader_EMailAddresses();

               break;
            }

            default: {
               // Den Wert des Props lesen
               propVal = (string)this.ØObjGetPropVal(propName);

               break;
            }
         }

         if (propVal.ØHasValue()) {
            //+ Erzeugt einen Base64 String
            // !M https://en.wikipedia.org/wiki/Base64#Base64_table_from_RFC_4648
            sb.Append(EncryptDecrypt.EncryptToBase64(key, propVal));
         }

         // Den Delimiter zufügen
         sb.Append(EncryptedRecDelimiter);
      }

      // Das letzte EncryptedRecDelimiter entfernen:
      sb.Remove(sb.Length - 1, 1);

      return sb.ToString();
   }


   /// <summary>
   /// Entschlüsselt einen Record
   /// 
   /// </summary>
   /// <param name="key"></param>
   /// <returns></returns>
   public static RecFragestellerV01? DecryptRecord(string key, string encryptedData) {
      //+ Die verschiedenen verschlüsselten Props in ein Array splitten
      var encryptedItems = encryptedData.Split
         (EncryptedRecDelimiter, StringSplitOptions.None);

      //+ Stimmt die Version des verschlpsselten Strings mit unserer Klasse überein?
      var Version = encryptedItems[0];

      if (!Version.Equals(RecordVersion, StringComparison.OrdinalIgnoreCase)) {
         // Die Version der verschlüsselten Daten passen nicht zu dieser Klasse
         return null;
      }

      //++ Die verschlüsselten Props des Objekts holen
      // Die verschlüsselten Datenm sind ab Element 1 (Element 0 ist die Version)
      var encryptedPropertyValues = encryptedItems[1..^0];

      //+ Den neuen Datensatz erzeugen
      var recFragestellerV01 = new RecFragestellerV01();

      // Im Obj die Props setzen
      for (var idx = 0; idx < encryptedPropertyValues.Length; idx++) {
         // Das Prop als verschlüsselter Str
         var encryptedPropertyValue = encryptedPropertyValues[idx];

         // Der Prop Name
         var propName = PropList[idx];

         switch (propName) {
            case "Serialize, Deserialize MailHeader_EMailAddresses": {
               if (encryptedPropertyValue.ØHasValue()) {
                  // Entschlüsseln
                  string decryptedVal = "";

                  try {
                     decryptedVal = EncryptDecrypt.DecryptFromBase64(key, encryptedPropertyValue);
                  } catch {
                     //+ Falsches Passwort
                     return null;
                  }

                  //+ Die Liste wieder herstellen
                  var mailHeader_EMailAddresses = RecFragestellerV01.Deserialize_MailHeader_EMailAddresses(decryptedVal);
                  // Das Obj Property seztzen
                  recFragestellerV01.ØObjSetPropVal("MailHeader_EMailAddresses", mailHeader_EMailAddresses);
                  
               } else
               {
                     // Das Obj Property ist leer
                     recFragestellerV01.ØObjSetPropVal("MailHeader_EMailAddresses", new List<EMailAddress>());
               }

               break;
            }

            default: {
               // Hat das Property Daten oder ist es ""?
               if (encryptedPropertyValue.ØHasValue()) {
                  // Entschlüsseln
                  string decryptedVal = "";

                  try {
                     decryptedVal = EncryptDecrypt.DecryptFromBase64(key, encryptedPropertyValue);
                  } catch {
                     //+ Falsches Passwort
                     return null;
                  }

                  // Das Obj Property seztzen
                  recFragestellerV01.ØObjSetPropVal(propName, decryptedVal);
               }
               else {
                  // Das Obj Property ist leer
                  recFragestellerV01.ØObjSetPropVal(propName, "");
               }

               break;
            }
         }
      }

      return recFragestellerV01;
   }

   #endregion Encryption / Decryption

   #endregion Funktionen

}
