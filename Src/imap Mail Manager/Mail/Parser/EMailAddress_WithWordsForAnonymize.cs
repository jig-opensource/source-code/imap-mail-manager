using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using ImapMailManager.Config;
using ImapMailManager.Mail.Anonymisierer;
using jig.Tools;
using jig.Tools.String;


namespace ImapMailManager.Mail.Parser;

/// <summary>
/// ! Extension Methods für EMailAddress_WithWordsForAnonymize
/// </summary>
public static class ExtensionMethods_EMailAddress_WithWordsForAnonymize {

   /// <summary>
   /// Liefert alle E-Mail Adressen, die nicht von unserer Antwort-Domäne stammen 
   /// </summary>
   /// <param name="eMailAddresses"></param>
   /// <returns></returns>
   public static IEnumerable<EMailAddress_WithWordsForAnonymize> ØGetForeignEMailAddresses(
      this IEnumerable<EMailAddress_WithWordsForAnonymize>? eMailAddresses) {
      if (eMailAddresses == null) { return Enumerable.Empty<EMailAddress_WithWordsForAnonymize>(); }

      return eMailAddresses.Where(x => !x.IsOurDomain);
   }


   /// <summary>
   /// Liefert von allen E-Mail Adressen von fremden Domänen
   /// die Wortliste
   ///
   /// Distinct, nach Wortlänge absteigend sortiert 
   /// </summary>
   /// <param name="eMailAddresses"></param>
   /// <returns></returns>
   public static IEnumerable<string> ØGetForeignWords(this IEnumerable<EMailAddress_WithWordsForAnonymize>? eMailAddresses) {
      if (eMailAddresses == null) { return Enumerable.Empty<string>(); }

      var foreign = ØGetForeignEMailAddresses(eMailAddresses);

      return foreign.SelectMany(x => x.Wortliste)
                    .Where(x => x.ØHasValue())
                    .Select(x => x.Trim())
                    .Distinct()
                    .OrderByDescending(x => x.Length);
   }

}


/// <summary>
/// Bildet eine E-Mail Adresse ab und hat Parser
/// - Die ganze E-Mail Adresse
/// - Der Displayname 
/// - LocalPart (alles vor dem @)
/// - Domain (alles nach dem @)
/// </summary>
public class EMailAddress_WithWordsForAnonymize : EMailAddress {

   //‼ Config

   #region Konfig

   /// <summary>
   /// Die Wortliste wird genützt, im persönliche Daten zu anonymisieren
   /// Damit wir nicht zu kurze Worte anonymisieren, müssen Worte in der Wortliste
   /// mindestens Cfg_MinWordLen Zeichen enthalten
   /// </summary>
   public static readonly int Cfg_MinWordLen = 4;

   #endregion Konfig

   #region Props

   /// <summary>
   /// Die Liste von Worten, die für die Anonymisierung benützt werden.
   /// Diese Worte extrahieren wir aus:
   /// - LocalPart
   /// - DisplayName
   /// </summary>
   public List<string> Wortliste = new List<string>();

   public List<string> WortlisteSortiert => IsOurDomain
                                               ? new List<string>()
                                               :

                                               //++ Die Worliste der Länge nach absteigend sortiert, damit wir zuerst die langen, passenden Worte ersetzen
                                               Wortliste.OrderByDescending(x => x.Length).ToList();

   #endregion Props

   #region Calculated Props

   /// <summary>
   /// Gehört die E-Mail Adresse zu unserer Domäne,
   /// aus der die Antworten kommen?
   /// </summary>
   public bool IsOurDomain {
      get {
         var isOurDomain = ConfigMail_Basics.Kat_FromAddr_DomainsAreProbablyAntworten.ØEqualsWithAnyItemInList(Domain)
                           || ConfigMail_Basics.Kat_FromAddr_Antworten.ØStartsWithAnyItemInList(EMailAddr);

         return isOurDomain;
      }
   }

   #endregion Calculated Props

   #region Konstruktoren

   #region Konstruktoren: Konvertierung von EMailAddress

   /// <summary>
   /// Erzeugt eine Instanz aus einem String mit einer E-Mail Adresse
   /// </summary>
   /// <param name="eMailAddress"></param>
   /// <returns></returns>
   public static EMailAddress_WithWordsForAnonymize Create(string eMailAddress) {
      return new EMailAddress_WithWordsForAnonymize(eMailAddress);
   }


   /// <summary>
   /// Erzeugt eine Instanz aus einer String-Liste mit E-Mail Adressen
   /// </summary>
   /// <param name="eMailAddresses"></param>
   /// <returns></returns>
   public static List<EMailAddress_WithWordsForAnonymize> Create(List<string> eMailAddresses) {
      List<EMailAddress_WithWordsForAnonymize> res = new List<EMailAddress_WithWordsForAnonymize>();

      foreach (var eMailAddress in eMailAddresses) {
         res.Add(new EMailAddress_WithWordsForAnonymize(eMailAddress));
      }

      return res;
   }


   public static EMailAddress_WithWordsForAnonymize Create(EMailAddress eMailAddress) {
      return new EMailAddress_WithWordsForAnonymize(eMailAddress);
   }


   public static List<EMailAddress_WithWordsForAnonymize> Create(List<EMailAddress> eMailAddresses) {
      List<EMailAddress_WithWordsForAnonymize> res = new List<EMailAddress_WithWordsForAnonymize>();

      foreach (var eMailAddress in eMailAddresses) {
         res.Add(new EMailAddress_WithWordsForAnonymize(eMailAddress));
      }

      return res;
   }


   /// <summary>
   /// Instanz mit einer EMailAddress erzeugen und die Worte analysieren 
   /// </summary>
   /// <param name="eMailAddress"></param>
   public EMailAddress_WithWordsForAnonymize(EMailAddress eMailAddress) : base(eMailAddress) {
      Calculate_Anonymisierungs_Wortliste();
   }

   #endregion Konstruktoren: Konvertierung von EMailAddress


   /// <summary>
   /// ! Konstruktor
   /// Bildet eine E-Mail Adresse ab:
   /// - Den DisplayName
   /// - Die ganze E-Mail Adresse
   /// - LocalPart
   /// - Domain
   /// - Es werden die Worte analysiert, die für die Anonymisierung genützt werden
   /// 
   /// !Ref https://en.wikipedia.org/wiki/Email_address
   /// </summary>
   /// <param name="eMailAddr"></param>
   /// <param name="displayName"></param>
   /// <param name="addressTyp"></param>
   public EMailAddress_WithWordsForAnonymize(
      string?        eMailAddr
      , string?      displayName = ""
      , EMailAddrTyp addressTyp  = EMailAddrTyp.Undefined) : base
      (eMailAddr
       , displayName
       , addressTyp) {
      Calculate_Anonymisierungs_Wortliste();
   }

   #endregion Konstruktoren

   #region Tools für die Wortliste

   /// <summary>
   /// Berechnet die Wortliste für die Anonymisierung
   /// </summary>
   void Calculate_Anonymisierungs_Wortliste() {
      //+ E-Mail Adressen unserer Antworten anonymisieren wir nicht
      if (IsOurDomain) { return; }

      //+ Alle Worte sammeln, die für die Anonymisierung genützt werden können
      AddWort_ToWortliste(EMailAddr);

      //+ Den Displayname in seine Worte trennen
      AddWorte_ToWortliste(Anonymizer_Tools.TokenizeString_ForAnonymizer(DisplayName, Cfg_MinWordLen));

      //++ LocalPart in seine Worte trennen
      AddWorte_ToWortliste(Anonymizer_Tools.TokenizeString_ForAnonymizer(LocalPart, Cfg_MinWordLen));

   }


   /// <summary>
   /// Die Wortliste ergänzen, wenn das Wort noch fehlt und lang genug ist
   /// </summary>
   /// <param name="wort"></param>
   public void AddWort_ToWortliste(string? wort) {
      if (wort.ØHasValue()) {
         wort = wort!.Trim().ToLower();

         // Nur genug lange Worte ergänzen, um nicht zu kurze Worte zu anonymisieren
         if (wort.Length >= Cfg_MinWordLen) {
            Wortliste.ØAppendItem_IfMissing(wort);
         }
      }
   }


   /// <summary>
   /// Die Wortliste ergänzen, wenn die Worte noch fehlen und lang genug sind
   /// </summary>
   /// <param name="worte"></param>
   public void AddWorte_ToWortliste(List<string>? worte) {
      if (worte != null) {
         foreach (var wort in worte) {
            AddWort_ToWortliste(wort);
         }
      }
   }

   #endregion Tools für die Wortliste

   #region Parser

   #region Parser für EMailAddress

   /// <summary>
   /// Liefert True, wenn text eine E-Mail Adresse beinhaltet
   /// </summary>
   /// <param name="text"></param>
   /// <returns></returns>
   public new static bool Has_EMail_Addr(string text) { return Parse_EMailAddresses(text).Item1.Any(); }


   /// <summary>
   /// Erzeugt aus str ein EMailAddress Obj 
   /// </summary>
   /// <param name="str"></param>
   /// <returns></returns>
   /// <exception cref="ArgumentException"></exception>
   [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
   public new static EMailAddress_WithWordsForAnonymize? Parse_OneEMailAddresses(string str) {
      var (res, _) = Parse_EMailAddresses(str);

      if (res.ØHasCountOfAtLeast(2)) {
         throw new ArgumentException("public EMailAddrAndDomain Parse(string str): Mehrere E-Mail Adressen gefunden!");
      }

      return res.FirstOrDefault();
   }


   /// <summary>
   /// Aus text alle E-Mail Adressen suchen, instanziieren und analysieren 
   /// </summary>
   /// <param name="text"></param>
   /// <returns></returns>
   public new static Tuple<List<EMailAddress_WithWordsForAnonymize>, string?> Parse_EMailAddresses(string text) {
      var (eMailAddresses, restString) = EMailAddress.Parse_EMailAddresses(text);

      return new Tuple<List<EMailAddress_WithWordsForAnonymize>, string?>(Create(eMailAddresses), restString);
   }

   #endregion Parser für EMailAddress

   #endregion Parser

   #region Tools für die Listen-Behandlung

   /*
      Der Compiler von vs.net meldet fehler, 
      also nützen wir direkt die generichen, statischen  Methoden in EMailAddress
    
      /// <summary>
      /// Address zur Liste zufügen, wenn sie noch nicht schon drin ist
      /// </summary>
      public static void AddToList(
         List<EMailAddress_WithWordsForAnonymize>   zielAddressList
         , List<EMailAddress_WithWordsForAnonymize> neueAdressen
         , bool                                     ignoreIgnoreType) {
         EMailAddress.AddToList
            (zielAddressList
             , neueAdressen
             , ignoreIgnoreType);
   
      }
   
   
      /// <summary>
      /// Address zur Liste zufügen, wenn sie noch nicht schon drin ist
      /// </summary>
      /// <param name="zielAddressList"></param>
      /// <param name="newAddressWithWordsForAnonymize"></param>
      /// <param name="ignoreIgnoreType"></param>
      public static void AddToList(
         List<EMailAddress_WithWordsForAnonymize> zielAddressList
         , EMailAddress_WithWordsForAnonymize     newAddressWithWordsForAnonymize
         , bool                                   ignoreIgnoreType) {
         EMailAddress.AddToList
            (zielAddressList
             , newAddressWithWordsForAnonymize
             , ignoreIgnoreType);
      }
   */

   #endregion Tools für die Listen-Behandlung

}
