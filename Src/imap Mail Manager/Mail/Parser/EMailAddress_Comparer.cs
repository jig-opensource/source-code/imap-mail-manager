using System.Collections.Generic;
using jig.Tools;
using jig.Tools.String;


namespace ImapMailManager.Mail.Parser; 


/// <summary>
/// Der Comparer der prüft, ob die Strings der Haupt-Liste
/// gleich starten wie einer der Strings der Vergleichsliste
/// </summary>
public class EMailAddress_Comparer : IEqualityComparer<EMailAddress> {

   public bool Equals(EMailAddress? left, EMailAddress? right) {
      if (left == null && right == null) {
         // Debug.WriteLine("• str2 == null && str1 == null");
         return true;
      }

      if (left == null || right == null) {
         // Debug.WriteLine("• str1 == null || str2 == null");
         return false;
      }

      var res = left.ToString().ØEqualsIgnoreCase(right.ToString());

      // Debug.WriteLine($"• Equals({str1}, {str2}): {res}");
      return res;
   }


   /// <summary>
   /// !KH9 TomTom
   /// 
   /// Damit der Vergleich immer Equals() aufruft,
   /// liefert der HashCode immer 0,
   /// so dass alle Elemente identisch scheinen und ein weiterer Vergleich nötig ist  
   /// </summary>
   /// <param name="obj"></param>
   /// <returns></returns>
   public int GetHashCode(EMailAddress obj) => 0;

}
