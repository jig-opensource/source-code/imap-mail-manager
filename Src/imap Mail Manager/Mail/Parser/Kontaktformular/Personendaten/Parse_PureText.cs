using System.Text.RegularExpressions;
using HtmlAgilityPack;
using jig.Tools;
using jig.Tools.String;


namespace ImapMailManager.Mail.Parser.Kontaktformular.Personendaten;

/// <summary>
/// Sucht, indem die ganze Mail als purer Text analysiert wird 
/// </summary>
public class Parse_PureText {

   #region Regex Kontaktformular Felder

   ///summary>
   /// Die Definition für die Formularfelder
   ///
   /// !To9^9 https://regex101.com/
   /// </summary>
   static readonly RegexOptions RgxOptionsKontaktformularFelder = RegexOptions.IgnoreCase
                                                                  | RegexOptions.Multiline
                                                                  | RegexOptions.Singleline
                                                                  | RegexOptions.ExplicitCapture
                                                                  | RegexOptions.CultureInvariant
                                                                  | RegexOptions.IgnorePatternWhitespace
                                                                  | RegexOptions.Compiled;

   static readonly string sRgxKontaktformularFelder = @"
                        (?<FormularElemente>
                        (?<VornameNachnameField>
                         # Toni, Block
                          (?<VornameNachnameTitel>
                           Vorname,\s*Nachname:\s*
                          )
                          (?:
                            (?<VornameNachnameSeparate>
                             (?<Vorname>[^,]+),\s*(?<Nachname>.+)
                            )|
                            (?<OnlyVornameOrNachname>
                             (?<VornameOrNachname>.+)
                            )
                          )
                        )
                        # gefolgt von irgendwelchen Zeichen
                        (?<EMailField>
                         E-Mail\s*Adresse:\s*(?<EMail>[^\s]+)?
                        )
                        )";

   public static readonly Regex oRgxKontaktformularFelder = new Regex
      (sRgxKontaktformularFelder, RgxOptionsKontaktformularFelder);

   #endregion Regex Kontaktformular Felder


   /// <summary>
   /// Parst den mailText und sucht nach Namen, Vornamen und E-Mail Adresse des Fragestellers 
   /// </summary>
   /// <param name="fullHtmlMailBody"></param>
   /// <returns></returns>
   public static RecFragestellerV01? Parse_MailHtmlBody(HtmlDocument htmlDoc_fullHtmlMailBody) {
      //+ Den Body holen
      var htmlBody = htmlDoc_fullHtmlMailBody.DocumentNode.SelectSingleNode("//body");

      //++ in puren Text konvertieren und white spaces entfernen
      var htmlAsText = BL_Mail_Common.OHtmlToText.Convert(htmlBody.OuterHtml).ØReduceWhiteSpaces
         (true);

      return Parse_MailHtmlBody(htmlAsText);
   }


   /// <summary>
   /// Parst den mailText und sucht nach Namen, Vornamen und E-Mail Adresse des Fragestellers 
   /// </summary>
   /// <returns></returns>
   public static RecFragestellerV01? Parse_MailHtmlBody(string pureTextBody) {
      //+ Suchen
      var matches = oRgxKontaktformularFelder.Matches(pureTextBody).ØOrEmptyIfNull();

      //+ Wenn gefunden, einen Record erzeugen
      var rec = new RecFragestellerV01();

      foreach (Match match in matches) {
         if (match.Groups["VornameOrNachname"].Value.ØHasValue()) {
            var VornameOrNachname = match.Groups["VornameOrNachname"].Value;

            // Weil VornameOrNachname gematcht hat und nicht Vorname & Nachname separat
            // deshalb wurde nur eines angegeben und wird löschen das Komma
            VornameOrNachname = VornameOrNachname.Replace(',', ' ').ØTrimAndReduce();
            if (VornameOrNachname.ØHasValue()) {
               rec.Kontaktformular_Nachname = VornameOrNachname;
            }
         }
         else {
            rec.Kontaktformular_Vorname  = match.Groups["Vorname"].Value;
            rec.Kontaktformular_Nachname = match.Groups["Nachname"].Value;
         }

         rec.Kontaktformular_EMailAddress = match.Groups["EMail"].Value;
      }

      return rec.HasData() ? rec : null;
   }

}
