using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using ImapMailManager.Config;
using ImapMailManager.Mail.MimeKit;
using jig.Tools;
using jig.Tools.String;
using MimeKit;


namespace ImapMailManager.Mail.Parser.Kontaktformular.Personendaten;

/// <summary>
/// Sucht im E-Mail Body nach Personendaten
/// Nützt:
/// - Parse_HtmlStruct
/// - Parse_PureText
/// </summary>
public class Parse_MailBody {

   /// <summary>
   /// Sucht im Mail Body alle E-Mail Adressen
   /// Filtert Adressen vom Team@RogerLiebi.ch
   /// </summary>
   /// <param name="msg"></param>
   /// <returns></returns>
   public static List<EMailAddress> Find_EMail_Addresses(MimeMessage msg) {
      //+ Den Mail Body (Text & HTML) holen
      var (msgTextPart, msgHtmlPart) = MimeKit_JigTools.Get_Msg_Bodies(msg, true);

      var mailText = "";

      //+ Haben wir einen HTML-Body?
      if (msgHtmlPart.ØHasValue()) {
         //++ Der Body als HTMLDoc
         var htmlDoc = new HtmlDocument();
         htmlDoc.LoadHtml(msgHtmlPart);
         //++ in puren Text konvertieren und white spaces entfernen
         mailText = BL_Mail_Common.OHtmlToText.Convert(htmlDoc.DocumentNode.OuterHtml).ØReduceWhiteSpaces();
      }

      //+ Hat der HTML Body keinen Text?
      if (!mailText.ØHasValue()) {
         //++ Den Text Body nützen
         mailText = msgTextPart;
      }

      //+ Nichts gefunden
      if (!mailText.ØHasValue()) {
         return new List<EMailAddress>();
      }

      //+ Alle E-Mail Adressen suchen
      var (eMailAddress_WithWordsForAnonymizes, _) = EMailAddress.Parse_EMailAddresses(mailText);

      
      //++ Entfernen von E-Mail Adressen vom Team
      IEnumerable<EMailAddress> objEMailAddresses = eMailAddress_WithWordsForAnonymizes.Where
         (x => !ConfigMail_Basics.Kat_FromAddr_Antworten
                                             .ØStartsWithAnyItemInList(x.EMailAddr)
                                              && !ConfigMail_Basics.Kat_FromAddr_DomainsAreProbablyAntworten
                                                                              .ØEqualsWithAnyItemInList(x.Domain));
      // Die E-Mail Adressen zurückgeben
      return objEMailAddresses.ToList();

   }


   /// <summary>
   /// Durchsucht den Message HTML Body anhand der HTML-Tags nach Kontaktformular Informationen
   /// Wenn der Parser nichts findet, den HTML Body als purer Text parsen
   /// </summary>
   /// <param name="msg"></param>
   /// <returns></returns>
   public static RecFragestellerV01? Find_Kontaktformular_Fragesteller_Daten(MimeMessage msg) {
      //+ Den Mail Body (Text & HTML) holen
      var (msgTextPart, msgHtmlPart) = MimeKit_JigTools.Get_Msg_Bodies(msg, true);

      RecFragestellerV01? recFragesteller = null;

      //+ Haben wir einen HTML-Body?
      if (msgHtmlPart.ØHasValue()) {
         //++ Der Body als HTMLDoc
         var htmlDoc = new HtmlDocument();
         htmlDoc.LoadHtml(msgHtmlPart);

         //+ Nach den Fragsteller-Daten suchen
         (_, _, recFragesteller)
            = Parse_HtmlStruct.Parse_MailHtmlBody(htmlDoc, msg.Subject);

         //+ Wenn der HTML Parser nicht erfolgreich war, den HTML-Text als pure Text durchsuchen
         if (recFragesteller == null
             || !recFragesteller.Get_DataAsString().ØHasValue()) {
            recFragesteller = Parse_PureText.Parse_MailHtmlBody(htmlDoc);
         }
      }

      //+ Wenn kein HTML Body vorhanden ist, den Text durchsuchen
      if (msgTextPart.ØHasValue()
          && (recFragesteller == null
              || !recFragesteller.Get_DataAsString().ØHasValue())) {
         recFragesteller = Parse_PureText.Parse_MailHtmlBody(msgTextPart);
      }

      return recFragesteller;

   }

}
