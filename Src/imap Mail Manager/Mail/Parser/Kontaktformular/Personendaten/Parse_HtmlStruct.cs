using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using jig.Tools;
using jig.Tools.Exception;
using jig.Tools.String;


namespace ImapMailManager.Mail.Parser.Kontaktformular.Personendaten;

/// <summary>
/// Sucht, indem aufeinanderfolgende HTML-Elemente analysiert werden 
/// </summary>
public class Parse_HtmlStruct {

   // ✅ Debug
   private static bool isDebugActive = false;


   private enum ParseState {

      SearchingFor_AnfrageRefNrTitel, SearchingFor_AnfrageRefNr, SearchingFor_VornameNachnameTitel
      , SearchingFor_VornameNachname, SearchingFor_EMailAddrTitel, SearchingFor_EMailAddr
      , ParsingDone

   }


   #region Regex Kontaktformular Felder: Vorname, Nachname

   ///summary>
   /// Definition Kontaktformular:
   /// Vorname, Nachname
   /// </summary>
   static readonly RegexOptions RgxOptionssRgxVornameNachname = RegexOptions.IgnoreCase
                                                                | RegexOptions.Multiline
                                                                | RegexOptions.ExplicitCapture
                                                                | RegexOptions.CultureInvariant
                                                                | RegexOptions
                                                                  .IgnorePatternWhitespace
                                                                | RegexOptions.Compiled;

   public static string SRgxVornameNachname = @"(?<Vorname>[^,]+),[^\S\r\n]*(?<Nachname>[^\r\n]*)
                                                |
                                                (?<VornameNachname>[^\r\n]*)
                                                ";
   public static readonly Regex ORgxVornameNachname = new Regex
      (SRgxVornameNachname, RgxOptionssRgxVornameNachname);

   #endregion Regex Kontaktformular Felder: Vorname, Nachname

   #region Parser Tests der neuen Abschnitte

   /// <summary>
   /// Test für den Parser:
   /// Haben wir diese Formular-Sektion?: "Anfrage:"
   /// </summary>
   /// <param name="testStr"></param>
   /// <returns>
   /// Liefert den Rest-String nach dem Match zurück
   /// var(success, restString) = … 
   /// </returns>
   private static Tuple<bool, string> Test_AnfrageTitel_RefNr(string testStr) {
      var match      = testStr.ØMatch(@"Anfrage\s*:\s*");
      var restString = "";

      if (match.Success) {
         restString = testStr.Substring(match.Index + match.Length);
      }

      return new Tuple<bool, string>(match.Success, restString);
   }


   /// <summary>
   /// Test für den Parser:
   /// Haben wir diese Formular-Sektion?: "Vorname, Nachname:"
   /// </summary>
   /// <param name="testStr"></param>
   /// <returns>
   /// Liefert den Rest-String nach dem Match zurück
   /// var(success, restString) = … 
   /// </returns>
   private static Tuple<bool, string> Test_AnfrageTitel_VornameNachname(string testStr) {
      var match      = testStr.ØMatch(@"Vorname,\s*Nachname\s*:\s*");
      var restString = "";

      if (match.Success) {
         restString = testStr.Substring(match.Index + match.Length);
      }

      return new Tuple<bool, string>(match.Success, restString);
   }


   /// <summary>
   /// Test für den Parser:
   /// Haben wir diese Formular-Sektion?: "E-Mail Adresse:"
   /// </summary>
   /// <param name="testStr"></param>
   /// <returns>
   /// Liefert den Rest-String nach dem Match zurück
   /// var(success, restString) = … 
   /// </returns>
   private static Tuple<bool, string> Test_AnfrageTitel_EMail(string testStr) {
      var match      = testStr.ØMatch(@"E-Mail\s*Adresse\s*:\s*");
      var restString = "";

      if (match.Success) {
         restString = testStr.Substring(match.Index + match.Length);
      }

      return new Tuple<bool, string>(match.Success, restString);
   }

   #endregion Parser Tests der neuen Abschnitte


   /// <summary>
   /// Prüft, ob der Parser eine andere Sektion findet, als dass wir erwarten
   /// </summary>
   /// <param name="testStr"></param>
   /// <returns>
   /// Allenfalls den State einer neu gefundnen Formular-Sektion
   /// var (newParseState, restString) = …
   /// </returns>
   private static Tuple<ParseState?, string> Parser_Test_Section_Skipped(ParseState currentState, string testStr) {
      foreach (ParseState testState in Enum.GetValues(typeof(ParseState))) {
         //+ Nur States prüfen, die nach dem aktuellen State sind
         if ((int)testState > (int)currentState) {
            //‼ Wir suchen nur nach neuen Formular-Sektionen, d.h. Titel
            switch (testState) {
               //+ Haben wir den Titel?: "Vorname, Nachname:"
               case ParseState.SearchingFor_VornameNachnameTitel: {
                  // Haben wir den Titel?: "Vorname, Nachname:"
                  var (success, restString) = Test_AnfrageTitel_VornameNachname(testStr);

                  if (success) {
                     return new Tuple<ParseState?, string>(ParseState.SearchingFor_VornameNachname, restString);
                  }

                  break;
               }

               case ParseState.SearchingFor_EMailAddrTitel: {
                  //++ Haben wir den Titel?: "E-Mail Adresse:"
                  var (success, restString) = Test_AnfrageTitel_EMail(testStr);

                  if (success) {
                     return new Tuple<ParseState?, string>(ParseState.SearchingFor_EMailAddr, restString);
                  }

                  break;
               }
            }
         }

      }

      //+ Keine neue Formular-Sektion gefunden 
      return new Tuple<ParseState?, string>(null, "");
   }


   /// <summary>
   /// Parst den HtmlText nach den Daten des Kontaktformulars
   /// </summary>
   /// <param name="fullHtmlMailBody"></param>
   /// <param name="thisMsgSubject"></param>
   /// <param name="subject"></param>
   /// <returns>
   /// var (subjectRefNr, bodyRefNr, recFragesteller) = …  
   /// </returns>
   /// <exception cref="ArgumentOutOfRangeException"></exception>
   public static Tuple<string?, string?, RecFragestellerV01?> Parse_MailHtmlBody(
      HtmlDocument htmlDoc_fullHtmlMailBody
      , string?    subject) {
      //+ Wenn der Betreff eine RefNr hat,
      // dann müsste auch der Body das Formular haben
      var subjectRefNr = BL_Mail_Common.Get_Mail_RefNr(subject);

      //+ Den Body holen
      var htmlBody = htmlDoc_fullHtmlMailBody.DocumentNode.SelectSingleNode("//body");

      //+ Die Nodes suchen
      //++ Alle Text Nodes
      // normalize-space: strips leading and trailing white-space from a string 
      // !M https://developer.mozilla.org/en-US/docs/Web/XPath/Functions/normalize-space
      var allTextNodes = htmlBody.SelectNodes("//text()[normalize-space(.) != '']");

      // ! Wenn wir keine Textnodes haben, sind wir fertig
      if (allTextNodes == null) {
         return new Tuple<string?, string?, RecFragestellerV01?>
            (subjectRefNr
             , null
             , null);
      }

      //+ Parser starten
      ParseState parseState = ParseState.SearchingFor_AnfrageRefNrTitel;

      //++ Ausgelesene Daten
      var     recFragesteller = new RecFragestellerV01();
      string? bodyRefNr       = null;

      // Wenn keine gültige E-Mail Adresse gefunden wurde,
      // dann speichern wir den ungültigen Text, ev. war es ein TippFehler 
      string? EMailAddrInvalidText = null;

      //+++ Alle Text-Elemente durchlaufen
      for (var idx = 0; idx < allTextNodes.Count && parseState != ParseState.ParsingDone; idx++) {
         var textNode = allTextNodes[idx];

         if (isDebugActive) {
            Debug.WriteLine(textNode.ØGetInnerText());
         }

         //++ Der restliche String, der noch zu parsen ist
         string restString = textNode.ØGetInnerText();

         ParseState lastParseState = parseState;
         string     lastRestString = restString;

         do {

            switch (parseState) {
               case ParseState.SearchingFor_AnfrageRefNrTitel: {
                  //+ Haben wir den Titel?: "Anfrage:"
                  (bool success, restString) = Test_AnfrageTitel_RefNr(restString);

                  if (success) {
                     parseState = ParseState.SearchingFor_AnfrageRefNr;

                     break;
                  }

                  // ! Skip?
                  //+ Haben wir einen anderen Bereich im Formular?
                  var (newSectionState, skippedStateRestString) = Parser_Test_Section_Skipped(parseState, restString);

                  if (newSectionState != null) {
                     restString = skippedStateRestString;
                     parseState = (ParseState)newSectionState;
                  }

                  break;
               }

               case ParseState.SearchingFor_AnfrageRefNr: {
                  //+ Haben wir die RefNr?
                  (bool success, string matchValue, restString) = BL_Mail_Common.Contains_Mail_RefNr(restString);

                  if (success) {
                     bodyRefNr  = matchValue;
                     parseState = ParseState.SearchingFor_VornameNachnameTitel;

                     break;
                  }

                  // ! Skip?
                  //+ Haben wir einen anderen Bereich im Formular?
                  var (newSectionState, skippedStateRestString) = Parser_Test_Section_Skipped(parseState, restString);

                  if (newSectionState != null) {
                     restString = skippedStateRestString;
                     parseState = (ParseState)newSectionState;
                  }

                  break;
               }

               case ParseState.SearchingFor_VornameNachnameTitel: {
                  //+ Haben wir den Titel?: "Vorname, Nachname:"
                  (bool success, restString) = Test_AnfrageTitel_VornameNachname(restString);

                  if (success) {
                     parseState = ParseState.SearchingFor_VornameNachname;

                     break;
                  }

                  // ! Skip?
                  //+ Haben wir einen anderen Bereich im Formular?
                  var (newSectionState, skippedStateRestString) = Parser_Test_Section_Skipped(parseState, restString);

                  if (newSectionState != null) {
                     restString = skippedStateRestString;
                     parseState = (ParseState)newSectionState;
                  }

                  break;
               }

               case ParseState.SearchingFor_VornameNachname: {
                  //++ Die erste folge-Node mit Inhalt ist der Vorname, Nachname
                  if (restString.ØHasValue()) {
                     // ! Skip?
                     //+ Haben wir einen anderen Bereich im Formular?
                     var (newSectionState, skippedStateRestString) = Parser_Test_Section_Skipped
                        (parseState, restString);

                     if (newSectionState != null) {
                        restString = skippedStateRestString;
                        parseState = (ParseState)newSectionState;

                        break;
                     }

                     //+ Haben wir ein Muster für den Namen und Vornamen?
                     var match = ORgxVornameNachname.Match(restString);

                     if (match.Success) {
                        // Wenn Vorname und Nachname kombiniert sind
                        if (match.Groups["VornameNachname"].Value.ØHasValue()) {
                           var vornameNachname = match.Groups["VornameNachname"].Value.Trim();

                           // Weil VornameOrNachname gematcht hat und nicht Vorname & Nachname separat
                           // deshalb wurde nur eines angegeben und wird löschen das Komma
                           vornameNachname = vornameNachname.Replace(',', ' ').ØTrimAndReduce();

                           if (vornameNachname.ØHasValue()) {
                              var items = vornameNachname.Split(' ');

                              //++ VornameNachname auftrennen:
                              // [0] » Vorname
                              // [1...] » Nachname 
                              var vorname = items.FirstOrDefault();

                              if (vorname.ØHasValue()) {
                                 recFragesteller.Kontaktformular_Vorname = vorname;
                              }

                              var nachname = string.Join(' ', items.Skip(1));

                              if (nachname.ØHasValue()) {
                                 recFragesteller.Kontaktformular_Nachname = nachname;
                              }
                           }

                        }
                        else {
                           recFragesteller.Kontaktformular_Vorname
                              = match.Groups["Vorname"].Value.Trim();

                           recFragesteller.Kontaktformular_Nachname
                              = match.Groups["Nachname"].Value.Trim();

                        }

                        restString = restString.Substring(match.Index + match.Length);
                        parseState = ParseState.SearchingFor_EMailAddrTitel;
                     }
                  }

                  break;
               }

               case ParseState.SearchingFor_EMailAddrTitel: {
                  //+ Haben wir den Titel?: "E-Mail Adresse:"
                  (bool success, restString) = Test_AnfrageTitel_EMail(restString);

                  if (success) {
                     parseState = ParseState.SearchingFor_EMailAddr;

                     break;
                  }

                  // ! Skip?
                  //+ Haben wir einen anderen Bereich im Formular?
                  var (newSectionState, skippedStateRestString) = Parser_Test_Section_Skipped(parseState, restString);

                  if (newSectionState != null) {
                     restString = skippedStateRestString;
                     parseState = (ParseState)newSectionState;
                  }

                  break;
               }

               case ParseState.SearchingFor_EMailAddr: {
                  //++ Die erste folge-Node mit Inhalt ist der Vorname, Nachname
                  if (restString.ØHasValue()) {
                     (List<EMailAddress_WithWordsForAnonymize> eMailAddresses, restString)
                        = EMailAddress_WithWordsForAnonymize.Parse_EMailAddresses(restString);

                     switch (eMailAddresses.Count) {
                        case 0: {
                           // Wenn das Parsen nicht klappte, könnte es im Formular ein Tippfehler gewesen sein
                           // Deshalb den Text trotzdem speichern 
                           recFragesteller.Kontaktformular_EMailAddressInvalidText
                              = restString;

                           break;
                        }

                        case 1: {
                           recFragesteller.Kontaktformular_EMailAddress = eMailAddresses[0].EMailAddr;

                           break;
                        }

                        default:
                           throw new RuntimeException
                              (@"Parse_MailHtmlBody(HtmlDocument htmlDoc_fullHtmlMailBody , string? subject)");
                     }

                     parseState = ParseState.ParsingDone;
                  }

                  break;
               }

               case ParseState.ParsingDone: {
                  break;
               }

               default: {
                  // Switch-Case hat ein Element, das als case behandelt werden muss 
                  throw new ArgumentOutOfRangeException();
               }
            }

            //+ Sicherstellen, dass der gleiche Rest-String im gleichen State nicht immer wieder geprüft wird
            if (parseState == lastParseState && lastRestString.Equals(restString)) {
               // Der State hat nicht geändert, d.h. der Rest-String hatte keinen passenden Inhalt
               restString = "";
            }
            else {
               // Den neuen State merken
               lastParseState = parseState;
               lastRestString = restString;
            }

         }
         while (restString.ØHasValue());

      }

      //+ Das Resultat zurückgeben
      if (recFragesteller.HasData()) {
         return new Tuple<string?, string?, RecFragestellerV01?>
            (subjectRefNr
             , bodyRefNr
             , recFragesteller);
      }
      else {
         return new Tuple<string?, string?, RecFragestellerV01?>
            (subjectRefNr
             , bodyRefNr
             , null);
      }
   }

}
