using ImapMailManager.BO;
using jig.Tools.String;
using PCRE;


namespace ImapMailManager.Mail.Parser.Kontaktformular;

/// <summary>
/// Sucht das im Kontaktformular ausgewählte Thema 
/// </summary>
public class ParseKontaktformular_Thema {

   #region RegEx

   // !KH9'9
   // i » PcreOptions.IgnoreCase 
   // n » PcreOptions.ExplicitCapture 
   // x » PcreOptions.IgnorePatternWhitespace 
   // m » PcreOptions.MultiLine   »  ^$ matches line breaks 
   // s » PcreOptions.Singleline  »   . matches line breaks 

   #region RegEx Kontaktformular Thema, alte Version: Mail Subject

   private const PcreOptions RgxOpt_MailSubject_Kontaktformular_Thema = PcreOptions.Compiled
                                                                        | PcreOptions.IgnoreCase
                                                                        | PcreOptions.Caseless
                                                                        | PcreOptions.IgnorePatternWhitespace
                                                                        | PcreOptions.ExplicitCapture

                                                                        // | PcreOptions.MultiLine
                                                                        // | PcreOptions.Singleline
                                                                        | PcreOptions.Unicode;

   private const string SRgxOpt_MailSubject_Kontaktformular_Thema = @"
                              (?<AltesFormular>
                                (?<PraefixAlt>\p{Zs}*•\p{Zs}*\bRL\p{Zs}+Formular:)
                                (?<WhiteSpaces>\s+)
                                (?<ThemaAlt>[^\r\n]*)
                                (?<EOL>\s*$)
                              )
                              |
                              (?<NeuesFormular>
                                (?<PraefixNeu>\p{Zs}*•\p{Zs}*)
                                (?<ThemaNeu>[^:]*)
                              )";

   private static PcreRegex _oRgx_MailSubject_Kontaktformular_Thema = new PcreRegex
      (SRgxOpt_MailSubject_Kontaktformular_Thema, RgxOpt_MailSubject_Kontaktformular_Thema);

   #endregion RegEx Kontaktformular Thema, alte Version: Mail Subject

   #region RegEx Kontaktformular Thema, neue Version: Mail Body

   private const PcreOptions RgxOpt_MailBody_Kontaktformular_Thema =
      PcreOptions.Compiled
      | PcreOptions.IgnoreCase
      | PcreOptions.Caseless
      | PcreOptions.IgnorePatternWhitespace
      | PcreOptions.ExplicitCapture

      // | PcreOptions.MultiLine
      // | PcreOptions.Singleline
      | PcreOptions.Unicode;

   private const string SRgxOpt_MailBody_Kontaktformular_Thema = @"
                              (?<Titel>^\s*\bThema:)
                              (?<WhiteSpaces>\s+)
                              (?<Thema>[^\r\n]*)
                              (?<EOL>\s*$)";

   private static PcreRegex _oRgx_MailBody_Kontaktformular_Thema = new PcreRegex
      (SRgxOpt_MailBody_Kontaktformular_Thema, RgxOpt_MailBody_Kontaktformular_Thema);

   #endregion RegEx Kontaktformular Thema, neue Version: Mail Body

   #endregion RegEx


   /// <summary>
   /// Sucht das Thema des Kontaktformulars:
   /// 1. Im Subject
   /// 2. Im Mail Body
   /// </summary>
   /// <param name="boMail"></param>
   /// <returns>
   /// "", wenn kein Thema gefunden
   /// Sonst das Thema 
   /// </returns>
   public static string Parse_Kontaktformular_Thema(BO_Mail boMail) {

      //+ Haben wir im Betreff einen Treffer?
      var subject = boMail.Mail_Subject;
      if (subject.ØHasValue()) {
         PcreMatch matchSubject = _oRgx_MailSubject_Kontaktformular_Thema.Match(subject);
         if (matchSubject.Success) {
            var themaAlt = matchSubject.Groups["ThemaAlt"].Value.ØTrim();
            var themaNeu = matchSubject.Groups["ThemaNeu"].Value.ØTrim();
            var thema    = themaNeu.ØHasValue() ? themaNeu : themaAlt;
            if (thema.ØHasValue()) { return thema; }
         }
      }
      
      //+ Sonst den Mail Body durchsuchen
      var       mailBodytextWithCrLf = boMail.Mail_Body_TextOnly;
      PcreMatch matchBody                = _oRgx_MailBody_Kontaktformular_Thema.Match(mailBodytextWithCrLf);
      if (matchBody.Success) {
         var thema = matchBody.Groups["Thema"].Value.ØTrim();
         return thema;
      }
      return "";
   }

}
