using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;
using jig.Tools;
using jig.Tools.Exception;
using jig.Tools.String;
using MimeKit;


namespace ImapMailManager.Mail.Parser;

/// <summary>
/// Bildet eine E-Mail Adresse ab, mit:
/// - Displayname
/// - E-Mail Adresse
/// - LocalPart, der Teil vor dem @
/// - Domain, der Teail nach dem @
/// 
/// Bietet Hilfsfunktionen:
/// - fürs Parsen
/// - zum Sammeln von unique E-Mail Adressen
/// </summary>
public class EMailAddress {

   //+ Config

   #region Config

   private static readonly char SerializeDeserializeDelimiter = '»';

   #endregion Config

   #region enum

   /// <summary>
   /// Die Art der E-Mail Adresse, wie sie adressiert wurde
   /// (To:, CC:, …)  
   /// </summary>
   public enum EMailAddrTyp {

      Cc, Bcc, To, From, Sender, Undefined

      , ReplyTo

   }

   #endregion enum

   #region Regex E-Mail Adresse

   ///summary>
   /// Regex für die E-Mail Adresse
   /// </summary>
   private static readonly RegexOptions RgxOptionssRgxEMailAddr = RegexOptions.IgnoreCase
                                                                  | RegexOptions.Multiline
                                                                  | RegexOptions.ExplicitCapture
                                                                  | RegexOptions.CultureInvariant
                                                                  | RegexOptions.IgnorePatternWhitespace
                                                                  | RegexOptions.Compiled;
   /// <summary>
   /// LocalPart@Domain
   /// </summary>
   public static string SRgxEMailAddr = @"
         (?<EMail>
           (?<MailLocalPart>([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z]))@
           (?<Domain>([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9])
         )";
   public static readonly Regex ORgxEMailAddr = new Regex
      (SRgxEMailAddr, RgxOptionssRgxEMailAddr);

   #endregion Regex E-Mail Adresse

   #region Props

   public EMailAddrTyp AddressTyp { get; init; }
   /// <summary>
   /// Die ganze E-Mail Adresse
   /// </summary>
   public string EMailAddr { get; init; }
   /// <summary>
   /// Der Anzeigename der E-Mail Adresse
   /// </summary>
   public string DisplayName { get; init; }
   /// <summary>
   /// Alles vor dem @
   /// </summary>
   public string LocalPart { get; init; }
   /// <summary>
   /// Alles nach dem @
   /// </summary>
   public string Domain { get; init; }

   #endregion Props

   #region Konstruktoren

   /// <summary>
   /// Initialisiert eine neue Instanz mit einer bestehenden E-Mail Adresse
   /// </summary>
   /// <param name="eMailAddress"></param>
   protected EMailAddress(EMailAddress eMailAddress) {
      AddressTyp  = eMailAddress.AddressTyp;
      EMailAddr   = eMailAddress.EMailAddr;
      DisplayName = eMailAddress.DisplayName;
      LocalPart   = eMailAddress.LocalPart;
      Domain      = eMailAddress.Domain;
   }


   /// <summary>
   /// ! Konstruktor
   /// Bildet eine E-Mail Adresse ab:
   /// - Den DisplayName
   /// - Die ganze E-Mail Adresse
   /// - LocalPart
   /// - Domain
   /// - Es werden die Worte analysiert, die für die Anonymisierung genützt werden
   /// 
   /// !Ref https://en.wikipedia.org/wiki/Email_address
   /// </summary>
   /// <param name="eMailAddr"></param>
   /// <param name="displayName"></param>
   /// <param name="addressTyp"></param>
   public EMailAddress(
      string?        eMailAddr
      , string?      displayName = ""
      , EMailAddrTyp addressTyp  = EMailAddrTyp.Undefined) {

      //+ Params prüfen
      eMailAddr   ??= "";
      displayName ??= "";

      AddressTyp  = addressTyp;
      DisplayName = displayName;

      //+ Init
      EMailAddr = "";
      LocalPart = "";
      Domain    = "";

      //+ eMailAddr parsen
      var match = ORgxEMailAddr.Match(eMailAddr);

      if (match.Success) {
         if (match.Groups["EMail"].Value.ØHasValue()) {
            EMailAddr = match.Groups["EMail"].Value.Trim();
         }

         if (match.Groups["MailLocalPart"].Value.ØHasValue()) {
            LocalPart = match.Groups["MailLocalPart"].Value.Trim();
         }

         if (match.Groups["Domain"].Value.ØHasValue()) {
            Domain = match.Groups["Domain"].Value.Trim();
         }
      }

   }

   #endregion Konstruktoren

   #region Parser

   #region Parser für EMailAddress

   /// <summary>
   /// Liefert True, wenn text eine E-Mail Adresse beinhaltet
   /// </summary>
   /// <param name="text"></param>
   /// <returns></returns>
   public static bool Has_EMail_Addr(string text) => Parse_EMailAddresses(text).Item1.Any();


   /// <summary>
   /// Erzeugt aus str ein EMailAddress Obj 
   /// </summary>
   /// <param name="str"></param>
   /// <returns></returns>
   /// <exception cref="ArgumentException"></exception>
   [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
   public static EMailAddress? Parse_OneEMailAddresses(string str) {
      var (res, _) = Parse_EMailAddresses(str);

      if (res.ØHasCountOfAtLeast(2)) {
         throw new ArgumentException("public EMailAddrAndDomain Parse(string str): Mehrere E-Mail Adressen gefunden!");
      }

      return res.FirstOrDefault();
   }


   /// <summary>
   /// Liest aus dem Text alle E-Mail Adressen, die gefunden werden
   /// und erzeugt daraus eine Liste vom EMailAddress Objekten
   /// </summary>
   /// <param name="text"></param>
   /// <returns>
   /// var (eMailAddresses, restString) = …
   /// eMailAddresses: Liste von EMailAddress 
   /// </returns>
   public static Tuple<List<EMailAddress>, string?> Parse_EMailAddresses(string text) {
      //+ Suchen
      var                matches        = EMailAddress.ORgxEMailAddr.Matches(text).ØOrEmptyIfNull();
      List<EMailAddress> eMailAddresses = new List<EMailAddress>();

      //+ Wenn gefunden, einen Record erzeugen
      var restString = "";

      foreach (Match match in matches) {
         if (match.Groups["EMail"].Success) {
            // Wenn wir E-Mail Daten haben, diese sammeln
            var eMailAddr = match.Groups["EMail"].Value;

            if (eMailAddr.ØHasValue()) {
               eMailAddresses.Add(new EMailAddress(eMailAddr));
            }
         }

         //+++ Den Rest des Strings nach dem Match merken
         restString = text.Substring(match.Index + match.Length);
      }

      //+ Nur eindeutige E-Mail Adressen zurückgeben
      return new Tuple<List<EMailAddress>, string?>
         (eMailAddresses.Distinct(new EMailAddress_Comparer()).ToList()
          , restString);
   }

   #endregion Parser für EMailAddress

   #region Parser für MimeKit

   /// <summary>
   /// Parser für Parse_MimeKit_InternetAddressList
   /// 
   /// Liefert eine berienigte Liste von EMailAddAndDisplayName zurück 
   /// </summary>
   /// <param name="eMailAddrTyp"></param>
   /// <param name="internetAddressList"></param>
   /// <returns></returns>
   public static List<T> Parse_MimeKit_InternetAddressList<T>(
      EMailAddrTyp          eMailAddrTyp
      , InternetAddressList internetAddressList)
      where T : EMailAddress {
      List<T> eMailAddresses = new List<T>();

      foreach (MailboxAddress? mailboxAddress in internetAddressList.Mailboxes) {
         // eMailAddresses.AddRange();
         eMailAddresses.Add(Parse_MimeKit_MailboxAddress<T>(eMailAddrTyp, mailboxAddress));
      }

      return eMailAddresses;
   }


   /// <summary>
   /// Parser für MailboxAddress
   /// 
   /// Liefert ein berienigtes MailboxAddress zurück 
   /// </summary>
   /// <param name="eMailAddrTyp">
   /// Der typ der Adressierung: To:, CC:, … 
   /// </param>
   /// <param name="mailboxAddress">
   /// E-Mail Adresse, optional kombiniert mit dem DisplayName
   /// !Ref https://www.rfc-editor.org/rfc/rfc5322#section-3.4 
   /// </param>
   /// <returns></returns>
   /// <exception cref="RuntimeException"></exception>
   public static T Parse_MimeKit_MailboxAddress<T>(
      EMailAddrTyp     eMailAddrTyp
      , MailboxAddress mailboxAddress)
      where T : EMailAddress {

      // Liste von Zeichenfolgen im EMail DisplayName, die wir filtern
      List<string> blacklistWordsRgx = new List<string>() { "/o=", "/cn=", "\bFrau\b", "\bHerr\b" };

      string eMailAdress = "";

      if (mailboxAddress.Address.ØHasValue()) {
         eMailAdress = mailboxAddress.Address.Trim('\'');
      }

      //+ eMailAdresse bereinigen
      //++ Sonderfall eMailAdresse MISSING_MAILBOX
      if (eMailAdress.ØEqualsIgnoreCase("MISSING_MAILBOX")) {
         eMailAdress = "";
      }

      //+ displayName bereinigen
      var displayName = mailboxAddress.Name?.Trim('\'', '"') ?? "";

      //++ Wenn ein Blacklist Element passt, den Displayname löschen
      if (blacklistWordsRgx.ØRgxMatchWithAnyItemInList(displayName)) {
         displayName = "";
      }

      //++ Die Liste mit den gefundenen E-Mail Adressen
      var (list, _) = Parse_EMailAddresses(displayName);

      //++ Wenn der DisplayName mehrere E-Mail Adressen hat
      if (list.ØHasCountOfAtLeast(2)) {
         throw new RuntimeException
            ("Parse_MimeKit_MailboxAddress(MailboxAddress mailboxAddress): Mehrere E-Mail Adressen gefunden!");
      }

      //++ Der Displayname hat eine E-Mail Adresse
      if (list.Any()) {
         //+++ Die E-Mail Adresse hat einen Wert
         if (eMailAdress.ØHasValue()) {
            // Der Displayname ist eine Kopie der E-Mail Adresse
            if (list.First().EMailAddr.ØEqualsIgnoreCase(eMailAdress)) {
               // Den Displayname löschen
               displayName = "";
            }
            else {
               //+++ eMailAdresse und displayName haben je eine andere E-Mail Adresse
               // Beide speichern
               displayName = list.First().EMailAddr;
            }
         }
         else {
            // Die eMailAdress ist leer, aber der Displayname hat eine E-Mail Adresse
            eMailAdress = list.First().EMailAddr;
            displayName = "";
         }
      }

      //++ _ entfernen
      displayName = Regex.Replace
                          (displayName
                           , @"_+"
                           , " "
                           , RegexOptions.Multiline)
                         .Trim();

      //+ Wenn der displayName keine E-Mail Adresse ist
      if (Parse_OneEMailAddresses(displayName) == null) {
         //++ Einzelne Punkte entfernen
         displayName = Regex.Replace
                             (displayName
                              , @"\.+"
                              , " "
                              , RegexOptions.Multiline)
                            .Trim();

         //++ Einzelne Buchstaben
         displayName = Regex.Replace
                             (displayName
                              , @"\b[a-zA-Z]\b"
                              , " "
                              , RegexOptions.Multiline)
                            .Trim();
      }

      //++ Mehrfache Whitespaces reduzieren
      displayName = displayName.ØTrimAndReduce();

      //+ Instanz erzeugen
      return (T)Activator.CreateInstance
         (typeof(T)
          , eMailAdress
          , displayName
          , eMailAddrTyp)!;
   }

   #endregion Parser für MimeKit

   #endregion Parser

   #region Tools für die Listen-Behandlung

   /// <summary>
   /// Address zur Liste zufügen, wenn sie noch nicht schon drin ist
   /// </summary>
   public static void AddToList<T>(
      List<T>   zielAddressList
      , List<T> neueAdressen
      , bool    ignoreIgnoreType)
      where T : EMailAddress {
      foreach (var thisAddress in neueAdressen) {
         AddToList
            (zielAddressList
             , thisAddress
             , ignoreIgnoreType);
      }
   }


   /// <summary>
   /// Address zur Liste zufügen, wenn sie noch nicht schon drin ist
   /// </summary>
   /// <param name="zielAddressList"></param>
   /// <param name="newAddressWithWordsForAnonymize"></param>
   /// <param name="ignoreIgnoreType"></param>
   public static void AddToList<T>(
      List<T> zielAddressList
      , T     newAddressWithWordsForAnonymize
      , bool  ignoreIgnoreType)
      where T : EMailAddress {
      var addrFound = false;

      foreach (var thisAddress in zielAddressList) {
         //+ Wenn wir den AddressTyp ignorieren 
         if (ignoreIgnoreType) {
            //++ Wenn E-Mail Adresse oder DisplayName ungleich sind,
            if (thisAddress.EMailAddr.ØEqualsIgnoreCase(newAddressWithWordsForAnonymize.EMailAddr)
                && !thisAddress.DisplayName.ØEqualsIgnoreCase(newAddressWithWordsForAnonymize.DisplayName)) {
               addrFound = true;
            }
         }
         else {
            //+ Wenn wir den AddressTyp berücksichtigen 
            //++ Wenn E-Mail Adresse, DisplayName oder AddressTyp ungleich sind,
            if (thisAddress.EMailAddr.ØEqualsIgnoreCase(newAddressWithWordsForAnonymize.EMailAddr)
                && !thisAddress.DisplayName.ØEqualsIgnoreCase(newAddressWithWordsForAnonymize.DisplayName)
                && thisAddress.AddressTyp != newAddressWithWordsForAnonymize.AddressTyp
               ) {
               addrFound = true;
            }
         }
      }

      //+++ Die Adresse erfassen
      if (!addrFound) {
         zielAddressList.Add(newAddressWithWordsForAnonymize);
      }
   }

   #endregion Tools für die Listen-Behandlung

   #region Serialize, Deserialize

   /// <summary>
   /// Serialisiert das Obj in einen String
   /// </summary>
   /// <returns></returns>
   public string Serialize()
      => $"{DisplayName}{SerializeDeserializeDelimiter}{EMailAddr}{SerializeDeserializeDelimiter}{AddressTyp}";


   /// <summary>
   /// De-Serialisiert das Obj von einen String
   /// </summary>
   /// <param name="serialized"></param>
   /// <returns></returns>
   public static EMailAddress Deserialize(string serialized) {
      var items       = serialized.Split(SerializeDeserializeDelimiter);
      var displayname = items.Length >= 1 ? items[0] : "";
      var eMailAddr   = items.Length >= 2 ? items[1] : "";
      Enum.TryParse(items[2], out EMailAddrTyp addressTyp);

      return new EMailAddress(eMailAddr, displayname, addressTyp);
   }

   #endregion Serialize, Deserialize


   /// <summary>Returns a string that represents the current object.</summary>
   /// <returns>A string that represents the current object.</returns>
   public override string ToString() { return $"{EMailAddr} / {LocalPart} / {Domain}"; }


   public void Deconstruct(
      out string eMailAddr

      // tom
      , out string localPart

      // jig.ch
      , out string domain) {
      eMailAddr = this.EMailAddr;
      localPart = this.LocalPart;
      domain    = this.Domain;
   }

}
