using System.Collections.Generic;
using MailKit;
using MimeKit;


namespace ImapMailManager.Mail.Parser;

/// <summary>
/// Analysiert die Mail Properties (To, From, …) und versucht so,
/// Personendaten zum Fragesteller zu erfahren 
/// </summary>
public class Parser_MsgProperties {

   /// <summary>
   /// ! Overloads diserer Funktion: (je mit identischer Funktion):
   /// - für mit IMessageSummary
   /// - für mit MimeMessage
   /// 
   /// Analysiert IMessageSummary Properties
   /// und liefert alle gefundenen E-Mail Adressen und deren DisplayNamen
   ///
   /// 
   /// </summary>
   /// <param name="msgSummary"></param>
   /// <returns>
   /// var (persönlicheDaten_EMailAddr, persönlicheDaten_Worte) = 
   /// </returns>
   public static List<EMailAddress> Parse_MailProperties(
      IMessageSummary msgSummary) {
      //+ Alle E-Mail Adressen dieser Msg sammeln
      List<EMailAddress> alleMails = new List<EMailAddress>();

      //++ Die CC Adressen analysieren
      if (msgSummary.Envelope.Cc != null) {
         List<EMailAddress> thisEMails = EMailAddress.Parse_MimeKit_InternetAddressList<EMailAddress>(EMailAddress.EMailAddrTyp.Cc, msgSummary.Envelope.Cc);

         //+ Und in der Liste erfassen, wenn sie noch unbekannt sind
         EMailAddress.AddToList
            (alleMails
             , thisEMails
             , true);
      }

      //++ Die BCC Adressen analysieren
      if (msgSummary.Envelope.Bcc != null) {
         List<EMailAddress> thisEMails = EMailAddress.Parse_MimeKit_InternetAddressList<EMailAddress>
            (EMailAddress.EMailAddrTyp.Bcc, msgSummary.Envelope.Bcc);

         //+ Und in der Liste erfassen, wenn sie noch unbekannt sind
         EMailAddress.AddToList
            (alleMails
             , thisEMails
             , true);
      }

      //++ Die To Adressen analysieren
      if (msgSummary.Envelope.To != null) {
         List<EMailAddress> thisEMails = EMailAddress.Parse_MimeKit_InternetAddressList<EMailAddress>
            (EMailAddress.EMailAddrTyp.To, msgSummary.Envelope.To);

         //+ Und in der Liste erfassen, wenn sie noch unbekannt sind
         EMailAddress.AddToList
            (alleMails
             , thisEMails
             , true);
      }

      //++ Die From Adressen analysieren
      if (msgSummary.Envelope.From != null) {
         List<EMailAddress> thisEMails = EMailAddress.Parse_MimeKit_InternetAddressList<EMailAddress>
            (EMailAddress.EMailAddrTyp.From, msgSummary.Envelope.From);

         //+ Und in der Liste erfassen, wenn sie noch unbekannt sind
         EMailAddress.AddToList
            (alleMails
             , thisEMails
             , true);
      }

      //++ Die Sender Adressen analysieren
      if (msgSummary.Envelope.Sender != null) {
         //+ Die CC Adressen analysieren
         List<EMailAddress> thisEMail = EMailAddress.Parse_MimeKit_InternetAddressList<EMailAddress>
            (EMailAddress.EMailAddrTyp.Sender, msgSummary.Envelope.Sender);

         //+ Und in der Liste erfassen, wenn sie noch unbekannt sind
         EMailAddress.AddToList
            (alleMails
             , thisEMail
             , true);
      }

      // Alle gefundenen E-Mails analysieren und die Personen-Daten zurückgeben
      return alleMails;
   }


   /// <summary>
   /// ! Overloads diserer Funktion: (je mit identischer Funktion):
   /// - für mit IMessageSummary
   /// - für mit MimeMessage
   /// 
   /// Analysiert MimeMessage Properties
   /// und liefert alle gefundenen E-Mail Adressen und deren DisplayNamen
   ///
   /// </summary>
   /// <param name="msg"></param>
   /// <returns>
   /// var (persönlicheDaten_EMailAddr, persönlicheDaten_Worte) = 
   /// </returns>
   public static List<EMailAddress> Parse_MailProperties(MimeMessage msg) {
      //+ Alle E-Mail Adressen dieser Msg sammeln
      List<EMailAddress> alleMails = new List<EMailAddress>();

      //++ CC
      if (msg.Cc != null) {
         //+ Die CC Adressen analysieren
         List<EMailAddress> thisEMails
            = EMailAddress.Parse_MimeKit_InternetAddressList<EMailAddress>
               (EMailAddress.EMailAddrTyp.Cc, msg.Cc);

         //+ Und in der Liste erfassen, wenn sie noch unbekannt sind
         EMailAddress.AddToList
            (alleMails
             , thisEMails
             , true);
      }

      //++ BCC
      if (msg.Bcc != null) {
         //+ Die CC Adressen analysieren
         List<EMailAddress> thisEMails
            = EMailAddress.Parse_MimeKit_InternetAddressList<EMailAddress>
               (EMailAddress.EMailAddrTyp.Bcc, msg.Bcc);

         //+ Und in der Liste erfassen, wenn sie noch unbekannt sind
         EMailAddress.AddToList
            (alleMails
             , thisEMails
             , true);
      }

      //++ To
      if (msg.To != null) {
         //+ Die CC Adressen analysieren
         List<EMailAddress> thisEMails
            = EMailAddress.Parse_MimeKit_InternetAddressList<EMailAddress>
               (EMailAddress.EMailAddrTyp.To, msg.To);

         //+ Und in der Liste erfassen, wenn sie noch unbekannt sind
         EMailAddress.AddToList
            (alleMails
             , thisEMails
             , true);
      }

      //++ From
      if (msg.From != null) {
         //+ Die CC Adressen analysieren
         List<EMailAddress> thisEMails
            = EMailAddress.Parse_MimeKit_InternetAddressList<EMailAddress>(EMailAddress.EMailAddrTyp.From, msg.From);

         //+ Und in der Liste erfassen, wenn sie noch unbekannt sind
         EMailAddress.AddToList
            (alleMails
             , thisEMails
             , true);
      }

      //++ Sender
      if (msg.Sender != null) {
         //+ Die CC Adressen analysieren
         EMailAddress thisEMail
            = EMailAddress.Parse_MimeKit_MailboxAddress<EMailAddress>
               (EMailAddress.EMailAddrTyp.Sender, msg.Sender);

         //+ Und in der Liste erfassen, wenn sie noch unbekannt sind
         EMailAddress.AddToList
            (alleMails
             , thisEMail
             , true);
      }

      // Alle gefundenen E-Mails analysieren und die Personen-Daten zurückgeben
      return alleMails;
   }


}
