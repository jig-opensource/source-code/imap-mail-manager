using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using jig.Tools;
using jig.Tools.String;


namespace ImapMailManager.Mail.Anonymisierer;

/// <summary>
/// Hilfsfunktionen für die Anonymisierung
/// </summary>
public class Anonymizer_Tools {

   /// <summary>
   /// Teilt text in Worte auf und erzeugt daraus Worte, die für die Anonymisierung genützt werden
   /// 
   /// text wird folgendermassen in Worte aufgeteilt:
   /// - "Hans Muster" > "Hans", "Muster"
   /// - "HansMuster" > "Hans", "Muster"
   /// 
   /// </summary>
   /// <param name="wort"></param>
   /// <param name="anonymizerWordMinLen">
   /// Die Mindestlänge, die Worte haben, damit wir sie anonymisieren
   /// Zu kurze Begriffe wollen wir nicht anonymisieren, weil sonst zu viel Text anonymisiert würde 
   /// </param>
   public static List<string>? TokenizeString_ForAnonymizer(string? text, int anonymizerWordMinLen) {
      if (!text.ØHasValue()) { return null; }
      
      //+ text in Worte trennen
      List<string> alleWorteGesammelt = new List<string>();
      //++ Normaler Split
      string[]     textSplitWords       = text.Split(new[] { '.', '-', '_' });
      // Das erste Resultat sammeln
      alleWorteGesammelt.AddRange(textSplitWords);
      // Jedes textSplitWord auf CamelCase prüfen
      foreach (var word in textSplitWords) {
         // Alle Camel Case Splits ergänzen
         alleWorteGesammelt.AddRange(word.ØSplitCamelCase().Split(' '));
      }

      //+ Für jedes gefundene distinct Wort die Schreibvarianten generieren
      List<string> wortliste = new List<string>();
      foreach (string wort in alleWorteGesammelt.Where(x => x.ØHasValue()).Distinct()) {
         wortliste.AddRange(CalculateWords_ForAnonymizer(wort, anonymizerWordMinLen).ØOrEmptyIfNull());
      }

      return wortliste.Distinct().ToList();
   }


   /// <summary>
   /// Berechnet für Wort die Schreibvarianten für die Anonymisierung:
   /// - Das Wort selber
   /// - Codiert mit: HttpUtility.HtmlEncode » Sonderzeichen als Unicode
   /// - Codiert mit: WebUtility.HtmlEncode » Sonderzeichen als &auml;
   /// - Codiert ae > ä, ue > ü, oe > ö
   /// - Codiert ä > ae, ü > ue, ö > oe
   /// </summary>
   /// <param name="wort"></param>
   /// <param name="anonymizerWordMinLen"></param>
   /// <returns></returns>
   public static List<string>? CalculateWords_ForAnonymizer(string wort, int anonymizerWordMinLen) {
      // Zu kurze Worte ignorieren
      if (!wort.ØHasValue()) { return null; }

      if (wort.Length < anonymizerWordMinLen) { return null; }

      List<string>? wortListe = new List<string>();

      //+ Das Wort selber zufügen
      wortListe.ØAppendItem_IfMissing(wort);

      //+ Das Wort HTML Encoded zufügen
      var encoding1 = HttpUtility.HtmlEncode(wort);
      if (!wort.ØEqualsIgnoreCase(encoding1) && encoding1.Length >= anonymizerWordMinLen) {
         wortListe.ØAppendItem_IfMissing(encoding1);
      }

      var encoding2 = WebUtility.HtmlEncode(wort);
      if (!wort.ØEqualsIgnoreCase(encoding2) && encoding2.Length >= anonymizerWordMinLen) {
         wortListe.ØAppendItem_IfMissing(encoding2);
      }

      //+ Die E-Mail Sonderzeichen auflösen
      //++ ae > ä, ue > ü, oe > ö
      var wortSonderzeichen1 = wort.Replace
                                   ("ae"
                                    , "ä"
                                    , StringComparison.OrdinalIgnoreCase)
                                  .Replace
                                      ("oe"
                                       , "ö"
                                       , StringComparison.OrdinalIgnoreCase)
                                  .Replace
                                      ("ue"
                                       , "ü"
                                       , StringComparison.OrdinalIgnoreCase);

      if (!wort.ØEqualsIgnoreCase(wortSonderzeichen1) && wortSonderzeichen1.Length >= anonymizerWordMinLen) {
         wortListe.ØAppendItem_IfMissing(wortSonderzeichen1);
      }
      //++ ä > ae, ü > ue, ö > oe
      var wortSonderzeichen2 = wort.Replace
                                   ("ä"
                                    , "ae"
                                    , StringComparison.OrdinalIgnoreCase)
                                  .Replace
                                      ("ö"
                                       , "oe"
                                       , StringComparison.OrdinalIgnoreCase)
                                  .Replace
                                      ("ü"
                                       , "ue"
                                       , StringComparison.OrdinalIgnoreCase);

      if (!wort.ØEqualsIgnoreCase(wortSonderzeichen2) && wortSonderzeichen2.Length >= anonymizerWordMinLen) {
         wortListe.ØAppendItem_IfMissing(wortSonderzeichen2);
      }

      return wortListe;
   }

}
