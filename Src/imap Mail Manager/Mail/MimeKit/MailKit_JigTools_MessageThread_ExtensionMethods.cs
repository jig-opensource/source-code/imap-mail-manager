using System.Collections.Generic;
using MailKit;
using MailKit.Search;


namespace ImapMailManager.Mail.MimeKit;

/// <summary>
/// Extension Methods für MailKit.MessageThread
/// </summary>
public static class MailKit_JigTools_MessageThread_ExtensionMethods {

   /// <summary>
   /// Berechnet aus den messageSummaries die Mail-Threads  
   /// </summary>
   /// <param name="messageSummaries"></param>
   /// <returns></returns>
   public static IList<MessageThread> ØGetMessageSummaryThreads(this List<IMessageSummary>? messageSummaries) {
      if (messageSummaries == null) { return new List<MessageThread>();}

      // Absteigend nach EMpfangsdatum sortiert
      var orderBy = new[] { OrderBy.ReverseDate };
      return messageSummaries.Thread
         (ThreadingAlgorithm.References
          , orderBy);
   }
   

}
