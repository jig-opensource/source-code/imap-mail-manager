using MailKit;


namespace ImapMailManager.Mail.MimeKit; 

public static class ExtensionMethods_MimeKit {

   /// <summary>
   /// Den Mailbox Ordner Readonly öffnen
   /// </summary>
   /// <param name="mbxFolder"></param>
   public static void ØOpen_ReadAccess(
      this IMailFolder? mbxFolder) {
      if (mbxFolder != null) {
         if (!mbxFolder.IsOpen) {
            mbxFolder.Open(FolderAccess.ReadOnly);
         }
      }
   }

   /// <summary>
   /// Den Mailbox Ordner RW öffnen
   /// </summary>
   /// <param name="mbxFolder"></param>
   public static void ØOpen_ReadWriteAccess(
      this IMailFolder? mbxFolder) {
      if (mbxFolder != null) {
         if (!mbxFolder.IsOpen) {
            //+ Sicherstellen, dass der Ordner rw offen ist 
            mbxFolder.Open(FolderAccess.ReadWrite);
         }
         else {
            if (mbxFolder.Access != FolderAccess.ReadWrite) {
               mbxFolder.Open(FolderAccess.ReadWrite);
            }
         }
      }  
   }

}
