using System.Linq;
using DocumentFormat.OpenXml.Office2010.CustomUI;
using jig.Tools.String;
using MailKit;


namespace ImapMailManager.Mail.MimeKit;

/// <summary>
/// Extension Methods für IMailFolder 
/// </summary>
public static class MimeKit_IMailFolder_ExtensionMethods {

   /// <summary>
   /// Erzeugt im Root Folder der ImapServerRuntimeConfig Unterordner
   ///
   /// !Ex
   ///   var res = imapSrvRuntimeConfig.ØGetOrCreateFolder(@"TomTom\123\abc");
   /// 
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   /// <param name="subFolder"></param>
   /// <returns></returns>
   public static IMailFolder ØGetOrCreateFolder(this ImapServerRuntimeConfig imapServerRuntimeConfig, string subFolder) {
      return ØGetOrCreateFolder(imapServerRuntimeConfig.RootFolder, subFolder);
   }


   /// <summary>
   /// Erzeugt eine Ordnerstruktur in der Mailbox unterhalb von thisFolder
   /// </summary>
   /// <param name="thisFolder"></param>
   /// <param name="subFolder"></param>
   /// <returns></returns>
   public static IMailFolder ØGetOrCreateFolder(this IMailFolder thisFolder, string subFolder) {
      var subDirs = subFolder.Split(new[] { '/', '\\' });

      //+ Jede Ebene suchen und allenfalls erzeugen
      var thisODir = thisFolder;
      foreach (var subDir in subDirs) {
         //+ Die Unterordner holen
         var thisDirSubDirs = thisODir.GetSubfolders();

         //+ Den gesuchten Unterordner holen
         IMailFolder? oSubDir = thisDirSubDirs.FirstOrDefault
            (f => f.Name.ØEqualsIgnoreCase(subDir));

         //+++ Der Ordner fehlt: Erzeugen
         if (oSubDir == null) {
            // Ordner erzeugen. False: In diesem Ordner keine Mails speichern
            // !M http://www.mimekit.net/docs/html/M_MailKit_Net_Imap_ImapFolder_Create.htm
            oSubDir = thisODir.Create(subDir, true);
            oSubDir.Subscribe();
         }

         thisODir = oSubDir;
      }

      // Das gesuchte Verzeichnis zurückgeben
      return thisODir;
   }

}
