using MailKit;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator; 

/// <summary>
/// Interface zum Iterieren der ganzen Mailbox
/// </summary>
public interface IMailboxIterator {

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }
   
   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }
   
   #endregion Props

   
   /// <summary>
   /// Funktion zum Initialisieren des Prozesses 
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}
   
   /// <summary>
   /// Wird am Ende des Prozesses aufgerufen
   /// </summary>
   public void ProcessMailboxFolder_Done() {}

   /// <summary>
   /// Wird für jeden Mbx Ordner aufgerufen
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force"></param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {}

}
