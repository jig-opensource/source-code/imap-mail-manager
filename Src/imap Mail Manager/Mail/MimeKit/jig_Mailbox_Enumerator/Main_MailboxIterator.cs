using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using ImapMailManager.Config;
using ImapMailManager.Properties;
using jig.WPF.SerilogViewer;
using MailKit;
using MailKit.Net.Imap;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator;

/// <summary>
/// Durchläuft alle Ordner einer Mailbox
/// und ruft für die Verarbeitung Funktionen auf
/// </summary>
public class Main_MailboxIterator {

   /// <summary>
   /// Durchläuft alle Ordner einer Mailbox
   /// und ruft für die Verarbeitung Funktionen auf
   /// </summary>
   public static void IterateMailbopxFolders(
      Sensitive_AppSettings.CfgImapServer       cfgImapServer
      , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred
      , IMailboxIterator                        iMailboxIterator
      , bool                                    force
      , SerilogViewer                           serilogViewer) {

      //+ Verbinden
      ImapServerRuntimeConfig imapSrvRuntimeConfig = ImapServerRuntimeConfig.ConnectImap
         (cfgImapServerCred.IsProductiveMailbox ? "Prod" : "Test"
          , cfgImapServer
          , cfgImapServerCred);

      imapSrvRuntimeConfig.Connect();

      //+ Den Iterator initialisieren
      if (!iMailboxIterator.MailboxIterator_Dont_ProcessInit.HasValue
          || !iMailboxIterator.MailboxIterator_Dont_ProcessInit.Value) {
         iMailboxIterator.Init_Process(imapSrvRuntimeConfig);
      }

      //+ Alle PersonalNamespaces durchlaufen
      foreach (var folderNamespace in imapSrvRuntimeConfig.ImapClient.PersonalNamespaces) {
         var thisFolder = imapSrvRuntimeConfig.ImapClient.GetFolder(folderNamespace);

         var thisNamespacePath = Path.Join
            (folderNamespace.Path, imapSrvRuntimeConfig.IMapDirSeperator);

         // 🚩 Debug
         // Debug.WriteLine($"PersonalNamespace:  {thisNamespacePath}");

         //+ Alle Ordner im NameSpace verarbeiten
         foreach (IMailFolder thisMailFolder in ReadImapMailbox.EnumFolder(thisFolder)) {
            var thisFolderPath = Path.Join
               (thisMailFolder.FullName
                , imapSrvRuntimeConfig
                  .IMapDirSeperator);

            // 🚩 Debug
            // Debug.WriteLine($"Lese: {thisFolderPath}");

            // Nur IMAP-ordner, die existieren und die ausgewählt werden können
            if (!thisMailFolder.Attributes.HasFlag(FolderAttributes.NonExistent)
                && !thisMailFolder.Attributes.HasFlag(FolderAttributes.NoSelect)) {

               var exists      = thisMailFolder.Exists;
               var hasChildren = thisMailFolder.Attributes.HasFlag(FolderAttributes.HasChildren);

               // Vom IMAP Ordner die statistischen infos erfassen
               try {
                  // Wenn der Befehl eine Exception wirft, rufen wir die iteratorFunc nicht auf 
                  thisMailFolder.Status(StatusItems.Unread | StatusItems.Count | StatusItems.Size);

                  var thisSize        = thisMailFolder.Size ?? 0;
                  var thisCount       = thisMailFolder.Count;
                  var thisUnreadCount = thisMailFolder.Unread;

                  //+ Die Iterator-Funktion ausführen
                  iMailboxIterator.ProcessMailboxFolder
                     (imapSrvRuntimeConfig
                      , thisMailFolder
                      , force);

               } catch (ImapCommandException ex) {
                  Debug.WriteLine($"  » {ex.ResponseText}");
                  int stopper = 1;
                  /*
                                    inbox.AddFlags(uid, MessageFlags.Deleted, false);
                  */
               } catch (ServiceNotConnectedException ex) {
                  // Abbruch, der Server hat die Verbindung geschlossen
                  MessageBox.Show
                     ("Der IMAP-Server hat die Verbindung unerbrochen"
                      , "Abbruch!"
                      , MessageBoxButton.OK
                      , MessageBoxImage.Error);

                  break;
               }

            }
         }

         // 🚩 Debug
         // Klappt:
         // Debug.WriteLine(hrchyImapMbox.PrintTree());

         // serilogViewer.LogInformation($"thisPath: {thisFolderPath} • folder.FullName: {folder.FullName}");
      }

      //+ Die Abschluss-Funktion ausführen
      if (!iMailboxIterator.MailboxIterator_Dont_ProcessDone.HasValue
          || !iMailboxIterator.MailboxIterator_Dont_ProcessDone.Value) {
         iMailboxIterator.ProcessMailboxFolder_Done();
      }

      Debug.WriteLine($"MailboxIterator fertig: {iMailboxIterator.GetType().Name}");

      //+ Trennen
      imapSrvRuntimeConfig.ImapClient.Disconnect(true);
   }

}
