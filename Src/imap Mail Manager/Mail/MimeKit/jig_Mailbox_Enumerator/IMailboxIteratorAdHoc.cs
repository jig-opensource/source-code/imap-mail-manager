namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator; 

/// <summary>
/// Nur ein Interface zur Markierung
/// Wird Iterator-Klassen zugeordnet, die AdHoc ausgeführt werden
/// </summary>
public interface IMailboxIteratorAdHoc {}
