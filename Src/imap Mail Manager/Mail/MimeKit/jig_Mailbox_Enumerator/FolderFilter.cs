using System;
using System.Collections.Generic;
using MailKit;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator;

/// <summary>
/// Dient als genereller Mailbox Ordner Filter
/// </summary>
public class FolderFilter : IMailbox_FolderFilter {

   #region IMailbox_FolderFilter

   /// <summary>
   /// Die Default Methode im Interface:
   ///   StopFolderProcessing_BlackWhiteList
   /// muss ins Interface gecastet werden, damit sie aufgerufen werden kann:
   /// !KH https://stackoverflow.com/a/57763254/4795779
   /// Dies hier ist ein Prop als Cast zum Interface, damit der Aufruf hübscher gemacht werden kann 
   /// </summary>
   private IMailbox_FolderFilter iMailbox_FolderFilter => (IMailbox_FolderFilter)this;


   /// <summary>
   /// Liefert True, wenn mbxFolder nicht verarbeitet werden soll
   ///
   /// ‼ Die Funktion hat ein _ als Suffix,
   /// um sie vom Interface mit dem sonst gleichen Funktionsnamen zu unterscheiden
   /// 
   /// </summary>
   /// <param name="mbxFolder"></param>
   /// <returns></returns>
   public bool StopFolderProcessing_BlackWhiteList_(IMailFolder mbxFolder) {
      return iMailbox_FolderFilter.StopFolderProcessing_BlackWhiteList(mbxFolder);
   }

   #endregion IMailbox_FolderFilter

   #region Props

   /// <summary>
   /// Die Liste der Whitelist-Ordner
   /// Hat Priorität gegenüber der Blacklist:
   /// Wenn die Whitelist definiert ist, dann wird die Blacklist ignoriert
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist { get; set; }
   /// <summary>
   /// Wenn die Whitelist definiert ist, 
   /// dann werden diese Order nicht verarbeitet
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist_Except_StopOn { get; set; }

   /// <summary>
   /// Die Liste der Blacklist-Ordner
   /// Wird nur angewendet, wenn die Whitelist nicht definiert ist
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Blacklist { get; set; }

   #endregion Props

   #region Konstruktoren

   public FolderFilter(
      Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>?   mbxFolder_Whitelist               = null
      , Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? mbxFolder_Whitelist_Except_StopOn = null
      , Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? mbxFolder_Blacklist               = null) {
      MbxFolder_Whitelist               = mbxFolder_Whitelist;
      MbxFolder_Whitelist_Except_StopOn = mbxFolder_Whitelist_Except_StopOn;
      MbxFolder_Blacklist               = mbxFolder_Blacklist;
   }

   #endregion Konstruktoren

}
