using System;
using System.Collections.Generic;
using jig.Tools;
using MailKit;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator;

/// <summary>
/// Interface zum Filtern von IMAP-Foldern,
/// dient als Ergänzung zu IMailboxIterator
/// oder kann allgemein genützt werden
/// </summary>
public interface IMailbox_FolderFilter {

   /// <summary>
   /// Enum, die die Art des Ordnernamen-Vergleichs definiert
   /// </summary>
   public enum FolderCompare {

      /// <summary>
      /// Der Suchstring hat einen Regex
      /// </summary>
      RgxMatch,
      /// <summary>
      /// Der Suchstring muss exakt mit einem Black / White Listenelement übereinstimmen 
      /// </summary>
      Equals,
      /// <summary>
      /// Ein Black / White Listenelement beinhaltet als Substring das gesuchte Element
      /// </summary>
      Contains,
      /// <summary>
      /// Ein Black / White Listenelement startet mit dem gesuchten Element
      /// </summary>
      StartsWith, 
      
      /// <summary>
      /// Filter nach MailKit.FolderAttributes
      /// </summary>
      FolderAttributes

   }


   #region Props

   /// <summary>
   /// Die Liste der Whitelist-Ordner
   /// Hat Priorität gegenüber der Blacklist:
   /// Wenn die Whitelist definiert ist, dann wird die Blacklist ignoriert
   /// </summary>
   public Tuple<FolderCompare, List<string>?>? MbxFolder_Whitelist { get; } 

   /// <summary>
   /// Wenn die Whitelist definiert ist, 
   /// dann werden diese Order nicht verarbeitet
   /// </summary>
   public Tuple<FolderCompare, List<string>?>? MbxFolder_Whitelist_Except_StopOn { get; } 

   /// <summary>
   /// Die Liste der Blacklist-Ordner
   /// Wird nur angewendet, wenn die Whitelist nicht definiert ist
   /// </summary>
   public Tuple<FolderCompare, List<string>?>? MbxFolder_Blacklist { get; } 

   #endregion Props


   /// <summary>
   /// Prüft, ob der Ordnername einen Match in der Black- oder Whitelist hat 
   /// Wenn die Whitelist definiert ist, dann wird die Blacklist ignoriert
   /// </summary>
   /// <param name="mbxFolder"></param>
   /// <returns>
   /// True: Die Verarbeitung des mbxFolder abbrechen
   /// </returns>
   internal bool StopFolderProcessing_BlackWhiteList(IMailFolder mbxFolder) {
      var hasWhiteList               = (MbxFolder_Whitelist?.Item2?.Count ??0) > 0;
      var hasWhiteList_Except_StopOn = (MbxFolder_Whitelist_Except_StopOn?.Item2?.Count ?? 0) > 0;

      //+ Wenn wir eine Ordner-Whitelist haben, dann mit dieser arbeiten
      if (hasWhiteList || hasWhiteList_Except_StopOn) {
         //+ Zuerst die Whitelist_except verarbeiten
         // Abbrechen, wenn MbxFolder_Whitelist_Except passt 
         if (hasWhiteList_Except_StopOn) {
            switch (MbxFolder_Whitelist_Except_StopOn!.Item1) {
               case FolderCompare.RgxMatch:
                  if (MbxFolder_Whitelist_Except_StopOn.Item2.ØRgxMatchWithAnyItemInList(mbxFolder.FullName)) {
                     return true;
                  }
                  break;

               case FolderCompare.Equals:
                  if (MbxFolder_Whitelist_Except_StopOn.Item2.ØEqualsWithAnyItemInList(mbxFolder.FullName)) {
                     return true;
                  }
                  break;

               case FolderCompare.StartsWith:
                  if (MbxFolder_Whitelist_Except_StopOn.Item2.ØStartsWithAnyItemInList(mbxFolder.FullName)) {
                     return true;
                  }
                  break;

               case FolderCompare.Contains:
                  if (MbxFolder_Whitelist_Except_StopOn.Item2.ØListItem_ContainsSubstring(mbxFolder.FullName)) {
                     return true;
                  }
                  break;

               case FolderCompare.FolderAttributes:
                  // ToDo 🟥 Logik implementieren
                  break;

               default:
                  throw new ArgumentOutOfRangeException(nameof(FolderCompare), MbxFolder_Whitelist_Except_StopOn
                                                           !.Item1, null);
            }
         }

         //+ Wenn wir bis hier kommen, dann prüfen, ob die Whitelist passt
         //+ Ohne Whitelist verarbeiten wir alle Ordner
         if (!hasWhiteList) {
            return false;
         }
         //++ Whitelist definiert: Wenn der Ordner nicht passt, dann überspringen
         switch (MbxFolder_Whitelist!.Item1) {
            case FolderCompare.RgxMatch:
               if (!MbxFolder_Whitelist.Item2.ØRgxMatchWithAnyItemInList(mbxFolder.FullName)) {
                  return true;
               }

               break;

            case FolderCompare.Equals:
               if (!MbxFolder_Whitelist.Item2.ØEqualsWithAnyItemInList(mbxFolder.FullName)) {
                  return true;
               }

               break;

            case FolderCompare.StartsWith:
               if (!MbxFolder_Whitelist.Item2.ØStartsWithAnyItemInList(mbxFolder.FullName)) {
                  return true;
               }

               break;

            case FolderCompare.Contains:
               if (!MbxFolder_Whitelist.Item2.ØListItem_ContainsSubstring(mbxFolder.FullName)) {
                  return true;
               }

               break;

            case FolderCompare.FolderAttributes:
               // ToDo 🟥 Logik implementieren
               break;

            default:
               throw new ArgumentOutOfRangeException(nameof(FolderCompare), MbxFolder_Whitelist!.Item2, null);
         }

      }
      else {
         //+ Sonst die Blacklist nützen
         // Die Blacklisted Ordner überspringen
         if ((MbxFolder_Blacklist?.Item2?.Count ?? 0) > 0) {
            switch (MbxFolder_Blacklist!.Item1) {
               case FolderCompare.RgxMatch:
                  if (MbxFolder_Blacklist.Item2.ØRgxMatchWithAnyItemInList(mbxFolder.FullName)) {
                     return true;
                  }

                  break;

               case FolderCompare.Equals:
                  if (MbxFolder_Blacklist.Item2.ØEqualsWithAnyItemInList(mbxFolder.FullName)) {
                     return true;
                  }

                  break;

               case FolderCompare.StartsWith:
                  if (MbxFolder_Blacklist.Item2.ØStartsWithAnyItemInList(mbxFolder.FullName)) {
                     return true;
                  }

                  break;

               case FolderCompare.Contains:
                  if (MbxFolder_Blacklist.Item2.ØListItem_ContainsSubstring(mbxFolder.FullName)) {
                     return true;
                  }

                  break;

               case FolderCompare.FolderAttributes:
                  // ToDo 🟥 Logik implementieren
                  break;

               default:
                  throw new ArgumentOutOfRangeException(nameof(FolderCompare), MbxFolder_Blacklist.Item1, null);
            }
         }
      }

      // Den Ordner verarbeiten
      return false;
   }

}
