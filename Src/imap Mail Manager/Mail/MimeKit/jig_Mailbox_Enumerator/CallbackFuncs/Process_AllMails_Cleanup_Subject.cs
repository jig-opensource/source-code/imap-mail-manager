using System;
using System.Collections.Generic;
using System.Diagnostics;
using ImapMailManager.BO;
using jig.Tools;
using jig.Tools.String;
using MailKit;
using MailKit.Search;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Bereinigt alle E-Mail Subjects:
/// - Bereinigt die Subject-Präfixe RE: FWD, etc.
/// - Setzt bei fremdsprachigen Mails den 
/// - Setzt bei fremdsprachigen Mails den das Präfix mit der Sprache 
/// </summary>
public class Process_AllMails_Cleanup_Subject : IMailboxIterator, IMailbox_FolderFilter {

   /// <summary>
   /// Die Default Methode im Interface:
   ///   StopFolderProcessing_BlackWhiteList
   /// muss ins Interface gecastet werden, damit sie aufgerufen werden kann:
   /// !KH https://stackoverflow.com/a/57763254/4795779
   /// Dies hier ist ein Prop als Cast zum Interface, damit der Aufruf hübscher gemacht werden kann 
   /// </summary>
   private IMailbox_FolderFilter iMailbox_FolderFilter => (IMailbox_FolderFilter)this;

   //‼ Config

   #region Config

   /// <summary>
   /// Wenn > 0, definiert der Wert, wie viele Mails pro Ordner max. verarbeitet werden
   /// </summary>
   private const int ProcessMaxMailsPerFolder = -1;

   #endregion Config

   #region IMailbox_FolderFilter White / Blacklist

   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist { get; } = new
      (IMailbox_FolderFilter.FolderCompare.RgxMatch
       , new List<string> {
      ExtensionMethods_RegEx.ØWildcardToRegex(@"*INBOX*")
       });
   
   /// <summary>
   /// Wenn die Whitelist definiert ist, 
   /// dann werden diese Order nicht verarbeitet
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist_Except_StopOn { get; } = new
      (IMailbox_FolderFilter.FolderCompare.RgxMatch
       , new List<string> {
      // ExtensionMethods_RegEx.ØWildcardToRegex(@"*INBOX*")
      // ExtensionMethods_RegEx.ØWildcardToRegex(@"*Entwürfe*")
       });

   /// <summary>
   /// Die Mailbox-Ordnernamen, die wir nicht verarbeiten
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Blacklist { get; } = new
      (IMailbox_FolderFilter.FolderCompare.RgxMatch
       , new List<string> {
          "Trash", "Gelöschte Elemente", "Junk-E-Mail", "Junk"
       });

   #endregion IMailbox_FolderFilter White / Blacklist


   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props


   /// <summary>
   /// Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}

   /// <summary>
   /// Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      int done = 1;

      int stopper = 2;

      /*
            StringBuilder sb = new StringBuilder();
      
            foreach (var subject in MailSubjects.OrderBy(x => x)) {
               sb.AppendLine(subject);
            }
      
            // 🚩 Debug: Ins ClipBoard kopieren
            ClipboardService.SetText(sb.ToString());
            
            Debug.WriteLine(sb.ToString());
      */

   }


   /// <summary>
   /// Verarbeitet den übergebenen Ordner
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {

      // Nötig, um den Inhalt zu erfahren
      mbxFolder.Open(FolderAccess.ReadOnly);
      var anzMails = mbxFolder.Count;
      Debug.WriteLine($"{mbxFolder.FullName}");

      //+ Die Blacklisted Ordner überspringen
      if (iMailbox_FolderFilter.StopFolderProcessing_BlackWhiteList(mbxFolder)) {
         Debug.WriteLine($"  » skipped");

         return;
      }

      //+ Alle Mails suchen
      var uids = mbxFolder.Search(SearchQuery.All);

      // 🚩 Debug.WriteLine($"  Anzahl: {uids.Count}");

      // Sortieren: zuerst die neuste E-Mail
      // !Q https://stackoverflow.com/a/58580564/4795779						
      // var messages = imapFolder.Fetch(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.InternalDate).ToList();
      // messages.Sort(new OrderBy[] { OrderBy.ReverseDate });

      
      //+ Die Message Summaries holen
      IList<IMessageSummary> messageSummaries = mbxFolder.Fetch
         (uids
          , MessageSummaryItems.Envelope

            // | MessageSummaryItems.BodyStructure
            // | MessageSummaryItems.Size
            // https://github.com/jstedfast/MailKit/issues/1254#issuecomment-1277881417
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Headers
            // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
            | MessageSummaryItems.InternalDate
            | MessageSummaryItems.References
            | MessageSummaryItems.Flags);

      //+ Alle gefundenen Mails bearbeiten
      int msgCnt = uids.Count;

      foreach (var msgSummary in messageSummaries) {
         // Wenn wir die vorgesehene Anzahl erreicht haben, überspringen
         if (ProcessMaxMailsPerFolder > 0
             && uids.Count - msgCnt > ProcessMaxMailsPerFolder) {
            break;
         }

         //‼ 🚩 Debug
         if (msgCnt--%20 == 0 || msgCnt == uids.Count - 1) { Debug.WriteLine($"    ↻ {msgCnt,4} {msgSummary.Envelope.Subject}"); }

         var srchSubject = @"";
         srchSubject = @"";
         srchSubject = @"⭕ENG Undelivered Anonymisiert Returned to Sender";
         srchSubject = @"Undelivered Anonymisiert Returned to Sender";

         if ((srchSubject.ØHasValue() && msgSummary.Envelope.Subject == null)
             || (srchSubject.ØHasValue()
                 && msgSummary.Envelope.Subject != null
                 && !msgSummary.Envelope.Subject.Contains(srchSubject))) {
            continue;
         }

         //++ Obj erzeugen
         BO_Mail bo = new BO_Mail
            (imapSrvRuntimeConfig
             , mbxFolder
             , msgSummary);

         // Alle App-Spezifischen Mails Überspringen: ✅ Dok:
         if (bo.IsAppSpecificMail) { continue; }


         if (!bo.HasHeaderSet_Metadaten) {
            // Debug.WriteLine($"    » Skipping: {msgSummary.Envelope.Subject}");
            continue;
         }

         Debug.WriteLine($"    » Clean Subject: {msgCnt,4} {msgSummary.Envelope.Subject}");

         bo.Cleanup_Subject
            (true
             , true
             , true);

      }
   }

   #endregion Iterator-Funktionen

}
