using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ImapMailManager.BO;
using ImapMailManager.Config;
using jig.Tools;
using jig.Tools.String;
using MailKit;
using MailKit.Search;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Kategorisiert alle E-Mails, d.h. setzt den Msg Header
/// AppMailHeaderType.MailKategorie 
/// </summary>
public class Process_AllMails_EMail_Kategorisierung : IMailboxIterator, IMailbox_FolderFilter {

   //‼ Config

   #region Config

   /// <summary>
   /// Wenn true, werden *alle* Mails in Unterverzeichnisse von
   /// \MailKategorie\ abgelegt 
   /// </summary>
   private static readonly bool Move_Klassifizierte_EMails = true;

   // In diesen Ordner werden die Klassifizierten E-Mails abgelegt
   private static readonly string AblageOrdner_MailKategorie = "MailKategorie";

   /// <summary>
   /// Wenn > 0, definiert der Wert, wie viele Mails pro Ordner max. verarbeitet werden
   /// </summary>
   private static readonly int ProcessMaxMailsPerFolder = -1;
   /// <summary>
   /// Alle wieviele E-Mails soll eine Progress-Nachricht angezeigt werden?
   /// </summary>
   private static readonly int PrintMailProgressModulo = 10;

   #endregion Config

   #region IMailbox_FolderFilter White / Blacklist

   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist { get; } = new
      (IMailbox_FolderFilter.FolderCompare.RgxMatch
       , new List<string> {
          // ExtensionMethods_RegEx.ØWildcardToRegex(@"*INBOX*")
          // ExtensionMethods_RegEx.ØWildcardToRegex(@"*Entwürfe*")
       });

   /// <summary>
   /// Wenn die Whitelist definiert ist, 
   /// dann werden diese Order nicht verarbeitet
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist_Except_StopOn { get; } = new
      (IMailbox_FolderFilter.FolderCompare.RgxMatch
       , new List<string> {
          // ExtensionMethods_RegEx.ØWildcardToRegex(@"*INBOX*")
          // ExtensionMethods_RegEx.ØWildcardToRegex(@"*Entwürfe*")
       });

   /// <summary>
   /// Die Mailbox-Ordnernamen, die wir nicht verarbeiten
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Blacklist { get; } = new
      (IMailbox_FolderFilter.FolderCompare.RgxMatch
       , new List<string> {
          // "Trash", "Gelöschte Elemente", "Junk-E-Mail", "Junk"
          ExtensionMethods_RegEx.ØWildcardToRegex($"*{AblageOrdner_MailKategorie}")
       });

   #endregion IMailbox_FolderFilter White / Blacklist

   private Dictionary<ConfigMail_MsgHeader.MailKategorie, int> _mailKategorieStatistik = new();

   /// <summary>
   /// Die Default Methode im Interface:
   ///   StopFolderProcessing_BlackWhiteList
   /// muss ins Interface gecastet werden, damit sie aufgerufen werden kann:
   /// !KH https://stackoverflow.com/a/57763254/4795779
   /// Dies hier ist ein Prop als Cast zum Interface, damit der Aufruf hübscher gemacht werden kann 
   /// </summary>
   private IMailbox_FolderFilter iMailbox_FolderFilter => (IMailbox_FolderFilter)this;

   Dictionary<ConfigMail_MsgHeader.MailKategorie, IMailFolder> mailKatordner = new();

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props


   /// <summary>
   /// » Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {
      //+ Init _mailKategorieStatistik
      foreach (ConfigMail_MsgHeader.MailKategorie key in Enum.GetValues<ConfigMail_MsgHeader.MailKategorie>()) {
         if (!_mailKategorieStatistik.ContainsKey(key)) {
            _mailKategorieStatistik.Add(key, 0);
         }
      }

      if (Move_Klassifizierte_EMails) {
         //+ Sicherstellen, dass wir die Ordner haben
         IMailFolder rootFolder = imapServerRuntimeConfig.RootFolder;

         //++ Alle Unterordner holen
         var rootSubfolders = rootFolder.GetSubfolders();

         //++ Haben wir den Ordner MailKategorie?
         IMailFolder? folder_MailKategorie = rootSubfolders.FirstOrDefault
            (f => f.Name.ØEqualsIgnoreCase(AblageOrdner_MailKategorie));

         //++ Nein: Erzeugen
         if (folder_MailKategorie == null) {
            // Ordner erzeugen. False: In diesem Ordner keine Mails speichern
            // !M http://www.mimekit.net/docs/html/M_MailKit_Net_Imap_ImapFolder_Create.htm
            folder_MailKategorie = rootFolder.Create(AblageOrdner_MailKategorie, false);
            folder_MailKategorie.Subscribe();
         }

         //+ Für jede Mailkategoerie einen Unterordner erzeugen  
         foreach (var mailKategorie in Enum.GetValues<ConfigMail_MsgHeader.MailKategorie>()) {
            IMailFolder? mailKatFolder = rootSubfolders.FirstOrDefault(f => f.Name.ØEqualsIgnoreCase(mailKategorie.ToString()));

            if (mailKatFolder == null) {
               // Ordner erzeugen. True: In diesem Ordner Mails speichern
               // !M http://www.mimekit.net/docs/html/M_MailKit_Net_Imap_ImapFolder_Create.htm
               mailKatFolder = folder_MailKategorie.Create(mailKategorie.ToString(), true);
               mailKatFolder.Subscribe();
            }

            mailKatordner.Add(mailKategorie, mailKatFolder!);
         }
      }

   }


   /// <summary>
   /// » Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      int done = 1;

      Debug.WriteLine("Mail Kategorie Statistik:");

      foreach (var kvp in _mailKategorieStatistik) {
         Debug.WriteLine($"{kvp.Key,26}: {kvp.Value}");
      }

      int stopper = 2;

      /*
            StringBuilder sb = new StringBuilder();
      
            foreach (var subject in MailSubjects.OrderBy(x => x)) {
               sb.AppendLine(subject);
            }
      
            // 🚩 Debug: Ins ClipBoard kopieren
            ClipboardService.SetText(sb.ToString());
            
            Debug.WriteLine(sb.ToString());
      */

   }


   /// <summary>
   /// Verarbeitet den übergebenen Ordner
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {

      // Nötig, um den Inhalt zu erfahren
      mbxFolder.Open(FolderAccess.ReadOnly);
      var anzMails = mbxFolder.Count;
      Debug.WriteLine($"{mbxFolder.FullName}");

      // var specialFolders = imapSrvRuntimeConfig.SpecialFolders;

      //» Ist der Ordner ein Special Folder?
      //» Die Flags sind jeweils für die englischen iund deutschen Ordner richtig gesetzt
      //!M http://www.mimekit.net/docs/html/T_MailKit_FolderAttributes.htm
      var isFldr_Archive   = mbxFolder.Attributes.HasFlag(FolderAttributes.Archive);
      var isFldr_Drafts    = mbxFolder.Attributes.HasFlag(FolderAttributes.Drafts);
      var isFldr_Flagged   = mbxFolder.Attributes.HasFlag(FolderAttributes.Flagged);
      var isFldr_Important = mbxFolder.Attributes.HasFlag(FolderAttributes.Important);
      var isFldr_Inbox     = mbxFolder.Attributes.HasFlag(FolderAttributes.Inbox);
      var isFldr_Junk      = mbxFolder.Attributes.HasFlag(FolderAttributes.Junk);
      var isFldr_Sent      = mbxFolder.Attributes.HasFlag(FolderAttributes.Sent);
      var isFldr_Trash     = mbxFolder.Attributes.HasFlag(FolderAttributes.Trash);

      // It is not possible for any subfolders to exist under the folder.
      var isFldr_NoInferiors = mbxFolder.Attributes.HasFlag(FolderAttributes.NoInferiors);

      // It is not possible to select the folder.
      var isFldr_NoSelect = mbxFolder.Attributes.HasFlag(FolderAttributes.NoSelect);

      // The folder has been marked as possibly containing new messages since the folder was last selected.
      var isFldr_Marked = mbxFolder.Attributes.HasFlag(FolderAttributes.Marked);

      // The folder does not contain any new messages since the folder was last selected.
      var isFldr_Unmarked = mbxFolder.Attributes.HasFlag(FolderAttributes.Unmarked);

      // The folder does not exist, but is simply a place-holder.
      var isFldr_NonExistent = mbxFolder.Attributes.HasFlag(FolderAttributes.NonExistent);

      // The folder is subscribed
      var isFldr_Subscribed = mbxFolder.Attributes.HasFlag(FolderAttributes.Subscribed);

      // The folder is remote
      var isFldr_Remote = mbxFolder.Attributes.HasFlag(FolderAttributes.Remote);

      // The folder has subfolders
      var isFldr_HasChildren = mbxFolder.Attributes.HasFlag(FolderAttributes.HasChildren);

      // The folder does not have any subfolders
      var isFldr_HasNoChildren = mbxFolder.Attributes.HasFlag(FolderAttributes.HasNoChildren);

      if (isFldr_Archive
          || isFldr_Drafts
          || isFldr_Flagged
          || isFldr_Important
          || isFldr_Inbox
          || isFldr_Junk
          || isFldr_Sent
          || isFldr_Trash) {
         int stopperrrr = 1;
      }

      //+ Die Blacklisted Ordner überspringen
      if (iMailbox_FolderFilter.StopFolderProcessing_BlackWhiteList(mbxFolder)) {
         Debug.WriteLine($"  » skipped");

         return;
      }

      //» Die Message Summaries holen und nach Datum absteigend sortieren
      // Absteigend sortiert: die neuste Mail zuerst / Empfangsdatum
      // Das zweite Argument -1
      // !KH9 https://stackoverflow.com/a/29121052/4795779
      IList<IMessageSummary> messageSummaries = mbxFolder.Fetch
         (0
          , -1
          , MessageSummaryItems.Envelope

            // | MessageSummaryItems.BodyStructure
            | MessageSummaryItems.UniqueId

            // | MessageSummaryItems.Size
            | MessageSummaryItems.Headers

            // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
            | MessageSummaryItems.InternalDate
            | MessageSummaryItems.Flags);
      messageSummaries.Sort(new[] { OrderBy.ReverseDate });

      // Alle Mails als ungelesen markieren
      var allIDs = messageSummaries.Select(x => x.UniqueId).ToList();
      MimeKit_JigTools.MarkMsg_AsUnread(mbxFolder, allIDs);

      //+ Alle gefundenen Mails bearbeiten
      int msgCnt       = anzMails;
      int msgProcessed = 0;

      foreach (var msgSummary in messageSummaries) {
         // Wenn wir die vorgesehene Anzahl erreicht haben, überspringen
         if (ProcessMaxMailsPerFolder > 0
             && msgProcessed > ProcessMaxMailsPerFolder) {
            break;
         }

         mbxFolder.Open(FolderAccess.ReadOnly);

         if (--msgCnt%PrintMailProgressModulo == 0 || msgCnt == anzMails - 1) {
            Debug.WriteLine($"    ↻ {msgCnt,4} {msgSummary.Envelope.Subject}");
         }

         //‼ 🚩 Debug: Eine E-Mail mit bestimmtem Betreff suchen?
         var srchSubject = @"";
         srchSubject = @"";

         if ((srchSubject.ØHasValue() && msgSummary.Envelope.Subject == null)
             || (srchSubject.ØHasValue()
                 && msgSummary.Envelope.Subject != null
                 && !msgSummary.Envelope.Subject.Contains(srchSubject))) {
            continue;
         }

         //» Das BO Mail erzeugen
         BO_Mail bo = new BO_Mail
            (imapSrvRuntimeConfig
             , mbxFolder
             , msgSummary);

         var mailKategorie = bo.Analyze_And_AddOrUpdate_MsgHdr_MailKategorie();

         // Zählen
         _mailKategorieStatistik[mailKategorie.Value]++;

         // Die klassifizierten Mails verschieben?
         if (Move_Klassifizierte_EMails) {

            // Die Mail in einen gruppierten Ordner verschieben
            var zielOrdner = mailKatordner[mailKategorie.Value];

            if (!mbxFolder.FullName.ØEqualsIgnoreCase(zielOrdner.FullName)) {
               mbxFolder.Open(FolderAccess.ReadWrite);

               // !KH9 Der MimeKit Move Befehl ist intelligent
               // Und nützt die Möglichkeiten des Servers
               // !Q https://github.com/jstedfast/MailKit/issues/160#issuecomment-79290966
               var uidMap = mbxFolder.MoveTo(bo.MsgUniqueId, zielOrdner);

               // Hilft vielleicht, um die Änderung auf dem Server zu persistieren
               // !KH http://www.mimekit.net/docs/html/M_MailKit_IMailFolder_Close.htm
               // > Closes the folder, optionally expunging the messages marked for deletion.
               mbxFolder.Close();
            }

         }

      }
   }

   #endregion Iterator-Funktionen

}
