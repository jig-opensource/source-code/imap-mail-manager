using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ImapMailManager.Config;
using MailKit;
using MailKit.Search;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Gibt persönliche Daten aus, die in den Msg Headern sind
///
/// ‼ Haben persönliche Daten
/// - Msg Props
///   - Sender
///   - BCC
///      Empfänger vom Team
///   - CC
///      Fragesteller antworten und fügen sich selber als CC hinzu
///   - From
///      Fragesteller antworten
///   - To
///      Das Team antwrotet,
///      Fragesteller fügen sich im To hinzu 
///
/// - Msg Header
///   - Return-Path
///   - Authentication-Results
///   - Received-SPF
///   - From
///   - To: Wenn sie sich selber mit reinnehmen
///
/// 
/// ‼ Keine persönlichen Daten 
/// - Msg Header
///   - References
///   - Message-ID
///   - InReplyTo
///
/// </summary>
public class Print_PersönlicheDaten_ImHeader : IMailboxIterator {

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props


   /// <summary>
   /// Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}


   /// <summary>
   /// Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      int done = 1;

   }


   /// <summary>
   /// Verarbeitet den übergebenen Ordner
   /// 
   /// 
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {
      //+ Alle Mails, denen der Header fehlt suchen

      // Nötig, um den Inhalt zu erfahren
      mbxFolder.Open(FolderAccess.ReadOnly);
      Debug.WriteLine($"{mbxFolder.FullName}");

      IList<UniqueId>? uids = null;

      //+ Alle suchen
      uids = mbxFolder.Search(SearchQuery.All);

      // 🚩 Debug.WriteLine($"  Anzahl: {uids.Count}");

      string[] _EmailHeaders = {
         "From", "Return-Path", "Authentication-Results", "Received-SPF"
      };

      IEnumerable<string> MsgHeadernames
         = ConfigMail_MsgHeader.Get_DefaultEMailHeader_Names().Concat(_EmailHeaders);

      string[] removeHeaders = { "References", "Message-ID", "InReplyTo", "jigQ" };

      List<string> sumHeaders = new List<string>();
      sumHeaders.AddRange(MsgHeadernames);

      foreach (var removeHeader in removeHeaders) {
         sumHeaders.Remove(removeHeader);
      }

      //+ Die Message Summaries holen
      IList<IMessageSummary>? messageSummaries = mbxFolder.ØFetch
         (uids
          , MessageSummaryItems.Envelope
            | MessageSummaryItems.BodyStructure
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Size
            | MessageSummaryItems.Flags

          //+++ Wichtige Header auch lesen
          , sumHeaders);

      //+ Jede Msg lesen
      foreach (var msgSummary in messageSummaries) {
         //‼ 🚩 Debug
         Debug.WriteLine($"\n\n    » {msgSummary.Envelope.Subject}");

         // Debug.WriteLine($"      - Subject  : {msgSummary.Envelope.Subject}");

         List<string> headerData = new List<string>();

         //++ Die props ausgeben
         int stoppfsffser = 1;

         // Debug.WriteLine($"      - TO       : {msgSummary.Envelope.To}");
         // Debug.WriteLine($"      - CC       : {msgSummary.Envelope.Cc}");
         // Debug.WriteLine($"      - From     : {msgSummary.Envelope.From}");
         // Debug.WriteLine($"      - Sender   : {msgSummary.Envelope.Sender}");
         // Debug.WriteLine($"      - InReplyTo: {msgSummary.Envelope.InReplyTo}");

         /*
         Debug.WriteLine($"\n    » Headers");

         foreach (var header in msgSummary.Headers) {
            Debug.WriteLine($"      - {header.Field,30}: {header.Value}");
         }
         */
      }

      int stopper = 1;
   }

   #endregion Iterator-Funktionen

}
