using System.Collections.Generic;
using System.Diagnostics;
using ImapMailManager.BO;
using jig.Tools;
using jig.Tools.String;
using MailKit;
using MailKit.Net.Imap;
using MailKit.Search;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Löscht alle E-Mails im Test-Postfach
/// </summary>
public class Process_AllMails_DELETE_AllMails : IMailboxIterator {

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props

   /// <summary>
   /// Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}


   /// <summary>
   /// Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      //‼ 🚩 Debug
      int done = 1;
   }


   /// <summary>
   /// Analysiert alle E-Mails und aktualisiert die Metadaten
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {

      // ToDo Now 🟥 Sicherstellen, dass die Funkon nur in einer Archiv-Mailbox gestartet wird!

      // Nötig, um den Inhalt abzurufen
      mbxFolder.Open(FolderAccess.ReadOnly);
      Debug.WriteLine($"{mbxFolder.FullName}");

      // ‼ 📌 Mailbox-Ordner Filter

      //++++ Überspringen, bis wir den Ordner gefunden haben
      var srchFolder = @"";
      srchFolder = @"Eschatologie";
      srchFolder = @"2b Frage Allgemein (anonymisiert!)";
      srchFolder = @"";

      // if (srchFolder.ØHasValue() && !mbxFolder.FullName.ØEndsWithIgnoreCase(srchFolder)) {
      // if (srchFolder.ØHasValue() && !mbxFolder.FullName.StartsWith(srchFolder)) {
      if (srchFolder.ØHasValue() && !mbxFolder.FullName.Contains(srchFolder)) {
         Debug.WriteLine($"  » skipped");

         return;
      }

      // ToDo Now 🟥 : Die Funktion sucht nur jene Mails, in denen die jig Hdr fehlen 
      //+ Alle Mails suchen
      var uids = mbxFolder.Search(SearchQuery.All);

      // 🚩 Debug.WriteLine($"  Anzahl: {uids.Count}");
      //+ Alle Mails löschen
      mbxFolder.Open(FolderAccess.ReadWrite);

      mbxFolder.AddFlags
         (uids
          , MessageFlags.Deleted
          , true);

      // Wenn möglich, die alte Message endgültig aus dem Ordner löschen
      if (imapSrvRuntimeConfig!.ImapClient.Capabilities.HasFlag
             (ImapCapabilities.UidPlus)) { mbxFolder.Expunge(uids); }

      //+ Den Ordner löschen
      mbxFolder.Delete();

      //+ Den Inhalt des Ordners(?) löschen
      // mbxFolder.Expunge();

      /*
            //+ Die Message Summaries holen
            IList<IMessageSummary> messageSummaries = mbxFolder.Fetch
               (uids
                , MessageSummaryItems.Envelope
                  | MessageSummaryItems.BodyStructure
                  | MessageSummaryItems.UniqueId
                  | MessageSummaryItems.Size
                  | MessageSummaryItems.Headers
                  | MessageSummaryItems.Flags);
      
            //+ Test_BO_Mail: Jede Msg lesen
            foreach (var msgSummary in messageSummaries) {
               //‼ 🚩 Debug
               Debug.WriteLine($"    » {msgSummary.Envelope.Subject}");
      
               //‼ 🚩 Debug
               var srchSubject = @"";
               srchSubject = @"Jer 22,30 vs 36,30";
               srchSubject = @"Re: #63-4334 • RL Formular: Seelsorge";
               srchSubject = @"""Magic Word"" im 21. Jhd.";
               srchSubject = @"Felice Del Prete :Grüße";
               srchSubject = @"Re: #2F-3955 • Allgemeine Frage: Taufen";
               srchSubject = @"FW: #2F-3955 • Allgemeine Frage: Taufen";
               srchSubject = @"";
      
               // if (srchSubject.ØHasValue() && !msgSummary.Envelope.Subject.ØEqualsIgnoreCase(srchSubject)) {
               //    continue;
               // }
      
               if (srchSubject.ØHasValue() && msgSummary.Envelope.Subject != null && !msgSummary.Envelope.Subject.Contains(srchSubject)) {
                  continue;
               }
      
               //+ Obj erzeugen
               BO_Mail bo = new BO_Mail
                  (imapSrvRuntimeConfig
                   , mbxFolder
                   , msgSummary);
      
               //+ Mail analysieren und alle Daten aktualisieren         
               bo.Delete();
      
               
      
               //‼ 🚩 Debug
      
            }
      */

   }

   #endregion Iterator-Funktionen


}
