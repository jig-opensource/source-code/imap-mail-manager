using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using ImapMailManager.Config;
using jig.Tools;
using jig.Tools.String;
using MailKit;
using MailKit.Search;
using MimeKit;
using TextCopy;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// welche Werte haben wir im Header?:
///  
///   thisMsg.Body.Headers
///     Content-Language              : de-DE
///
/// ‼ Erkennen wir so die Sprache der E-Mail?
///
/// ‼ » Resultat: Dieses Body Header identifiziert die Sprache des Mail-Inhalts nicht / max. schlecht.  
/// 
/// </summary>
public class Analyze_Headers_ContentLanguage {

   static List<string> hdrContentLanguage = new List<string>();

   /// <summary>
   /// Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public static void ProcessMailboxFolder_Done() {
      int done = 1;
   }


   /// <summary>
   /// Verarbeitet den übergebenen Ordner
   /// 
   /// 
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public static void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {
      //+ Alle Mails, denen der Header fehlt suchen

      // Nötig, um den Inhalt zu erfahren
      mbxFolder.Open(FolderAccess.ReadOnly);
      Debug.WriteLine($"{mbxFolder.FullName}");

      IList<UniqueId>? uids = null;

      //+ Alle suchen
      uids = mbxFolder.Search(SearchQuery.All);

      // 🚩 Debug.WriteLine($"  Anzahl: {uids.Count}");

      string[] _EmailHeaders = {
         "From", "Return-Path", "Authentication-Results", "Received-SPF"
      };

      IEnumerable<string> MsgHeadernames
         = ConfigMail_MsgHeader.Get_DefaultEMailHeader_Names().Concat(_EmailHeaders);

      string[] removeHeaders = { "References", "Message-ID", "InReplyTo", "jigQ" };

      List<string> sumHeaders = new List<string>();
      sumHeaders.AddRange(MsgHeadernames);

      foreach (var removeHeader in removeHeaders) {
         sumHeaders.Remove(removeHeader);
      }

      //+ Die Message Summaries holen
      IList<IMessageSummary>? messageSummaries = mbxFolder.ØFetch
         (uids
          , MessageSummaryItems.Envelope
            | MessageSummaryItems.BodyStructure
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Size
            | MessageSummaryItems.Flags
            | MessageSummaryItems.Headers

            // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
            | MessageSummaryItems.InternalDate

          //+++ Wichtige Header auch lesen
          , sumHeaders);

      //+ Jede Msg lesen
      var msgCnt = uids.Count;
      foreach (var msgSummary in messageSummaries) {
         //‼ 🚩 Debug Subject
         // Debug.WriteLine($"\n\n    » {msgSummary.Envelope.Subject}");
         // Debug.WriteLine($"      - Subject  : {msgSummary.Envelope.Subject}");

         if (msgCnt-- %20 == 0 || msgCnt == uids.Count-1) { Debug.WriteLine($"  ↻ Lese Mails: {msgCnt,4}"); }


         //+ Gibt es einen jig* header?
         /*
         var hasJigHeader =
            msgSummary.Headers.Any
               (x => x.Field.StartsWith("jig", StringComparison.OrdinalIgnoreCase));

         if (!hasJigHeader) {
            continue;
         }
         */

         //+ Die Meaasge lesen
         MimeMessage thisMsg = mbxFolder.GetMessage(msgSummary!.UniqueId);

         //+ Wenn wir den Header  haben, dann sammeln
         Header? currRefNrHdr = thisMsg.Body.Headers.FirstOrDefault
               (h => h.Field.ØEqualsIgnoreCase("Content-Language"));

         if (currRefNrHdr != null) {
            hdrContentLanguage.ØAppendItem_IfMissing(currRefNrHdr.Value);

            if (!currRefNrHdr.Value.StartsWith("de", StringComparison.OrdinalIgnoreCase)) {
               Debug.WriteLine($"  - {currRefNrHdr.Value,-6} Subject  : {thisMsg.Subject}");
            }
            
         }

         continue;
            
            
         
         //+ Alle Header ausgeben

         StringBuilder sb = new StringBuilder();

         //++ IMessageSummary
         sb.AppendLine("");
         sb.AppendLine("IMessageSummary.Headers");

         foreach (var msgSumHeader in msgSummary.Headers.OrderBy(x => x.Field)) {
            sb.AppendLine($"  {msgSumHeader.Field,-30}: {msgSumHeader.Value}");
         }

         //++ thisMsg
         sb.AppendLine("");
         sb.AppendLine("thisMsg.Headers");

         foreach (var msgSumHeader in thisMsg.Headers.OrderBy(x => x.Field)) {
            sb.AppendLine($"  {msgSumHeader.Field,-30}: {msgSumHeader.Value}");
         }

         //++ thisMsg.Body
         sb.AppendLine("");
         sb.AppendLine("thisMsg.Body.Headers");

         foreach (var msgSumHeader in thisMsg.Body.Headers.OrderBy(x => x.Field)) {
            sb.AppendLine($"  {msgSumHeader.Field,-30}: {msgSumHeader.Value}");
         }

         sb.AppendLine("");

         //‼ 🚩 Debug: Ins ClipBoard kopieren
         ClipboardService.SetText(sb.ToString());

         int stopper = 1;
      }
   }

}

