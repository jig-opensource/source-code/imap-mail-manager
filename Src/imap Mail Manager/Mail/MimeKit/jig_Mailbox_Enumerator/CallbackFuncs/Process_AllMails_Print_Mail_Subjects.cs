using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using ImapMailManager.BO;
using jig.Tools;
using jig.Tools.String;
using MailKit;
using MailKit.Search;
using TextCopy;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Sammelt alle Mail-Subjects
/// </summary>
public class Process_AllMails_Print_Mail_Subjects : IMailboxIterator {

   private static readonly List<string> MailSubjects = new List<string>();

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props


   /// <summary>
   /// Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}


   /// <summary>
   /// Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      int done = 1;

      StringBuilder sb = new StringBuilder();

      foreach (var subject in MailSubjects.OrderBy(x => x)) {
         sb.AppendLine(subject);
      }

      // 🚩 Debug: Ins ClipBoard kopieren
      ClipboardService.SetText(sb.ToString());
      
      Debug.WriteLine(sb.ToString());
   }


   /// <summary>
   /// Verarbeitet den übergebenen Ordner
   /// 
   /// 
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {
      //+ Alle Mails, denen der Header fehlt suchen

      // Nötig, um den Inhalt zu erfahren
      mbxFolder.Open(FolderAccess.ReadOnly);
      Debug.WriteLine($"{mbxFolder.FullName}");

      // ‼ 📌 Mailbox-Ordner Filter

      //++++ Überspringen, bis wir den Ordner gefunden haben
      var srchFolder = @"";
      srchFolder = @"10 In Arbeit: Mit Roger klären";
      srchFolder = @"1 Seelsorge";
      srchFolder = @"Ehe und Scheidung";
      srchFolder = @"2a Frage Bibel";
      srchFolder = @"AT allgemein";
      srchFolder = @"Eschatologie";
      srchFolder = @"";

      if (srchFolder.ØHasValue() && !mbxFolder.FullName.ØEndsWithIgnoreCase(srchFolder)) {
         Debug.WriteLine($"  » skipped");

         return;
      }

      IList<UniqueId>? uids = null;

      //+ Alle suchen
      uids = mbxFolder.Search(SearchQuery.All);

      // 🚩 Debug.WriteLine($"  Anzahl: {uids.Count}");

      // Sortieren: zuerst die neuste E-Mail
      // !Q https://stackoverflow.com/a/58580564/4795779						
      // var messages = imapFolder.Fetch(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.InternalDate).ToList();
      // messages.Sort(new OrderBy[] { OrderBy.ReverseDate });


      //+ Die Message Summaries holen
      IList<IMessageSummary> messageSummaries = mbxFolder.Fetch
         (uids
          , MessageSummaryItems.Envelope
            | MessageSummaryItems.BodyStructure
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Size
            | MessageSummaryItems.Headers
            // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
            | MessageSummaryItems.InternalDate
            | MessageSummaryItems.Flags);

      //+ Test_BO_Mail: Jede Msg lesen
      foreach (var msgSummary in messageSummaries) {
         //‼ 🚩 Debug
         Debug.WriteLine($"    » {msgSummary.Envelope.Subject}");

         // Debug.WriteLine($"      - Subject  : {msgSummary.Envelope.Subject}");

         // 🚩 Debug
         var srchSubject = @"";
         srchSubject = @"230120 173625";
         srchSubject = @"Jer 22,30 vs 36,30";
         srchSubject = @"";

         if (srchSubject.ØHasValue() && !msgSummary.Envelope.Subject.ØEqualsIgnoreCase(srchSubject)) {
            continue;
         }

         MailSubjects.Add(msgSummary.Envelope.Subject);
         
/*
         //+ Obj erzeugen
         BO_Mail bo = new BO_Mail
            (imapSrvRuntimeConfig
             , mbxFolder
             , msgSummary);
             
         // Alle App-Spezifischen Mails Überspringen: ✅ Dok:
         if (bo.IsAppSpecificMail) { continue; }


         //‼ 🚩 Debug
         Debug.WriteLine($"      Sprache: {bo.MailInhalt_Sprache}");
*/
      }
   }

   #endregion Iterator-Funktionen


}
