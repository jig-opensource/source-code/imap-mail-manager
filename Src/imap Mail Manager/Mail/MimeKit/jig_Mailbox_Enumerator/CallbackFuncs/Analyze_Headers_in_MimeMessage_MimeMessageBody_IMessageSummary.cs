using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using ImapMailManager.Config;
using MailKit;
using MailKit.Search;
using MimeKit;
using TextCopy;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// 
/// ‼ » Details sind unten im Kommentar beschrieben 
/// ‼ » Resultat:
///   
///   • IMessageSummary.Headers
///         hat nur einen Bruchteil der Message-header
///      !9 hat aber die App-Headers jig*
///         <see cref="ImapMailManager.Config.ConfigMail_MsgHeader.AppMailHeaderType"/>
///  
///   • thisMsg.Body.Headers
///      Nur nützlich für Mime Parts
///   • thisMsg.Headers
///      Hat die meisten und nützlichsten Header
/// 
///   
///   Beispiel von Headern:
/// 
///      thisMsg.Body.Headers
///         Content-Language
///         Content-Type
/// 
///       IMessageSummary.Headers
///         Authentication-Results
///         From
///         In-Reply-To
///         jigQNotFound
///         Received-SPF
///         Return-Path
///         Thread-Index
///         Thread-Topic
///
///      thisMsg.Headers
///         Accept-Language
///         ARC-Authentication-Results
///         ARC-Message-Signature
///         ARC-Seal
///         Authentication-Results
///         Date
///         Delivered-To
///         DKIM-Signature
///         From
///         In-Reply-To
///         jigQNotFound
///         Message-ID
///         MIME-Version
///         Received
///         Received-SPF
///         References
///         Return-Path
///         Sender
///         Subject
///         Thread-Index
///         Thread-Topic
///         To
///         X-GBUdb-Analysis
///         X-MessageSniffer-Rules
///         X-MessageSniffer-Scan-Result
///         x-microsoft-antispam
///         x-microsoft-antispam-message-info
///         x-ms-…
///         x-ms-exchange-antispam-messagedata-0
///         X-MS-Exchange-CrossTenant-id
///         X-MS-Exchange-CrossTenant-Network-Message-Id
///         X-MS-Has-Attach
///         X-Original-To
///         X-OriginatorOrg
///         X-Spam-Checker-Version
///         X-Spam-Level
///         X-Spam-Status
///            
/// 
/// MailKit / Mimekit hat Header in verschiedenen Klassen:
/// - IMessageSummary.Headers
///   http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_Headers.htm
///   Gets the list of headers, if available.
///   This property will only be set if the IFetchRequest used
///   with Fetch or FetchAsync has the Headers flag set on Items
///   or if the Headers list is non - empty.
/// 
/// - MimeMessage.Headers
///   http://www.mimekit.net/docs/html/P_MimeKit_MimeMessage_Headers.htm
///   Represents the list of headers for a message.
///   Typically, the headers of a message will contain transmission headers
///   such as From and To
///   along with metadata headers such as Subject and Date,
///   but may include just about anything.
/// 
/// - MimeMessage.Body.Headers
///   » Entspricht: MimeEntity.Headers
///   http://www.mimekit.net/docs/html/P_MimeKit_MimeEntity_Headers.htm
///   Represents the list of headers for a MIME part.
///   Typically, the headers of a MIME part will be various Content-* headers
///   such as Content-Type or Content-Disposition,
///   but may include just about anything.
/// 
/// </summary>
public class Analyze_Headers_in_MimeMessage_MimeMessageBody_IMessageSummary : IMailboxIterator {

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props


   /// <summary>
   /// Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}

   /// <summary>
   /// Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      int done = 1;
   }


   /// <summary>
   /// Verarbeitet den übergebenen Ordner
   /// 
   /// 
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {
      //+ Alle Mails, denen der Header fehlt suchen

      // Nötig, um den Inhalt zu erfahren
      mbxFolder.Open(FolderAccess.ReadOnly);
      Debug.WriteLine($"{mbxFolder.FullName}");

      IList<UniqueId>? uids = null;

      //+ Alle suchen
      uids = mbxFolder.Search(SearchQuery.All);

      // 🚩 Debug.WriteLine($"  Anzahl: {uids.Count}");

      string[] _EmailHeaders = {
         "From", "Return-Path", "Authentication-Results", "Received-SPF"
      };

      IEnumerable<string> MsgHeadernames
         = ConfigMail_MsgHeader.Get_DefaultEMailHeader_Names().Concat(_EmailHeaders);

      string[] removeHeaders = { "References", "Message-ID", "InReplyTo", "jigQ" };

      List<string> sumHeaders = new List<string>();
      sumHeaders.AddRange(MsgHeadernames);

      foreach (var removeHeader in removeHeaders) {
         sumHeaders.Remove(removeHeader);
      }

      //+ Die Message Summaries holen
      IList<IMessageSummary>? messageSummaries = mbxFolder.ØFetch
         (uids
          , MessageSummaryItems.Envelope
            | MessageSummaryItems.BodyStructure
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Size
            | MessageSummaryItems.Flags
            | MessageSummaryItems.Headers

            // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
            | MessageSummaryItems.InternalDate

          //+++ Wichtige Header auch lesen
          , sumHeaders);

      //+ Jede Msg lesen
      //++ tester
      //+++ tester
      foreach (var msgSummary in messageSummaries) {
         //‼ 🚩 Debug
         Debug.WriteLine($"\n\n    » {msgSummary.Envelope.Subject}");

         // Debug.WriteLine($"      - Subject  : {msgSummary.Envelope.Subject}");

         //+ Gibt es einen jig* header?
         var hasJigHeader =
            msgSummary.Headers.Any
               (x => x.Field.StartsWith("jig", StringComparison.OrdinalIgnoreCase));

         if (!hasJigHeader) {
            continue;
         }

         //+ Die Meaasge lesen
         MimeMessage thisMsg = mbxFolder.GetMessage(msgSummary!.UniqueId);

         //+ Alle Header ausgeben

         StringBuilder sb = new StringBuilder();

         //++ IMessageSummary
         sb.AppendLine("");
         sb.AppendLine("IMessageSummary.Headers");

         foreach (var msgSumHeader in msgSummary.Headers.OrderBy(x => x.Field)) {
            sb.AppendLine($"  {msgSumHeader.Field,-30}: {msgSumHeader.Value}");
         }

         //++ thisMsg
         sb.AppendLine("");
         sb.AppendLine("thisMsg.Headers");

         foreach (var msgSumHeader in thisMsg.Headers.OrderBy(x => x.Field)) {
            sb.AppendLine($"  {msgSumHeader.Field,-30}: {msgSumHeader.Value}");
         }

         //++ thisMsg.Body
         sb.AppendLine("");
         sb.AppendLine("thisMsg.Body.Headers");

         foreach (var msgSumHeader in thisMsg.Body.Headers.OrderBy(x => x.Field)) {
            sb.AppendLine($"  {msgSumHeader.Field,-30}: {msgSumHeader.Value}");
         }

         sb.AppendLine("");

         //‼ 🚩 Debug: Ins ClipBoard kopieren
         ClipboardService.SetText(sb.ToString());

         int stopper = 1;
      }
   }

   #endregion Iterator-Funktionen

}

