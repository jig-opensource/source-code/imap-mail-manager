using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using jig.Tools.String;
using MailKit;
using MailKit.Search;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Liest alle Nachrichten
/// Analysiert sie dann mit dem MessageThreader
/// http://www.mimekit.net/docs/html/M_MailKit_MessageThreader_Thread.htm
/// </summary>
public class Process_AllMails_Get_MessageThreads : IMailboxIterator, IMailbox_FolderFilter {

   #region IMailbox_FolderFilter

   /// <summary>
   /// Die Default Methode im Interface:
   ///   StopFolderProcessing_BlackWhiteList
   /// muss ins Interface gecastet werden, damit sie aufgerufen werden kann:
   /// !KH https://stackoverflow.com/a/57763254/4795779
   /// Dies hier ist ein Prop als Cast zum Interface, damit der Aufruf hübscher gemacht werden kann 
   /// </summary>
   private IMailbox_FolderFilter iMailbox_FolderFilter => (IMailbox_FolderFilter)this;

   #endregion IMailbox_FolderFilter

   #region Props

   #region Quell-Daten

   /// <summary>
   /// Sammelt von der ganzen Mailbox alle IMessageSummary
   /// </summary>
   public List<IMessageSummary> AllMsgSummary = new();

   #endregion Quell-Daten

   #region Abgeleitete Daten

   /// <summary>
   /// Initialisiert alle Props mit null, deren Daten von anderen Props abhängig sind,
   /// so dass die Inhalte wieder neu berechnet werden
   /// </summary>
   public void Reset_Abgeleitete_Daten() {
      _allMsgThreads     = null;
      _allMailboxFolders = null;
   }


   private IList<MessageThread>? _allMsgThreads;
   private IList<IMailFolder>?   _allMailboxFolders;

   /// <summary>
   ///  Alle Mails gruppiert nach Threads
   /// </summary>
   public IList<MessageThread> AllMsgThreads => _allMsgThreads ??= AllMsgSummary.ØGetMessageSummaryThreads();

   /// <summary>
   /// Von der Mailbox alle IMailFolder
   /// </summary>
   public IList<IMailFolder> AllMailBoxFolders
      => _allMailboxFolders ??= AllMsgSummary.Select(x => x.Folder).DistinctBy(x => x.FullName).ToList();

   #endregion Abgeleitete Daten

   #endregion Props

   #region IMailbox_FolderFilter

   /// <summary>
   /// Die Liste der Whitelist-Ordner
   /// Hat Priorität gegenüber der Blacklist:
   /// Wenn die Whitelist definiert ist, dann wird die Blacklist ignoriert
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist { get; }
   /// <summary>
   /// Wenn die Whitelist definiert ist, 
   /// dann werden diese Order nicht verarbeitet
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist_Except_StopOn { get; }
   /// <summary>
   /// Die Liste der Blacklist-Ordner
   /// Wird nur angewendet, wenn die Whitelist nicht definiert ist
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Blacklist { get; }

   #endregion IMailbox_FolderFilter

   #region Konstruktoren

   /// <summary>
   /// Konstruktor ohne FolderFilter 
   /// </summary>
   public Process_AllMails_Get_MessageThreads() {}


   /// <summary>
   /// Initialisiert den Iterator mit einem FolderFilter
   /// </summary>
   /// <param name="folderFilter"></param>
   public Process_AllMails_Get_MessageThreads(FolderFilter folderFilter) {
      MbxFolder_Whitelist               = folderFilter.MbxFolder_Whitelist;
      MbxFolder_Whitelist_Except_StopOn = folderFilter.MbxFolder_Whitelist_Except_StopOn;
      MbxFolder_Blacklist               = folderFilter.MbxFolder_Blacklist;
   }

   #endregion Konstruktoren

   #region Iterator
   
   #region Iterator-Funktionen

   #region Iterator-Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Iterator-Props

   #endregion Iterator

   /// <summary>
   /// Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}


   /// <summary>
   /// Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      int done = 1;

      //» Process done: die erfassten Daten analysieren

      //+ Alle Msg Summary nach der Message-ID gruppieren
      var allMsgSummaryByMessageID = AllMsgSummary
                                    .Where(x => x.Headers.Contains(("Message-ID")))
                                    .GroupBy(x => x.Headers["Message-ID"]);

      //+ Alle Nachrichten, für die eine Meassge-ID mind. 2x vorkommt 
      var allMailsWithIdenticalMessageIDs = allMsgSummaryByMessageID.Where(x => x.Count() >= 2);

      // Hier weiter:
      // - den Mail Body lesen alle Mails in allMailsWithIdenticalMessageIDs
      // - Den MsgBody_Hash lesen und vergleichen 

      var orderBy = new[] { OrderBy.ReverseDate };

      //+ Alle Msg Summaries analysieren
      // und die Threads zusammenfassen
      var threads = AllMsgSummary.ØGetMessageSummaryThreads();

      // Alle Threads, die tatsächlich Kinder haben
      var treadsWithChildren = threads.Where(x => x.Children.Any());

      int stopper = 1;

      /*
            StringBuilder sb = new StringBuilder();
      
            foreach (var subject in MailSubjects.OrderBy(x => x)) {
               sb.AppendLine(subject);
            }
      
            // 🚩 Debug: Ins ClipBoard kopieren
            ClipboardService.SetText(sb.ToString());
            
            Debug.WriteLine(sb.ToString());
      */

   }


   /// <summary>
   /// Verarbeitet den übergebenen Ordner
   /// 
   /// 
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {

      // Nötig, um den Inhalt zu erfahren
      mbxFolder.Open(FolderAccess.ReadOnly);
      var anzMails = mbxFolder.Count;
      Debug.WriteLine($"{mbxFolder.FullName} ({anzMails})");

      //+ Die Blacklisted Ordner überspringen
      if (iMailbox_FolderFilter.StopFolderProcessing_BlackWhiteList(mbxFolder)) {
         Debug.WriteLine($"  » skipped");

         return;
      }

      // ‼ 📌 Mailbox-Ordner Filter

      //++++ Überspringen, bis wir den Ordner gefunden haben
      var srchFolder = @"";

      if (srchFolder.ØHasValue() && !mbxFolder.FullName.Contains(srchFolder, StringComparison.OrdinalIgnoreCase)) {
         Debug.WriteLine("  » skipped");

         return;
      }

      //» Die Message Summaries holen und nach Datum absteigend sortieren
      // Absteigend sortiert: die neuste Mail zuerst / Empfangsdatum
      // Das zweite Argument -1
      // !KH9 https://stackoverflow.com/a/29121052/4795779
      IList<IMessageSummary> messageSummaries = mbxFolder.Fetch
         (0
          , -1
          , MessageSummaryItems.Envelope
            | MessageSummaryItems.BodyStructure
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Size
            | MessageSummaryItems.Headers

            // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
            | MessageSummaryItems.InternalDate
            | MessageSummaryItems.References
            | MessageSummaryItems.Flags);

      // Sortieren: zuerst die neuste E-Mail
      messageSummaries.Sort(new[] { OrderBy.ReverseDate });

      // Die Msg Summaries sammelb
      AllMsgSummary.AddRange(messageSummaries);

   }

   #endregion Iterator-Funktionen

}
