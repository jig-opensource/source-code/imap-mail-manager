using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ImapMailManager.BO;
using ImapMailManager.Config;
using jig.Tools;
using jig.Tools.Exception;
using jig.Tools.String;
using MailKit;
using MailKit.Search;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Sucht alle Doc-Mails und setzt das Empfangs-Datum aufs aktuelle Datum,
/// damit sie immer als erstes angezeigt werden
/// </summary>
public class Process_AllMails_UpdateRecievedDate_ForDocMails : IMailboxIterator {

   //‼ Config

   #region Config

   /// <summary>
   /// Wenn true, dann wird bei den Doc-Mails das Empfangsdatum zwingend gesetzt,
   /// sonst erst 3 Tage bevor die E-Mail von der Gegenawart eingeholt würde 
   /// </summary>
   private static readonly bool ForceUpdateEmpfangsdatum = true;

   /// <summary>
   /// Ignoriert MbxFolderBlacklist und verarbeitet alle Mailbox-Ordner
   /// </summary>
   private static readonly bool ProcessAllMailboxFolders = true;

   /// <summary>
   /// Das Query für die Suche der Doc-Mails mit dem Betreffm wie er aktuell ist:
   ///   ⬛️⬛️⬛️ Dokumentation zu diesem Ordner 
   ///   ▶▶▶ Dokumentation zu diesem Ordner 
   /// </summary>
   private static readonly TextSearchQuery MailSujectSrchQry = SearchQuery.SubjectContains(ConfigMail_Basics
                                                                                             .AppSpecMail_Doc_SubjectStart);

   /// <summary>
   /// Die Mailbox-Ordnernamen, die wir nicht verarbeiten
   /// </summary>
   private static readonly List<string> MbxFolderBlacklist = new List<string>
      (new[] {
         "Trash", "Gelöschte Elemente", "Junk-E-Mail", "Junk"
      });

   #endregion Config

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props

   /// <summary>
   /// Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}


   /// <summary>
   /// Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      int done = 1;

      int stopper = 2;

      /*
            StringBuilder sb = new StringBuilder();
      
            foreach (var subject in MailSubjects.OrderBy(x => x)) {
               sb.AppendLine(subject);
            }
      
            // 🚩 Debug: Ins ClipBoard kopieren
            ClipboardService.SetText(sb.ToString());
            
            Debug.WriteLine(sb.ToString());
      */

   }


   /// <summary>
   /// Verarbeitet den übergebenen Ordner
   /// 
   /// 
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {

      // Nötig, um den Inhalt zu erfahren
      mbxFolder.Open(FolderAccess.ReadOnly);
      Debug.WriteLine($"{mbxFolder.FullName}");

      //+ Die Blacklisted Ordner überspringen 
      if (!ProcessAllMailboxFolders && MbxFolderBlacklist.ØRgxMatchWithAnyItemInList(mbxFolder.FullName)) {
         Debug.WriteLine($"  » skipped");

         return;
      }

      //+ Im Ordner die Doc-Mails suchen
      mbxFolder.Open(FolderAccess.ReadOnly);
      var uids = mbxFolder.Search(MailSujectSrchQry);

      // 🚩 Debug.WriteLine($"  Anzahl: {uids.Count}");

      // Sortieren: zuerst die neuste E-Mail
      // !Q https://stackoverflow.com/a/58580564/4795779						
      // var messages = imapFolder.Fetch(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.InternalDate).ToList();
      // messages.Sort(new OrderBy[] { OrderBy.ReverseDate });


      //+ Die Message Summaries holen
      IList<IMessageSummary> messageSummaries = mbxFolder.Fetch
         (uids
          , MessageSummaryItems.Envelope
            | MessageSummaryItems.BodyStructure
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Size
            | MessageSummaryItems.Headers
            // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
            | MessageSummaryItems.InternalDate
            // https://github.com/jstedfast/MailKit/issues/1254#issuecomment-1277881417
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Envelope
            | MessageSummaryItems.References
            | MessageSummaryItems.Flags);

      //+ Alle gefundenen Mails bearbeiten
      int msgCnt = uids.Count;
      foreach (var msgSummary in messageSummaries) {
         //‼ 🚩 Debug
         Debug.WriteLine($"    » {msgSummary.Envelope.Subject}");
         // if (msgCnt-- %20 == 0 || msgCnt == uids.Count-1) { Debug.WriteLine($"    ↻ {msgCnt,4} {msgSummary.Envelope.Subject}"); }

         //++ Obj erzeugen
         BO_Mail bo = new BO_Mail
            (imapSrvRuntimeConfig
             , mbxFolder
             , msgSummary);

         // Assert: Nur Doc-Mails verarbeiten: ✅ Dok:
         if (!bo.IsAppSpecificMail_DocMail) { continue; }


         //++ Das Empfangsdatum prüfen und allenfalls setzen
         // Wie viele Tage ist das Mail-Datum noch in der Zukunft?
         TimeSpan dt = bo.MsgSummary.Date - DateTimeOffset.Now;

         // Wenn das Datum nicht mehr mind. 3 Tage in der Zukunft liegt, es neu setzen
         if (ForceUpdateEmpfangsdatum || dt.Days <= 3) {

            // Das Datum 9 Tage voraus setzen
            DateTimeOffset neuesDocDatum = DateTimeOffset.Now.AddDays(9)
                                                         .Date.Add
                                                             (new TimeSpan
                                                                 (0
                                                                  , 0
                                                                  , 0));

            //+++ Das Empfangsdatum setzen
            bo.Msg.Date = neuesDocDatum;

            //+ Im Msg Hdr Received den Timestamp ersetzen
            //‼ Struktur Msg Header Received
            // !Ref https://www.rfc-editor.org/rfc/rfc822#section-4.1
            // 
            // !Ex 
            // from webmail.rogerliebi.ch (localhost [127.0.0.1])	by mx-out.sr37.firestorm.ch (Postfix) with ESMTPSA id 69C12358087C	for <Team@RogerLiebi.ch>; Fri,  3 Feb 2023 20:18:15 +0100 (CET)
            // MailKit trennt die Elemente durch \t
            //  Received: from webmail.rogerliebi.ch (localhost [127.0.0.1])
            //            by mx -out.sr37.firestorm.ch(Postfix) with ESMTPSA id 6C7F835803CF
            //            for < team@rogerliebi.ch >; Sat, 11 Feb 2023 10:14:46 + 0100(CET)
            //
            // !KH9
            // - Das letzte Element ist time received, getrennt durch ;
            //   » So können wir den Timestamp finden

            //++ Den Msg Header lesen: Received
            
               /*
               Received: from EUR03-AM7-obe.outbound.protection.outlook.com (mail-am7eur03on2112.outbound.protection.outlook.com [40.107.105.112])
                   by mx-out.sr37.firestorm.ch (Postfix) with ESMTPS id 22D873580AFD
                   for <team@rogerliebi.ch>; Mon, 13 Feb 2023 13:43:38 +0100 (CET)
               Received: from DB9P191MB1820.EURP191.PROD.OUTLOOK.COM
                   ([fe80::6813:b5c4:ddcc:f42a]) by DB9P191MB1820.EURP191.PROD.OUTLOOK.COM
                   ([fe80::6813:b5c4:ddcc:f42a%9]) with mapi id 15.20.6086.024; Mon, 13 Feb 2023
                   12:43:37 +0000
               Received: from DB9P191MB1820.EURP191.PROD.OUTLOOK.COM (2603:10a6:10:243::17)
                   by GV2P191MB2236.EURP191.PROD.OUTLOOK.COM (2603:10a6:150:ad::15) with
                   Microsoft SMTP Server (version=TLS1_2,
                   cipher=TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384) id 15.20.6064.34; Mon, 13 Feb
                   2023 12:43:37 +0000
                */

            var allMsgHdrReceivHdrs = bo.MsgSummary.Headers.Where(x => x.Field.ØEqualsIgnoreCase("Received")).ToList();

            if (allMsgHdrReceivHdrs.Count > 1) {
               throw new RuntimeException
                  ("ProcessMailboxFolder(): Mehrere Msg Header 'Received' gefunden!");
            }

            var msgHdrReceived = bo.MsgSummary.Headers["Received"];

            if (!msgHdrReceived.ØHasValue()) {
               throw new RuntimeException
                  ("ProcessMailboxFolder(): Keinen Msg Header 'Received' gefunden");
            }

            // Alle Received-Header löschen
            bo.MsgSummary.Headers.RemoveAll("Received");

            //++ Suche den Text vor dem Timestamp
            var timestampPos                = msgHdrReceived.LastIndexOf(';');
            var msgHdrReceived_VorTimestamp = msgHdrReceived.Substring(0, timestampPos);

            //++ Den neuen Header definieren
            var newReceivedHdr = $"{msgHdrReceived_VorTimestamp}; {neuesDocDatum.ØTo_Rfc822Rfc2822_Str()}";

            //+ Den Header setzen
            // bo.MsgSummary.Headers["Received"] = newReceivedHdr;
            bo.MsgSummary.Headers.Add("Received", newReceivedHdr);

            // Allenfalls auch den Msg Hdr setzen
            if (bo.HasMsg) {
               // Alle Received-Header löschen
               bo.Msg.Headers.RemoveAll("Received");

               // Den Header zufügen
               bo.Msg.Headers.Add("Received", newReceivedHdr);
            }

            /* !KH: Den Msg Header Timestamp analysieren
               var msgHdrReceived_Timestamp    = msgHdrReceived.ØSubstring(timestampPos + 1).Trim();
               //++ Wenn wir keinen Timestamp haben
               var timeStamp = Extract_RFC2822_Timestamp(msgHdrReceived_Timestamp);
               if (!timeStamp.ØIs_Rfc822Rfc2822_Date()) {
                  throw new RuntimeException("ProcessMailboxFolder(): Im Msg Header 'Received' keinen Zeitstempel gefunden");
               }
               else {
                  // timeStamp hat einen wahrscheinlich gültigen RFC 822 / 2822 Timestamp
               }
            */

            bo.Save(true, true);
         }
      }
   }

   #endregion Iterator-Funktionen

}
