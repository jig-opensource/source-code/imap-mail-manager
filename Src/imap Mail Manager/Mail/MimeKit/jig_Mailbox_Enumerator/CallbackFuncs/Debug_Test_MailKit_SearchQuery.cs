using System;
using System.Collections.Generic;
using System.Diagnostics;
using ImapMailManager.BO;
using jig.Tools;
using jig.Tools.String;
using MailKit;
using MailKit.Search;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Bereinigt alle E-Mail Subjects:
/// - Bereinigt die Subject-Präfixe RE: FWD, etc.
/// - Setzt bei fremdsprachigen Mails den 
/// - Setzt bei fremdsprachigen Mails den das Präfix mit der Sprache 
/// </summary>
public class Debug_Test_MailKit_SearchQuery : IMailboxIterator, IMailbox_FolderFilter {

   #region IMailbox_FolderFilter

   /// <summary>
   /// Die Default Methode im Interface:
   ///   StopFolderProcessing_BlackWhiteList
   /// muss ins Interface gecastet werden, damit sie aufgerufen werden kann:
   /// !KH https://stackoverflow.com/a/57763254/4795779
   /// Dies hier ist ein Prop als Cast zum Interface, damit der Aufruf hübscher gemacht werden kann 
   /// </summary>
   private IMailbox_FolderFilter iMailbox_FolderFilter => (IMailbox_FolderFilter)this;

   #endregion IMailbox_FolderFilter

   //» Config

   #region Config

   public enum Process_State { SetupMailHeaders, TestSearchMails }

   private const string MsgHdr_HdrName_forFlag_WithoutValue = "TomTomFlag";
   private const string MsgHdr_HdrName_WithValue            = "TomTomStringValue";

   /// <summary>
   /// Wenn > 0, definiert der Wert, wie viele Mails pro Ordner max. verarbeitet werden
   /// </summary>
   private const int ProcessMaxMailsPerFolder = -1;

   /// <summary>
   /// Alle wieviele E-Mails soll eine Progress-Nachricht angezeigt werden?
   /// </summary>
   private static readonly int PrintMailProgressModulo = 1;

   #endregion Config

   #region IMailbox_FolderFilter White / Blacklist

   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist { get; } = new
      (IMailbox_FolderFilter.FolderCompare.RgxMatch
       , new List<string> {
          ExtensionMethods_RegEx.ØWildcardToRegex(@"*INBOX*")
       });

   /// <summary>
   /// Wenn die Whitelist definiert ist, 
   /// dann werden diese Order nicht verarbeitet
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist_Except_StopOn { get; }
      = new
         (IMailbox_FolderFilter.FolderCompare.RgxMatch
          , new List<string> {
             // ExtensionMethods_RegEx.ØWildcardToRegex(@"*INBOX*")
             // ExtensionMethods_RegEx.ØWildcardToRegex(@"*Entwürfe*")
          });

   /// <summary>
   /// Die Mailbox-Ordnernamen, die wir nicht verarbeiten
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Blacklist { get; }
      = new
         (IMailbox_FolderFilter.FolderCompare.RgxMatch
          , new List<string> {
             "Trash", "Gelöschte Elemente", "Junk-E-Mail", "Junk"
          });

   #endregion IMailbox_FolderFilter White / Blacklist

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props


   /// <summary>
   /// Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}


   /// <summary>
   /// Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      int done = 1;

      int stopper = 2;

      /*
            StringBuilder sb = new StringBuilder();
      
            foreach (var subject in MailSubjects.OrderBy(x => x)) {
               sb.AppendLine(subject);
            }
      
            // 🚩 Debug: Ins ClipBoard kopieren
            ClipboardService.SetText(sb.ToString());
            
            Debug.WriteLine(sb.ToString());
      */

   }


   /// <summary>
   /// Verarbeitet den übergebenen Ordner
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {

      // Nötig, um den Inhalt zu erfahren
      mbxFolder.Open(FolderAccess.ReadOnly);
      var anzMails = mbxFolder.Count;
      Debug.WriteLine($"{mbxFolder.FullName}");

      //+ Die Blacklisted Ordner überspringen
      if (iMailbox_FolderFilter.StopFolderProcessing_BlackWhiteList(mbxFolder)) {
         Debug.WriteLine($"  » skipped");

         return;
      }

      // ‼ Konfig der Funktion
      // Process_State state = Process_State.SetupMailHeaders;
      Process_State state = Process_State.TestSearchMails;

      IList<IMessageSummary> messageSummaries = null;

      if (state == Process_State.SetupMailHeaders) {
         //» Die Message Summaries holen und nach Datum absteigend sortieren
         // !KQ https://stackoverflow.com/a/58580564/4795779						
         messageSummaries = mbxFolder.Fetch
            (0
             , -1
             , MessageSummaryItems.Envelope

               // | MessageSummaryItems.BodyStructure
               // | MessageSummaryItems.Size
               // https://github.com/jstedfast/MailKit/issues/1254#issuecomment-1277881417
               | MessageSummaryItems.UniqueId
               | MessageSummaryItems.Headers

               // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
               | MessageSummaryItems.InternalDate
               | MessageSummaryItems.References
               | MessageSummaryItems.Flags);
         messageSummaries.Sort(new[] { OrderBy.ReverseDate });
      }

      if (state == Process_State.TestSearchMails) {

         // ‼ !KH9^9 MailKit Suche - getestete Beispiele  
         //» Suche 1: Findet alle Mails,
         //» die den Header TomTomFlag haben (unabhängig vom Inhalt des Headers)
         var uids1 = mbxFolder.Search
            (SearchQuery.HeaderContains("TomTomFlag", ""));

         IList<IMessageSummary> uids1_MessageSummaries = mbxFolder.Fetch
            (uids1
             , MessageSummaryItems.Envelope
               | MessageSummaryItems.UniqueId

               // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
               | MessageSummaryItems.InternalDate
               | MessageSummaryItems.Headers
               | MessageSummaryItems.References
               | MessageSummaryItems.Flags);

         //» Suche 2: Findet alle Mails,
         //» die den Header TomTomFlag NICHT haben (der Header hat den Wert "")
         var uids2 = mbxFolder.Search
            (SearchQuery.Not(SearchQuery.HeaderContains("TomTomFlag", "")));

         IList<IMessageSummary> uids2_MessageSummaries = mbxFolder.Fetch
            (uids2
             , MessageSummaryItems.Envelope
               | MessageSummaryItems.UniqueId
               | MessageSummaryItems.Headers

               // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
               | MessageSummaryItems.InternalDate
               | MessageSummaryItems.References
               | MessageSummaryItems.Flags);

         //» Suche 3: Alle Mails,
         //» die TomTomStringValue mit irgend einem Wert haben

         var uids3 = mbxFolder.Search(SearchQuery.HeaderContains("TomTomStringValue", ""));

         IList<IMessageSummary> uids3_MessageSummaries = mbxFolder.Fetch
            (uids3
             , MessageSummaryItems.Envelope
               | MessageSummaryItems.UniqueId
               | MessageSummaryItems.Headers

               // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
               | MessageSummaryItems.InternalDate
               | MessageSummaryItems.References
               | MessageSummaryItems.Flags);

         //» Suche 4: Alle Mails,
         //» die TomTomStringValue = "Ex Test #3" haben

         var uids4 = mbxFolder.ØSearch_Header_ValueEquals("TomTomStringValue", "Ex Test Nummer 3");

         IList<IMessageSummary> uids4_MessageSummaries = mbxFolder.Fetch
            (uids4
             , MessageSummaryItems.Envelope
               | MessageSummaryItems.UniqueId
               | MessageSummaryItems.Headers

               // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
               | MessageSummaryItems.InternalDate
               | MessageSummaryItems.References
               | MessageSummaryItems.Flags);

         int stopper1 = 1;

      }

      int stopper2 = 1;

      //+ Alle gefundenen Mails bearbeiten
      int msgCnt       = anzMails;
      int msgProcessed = 0;

      if (messageSummaries != null)
         foreach (var msgSummary in messageSummaries) {
            // Wenn wir die vorgesehene Anzahl erreicht haben, überspringen
            if (ProcessMaxMailsPerFolder > 0
                && msgProcessed > ProcessMaxMailsPerFolder) {
               break;
            }

            if (--msgCnt%PrintMailProgressModulo == 0 || msgCnt == anzMails - 1) {
               Debug.WriteLine($"    ↻ {msgCnt,4} {msgSummary.Envelope.Subject}");
            }

            #region Process_State.SetupMailHeaders

            if (state == Process_State.SetupMailHeaders) {
               //» Test #1: Mail ohne besondere Header 
               //» Test #2: Mail mit leerem Header, der als Flag dient
               if (msgSummary.Envelope.Subject.ØStartsWith
                      ("Test #2: Mail mit leerem Header, der als Flag dient", StringComparison.OrdinalIgnoreCase)) {
                  //++ Obj erzeugen
                  BO_Mail boTest2 = new BO_Mail
                     (imapSrvRuntimeConfig
                      , mbxFolder
                      , msgSummary);

                  boTest2.MsgHdr_UpdateOrSet(MsgHdr_HdrName_forFlag_WithoutValue, "");
                  boTest2.Save(true);

                  continue;
               }

               //» Test #3: Mail mit Header & Text
               if (msgSummary.Envelope.Subject.ØStartsWith("Test #3-2: ", StringComparison.OrdinalIgnoreCase)) {
                  //++ Obj erzeugen
                  BO_Mail boTest3 = new BO_Mail
                     (imapSrvRuntimeConfig
                      , mbxFolder
                      , msgSummary);

                  boTest3.MsgHdr_UpdateOrSet(MsgHdr_HdrName_WithValue, "Ex Test Nummer 3");
                  boTest3.Save(true);

                  continue;
               }

               //» Test #4: Mail mit Header & Text
               if (msgSummary.Envelope.Subject.ØStartsWith("Test #4-2: ", StringComparison.OrdinalIgnoreCase)) {
                  //++ Obj erzeugen
                  BO_Mail boTest4 = new BO_Mail
                     (imapSrvRuntimeConfig
                      , mbxFolder
                      , msgSummary);

                  boTest4.MsgHdr_UpdateOrSet(MsgHdr_HdrName_WithValue, "Ex Test Nummer 4");
                  boTest4.Save(true);

                  continue;
               }

               continue;
            }

            #endregion Process_State.SetupMailHeaders

            var srchSubject = @"";
            srchSubject = @"Test #1: Mail ohne besondere Header";
            srchSubject = @"Test #2: Mail mit leerem Header, der als Flag dient";
            srchSubject = @"Test #3: Mail mit Header & Text";
            srchSubject = @"Test #4: Mail mit Header & Text";
            srchSubject = @"";

            if ((srchSubject.ØHasValue() && msgSummary.Envelope.Subject == null)
                || (srchSubject.ØHasValue()
                    && msgSummary.Envelope.Subject != null
                    && !msgSummary.Envelope.Subject.Contains(srchSubject))) {
               continue;
            }

            //++ Obj erzeugen
            BO_Mail bo = new BO_Mail
               (imapSrvRuntimeConfig
                , mbxFolder
                , msgSummary);

            // bo.MsgHdr_UpdateOrSet(MsgHdr_HdrName_forFlag_WithoutValue, "");
            // bo.MsgHdr_UpdateOrSet(MsgHdr_HdrName_WithValue, "Ex Test #3");
            bo.MsgHdr_UpdateOrSet(MsgHdr_HdrName_WithValue, "Ex Test #4");
            bo.Save(true, true);

            // private const string MsgHdr_HdrName_WithValue = "TomTomStringValue";

            if (!bo.HasHeaderSet_Metadaten) {
               // Debug.WriteLine($"    » Skipping: {msgSummary.Envelope.Subject}");
               continue;
            }

            Debug.WriteLine($"    » Clean Subject: {msgCnt,4} {msgSummary.Envelope.Subject}");

            bo.Cleanup_Subject
               (true
                , true
                , true);

            msgProcessed++;
         }
   }

   #endregion Iterator-Funktionen

}
