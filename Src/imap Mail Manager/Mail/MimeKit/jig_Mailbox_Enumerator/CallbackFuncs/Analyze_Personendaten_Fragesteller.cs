using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ImapMailManager.Config;
using ImapMailManager.Mail.Parser.Kontaktformular.Personendaten;
using MailKit;
using MailKit.Search;
using MimeKit;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Sucht alle E-Mails, die keinen dieser Msg Header haben:
/// - AppMailHeaderType.FragestellerPersonalDaten
/// - AppMailHeaderType.FragestellerPersonalDatenNotFound
/// 
/// Bei diesen E-Mails wird
/// - der Msg Body analysiert,
/// - die Kontaktinformationen der Fragesteller ausgelesen
/// - und dann im Msg Header verschlüsselt gespeichert
/// </summary>
public class AnalyzePersonenDatenFragesteller : IMailboxIterator {

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props

   /// <summary>
   /// Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}
   
   /// <summary>
   /// Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      int done = 1;
   }

   /// <summary>
   /// Verarbeitet den übergebenen Ordner
   /// 
   /// 
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {
      //+ Alle Mails, denen der Header fehlt suchen
      
      //+ Die Header-Namen bestimmen, die in diesem Prozess aktualisiert werden sollen
      var hdr1Name =
         ConfigMail_MsgHeader.GetHeadername
            (ConfigMail_MsgHeader.AppMailHeaderType
                                 .FragestellerPersoenlicheDaten);

      var hdr2Name =
         ConfigMail_MsgHeader.GetHeadername
            (ConfigMail_MsgHeader.AppMailHeaderType
                                 .MailKontaktformularDatenFound);

      // Nötig, um den Inhalt zu erfahren
      mbxFolder.Open(FolderAccess.ReadOnly);
      Debug.WriteLine($"{mbxFolder.FullName}");

      IList<UniqueId>? uids = null;

      if (force) {
         //+ Alle suchen
         uids = mbxFolder.Search(SearchQuery.All);
      }
      else {
         //+ Die UIDs suchen, denen der Header fehlt
         uids = mbxFolder.Search
            (SearchQuery.And

                //+++ Hat weder FragestellerPersonalDaten noch FragestellerPersonalDatenNotFound
                ((SearchQuery.Not(SearchQuery.HeaderContains(hdr1Name,   string.Empty)))
                 , (SearchQuery.Not(SearchQuery.HeaderContains(hdr2Name, string.Empty))))
            );
      }

      // Debug.WriteLine($"  Anzahl: {uids.Count}");


      // Sortieren: zuerst die neuste E-Mail
      // !Q https://stackoverflow.com/a/58580564/4795779						
      // var messages = imapFolder.Fetch(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.InternalDate).ToList();
      // messages.Sort(new OrderBy[] { OrderBy.ReverseDate });


      //+ die Message Summaries holen
      IList<IMessageSummary>? messageSummaries = mbxFolder.Fetch
         (uids
          , MessageSummaryItems.UniqueId
            | MessageSummaryItems.BodyStructure
            | MessageSummaryItems.Flags);

      //+ Jede Msg lesen
      foreach (var messageSummary in messageSummaries) {
         //++ Die Msg lesen
         MimeMessage? msg = mbxFolder.GetMessage(messageSummary.UniqueId);

         //‼ 🚩 Debug
         Debug.WriteLine($"    » {messageSummary.Envelope.Subject}");

         //+ Den aktuellen Lese-State bestimmen
         var isMsg_marked_AsRead = MimeKit_JigTools.Is_Msg_Marked_AsRead(messageSummary);

         //+ Header entfernen, die sogleich zugefügt werden
         //‼ Msg Header sind nicht Unique!, der gleiche Header kann also mehrmals zugefügt werden

         //++ Header FragestellerPersonalDaten löschen
         msg.Headers.Where
            ((h => h.Field
                   == ConfigMail_MsgHeader.GetHeadername
                      (ConfigMail_MsgHeader.AppMailHeaderType
                                           .FragestellerPersoenlicheDaten))).ToList().ForEach
            (x => msg
                 .Headers
                 .Remove(x));

         //++ Header FragestellerPersonalDaten löschen
         msg.Headers.Where
            ((h => h.Field
                   == ConfigMail_MsgHeader.GetHeadername
                      (ConfigMail_MsgHeader.AppMailHeaderType
                                           .MailKontaktformularDatenFound))).ToList().ForEach
            (x => msg
                 .Headers
                 .Remove(x));

         //+++ Die Daten berechnen
         var recFragesteller = Parse_MailBody.Find_Kontaktformular_Fragesteller_Daten(msg);

         //+++ Wenn wir Formulardaten gefunden haben
         if (recFragesteller != null) {
            //+ Diese speichern
            //++ Den Record verschlüsseln
            var encryptedRecord = recFragesteller.EncryptRecord(ConfigMail_MsgHeader.EncryptionPW);

            //++ Den Header ergänzen
            msg.Headers.Add
               (ConfigMail_MsgHeader.GetHeadername
                   (ConfigMail_MsgHeader.AppMailHeaderType.FragestellerPersoenlicheDaten)
                , encryptedRecord);
         }
         else {
            //+ Keine Formulardaten gefunden
            //++ Den Header setzen
            msg.Headers.Add
               (ConfigMail_MsgHeader.GetHeadername
                   (ConfigMail_MsgHeader.AppMailHeaderType.MailKontaktformularDatenFound)
                , ConfigMail_MsgHeader.NoFormDataValue);
         }

         //+ Die Msg wieder speichern 
         ModifyImapEMail.SaveMessage
            (imapSrvRuntimeConfig.ImapClient
             , messageSummary
             , mbxFolder
             , msg, true);

      }

      int stopper = 1;
   }

   #endregion Iterator-Funktionen

}
