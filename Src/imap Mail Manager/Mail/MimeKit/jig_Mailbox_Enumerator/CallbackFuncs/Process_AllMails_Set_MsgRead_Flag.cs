using System;
using System.Collections.Generic;
using System.Diagnostics;
using ImapMailManager.BO;
using jig.Tools;
using MailKit;
using MailKit.Search;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Sucht alle Mails in den definierten Mbx-Ordnern
/// und setzt die Mails auf ungelesen 
/// </summary>
public class Process_AllMails_Set_MsgRead_Flag : IMailboxIterator {

   //‼ Config

   #region Config

   /// <summary>
   /// Die Mailbox-Ordnernamen, die wir verarbeiten
   /// </summary>
   private static readonly List<string> MbxFolder_Whitelist = new List<string>
      (new[] {
         "Kontaktformular ★"
      });
   
   /// <summary>
   /// Die Mailbox-Ordnernamen, die wir nicht verarbeiten
   /// </summary>
   private static readonly List<string> MbxFolderBlacklist = new List<string>
      (new[] {
         "Trash", "Gelöschte Elemente", "Junk-E-Mail", "Junk"
      });

   #endregion Config

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props

   /// <summary>
   /// Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}


   /// <summary>
   /// Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      int done = 1;

      int stopper = 2;

      /*
            StringBuilder sb = new StringBuilder();
      
            foreach (var subject in MailSubjects.OrderBy(x => x)) {
               sb.AppendLine(subject);
            }
      
            // 🚩 Debug: Ins ClipBoard kopieren
            ClipboardService.SetText(sb.ToString());
            
            Debug.WriteLine(sb.ToString());
      */

   }


   /// <summary>
   /// Verarbeitet den übergebenen Ordner
   /// 
   /// 
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {

      // Nötig, um den Inhalt zu erfahren
      mbxFolder.Open(FolderAccess.ReadOnly);
      Debug.WriteLine($"{mbxFolder.FullName}");

      //+ Die Blacklisted Ordner überspringen
      /*
      if (MbxFolderBlacklist.ØRgxMatchWithAnyItemInList(mbxFolder.FullName)) {
         Debug.WriteLine($"  » skipped");
         return;
      }
      */

      //+ Wenn icht in der Whitelist: überspringen 
      if (!MbxFolder_Whitelist.ØRgxMatchWithAnyItemInList(mbxFolder.FullName)) {
         Debug.WriteLine($"  » skipped");

         return;
      }

      //+ Im Ordner alle gelesenen Mails abrufen
      var uids = mbxFolder.Search(SearchQuery.Seen);

      // 🚩 Debug.WriteLine($"  Anzahl: {uids.Count}");

      //+ Das gelesen Flag löschen
      mbxFolder.Open(FolderAccess.ReadWrite);
      // !KH https://stackoverflow.com/a/31994027/4795779
      mbxFolder.RemoveFlags(uids, MessageFlags.Seen, true);

   }

   #endregion Iterator-Funktionen

}
