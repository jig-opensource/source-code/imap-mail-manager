using System;
using System.Collections.Generic;
using System.Diagnostics;
using ImapMailManager.BO;
using ImapMailManager.Config;
using jig.Tools;
using jig.Tools.String;
using MailKit;
using MailKit.Search;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Analysiert alle E-Mails und aktualisiert die Metadaten
/// </summary>
public class Process_AllMails_Update_MetaData : IMailboxIterator, IMailbox_FolderFilter {

   #region IMailbox_FolderFilter

   /// <summary>
   /// Die Default Methode im Interface:
   ///   StopFolderProcessing_BlackWhiteList
   /// muss ins Interface gecastet werden, damit sie aufgerufen werden kann:
   /// !KH https://stackoverflow.com/a/57763254/4795779
   /// Dies hier ist ein Prop als Cast zum Interface, damit der Aufruf hübscher gemacht werden kann 
   /// </summary>
   private IMailbox_FolderFilter iMailbox_FolderFilter => (IMailbox_FolderFilter)this;

   #endregion IMailbox_FolderFilter

   // Zählt für jede Sprache die Anzahl Mails
   private static Dictionary<string, int> mailinhalt_SprachenStat = new Dictionary<string, int>();
   private static Dictionary<string, List<string>>
      mailinhalt_SubjectProSprache = new Dictionary<string, List<string>>();

   
   //‼ Config

   #region Config

   private static readonly bool Verbose = false;

   /// <summary>
   /// Wenn > 0, definiert der Wert, wie viele Mails pro Ordner max. verarbeitet werden
   /// </summary>
   private static readonly int ProcessMaxMailsPerFolder = -1;
   /// <summary>
   /// Alle wieviele E-Mails soll eine Progress-Nachricht angezeigt werden?
   /// </summary>
   private static readonly int PrintMailProgressModulo = 20;

   #endregion Config

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props

   #region IMailbox_FolderFilter

   /// <summary>
   /// Die Liste der Whitelist-Ordner
   /// Hat Priorität gegenüber der Blacklist:
   /// Wenn die Whitelist definiert ist, dann wird die Blacklist ignoriert
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist { get; }
   /// <summary>
   /// Wenn die Whitelist definiert ist, 
   /// dann werden diese Order nicht verarbeitet
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist_Except_StopOn { get; }
   /// <summary>
   /// Die Liste der Blacklist-Ordner
   /// Wird nur angewendet, wenn die Whitelist nicht definiert ist
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Blacklist { get; } = new
      (IMailbox_FolderFilter.FolderCompare.RgxMatch
       , new List<string> {
          ExtensionMethods_RegEx.ØWildcardToRegex(@"Trash*".Replace('\\', '/'))          
          ,ExtensionMethods_RegEx.ØWildcardToRegex(@"Gelöschte Elemente*".Replace('\\', '/'))          
          ,ExtensionMethods_RegEx.ØWildcardToRegex(@"Junk-E-Mail".Replace('\\', '/'))          
          ,ExtensionMethods_RegEx.ØWildcardToRegex(@"Junk".Replace('\\', '/'))          
       });

   
   
   #endregion IMailbox_FolderFilter

   
   /// <summary>
   /// » Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {
   }


   /// <summary>
   /// » Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      //‼ 🚩 Debug
      int done = 1;
   }


   /// <summary>
   /// » Analysiert alle E-Mails und aktualisiert die Metadaten
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {

      // Nötig, um den Inhalt abzurufen
      mbxFolder.Open(FolderAccess.ReadOnly);
      var anzMails = mbxFolder.Count;
      Debug.WriteLine($"{mbxFolder.FullName} ({anzMails})");

      //+ Die Blacklisted Ordner überspringen
      if (iMailbox_FolderFilter.StopFolderProcessing_BlackWhiteList(mbxFolder)) {
         Debug.WriteLine($"  » skipped");
         return;
      }

      //+ Die Message Summaries holen
      // Absteigend sortiert: die neuste Mail zuerst / Empfangsdatum
      // Das zweite Argument -1
      // !KH9 https://stackoverflow.com/a/29121052/4795779
      IList<IMessageSummary> messageSummaries = mbxFolder.Fetch
         (0
          , -1
          , MessageSummaryItems.Envelope
            | MessageSummaryItems.BodyStructure
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Size
            | MessageSummaryItems.Headers

            // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
            | MessageSummaryItems.InternalDate
            | MessageSummaryItems.Flags);

      // Sortieren: zuerst die neuste E-Mail
      messageSummaries.Sort(new[] { OrderBy.ReverseDate });

      int msgCnt       = anzMails;
      int msgProcessed = 0;

      foreach (var msgSummary in messageSummaries) {
         // Wenn wir die vorgesehene Anzahl erreicht haben, überspringen
         if (ProcessMaxMailsPerFolder > 0
             && msgProcessed > ProcessMaxMailsPerFolder) {
            break;
         }

         if (--msgCnt % PrintMailProgressModulo == 0 || msgCnt == anzMails - 1) {
            Debug.WriteLine($"    ↻ {msgCnt,4} {msgSummary.Envelope.Subject}");
         }

         //‼ 🚩 Debug
         var srchSubject = @"";
         srchSubject = @"";
         if (srchSubject.ØHasValue()
             && (
                   msgSummary.Envelope.Subject == null
                   || !msgSummary.Envelope.Subject.Contains(srchSubject))) {
            continue;
         }

         //+ Obj erzeugen
         BO_Mail bo = new BO_Mail
            (imapSrvRuntimeConfig
             , mbxFolder
             , msgSummary);

         // Alle App-Spezifischen Mails (✅ Dok:) Überspringen
         if (bo.IsAppSpecificMail) { continue; }
         
         //‼ 🚩 Debugging
         if (Verbose) Debug.WriteLine($"    » {msgSummary.Envelope.Subject}");
         if (Verbose) Debug.WriteLine($"      » [{msgProcessed,4}] Ergänze Metadaten");
         msgProcessed++;

         lock (mbxFolder.SyncRoot) {

            // Die Metadaten berechnen
            bo.Update_Mail_MetaDaten(false);

/*         
            bo.Cleanup_Subject
               (true
                , true
                , true
                , true);
*/
            //+ Um absolut sicher zu sein...
            // bo.Save();
         }
      }
   }

   #endregion Iterator-Funktionen

}
