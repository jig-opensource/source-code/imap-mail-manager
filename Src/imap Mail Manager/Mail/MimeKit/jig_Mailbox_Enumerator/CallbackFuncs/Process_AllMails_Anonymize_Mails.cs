using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ImapMailManager.BO;
using jig.Tools;
using jig.Tools.String;
using MailKit;
using MailKit.Search;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Anonymisiert alle E-Mails im Archiv-Postfach
/// </summary>
public class Process_AllMails_Anonymize_Mails : IMailboxIterator {

   //‼ Config

   #region Config

   private static readonly bool Verbose = false;

   //‼ 🚩 Debugging
   /// <summary>
   /// Wenn true, dann werden nur E-Mails anonymisiert, die Metadaten haben
   /// ‼ Überschreibt ProcessMaxMailsPerFolder
   /// </summary>
   private static readonly bool ProcessOnlyMailsWithMetadata = false;

   /// <summary>
   /// Wenn > 0, definiert der Wert, wie viele Mails pro Ordner max. verarbeitet werden
   /// ‼ Wird von ProcessOnlyMailsWithMetadata übersteuert, wenn true!
   /// </summary>
   private static readonly int ProcessMaxMailsPerFolder = -1;

   /// <summary>
   /// Die Mailbox-Ordnernamen, die wir nicht verarbeiten
   /// </summary>
   private static readonly List<string> MbxFolderBlacklist = new List<string>
      (new[] {
         "Trash", "Gelöschte Elemente", "Junk-E-Mail", "Junk"
      });

   /// <summary>
   /// Die Mailbox-Ordnernamen, die wir verarbeiten
   /// (anonymisiert!)
   /// </summary>
   private static readonly List<string> MbxFolder_Whitelist = new List<string>
      (new[] {
         ExtensionMethods_RegEx.ØWildcardToRegex(@"*INBOX*")
         , ExtensionMethods_RegEx.ØWildcardToRegex(@"*2b Frage Allgemein ★*")
         , ExtensionMethods_RegEx.ØWildcardToRegex(@"*90 Erledigt ★*"),
      });

   #endregion Config

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props


   /// <summary>
   /// Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}


   /// <summary>
   /// Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      //‼ 🚩 Debug
      int done = 1;
   }


   /// <summary>
   /// Analysiert alle E-Mails und aktualisiert die Metadaten
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {

      // ‼ Sicherstellen, dass die Funkon nie in der produktiven frontend Mailbox gestartet wird
      if (imapSrvRuntimeConfig!.MailboxIsProductive
          && !imapSrvRuntimeConfig!.IsArchiveMailbox) { return; }

      // Nötig, um den Inhalt abzurufen
      mbxFolder.Open(FolderAccess.ReadOnly);
      var anzMails = mbxFolder.Count;
      Debug.WriteLine($"{mbxFolder.FullName}");

      //+ Die Blacklisted Ordner überspringen
      /*
      if (MbxFolderBlacklist.ØRgxMatchWithAnyItemInList(mbxFolder.FullName)) {
         Debug.WriteLine($"  » skipped");
         return;
      }
      */

      //+ Wenn icht in der Whitelist: überspringen 
      if (!MbxFolder_Whitelist.ØRgxMatchWithAnyItemInList(mbxFolder.FullName)) {
         if (Verbose) Debug.WriteLine($"  » skipped");

         return;
      }

      /*      
            // ‼ 📌 Mailbox-Ordner Filter
      
            //++++ Überspringen, bis wir den Ordner gefunden haben
            var srchFolder = @"";
            srchFolder = @"Eschatologie";
            srchFolder = @"2b Frage Allgemein (anonymisiert!)";
            srchFolder = @"2b Frage Allgemein";
            srchFolder = @"90 Erledigt";
            srchFolder = @"99 Tests Thomas ★";
            srchFolder = @"INBOX";
            srchFolder = @"";
      */

      //    var srchFolders = new List<string>( new []{ @".*/2b Frage Allgemein.*", @".*/90 Erledigt.*" });
      /*
            // if (srchFolder.ØHasValue() && !mbxFolder.FullName.ØEndsWithIgnoreCase(srchFolder)) {
            // if (srchFolder.ØHasValue() && !mbxFolder.FullName.StartsWith(srchFolder)) {
            
            // if (srchFolder.ØHasValue() && !mbxFolder.FullName.Contains(srchFolder)) {
            if (!srchFolders.ØRgxMatchWithAnyItemInList(mbxFolder.FullName)) {
               Debug.WriteLine($"  » skipped");
               return;
            }
      */

      // ToDo Now 🟥 : Die Funktion sucht nur jene Mails, in denen die jig Hdr fehlen 
      // 🚩 Debug.WriteLine($"  Anzahl: {uids.Count}");

      //+ Die Message Summaries holen
      // ‼ Absteigend sortiert: die neuste Mail zuerst / Empfangsdatum
      // Das zweite Argument -1
      // !KH9 https://stackoverflow.com/a/29121052/4795779

      // Sortieren: zuerst die neuste E-Mail
      // !Q https://stackoverflow.com/a/58580564/4795779						
      // var messages = imapFolder.Fetch(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.InternalDate).ToList();
      // messages.Sort(new OrderBy[] { OrderBy.ReverseDate });

      IList<IMessageSummary> messageSummaries = mbxFolder.Fetch
         (0
          , -1
          , MessageSummaryItems.Envelope
            | MessageSummaryItems.BodyStructure
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Size
            | MessageSummaryItems.Headers
            | MessageSummaryItems.Flags);
      messageSummaries.Sort(new[] { OrderBy.ReverseDate });

      //+ Test_BO_Mail: Jede Msg lesen
      int msgCnt       = anzMails;
      int msgProcessed = 0;

      foreach (var msgSummary in messageSummaries) {
         // Wenn wir die vorgesehene Anzahl erreicht haben, überspringen
         if (!ProcessOnlyMailsWithMetadata
             && ProcessMaxMailsPerFolder > 0

             // && anzMails - msgCnt > ProcessMaxMailsPerFolder) {
             && msgProcessed > ProcessMaxMailsPerFolder) {
            break;
         }

         //‼ 🚩 Debug
         if (msgCnt--%10 == 0 || msgCnt == anzMails - 1) { Debug.WriteLine($"    ↻ {msgCnt,4} {msgSummary.Envelope.Subject}"); }

         //‼ 🚩 Debug
         var srchSubject = @"";
         srchSubject = @"";
         srchSubject = @"AW: #5-1918 • RL Formular: Allgemeine Frage";

         // if (srchSubject.ØHasValue() && !msgSummary.Envelope.Subject.ØEqualsIgnoreCase(srchSubject)) {
         //    continue;
         // }

         if ((srchSubject.ØHasValue() && msgSummary.Envelope.Subject == null)
             || (srchSubject.ØHasValue()
                 && msgSummary.Envelope.Subject != null
                 && !msgSummary.Envelope.Subject.Contains(srchSubject))) {
            continue;
         }

         //+ Obj erzeugen
         BO_Mail bo = new BO_Mail
            (imapSrvRuntimeConfig
             , mbxFolder
             , msgSummary);

         // Alle App-Spezifischen Mails Überspringen: ✅ Dok:
         if (bo.IsAppSpecificMail) { continue; }

         //‼ 🚩 Debug
         /*
         // Nach einem spezifischen header suchen
         var hasJigHdr= bo.MsgSummary.Headers.Where(x => x.Field.StartsWith("jig")).ToList();
         if (hasJigHdr.Count > 0) {
            int stopper = 1;
         }
         */

         if (Verbose) Debug.WriteLine($"    » {msgSummary.Envelope.Subject}");

         if (ProcessOnlyMailsWithMetadata && !bo.HasHeaderSet_Metadaten) {
            if (Verbose) Debug.WriteLine($"      🛈 Skipping: Keine Metadaten");

            continue;
         }

         if (bo.HasHeaderSet_MailInhaltAnonymisiert) {
            if (Verbose) Debug.WriteLine($"      ⓘ Skipping: Ist anonymisiert");

            continue;
         }

         if (Verbose) Debug.WriteLine($"      » [{msgProcessed,4}] Anonymisiere");
         msgProcessed++;

         //+ Mail analysieren und alle Daten aktualisieren         
         bo.Anonymize_Mail();

         //‼ 🚩 Debug

      }
   }

   #endregion Iterator-Funktionen

}
