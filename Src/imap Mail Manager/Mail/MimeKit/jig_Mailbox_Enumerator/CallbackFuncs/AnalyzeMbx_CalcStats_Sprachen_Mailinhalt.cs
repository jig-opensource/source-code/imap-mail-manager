using System.Collections.Generic;
using System.Diagnostics;
using ImapMailManager.BO;
using jig.Tools;
using MailKit;
using MailKit.Search;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Analysiert alle E-Mails und berechnet Sprachen der Mail-Texte 
/// </summary>
public class AnalyzeMbx_CalcStats_Sprachen_Mailinhalt : IMailboxIterator {

   // Zählt für jede Sprache die Anzahl Mails
   private static Dictionary<string, int> mailinhalt_SprachenStat = new Dictionary<string, int>();
   private static Dictionary<string, List<string>>
      mailinhalt_SubjectProSprache = new Dictionary<string, List<string>>();

   //‼ Config

   #region Config

   /// <summary>
   /// Wenn > 0, definiert der Wert, wie viele Mails pro Ordner max. verarbeitet werden
   /// </summary>
   private static readonly int ProcessMaxMailsPerFolder = -1;

   /// <summary>
   /// Die Mailbox-Ordnernamen, die wir verarbeiten
   /// </summary>
   private static readonly List<string> MbxFolder_Whitelist = new List<string>
      (new[] {
         "Kontaktformular ★"
      });

   /// <summary>
   /// Die Mailbox-Ordnernamen, die wir nicht verarbeiten
   /// </summary>
   private static readonly List<string> MbxFolderBlacklist = new List<string>
      (new[] {
         ""

         // "Trash", "Gelöschte Elemente", "Junk-E-Mail", "Junk"
      });

   #endregion Config

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props
   
   /// <summary>
   /// Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}


   /// <summary>
   /// Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      //‼ 🚩 Debug
      int done = 1;
   }


   /// <summary>
   /// Analysiert alle E-Mails und aktualisiert die Metadaten
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {

      // Nötig, um den Inhalt abzurufen
      mbxFolder.Open(FolderAccess.ReadOnly);
      Debug.WriteLine($"{mbxFolder.FullName}");

      //+ Die Blacklisted Ordner überspringen
      if (MbxFolderBlacklist.ØRgxMatchWithAnyItemInList(mbxFolder.FullName)) {
         Debug.WriteLine($"  » skipped");

         return;
      }

      // ‼ 📌 Mailbox-Ordner Filter

      IList<UniqueId>? uids;

      // ToDo Now 🟥 : Die Funktion sucht nur jene Mails, in denen die jig Hdr fehlen 

      //+ Alle Mails suchen
      uids = mbxFolder.Search(SearchQuery.All);

      // 🚩 Debug.WriteLine($"  Anzahl: {uids.Count}");


      // Sortieren: zuerst die neuste E-Mail
      // !Q https://stackoverflow.com/a/58580564/4795779						
      // var messages = imapFolder.Fetch(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.InternalDate).ToList();
      // messages.Sort(new OrderBy[] { OrderBy.ReverseDate });

      
      //+ Die Message Summaries holen
      IList<IMessageSummary> messageSummaries = mbxFolder.Fetch
         (uids
          , MessageSummaryItems.Envelope
            | MessageSummaryItems.BodyStructure
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Size
            | MessageSummaryItems.Headers
            // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
            | MessageSummaryItems.InternalDate
            | MessageSummaryItems.Flags);

      //+ Test_BO_Mail: Jede Msg lesen
      int msgCnt = uids.Count;

      foreach (var msgSummary in messageSummaries) {
         // Wenn wir die vorgesehene Anzahl erreicht haben, überspringen
         if (ProcessMaxMailsPerFolder > 0
             && uids.Count - msgCnt > ProcessMaxMailsPerFolder) {
            break;
         }

         if (msgCnt--%20 == 0 || msgCnt == uids.Count - 1) {
            Debug.WriteLine($"    ↻ {msgCnt,4} {msgSummary.Envelope.Subject}");
         }

         //+ Obj erzeugen
         BO_Mail bo = new BO_Mail
            (imapSrvRuntimeConfig
             , mbxFolder
             , msgSummary);

         // Alle App-Spezifischen Mails Überspringen: ✅ Dok:
         if (bo.IsAppSpecificMail) { continue; }


         //+ Statistik nachführen
         if (mailinhalt_SprachenStat.ContainsKey
                (bo.Analyze_Mail_Get_MailInhalt_Sprache()?.ThreeLetterISOLanguageName ?? "Unbekannt")) {
            mailinhalt_SprachenStat[bo.Analyze_Mail_Get_MailInhalt_Sprache()?.ThreeLetterISOLanguageName ?? "Unbekannt"]++;
         }
         else {
            mailinhalt_SprachenStat.Add(bo.Analyze_Mail_Get_MailInhalt_Sprache()?.ThreeLetterISOLanguageName ?? "Unbekannt", 1);
         }

         //++ Stats: In jeder Sprache die E-Mail Subjects sammeln
         if (mailinhalt_SubjectProSprache.ContainsKey
                (bo.Analyze_Mail_Get_MailInhalt_Sprache()?.ThreeLetterISOLanguageName ?? "Unbekannt")) {
            var liste
               = mailinhalt_SubjectProSprache[bo.Analyze_Mail_Get_MailInhalt_Sprache()?.ThreeLetterISOLanguageName ?? "Unbekannt"];
            liste.Add(msgSummary.Envelope.Subject);
         }
         else {
            List<string> liste = new List<string>();
            liste.Add(msgSummary.Envelope.Subject);

            mailinhalt_SubjectProSprache.Add
               (bo.Analyze_Mail_Get_MailInhalt_Sprache()?.ThreeLetterISOLanguageName ?? "Unbekannt", liste);
         }

      }
   }

   #endregion Iterator-Funktionen

}
