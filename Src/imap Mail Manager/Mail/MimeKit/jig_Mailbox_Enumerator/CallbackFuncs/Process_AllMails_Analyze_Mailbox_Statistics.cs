using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using jig.Tools;
using jig.Tools.String;
using MailKit;
using MailKit.Search;
using TextCopy;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Liest alle Nachrichten und erzeugt eine Mailbox-Statistik:
/// 
///   Mailboxgrösse                     : 3.039 GB
///   Anzahl Mails                      : 2109
///   Anzahl Mail Threads               : 1563
///   Anzahl mehrfach gespeicherte Mails: 12
///   Max.Anzahl Kopien einer Mail      : 8
///   Mailgrösse pro Mail-Thread        : 1.991 MB
///   Älteste Mail                      : 09.07 .2021 00:00:00
///   Neuste Mail                       : 31.07 .2022 00:00:00
/// 
/// </summary>
public class Process_AllMails_Analyze_Mailbox_Statistics : IMailboxIterator {

   /// <summary>
   /// Sammelt von der ganzen Mailbox alle IMessageSummary
   /// </summary>
   private static readonly List<IMessageSummary> AllMsgSummary = new List<IMessageSummary>();

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props

   /// <summary>
   /// Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}


   /// <summary>
   /// Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      int done = 1;
      
      //+ Statistische Daten berechnen
      
      //++ Alle Msg Summaries analysieren
      // und die Threads zusammenfassen
      IList<MessageThread>? allMailThreads = AllMsgSummary.ØGetMessageSummaryThreads();

      
      int              anzahlMails       = AllMsgSummary.Count;
      int              anzahlMailThreads = allMailThreads.Count;
      DateTimeOffset ältesteMail       = AllMsgSummary
                                        .Where(x => x.Date > DateTimeOffset.Now.AddYears(-5))
                                        .MinBy(x => x.Date)!.Date;
      DateTimeOffset neusteMail    = AllMsgSummary.MaxBy(x => x.Date)!.Date;
      long?          mailGrösseSum = AllMsgSummary.Sum(x => x.Size);

      //+ Alle Msg Summary nach der Message-ID gruppieren
      IEnumerable<IGrouping<string, IMessageSummary>> allMsgSummary_GroupBy_MessageID = AllMsgSummary
                                                                                       .Where(x => x.Headers.Contains(("Message-ID")))
                                                                                       .GroupBy(x => x.Headers["Message-ID"]);

      //+ Alle identischen Mails suchen, d.h. due Meassge-ID haben wir mind. 2x 
      IEnumerable<IGrouping<string, IMessageSummary>> allMailsWithIdenticalMessageIDs = allMsgSummary_GroupBy_MessageID.Where(x => x.Count() >= 2);

      int anzahlMehrfachGespeicherteMails = allMailsWithIdenticalMessageIDs.Count();

      int maxAnzahlKopienEinerMail = allMailsWithIdenticalMessageIDs.OrderByDescending(x => x.Count()).First().Count();

      long? mailGrösseProMailThread = mailGrösseSum/anzahlMailThreads;

      //+ Stats ausgeben
      int stopper = 1;
      
      Debug.WriteLine("\n");
      Debug.WriteLine($"Mailboxgrösse                     : {mailGrösseSum!.Value.ØToUnitStr()}");
      Debug.WriteLine($"Anzahl Mails                      : {anzahlMails}");
      Debug.WriteLine($"Anzahl Mail Threads               : {anzahlMailThreads}");
      Debug.WriteLine($"Anzahl mehrfach gespeicherte Mails: {anzahlMehrfachGespeicherteMails}");
      Debug.WriteLine($"Max. Anzahl Kopien einer Mail     : {maxAnzahlKopienEinerMail}");
      Debug.WriteLine($"Mailgrösse pro Mail-Thread        : {mailGrösseProMailThread!.Value.ØToUnitStr()}");
      Debug.WriteLine($"Älteste Mail                      : {ältesteMail.Date}");
      Debug.WriteLine($"Neuste Mail                       : {neusteMail.Date}");
      
      stopper = 2;

      /*
      StringBuilder sb = new StringBuilder();

      foreach (var subject in MailSubjects.OrderBy(x => x)) {
         sb.AppendLine(subject);
      }

      // 🚩 Debug: Ins ClipBoard kopieren
      ClipboardService.SetText(sb.ToString());
      
      Debug.WriteLine(sb.ToString());
*/


   }


   /// <summary>
   /// Verarbeitet den übergebenen Ordner
   /// 
   /// 
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {
      //+ Alle Mails, denen der Header fehlt suchen

      // Nötig, um den Inhalt zu erfahren
      mbxFolder.Open(FolderAccess.ReadOnly);
      Debug.WriteLine($"{mbxFolder.FullName}");

      // ‼ 📌 Mailbox-Ordner Filter

      //++++ Überspringen, bis wir den Ordner gefunden haben
      var srchFolder = @"";
      srchFolder = @"10 In Arbeit: Mit Roger klären";
      srchFolder = @"1 Seelsorge";
      srchFolder = @"Ehe und Scheidung";
      srchFolder = @"2a Frage Bibel";
      srchFolder = @"AT allgemein";
      srchFolder = @"Eschatologie";
      srchFolder = @"90 Erledigt ★";
      srchFolder = @"";

      // if (srchFolder.ØHasValue() && !mbxFolder.FullName.ØEndsWithIgnoreCase(srchFolder)) {
      if (srchFolder.ØHasValue() && !mbxFolder.FullName.Contains(srchFolder, StringComparison.OrdinalIgnoreCase)) {
         Debug.WriteLine($"  » skipped");

         return;
      }

      IList<UniqueId>? uids = null;

      //+ Alle suchen
      uids = mbxFolder.Search(SearchQuery.All);

      // 🚩 Debug.WriteLine($"  Anzahl: {uids.Count}");

      //+ Die Message Summaries holen


      // Sortieren: zuerst die neuste E-Mail
      // !Q https://stackoverflow.com/a/58580564/4795779						
      // var messages = imapFolder.Fetch(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.InternalDate).ToList();
      // messages.Sort(new OrderBy[] { OrderBy.ReverseDate });

      
      IList<IMessageSummary> messageSummaries = mbxFolder.Fetch
         (uids
          , MessageSummaryItems.Envelope
            | MessageSummaryItems.BodyStructure
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Size
            | MessageSummaryItems.Headers
            // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
            | MessageSummaryItems.InternalDate
            // https://github.com/jstedfast/MailKit/issues/1254#issuecomment-1277881417
            | MessageSummaryItems.UniqueId | MessageSummaryItems.Envelope | MessageSummaryItems.References
            | MessageSummaryItems.Flags);

      // Die Msg Summaries sammeln
      AllMsgSummary.AddRange(messageSummaries);

   }

   #endregion Iterator-Funktionen

}
