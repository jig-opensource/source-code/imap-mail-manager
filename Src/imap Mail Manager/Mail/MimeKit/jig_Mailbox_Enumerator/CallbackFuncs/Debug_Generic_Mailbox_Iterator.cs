using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ImapMailManager.BO;
using ImapMailManager.Config;
using jig.Tools;
using jig.Tools.String;
using MailKit;
using MailKit.Search;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Allgemeiner Mailbox-Iterator
/// </summary>
public class Debug_Generic_Mailbox_Iterator : IMailboxIterator, IMailbox_FolderFilter {

   #region IMailbox_FolderFilter

   /// <summary>
   /// Die Default Methode im Interface:
   ///   StopFolderProcessing_BlackWhiteList
   /// muss ins Interface gecastet werden, damit sie aufgerufen werden kann:
   /// !KH https://stackoverflow.com/a/57763254/4795779
   /// Dies hier ist ein Prop als Cast zum Interface, damit der Aufruf hübscher gemacht werden kann 
   /// </summary>
   private IMailbox_FolderFilter iMailbox_FolderFilter => (IMailbox_FolderFilter)this;

   #endregion IMailbox_FolderFilter

   //‼ Config

   #region IMailbox_FolderFilter White / Blacklist

   /// <summary>
   /// Wenn die Whitelist definiert ist,
   /// dann wird die Blacklist ignoriert
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist { get; } = new
      (IMailbox_FolderFilter.FolderCompare.RgxMatch
       , new List<string> {
          // ExtensionMethods_RegEx.ØWildcardToRegex(@"*INBOX*".Replace( '\\', '/'))
       });

   /// <summary>
   /// Wenn die Whitelist definiert ist, 
   /// dann werden diese Order nicht verarbeitet
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist_Except_StopOn { get; }

   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Blacklist { get; } = new
      (IMailbox_FolderFilter.FolderCompare.RgxMatch
       , new List<string> {
          // "Trash", "Gelöschte Elemente", "Junk-E-Mail", "Junk"
          // ExtensionMethods_RegEx.ØWildcardToRegex(@"*INBOX*".Replace( '\\', '/'))
       });

   #endregion IMailbox_FolderFilter White / Blacklist

   #region Spezifische Iterator Config

   //‼ 🚩 Sammeln der Sender Anzeigenamen
   private readonly List<string> _gesammelteSenderAnzeigenamen = new();

   /// <summary>
   /// Wenn > 0, definiert der Wert, wie viele Mails pro Ordner max. verarbeitet werden
   /// </summary>
   private const int ProcessMaxMailsPerFolder = -1;

   /// <summary>
   /// Alle wieviele E-Mails soll eine Progress-Nachricht angezeigt werden?
   /// </summary>
   private static readonly int PrintMailProgressModulo = 10;

   #endregion Spezifische Iterator Config

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props


   /// <summary>
   /// » Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}


   /// <summary>
   /// » Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      int done = 1;

      // Debug.WriteLine($"No of Authors found: {_noOfAuthorsFound}");

      int stopper = 2;

      /*
            StringBuilder sb = new StringBuilder();
      
            foreach (var subject in MailSubjects.OrderBy(x => x)) {
               sb.AppendLine(subject);
            }
      
            // 🚩 Debug: Ins ClipBoard kopieren
            ClipboardService.SetText(sb.ToString());
            
            Debug.WriteLine(sb.ToString());
      */

   }


   /// <summary>
   /// Verarbeitet den übergebenen Ordner
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {

      // Nötig, um den Inhalt zu erfahren
      mbxFolder.Open(FolderAccess.ReadOnly);
      var anzMails = mbxFolder.Count;
      Debug.WriteLine($"{mbxFolder.FullName}");

      var permanentFlags    = mbxFolder.PermanentFlags;
      var permanentKeywords = mbxFolder.PermanentKeywords;

      //+ Die Blacklisted Ordner überspringen
      if (iMailbox_FolderFilter.StopFolderProcessing_BlackWhiteList(mbxFolder)) {
         Debug.WriteLine($"  » skipped");

         return;
      }

      //+ Alle Mails suchen
      //» Die Message Summaries holen und nach Datum absteigend sortieren
      // Absteigend sortiert: die neuste Mail zuerst / Empfangsdatum
      // Das zweite Argument -1
      // !KH9 https://stackoverflow.com/a/29121052/4795779
      IList<IMessageSummary> messageSummaries = mbxFolder.Fetch
         (0
          , -1
          , MessageSummaryItems.Envelope
            | MessageSummaryItems.BodyStructure
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Size
            | MessageSummaryItems.Headers

            // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
            | MessageSummaryItems.InternalDate
            | MessageSummaryItems.Flags);
      messageSummaries.Sort(new[] { OrderBy.ReverseDate });

      // 🚩 Debug.WriteLine($"  Anzahl: {messageSummaries.Count}");

      //+ Alle gefundenen Mails bearbeiten
      int msgCnt       = anzMails;
      int msgProcessed = 0;

      foreach (var msgSummary in messageSummaries) {
         // Wenn wir die vorgesehene Anzahl erreicht haben, überspringen
         if (ProcessMaxMailsPerFolder > 0
             && msgProcessed > ProcessMaxMailsPerFolder) {
            break;
         }

         if (--msgCnt%PrintMailProgressModulo == 0 || msgCnt == anzMails - 1) {
            Debug.WriteLine($"    ↻ {msgCnt,4} {msgSummary.Envelope.Subject}");
         }

         mbxFolder.Open(FolderAccess.ReadOnly);

         //‼ 🚩 Debug: Eine E-Mail mit bestimmtem Betreff suchen?
         var srchSubject = @"";
         srchSubject = @"Skripte BST 25.2.23";
         srchSubject = @"";

         if ((srchSubject.ØHasValue() && msgSummary.Envelope.Subject == null)
             || (srchSubject.ØHasValue()
                 && msgSummary.Envelope.Subject != null
                 && !msgSummary.Envelope.Subject.Contains(srchSubject))) {
            continue;
         }

         //» Das BO Mail erzeugen
         BO_Mail bo = new BO_Mail
            (imapSrvRuntimeConfig
             , mbxFolder
             , msgSummary);

         if (bo.Get_MsgHdr_MailKategorie() != ConfigMail_MsgHeader.MailKategorie.AntwortVomTeam) {
            continue;
            ;
         }

         var fromDisplayNames = msgSummary.Envelope
                                          .From.Where(m => m.Name.ØHasValue())
                                          .Select(m => m.Name);

         foreach (var fromDisplayName in fromDisplayNames) {
            _gesammelteSenderAnzeigenamen.ØAppendItem_IfMissing(fromDisplayName);
         }

         var senderDisplayNames = msgSummary.Envelope
                                            .Sender.Where(m => m.Name.ØHasValue())
                                            .Select(m => m.Name);

         foreach (var senderDisplayName in senderDisplayNames) {
            _gesammelteSenderAnzeigenamen.ØAppendItem_IfMissing(senderDisplayName);
         }

         continue;

         //‼ 🚩 Testen des Speicherns einer E-Mail
         // bo.Save(@"c:\Temp\¦\aaaaaaaaa\", true, true);

         continue;

         //+ Alle App-Spezifischen Mails Überspringen: ✅ Dok:
         if (bo.IsAppSpecificMail) { continue; }

         //+ Wenn die MailKategorie fehlt oder nicht AntwortVomTeam ist, dann weiter 
         if (!bo.HasHeaderSet_MailKategorie
             || bo.Get_MsgHdr_MailKategorie() != ConfigMail_MsgHeader.MailKategorie.AntwortVomTeam) {
            continue;
         }

         // !KH http://www.mimekit.net/docs/html/M_MailKit_IMailFolder_Close.htm
         // Hilft vielleicht, um die Änderung auf dem Server zu persistieren
         // > Closes the folder, optionally expunging the messages marked for deletion.
         mbxFolder.Close();

      }
   }

   #endregion Iterator-Funktionen

}
