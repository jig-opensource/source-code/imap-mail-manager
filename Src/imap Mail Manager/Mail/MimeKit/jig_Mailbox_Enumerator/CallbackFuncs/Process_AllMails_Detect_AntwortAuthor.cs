using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ImapMailManager.BO;
using ImapMailManager.Config;
using ImapMailManager.Properties;
using jig.Tools;
using jig.Tools.String;
using MailKit;
using MailKit.Search;
using MimeKit;
using PCRE;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Versucht, den Author der Antwort herauszufinden 
/// </summary>
public class Process_AllMails_Detect_AntwortAuthor : IMailboxIterator, IMailbox_FolderFilter {

   #region IMailbox_FolderFilter

   /// <summary>
   /// Die Default Methode im Interface:
   ///   StopFolderProcessing_BlackWhiteList
   /// muss ins Interface gecastet werden, damit sie aufgerufen werden kann:
   /// !KH https://stackoverflow.com/a/57763254/4795779
   /// Dies hier ist ein Prop als Cast zum Interface, damit der Aufruf hübscher gemacht werden kann 
   /// </summary>
   private IMailbox_FolderFilter iMailbox_FolderFilter => (IMailbox_FolderFilter)this;

   #endregion IMailbox_FolderFilter

   //‼ Config

   #region Allgemeine Config

   /// <summary>
   /// Der aktuelle Prozess-Schritt
   /// </summary>
   private const BO_Mail.DetectAuthor_Level CurrentProcessPhase = BO_Mail.DetectAuthor_Level.Lvl3_By_RgxVornameNachname;

   #endregion Config

   #region IMailbox_FolderFilter White / Blacklist

   /// <summary>
   /// Wenn die Whitelist definiert ist,
   /// dann wird die Blacklist ignoriert
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist { get; } = new
      (IMailbox_FolderFilter.FolderCompare.RgxMatch
       , new List<string> {
          // ExtensionMethods_RegEx.ØWildcardToRegex(@"*INBOX*".Replace( '\\', '/'))
       });

   Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? IMailbox_FolderFilter.MbxFolder_Whitelist {
      get {
         switch (CurrentProcessPhase) {
            case BO_Mail.DetectAuthor_Level.Lvl1_By_SenderDisplayName:
               // Alle Ordner verarbeiten
               return new
                  (IMailbox_FolderFilter.FolderCompare.RgxMatch
                   , new List<string> {
                      // ExtensionMethods_RegEx.ØWildcardToRegex(@"*INBOX*".Replace( '\\', '/'))
                   });

            case BO_Mail.DetectAuthor_Level.Lvl2_By_RgxMailSignatur:

               // Nur MailKategorie\AntwortVomTeam\01 Sender Displayname verarbeiten
               return new
                  (IMailbox_FolderFilter.FolderCompare.RgxMatch
                   , new List<string> {
                      ExtensionMethods_RegEx.ØWildcardToRegex(@"MailKategorie/AntwortVomTeam*".Replace('\\', '/'))
                   });

            case BO_Mail.DetectAuthor_Level.Lvl3_By_RgxVornameNachname:

               // Nur MailKategorie\AntwortVomTeam\01 Sender Displayname verarbeiten
               return new
                  (IMailbox_FolderFilter.FolderCompare.RgxMatch
                   , new List<string> {
                      ExtensionMethods_RegEx.ØWildcardToRegex(@"MailKategorie/AntwortVomTeam*".Replace('\\', '/'))
                   });

            default:

               throw new ArgumentOutOfRangeException();
         }
      }
   }

   Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? IMailbox_FolderFilter.MbxFolder_Whitelist_Except_StopOn {
      get {
         switch (CurrentProcessPhase) {
            case BO_Mail.DetectAuthor_Level.Lvl1_By_SenderDisplayName:
               // Ignorieren: MailKategorie/AntwortVomTeam/01 Sender Displayname
               return new
                  (IMailbox_FolderFilter.FolderCompare.RgxMatch
                   , new List<string> {
                      ExtensionMethods_RegEx.ØWildcardToRegex(@"MailKategorie/AntwortVomTeam/*".Replace('\\', '/'))
                   });

            case BO_Mail.DetectAuthor_Level.Lvl2_By_RgxMailSignatur:
               return new
                  (IMailbox_FolderFilter.FolderCompare.RgxMatch
                   , new List<string> {
                      ExtensionMethods_RegEx.ØWildcardToRegex
                         (@"MailKategorie/AntwortVomTeam/01 Sender Displayname*".Replace('\\', '/'))
                      , ExtensionMethods_RegEx.ØWildcardToRegex
                         (@"MailKategorie/AntwortVomTeam/02 RegEx Mail Signatur*".Replace('\\', '/'))
                   });

            case BO_Mail.DetectAuthor_Level.Lvl3_By_RgxVornameNachname:
               return new
                  (IMailbox_FolderFilter.FolderCompare.RgxMatch
                   , new List<string> {
                      ExtensionMethods_RegEx.ØWildcardToRegex
                         (@"MailKategorie/AntwortVomTeam/01 Sender Displayname*".Replace('\\', '/'))
                      , ExtensionMethods_RegEx.ØWildcardToRegex
                         (@"MailKategorie/AntwortVomTeam/02 RegEx Mail Signatur*".Replace('\\', '/'))
                      , ExtensionMethods_RegEx.ØWildcardToRegex
                         (@"MailKategorie/AntwortVomTeam/03 RegEx Vorname Nachname*".Replace('\\', '/'))
                   });

            default:
               throw new ArgumentOutOfRangeException();
         }
      }
   }

   /// <summary>
   /// Die Blacklist wird nur verwendet, 
   /// wenn die Whitelist nicht definiert ist
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Blacklist { get; } = new
      (IMailbox_FolderFilter.FolderCompare.RgxMatch
       , new List<string> {
          // "Trash", "Gelöschte Elemente", "Junk-E-Mail", "Junk"
       });

   #endregion IMailbox_FolderFilter White / Blacklist

   #region Konstruktor

   static Process_AllMails_Detect_AntwortAuthor() { CalcRegex(); }

   #endregion Konstruktor

   #region Spezifische Iterator Config

   /// <summary>
   /// Wenn true, werden Antwort-Mails in Unterverzeichnisse von
   /// \MailKategorie\AntwortVomTeam\<Author>
   /// abgelegt 
   /// </summary>
   private static readonly bool Move_MailsByAuthor = true;

   // In diesen Ordner werden die Klassifizierten E-Mails abgelegt
   private static readonly string AblageOrdner_MailKategorie = "MailKategorie";
   private                 int    _noOfAuthorsFound          = 0;

   /// <summary>
   /// Negativ: Alle Authoren suchen
   /// </summary>
   private const int StopIfAuthorsFound = -1;

   /// <summary>
   /// Wenn > 0, definiert der Wert, wie viele Mails pro Ordner max. verarbeitet werden
   /// </summary>
   private const int ProcessMaxMailsPerFolder = -1;

   /// <summary>
   /// Die Mbx Unterordner:
   /// MailKategorie\AntwortVomTeam\❮Detector-Schritt❯  
   /// </summary>
   private readonly Dictionary<BO_Mail.DetectAuthor_Level, string>
      _dirsProzess_DetectedAuthor = new() {
         { BO_Mail.DetectAuthor_Level.Lvl1_By_SenderDisplayName, "01 Sender Displayname" }
         , { BO_Mail.DetectAuthor_Level.Lvl2_By_RgxMailSignatur, "02 RegEx Mail Signatur" }
         , { BO_Mail.DetectAuthor_Level.Lvl3_By_RgxVornameNachname, "03 RegEx Vorname Nachname" }
      };

   /// <summary>
   /// Für jede Erkennungs-Phase die jeweiligen Ordner der Team-Mitglieder
   /// </summary>
   readonly Dictionary<BO_Mail.DetectAuthor_Level, Dictionary<TeamMitglied, IMailFolder>> _teamMitgliederOrdner = new();
   readonly Dictionary<string, IMailFolder> _ordnerCache = new();

   #endregion Spezifische Iterator Config

   // Kürzel
   // » n Subjects
   //   » n Found100CharsAroundTeammemberName
   private static readonly Dictionary<string,
      Dictionary<string, List<string>>> Found100CharsAroundTeammemberName = new();
   private static readonly Dictionary<string, List<PcreRegex>> AllRgx_PlusMinus100Chars = new();


   private static void CalcRegex() {

      // !KH9'9
      // i » PcreOptions.IgnoreCase 
      // n » PcreOptions.ExplicitCapture 
      // x » PcreOptions.IgnorePatternWhitespace 
      // m » PcreOptions.MultiLine   »  ^$ matches line breaks 
      // s » PcreOptions.Singleline  »   . matches line breaks 

      #region rgxPlusMinus100Chars

      PcreOptions RgxPcre2Optionss_rgxPlusMinus100Chars =
         PcreOptions.Compiled
         | PcreOptions.IgnoreCase
         | PcreOptions.Caseless
         | PcreOptions.IgnorePatternWhitespace
         | PcreOptions.ExplicitCapture
         | PcreOptions.MultiLine
         | PcreOptions.Singleline
         | PcreOptions.Ungreedy
         | PcreOptions.Unicode;

      string rgxPlusMinus100Chars = @"
                    (?<ChrsBefore>.{1,100})
                    (?<Author>{Platzhalter})
                    (?<ChrsAfter>.{1,100})
                    ";

      #endregion rgxPlusMinus100Chars

      foreach (var teamMitglied in App.Cfg_TeamConfig.TeamConfig.TeamMitglieder) {
         //+ Das Dict initialisieren
         if (!AllRgx_PlusMinus100Chars.ContainsKey(teamMitglied.Kuerzel)) {
            AllRgx_PlusMinus100Chars.Add(teamMitglied.Kuerzel, new List<PcreRegex>());
         }

         //++ Den Nachnamen aufsplitten
         var nachnameItems = teamMitglied.Nachname.Split('-');

         //+ Der rgxPlusMinus100Chars

         #region rgxPlusMinus100Chars

         if (nachnameItems.Length > 1) {
            foreach (var nachnameItem in nachnameItems) {
               var       strRgxLoop = rgxPlusMinus100Chars.Replace("{Platzhalter}", nachnameItem);
               PcreRegex oRgxLoop   = new PcreRegex(strRgxLoop, RgxPcre2Optionss_rgxPlusMinus100Chars);
               AllRgx_PlusMinus100Chars[teamMitglied.Kuerzel].Add(oRgxLoop);
            }
         }

         var       strRgx = rgxPlusMinus100Chars.Replace("{Platzhalter}", teamMitglied.Nachname);
         PcreRegex oRgx   = new PcreRegex(strRgx, RgxPcre2Optionss_rgxPlusMinus100Chars);
         AllRgx_PlusMinus100Chars[teamMitglied.Kuerzel].Add(oRgx);

         #endregion rgxPlusMinus100Chars

      }

   }


   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props


   /// <summary>
   /// » Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {
      if (Move_MailsByAuthor) {
         //+ Sicherstellen, dass wir in der Mbx die Ordner haben
         //++ Den Root Ordner und die Unterordner holen 
         IMailFolder rootFolder     = imapServerRuntimeConfig.RootFolder;
         var         rootSubfolders = rootFolder.GetSubfolders();

         //++ Den Ordner MailKategorie erzeugen
         IMailFolder? folder_MailKategorie = rootSubfolders.FirstOrDefault
            (f => f.Name.ØEqualsIgnoreCase(AblageOrdner_MailKategorie));

         //+++ Der Ordner fehlt: Erzeugen
         if (folder_MailKategorie == null) {
            // Ordner erzeugen. False: In diesem Ordner keine Mails speichern
            // !M http://www.mimekit.net/docs/html/M_MailKit_Net_Imap_ImapFolder_Create.htm
            folder_MailKategorie = rootFolder.Create(AblageOrdner_MailKategorie, true);
            folder_MailKategorie.Subscribe();
         }

         //+++ Alle Unterordner holen
         var folder_MailKategorieSubfolders = folder_MailKategorie.GetSubfolders();

         //++ Den Ordner erzeugen: \MailKlassen\AntwortVomTeam\
         IMailFolder? mailFolder_AntwortVomTeam = folder_MailKategorieSubfolders
           .FirstOrDefault
               (f => f.Name.ØEqualsIgnoreCase
                   (ConfigMail_MsgHeader
                   .MailKategorie
                   .AntwortVomTeam
                   .ToString()));

         //+++ Fehlt: Erzeugen
         if (mailFolder_AntwortVomTeam == null) {
            mailFolder_AntwortVomTeam = folder_MailKategorie.Create
               (ConfigMail_MsgHeader
               .MailKategorie
               .AntwortVomTeam
               .ToString()
                , true);
            mailFolder_AntwortVomTeam.Subscribe();
         }

         //++ Den Ordner erzeugen: \MailKlassen\AntwortVomTeam\Erkennungs-Phase
         var mailFolder_AntwortVomTeamSubfolders = mailFolder_AntwortVomTeam.GetSubfolders();

         foreach (BO_Mail.DetectAuthor_Level phase in Enum.GetValues<BO_Mail.DetectAuthor_Level>()) {
            var phaseOrdnerName = _dirsProzess_DetectedAuthor[phase];

            //+ Den Prozess-Ordner erzeugen
            IMailFolder? prozessFolder = mailFolder_AntwortVomTeamSubfolders.FirstOrDefault
               (f => f.Name.ØEqualsIgnoreCase(phaseOrdnerName));

            if (prozessFolder == null) {
               // Ordner erzeugen. True: In diesem Ordner Mails speichern
               // !M http://www.mimekit.net/docs/html/M_MailKit_Net_Imap_ImapFolder_Create.htm
               prozessFolder = mailFolder_AntwortVomTeam.Create(phaseOrdnerName, true);
               prozessFolder.Subscribe();
            }

            //+++ Alle Unterordner holen
            var prozessFolderSubfolders = prozessFolder.GetSubfolders();

            //+ Für jedes Team-Mitglied einen Unterordner erzeugen  
            foreach (var teamMitglied in App.Cfg_TeamConfig.TeamConfig.TeamMitglieder) {
               var name = $"{teamMitglied.Vorname} {teamMitglied.Nachname}";

               IMailFolder? teamMitgliedFolder = prozessFolderSubfolders.FirstOrDefault
                  (f => f.Name.ØEqualsIgnoreCase(name));

               if (teamMitgliedFolder == null) {
                  // Ordner erzeugen. True: In diesem Ordner Mails speichern
                  // !M http://www.mimekit.net/docs/html/M_MailKit_Net_Imap_ImapFolder_Create.htm
                  teamMitgliedFolder = prozessFolder.Create(name, true);
                  teamMitgliedFolder.Subscribe();
               }

               if (_teamMitgliederOrdner.ContainsKey(phase)) {
                  _teamMitgliederOrdner[phase].Add(teamMitglied, teamMitgliedFolder!);
               }
               else {
                  _teamMitgliederOrdner.Add
                     (phase, new Dictionary<TeamMitglied, IMailFolder> { { teamMitglied, teamMitgliedFolder! } });
               }
            }

         }

      }

   }


   /// <summary>
   /// » Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      int done = 1;

      Debug.WriteLine($"No of Authors found: {_noOfAuthorsFound}");

      int stopper = 2;

      /*
            StringBuilder sb = new StringBuilder();
      
            foreach (var subject in MailSubjects.OrderBy(x => x)) {
               sb.AppendLine(subject);
            }
      
            // 🚩 Debug: Ins ClipBoard kopieren
            ClipboardService.SetText(sb.ToString());
            
            Debug.WriteLine(sb.ToString());
      */

   }


   /// <summary>
   /// Verarbeitet den übergebenen Ordner
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {

      // Nötig, um den Inhalt zu erfahren
      mbxFolder.Open(FolderAccess.ReadOnly);
      var anzMails = mbxFolder.Count;
      Debug.WriteLine($"{mbxFolder.FullName}");

      //» Wenn wir den ersten Author haben...
      if (StopIfAuthorsFound > 0 && _noOfAuthorsFound >= StopIfAuthorsFound) {
         Debug.WriteLine($"  » skipped");

         return;
      }

      var permanentFlags    = mbxFolder.PermanentFlags;
      var permanentKeywords = mbxFolder.PermanentKeywords;

      //+ Die Blacklisted Ordner überspringen
      if (iMailbox_FolderFilter.StopFolderProcessing_BlackWhiteList(mbxFolder)) {
         Debug.WriteLine($"  » skipped");

         return;
      }

      //+ Wir sammeln mal 100 unbekannte Grussformeln
      if (AllRgx_PlusMinus100Chars.Count > 100) { return; }

      //+ Alle Mails suchen
      var uids = mbxFolder.Search(SearchQuery.All);

      // 🚩 Debug.WriteLine($"  Anzahl: {uids.Count}");

      // Sortieren: zuerst die neuste E-Mail
      // !Q https://stackoverflow.com/a/58580564/4795779						
      // var messages = imapFolder.Fetch(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.InternalDate).ToList();
      // messages.Sort(new OrderBy[] { OrderBy.ReverseDate });

      //+ Die Message Summaries holen
      IList<IMessageSummary> messageSummaries = mbxFolder.Fetch
         (uids
          , MessageSummaryItems.Envelope

            // | MessageSummaryItems.BodyStructure
            // | MessageSummaryItems.Size
            // https://github.com/jstedfast/MailKit/issues/1254#issuecomment-1277881417
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Headers

            // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
            | MessageSummaryItems.InternalDate
            | MessageSummaryItems.References
            | MessageSummaryItems.Flags);

      //+ Alle gefundenen Mails bearbeiten
      int msgCnt = uids.Count;

      foreach (var msgSummary in messageSummaries) {
         // Wenn wir die vorgesehene Anzahl erreicht haben, überspringen
         if (ProcessMaxMailsPerFolder > 0
             && uids.Count - msgCnt > ProcessMaxMailsPerFolder) {
            break;
         }

         if (--msgCnt%10 == 0 || msgCnt == uids.Count - 1) { Debug.WriteLine($"    ↻ {msgCnt,4} {msgSummary.Envelope.Subject}"); }

         mbxFolder.Open(FolderAccess.ReadOnly);

         //‼ 🚩 Debug: Eine E-Mail mit bestimmtem Betreff suchen?
         var srchSubject = @"";
         srchSubject = @"AW: 430, 400, 215 Jahre- wie geht das zusammen?";
         srchSubject = @"";

         if ((srchSubject.ØHasValue() && msgSummary.Envelope.Subject == null)
             || (srchSubject.ØHasValue()
                 && msgSummary.Envelope.Subject != null
                 && !msgSummary.Envelope.Subject.Contains(srchSubject))) {
            continue;
         }

         //» Das BO Mail erzeugen
         BO_Mail bo = new BO_Mail
            (imapSrvRuntimeConfig
             , mbxFolder
             , msgSummary);

         // Alle App-Spezifischen Mails Überspringen: ✅ Dok:
         if (bo.IsAppSpecificMail) { continue; }

         //+ Wenn die MailKategorie fehlt oder nicht AntwortVomTeam ist, dann weiter 
         if (!bo.HasHeaderSet_MailKategorie
             || bo.Get_MsgHdr_MailKategorie() != ConfigMail_MsgHeader.MailKategorie.AntwortVomTeam) {
            continue;
         }

         //» Den Author bestimmen
         BO_Mail_BO_MsgHdr_Author_AntwortVomTeam resAuthorSet = bo.Get_MsgHdr_Cached_Author_AntwortVomTeam();

         if (resAuthorSet is { TeamAutor: {} }) {
            // Zählen
            _noOfAuthorsFound++;

            // Alle erkannten Mails als ungelesen markieren,
            // um im Postfach eine bessere Übersicht zu kriegen
            MimeKit_JigTools.MarkMsg_AsUnread(mbxFolder, bo.MsgUniqueId);

            if (Move_MailsByAuthor) {

               // hier weiter: Ordner erzeugen
               //  Prozent\Autor
               var prozentOrdnerPfad
                  = $@"MailKategorie\AntwortVomTeam\{resAuthorSet.ErkennungsZuverlässigkeitProzent}\{resAuthorSet.TeamAutor.GetVornameNachname()}";
               IMailFolder prozentOrdner;

               if (!_ordnerCache.ContainsKey(prozentOrdnerPfad)) {
                  // Ordner erzeugen
                  prozentOrdner = imapSrvRuntimeConfig.ØGetOrCreateFolder(prozentOrdnerPfad);
                  _ordnerCache.Add(prozentOrdnerPfad, prozentOrdner);
               }
               else {
                  prozentOrdner = _ordnerCache[prozentOrdnerPfad];
               }

               prozentOrdner.Open(FolderAccess.ReadWrite);

               // !KH9 Der MimeKit Move Befehl ist intelligent
               // Und nützt die Möglichkeiten des Servers
               // !Q https://github.com/jstedfast/MailKit/issues/160#issuecomment-79290966
               var uidMap = mbxFolder.MoveTo(bo.MsgUniqueId, prozentOrdner);
 /*
               IMailFolder zielOrdner = _teamMitgliederOrdner[CurrentProcessPhase][resAuthorSet.TeamAutor];

               //+ Die Mail in den Ordner des Team-Mitglieds verschieben
               // Wenn die Mail nicht schon im richtigen Ordner ist
               if (!mbxFolder.FullName.ØEqualsIgnoreCase(zielOrdner.FullName)) {
                  mbxFolder.Open(FolderAccess.ReadWrite);

                  // !KH9 Der MimeKit Move Befehl ist intelligent
                  // Und nützt die Möglichkeiten des Servers
                  // !Q https://github.com/jstedfast/MailKit/issues/160#issuecomment-79290966
                  var uidMap = mbxFolder.MoveTo(bo.MsgUniqueId, zielOrdner);

               }
*/               
            }
         }

         // Hilft vielleicht, um die Änderung auf dem Server zu persistieren
         // !KH http://www.mimekit.net/docs/html/M_MailKit_IMailFolder_Close.htm
         // > Closes the folder, optionally expunging the messages marked for deletion.
         mbxFolder.Close();

         continue;

         /*
          Analysieren des Postafachs
          und suchen der Autoren
           
            hier weiter
               
               Suche nur nach Mails, in denen der Header fehlt
               - Author_AntwortVomTeam
               - MailKategorie
          */

         //+ Ist die E-Mail von der Absender-Domäne @RogerLiebi.ch?

         var isTeamFromAddr = msgSummary.Envelope.From.Any
            (x => ((MailboxAddress)x).Address.EndsWith("@RogerLiebi.ch", StringComparison.OrdinalIgnoreCase));

         var isTeamSenderAddr = msgSummary.Envelope.Sender.Any
            (x => ((MailboxAddress)x).Address.EndsWith("@RogerLiebi.ch", StringComparison.OrdinalIgnoreCase));

         //++ Weder From noch Sender ist von uns - wir sind nicht der Autor
         if (!isTeamFromAddr && !isTeamSenderAddr) {
            continue;
         }

         //+++ Ist ReplyTo anonymisiert?
         // var hasReplyToMailAddr = msgSummary.Envelope.ReplyTo.Any(x => !((MailboxAddress)x).Address.Equals("Anonymisiert"));
         // if (!hasReplyToMailAddr) { continue; }

         //+ Die E-Mail ist vom Team
         //+ Versuchen, den Autor herauszufinden

         //+ Sammeln der Autoren für diese E-Mail
         List<TeamMitglied> thisMailAutors = new List<TeamMitglied>();

         //+ Haben wir eine persönliche ReplyTo-Adresse?
         bool foundAuthor = false;

         for (var idx = 0; !foundAuthor && idx < App.Cfg_TeamConfig.TeamConfig.TeamMitglieder.Count; idx++) {
            var teamMitglied = App.Cfg_TeamConfig.TeamConfig.TeamMitglieder[idx];

            //++ Haben wir eine bekannte ReplyTo Adresse?
            if (msgSummary.Envelope.ReplyTo.Any
                   (x => ((MailboxAddress)x).Address.ØEqualsIgnoreCase(teamMitglied.AntwortEMailAddress))) {
               foundAuthor = true;
               thisMailAutors.Add(teamMitglied);
            }
         }

         if (foundAuthor) {
            continue;
         }

         //+ Keine ReplyTo adresse gefunden
         //+ Im Mail-Text nach dem Autor suchen

         //+ Sammeln von 100 Zeichen vor / nach dem Team-Mitglied - Namen

         //+++ Wenn der betreff leer ist, ignorieren wir diese Mail
         if (!msgSummary.Envelope.Subject.ØHasValue()) {
            continue;
         }

         var MailBodytextWithCrLf = bo.Mail_Body_TextOnly;

         foreach (var thisTeamMember_AllRgx_100Chars in AllRgx_PlusMinus100Chars) {
            var kürzel = thisTeamMember_AllRgx_100Chars.Key;

            // Pro Team-Mitarbeiter hat es mehrere Regex haben

            // KeyValuePair<string, Dictionary<string, List<string>>> thisUserDict = Found100CharsAroundTeammemberName.First
            //    (x => x.Key.ØEqualsIgnoreCase(kürzel));
            // Dictionary<string, List<string>> thisUserDictSubjects = thisUserDict.Value;

            List<string> thisUser_Matches = new List<string>();

            foreach (var this_AllRgx_Grussformel in thisTeamMember_AllRgx_100Chars.Value) {
               var matches = this_AllRgx_Grussformel.Matches(MailBodytextWithCrLf);

               foreach (var match in matches) {
                  if (match.Success) { thisUser_Matches.Add(match.Value); }
               }
            }

            // Wenn 
            if (thisUser_Matches.Count > 0) {

               // Wenn fürs Team-Mitglied noch kein Dict existiert
               if (!Found100CharsAroundTeammemberName.ContainsKey(kürzel)) {
                  Dictionary<string, List<string>> thisSubjectMatches = new Dictionary<string, List<string>>();
                  thisSubjectMatches.Add(msgSummary.Envelope.Subject!, thisUser_Matches);
                  Found100CharsAroundTeammemberName.Add(kürzel, thisSubjectMatches);

                  if (Found100CharsAroundTeammemberName[kürzel].ContainsKey(msgSummary.Envelope.Subject!)) {
                     foreach (var thisUser_Match in thisUser_Matches) {
                        Found100CharsAroundTeammemberName[kürzel][msgSummary.Envelope.Subject!].ØAppendItem_IfMissing
                           (thisUser_Match);
                     }
                  }
                  else {
                     Found100CharsAroundTeammemberName[kürzel].Add(msgSummary.Envelope.Subject!, thisUser_Matches);
                  }
               }
               else {
                  if (Found100CharsAroundTeammemberName[kürzel].ContainsKey(msgSummary.Envelope.Subject!)) {
                     foreach (var thisUser_Match in thisUser_Matches) {
                        Found100CharsAroundTeammemberName[kürzel][msgSummary.Envelope.Subject!].ØAppendItem_IfMissing
                           (thisUser_Match);
                     }
                  }
                  else {
                     Found100CharsAroundTeammemberName[kürzel].Add(msgSummary.Envelope.Subject!, thisUser_Matches);
                  }
               }
            }
         }

         /*
         if (!bo.HasHeaderSet_Metadaten) {
            // Debug.WriteLine($"    » Skipping: {msgSummary.Envelope.Subject}");
            continue;
         }
         */

         // Debug.WriteLine($"    » Clean Subject: {msgCnt,4} {msgSummary.Envelope.Subject}");

      }
   }

   #endregion Iterator-Funktionen

}
