using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ImapMailManager.BO;
using ImapMailManager.Config;
using jig.Tools;
using jig.Tools.String;
using MailKit;
using MailKit.Search;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Gibt die Ordner der Mailbox aus
/// </summary>
public class Process_AllMails_Info_MbxFolders : IMailboxIterator, IMailbox_FolderFilter, IMailboxIteratorAdHoc {

   #region IMailbox_FolderFilter

   /// <summary>
   /// Die Default Methode im Interface:
   ///   StopFolderProcessing_BlackWhiteList
   /// muss ins Interface gecastet werden, damit sie aufgerufen werden kann:
   /// !KH https://stackoverflow.com/a/57763254/4795779
   /// Dies hier ist ein Prop als Cast zum Interface, damit der Aufruf hübscher gemacht werden kann 
   /// </summary>
   private IMailbox_FolderFilter iMailbox_FolderFilter => (IMailbox_FolderFilter)this;

   #endregion IMailbox_FolderFilter

   //‼ Config

   #region IMailbox_FolderFilter White / Blacklist

   /// <summary>
   /// Wenn die Whitelist definiert ist,
   /// dann wird die Blacklist ignoriert
   /// </summary>
   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist { get; } = new
      (IMailbox_FolderFilter.FolderCompare.RgxMatch
       , new List<string> {
          // ExtensionMethods_RegEx.ØWildcardToRegex(@"*INBOX*".Replace( '\\', '/'))
       });

   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Whitelist_Except_StopOn { get; } = new
      (IMailbox_FolderFilter.FolderCompare.RgxMatch
       , new List<string> {
          // ExtensionMethods_RegEx.ØWildcardToRegex(@"*INBOX*".Replace( '\\', '/'))
       });

   public Tuple<IMailbox_FolderFilter.FolderCompare, List<string>?>? MbxFolder_Blacklist { get; } = new
      (IMailbox_FolderFilter.FolderCompare.RgxMatch
       , new List<string> {
          // "Trash", "Gelöschte Elemente", "Junk-E-Mail", "Junk"
          // ExtensionMethods_RegEx.ØWildcardToRegex(@"*INBOX*".Replace( '\\', '/'))
       });

   #endregion IMailbox_FolderFilter White / Blacklist

   #region Spezifische Iterator Config

   //‼ 🚩 Sammeln der Sender Anzeigenamen
   private readonly List<string> All_MbxOrdner_FullName = new();

   /// <summary>
   /// Wenn > 0, definiert der Wert, wie viele Mails pro Ordner max. verarbeitet werden
   /// </summary>
   private const int ProcessMaxMailsPerFolder = -1;

   /// <summary>
   /// Alle wieviele E-Mails soll eine Progress-Nachricht angezeigt werden?
   /// </summary>
   private static readonly int PrintMailProgressModulo = 10;

   #endregion Spezifische Iterator Config

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props


   /// <summary>
   /// » Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}


   /// <summary>
   /// » Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      int done = 1;

      Debug.WriteLine("\nAlle Ordner-Namen:");

      foreach (var ordnerFullName in All_MbxOrdner_FullName.OrderBy(x => x)) {
         Debug.WriteLine(ordnerFullName);
      }

      // Debug.WriteLine($"No of Authors found: {_noOfAuthorsFound}");

      int stopper = 2;

      /*
            StringBuilder sb = new StringBuilder();
      
            foreach (var subject in MailSubjects.OrderBy(x => x)) {
               sb.AppendLine(subject);
            }
      
            // 🚩 Debug: Ins ClipBoard kopieren
            ClipboardService.SetText(sb.ToString());
            
            Debug.WriteLine(sb.ToString());
      */

   }


   /// <summary>
   /// Verarbeitet den übergebenen Ordner
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {

      // Nötig, um den Inhalt zu erfahren
      mbxFolder.Open(FolderAccess.ReadOnly);
      var anzMails = mbxFolder.Count;
      Debug.WriteLine($"{mbxFolder.FullName}");

      var permanentFlags    = mbxFolder.PermanentFlags;
      var permanentKeywords = mbxFolder.PermanentKeywords;

      //+ Die Blacklisted Ordner überspringen
      if (iMailbox_FolderFilter.StopFolderProcessing_BlackWhiteList(mbxFolder)) {
         Debug.WriteLine($"  » skipped");

         return;
      }

      All_MbxOrdner_FullName.Add(mbxFolder.FullName);

   }

   #endregion Iterator-Funktionen

}
