using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using ImapMailManager.BO;
using ImapMailManager.Mail.Parser;
using jig.Tools;
using MailKit;
using MailKit.Search;
using MimeKit;


namespace ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;

/// <summary>
/// Fürs ad hoc Debugging genützt
/// Sammelt Informationen zu allen E-Mails, z.B. Informationen zum Absender  
/// </summary>
public class AnalyzeMbx_Collect_MailData : IMailboxIterator {

   // Sammelt 
   // EMailAddress.EMailAddrTyp.Bcc
   private static Dictionary<EMailAddress.EMailAddrTyp, List<InternetAddressList>> Antwort_Info_SenderMails = new();

   //» Config
   
   #region Config

   /// <summary>
   /// Wenn > 0, definiert der Wert, wie viele Mails pro Ordner max. verarbeitet werden
   /// </summary>
   private static readonly int ProcessMaxMailsPerFolder = -1;
   /// <summary>
   /// Alle wieviele E-Mails soll eine Progress-Nachricht angezeigt werden?
   /// </summary>
   private static readonly int PrintMailProgressModulo = 10;

   /// <summary>
   /// Die Mailbox-Ordnernamen, die wir verarbeiten
   /// </summary>
   private static readonly List<string> MbxFolder_Whitelist = new List<string>
      (new[] {
         "Sent", "Gesendete Elemente"
      });

   /// <summary>
   /// Die Mailbox-Ordnernamen, die wir nicht verarbeiten
   /// </summary>
   private static readonly List<string> MbxFolderBlacklist = new List<string>
      (new[] {
         ""

         // "Trash", "Gelöschte Elemente", "Junk-E-Mail", "Junk"
      });

   #endregion Config

   #region Iterator-Funktionen

   #region Props

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessInit nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessInit { get; set; }

   /// <summary>
   /// Wenn true, dann ruft MailboxIterator
   /// ProcessDone nicht auf
   /// </summary>
   public bool? MailboxIterator_Dont_ProcessDone { get; set; }

   #endregion Props

   /// <summary>
   /// Den Iterator initialisieren
   /// </summary>
   /// <param name="imapServerRuntimeConfig"></param>
   public void Init_Process(ImapServerRuntimeConfig imapServerRuntimeConfig) {}

   /// <summary>
   /// Funktion, wenn alle MailboxFolder verarbeitet wurden
   /// </summary>
   public void ProcessMailboxFolder_Done() {
      //‼ 🚩 Debug

      var aFrom    = Antwort_Info_SenderMails[EMailAddress.EMailAddrTyp.From];
      var aSender  = Antwort_Info_SenderMails[EMailAddress.EMailAddrTyp.Sender];
      var aReplyTo = Antwort_Info_SenderMails[EMailAddress.EMailAddrTyp.ReplyTo];
      
      var fromAddresses     = aFrom.SelectMany(x => x.Mailboxes.Select(m => m.Address)).Distinct().ToList();
      var senderAddresses   = aSender.SelectMany(x => x.Mailboxes.Select(m => m.Address)).Distinct().ToList();
      var replyToAddresses = aReplyTo.SelectMany(x => x.Mailboxes.Select(m => m.Address)).Distinct().ToList();

      var fromName    = aFrom.SelectMany(x => x.Mailboxes.Select(m => m.Name)).Distinct().ToList();
      var senderName  = aSender.SelectMany(x => x.Mailboxes.Select(m => m.Name)).Distinct().ToList();
      var replyToName = aReplyTo.SelectMany(x => x.Mailboxes.Select(m => m.Name)).Distinct().ToList();

      int done = 1;
   }


   /// <summary>
   /// Analysiert alle E-Mails und aktualisiert die Metadaten
   /// </summary>
   /// <param name="imapSrvRuntimeConfig"></param>
   /// <param name="mbxFolder"></param>
   /// <param name="force">
   /// Alle Header-Daten werden neu berechnet
   /// </param>
   public void ProcessMailboxFolder(
      ImapServerRuntimeConfig imapSrvRuntimeConfig
      , IMailFolder           mbxFolder
      , bool                  force) {

      // Nötig, um den Inhalt abzurufen
      mbxFolder.Open(FolderAccess.ReadOnly);
      var anzMails = mbxFolder.Count;
      Debug.WriteLine($"{mbxFolder.FullName} ({anzMails})");

      //+ Wenn wir eine Ordner-Whitelist haben, dann mit dieser arbeiten
      if (MbxFolder_Whitelist.Count > 0) {
         //+ Wenn nicht in der Whitelist: überspringen 
         if (!MbxFolder_Whitelist.ØRgxMatchWithAnyItemInList(mbxFolder.FullName)) {
            Debug.WriteLine($"  » skipped");

            return;
         }
      }
      else {
         //+ Sonst die Blacklist nützen
         // Die Blacklisted Ordner überspringen
         if (MbxFolderBlacklist.ØRgxMatchWithAnyItemInList(mbxFolder.FullName)) {
            Debug.WriteLine($"  » skipped");

            return;
         }
      }

      //» Die Message Summaries holen und nach Datum absteigend sortieren
      // Absteigend sortiert: die neuste Mail zuerst / Empfangsdatum
      // Das zweite Argument -1
      // !KH9 https://stackoverflow.com/a/29121052/4795779
      IList<IMessageSummary> messageSummaries = mbxFolder.Fetch
         (0
          , -1
          , MessageSummaryItems.Envelope
            | MessageSummaryItems.BodyStructure
            | MessageSummaryItems.UniqueId
            | MessageSummaryItems.Size
            | MessageSummaryItems.Headers
            // !M http://www.mimekit.net/docs/html/P_MailKit_IMessageSummary_InternalDate.htm
            | MessageSummaryItems.InternalDate
            | MessageSummaryItems.Flags);

      // Sortieren: zuerst die neuste E-Mail
      messageSummaries.Sort(new[] { OrderBy.ReverseDate });

      //+ Test_BO_Mail: Jede Msg lesen
      int msgCnt       = anzMails;
      int msgProcessed = 0;

      foreach (var msgSummary in messageSummaries) {
         // Wenn wir die vorgesehene Anzahl erreicht haben, überspringen
         if (ProcessMaxMailsPerFolder > 0
             && msgProcessed > ProcessMaxMailsPerFolder) {
            break;
         }

         if (--msgCnt % PrintMailProgressModulo == 0 || msgCnt == anzMails - 1) {
            Debug.WriteLine($"    ↻ {msgCnt,4} {msgSummary.Envelope.Subject}");
         }

         //+ Die Daten sammeln
         //++ EMailAddress.EMailAddrTyp.From
         if (!Antwort_Info_SenderMails.ContainsKey(EMailAddress.EMailAddrTyp.From)) {
            Antwort_Info_SenderMails.Add
               (EMailAddress.EMailAddrTyp.From
                , new List<InternetAddressList>() {
                   msgSummary.Envelope.From
                });
         }
         else {
            Antwort_Info_SenderMails[EMailAddress.EMailAddrTyp.From].Add(msgSummary.Envelope.From);
         }
         
         //++ EMailAddress.EMailAddrTyp.Sender
         if (!Antwort_Info_SenderMails.ContainsKey(EMailAddress.EMailAddrTyp.Sender)) {
            Antwort_Info_SenderMails.Add
               (EMailAddress.EMailAddrTyp.Sender
                , new List<InternetAddressList>() {
                   msgSummary.Envelope.Sender
                });
         }
         else {
            Antwort_Info_SenderMails[EMailAddress.EMailAddrTyp.Sender].Add(msgSummary.Envelope.Sender);
         }
         
         //++ EMailAddress.EMailAddrTyp.ReplyTo
         if (!Antwort_Info_SenderMails.ContainsKey(EMailAddress.EMailAddrTyp.ReplyTo)) {
            Antwort_Info_SenderMails.Add
               (EMailAddress.EMailAddrTyp.ReplyTo
                , new List<InternetAddressList>() {
                   msgSummary.Envelope.ReplyTo
                });
         }
         else {
            Antwort_Info_SenderMails[EMailAddress.EMailAddrTyp.ReplyTo].Add(msgSummary.Envelope.ReplyTo);
         }

         continue;

         //+ Obj erzeugen
         BO_Mail bo = new BO_Mail
            (imapSrvRuntimeConfig
             , mbxFolder
             , msgSummary);

         // Alle App-Spezifischen Mails Überspringen: ✅ Dok:
         if (bo.IsAppSpecificMail) { continue; }


         msgProcessed++;
      }
   }

   #endregion Iterator-Funktionen

}
