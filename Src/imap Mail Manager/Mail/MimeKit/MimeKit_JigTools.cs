using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using ImapMailManager.Config;
using jig.Tools;
using jig.Tools.HTML;
using jig.Tools.String;
using MailKit;
using MailKit.Search;
using MimeKit;


namespace ImapMailManager.Mail.MimeKit;

/// <summary>
/// Tools für MimeKit
/// </summary>
public class MimeKit_JigTools {

   #region Mail Bodies, Inhalt

   #region MIME Parts

   /// <summary>
   /// Enumeriert die MIME-Parts
   /// </summary>
   /// <param name="msg"></param>
   public static void Enumerate_MimeParts(MimeMessage? msg) {
      if (msg == null) {
         return;
      }

      // Ex
      //    MimeKit.TextPart
      var bodyType = msg.Body.GetType();

      var iter = new MimeIterator(msg);

      // collect our list of attachments and their parent multiparts
      while (iter.MoveNext()) {
         var multipart = iter.Parent as Multipart;
         var part      = iter.Current as MimePart;

         if (multipart != null && part != null && part.IsAttachment) {
            // keep track of each attachment's parent multipart
            // multiparts.Add(multipart);
            // attachments.Add(part);
         }
      }

   }


   /// <summary>
   /// Liest die Mime Text- und Html Elemente der Msg
   /// Dient dazu, eine bestehende E-Mail zu verändern
   /// 
   /// ! Beide Elemente können null sein!
   /// 
   /// <seealso cref="ImapMailManager.Mail.BL_Mail_Body.Get_MsgHtmlBody_Complete"/>
   /// <seealso cref="ImapMailManager.Mail.BL_Mail_Body.Get_MsgHtmlBody_BodyPartOnly"/>
   /// 
   /// !Q https://stackoverflow.com/a/68583177/4795779
   /// </summary>
   /// <param name="msg"></param>
   /// <param name="ensure_Complete_HtmlDoc">
   /// Sicherstellen, dass der htmlPart ein vollständiges HTML-Dokument inkl. Head & Body ist
   /// </param>
   /// <returns>
   ///   var (textPart, htmlPart) = …  
   /// </returns>
   [Obsolete("Diese Funktion ist obsolete » MimeKit_JigTools.Get_Msg_Bodies", false)]
   public static Tuple<TextPart?, TextPart?> Get_Msg_Mime_TextParts(MimeMessage msg) {
      TextPart? textPart = msg.BodyParts.OfType<TextPart>().FirstOrDefault(x => x.IsPlain);
      TextPart? htmlPart = msg.BodyParts.OfType<TextPart>().FirstOrDefault(x => x.IsHtml);

      return new Tuple<TextPart?, TextPart?>(textPart, htmlPart);
   }

   #endregion MIME Parts


   /// <summary>
   /// Holt aus Msg die Text- und HTML Bodies
   /// </summary>
   /// <param name="msg"></param>
   /// <param name="ensure_Complete_HtmlDoc"></param>
   /// Sicherstellen, dass der htmlPart ein vollständiges HTML-Dokument inkl. Head & Body HTML Elemente
   /// <returns>
   /// var (textBodyText, htmlBodyText) = …  
   /// </returns>
   public static Tuple<string?, string?> Get_Msg_Bodies(MimeMessage msg, bool ensure_Complete_HtmlDoc) {
      //+ Den Text Body lesen
      TextPart? textPart     = msg.BodyParts.OfType<TextPart>().FirstOrDefault(x => x.IsPlain);
      var       textPartText = textPart != null ? textPart.Text ?? "" : "";

      //+ Den HTML Body lesen
      //++ 1. Versuch: direkt das Mime Element lesen
      TextPart? htmlPart     = msg.BodyParts.OfType<TextPart>().FirstOrDefault(x => x.IsHtml);
      var       htmlPartText = htmlPart?.Text;

      //+++ Wenn das Mime-Element leer war: 
      if (htmlPartText != null) {
         if (ensure_Complete_HtmlDoc) {
            var htmlDoc = HtmlTools.Ensure_Complete_HtmlDoc
               (ConfigMail_MsgHtmlBody.Default_HtmlDoc_Structure, htmlPartText);
            htmlPartText = htmlDoc.DocumentNode.OuterHtml;
         }
      }
      else {
         // Fallback: mit dem Parser arbeiten
         htmlPartText = BL_Mail_Body.Get_MsgHtmlBody_Complete(msg, ensure_Complete_HtmlDoc);
      }

      return new Tuple<string?, string?>(textPartText, htmlPartText);
   }


   /// <summary>
   /// Setzt die neuen Text- und HTML-Bodies
   /// !M http://www.mimekit.net/docs/html/Creating-Messages.htm#UsingBodyBuilder
   /// </summary>
   public static void Update_Msg_Bodies(MimeMessage msg, string? textBodyText, string? htmlBodyText) {
      var builder = new BodyBuilder();

      // Die Anhänge übernehmen
      foreach (var attachment in msg.Attachments) {
         //‼ winmail.dat nicht übertragen, weil die Datei Infos zum alten Mail-Inhalt hat
         if (!attachment.ContentDisposition.FileName.ØEqualsIgnoreCase("winmail.dat")) {
            builder.Attachments.Add(attachment);
         }
      }

      // Den Text und HTML Body übernehmen
      if (textBodyText.ØHasValue()) { builder.TextBody = textBodyText; }

      if (htmlBodyText.ØHasValue()) { builder.HtmlBody = htmlBodyText; }

      msg.Body = builder.ToMessageBody();
   }

   #endregion Mail Bodies, Inhalt

   #region Msg Flags (Read, Seen, …)

   /// <summary>
   /// Markiert eine E-Mail als gelesen
   /// !KH https://stackoverflow.com/a/31994027/4795779
   /// </summary>
   /// <param name="mbxFolder"></param>
   /// <param name="uid"></param>
   public static void MarkMsg_AsRead(IMailFolder mbxFolder, UniqueId uid) {
      //+ Ordner öffnen
      mbxFolder.ØOpen_ReadWriteAccess();

      //+ Das Flag setzen
      mbxFolder.AddFlags(uid, MessageFlags.Seen, true);
   }


   /// <summary>
   /// Markiert eine E-Mail als gelesen
   /// !KH https://stackoverflow.com/a/31994027/4795779
   /// </summary>
   /// <param name="mbxFolder"></param>
   /// <param name="uid"></param>
   public static void MarkMsg_AsRead(IMailFolder mbxFolder, IList<UniqueId> uids) {
      //+ Ordner öffnen
      mbxFolder.ØOpen_ReadWriteAccess();

      //+ Das Flag setzen
      mbxFolder.AddFlags(uids, MessageFlags.Seen, true);
   }


   /// <summary>
   /// Markiert eine E-Mail als ungelesen
   /// !KH https://stackoverflow.com/a/31994027/4795779
   /// </summary>
   /// <param name="mbxFolder"></param>
   /// <param name="uid"></param>
   public static void MarkMsg_AsUnread(IMailFolder mbxFolder, UniqueId uid) {
      //+ Ordner öffnen
      mbxFolder.ØOpen_ReadWriteAccess();

      //+ Das Flag löschen
      mbxFolder.RemoveFlags(uid, MessageFlags.Seen, true);
   }
   /// <summary>
   /// Markiert eine E-Mail als ungelesen
   /// !KH https://stackoverflow.com/a/31994027/4795779
   /// </summary>
   /// <param name="mbxFolder"></param>
   /// <param name="uid"></param>
   public static void MarkMsg_AsUnread(IMailFolder mbxFolder, IList<UniqueId> uids) {
      //+ Ordner öffnen
      mbxFolder.ØOpen_ReadWriteAccess();

      //+ Das Flag löschen
      mbxFolder.RemoveFlags(uids, MessageFlags.Seen, true);
   }


   /// <summary>
   /// Liefert true, wenn msg gelesen wurde
   /// </summary>
   /// <param name="msgSummary"></param>
   /// <returns></returns>
   public static bool Is_Msg_Marked_AsRead(
      IMessageSummary msgSummary) {
      return msgSummary.Flags != null && msgSummary.Flags.Value.HasFlag(MessageFlags.Seen);
   }

   #endregion Msg Flags (Read, Seen, …)

}
