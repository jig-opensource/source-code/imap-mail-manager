using System.Collections.Generic;
using System.Linq;
using jig.Tools;
using jig.Tools.String;
using MailKit;
using MailKit.Search;


namespace ImapMailManager.Mail.MimeKit;

public static class MimeKit_JigTools_ExtensionMethods {

   #region IMailFolder

   #region SearchQuery

   /// <summary>
   /// HeaderSearchQuery() gibt die Suche an den IMAP-Server weiter
   /// Ja nach IMAP-Server liefert die Suche unterschiedliche Resultate
   /// Diese Funktion sucht E-Mails, die bestimmte Header mit exakten Werten haben
   ///
   /// !i  https://stackoverflow.com/a/39508906/4795779
   /// </summary>
   /// <param name="folder"></param>
   /// <param name="headerSearchQuery"></param>
   /// <returns></returns>
   public static IList<UniqueId> ØSearch_Header_ValueEquals(this IMailFolder? folder, string? hdrName, string? hdrValue) {
      if (folder == null || !hdrName.ØHasValue()) { return new List<UniqueId>(); }
      // hdrValue allenfalls auf "" setzen 
      hdrValue ??= "";

      //+ Die normale Suche starten
      var foundUids = folder.Search(SearchQuery.HeaderContains(hdrName, hdrValue));
      
      //+ Prüfen, welche gefundenen Resultate tatsächlich der Suche genügen
      //++ Die IMessageSummary abrufen 
      IList<IMessageSummary> found_MessageSummaries = folder.Fetch(foundUids
                                                                   , MessageSummaryItems.Envelope
                                                                   | MessageSummaryItems.Headers);

      //++ Das Resultat filtern
      return found_MessageSummaries.Where(m => m.Headers[hdrName]
                                                .ØEqualsIgnoreCase(hdrValue))
                                   .Select(x => x.UniqueId).ToList();
   }

   #endregion SearchQuery

   #region Fetch

   /// <summary>
   /// Wrapper für:
   /// <see cref="MailKit.IMailFolderExtensions.Fetch"/>
   ///
   /// Stellt sicher, dass Unkarheiten im API 
   /// nicht zu Code-Fehler werden
   /// 
   /// </summary>
   /// <param name="folder"></param>
   /// <param name="uids"></param>
   /// <param name="items"></param>
   /// <param name="headers"></param>
   /// <returns></returns>
   public static IList<IMessageSummary> ØFetch(
      this IMailFolder      folder
      , IList<UniqueId>     uids
      , MessageSummaryItems items
      , IEnumerable<string> headers
   ) {

      // Sortieren: zuerst die neuste E-Mail
      // !Q https://stackoverflow.com/a/58580564/4795779						
      // var messages = imapFolder.Fetch(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.InternalDate).ToList();
      // messages.Sort(new OrderBy[] { OrderBy.ReverseDate });

      // ‼ Wenn MessageSummaryItems.Headers gesetzt ist,
      // dann werden alle Header gelesen und headers muss nicht definiert werden
      // » Sonst würden nur die in headers aufgelisteten Elemente angerufen,
      //   auch, wenn MessageSummaryItems.Headers nicht angegeben wäre!

      // Sortieren: zuerst die neuste E-Mail
      // !Q https://stackoverflow.com/a/58580564/4795779						
      // var messages = imapFolder.Fetch(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.InternalDate).ToList();
      // messages.Sort(new OrderBy[] { OrderBy.ReverseDate });

      if (items.IsSet(MessageSummaryItems.Headers)) {
         return folder.Fetch(uids, items);
      }
      else {
         return folder.Fetch
            (uids
             , items
             , headers);
      }
   }


   /// <summary>
   /// Wrapper für:
   /// <see cref="MailKit.IMailFolderExtensions.Fetch"/>
   ///
   /// Stellt sicher, dass Unkarheiten im API 
   /// nicht zu Code-Fehler werden
   /// 
   /// </summary>
   /// <param name="folder"></param>
   /// <param name="uids"></param>
   /// <param name="items"></param>
   /// <param name="headers"></param>
   /// <returns></returns>
   public static IList<IMessageSummary> ØFetch(
      this IMailFolder      folder
      , int                 min
      , int                 max
      , MessageSummaryItems items
      , IEnumerable<string> headers
   ) {
      // ‼ Wenn MessageSummaryItems.Headers gesetzt ist,
      // dann werden alle Header gelesen und headers muss nicht definiert werden
      // » Sonst würden nur die in headers aufgelisteten Elemente angerufen,
      //   auch, wenn MessageSummaryItems.Headers nicht angegeben wäre!

      // Sortieren: zuerst die neuste E-Mail
      // !Q https://stackoverflow.com/a/58580564/4795779						
      // var messages = imapFolder.Fetch(0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.InternalDate).ToList();
      // messages.Sort(new OrderBy[] { OrderBy.ReverseDate });

      if (items.IsSet(MessageSummaryItems.Headers)) {
         return folder.Fetch
            (min
             , max
             , items);
      }
      else {
         return folder.Fetch
            (min
             , max
             , items
             , headers);
      }
   }

   #endregion Fetch

   #endregion IMailFolder

}
