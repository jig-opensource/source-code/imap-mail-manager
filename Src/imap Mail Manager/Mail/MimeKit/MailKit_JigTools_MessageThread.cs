using System;
using System.Collections.Generic;
using System.Linq;
using ImapMailManager.BO;
using ImapMailManager.Config;
using ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;
using jig.Tools;
using jig.Tools.String;
using MailKit;


namespace ImapMailManager.Mail.MimeKit;

/// <summary>
/// Tools für MailKit.MessageThread
/// </summary>
public class MailKit_JigTools_MessageThread {

   /// <summary>
   /// Sucht rekursiv im MessageThread nach der Msg mit der messageID
   /// ! Rekursiv
   /// </summary>
   /// <param name="messageThread"></param>
   /// <param name="messageID"></param>
   /// <returns></returns>
   public static IEnumerable<IMessageSummary?> SearchMessageThread_Recursive(MessageThread messageThread, string messageID) {
      foreach (MessageThread messageThreadChild in messageThread.Children) {
         var thisMsgId = messageThreadChild.Message?.Envelope?.MessageId ?? "";

         if (thisMsgId.ØEqualsIgnoreCase(messageID)) {
            yield return messageThreadChild.Message;
         }

         foreach (var messageSummary in SearchMessageThread_Recursive(messageThreadChild, messageID)) {
            yield return messageSummary;
         }
      }

      var msgId = messageThread.Message?.Envelope?.MessageId ?? "";

      if (msgId.ØEqualsIgnoreCase(messageID)) {
         yield return messageThread.Message;
      }
   }


   /// <summary>
   /// Holt rekursiv alle Messages im MessageThread
   /// ! Rekursiv
   /// </summary>
   /// <param name="messageThread"></param>
   /// <returns>
   /// var (level, messageSummary) = GetMessageThread_Messages_Recusrive(…) 
   /// </returns>
   public static IEnumerable<IMessageSummary> GetMessageThread_Messages_Recursive(
      MessageThread messageThread) {

      if (messageThread.Message != null) {
         yield return messageThread.Message;
      }

      foreach (MessageThread messageThreadChild in messageThread.Children) {
         if (messageThreadChild.Message != null) {
            yield return messageThreadChild.Message;
         }

         foreach (var messageSummary in GetMessageThread_Messages_Recursive(messageThreadChild)) {
            yield return messageSummary;
         }
      }
   }


   /// <summary>
   /// Sammelt in einem Msg Thread alle MailSummery
   /// </summary>
   /// <param name="messageThread"></param>
   /// <returns></returns>
   public static List<IMessageSummary> GetMessageThread_Messages(MessageThread messageThread) {
      // Alle Nachrichten des Threads sammeln 
      var                   foundMessages = GetMessageThread_Messages_Recursive(messageThread);

      // Alle distinct Nachrichten sammeln
      List<IMessageSummary> threadMsgs    = new();
      foreach (var foundMsg in foundMessages) {
         // Haben wir die nachricht bereits in der Liste?
         var weveAlreadyGotMsg = threadMsgs.Any
            (x => x.Envelope.MessageId.ØEqualsIgnoreCase(foundMsg.Envelope.MessageId)
                  && x.Folder.FullName.ØEqualsIgnoreCase(foundMsg.Folder.FullName));
         // Nein: Sammeln
         if (!weveAlreadyGotMsg) {
            threadMsgs.Add(foundMsg);
         }
      }
      return threadMsgs;
   }


   /// <summary>
   /// Sucht in allen messageThreads nach den Nachricht mit der gesuchten Message-ID
   /// 
   /// !KH9 Tatsächlich kann in einem IMAP Server eine Mail mit der identischen Message-ID mehrfach vorkommen,
   /// ev. passiert das, wenn eine Mail verändert wurde und dann die alte Version im Papierkob ist
   /// 
   /// </summary>
   /// <param name="messageID"></param>
   /// <param name="stopSearchOnFirstFoundItem">
   /// Stoppt die Suche, sobald eine Nachricht ausserhalb des Papierkorbs gefunden wurde
   /// </param>
   /// <returns></returns>
   public static List<IMessageSummary?> SearchMsg_inMsgThreads(
      IList<MessageThread> messageThreads
      , string             messageID
      , bool               stopSearchOnFirstFoundItem = true) {
      List<IMessageSummary?> res = new();

      foreach (var thisMsgThread in messageThreads) {
         // Im Thread rekursiv alle Elemente durchsuchen
         IEnumerable<IMessageSummary?> foundMsgs = SearchMessageThread_Recursive(thisMsgThread, messageID);

         // Nur zufügen, wenn wir das Element noch nicht haben
         foreach (IMessageSummary? messageSummary in foundMsgs) {
            var hasItem = res.Any
               (x => x.Envelope.MessageId.ØEqualsIgnoreCase(messageSummary.Envelope.MessageId)
                     && x.Folder.FullName.ØEqualsIgnoreCase(messageSummary.Folder.FullName));

            if (!hasItem) {
               res.Add(messageSummary);

               // Stoppen, sobald wir das erste Element haben, das nicht im Papirtkorb ist
               if (stopSearchOnFirstFoundItem && !messageSummary.Folder.Attributes.IsSet(FolderAttributes.Trash)) {
                  // Elemente im Papierkorb zuletzt
                  return res.OrderBy(x => x.Folder.Attributes.IsSet(FolderAttributes.Trash)).ToList();
               }
            }
         }
      }

      // Elemente im Papierkorb zuletzt
      return res.OrderBy(x => x.Folder.Attributes.IsSet(FolderAttributes.Trash)).ToList();
   }


   /// <summary>
   /// Sucht in allen Message-Threads *nicht* rekursiv nach der Mail mit der Message-ID
   /// und liefert die gefundenen Threads zurück
   /// 
   /// Wenn nichts gefunden wird, wird die rekursive Suche gestartet
   ///
   /// !Ex
   ///  // Für diese Msg den Thread suchen
   ///  var msgMainId = "f8a913238432711c966d8d9405e77e6b@rogerliebi.ch";
   ///  var msgMainThread = MailKit_JigTools_MessageThread.GetMessageThread_ForMsg(mailboxIterator.AllMsgSummary, msgMainId);
   ///  // Alle Nachrichten im Thread abrufen
   ///  var msgMainThread = MailKit_JigTools_MessageThread.GetMessageThread_ForMsg(mailboxIterator.AllMsgSummary, msgMainId);
   /// 
   /// </summary>
   /// <param name="AllMsgSummaries"></param>
   /// <param name="messageID"></param>
   /// <returns></returns>
   public static List<MessageThread> GetMessageThread_ForMsg(List<IMessageSummary> AllMsgSummaries, string messageID) {
      // Die MsgSummary mit unserer MessageID suchen
      // Sortiert, so dass die gelöschten Elemente am Ende sind
      IOrderedEnumerable<IMessageSummary> thisMsg = AllMsgSummaries
                                                   .Where(x => x.Envelope.MessageId.ØEqualsIgnoreCase(messageID))
                                                   .OrderBy(x => x.Folder.Attributes.IsSet(FolderAttributes.Trash));

      // Die Referenz-Nummern unserer Nachricht sammeln
      // !KH9 rfc 3.6.4. Identification Fields:
      // https://www.rfc-editor.org/rfc/rfc5322.html#section-3.6.4
      // - Message-ID:
      // - In-Reply-To:
      // - References:
      List<string> thisMailRefIDs = new List<string> { messageID };
      thisMailRefIDs.AddRange(thisMsg.First().References);

      // Alle Threads bestimmen
      var messageSummaryThreads = AllMsgSummaries.ØGetMessageSummaryThreads();

      // Die Threads suchen, in der unsere Nachricht ist
      var thisMailMsgThread = messageSummaryThreads.Where
         (x =>
             x.Message?.Envelope?.MessageId != null
             && thisMailRefIDs.Contains(x.Message.Envelope.MessageId)).ToList();

      // Nichts gefunden: rekursiv suchen,
      // weil unsere Nachricht in einer unteren Ebende der Thread-Hierarchie sein kann 
      if (!thisMailMsgThread.Any()) {
         return SearchMsgThread_forMsg(messageSummaryThreads, messageID);
      }

      return thisMailMsgThread;
   }


   /// <summary>
   /// Sucht rekursiv in den messageThreads nach der Mail mit der Message-ID
   /// und liefert die gefundenen *Threads* zurück
   /// </summary>
   /// <param name="messageThreads"></param>
   /// <param name="messageID"></param>
   /// <param name="stopSearchOnFirstFoundItem"></param>
   /// <returns></returns>
   public static List<MessageThread> SearchMsgThread_forMsg(
      IList<MessageThread> messageThreads
      , string             messageID
      , bool               stopSearchOnFirstFoundItem = true) {
      List<MessageThread> res = new();

      // Jeden einzelnen Thread durchsuchen
      foreach (var thisMsgThread in messageThreads) {
         // Im Thread rekursiv alle Elemente durchsuchen
         var foundMsgs = SearchMessageThread_Recursive(thisMsgThread, messageID);

         if (foundMsgs.Any()) {
            res.Add(thisMsgThread);

            // Beim ersten Fund die Suche abbrechen?
            if (stopSearchOnFirstFoundItem) { return res; }
         }
      }

      return res;
   }


   /// <summary>
   /// Aktualisiert bo_Mail Metadaten aus Mails vom gleiche Maiol-Thread
   /// </summary>
   /// <param name="appMailHeaderTypes"></param>
   /// <param name="bo_Mail"></param>
   /// <returns>
   /// var metadataMissing = UpdateMetadata_FromMsgThread() 
   /// </returns>
   public static bool UpdateMetadata_FromMsgThread(ConfigMail_MsgHeader.AppMailHeaderType[] appMailHeaderTypes, BO_Mail bo_Mail) {

      Process_AllMails_Get_MessageThreads mbxMsgThreads = BO_Mail_BL_MailThread_Mgr.Get_Mailbox_MailThreadsData(bo_Mail.ImapSrvRuntimeConfig.Cfg_ImapServerCred);
      
      var myMailMessageID = bo_Mail.MsgHdr_MessageId;
      // Den Thread unserer Nachricht suchen
      var msgMainThread   = GetMessageThread_ForMsg(mbxMsgThreads.AllMsgSummary, myMailMessageID);
      // Die Mails unseres Threads abrufen
      var threadMsgs        = GetMessageThread_Messages(msgMainThread.First());
      // Den Index unserer Nachricht suchen
      int idxMyMail;
      for (idxMyMail = 0; idxMyMail < threadMsgs.Count; idxMyMail++) {
         var messageSummary = threadMsgs[idxMyMail];
         if (messageSummary.Envelope.MessageId.ØEqualsIgnoreCase(myMailMessageID)) {
            break;
         }
      }

      // Rückwärts / aufwärts die Metadaten übernehmen
      bool metadataMissing = true;
      for (var idx = idxMyMail - 1; idx >= 0; idx--) {
         var messageSummary = threadMsgs[idx];
         var src            = new BO_Mail(messageSummary);
         // Die Metadaten übertragen
         metadataMissing = BO_Mail_BL_UpdateOrSet_Metadata_MsgHdr.UpdateMsgMetadata(appMailHeaderTypes, src, bo_Mail);

         // Wenn wir alle Metadaten übertragen konnten, sind wir fertig
         if (!metadataMissing) {
            break;
         }
         
      }
      
      return metadataMissing;

   }
   
}
