using System;
using System.Collections.Generic;
using ImapMailManager.Config;
using ImapMailManager.Mail.Stats;
using jig.Tools;
using MailKit;


namespace ImapMailManager.Mail.Hierarchy;

/// <summary>
/// Sortiert die IMAP Ordner
/// </summary>
class FolderComparer : IComparer<IMailFolder> {

   #region Public Methods and Operators

   public int Compare(IMailFolder? x, IMailFolder? y)
      => string.Compare(x?.Name ?? "", y?.Name ?? "", StringComparison.CurrentCulture);

   #endregion

}


/// <summary>
/// Für die Hierarchy eine Node
/// ein Datenelement mit Informationen zu einem IMAP Mailbox Ordner
/// </summary>
public class ImapFolderNde {

   /// <summary>
   /// Die Kategorie dieser E-Mail
   /// </summary>
   public readonly CalcImapStats.eMailCategory MailCategory;


   #region Constructors and Destructors

   /// <summary>
   /// Für die Hierarchy eine Node
   /// ein Datenelement mit Informationen zu einem IMAP Mailbox Ordner
   /// </summary>
   /// <param name="oImapFolder"></param>
   /// <param name="Path"></param>
   /// <param name="NoOfMails"></param>
   /// <param name="NoOfUnread"></param>
   /// <param name="SizeBytes"></param>
   public ImapFolderNde(IMailFolder oImapFolder,
                        string      Path,        int      NoOfMails,
                        int         NoOfUnread,  ulong    SizeBytes,
                        DateTime    ÄltesteMail, DateTime NeusteMail) {
      this.oImapFolder = oImapFolder;
      this.Path        = Path;
      this.NoOfMails   = NoOfMails;
      this.NoOfUnread  = NoOfUnread;
      this.SizeBytes   = SizeBytes;
      this.ÄltesteMail = ÄltesteMail;
      this.NeusteMail  = NeusteMail;

      //++ Die E-Mail Kategorie berechen,
      //! Siehe auch: ImapMsgNde.CalculateMailCategory
      MailCategory = CalculateMailCategoryByFolder(oImapFolder.FullName);
      
   }

   #endregion


   #region Public Properties

   public DateTime? ÄltesteMail { get; set; }
   public DateTime? NeusteMail  { get; set; }
   /// <summary></summary>
   public int NoOfMails { get; init; }
   /// <summary></summary>
   public int NoOfUnread { get; init; }

   /// <summary></summary>
   public IMailFolder oImapFolder { get; init; }
   /// <summary></summary>
   public string Path { get; init; }
   /// <summary></summary>
   public ulong SizeBytes { get; init; }

   #endregion


   #region Public Methods and Operators

   
   /// <summary>
   /// Berechnet aufgrund des Postfach-Ordners die E-Mail Kategorie
   ///
   /// Siehe auch: ImapMsgNde.CalculateMailCategory
   /// 
   /// </summary>
   /// <param name="ordnerPfad"></param>
   /// <returns></returns>
   public static CalcImapStats.eMailCategory CalculateMailCategoryByFolder(string ordnerPfad) {

      if (ConfigMail_Basics.Kat_Folder_MailsSpam.ØStartsWithAnyItemInList(ordnerPfad)) {
         return CalcImapStats.eMailCategory.Spam;
      }
      else if (ConfigMail_Basics.Kat_Folder_MailsDeleted.ØStartsWithAnyItemInList(ordnerPfad)) {
         return CalcImapStats.eMailCategory.Gelöscht;
      }
      else if (ConfigMail_Basics.Kat_Folder_MailsInArbeit.ØStartsWithAnyItemInList(ordnerPfad)) {
         return CalcImapStats.eMailCategory.FrageInArbeit;
      }
      else if (ConfigMail_Basics.Kat_Folder_GeschickteAntworten.ØStartsWithAnyItemInList(ordnerPfad)) {
         return CalcImapStats.eMailCategory.Antwort;
      }
      else if (ConfigMail_Basics.Kat_Folder_ErledigteFragen.ØStartsWithAnyItemInList(ordnerPfad)) {
         return CalcImapStats.eMailCategory.FrageErledigt;
      }
      else {
         // Default
         return CalcImapStats.eMailCategory.FrageOffen;
      }
   }


   public void Deconstruct(out IMailFolder oImapFolder, out string    Path, out int NoOfMails, out int NoOfUnread, out ulong SizeBytes,
                           out DateTime?   ÄltesteMail, out DateTime? NeusteMail) {
      oImapFolder = this.oImapFolder;
      Path        = this.Path;
      NoOfMails   = this.NoOfMails;
      NoOfUnread  = this.NoOfUnread;
      SizeBytes   = this.SizeBytes;
      ÄltesteMail = this.ÄltesteMail;
      NeusteMail  = this.NeusteMail;
   }


   public string GetCsv(string kategorie)
      => $"{kategorie};{Path};{NoOfMails};{NoOfUnread};{Math.Truncate((double)SizeBytes/1024)};{ÄltesteMail?.ToString("dd.MM.yy HH:mm") ?? "-"};{NeusteMail?.ToString("dd.MM.yy HH:mm")}";


   public override string ToString() => $"F: {Path} ({NoOfUnread}/{NoOfMails}), {SizeBytes.ØToUnitStr(0, "")}";

   #endregion

}
