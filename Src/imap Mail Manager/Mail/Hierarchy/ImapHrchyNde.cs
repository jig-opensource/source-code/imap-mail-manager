using ImapMailManager.Mail.Hierarchy;


namespace ImapMailManager.Mail.Hierarchy;

/// <summary>
/// Das Datenelement jeder Node in der Hierarchy
/// Sammelt für jeden Datentyp die Elemente:
/// - ImapPersonalNamespaceNde  
/// - ImapFolderNde 
/// - ImapMsgNde  
/// </summary>
public class ImapHrchyNde {

   #region Constructors and Destructors

   /// <summary>
   /// Konstruktoren
   /// </summary>
   /// <param name="ImapPersonalNamespaceNde"></param>
   public ImapHrchyNde(ImapPersonalNamespaceNde imapPersonalNamespaceNde)
      => ImapPersonalNamespaceNde = imapPersonalNamespaceNde;


   public ImapHrchyNde(ImapFolderNde imapImapFolder) => ImapFolderNde = imapImapFolder;

   public ImapHrchyNde(ImapMsgNde imapMsg) => ImapMsgNde = imapMsg;

   #endregion


   #region Public Properties

   /// <summary>
   /// Das Datenelement für einen IMAP Ordner
   /// </summary>
   public ImapFolderNde? ImapFolderNde { get; set; }
   /// <summary>
   /// Das Datenelement für eine E-Mail
   /// </summary>
   public ImapMsgNde? ImapMsgNde { get; set; }

   /// <summary>
   /// Das Datenelement für den IMAP Personal Namespace
   /// </summary>
   public ImapPersonalNamespaceNde? ImapPersonalNamespaceNde { get; set; }

   #endregion


   #region Public Methods and Operators

   public override string ToString()
      => ImapPersonalNamespaceNde?.ToString()
         ?? ImapFolderNde?.ToString() ?? ImapMsgNde?.ToString() ?? "(unbekannter Typ)";

   #endregion

}
