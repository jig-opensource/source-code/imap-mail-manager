using MailKit;


namespace ImapMailManager.Mail.Hierarchy;

/// <summary>
/// Für eine Hierarchy eine Node
/// ein Datenelement mit Informationen zu einem IMAP PersonalNamespaces
/// </summary>
/// <param name="oImapFolder"></param>
/// <param name="Path"></param>
/// <param name="NoOfMails"></param>
/// <param name="NoOfUnread"></param>
/// <param name="SizeBytes"></param>
public record ImapPersonalNamespaceNde(
   IMailFolder oImapFolder,
   string      Path,       int   NoOfMails,
   int         NoOfUnread, ulong SizeBytes) {

   public override string ToString() => $"Personal Namespace: {Path}";

}