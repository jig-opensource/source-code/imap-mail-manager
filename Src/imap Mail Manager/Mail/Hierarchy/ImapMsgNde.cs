using System;
using System.Linq;
using System.Windows.Xps;
using DocumentFormat.OpenXml.Bibliography;
using ImapMailManager.Config;
using ImapMailManager.Mail.Stats;
using jig.Tools;
using jig.Tools.String;
using MailKit;
using MimeKit;


namespace ImapMailManager.Mail.Hierarchy;

/// <summary>
/// Für die Hierarchy eine Node
/// ein Datenelement mit Informationen zu einer E-Mail
/// </summary>
public class ImapMsgNde {

   /// <summary>
   /// Die Kategorie dieser E-Mail
   /// </summary>
   public readonly CalcImapStats.eMailCategory MailCategory;

   /// <summary>
   /// Für die Hierarchy eine Node
   /// ein Datenelement mit Informationen zu einer E-Mail
   /// </summary>
   public readonly IMailFolder OImapFolder;
   public readonly string     OrdnerPfad;
   public readonly HeaderList MailHeaders;

   /// <summary>
   /// Ist null, wenn leer 
   /// </summary>
   public readonly string? EnvelopeMessageId;

   public string FromDomain;

   public InternetAddressList _from;

   /// <summary>
   ///   From  : "Thomas Schittli" <Thomas.Schittli@jig.ch>
   /// </summary>
   public InternetAddressList From {
      get => _from;
      private init {
         _from      = value;
         FromDomain = _from.Mailboxes.Select(x => x.Domain).FirstOrDefault();
      }
   }

   /// <summary>
   /// Sender: "Thomas Schittli" <outlook_5631ED86E3A3F8EF@outlook.com>
   /// » Wird zZ nicht initialisiert
   /// </summary>

   // public readonly InternetAddressList Sender;
   public readonly InternetAddressList To;
   public readonly string          Subject;
   public readonly string          SubjectNormalized;
   public readonly uint?           SizeBytes;
   public readonly DateTimeOffset? ADate;


   public ImapMsgNde(
      IMailFolder           oImapFolder
      , string?             envelopeMessageId
      , DateTimeOffset?     aDate
      , InternetAddressList From
      , InternetAddressList To
      , string              Subject
      , string              SubjectNormalized
      , uint?               SizeBytes
      , HeaderList          mailHeaders) {
      EnvelopeMessageId      = envelopeMessageId;
      this.From              = From;
      this.To                = To;
      this.Subject           = Subject;
      this.SubjectNormalized = SubjectNormalized;
      this.SizeBytes         = SizeBytes;
      OImapFolder            = oImapFolder;
      OrdnerPfad             = oImapFolder.FullName;
      ADate                  = aDate;
      MailHeaders            = mailHeaders;

      // Debug
      if (Subject != null
          && Subject.Equals
             ("#28-5438 • RL Formular: Biblische Frage", StringComparison.OrdinalIgnoreCase)) {
         int rrrr = 1;
      }

      //++ Die E-Mail Kategorie berechen,
      //! Siehe auch: ImapFolderNde.CalculateMailCategory
      MailCategory = CalculateMailCategory(OImapFolder.FullName, FromDomain);
   }


   /// <summary>
   /// Berechnet die E-Mail Kategoerie
   ///
   /// Siehe auch: ImapFolderNde.CalculateMailCategory
   /// 
   /// </summary>
   /// <param name="ordnerPfad"></param>
   /// <param name="senderEMailDomain"></param>
   /// <returns></returns>
   public CalcImapStats.eMailCategory CalculateMailCategory(
      string   ordnerPfad
      , string senderEMailDomain) {
      // Ist genauer als nur die Klassifizierung aufgrund des Mailbox-Ordners,
      // weil die E-Mail Adresse und die Domäne der Absender-E-Mail Adresse berücksichtig werden kann

      //+++ Wenn die E-Mail im Spam-Ordner ist
      if (ConfigMail_Basics.Kat_Folder_MailsSpam.ØStartsWithAnyItemInList(ordnerPfad)) {
         return CalcImapStats.eMailCategory.Spam;
      }

      //+++ Wenn die E-Mail im Gelöscht-Ordner ist
      if (ConfigMail_Basics.Kat_Folder_MailsDeleted.ØStartsWithAnyItemInList
             (ordnerPfad)) {
         return CalcImapStats.eMailCategory.Gelöscht;
      }

      //+++ Wenn die E-Mail im Entwurf- oder Arbeit-Ordner ist
      if (ConfigMail_Basics.Kat_Folder_MailsInArbeit.ØStartsWithAnyItemInList
             (ordnerPfad)) {
         return CalcImapStats.eMailCategory.FrageInArbeit;
      }

      //+++ Wenn die E-Mail im Entwurf- oder Arbeit-Ordner ist
      if (ConfigMail_Basics.Kat_Folder_GeschickteAntworten.ØStartsWithAnyItemInList
             (ordnerPfad)) {
         return CalcImapStats.eMailCategory.Antwort;
      }

      //+++ Wenn die E-Mail in einem Antworten-Ordner ist
      if (ConfigMail_Basics.Kat_Folder_ErledigteFragen.ØStartsWithAnyItemInList
             (ordnerPfad)) {
         return CalcImapStats.eMailCategory.FrageErledigt;
      }

      //+ Dürfte nierelevant sein, weil die Ordner bereits die Zugehörogkeit definieren...
      // aber es könnte ja trotzdem sein:

      //+++ Wenn die Absender E-Mail Adresse eindeutig einer Frage zugeordnet werden kann
      if (From.Mailboxes.Any
             (x => ConfigMail_Basics.Kat_FromAddr_Fragen
                                                .ØStartsWithAnyItemInList(x.Address))) {
         // Dann ist die Kategorie entsprechend dem Ordner
         return ImapFolderNde.CalculateMailCategoryByFolder(ordnerPfad);
      }

      //+++ Wenn die Absender E-Mail Adresse eindeutig einer Antwort zugeordnet werden kann
      if (From.Mailboxes.Any
             (x => ConfigMail_Basics.Kat_FromAddr_Antworten
                                                .ØStartsWithAnyItemInList(x.Address))) {
         return CalcImapStats.eMailCategory.Antwort;
      }

      //+++ Wenn die E-Mail keinen Absender-Domänennamen hat
      if (!senderEMailDomain.ØHasValue()) {
         // Könnte ein Entwurf seinè
         return CalcImapStats.eMailCategory.AbsenderUnbekannt;
      }

      //+++ Wenn die Absender E-Mail Domäne ziemlich sicher der Antwort-Domäne entspricht
      if (From.Mailboxes.Any
             (x => ConfigMail_Basics.Kat_FromAddr_DomainsAreProbablyAntworten
                                                .ØEqualsWithAnyItemInList(x.Domain))) {
         return CalcImapStats.eMailCategory.Antwort;
      }

      // Sonst wissen wir nicht, in welche Kategorie die E-Mail gehört
      return CalcImapStats.eMailCategory.CatUnkown;
   }


   public override string ToString() {
      // !M https://learn.microsoft.com/en-us/dotnet/standard/base-types/custom-date-and-time-format-strings
      // return $"Msg: {(aDate.HasValue ? aDate.Value.ToString("d.M.yy HH:mm") : "")} {SubjectNormalized.ØSubstring(0, 15)}";
      return
         $"Msg: {MailCategory}: {(ADate.HasValue ? ADate.Value.ToString("dd.MM.yy HH:mm") : "")} {SubjectNormalized.ØSubstring(0, 15)} • {SubjectNormalized}";
   }

}
