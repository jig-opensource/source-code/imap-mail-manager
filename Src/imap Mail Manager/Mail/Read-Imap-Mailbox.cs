﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Hierarchy;
using ImapMailManager.BO;
using ImapMailManager.Config;
using ImapMailManager.Mail.Hierarchy;
using ImapMailManager.Mail.MimeKit;
using ImapMailManager.Properties;
using jig.Tools;
using jig.Tools.IEqualityComparer;
using jig.Tools.String;
using jig.WPF.SerilogViewer;
using MailKit;
using MailKit.Net.Imap;
using MimeKit;
using MimeKit.Text;
using static ImapMailManager.Config.RuntimeConfig_MailServer;


namespace ImapMailManager.Mail {

   public class ReadImapMailbox {

      #region Public Methods and Operators

      /// <summary>
      /// Liest das ganze IMAP Posfach
      /// und analysiert die Referenz-Nummern #xx-xxxx
      /// </summary>
      /// <param name="cfgImapServer"></param>
      /// <param name="cfgImapServerCred"></param>
      /// <param name="serilogViewer"></param>
      public static void AnalyzeMailboxMailRefNr(
         Sensitive_AppSettings.CfgImapServer                                 cfgImapServer
         , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred
         , bool                                        fetchHeaders
         , SerilogViewer                               serilogViewer) {
         /// Die Mailbox lesen
         var rootNode =
            ReadMailbox_CreateHierarchy
               (cfgImapServer
                , cfgImapServerCred
                , fetchHeaders
                , serilogViewer);

         //+ Die IMAP-Daten enumerieren
         // var childNodesBreadthFirst = node.DescendantNodes(TraversalType.BreadthFirst);
         // Console.WriteLine("We get all descendant nodes of this node (Breadth First)");
         // Console.WriteLine(childNodesBreadthFirst.PrintNodes());

         // var childNodesDepthFirst = node.DescendantNodes(TraversalType.DepthFirst);
         // Console.WriteLine("We get all descendant nodes of this node (Depth First)");
         // Console.WriteLine(childNodesDepthFirst.PrintNodes());		  

         //++ Die Formular-Ref.Nr suchen
         List<string> matches   = new();
         List<string> noMatches = new();

         //+ Herausfinden,
         // welche Refnr wir haben
         // und welche Betreff mit # nicht zur RefNr Regex passen

         // Jedes Element durchlaufen
         foreach (var item in rootNode.DescendantNodes(TraversalType.DepthFirst)) {
            var xxx = 1;

            if (item.Data.ImapPersonalNamespaceNde != null) {}

            if (item.Data.ImapFolderNde != null) {}

            if (item.Data.ImapMsgNde != null) {
               //++ Wir haben eine E-Mail Node
               var msg = item.Data.ImapMsgNde;

               // Hat der Betreff eine RefNr?
               var matches1 = BL_Mail_Common.oRgxRefNr.Match(msg.SubjectNormalized);

               if (matches1.Success) {
                  // Die RefNr in der macthes-Liste erfassen
                  var refNr = matches1.Groups["RefNr"].Value;
                  matches.Add(refNr);
               }
               else {
                  //++ Wir haben keine RefNr
                  // Die RefNr in der macthes-Liste erfassen
                  if (msg.Subject.ØHasValue()) {
                     // Der Betreff hat einen Wert
                     if (msg.Subject.Contains("#")) {
                        // Der Betref hat ein #
                        // Wenn die noMatches-Liste den Betreff noch nicht kennt
                        if (!noMatches.Contains(msg.Subject)) {
                           // Die noMatches-Liste ergänzen
                           Debug.WriteLine(msg.Subject);
                           noMatches.Add(msg.Subject);
                        }
                     }
                  }
               }

               var dsfdsfs = 1;
            }
         }

         foreach (var item in rootNode.DescendantNodes()) {
            var xxx = 1;
         }
      }


      /// <summary>
      /// Liest das ganze IMAP Posfach
      /// und analysiert die MessageID: Haben alle eine solche?
      /// </summary>
      /// <param name="cfgImapServer"></param>
      /// <param name="cfgImapServerCred"></param>
      /// <param name="serilogViewer"></param>
      public static void AnalyzeMailboxMessageID(
         Sensitive_AppSettings.CfgImapServer                                 cfgImapServer
         , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred
         , bool                                        fetchHeaders
         , SerilogViewer                               serilogViewer) {
         //+ Die Mailbox lesen
         var rootNode =
            ReadMailbox_CreateHierarchy
               (cfgImapServer
                , cfgImapServerCred
                , fetchHeaders
                , serilogViewer);

         //+ Die IMAP-Daten enumerieren
         // var childNodesBreadthFirst = node.DescendantNodes(TraversalType.BreadthFirst);
         // Console.WriteLine("We get all descendant nodes of this node (Breadth First)");
         // Console.WriteLine(childNodesBreadthFirst.PrintNodes());

         // var childNodesDepthFirst = node.DescendantNodes(TraversalType.DepthFirst);
         // Console.WriteLine("We get all descendant nodes of this node (Depth First)");
         // Console.WriteLine(childNodesDepthFirst.PrintNodes());		  

         //+ Die Formular-Ref.Nr suchen
         var allMsgs = rootNode.DescendantNodes(TraversalType.DepthFirst)
                               .Where(x => x.Data.ImapMsgNde != null)
                               .Select(x => x.Data.ImapMsgNde);

         Debug.WriteLine($"allMsgs.Count(): {allMsgs.Count()}");

         var allMsgsWithoutMsgID = allMsgs.Where(x => x.EnvelopeMessageId == null);
         Debug.WriteLine($"allMsgsWithoutMsgID.Count(): {allMsgsWithoutMsgID.Count()}");

         var ssadsadfds = 1;
      }


      public static void ParseHtml(string html) {
         var byteArray = Encoding.UTF8.GetBytes(html);

         using (var mStream = new MemoryStream(byteArray)) {
            using (var reader = new StreamReader(mStream)) {
               var       tokenizer = new HtmlTokenizer(reader);
               HtmlToken token;

               // ReadNextToken() returns `false` when the end of the stream is reached.
               while (tokenizer.ReadNextToken(out token)) {
                  switch (token.Kind) {
                     case HtmlTokenKind.ScriptData:
                     case HtmlTokenKind.CData:
                     case HtmlTokenKind.Data:
                        // ScriptData, CData, and Data tokens contain text data.
                        var text = (HtmlDataToken)token;

                        Debug.WriteLine
                           ("{0}: {1}"
                            , token.Kind
                            , text.Data);

                        break;

                     case HtmlTokenKind.Tag:
                        // Tag tokens represent tags and their attributes.
                        var tag = (HtmlTagToken)token;

                        // $"Subject: {message.Subject}"
                        Debug.Write(string.Format($"<{0}{tag.Name}", tag.IsEndTag ? "/" : ""));

                        foreach (var attribute in tag.Attributes) {
                           if (attribute.Value != null)
                              Debug.Write(" {attribute.Name}={Quote(attribute.Value)}");

                           // Debug.Write(" {0}={1}", attribute.Name, Quote(attribute.Value));
                           else
                              Debug.Write(" {attribute.Name}");
                        }

                        Debug.WriteLine(tag.IsEmptyElement ? "/>" : ">");

                        break;

                     case HtmlTokenKind.Comment:
                        var comment = (HtmlCommentToken)token;

                        Debug.WriteLine("Comment: {0}", comment.Comment);

                        break;

                     case HtmlTokenKind.DocType:
                        var doctype = (HtmlDocTypeToken)token;

                        if (doctype.ForceQuirksMode)
                           Debug.Write("<!-- force quirks mode -->");

                        Debug.Write("<!DOCTYPE");

                        if (doctype.Name != null)
                           Debug.Write(" {0}", doctype.Name.ToUpperInvariant());

                        if (doctype.PublicIdentifier != null) {
                           Debug.Write(" PUBLIC \"{0}\"", doctype.PublicIdentifier);

                           if (doctype.SystemIdentifier != null)
                              Debug.Write(" \"{0}\"", doctype.SystemIdentifier);
                        }
                        else if (doctype.SystemIdentifier != null) {
                           Debug.Write(" SYSTEM \"{0}\"", doctype.SystemIdentifier);
                        }

                        Debug.WriteLine(">");

                        break;
                  }
               }
            }
         }
      }


      /// <summary>
      ///  Liest die IMessageSummary eines Ordners
      /// </summary>
      /// <param name="cfgImapServer"></param>
      /// <param name="cfgImapServerCred"></param>
      /// <param name="imapFolder"></param>
      /// <returns>
      ///      var (imapClient, workingImapFolder, thisMsg, uniqueId) = … 
      /// </returns>
      public static Tuple<ImapClient, IMailFolder, MimeMessage, UniqueId> Fetch_Folder_MessageSummaries(
         Sensitive_AppSettings.CfgImapServer                                 cfgImapServer
         , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred
         , IMailFolder                                 imapFolder,
         string                                        envelopeMessageId) {
         //+ Verbinden
         ImapServerRuntimeConfig imapSrvRuntimeConfig = ImapServerRuntimeConfig.ConnectImap
            (cfgImapServerCred.IsProductiveMailbox ? "Prod" : "Test"
             , cfgImapServer
             , cfgImapServerCred);

         IMailFolder? workingImapFolder = imapSrvRuntimeConfig.ImapClient.GetFolder(imapFolder.FullName);
         workingImapFolder.Open(FolderAccess.ReadOnly);

         //++ Die messageSummaries lesen 
         var messageSummaries = workingImapFolder.ØFetch
            (
             0
             , -1
             , MessageSummaryItems.Envelope
               | MessageSummaryItems.BodyStructure
               | MessageSummaryItems.UniqueId
               | MessageSummaryItems.Size
               | MessageSummaryItems.Flags

             //+++ Wichtige Header auch lesen
             , ConfigMail_MsgHeader.Get_DefaultEMailHeader_Names());
   

         IMessageSummary? thisMessageSummaries = messageSummaries.FirstOrDefault(x => x.Envelope.MessageId == envelopeMessageId);

         MimeMessage thisMsg = workingImapFolder.GetMessage(thisMessageSummaries!.UniqueId);

         //+ Trennen
         // imapClient.Disconnect(true);

         return new Tuple<ImapClient, IMailFolder, MimeMessage, UniqueId>(imapSrvRuntimeConfig
                                                                            .ImapClient
                                                                          , workingImapFolder, thisMsg, thisMessageSummaries!
                                                                            .UniqueId);
         
      }


      /// <summary>
      /// Erzeugt einen Hierarchy Baum
      /// </summary>
      /// <param name="serilogViewer"></param>
      public static HierarchyNode<ImapHrchyNde>? ReadMailbox_CreateHierarchy(
         Sensitive_AppSettings.CfgImapServer                                 cfgImapServer
         , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred
         , bool                                        fetchHeaders
         , SerilogViewer?                              serilogViewer) {
         
         // Die Liste mit allen IMAP Postfach Elementen
         var hrchyImapMbox = new List<HierarchyNode<ImapHrchyNde>?>();

         //+ Verbinden
         ImapServerRuntimeConfig imapSrvRuntimeConfig = ImapServerRuntimeConfig.ConnectImap
            (cfgImapServerCred.IsProductiveMailbox ? "Prod" : "Test"
             , cfgImapServer
             , cfgImapServerCred);

         //+ Alle PersonalNamespaces durchlaufen
         foreach (var folderNamespace in imapSrvRuntimeConfig.ImapClient.PersonalNamespaces) {
            var thisFolder        = imapSrvRuntimeConfig.ImapClient.GetFolder(folderNamespace);
            var thisNamespacePath = Path.Join(folderNamespace.Path, imapSrvRuntimeConfig.IMapDirSeperator);
            Debug.WriteLine($"PersonalNamespace:  {thisNamespacePath}");

            // Personal Namespace Node erzeugen
            var nodeThisPersonalNmspcHrchy = new HierarchyNode<ImapHrchyNde> {
               Data = new ImapHrchyNde
                  (new ImapPersonalNamespaceNde
                      (thisFolder
                       , thisNamespacePath
                       , 0
                       , 0
                       , 0))
            };
            hrchyImapMbox.Add(nodeThisPersonalNmspcHrchy);

            /// Alle Ordner im NameSpace verarbeiten
            foreach (var thisEnumFolder in EnumFolder(thisFolder)) {
               var thisFolderPath = Path.Join(thisEnumFolder.FullName, imapSrvRuntimeConfig
                                                                      .IMapDirSeperator);
               Debug.WriteLine($"Lese: {thisFolderPath}");

               // ‼ ToDo 🚩 Nur ein Test, für prod entfernen
               BL_Mail_Common.Calc_MbxOrdner_ArchivPath(thisEnumFolder);
               BL_Mail_Common.Analyze_TriageHashTags(thisEnumFolder);

               // Nur IMAP-ordner, die existieren und die ausgewählt werden können
               if (!thisEnumFolder.Attributes.HasFlag(FolderAttributes.NonExistent)
                   && !thisEnumFolder.Attributes.HasFlag(FolderAttributes.NoSelect)) {
                  // Vom IMAP Ordner die statistischen infos erfassen
                  thisEnumFolder.Status(StatusItems.Unread | StatusItems.Count | StatusItems.Size);

                  var thisSize        = thisEnumFolder.Size ?? 0;
                  var thisCount       = thisEnumFolder.Count;
                  var thisUnreadCount = thisEnumFolder.Unread;

                  // Den IMAP Ordner der Hierarchy als Node zufügen
                  var newFolderNde = nodeThisPersonalNmspcHrchy.ØAddChild
                     (
                      new HierarchyNode<ImapHrchyNde> {
                         Data = new ImapHrchyNde
                            (
                             new ImapFolderNde
                                (
                                 thisEnumFolder
                                 , thisFolderPath
                                 , thisCount
                                 , thisUnreadCount
                                 , thisSize
                                 , DateTime.MinValue
                                 , DateTime.MinValue))
                      });

                  //+ Alle Nachrichten der Node zufügen
                  var anz = ReadMessages
                     (newFolderNde
                      , thisEnumFolder
                      , fetchHeaders);
               }
            }

            // Klappt:
            // Debug.WriteLine(hrchyImapMbox.PrintTree());

            // serilogViewer.LogInformation($"thisPath: {thisFolderPath} • folder.FullName: {folder.FullName}");
         }

         //+ Trennen
         imapSrvRuntimeConfig.ImapClient.Disconnect(true);

         //ToDo Die Statistik der übergeordneten Ordner aufgrund der Unterelemente berechnen?

         // Nur die Root-Node zurückgeben
         return hrchyImapMbox.First();
      }


      public static void TestIt(
         Sensitive_AppSettings.CfgImapServer                                 cfgImapServer
         , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred
         , SerilogViewer                               serilogViewer) {
         // Die Ordner des Postfachs mit Zusatzinfos
         var allFolderData = new Dictionary<string, ImapFolderNde>();

         ImapServerRuntimeConfig imapSrvRuntimeConfig = ImapServerRuntimeConfig.ConnectImap
            (cfgImapServerCred.IsProductiveMailbox ? "Prod" : "Test"
             , cfgImapServer
             , cfgImapServerCred);


         if (imapSrvRuntimeConfig.ImapClient.Capabilities.HasFlag(ImapCapabilities.SpecialUse)) {
            // http://www.mimekit.net/docs/html/T_MailKit_FolderAttributes.htm
            // http://www.mimekit.net/docs/html/T_MailKit_SpecialFolder.htm
            var folder1 = imapSrvRuntimeConfig.ImapClient.GetFolder(SpecialFolder.All);
            var folder2 = imapSrvRuntimeConfig.ImapClient.GetFolder(SpecialFolder.Archive);
            var folder3 = imapSrvRuntimeConfig.ImapClient.GetFolder(SpecialFolder.Drafts);
            var folder4 = imapSrvRuntimeConfig.ImapClient.GetFolder(SpecialFolder.Flagged);
            var folder5 = imapSrvRuntimeConfig.ImapClient.GetFolder(SpecialFolder.Important);
            var folder6 = imapSrvRuntimeConfig.ImapClient.GetFolder(SpecialFolder.Junk);
            var folder7 = imapSrvRuntimeConfig.ImapClient.GetFolder(SpecialFolder.Sent);
            var folder8 = imapSrvRuntimeConfig.ImapClient.GetFolder(SpecialFolder.Trash);
            var sdfs    = 1;
         }

         var source            = new CancellationTokenSource();
         var cancellationToken = source.Token;

         var root      = imapSrvRuntimeConfig.ImapClient.GetFolder(imapSrvRuntimeConfig.ImapClient.PersonalNamespaces[0]);
         var rootQuota = root.GetQuota();

         Debug.WriteLine("PersonalNamespaces und SubFolders:");
         Debug.WriteLine($"  Anzahl: {imapSrvRuntimeConfig.ImapClient.PersonalNamespaces.Count}");

         foreach (var folderNamespace in imapSrvRuntimeConfig.ImapClient.PersonalNamespaces) {
            var thisFolder = imapSrvRuntimeConfig.ImapClient.GetFolder(folderNamespace);
            var thisPath   = Path.Join(folderNamespace.Path, imapSrvRuntimeConfig.IMapDirSeperator);
            Debug.WriteLine($"PersonalNamespace:  {thisPath}");

            foreach (var thisEnumFolder in EnumFolder(thisFolder)) {
               thisPath = Path.Join(thisEnumFolder.FullName, imapSrvRuntimeConfig.IMapDirSeperator);

               if (!thisEnumFolder.Attributes.HasFlag(FolderAttributes.NonExistent)
                   && !thisEnumFolder.Attributes.HasFlag(FolderAttributes.NoSelect)) {
                  if (!thisEnumFolder.IsOpen) {
                     // thisEnumFolder.Open(FolderAccess.ReadOnly);
                  }

                  thisEnumFolder.Status(StatusItems.Unread | StatusItems.Count | StatusItems.Size);
                  var thisSize        = (ulong)thisEnumFolder.Size;
                  var thisCount       = thisEnumFolder.Count;
                  var thisUnreadCount = thisEnumFolder.Unread;

                  allFolderData.Add
                     (
                      thisPath
                      , new ImapFolderNde
                         (
                          thisEnumFolder
                          , thisPath
                          , thisCount
                          , thisUnreadCount
                          , thisSize
                          , DateTime.MinValue
                          , DateTime.MinValue));

                  Debug.WriteLine
                     ($"    {thisPath} ({thisUnreadCount}/{thisCount}), {thisSize.ØToUnitStr(0, "")}");
               }
            }

            // serilogViewer.LogInformation($"thisPath: {thisPath} • folder.FullName: {folder.FullName}");
         }

         /// Server-Props abrufen
         // GetImapCapabilities(client, serilogViewer);

         Debug.WriteLine("PersonalNamespaces und SubFolders:");

         foreach (var ns in imapSrvRuntimeConfig.ImapClient.PersonalNamespaces) {
            var thisFolder = imapSrvRuntimeConfig.ImapClient.GetFolder(ns);
            var thisPath   = Path.Join(ns.Path, imapSrvRuntimeConfig.IMapDirSeperator);
            Debug.WriteLine($"  {thisPath}");

            // foreach (IMailFolder thisEnumFolder in EnumFolder(thisImapFolder)) {
            foreach (var thisEnumFolder in thisFolder.GetSubfolders()) {
               var fullName = thisEnumFolder.FullName;
               Debug.WriteLine($"    {fullName}");

               var aa = 1;
            }

            // serilogViewer.LogInformation($"thisPath: {thisPath} • folder.FullName: {folder.FullName}");
            var d = 1;
         }

         // Alle Ordner durchlaufen
         foreach (var ns in imapSrvRuntimeConfig.ImapClient.PersonalNamespaces) {
            var folder   = imapSrvRuntimeConfig.ImapClient.GetFolder(ns);
            var thisPath = Path.Join(ns.Path, imapSrvRuntimeConfig.IMapDirSeperator);

            serilogViewer.LogInformation
               ($"thisPath: {thisPath} • folder.FullName: {folder.FullName}");

            foreach (var subfolder in folder.GetSubfolders()) {
               serilogViewer.LogInformation($"subfolder.FullName: {subfolder.FullName}");
            }
         }

         // var folders = (await imapClient.GetFoldersAsync (imapClient.PersonalNamespaces[0], all, true)).ToList ();

         // var folders = await imapClient.GetFolderAsync(new FolderNamespace('.', ""));

         // The Inbox folder is always available on all IMAP servers...
         var inbox = imapSrvRuntimeConfig.ImapClient.Inbox;
         inbox.Open(FolderAccess.ReadOnly);

         // Anstatt Debug.WriteLine():
         Debug.WriteLine($"Total messages: {inbox.Count}");
         Debug.WriteLine($"Recent messages: {inbox.Recent}");
         serilogViewer.LogInformation($"Total messages: {inbox.Count}");
         serilogViewer.LogInformation($"Recent messages: {inbox.Recent}");

         for (var i = 0; i < inbox.Count; i++) {
            var message = inbox.GetMessage(i);
            Debug.WriteLine($"Subject: {message.Subject}");
            serilogViewer.LogInformation($"Subject: {message.Subject}");

            var htmlBody = message.HtmlBody;
            ParseHtml(htmlBody);
         }

         imapSrvRuntimeConfig.ImapClient.Disconnect(true);
      }

      #endregion

      #region Internal Methods

      /// <summary>
      /// Enumeriert einen IMAP Ordner
      /// und gibt jeden Ordner einzeln zurück
      /// </summary>
      /// <param name="thisLevel"></param>
      /// <returns></returns>
      public static IEnumerable<IMailFolder> EnumFolder(IMailFolder thisLevel) {
         // Subfolder sortieren
         IList<IMailFolder>? subfolders = null;
         try {
            subfolders = thisLevel.GetSubfolders();
         } catch {
            // 
         }

         // Abbruch, wenn nichts gefunden
         if (subfolders == null) { yield break; }
         
         var subfoldersSorted = new List<IMailFolder>(subfolders);
         subfoldersSorted.Sort(new FolderComparer());

         foreach (var subfolder in subfoldersSorted) {
            yield return subfolder;

            foreach (var mailFolder in EnumFolder(subfolder)) { yield return mailFolder; }
         }
      }


      /// <summary>
      /// Sucht in der Hierarchie einen IMAP-Ordner
      /// </summary>
      /// <param name="toplevel"></param>
      /// <param name="name"></param>
      /// <returns></returns>
      private static IMailFolder FindFolder(IMailFolder toplevel, string name) {
         var subfolders = toplevel.GetSubfolders().ToList();

         foreach (var subfolder in subfolders) {
            if (subfolder.Name == name)
               return subfolder;
         }

         foreach (var subfolder in subfolders) {
            var folder = FindFolder(subfolder, name);

            if (folder != null)
               return folder;
         }

         return null;
      }


      private static string Quote(string text) {
         if (text == null)
            throw new ArgumentNullException(nameof(text));

         var quoted = new StringBuilder(text.Length + 2, text.Length*2 + 2);

         quoted.Append("\"");

         for (var i = 0; i < text.Length; i++) {
            if (text[i] == '\\'
                || text[i] == '"')
               quoted.Append('\\');
            else if (text[i] == '\r')
               continue;
            quoted.Append(text[i]);
         }

         quoted.Append("\"");

         return quoted.ToString();
      }


      /// <summary>
      /// Liest alle E-Mails eines IMAP Ordners
      /// und ergänzt die in der Hierarchy
      /// </summary>
      /// <param name="imapClient"></param>
      /// <param name="hierarchyNodeImapFolder"></param>
      /// <param name="thisImapFolder"></param>
      /// <param name="fetchAllHeaders">
      /// True: Liest alle Header, langsam
      /// False: Liest nur ausgewählte Header, see headerProps
      /// </param>
      /// <returns></returns>
      private static int ReadMessages(
         IHierarchyNode<ImapHrchyNde> hierarchyNodeImapFolder
         , IMailFolder                thisImapFolder
         , bool                       fetchAllHeaders) {
         // Ordner öffnen
         if (!thisImapFolder.IsOpen) { thisImapFolder.Open(FolderAccess.ReadOnly); }

         // Alle Nachrichten lesen
         var MsgCnt = 0;

         // Wichtige Header auch lesen
         foreach (var summary in thisImapFolder.ØFetch
                     (
                      0
                      , -1
                      , MessageSummaryItems.Envelope
                        | MessageSummaryItems.UniqueId
                        | MessageSummaryItems.Size
                        | MessageSummaryItems.Flags
                      , ConfigMail_MsgHeader.Get_DefaultEMailHeader_Names())) {
            MsgCnt++;

            //+ Allenfalls ein Status-Update anzeigen
            if (fetchAllHeaders && MsgCnt%10 == 0) { Debug.WriteLine($"Lese Mails: {MsgCnt,4}"); }

            //+ Die uniqueIdholen 
            var uniqueId = summary.UniqueId;

            // Console.WriteLine("[summary] {0:D2}: {1}", summary.Index, summary.Envelope.Subject);
            var date = summary.Date;

            //+ Alle Header lesen?
            HeaderList? mailHeaders = null;

            //+ Daten zusammenstellen
            if (fetchAllHeaders) { mailHeaders = thisImapFolder.GetHeaders(uniqueId); }
            else { mailHeaders                 = summary.Headers; }

            var envelopeMessageId = summary.Envelope.MessageId.ØHasValue()
                                       ? summary.Envelope.MessageId
                                       : null;

            var sender            = summary.Envelope.Sender;
            var from              = summary.Envelope.From;
            var to                = summary.Envelope.To;
            var subject           = summary.Envelope.Subject;
            var subjectNormalized = summary.Envelope.Subject.ØCleanEmailSubjectPrefix();
            var size              = summary.Size;

            //+ Node erzeugen und der Hierarchy zufügen
            var newMsgNode = hierarchyNodeImapFolder.ØAddChild
               (
                new HierarchyNode<ImapHrchyNde> {
                   Data = new ImapHrchyNde
                      (
                       new ImapMsgNde
                          (
                           thisImapFolder
                           , envelopeMessageId
                           , date
                           , from
                           , to
                           , subject
                           , subjectNormalized
                           , size
                           , mailHeaders))
                });
         }

         //+ Die älteste / Neuste E-Mail suchen
         var ältesteEMail = hierarchyNodeImapFolder.Children.Min(x => x.Data.ImapMsgNde?.ADate);
         var neusteEMail  = hierarchyNodeImapFolder.Children.Max(x => x.Data.ImapMsgNde?.ADate);

         //++ Stat aktualisieren
         hierarchyNodeImapFolder.Data.ImapFolderNde.ÄltesteMail = ältesteEMail?.LocalDateTime;
         hierarchyNodeImapFolder.Data.ImapFolderNde.NeusteMail  = neusteEMail?.LocalDateTime;

         return MsgCnt;
      }

      #endregion

   }

}
