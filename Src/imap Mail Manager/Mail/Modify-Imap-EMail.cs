﻿using System;
using ImapMailManager.Mail.MimeKit;
using MailKit;
using MailKit.Net.Imap;
using MimeKit;


namespace ImapMailManager.Mail;

public class ModifyImapEMail {

   #region Public Methods and Operators

   /// <summary>
   /// Speichert eine E-Mail
   /// </summary>
   /// <param name="imapClient"></param>
   /// <param name="mailFolder"></param>
   /// <param name="uniqueId"></param>
   /// <param name="message"></param>
   /// <param name="markAsRead">
   /// Wenn true, dann wird die neue Message als Read markiert 
   /// </param>
   /// <returns>
   /// newUid?, die ID der neuen Message
   /// </returns>
   public static UniqueId? SaveMessage(
      ImapClient    imapClient
      , IMailFolder mailFolder
      , UniqueId    uniqueId
      , MimeMessage message
      , bool        markAsRead) {
      //+ Die Mailbox zum Schreiben vorbereiten 
      mailFolder.Open(FolderAccess.ReadWrite);

      //+ Die Msg dem Ordner zufügen 
      UniqueId? newUid = mailFolder.Append(message);

      //+ Das Read-Flag setzen?
      if (markAsRead) {
         //+ Wenn wir die neue UID haben
         if (newUid.HasValue) {
            mailFolder.AddFlags
               (newUid.Value
                , MessageFlags.Seen
                , true);
         }
      }

      //+ Im Ordner die alte Version als gelöscht markieren
      mailFolder.AddFlags
         (uniqueId
          , MessageFlags.Deleted
          , true);

      // Wenn möglich, die alte Message endgültig aus dem Ordner löschen
      if (imapClient.Capabilities.HasFlag
             (ImapCapabilities.UidPlus)) { mailFolder.Expunge(new[] { uniqueId }); }

      return newUid;
   }


   /// <summary>
   /// Speichert eine E-Mail und kopiert auf Wunsch das Read-Flag vom
   /// Original zur neuen Nachricht
   /// </summary>
   /// <param name="imapClient"></param>
   /// <param name="msgSummary"></param>
   /// <param name="mailFolder"></param>
   /// <param name="uniqueId"></param>
   /// <param name="message"></param>
   /// <param name="copyMarkReadFlag">
   /// Wenn True, dann wird das Read-Flag von der Original-Msg
   /// in die neue kopiert
   /// </param>
   /// <returns></returns>
   [Obsolete("Diese Funktion ist obsolete » BO_Mail.Srv_Read_MsgSummaryData()", false)]
   public static UniqueId? SaveMessage(
      ImapClient        imapClient
      , IMessageSummary msgSummary
      , IMailFolder     mailFolder
      , MimeMessage     message
      , bool            copyMarkReadFlag) {
      //+ Die Mailbox zum Schreiben vorbereiten 
      mailFolder.Open(FolderAccess.ReadWrite);

      //+ Die Msg dem Ordner zufügen 
      UniqueId? newUid = mailFolder.Append(message);

      //+ Das Read-Flag kopieren?
      if (newUid.HasValue && copyMarkReadFlag && MimeKit_JigTools.Is_Msg_Marked_AsRead(msgSummary)) {
         mailFolder.AddFlags
            (newUid.Value
             , MessageFlags.Seen
             , true);
      }

      //+ Im Ordner die alte Nachricht als gelöscht markieren
      mailFolder.AddFlags
         (msgSummary.UniqueId
          , MessageFlags.Deleted
          , true);

      // Wenn möglich, die alte Message endgültig aus dem Ordner löschen
      if (imapClient.Capabilities.HasFlag
             (ImapCapabilities.UidPlus)) { mailFolder.Expunge(new[] { msgSummary.UniqueId }); }

      return newUid;
   }

   #endregion

}
