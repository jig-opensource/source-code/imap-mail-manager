using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using ImapMailManager.Mail.Parser;
using jig.Tools;
using jig.Tools.String;


namespace ImapMailManager.Mail;

/// <summary>
/// Handelt ein Stirng-Array im Msg header
/// </summary>
public class BL_Mail_MsgHeader_StringArray {

   //+ Config

   #region Config

   // private const char ListDelimiter = '¦';
   // private const char ListDelimiter = '༶';
   // private const char ListDelimiter = '༝';
   // private const char ListDelimiter = '᎒';
   // private const char ListDelimiter = '⌇';
   // private const char ListDelimiter = '⌶';
   // private const char ListDelimiter = '⍚';
   private const char ListDelimiter = '╳';

   #endregion Config

   #region Props

   /// <summary>
   /// True, wenn die Daten-Liste verändert wurde
   /// </summary>
   public bool IsDirty { get; internal set; } = false;

   private List<string> _daten = new();

   /// <summary>
   /// Die Daten in der Liste
   /// </summary>
   public ImmutableList<string> Daten {
      get => _daten.ToImmutableList();
   }

   #endregion Props

   #region Konstruktor

   /// <summary>
   /// Instanziiert das Datenarray
   /// </summary>
   /// <param name="delimitedDaten"></param>
   public BL_Mail_MsgHeader_StringArray(string delimitedDaten) => DatenSplit(delimitedDaten);

   #endregion Konstruktor

   /// <summary>
   /// Liefert die string Liste als delmited String
   /// </summary>
   public string DatenDelimted => string.Join
      (ListDelimiter, _daten.ToList());


   /// <summary>
   /// Splittet den die delimited String in eine string Liste 
   /// </summary>
   /// <param name="delimitedDaten"></param>
   /// <returns></returns>
   private List<string> DatenSplit(string? delimitedDaten) {
      if (!delimitedDaten.ØHasValue()) { return new List<string>(); }
      _daten = delimitedDaten!.Split(ListDelimiter, StringSplitOptions.RemoveEmptyEntries).ToList();
      return _daten;
   }


   /// <summary>
   /// Ergänzt die Stirng-Liste um ein Element, wenn es noch nicht in der Liste ist
   /// </summary>
   /// <param name="str"></param>
   /// <returns>
   /// True: Die Liste wurde verändert
   /// </returns>
   public bool AddString(string str) {
      var hasChanged = _daten.ØAppendItem_IfMissing(str);
      hasChanged.IfTrue(() => IsDirty = true);
      return hasChanged;
   }

}
