using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using jig.Tools;
using jig.Tools.String;


namespace ImapMailManager.Mail;

/// <summary>
/// Business-Logik rund um die Mail HashTags
/// </summary>
public class BL_Mail_HashTag {

   #region Rgx Single HashTag

   public static readonly RegexOptions RgxOptionsHashTag = RegexOptions.IgnoreCase
                                                           | RegexOptions.Multiline
                                                           | RegexOptions.ExplicitCapture
                                                           | RegexOptions.CultureInvariant
                                                           | RegexOptions.Compiled;

   /// <summary>
   /// Gültiges erstes Zeichen einer Hashtag-Bezeichnung
   /// </summary>

   // public static readonly string SRgxHashTagStartChars = "a-zA-Z0-9$";
   public static readonly string SRgx_HashTag_StartChars = @"\p{L}0-9$";
   /// <summary>
   /// Gültiges Folgezeichen eines Hashtag-Bezeichnung
   /// </summary>
   public static readonly string SRgx_HashTag_FollowUpChars = $@"{SRgx_HashTag_StartChars}_\-.()\[\]";

   // private static readonly string SRgxHashTag = @"(?<HashTag>\#[a-zA-Z0-9$][a-zA-Z0-9_\-.()\[\]]*)";
   public static readonly string SRgx_HashTag
      = $@"(?<HashTag>\#[{SRgx_HashTag_StartChars}][{SRgx_HashTag_FollowUpChars}]*)";
   public static readonly Regex ORgx_HashTag = new Regex(SRgx_HashTag, RgxOptionsHashTag);

   #endregion Rgx Single HashTag

   #region Rgx HashTag in Text Mail Body

   public static readonly RegexOptions RgxOptions_HashTagBlock_InTextMailBody = RegexOptions.IgnoreCase
                                                                                | RegexOptions.Multiline
                                                                                | RegexOptions.ExplicitCapture
                                                                                | RegexOptions.CultureInvariant
                                                                                | RegexOptions.IgnorePatternWhitespace
                                                                                | RegexOptions.Compiled;

   public static readonly string SRgxOptions_HashTagBlock_InTextMailBody = @"(?<TriageHashTags>
                                                                             (?<Header>
                                                                               Triage-HashTags:\s*
                                                                             )
                                                                             (?<SingleHashTag>
                                                                               \#[\p{L}0-9$][\p{L}0-9$_\-.()\[\]]*\s+
                                                                             )*
                                                                           )";
   public static readonly Regex ORgxOptions_HashTagBlock_InTextMailBody = new Regex
      (SRgxOptions_HashTagBlock_InTextMailBody, RgxOptions_HashTagBlock_InTextMailBody);

   #endregion Rgx HashTag in Text Mail Body


   // Liefert True, wenn text ein HashTag ist
   public static bool IsValidHashtag(string text) {
      if (text.ØHasValue()) {
         //+ Bereinigen
         text = text.Trim();

         //+ Testen
         return Regex.Match
            (text
             , $"^{SRgx_HashTag}$"
             , RgxOptionsHashTag).Success;
      }
      else {
         return false;
      }
   }


   /// <summary>
   /// Sucht in text alle HashTags und liefert die Liste zurück
   ///
   /// !! Entfernt alle HashTags, die einer Mail Ref Nr #xx-xxxx entprechen
   /// 
   /// </summary>
   /// <param name="text"></param>
   /// <returns></returns>
   public static List<string> FindHashtags_InText(string? text) {
      return FilterHashTag_RemoveRefNr
         (BL_Mail_HashTag.ORgx_HashTag.ØGetNamedMatches(text, "HashTag"));
   }


   /// <summary>
   /// Filtert die HashTag Liste und entfernt allenfalls EMail RefNr Elemente
   /// </summary>
   /// <param name="hashTags"></param>
   /// <returns></returns>
   public static List<string> FilterHashTag_RemoveRefNr(List<string> hashTags) {
      return hashTags.Where(x => !BL_Mail_Common.Contains_Mail_RefNr(x).Item1).ToList();
   }

}
