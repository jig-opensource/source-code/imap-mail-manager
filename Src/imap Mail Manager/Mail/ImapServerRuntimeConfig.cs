using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using ImapMailManager.Config;
using ImapMailManager.Properties;
using jig.Tools;
using jig.Tools.String;
using jig.WPF.SerilogViewer;
using MailKit;
using MailKit.Net.Imap;


namespace ImapMailManager.Mail;

/// <summary>
/// Objekt mit der Runtime-Konfig für einen Server
/// </summary>
public class ImapServerRuntimeConfig {

   /// <summary>
   /// Liste der Server, zu denen wir die Verbindiung kennen
   /// </summary>
   private static Dictionary<(string serverHostname, string userName), ImapServerRuntimeConfig>
      _serverConnections
         = new();

   #region Props

   /// <summary>
   /// Das Alias in der Conifg-Datei
   /// </summary>
   public string Alias { get; private set; }

   /// <summary>
   /// Das Objekt mit dem IMAP-Client
   /// !M http://www.mimekit.net/docs/html/T_MailKit_Net_Imap_ImapClient.htm
   /// </summary>
   public ImapClient ImapClient { get; private set; }

   /// <summary>
   /// Das IMAP Ordner-Pfad Trennzeichen
   /// </summary>
   public string IMapDirSeperator = null;

   public           Sensitive_AppSettings.CfgImapServer     Cfg_ImapServer     { get; }
   public           Sensitive_AppSettings.CfgImapServerCred Cfg_ImapServerCred { get; }

   private readonly Lazy<Dictionary<SpecialFolder, IMailFolder>> _specialFolders;
   
   /// <summary>
   /// Liste aller festen, englischsprschigen Special folders
   /// 
   /// ‼ Es gibt noch die regionalen Spezialfülder,
   /// 
   /// Dieseüssen mit dem mbxFolder.Attributes geprüft werden, siehe:
   ///   var isFldr_Archive   = mbxFolder.Attributes.HasFlag(FolderAttributes.Archive);
   /// 
   /// </summary>
   public Dictionary<SpecialFolder, IMailFolder> SpecialFolders => _specialFolders.Value;

   
   /// <summary>
   /// Liefert den TopLevel-Order der Mailbox
   /// </summary>
   public IMailFolder RootFolder {
      get { return ImapClient.GetFolder(ImapClient.PersonalNamespaces[0]); }
   }


   /// <summary>
   /// Liest vom Server alle SpecialFolders aus
   /// </summary>
   /// <returns></returns>
   private Dictionary<SpecialFolder, IMailFolder> GetSpecialFolders() {
      Dictionary<SpecialFolder, IMailFolder> specialFolders = new();

      foreach (var specialFolder in Enum.GetValues<SpecialFolder>()) {
         // Ordner All = 0 überspringen
         if ((int)specialFolder > 0) {
            specialFolders.Add(specialFolder, ImapClient.GetFolder(specialFolder));
         }
      }

      return specialFolders;
   }

   #endregion Props

   #region Berechnete Props


   /// <summary>
   /// Die Mailbox ist produktiv, keine Testumgebung!
   /// </summary>
   public bool MailboxIsProductive => Cfg_ImapServerCred.IsProductiveMailbox;

   /// <summary>
   /// Die Mailbox ist nur für Tests, nicht produktiv!
   /// </summary>
   public bool MailboxIsTest => Cfg_ImapServerCred.IsTestMailbox;

   /// <summary>
   /// Die Mailbox ist eine Archiv-Mailbox 
   /// </summary>
   public bool IsArchiveMailbox => Cfg_ImapServerCred.IsMailboxTypeArchive;

   /// <summary>
   /// Liefert True, wenn der Server vebrunden ist
   /// </summary>
   public bool IsConnected => ImapClient.IsConnected;

   #endregion Berechnete Props

   #region Constructors

   /// <summary>
   /// Default constructor
   /// </summary>
   private ImapServerRuntimeConfig() {
      _specialFolders = new(GetSpecialFolders, true);
   }
   
   /// <summary>
   /// Konstruktor
   /// </summary>
   /// <param name="cfgImapServer"></param>
   /// <param name="cfgImapServerCred"></param>
   /// <param name="alias"></param>
   public ImapServerRuntimeConfig(
      string                                    alias
      , Sensitive_AppSettings.CfgImapServer     cfgImapServer
      , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred
   ) : this() {
      Cfg_ImapServer     = cfgImapServer;
      Cfg_ImapServerCred = cfgImapServerCred;
      Alias               = alias;

      // Den Imap Client erzeugen

      // 🚩 Debug:
      var logFile = @"c:\Temp\ImapClient-Log.txt";

      if (File.Exists(logFile)) { File.Delete(logFile); }

      // ToDo 🟥 Konfigurierbar machen
      ImapClient = new ImapClient(new ProtocolLogger(logFile));

      // ImapClient = new ImapClient();

      // Console.WriteLine("Connect");
      Debug.WriteLine("Connect");

      //+ Verbinden
      ImapClient.Connect
         (Cfg_ImapServer.Hostname
          , Cfg_ImapServer.Port
          , Cfg_ImapServer.UseSsl);
      ImapClient.Authenticate(Cfg_ImapServerCred.UserName, Cfg_ImapServerCred.Pw);

      //+ Den Pfad-Seperator merken
      IMapDirSeperator = ImapClient.Inbox.DirectorySeparator.ToString();
   }

   #endregion Constructors


   /// <summary>
   /// Verbindet zum Server
   /// </summary>
   /// <returns></returns>
   public bool Connect() {
      if (IsConnected) { return true; }

      ImapClient.Connect
         (Cfg_ImapServer.Hostname
          , Cfg_ImapServer.Port
          , Cfg_ImapServer.UseSsl);
      ImapClient.Authenticate(Cfg_ImapServerCred.UserName, Cfg_ImapServerCred.Pw);

      //+ Den Pfad-Seperator merken
      IMapDirSeperator = ImapClient.Inbox.DirectorySeparator.ToString();

      return ImapClient.IsConnected;
   }


   /// <summary>
   /// Verbindet zum IMAP Server
   /// </summary>
   /// <returns></returns>
   public static ImapServerRuntimeConfig ConnectImap(
      string                                    alias
      , Sensitive_AppSettings.CfgImapServer     cfgImapServer
      , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred) {
      //+ Haben wir den Server bereits?
      KeyValuePair<(string serverHostname, string userName), ImapServerRuntimeConfig>?
         foundServerConnection = null;

      foreach (KeyValuePair<(string serverHostname, string userName), ImapServerRuntimeConfig>
                  thiServerConn in _serverConnections) {
         if (thiServerConn.Key.serverHostname.ØEqualsIgnoreCase(cfgImapServer.Hostname)
             && thiServerConn.Key.userName.ØEqualsIgnoreCase(cfgImapServerCred.UserName)) {
            foundServerConnection = thiServerConn;

            break;
         }
      }

      //+ Den Server kennen wir schon
      if (foundServerConnection != null) {
         foundServerConnection.Value.Value.Connect();

         return foundServerConnection.Value.Value;
      }

      //+ Server erzeugen
      var server = new ImapServerRuntimeConfig
         (alias
          , cfgImapServer
          , cfgImapServerCred);

      //+ Server Config merken
      _serverConnections.Add((cfgImapServer.Hostname, cfgImapServerCred.UserName), server);
      server.Connect();

      return server;
   }


   /// <summary>
   /// Liest die IMAP Server Props aus
   /// </summary>
   /// <param name="imapClient"></param>
   /// <param name="serilogViewer"></param>
   /// <param name="imapClient"></param>
   public void GetImapCapabilities(SerilogViewer serilogViewer) {
      serilogViewer.LogInformation("The IMAP server supports:");

      var root         = ImapClient.GetFolder(ImapClient.PersonalNamespaces[0]);
      var rootQuota    = root.GetQuota();
      var DirSeperator = root.DirectorySeparator;

      var mechanisms = string.Join(", ", ImapClient.AuthenticationMechanisms);

      if (mechanisms.ØHasValue()) {
         serilogViewer.LogInformation($"✅ SASL authentication mechanisms: {mechanisms}");
      }
      else { serilogViewer.LogInformation("\u274c SASL authentication mechanisms"); }

      if (ImapClient.Capabilities.HasFlag(ImapCapabilities.Id)) {
         var clientImplementation = new ImapImplementation {
            Name      = "MailKit"
            , Version = "1.0"
         };
         var serverImplementation = ImapClient.Identify(clientImplementation);

         serilogViewer.LogInformation("- Server implementation details:");

         foreach (var property in serverImplementation.Properties)
            serilogViewer.LogInformation($"  {property.Key}: {property.Value}");
      }

      if (ImapClient.Capabilities.HasFlag(ImapCapabilities.Acl)) {
         serilogViewer.LogInformation("\u2705 Access Control Lists");

         serilogViewer.LogInformation($"✅ These access rights: {ImapClient.Rights}");

         serilogViewer.LogInformation("\u2705 The Inbox has the following access controls:");
         var acl = ImapClient.Inbox.GetAccessControlList();

         foreach (var ac in acl)
            serilogViewer.LogInformation($"  {ac.Name} = {ac.Rights}");

         var myRights = ImapClient.Inbox.GetMyAccessRights();
         serilogViewer.LogInformation($"✅ The current rights for the Inbox folder: {myRights}");
      }
      else { serilogViewer.LogInformation("\u274c Access Control Lists"); }

      if (ImapClient.Capabilities.HasFlag(ImapCapabilities.Quota)) {
         serilogViewer.LogInformation("\u2705 Quotas (Speicherbegrenzung)");

         serilogViewer.LogInformation("  - The current quota for the Inbox:");
         var quota = ImapClient.Inbox.GetQuota();

         if (quota.StorageLimit.HasValue) {
            serilogViewer.LogInformation
               (
                $"    Limited storage space: {quota.CurrentStorageSize.Value} / {quota.StorageLimit.Value} bytes");
         }
         else { serilogViewer.LogInformation("\u274c Limited storage space"); }

         if (quota.MessageLimit.HasValue) {
            serilogViewer.LogInformation
               (
                $"    Limited number of messages: {quota.CurrentMessageCount.Value} / {quota.MessageLimit.Value} bytes");
         }
         else { serilogViewer.LogInformation("\u274c Limited number of messages"); }

         if (quota.QuotaRoot != null) {
            serilogViewer.LogInformation($"✅ Quota root: {quota.QuotaRoot}");
         }
         else { serilogViewer.LogInformation("\u274c Quota root"); }
      }
      else { serilogViewer.LogInformation("\u274c Quotas (Speicherbegrenzung)"); }

      if (ImapClient.Capabilities.HasFlag(ImapCapabilities.Thread)) {
         if (ImapClient.ThreadingAlgorithms.Contains(ThreadingAlgorithm.OrderedSubject)) {
            serilogViewer.LogInformation("\u2705 Threading by subject");
         }
         else { serilogViewer.LogInformation("\u274c Threading by subject"); }

         if (ImapClient.ThreadingAlgorithms.Contains(ThreadingAlgorithm.References)) {
            serilogViewer.LogInformation("\u2705 Threading by references");
         }
         else { serilogViewer.LogInformation("\u274c Threading by references"); }
      }
      else { serilogViewer.LogInformation("\u274c Threading"); }
   }

}
