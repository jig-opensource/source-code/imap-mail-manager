﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Hierarchy;
using ImapMailManager.Config;
using ImapMailManager.Mail.Hierarchy;
using ImapMailManager.Properties;
using jig.Tools;
using static ImapMailManager.Config.RuntimeConfig_MailServer;
using static ImapMailManager.Mail.ReadImapMailbox;


namespace ImapMailManager.Mail.Stats;

/// <summary>
/// Erzeugt die Mailbox-Statistiken
/// </summary>
public class Create_ImapMailbox_Statistics {

   #region Static Fields

   //+ Config
   //++ E-Mail Absender-Adressen, die die E-Mail klassifizieren 
   // From-Adressen, die sicher Fragen darstellen

   // From-Adressen, die sicher Antworten darstellen

   // From-Domänen, die zu unserer Domäne gehören
   // und deshalb wahrscheinlich Antworten darstellen

   //++ Postfach-Ordner, die die E-Mail klassifizieren 
   // Namen der Spam-Ordner

   // Namen der Gelöscht-Ordner

   // Namen der Entwurf-Ordner

   // Ordner mit den beantworteten Fragen, die danach ins Archiv kommen

   // Namen der Ordner mit erledigten E-Mails

   #endregion


   #region Public Methods and Operators

   /// <summary>
   /// Berechnet die Mailbox-Statistik
   ///
   ///ToDo Löschen, sobald funktioniert: CalcImapStats.Get_AntwortenVsNeueFragen_Stats
   /// 
   /// </summary>
   /// <param name="cfgImapServer"></param>
   /// <param name="cfgImapServerCred"></param>
   public static List<MailboxStatisticsProKW> GetMailboxStats(
      Sensitive_AppSettings.CfgImapServer                                 cfgImapServer
      , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred
      , bool                                        fetchHeaders) {
      //+ Die Mailbox lesen
      var rootNode =
         ReadMailbox_CreateHierarchy
            (cfgImapServer
             , cfgImapServerCred
             , fetchHeaders
             , null);

      //+ Alle Ordner-Elemente extrahieren
      IEnumerable<ImapFolderNde?> allFolders = rootNode.DescendantNodes(TraversalType.DepthFirst)
                                                       .Where(x => x.Data.ImapFolderNde != null)
                                                       .Select(x => x.Data.ImapFolderNde);

      Debug.WriteLine($"Anzahl Ordner: {allFolders.Count()}");

      // PrintFolderStatistics(allFolders);

      //+ Alle Mail-Elemente sammeln
      var allMsgs = rootNode.DescendantNodes(TraversalType.DepthFirst)
                            .Where(x => x.Data.ImapMsgNde != null)
                            .Select(x => x.Data.ImapMsgNde);

      Debug.WriteLine($"Anzahl Mails total: {allMsgs.Count()}");

      //++ Die unique Ordner-Liste aller Mails bestimmen 
      List<string> OrdnerListe = (
                                    from msgNde in allMsgs
                                    select msgNde.OImapFolder.FullName).Distinct()
                                                                       .OrderBy(x => x)
                                                                       .ToList();

      Debug.WriteLine($"Anzahl Ordner: {OrdnerListe.Count()}");

      //++ Alle Spam-Mails sammeln
      var SpamMsgs = allMsgs.Where
         (x => x.OImapFolder.FullName.StartsWith
             ("Junk-E-Mail"
              , StringComparison
                .OrdinalIgnoreCase));

      Debug.WriteLine($"Anzahl Spam-Mails: {SpamMsgs.Count()}");

      //++ Alle erledigten Mails sammeln
      var ErledigteMsgs = allMsgs.Where
         (x => ConfigMail_Basics.Kat_Folder_ErledigteFragen.ØStartsWithAnyItemInList
                (x.OImapFolder
                  .FullName));

      Debug.WriteLine($"Anzahl Mails beantwortet: {ErledigteMsgs.Count()}");

      //+ Statistik pro Jahr und KW
      var statistikJahrKw =
         from msg in allMsgs
         group msg by
            new {
               year = msg.ADate.Value.Year
               , kw = ISOWeek.GetWeekOfYear(msg.ADate.Value.DateTime)
            }
         into kwData
         select new MailboxStatisticsProKW {
            // Aus dem Jahr und der KW das Datum des letzten Tages der KW berechnen
            EndDatumDerKW = ISOWeek.ToDateTime
               (kwData.Key.year
                , kwData.Key.kw
                , DayOfWeek.Sunday)
            , ListeAntworten = kwData.Where
                                      (x => ConfigMail_Basics.Kat_Folder_ErledigteFragen.ØStartsWithAnyItemInList
                                             (x.OImapFolder
                                               .FullName))
                                     .ToList()
            ,

            // Wie viele AntwortenListe gab es in dieser KW?
            AnzAntwortenInKW = kwData
              .Count
                  (x => ConfigMail_Basics.Kat_Folder_ErledigteFragen.ØStartsWithAnyItemInList
                      (x.OImapFolder.FullName))
            ,

            // Welches waren die neuen Fragen in dieser KW?
            ListeNeueFragen = kwData.Where
                                     (x =>
                                         !ConfigMail_Basics.Kat_Folder_ErledigteFragen.ØStartsWithAnyItemInList
                                            (x.OImapFolder
                                              .FullName))
                                    .ToList()
            ,

            // Wie viele neue Fragen gab es in dieser KW?
            AnzNeueFragenInKW = kwData
              .Count
                  (x => !ConfigMail_Basics.Kat_Folder_ErledigteFragen.ØStartsWithAnyItemInList
                           (x.OImapFolder.FullName))
            ,

            // Die Summer aller offenen Fragen bis zu dieser KW
            // Wird später berechnet
            AnzOffeneFragenBisKW = 0
            ,

            // Nur zur Info
            AnzTotMailsInKW = kwData.Count()
         };

      // todo AnzOffeneFragenInKW berechnen

      // Sortieren
      var sortiert = statistikJahrKw.OrderBy(x => x.EndDatumDerKW).ToList();

      // AnzOffeneFragen berechnen
      foreach (var stat in sortiert) {
         // stat.AnzOffeneFragen = stat.AnzNeueFragenInKW - stat.AnzAntwortenInKW;
         // if (stat.AnzOffeneFragen < 0) { stat.AnzOffeneFragen = 0; }

         // Alle Fragen, die zu diesem Zeitpunkt offen sind
         stat.AnzOffeneFragenBisKW = sortiert.Where(x => x.EndDatumDerKW <= stat.EndDatumDerKW)
                                             .Sum(x => x.AnzNeueFragenInKW);
      }

      // var sres = sortiert.ToList();

      //+ Die Statistik im Excel speichern
      // MyExcelTools.ExportMailboxStatisticsProKW(sortiert, true);

      return sortiert;
   }


   /// <summary>
   /// Gibt die Statistik für jeden Ordner aus
   /// </summary>
   /// <param name="allFolders"></param>
   /// <param name="eMailCategories"></param>
   /// <param name="imapFolderNdes"></param>
   /// <param name="erledigteMailsFolders"></param>
   /// <param name="offeneMailsFolders"></param>
   public static void PrintFolderStatistics(
      IEnumerable<IGrouping<CalcImapStats.eMailCategory, ImapFolderNde>> mbxFoldersGroupByCat
      , Dictionary<int, CalcImapStats.eMailCategory>                     catOrder) {
      //+ Config
      var printCsv = true;

      //+ Die Liste nach Category sortiert verarbeiten
      foreach (var thisCat in catOrder.OrderBy(x => x.Key).Select(x => x.Value)) {
         //++ Den Header ausgeben
         Debug.WriteLine("");
         Debug.WriteLine($"Ordner: {thisCat}");

         //! Ex: Die Mails einer Gruppe holen 
         List<ImapFolderNde>? mailsInCat
            = mbxFoldersGroupByCat.FirstOrDefault(x => x.Key == thisCat)?.ToList();

         // Die Mails der Kategorie durchlaufen 
         // ‼ !TTTom C#, Linq: foreach, das auch mit null objects klappt
         mailsInCat?.ForEach
            (
             imapFolderNde => {
                //++ Die statistischen Infos ausgeben
                Debug.WriteLine
                   (
                    printCsv
                       ? imapFolderNde?.GetCsv($"{thisCat}")
                       : imapFolderNde?.ToString());
             });
      }
   }

   #endregion

}
