using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Hierarchy;
using ImapMailManager.Config;
using ImapMailManager.Mail.Hierarchy;
using ImapMailManager.Properties;
using jig.Tools;
using jig.WPF.SerilogViewer;
using NPOI.OpenXmlFormats.Dml;


namespace ImapMailManager.Mail.Stats;

public class CalcImapStats {

   /// <summary>
   /// Klassifiziert die E-Mails und Mailbox-Ordner
   /// </summary>
   public enum eMailCategory {

      // Könnte ein Entwurf sein?
      AbsenderUnbekannt,

      // Standard-Ordner.
      Spam, Gelöscht,

      // Antort: Die verschickte E-Mail ist eine Antwort vom Team
      Antwort,

      // Die E-Mail ist die original Frage 
      FrageOffen,

      // Die E-Mail ist die original Frage, sie wurde aber beantwortet und ist deshalb auf dem weg ins Archiv 
      FrageErledigt

      // Die E-Mail wird z.Z. bearbeitet / beantwortet 
      , FrageInArbeit

      // Die Kategorie konnte nicht erkannt werden
      , CatUnkown

   }


   //++ Die Sortierung in der Excel-Auswertung
   public static readonly Dictionary<int, CalcImapStats.eMailCategory> StatsSortierung
      = new Dictionary<int, CalcImapStats.eMailCategory>() {
         { 0, CalcImapStats.eMailCategory.Gelöscht }
         , { 1, CalcImapStats.eMailCategory.Spam }
         , { 2, CalcImapStats.eMailCategory.AbsenderUnbekannt }
         , { 3, CalcImapStats.eMailCategory.FrageInArbeit }
         , { 4, CalcImapStats.eMailCategory.FrageOffen }
         , { 5, CalcImapStats.eMailCategory.Antwort }
         , { 6, CalcImapStats.eMailCategory.FrageErledigt }
         , { 7, CalcImapStats.eMailCategory.CatUnkown }
      };

   
   
   /// <summary>
   /// Hilfsfunktion für Tests
   /// </summary>
   /// <param name="cfgImapServer"></param>
   /// <param name="cfgImapServerCred"></param>
   /// <param name="fetchHeaders"></param>
   /// <param name="serilogViewer"></param>
   public static IEnumerable<IGrouping<CalcImapStats.eMailCategory, ImapMsgNde>>
      Analyze_GesendetFolder_SenderDomain(
         Sensitive_AppSettings.CfgImapServer               cfgImapServer
         , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred
         , bool                                        fetchHeaders
         , SerilogViewer                               serilogViewer) {
      //+ Die Mailbox-Hierarchie lesen
      var rootNode =
         ReadImapMailbox.ReadMailbox_CreateHierarchy
            (
             cfgImapServer
             , cfgImapServerCred
             , fetchHeaders
             , serilogViewer);

      //++ Alle Mail-Elemente sammeln
      IEnumerable<ImapMsgNde?> allMsgs = rootNode.DescendantNodes(TraversalType.DepthFirst)
                                                 .Where(x => x.Data.ImapMsgNde != null)
                                                 .Select(x => x.Data.ImapMsgNde);

      //+ Die Mails gruppieren
      var mbxFoldersGroupByCat =
         from mails in allMsgs
         group mails by mails.MailCategory;

      return mbxFoldersGroupByCat;
   }


   /// <summary>
   /// Hilfsfunktion für Tests
   /// </summary>
   /// <param name="cfgImapServer"></param>
   /// <param name="cfgImapServerCred"></param>
   /// <param name="fetchHeaders"></param>
   /// <param name="serilogViewer"></param>
   public static IEnumerable<IGrouping<CalcImapStats.eMailCategory, ImapMsgNde>>
      Analyze_Mbx_SendEMailAddresses(
         Sensitive_AppSettings.CfgImapServer               cfgImapServer
         , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred
         , bool                                        fetchHeaders
         , SerilogViewer                               serilogViewer) {
      //+ Die Mailbox-Hierarchie lesen
      var rootNode =
         ReadImapMailbox.ReadMailbox_CreateHierarchy
            (
             cfgImapServer
             , cfgImapServerCred
             , fetchHeaders
             , serilogViewer);

      //++ Alle Mail-Elemente sammeln
      IEnumerable<ImapMsgNde?> allMsgs = rootNode.DescendantNodes(TraversalType.DepthFirst)
                                                 .Where(x => x.Data.ImapMsgNde != null)
                                                 .Select(x => x.Data.ImapMsgNde);

      // Filtern nach Sender Domain
      var SentbyRogerLiebiCh =
         allMsgs.Where
            (
             x => x.FromDomain != null
                  && x.FromDomain.Equals
                     (
                      "RogerLiebi.ch"
                      , StringComparison.OrdinalIgnoreCase)).ToList();

      var SentbyRogerLiebiChFroms = SentbyRogerLiebiCh.SelectMany(x => x.From);

      //+ Interessant:
      // "RL-Kontaktformular" <KontaktFormular@rogerliebi.ch>
      // Address  : KontaktFormular@rogerliebi.ch
      // Name     : RL-Kontaktformular

      // Sind sicher Fragen:
      //    Address  : KontaktFormular@rogerliebi.ch
      //    Address  : freundesbrief@rogerliebi.ch

      // Sind sicher Antworten:
      //    Address  : team@rogerliebi.ch
      //    Address  : RogerLiebi@RogerLiebi.ch
      var SentbyRogerLiebiChFromsunique = SentbyRogerLiebiChFroms.Distinct();

      // Gruppieren nach Sender Domain
      var mbxMailsGroupByFromDomain =
         from mails in allMsgs
         group mails by mails.FromDomain;

      // Nur rogerliebi absender
      /*
      var SentbyRogerLiebiCh = mbxMailsGroupByFromDomain
        .Where(x => x.Key.Equals("RogerLiebi.ch", StringComparison.OrdinalIgnoreCase) );
      */

      return null;
   }


   /// <summary>
   /// Bereitet die Statistik für die Mailbox-Ordner Belegung vor,
   /// gruppiert die E-Mails und liefert die Sortier-Reihenfolge
   /// </summary>
   /// <param name="cfgImapServer"></param>
   /// <param name="cfgImapServerCred"></param>
   /// <param name="fetchHeaders"></param>
   /// <param name="serilogViewer"></param>
   /// <param name="rootNodeyNode"></param>
   /// <returns>
   /// - Gruppierte E-Mails nach Kategorie
   /// - Sortierung der Gruppen
   /// </returns>
   public static Tuple<IEnumerable<IGrouping<CalcImapStats.eMailCategory, ImapFolderNde>>,
         Dictionary<int, CalcImapStats.eMailCategory>>
      Calc_MailboxFolder_Stats(
         Sensitive_AppSettings.CfgImapServer               cfgImapServer
         , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred
         , bool                                        fetchHeaders
         , SerilogViewer                               serilogViewer
         , HierarchyNode<ImapHrchyNde>?                rootNode) {

      //+ Die Mailbox lesen
      if (rootNode == null) {
         rootNode =
            ReadImapMailbox.ReadMailbox_CreateHierarchy
               (
                cfgImapServer
                , cfgImapServerCred
                , fetchHeaders
                , serilogViewer);
      }

      //++ Alle Ordner-Elemente sammeln
      var allFolders = rootNode.DescendantNodes(TraversalType.DepthFirst)
                               .Where(x => x.Data.ImapFolderNde != null)
                               .Select(x => x.Data.ImapFolderNde);

      //++ Mails nach Kategorie gruppieren
      //‼ Kann so nicht als Resultat zurückgegeben werden, weil select new ein anonymes Obj erzeugt:
      /*
      var allMsgsGroupByCat =
         from mails in allFolders
         group mails by mails.MailCategory into g
         select new {
            Cat     = g.Key
            , Mails = g.ToList()
         };
      */

      //‼ Workaround
      IEnumerable<IGrouping<CalcImapStats.eMailCategory, ImapFolderNde>> allMsgsGroupByCat =
         from mails in allFolders
         group mails by mails.MailCategory;

      return new Tuple<IEnumerable<IGrouping<CalcImapStats.eMailCategory, ImapFolderNde>>,
            Dictionary<int, CalcImapStats.eMailCategory>>
         (
          allMsgsGroupByCat
          , StatsSortierung);
   }


   /// <summary>
   /// Bereitet die Statistik für jede KW:
   /// - Anz. beantwortete Fragen in dieser KW
   /// - Anz. neue Fragen in dieser KW
   /// - Anz. offene Fragen bis zu dieser KW
   /// </summary>
   /// <param name="cfgImapServer"></param>
   /// <param name="cfgImapServerCred"></param>
   /// <param name="fetchHeaders"></param>
   /// <param name="serilogViewer"></param>
   /// <param name="hierarchyNode"></param>
   /// <param name="rootNode"></param>
   /// <returns>
   /// - Gruppierte E-Mails nach Kategorie
   /// - Sortierung der Gruppen
   /// </returns>
   ///
   /// Zugriff via: AppCfgRuntime.AntwortenVsNeueFragen_Stats
   /// 
   public static List<MailboxStatisticsProKW> Calc_AntwortenVsNeueFragen_Stats(
      Sensitive_AppSettings.CfgImapServer               cfgImapServer
      , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred
      , bool                                        fetchHeaders
      , SerilogViewer                               serilogViewer
      , HierarchyNode<ImapHrchyNde>?                rootNode) {
      //+ Config

      //+ Die Mailbox lesen
      if (rootNode == null) {
         rootNode =
            ReadImapMailbox.ReadMailbox_CreateHierarchy
               (
                cfgImapServer
                , cfgImapServerCred
                , fetchHeaders
                , serilogViewer);
      }

      //+ Alle Mail-Elemente sammeln
      var allMsgs = rootNode.DescendantNodes(TraversalType.DepthFirst)
                            .Where(x => x.Data.ImapMsgNde != null)
                            .Select(x => x.Data.ImapMsgNde);

      Debug.WriteLine($"Anzahl Mails total: {allMsgs.Count()}");

      //++ Mails nach Jahr und KW
      //+ Statistik pro Jahr und KW
      var statistikJahrKw =
         from msg in allMsgs
         group msg by
            new {
               year = msg.ADate.Value.Year
               , kw = ISOWeek.GetWeekOfYear(msg.ADate.Value.DateTime)
            }
         into kwData
         select new MailboxStatisticsProKW {
            // Aus dem Jahr und der KW das Datum des letzten Tages der KW berechnen
            EndDatumDerKW = ISOWeek.ToDateTime
               (kwData.Key.year
                , kwData.Key.kw
                , DayOfWeek.Sunday)

            // Alle E-Mails in dieser KW, die als Antwort klassifiziert sind
            , ListeAntworten = kwData.Where
                                      (
                                       x =>
                                          x.MailCategory == CalcImapStats.eMailCategory.Antwort)
                                     .ToList()
            , ListeFrageInArbeit = kwData.Where
                                                   (
                                                    x =>
                                                       x.MailCategory == CalcImapStats.eMailCategory.FrageInArbeit)
                                                  .ToList()
            , ListeFrageErledigt = kwData.Where
                                          (
                                           x =>
                                              x.MailCategory == CalcImapStats.eMailCategory.FrageErledigt)
                                         .ToList()
            ,

            // Anz. E-Mails in dieser KW, die als Antwort klassifiziert sind
            AnzAntwortenInKW = kwData
              .Count(x => x.MailCategory == CalcImapStats.eMailCategory.Antwort)
            ,
            
            // Anz. E-Mails in dieser KW, die als erledigte Frage klassifiziert sind
            AnzFrageErledigtInKW = kwData
              .Count(x => x.MailCategory == CalcImapStats.eMailCategory.FrageErledigt)
            ,

            // Anz. E-Mails in dieser KW, die als erledigte Frage klassifiziert sind
            AnzFrageInArbeitInKW = kwData
              .Count(x => x.MailCategory == CalcImapStats.eMailCategory.FrageInArbeit)
            ,

            // Alle E-Mails in dieser KW, die als Frage klassifiziert sind
            ListeNeueFragen = kwData.Where
                                     (
                                      x =>
                                         x.MailCategory == CalcImapStats.eMailCategory.FrageOffen)
                                    .ToList()
            ,

            // Anz. E-Mails in dieser KW, die als Frage klassifiziert sind
            AnzNeueFragenInKW = kwData
              .Count(x => x.MailCategory == CalcImapStats.eMailCategory.FrageOffen)
            ,

            // Die Summer aller offenen Fragen bis zu dieser KW
            // Wird später berechnet
            AnzOffeneFragenBisKW = 0
            ,

            // Nur zur Info
            AnzTotMailsInKW = kwData.Count()
         };

      //+++ Sortieren nach Datum
      var sortiert = statistikJahrKw.OrderBy(x => x.EndDatumDerKW).ToList();

      var kw382022 = sortiert.Where(x => x.Jahr == 2022 && x.KW == 38).ToList();

      //+++ AnzOffeneFragen summieren
      foreach (var stat in sortiert) {
         // stat.AnzOffeneFragen = stat.AnzNeueFragenInKW - stat.AnzAntwortenInKW;
         // if (stat.AnzOffeneFragen < 0) { stat.AnzOffeneFragen = 0; }

         //+ Debug
         /*
         var alleFragenBis    = sortiert.Where(x => x.EndDatumDerKW <= stat.EndDatumDerKW);
         var alleFragenBisSum = alleFragenBis.SelectMany(x => x.ListeNeueFragen);
         var alleFragenBisAnz = alleFragenBisSum.Count();
         var thisKW           = $"{stat.Jahr}-{stat.KW}";
         // Debug.WriteLine(thisKW);

         if (stat.Jahr == 2021 && stat.KW == 47) {
            int stopper = 1;
         }

         int stopper2 = 1;
         */
         
         // Alle Fragen, die zu diesem Zeitpunkt offen sind
         stat.AnzOffeneFragenBisKW = sortiert.Where(x => x.EndDatumDerKW <= stat.EndDatumDerKW).Sum
            (x => x.AnzNeueFragenInKW);
      }

      return sortiert;
   }

}
