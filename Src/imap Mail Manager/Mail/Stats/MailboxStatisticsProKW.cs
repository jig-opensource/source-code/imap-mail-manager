using System;
using System.Collections.Generic;
using System.Globalization;
using ImapMailManager.Mail.Hierarchy;


namespace ImapMailManager.Mail.Stats;

/// <summary>
/// Die Statistik der Mailbox pro KW,
/// wird von der Mailbox-Statistik abgefüllt
/// </summary>
public class MailboxStatisticsProKW {

   #region Constructors and Destructors

      public MailboxStatisticsProKW() => IsExportedToGoogleSheets = false;

      #endregion


      #region Public Properties

      /// <summary>
      /// Alle verschickten E-Mails, die in dieser KW beantwortet wurden
      /// </summary>
      public List<ImapMsgNde?> ListeAntworten { get; set; }
      /// <summary>
      /// Alle E-Mail Fragen, die in dieser KW bearbeitet werden
      /// </summary>
      public List<ImapMsgNde?> ListeFrageInArbeit { get; set; }
      /// <summary>
      /// Alle E-Mail Fragen, die in dieser KW beantwortet wurden
      /// </summary>
      public List<ImapMsgNde?> ListeFrageErledigt { get; set; }

      /// <summary>
      /// Wie viele Fragen wurden in dieser KW beantwortet?
      /// </summary>
      public int AnzFrageErledigtInKW { get; init; }
      /// <summary>
      /// Bei wie vielen Fragen arbeiten wir an der Antwort?
      /// </summary>
      public int AnzFrageInArbeitInKW { get; init; }
      /// <summary>
      /// Wie viele Antworten wurden in dieser KW erzeugt?
      /// Zählt uA die Anz. Mail im Gesendet-Ordner
      /// </summary>
      public int AnzAntwortenInKW { get; init; }

      /// <summary>
      /// Die Anz. E-Mails, die in dieser KW empfangen wurden
      /// D.h. die in einem Ordner in Arbeit sind
      /// </summary>
      public int AnzNeueFragenInKW { get; init; }

      /// <summary>
      /// Die Anz. E-Mails, die in dieser KW bearbeitet wurden
      /// </summary>
      public int AnzAntwortenInArbeitInKW { get; init; }


      /// <summary>
      /// Summiert alle offenen E-Mails bis zu dieser KW
      /// </summary>
      public int AnzOffeneFragenBisKW { get; set; }
      /// <summary>
      /// E-Mails insgesamt in der Mailbox in dieser KW
      /// </summary>
      public int AnzTotMailsInKW { get; init; }

      /// <summary>
      /// Das Datum der Kalenderwoche mit dem letzten Tag der KW
      /// </summary>
      public DateTime EndDatumDerKW { get; init; }

      /// <summary>
      /// Markiert, sobald ein Datensatz zu GSheets übertragen wurde
      /// </summary>
      public bool IsExportedToGoogleSheets { get; set; }

      /// <summary>
      /// Wenn das Datum im Jahr 1 ist, dann ist eine E-Mail ziemlich sicher im Entwurf
      /// Die statistische Auswertung dieser E-Mail müssen wir ausbleden können
      /// Deshalb: alles vor 1900 ist komisch :-)
      /// </summary>
      public bool IsStatsForMailsImEntwurf => EndDatumDerKW.Year < 1900;

      //+++ Berechnete Daten
      public int Jahr => EndDatumDerKW.Year;
      public int KW   => ISOWeek.GetWeekOfYear(EndDatumDerKW);
      /// <summary>
      /// Alle neuen E-Mails, die in dieser KW empfangen wurden
      /// </summary>
      public List<ImapMsgNde?> ListeNeueFragen { get; set; }

      #endregion


      #region Public Indexers

      // Via String Array Zugriff und Reflection auf Properties des Obj zugreifen
      // Erlaubt:
      //	 Foo f = new Foo();
      //	 f["Bar"] = "asdf";
      //	 string s = (string)f["Bar"];
      // 
      // !Q9 https://stackoverflow.com/a/24919811/4795779
      // !Q9 https://stackoverflow.com/a/49133964/4795779
      public object? this[string propName] {
         get => GetType().GetProperty(propName)?.GetValue(this, null);
         set => GetType().GetProperty(propName)?.SetValue(this, value, null);
      }

      #endregion


      #region Public Methods and Operators

      public void Deconstruct(out DateTime Datum,                out int AnzAntworten, out int AnzNeueFragen,
                              out int      AnzOffeneFragenBisKW, out int AnzTot) {
         Datum                = this.EndDatumDerKW;
         AnzAntworten         = AnzAntwortenInKW;
         AnzNeueFragen        = AnzNeueFragenInKW;
         AnzOffeneFragenBisKW = this.AnzOffeneFragenBisKW;
         AnzTot               = this.AnzTotMailsInKW;
      }


      public override string ToString() => $"{Jahr}-{KW}, {EndDatumDerKW}: {AnzAntwortenInKW} / {AnzNeueFragenInKW} / {AnzOffeneFragenBisKW}";

      #endregion
}

