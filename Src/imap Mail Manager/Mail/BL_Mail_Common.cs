using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ImapMailManager.Config;
using ImapMailManager.Mail.Hierarchy;
using ImapMailManager.Mail.Stats;
using jig.Tools;
using jig.Tools.HTML;
using jig.Tools.String;
using MailKit;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ImapMailManager.Mail;

/// <summary>
/// Generelle Business Logik für's Handling der E-Mails 
/// </summary>
public class BL_Mail_Common {

   /// <summary>
   /// Der Konverter für Html zu Text
   /// ‼ Wenn gewählt werden kann, unbedingt OuterHTML nützen, sonst erhält man nicht den ganzten Text!
   /// </summary>
   public static readonly HtmlToText OHtmlToText = new();

   #region Mailbox: Standard FolderFilter

   /// <summary>
   /// Standard FolderFilter für Mbx Ordner Gelöscht
   /// IMailbox_FolderFilter.FolderCompare.RgxMatch
   /// </summary>
   public static readonly List<string> FolderFilter_Gelöscht = new()
   { ExtensionMethods_RegEx.ØWildcardToRegex(@"Gelöscht*".Replace('\\', '/')),
     ExtensionMethods_RegEx.ØWildcardToRegex(@"Trash*".Replace('\\', '/')) };

   /// <summary>
   /// Standard FolderFilter für Mbx Ordner Spam
   /// IMailbox_FolderFilter.FolderCompare.RgxMatch
   /// </summary>
   public static readonly List<string> FolderFilter_Spam = new()
   { ExtensionMethods_RegEx.ØWildcardToRegex(@"Spam*".Replace('\\', '/')),
     ExtensionMethods_RegEx.ØWildcardToRegex(@"Junk*".Replace('\\', '/')) };

   /// <summary>
   /// Standard FolderFilter für Mbx Ordner Entwürfe
   /// IMailbox_FolderFilter.FolderCompare.RgxMatch
   /// </summary>
   public static readonly List<string> FolderFilter_Draft = new()
   { ExtensionMethods_RegEx.ØWildcardToRegex(@"Drafts*".Replace('\\', '/')),
     ExtensionMethods_RegEx.ØWildcardToRegex(@"Entwürfe*".Replace('\\', '/')) };

      /// <summary>
   /// Standard FolderFilter für Mbx Ordner Gesendet
   /// IMailbox_FolderFilter.FolderCompare.RgxMatch
   /// </summary>
   public static readonly List<string> FolderFilter_Send = new()
   { ExtensionMethods_RegEx.ØWildcardToRegex(@"Sent*".Replace('\\', '/')),
     ExtensionMethods_RegEx.ØWildcardToRegex(@"Gesendet*".Replace('\\', '/')) };

   
   #endregion Mailbox: Standard FolderFilter
   
   #region Mailbox: Ordner-Klassifikation fürs Archiv
   

   /// <summary>
   /// Ordner mit gelöschten Mails
   /// </summary>
   public static readonly List<string> Kat_ArchivOrdner_Gelöscht_Rgx
      = new List<string>(new[] { "Gelöscht", "Trash" });

   /// <summary>
   /// Ordner mit Spam
   /// </summary>
   public static readonly List<string> Kat_ArchivOrdner_Spam_Rgx
      = new List<string>(new[] { "Junk", "Spam" });

   /// <summary>
   /// Ordner mit Entwürfen
   /// </summary>
   public static readonly List<string> Kat_ArchivOrdner_Entwurf_Rgx
      = new List<string>(new[] { "Drafts", "Entwürfe" });

   /// <summary>
   /// Ordner Inbox
   /// </summary>
   public static readonly List<string> Kat_ArchivOrdner_Inbox_Rgx
      = new List<string>(new[] { "Inbox" });

   /// <summary>
   /// Ordner Gesendet
   /// </summary>
   public static readonly List<string> Kat_ArchivOrdner_Gesendet_Rgx
      = new List<string>(new[] { @"Gesendete\s+Elemente", "Sent" });

   /// <summary>
   /// Ordner Archiv
   /// </summary>
   public static readonly List<string> Kat_ArchivOrdner_Archiv_Rgx
      = new List<string>(new[] { "Archive", "Sent" });

   /// <summary>
   /// Erledigt
   /// </summary>
   public static readonly List<string> Kat_ArchivOrdner_Erledigt_Rgx
      = new List<string>(new[] { @"\d+\s+Erledigt" });

   #endregion Mailbox: Ordner-Klassifikation fürs Archiv

   #region Rgx RefNr

   /// <summary>
   /// RefNr., wenn wir keine gefunden haben
   /// </summary>
   public static readonly string NoRefNrFound = "#00-0000";

   ///summary>
   /// Die Regex-Definition unserer Mail Referenz Nummer
   /// #xx-xxxx
   /// </summary>
   private static readonly RegexOptions RgxOptionsRefNr = RegexOptions.IgnoreCase
                                                          | RegexOptions.Multiline
                                                          | RegexOptions.ExplicitCapture
                                                          | RegexOptions.CultureInvariant
                                                          | RegexOptions.Compiled;
   private static readonly string sRgxRefNr
      = "(?<ID>(?<RefNr>#[a-zA-Z0-9]{1,2}-[0-9]{4})(?:\\s+|:|$))";
   public static readonly Regex oRgxRefNr = new Regex(sRgxRefNr, RgxOptionsRefNr);

   #endregion Rgx RefNr


   /// <summary>
   /// Liefert true, wenn strTest eine Refnr ist
   /// </summary>
   /// <param name="strTest"></param>
   /// <returns>
   /// Liefert den Rest-String nach dem Match zurück
   /// var(success, matchValue, restString) = … 
   /// </returns>
   public static Tuple<bool, string, string> Contains_Mail_RefNr(string strTest) {
      var match      = oRgxRefNr.ØMatch(strTest);
      var restString = "";
      if (match.Success) {
         restString = strTest.Substring(match.Index + match.Length);
      }
      return new Tuple<bool, string, string>(match.Success, match.Value, restString);
   }


   /// <summary>
   /// Liest die RedNr aus strTest
   /// </summary>
   /// <param name="strTest"></param>
   /// <returns></returns>
   public static string? Get_Mail_RefNr(string? strTest) {
      if (strTest.ØHasValue()) {
         var match = oRgxRefNr.Match(strTest);

         if (match.Success) {
            return match.Groups["RefNr"].Value.Trim();
         }
      }

      return null;
   }


   /// <summary>
   /// Berechnet für einen Mailbox-Ordner den entsprechenden Pfad im Archiv
   /// 
   /// ! Die Berechnung erfolgt nur aufgrund des Ordners
   /// Für einzelne E-Mails im \Archiv muss der Zielpfad individuell pro Mail berechnet werden
   /// </summary>
   /// <param name="mailFolder"></param>
   /// <returns>
   /// ‼ Null: Die E-Mail wird nicht im Archiv gespeichert sondern gelöscht
   /// </returns>
   [Obsolete("Diese Funktion ist obsolete » obj BO_Mail.MailFolder_ArchivPath", false)]   
   public static string? Calc_MbxOrdner_ArchivPath(IMailFolder mailFolder) {
      // ✅ Debug
      bool isDebugActive = false;

      //+ Die eindeutigen Ordner verarbeiten

      #region E-Mails, die gelöscht werden

      //++ E-Mails, die gelöscht werden

      //+++ Gelöscht
      if (Kat_ArchivOrdner_Gelöscht_Rgx.ØRgxMatchWithAnyItemInList
             (mailFolder.FullName)) {
         // Die E-Mail wird nicht im Archiv gespeichert sondern gelöscht 
         return null;
      }

      //+++ Spam
      if (Kat_ArchivOrdner_Spam_Rgx.ØRgxMatchWithAnyItemInList
             (mailFolder.FullName)) {
         // Die E-Mail wird nicht im Archiv gespeichert sondern gelöscht 
         return null;
      }

      #endregion E-Mails, die gelöscht werden

      //++ E-Mails, die im Archiv abgelegt werden

      #region Alle Ordner, deren E-Mails im Archiv abgelegt werden können

      //+++ Archiv
      if (Kat_ArchivOrdner_Archiv_Rgx.ØRgxMatchWithAnyItemInList
             (mailFolder.FullName)) {
         return "Archiv";
      }

      //+++ Entwurf
      if (Kat_ArchivOrdner_Entwurf_Rgx.ØRgxMatchWithAnyItemInList
             (mailFolder.FullName)) {
         return "Entwurf";
      }

      //+++ Inbox
      if (Kat_ArchivOrdner_Inbox_Rgx.ØRgxMatchWithAnyItemInList
             (mailFolder.FullName)) {
         // Die E-Mail wird nicht im Archiv gespeichert sondern gelöscht 
         return "Inbox";
      }

      //+++ Gesendet
      if (Kat_ArchivOrdner_Gesendet_Rgx.ØRgxMatchWithAnyItemInList
             (mailFolder.FullName)) {
         // Die E-Mail wird nicht im Archiv gespeichert sondern gelöscht 
         return "Gesendet";
      }

      //+++ Erledigt
      // ! Muss besonders behandelt werden:
      // Im Ordner Erledigt sind E-Mails, die beantwortet wurden.
      // Diese werden im Archiv entsprechend den Triage-HashTags in den Mails Header 
      // in den entsprechenden Archiv-Ordnern gespeicher
      if (Kat_ArchivOrdner_Erledigt_Rgx.ØRgxMatchWithAnyItemInList
             (mailFolder.FullName)) {
         return "Entwurf";
      }

      #endregion E-Mails, die im Archiv abgelegt werden

      //+ Alle Order des Arbeitsprozesses 

      #region Alle Order des Arbeitsprozesses

      //++ Die Nummer zur Sortierung der Ordner erscheint nicht im Archiv oder Hashtag,
      // weil diese Nummerierung ändern kann und so später im Archiv nur Verwirrung stiftet
      var archivPath = CleanPath_RemoveNumbers(mailFolder.FullName);

      // Debug.WriteLine($"\n{mailFolder.FullName}");
      // Debug.WriteLine(archivPath);

      //+ Wenn das Ziel im Hauptordner der Triage liegt, dann diesen Verzeichnisnamen ersetzen,
      // Weil der Hauptordner nichts zur Klassifizierung der E-Mails beiträgt 
      if (ConfigMail_Triage.Kat_Triage_Hauptordner_Rgx.ØRgxMatchWithAnyItemInList
             (archivPath)) {
         //++ Den obersten Pfad ersetzen
         archivPath = archivPath.ØReplaceRootDir(ConfigMail_Triage.TriageArchivOrdnerName);
      }

      #endregion Alle Order des Arbeitsprozesses

      // ‼ Debug
      if (isDebugActive && !allArchivePaths.Contains(archivPath)) {
         allArchivePaths.Add(archivPath);
         Debug.Write($"{mailFolder.FullName,70}  »  ");
         Debug.WriteLine(archivPath);
      }

      return archivPath;
   }


   /// <summary>
   /// Entfernt von einem Pfad die Nummern,
   /// die der Sortierung der Mailbox-Ordner dienen
   /// 
   /// Sie haben dieses Format:
   /// ^\d+[a-z[A-Z]?
   /// 
   /// e.g.
   /// 10 Prozess, in Arbeit/20 In Arbeit: Seelsorge/Patrik Hasler/
   /// » Prozess, in Arbeit/In Arbeit: Seelsorge/Patrik Hasler/
   /// </summary>
   /// <param name="pfad"></param>
   /// <returns></returns>
   internal static string CleanPath_RemoveNumbers(string pfad) {
      //+ Den Pfad bereinigen
      List<string> archivPathItems = new List<string>();

      //++ Den Pfad Delimiter suchen
      var pathDelimiter = pfad.ØGetDelimimter(new[] { '\\', '/' });

      //++ Den Pfad aufteilen
      var pathItems = pfad.Split
         (new[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);

      //++ Den Pfad bereinigen
      foreach (var pathItem in pathItems) {
         //+++ Die Nummern, die ser Sortierung der Mailbox-Ordner dienen, löschen
         // weil sie ändern können und dann später im Archiv nur Verwirrung stiften 
         var tmp = Regex.Replace
            (pathItem
             , @"^\d+[a-z[A-Z]?"
             , ""
             , RegexOptions.IgnoreCase).Trim();

         //+++ Alle anderen Zahlen löschen
         tmp = Regex.Replace
            (tmp
             , @"\d+"
             , ""
             , RegexOptions.IgnoreCase).Trim();

         //++ Mehrfache Leerzeichen löschen
         tmp = tmp.ØReduceWhiteSpaces();
         archivPathItems.Add(tmp);
      }

      string archivPath;

      if (archivPathItems.Count > 1) {
         archivPath = String.Join((char)pathDelimiter!, archivPathItems);
      }
      else {
         archivPath = archivPathItems[0];
      }

      return archivPath;
   }


   /// <summary>
   /// Berechnet aus dem Mailbox-Ordner die HashTags für die Triage
   /// ‼ HashTags werden nur für triagierte E-Mails, also offene Fragen gesetzt,
   /// also wenn sie innerhalb des Ornders sind:
   /// \Kontaktformular
   /// </summary>
   public static List<string> Analyze_TriageHashTags(IMailFolder mailFolder) {
      // ✅ Debug
      bool isDebugActive = false;

      //+ Prepare
      List<string> triageHashTags = new List<string>();

      //+ Den Archiv-Pfad aufgrund des Ordners bestimmen
      string? archivPath = Calc_MbxOrdner_ArchivPath(mailFolder);

      //+ Wenn wir keinen Archiv-Pfad haben
      if (archivPath == null) {
         // Keine HashTags
         return triageHashTags;
      }

      //+ Aus dem Archiv-Pfad die HashTags berechnen
      //+ Betrifft nur Ordner der Triage, weil diese die E-Mails klassifizieren
      //++ Die Kategorie des Ordners bestimmen
      CalcImapStats.eMailCategory ordnerPfadCat =
         ImapFolderNde.CalculateMailCategoryByFolder(mailFolder.FullName);

      //+ Wenn es ein Triage-Ordner ist, d.h. wenn die Frage noch offen ist
      if (ordnerPfadCat == CalcImapStats.eMailCategory.FrageOffen) {
         //++ Wurde die E-Mail in einen allgemeinen Ordner triagiert,
         // der die E-Mail nicht klassifiziert?
         if (ConfigMail_Triage.Kat_Triage_TriageUnbekannt_Rgx.ØRgxMatchWithAnyItemInList
                (mailFolder.FullName)) {
            return triageHashTags;
         }

         //+ Die HashTags berechnen
         //++ Die Pfad-Elemente lesen
         var pathItems = archivPath.Split
            (new Char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);

         //++ Die Pfad Elemente verarbeiten
         foreach (var pathItem in pathItems) {
            //+++ Haben wir den Hauptordner der Triage, der nichts zur Klassifizierung der E-Mail beiträgt?
            if (ConfigMail_Triage.Kat_Triage_Hauptordner_Rgx.ØRgxMatchWithAnyItemInList
                   (pathItem)) {
               // Überspringen
               continue;
            }

            //+ Aus dem Ordner den HashTag berechnen

            //+++ Alle Zeichen, die nicht Teil eines HashTags sind, entfernen wir  
            var hashTag = Regex.Replace
                                (pathItem
                                 , $@"[^{BL_Mail_HashTag.SRgx_HashTag_FollowUpChars}]+"
                                 , " ")
                               .Trim();

            // Mehrfache Leerzeichen auf eines reduzieren
            hashTag = hashTag.ØReduceSpaces();

            // TitelCase applizieren: jesus christus » Jesus Christus
            hashTag = hashTag.ØToTitleCase();

            // Alle leerzeichen entfernen
            hashTag = hashTag.ØRemoveWhiteSpaces();

            // '#' ergänzen
            hashTag = hashTag.ØAddPrefix("#");

            //! Sicherstellen, dass das erzeuge HashTag gültig ist
            Assert.IsTrue(BL_Mail_HashTag.IsValidHashtag(hashTag));

            // ‼ Debug
            if (isDebugActive && !allHashTags.Contains(hashTag)) {
               Debug.Write($"\nPfad: {pathItem,30}");
               Debug.WriteLine($"#Tag: {hashTag}");
               allHashTags.Add(hashTag);
            }

            triageHashTags.Add(hashTag);
         }
      }

      return triageHashTags;
   }


   private static List<string> allHashTags     = new List<string>();
   private static List<string> allArchivePaths = new List<string>();

}
