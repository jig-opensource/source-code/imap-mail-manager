using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Ink;
using DocumentFormat.OpenXml.Drawing;
using Hierarchy;
using HtmlAgilityPack;
using ImapMailManager.Config;
using ImapMailManager.HTML.HtmlAgilityPack;
using ImapMailManager.Mail.MimeKit;
using ImapMailManager.Mail.Parser.Kontaktformular.Personendaten;
using ImapMailManager.Properties;
using jig.Tools;
using jig.Tools.String;
using jig.WPF.SerilogViewer;
using MailKit;
using MailKit.Net.Imap;
using MimeKit;
using TextCopy;


namespace ImapMailManager.Mail.TestTools;

/// <summary>
/// Testet, ob der Parser in allen E-Mails die Kontaktformular-Daten erkennt
/// </summary>
public class TestPostfach_AllMails_KontaktformularParser {

   /// <summary>
   /// Testet, ob der Parser in allen E-Mails die Kontaktformular-Daten erkennt
   /// </summary>
   /// <param name="cfgImapServerRl"></param>
   /// <param name="cfgImapServerCredAktiv"></param>
   /// <param name="serilogViewer"></param>
   public static void StartTest(
      Sensitive_AppSettings.CfgImapServer               cfgImapServer
      , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCred
      , SerilogViewer                               serilogViewer) {

      //+ Die Mailbox lesen
      var rootNode =
         ReadImapMailbox.ReadMailbox_CreateHierarchy
            (
             RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , false
             , serilogViewer);

      //+ Alle Ordner verarbeiten und jeweils alle E-Mails lesen

      //+ Verbinden
      ImapServerRuntimeConfig imapSrvRuntimeConfig = ImapServerRuntimeConfig.ConnectImap
         (cfgImapServerCred.IsProductiveMailbox ? "Prod" : "Test"
          , cfgImapServer
          , cfgImapServerCred);

      //++ Alle Ordner-Elemente sammeln
      var allFolders = rootNode.DescendantNodes(TraversalType.DepthFirst)
                               .Where(x => x.Data.ImapFolderNde != null)
                               .Select(x => x.Data.ImapFolderNde);

      //+ Alle Ordner durchlaufen
      foreach (var imapFolderNde in allFolders) {
         //++ Nur aktuellere Mails sind interessant
         if (!imapFolderNde.Path.StartsWith(@"Kontaktformular")) {
            Debug.WriteLine($"Skipped: {imapFolderNde.Path}");

            continue;
         }

         //++ Ältere Mails überspringen
         if (imapFolderNde.Path.StartsWith(@"Kontaktformular/7 Last Call")) {
            Debug.WriteLine($"Skipped: {imapFolderNde.Path}");

            continue;
         }

         Debug.WriteLine($"Verarbeite: {imapFolderNde.Path}");

         //++ Alle E-Mails lesen und verarbeiten
         IMailFolder? workingImapFolder = null;

         try {
            workingImapFolder = imapSrvRuntimeConfig.ImapClient.GetFolder
               (imapFolderNde.oImapFolder.FullName);
         } catch {
            Debug.WriteLine($" » Exception #2");

            continue;
         }

         workingImapFolder.Open(FolderAccess.ReadOnly);

         //++ Die messageSummaries lesen 
         var messageSummaries = workingImapFolder.ØFetch
            (
             0
             , -1
             , MessageSummaryItems.Envelope
               | MessageSummaryItems.BodyStructure
               | MessageSummaryItems.UniqueId
               | MessageSummaryItems.Size
               | MessageSummaryItems.Flags

             //+++ Wichtige Header auch lesen
             , ConfigMail_MsgHeader.Get_DefaultEMailHeader_Names());

         //+ Alle Mails lesen
         foreach (var messageSummary in messageSummaries) {
            MimeMessage thisMsg = workingImapFolder.GetMessage(messageSummary!.UniqueId);

            // ToDo 🚩 Test 
            // Alle Msg Header lesen, wenn eine E-Mail im Betreff Re: hat
            /*
            if(thisMsg.Subject.Contains("Re:") || thisMsg.Subject.Contains("Re ")) {
               HeaderList? allMailHeaders = null;
               allMailHeaders = workingImapFolder.GetHeaders(messageSummary!.UniqueId);

               foreach (var mailHeader in allMailHeaders) {
                  Debug.WriteLine($"\n{mailHeader.Field}:");
                  Debug.WriteLine($"{mailHeader.Value}");
               }

               int stopper = 1;

            }
            */

            //+++ Den Body holen
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(BL_Mail_Body.Get_MsgHtmlBody_Complete(thisMsg, true));

            //+++ Wenn der HTML Body einen Inhalt hat
            if (htmlDoc.DocumentNode.OuterHtml.ØHasValue()) {
               var subjectRefNr = BL_Mail_Common.Get_Mail_RefNr(thisMsg.Subject);

               // ‼  ️☑️ Debug: Ein Mail-Muster holen, um den Parser zu programmieren / testen / trainieren

               //! Prozess:
               // 1. Die gefundene Mail HTML in die Zwischenablage kopieren lassen
               // 2. In Notepad den HTML Text einfügen
               // 3. Und als neue Testdate speichern:
               //    c:\GitWork\GitLab.com\jig-Opensource\Source-Code\imap-Mail-Manager\Repo\Src\ImapMailManager.Test\Mail\Parser\Kontaktformular\UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #3.html
               // 4. Einen neuen Test schreiben:
               //    ImapMailManager.Test.Mail.Parser.Kontaktformular.UnitTest_ParseMail_Kontaktformular_FragestellerDaten

               var srchRfNr = @""; // #19-2603

               if (srchRfNr.ØHasValue()) {
                  if (thisMsg.Subject.Contains(srchRfNr)) {
                     // ‼ Body als html speichern
                     ClipboardService.SetText(BL_Mail_Body.Get_MsgHtmlBody_Complete(thisMsg, true));
                  }
               }

               //+ Den HTML Parser starten
               RecFragestellerV01? recFragesteller = null;
               string?             bodyRefNr;

               (_, bodyRefNr, recFragesteller)
                  = Parse_HtmlStruct.Parse_MailHtmlBody(htmlDoc, thisMsg.Subject);

               //+ Wenn der HTML Parser nicht erfolgreich war, den Text-Parser testen
               if (recFragesteller == null
                   || !recFragesteller.Get_DataAsString().ØHasValue()) {
                  recFragesteller = Parse_PureText.Parse_MailHtmlBody(htmlDoc);
               }

               //‼ Wenn der Betreff eine RefNr hat,
               //‼ dann müsste der Body Formulardaten haben
               if (subjectRefNr.ØHasValue()) {
                  //! weder bodyRefNr noch der recFragesteller gefunden wurden, melden
                  if (recFragesteller == null
                      || (!bodyRefNr.ØHasValue()
                          && !recFragesteller.Get_DataAsString().ØHasValue())) {
                     Debug.WriteLine
                        ($"  Keine Formulardaten gefunden: {messageSummary.Envelope.Subject}");
                     int stopper1 = 1;
                  }
               }
               else {
                  //+ Der Betreff hat keine RefNr
                  // Dann kann es eher sein, dass der Body keine Formulardaten hat
                  if (recFragesteller == null
                      || (!bodyRefNr.ØHasValue()
                          && !recFragesteller.Get_DataAsString().ØHasValue())) {
                     Debug.WriteLine
                        ($"  Keine Formulardaten gefunden: {messageSummary.Envelope.Subject}");
                     int stopper1 = 1;
                  }
               }
            }
         }
      }

      //+ Trennen
      imapSrvRuntimeConfig.ImapClient.Disconnect(true);
   }

}
