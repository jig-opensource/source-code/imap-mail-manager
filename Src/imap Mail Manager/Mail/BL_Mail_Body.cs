using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using Hierarchy;
using HtmlAgilityPack;
using ImapMailManager.Config;
using ImapMailManager.Mail.Hierarchy;
using ImapMailManager.Mail.MimeKit;
using ImapMailManager.Mail.Parser.Kontaktformular.Personendaten;
using ImapMailManager.Mail.Stats;
using jig.Tools;
using jig.Tools.Exception;
using jig.Tools.HTML;
using jig.Tools.String;
using MimeKit;
using TextCopy;


namespace ImapMailManager.Mail;

/// <summary>
/// Business Logik für den E-Mail HTML Body, inkl. html\head
/// </summary>
public class BL_Mail_Body {

   #region Business Logik

   /// <summary>
   /// Versucht die Personaldaten des Fragestellers herauszufinden
   /// </summary>
   /// <param name="msg"></param>
   public static RecFragestellerV01? TryGet_Msg_FragestellerDaten(MimeMessage msg) {
      //+ Den Mail Body (Text & HTML) holen
      var (msgTextPart, msgHtmlPart) = MimeKit_JigTools.Get_Msg_Bodies(msg, true);

      RecFragestellerV01? frageSteller = null;
      
      //+ Haben wir einen HTML-Body?
      if (msgHtmlPart.ØHasValue()) {
         //++ Der Body als HTMLDoc
         var htmlDoc = new HtmlDocument();
         htmlDoc.LoadHtml(msgHtmlPart);

         //++ Versuchen, den Fragesteller zu finden
         frageSteller = Parse_PureText.Parse_MailHtmlBody(htmlDoc);

         //‼ 🚩 Debug: Ins ClipBoard kopieren
         // ClipboardService.SetText(htmlAsText);
      }


      //+ Wenn wir keinen HTML Body haben
      if (frageSteller == null || !frageSteller.HasData()) {
         // Versuchen, den Fragesteller zu finden
         frageSteller = Parse_PureText.Parse_MailHtmlBody(msg.TextBody);
      }

      return frageSteller;
   }

   #endregion Business Logik

   #region Hilfsfunktionen

   /// <summary>
   /// Sucht nach unserer Mail ReferenzNummer
   /// #xx-xxxx
   /// </summary>
   /// <param name="msg"></param>
   [Obsolete("Diese Funktion ist obsolete » obj BO_Mail", false)]
   public static string FindMail_RefNr(MimeMessage msg) {
      string refNr_Subject = "";
      string refNr_Body    = "";

      //+ Suche im Betreff
      var matchSubject = BL_Mail_Common.oRgxRefNr.Match(msg.Subject);

      if (matchSubject.Success) {
         refNr_Subject = matchSubject.Groups["RefNr"].Value;
      }

      //+ Suche im Body
      //++ Den Mail Html Body holen
      var htmlBody = Get_MsgHtmlBody_BodyPartOnly(msg);

      // In Text konvertieren  
      var htmlAsText = BL_Mail_Common.OHtmlToText.Convert(htmlBody).ØReduceWhiteSpaces();

      // Finden wir eine RefNr?
      var matchHtml = BL_Mail_Common.oRgxRefNr.Match(htmlAsText);

      if (matchHtml.Success) {
         refNr_Body = matchSubject.Groups["RefNr"].Value;
      }

      //++ Wenn wir einen HTML Body haben, dann nützen wir diesen
      if (!refNr_Body.ØHasValue()) {
         var matchText = BL_Mail_Common.oRgxRefNr.Match(msg.TextBody);

         if (matchText.Success) {
            refNr_Body = matchSubject.Groups["RefNr"].Value;
         }
      }

      //+ Haben wir unterschiedliche RefNr?
      if (refNr_Subject.ØHasValue() && refNr_Body.ØHasValue()) {
         if (!refNr_Subject.Equals(refNr_Body, StringComparison.OrdinalIgnoreCase)) {
            throw new RuntimeException
               ($"Subject Mail RefNr: {refNr_Subject} != Body Mail RefNr: {refNr_Body}");
         }
      }

      //+ Haben wir keine RefNr gefunden?
      if (!refNr_Subject.ØHasValue() && !refNr_Body.ØHasValue()) {
         return BL_Mail_Common.NoRefNrFound;
      }

      //+ Eine der gefundenen RefNr zurückgeben
      return refNr_Subject.ØHasValue() ? refNr_Subject : refNr_Body;
   }


   /// <summary>
   /// Sucht alle HashTags in der E-Mail:
   /// - im Betreff
   /// - im Mail Header 
   /// - im Mail Body
   /// </summary>
   /// <param name="msg"></param>
   /// <returns>
   ///var (hashtags_InHeader, hashtags_InSubject, hashtags_InBody) = 
   /// </returns>
   /// <exception cref="RuntimeException"></exception>
   public static Tuple<List<string>, List<string>, List<string>>
      FindMail_Hashtags(MimeMessage msg) {

      //+Suche im Mail Header
      var hdrMsgHashTags = BL_Mail_MsgHeader.Get_MsgHeader
         (msg, ConfigMail_MsgHeader.AppMailHeaderType.TriageHashtags);
      List<string> hashtags_InHeader = BL_Mail_HashTag.FindHashtags_InText(hdrMsgHashTags?.Value);

      //+ Suche im Betreff
      List<string> hashtags_InSubject = BL_Mail_HashTag.FindHashtags_InText(msg.Subject);

      //+ Suche im Body
      //++ Den Mail Html Body holen
      var htmlBody = Get_MsgHtmlBody_BodyPartOnly(msg);

      // Html als Text  
      var htmlAsText = BL_Mail_Common.OHtmlToText.Convert(htmlBody).ØReduceWhiteSpaces();

      // Die HashTags suchen  
      List<string> hashtags_InBody = BL_Mail_HashTag.FindHashtags_InText(htmlAsText);

      //++ Wenn der Body keine HashTags hat, suchen wir im Text
      if (hashtags_InBody.Count == 0) {
         hashtags_InBody = BL_Mail_HashTag.FindHashtags_InText(msg.TextBody);
      }

      return new Tuple<List<string>, List<string>, List<string>>
         (hashtags_InHeader
          , hashtags_InSubject
          , hashtags_InBody);
   }


   /// <summary>
   /// Holt von einer E-Mail den ganzen HTML Body
   ///
   /// <seealso cref="MimeKit_JigTools.Get_Msg_Bodies"/>
   /// 
   /// </summary>
   /// <param name="msg"></param>
   /// <param name="ensure_Complete_HtmlDoc">
   /// Sicherstellen, dass der htmlPart ein vollständiges HTML-Dokument inkl. Head & Body HTML Elemente
   /// </param>
   /// <returns></returns>
   public static string? Get_MsgHtmlBody_Complete(
      MimeMessage msg
      , bool      ensure_Complete_HtmlDoc) {
      var visitor = new HtmlPreviewVisitor();
      msg.Accept(visitor);

      //+ Sicherstellen, dass der htmlPart ein vollständiges HTML-Dokument inkl. Head & Body ist?
      if (ensure_Complete_HtmlDoc) {
         var htmlDoc = HtmlTools.Ensure_Complete_HtmlDoc
            (ConfigMail_MsgHtmlBody.Default_HtmlDoc_Structure, visitor.HtmlBody);

         return htmlDoc.DocumentNode.OuterHtml;
      }
      else {
         return visitor.HtmlBody;
      }
   }


   /// <summary>
   /// Holt von einer E-Mail den HTML Body
   ///
   /// <seealso cref="MimeKit_JigTools.Get_Msg_Bodies"/>
   /// 
   /// </summary>
   /// <param name="msg"></param>
   /// <returns></returns>
   public static string Get_MsgHtmlBody_BodyPartOnly(
      MimeMessage msg) {
      var htmlDoc = new HtmlDocument();
      // ! Forciert immer die Erstellung einer kompleten HTML-Struktur,
      // weil der E-Mail HTML Textpart nicht immer ein <Body> Element hat
      // und deshalb diese Funktion scheitern würde
      htmlDoc.LoadHtml(Get_MsgHtmlBody_Complete(msg, true));

      //+ Die Body Node suchen 
      var htmlBody = htmlDoc.DocumentNode.SelectSingleNode("//body");

      return htmlBody.OuterHtml;
   }

   #endregion Hilfsfunktionen

}
