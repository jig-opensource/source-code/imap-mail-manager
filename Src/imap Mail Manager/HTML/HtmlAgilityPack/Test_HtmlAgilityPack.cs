using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using HtmlAgilityPack;

using ImapMailManager.HTML;
using jig.Tools.HTML;
using jig.Tools.Lib.Jo_CSS_Parser.Parser;


namespace ImapMailManager.HTML.HtmlAgilityPack;

public class Test_HtmlAgilityPack {

   public static async Task TestAddNodeBeforeBodyEndTag() {
      var htmlOri =
         @"<body>
            <h1>This is <b>bold</b> heading</h1>
            <p>This is <u>underlined</u> paragraph</p>
        </body>";

      var htmlsStyle =
         @"<style>
            .classDemo{
              color:orange;
              font-size:25px;
            }
            </style>
         ";

      var htmlsStyleInner =
         @".classDemo{
              color:orange;
              font-size:25px;
            }
         ";

      var stopper0 = 1;

      CssParser cssParser1 = new CssParser();
      cssParser1.Css = htmlsStyleInner;
      var hasTag1 = cssParser1.TagExist(".classDemo");
      
      Debug.WriteLine(cssParser1.Css);
      
      CssParser cssParser2 = new CssParser();
      cssParser2.Css = htmlsStyle;
      var hasTag2 = cssParser1.TagExist(".classDemo");
      Debug.WriteLine(cssParser2.Css);
      
      

      var stopper3 = 1;


      // ISelector selector = cssParser.ParseSelector(htmlsStyleInner);
      //ISelector selector = cssParser.ParseSelector(htmlsStyle);
      //ISelector selector = cssParser.

      var stoppetx = 1;


      var stopper32 = 1;

      var htmlOhne =
         @"<!DOCTYPE html>
            <html>
            <head>
            <title>
            Class demo
            </title>
            <style>
               .classDemo{
                  color:orange;
                  font-size:25px;
               }
            </style>
            </head>
            <body style=""text-align:center"">
               <h1>Get element by class</h1>
               <p class=""classDemo"">Demo for class selector</p>
             </body>
            </html>";

      var htmlMit =
         @"<!DOCTYPE html>
            <html>
            <head>
            <title>
            Class demo
            </title>
            <style>
               .classDemo{
                  color:orange;
                  font-size:25px;
               }
            </style>
            <style>
               .jigTag { color: red; }
               .jigHidden { display:none; }
            </style>
            </head>
            <body style=""text-align:center"">
               <h1>Get element by class</h1>
               <p class=""classDemo"">Demo for class selector</p>
             </body>
            </html>";

      var htmlDocMit = new HtmlDocument();
      htmlDocMit.LoadHtml(htmlMit);

      var htmlDocOhne = new HtmlDocument();
      htmlDocOhne.LoadHtml(htmlOhne);

      var htmlBody = htmlDocMit.DocumentNode.SelectSingleNode("//body");
      HtmlTools.DisplayNode(htmlBody);

      var htmlhead1 = htmlDocMit.DocumentNode.SelectNodes("/head/style");
      var htmlhead2 = htmlDocMit.DocumentNode.SelectNodes("//head/style");

      var stopper = 1;

      HtmlNode newChild = HtmlNode.CreateNode("<h1> This is added at the beginning</h>");
      htmlBody.PrependChild(newChild);

      Debug.WriteLine("\n****After prepending child node****\n");
      HtmlTools.DisplayNode(htmlBody);

      //+ Funktioniert: Fügt die Node als letztes Element hinzu 
      HtmlNode newChildEnd = HtmlNode.CreateNode("<h1>The End</h>");
      htmlBody.AppendChild(newChildEnd);
      HtmlTools.DisplayNode(htmlBody);

      var stopper1 = 1;
   }



}
