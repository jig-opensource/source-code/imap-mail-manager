﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Hierarchy;
using ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper.StatsMbxAntwortenVsNeueFragen;
using ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper.StatsMbxFolder;
using ImapMailManager.GoogleAPI.UsingGoogleSheetsWrapper.StatsMbxOffeneFragenProWK;
using ImapMailManager.Mail;
using ImapMailManager.Mail.Hierarchy;
using ImapMailManager.Mail.Stats;
using ImapMailManager.Test.Hierarchy;
using jig.WPF.SerilogViewer;
using Serilog.Events;
using ImapMailManager.Config;
using ImapMailManager.GoogleAPI.TomTools;
using ImapMailManager.HTML.HtmlAgilityPack;
using ImapMailManager.Libs.SQLite;
using ImapMailManager.Libs.Text_Spracherkennung.Catalyst;
using ImapMailManager.Libs.Text_Spracherkennung.NTextCat;
using ImapMailManager.Mail.MimeKit;
using ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator;
using ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator.CallbackFuncs;
using ImapMailManager.Mail.TestTools;
using ImapMailManager.Properties;
using jig.Tools;
using jig.Tools.Exception;
using jig.Tools.String;
using jig.WPF;
using MailKit;
using MailKit.Search;
using Microsoft.VisualStudio.Threading;


namespace ImapMailManager.UI {

   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : Window {

      #region Fields

      private readonly SerilogViewer _serilogViewer = new();

      #endregion

      #region Constructors and Destructors

      // private static WinSerilogViewer _winSerilogViewer = new();


      public MainWindow(
         Sensitive_AppSettings.CfgImapServer       cfgImapServers
         , Sensitive_AppSettings.CfgImapServerCred cfgImapServerCredAktiv) {
         InitializeComponent();

         // ToDo 🟥 Dok

         AppCfg_ImapServer.ItemsSource = cfgImapServers.Creds;

         //+ Das aktive Element suchen
         AppCfg_ImapServer.SelectedIndex = 0;

         for (var idxCfg = 0; idxCfg < AppCfg_ImapServer.Items.Count; idxCfg++) {
            Sensitive_AppSettings.CfgImapServerCred? thisCfg
               = AppCfg_ImapServer.Items[idxCfg] as Sensitive_AppSettings.CfgImapServerCred;

            if (thisCfg.CredKey == cfgImapServerCredAktiv.CredKey) {
               AppCfg_ImapServer.SelectedIndex = idxCfg;

               break;
            }
         }

         //+ Den Titel setzen
         FrameworkElement? childByName = WPF_JitTools.FindChildByName(MainWindowGrid, "LblMaiboxInfo");

         if (childByName is Label lblMaiboxInfo) {
            lblMaiboxInfo.Content =
               $"{RuntimeConfig_MailServer.CfgImapServerRl.SrvConfigName}: {RuntimeConfig_MailServer.CfgImapServerCredAktiv.UserName}";
         }

         //+ Die Panel Hintergrundfarbe setzen, wenn Prod
         if (cfgImapServerCredAktiv.IsProductiveMailbox) {
            Background = Brushes.OrangeRed;
         }

         // Beim Decodieren einer Nachricht:
         // System.NotSupportedException
         //	 No data is available for encoding 10000. 
         //	 For information on defining a custom encoding,
         //	 see the documentation for the Encoding.RegisterProvider method.

         var codePages = CodePagesEncodingProvider.Instance;
         Encoding.RegisterProvider(codePages);
      }

      #endregion

      #region Tastks, Async

      public static async Task myJoinableTaskAsync() {
         JoinableTaskFactory joinableTaskFactory = new JoinableTaskFactory(new JoinableTaskContext());

         // hier weiter mit testen

         joinableTaskFactory.Run
            (async () => {
                await MethodReturningVoidTaskAsync();

                //await SomeOtherAsyncMethod();
                //example
                await GenerateAndExportMailboxStatsToGSheets();
             });
      }


      static async Task MethodReturningVoidTaskAsync() {
         await Task.Run
            (() => {
                /* Do some work here... */
                Thread.Sleep(4_000);

                // throw new Exception("Something bad happened");
             });
         Console.WriteLine("Void method completed");
      }


      static async Task GenerateAndExportMailboxStatsToGSheets() {
         var tasks = new List<Task> {
            Task.Run
               (() => {
                   Debug.WriteLine("1 - Start: GSheets_MbxFldr_Stats_BusinessLogic");

                   //+ Die Mailbox Order Statistik erzeugen und in GSheets publizieren
                   GSheets_MbxFldr_Stats_BusinessLogic.PublishStats
                      (RuntimeConfig_MailServer.CfgImapServerRl
                       , RuntimeConfig_MailServer.CfgImapServerCredAktiv
                       , false
                       , null
                       , AppCfgRuntime.ImapMbxDataRootNode);

                   Debug.WriteLine("1 - Done : GSheets_MbxFldr_Stats_BusinessLogic");
                   Thread.Sleep(2_000);
                })
            , Task.Run
               (() => {
                   Debug.WriteLine("2 - Start: GSheets_Mbx_OffeneFragenProWK_Stats_BusinessLogic");

                   // Die statistischen Daten berechnen und exportieren
                   GSheets_Mbx_OffeneFragenProWK_Stats_BusinessLogic.PublishStats
                      (RuntimeConfig_MailServer.CfgImapServerRl
                       , RuntimeConfig_MailServer.CfgImapServerCredAktiv
                       , false
                       , null
                       , AppCfgRuntime.ImapMbxDataRootNode);

                   Debug.WriteLine("2 - Done : GSheets_Mbx_OffeneFragenProWK_Stats_BusinessLogic");
                   Thread.Sleep(2_000);
                })
            , Task.Run
               (() => {
                   Debug.WriteLine("3- Start: GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic");

                   // Die statistischen Daten berechnen und exportieren
                   GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic.PublishStats
                      (RuntimeConfig_MailServer.CfgImapServerRl
                       , RuntimeConfig_MailServer.CfgImapServerCredAktiv
                       , false
                       , null
                       , AppCfgRuntime.ImapMbxDataRootNode);

                   Debug.WriteLine("3- Done : GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic");
                   Thread.Sleep(2_000);
                })
         };

         //+ Die Tasks einzeln starten
         // await Task.WhenAll(tasks);
         // await Task.WhenAny(tasks);

         foreach (var task in tasks) {
            Debug.WriteLine($"Start: Task #{task.Id}");
            await task;
            Debug.WriteLine($" Done: Task #{task.Id}");
         }

         int stopper = 1;
      }

      #endregion Tastks, Async

      #region Internal Methods

      private void BtnAnalyzeMailboxMessageID_Click(object sender, RoutedEventArgs e)
         => ReadImapMailbox.AnalyzeMailboxMessageID
            (RuntimeConfig_MailServer
               .CfgImapServerRl
             , RuntimeConfig_MailServer
               .CfgImapServerCredAktiv
             , true
             , _serilogViewer);


      private void BtnAnalyzeMailRefNr_Click(object sender, RoutedEventArgs e)
         => ReadImapMailbox.AnalyzeMailboxMailRefNr
            (RuntimeConfig_MailServer
               .CfgImapServerRl
             , RuntimeConfig_MailServer
               .CfgImapServerCredAktiv
             , true
             , _serilogViewer);


      private void BtnDBInit_Click(object sender, RoutedEventArgs e) {
         // Das verzeichnis, in dem das exe ist
         Debug.WriteLine(AppDomain.CurrentDomain.BaseDirectory);
         var db = new SQLite_Init();

         // Automatisch erledigt:
         // db.InitDB();
         var dfdf = 1;
      }


      private void BtnHierarchTest_Click(object sender, RoutedEventArgs e) { TestHierarchy.Test(); }


      private void BtnImap_Click(object sender, RoutedEventArgs e) => ReadImapMailbox.TestIt
         (RuntimeConfig_MailServer.CfgImapServerRl
          , RuntimeConfig_MailServer.CfgImapServerCredAktiv
          , _serilogViewer);


      /// <summary>
      /// Liest die IMAP Mailbox in einen Baum
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private async void BtnMailboxHierarchy_Click(object sender, RoutedEventArgs e) {
         // Klappt, Quelle der Sprach-Modelle ist noch rätselhaft
         // Catalyst_JigTools.Tester();

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Print_MsgHeader_MailSprache()
             , true
             , _serilogViewer);

         int sd11s113131fsd = 1;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Test_BO_Mail()
             , true
             , _serilogViewer);

         int sd11s11fsd = 1;

         return;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer
               .CfgImapServerCredAktiv
             , new Analyze_Headers_in_MimeMessage_MimeMessageBody_IMessageSummary()
             , true
             , _serilogViewer);

         int sd1111fsd = 1;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Print_PersönlicheDaten_InMsgProperties()
             , true
             , _serilogViewer);

         int sd1fsd = 1;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Print_PersönlicheDaten_ImHeader()
             , true
             , _serilogViewer);

         int sdfsd = 1;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new AnalyzePersonenDatenFragesteller()
             , true
             , _serilogViewer);

         int sddffsfsd = 1;

         TestPostfach_AllMails_KontaktformularParser.StartTest
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer
               .CfgImapServerCredAktiv
             , _serilogViewer);

         //+ Die Mailbox lesen
         var rootNode =
            ReadImapMailbox.ReadMailbox_CreateHierarchy
               (RuntimeConfig_MailServer.CfgImapServerRl
                , RuntimeConfig_MailServer.CfgImapServerCredAktiv
                , false
                , _serilogViewer);

         // ‼ 🚩 Debug
         //+ Eine Mail suchen
         var betreff = "Re: #3-0735 • Seelsorge: Wäre ein Online-Gespräch möglich?";

         //+ Jedes Element durchlaufen
         foreach (var item in rootNode.DescendantNodes(TraversalType.DepthFirst)) {
            var xxx = 1;

            if (item.Data.ImapPersonalNamespaceNde != null) {}

            if (item.Data.ImapFolderNde != null) {}

            //++ Die Formular-Ref.Nr suchen
            List<string> matches   = new();
            List<string> noMatches = new();

            if (item.Data.ImapMsgNde != null) {
               //++ Wir haben eine E-Mail Node
               var msg = item.Data.ImapMsgNde;

               //+ Haben wir die Mail mit unserem Betreff gefunden?
               if (msg.Subject.Equals(betreff, StringComparison.OrdinalIgnoreCase)) {
                  //  new  new int stopper = 1;

                  //++ Den IMAP Ordner holen
                  ImapFolderNde folder = item.Parent.Data.ImapFolderNde!;

                  // Sortieren: zuerst die neuste E-Mail
                  // !Q https://stackoverflow.com/a/58580564/4795779						
                  // var messages = imapFolder.Fetch
                  //    (0, -1, MessageSummaryItems.UniqueId | MessageSummaryItems.InternalDate).ToList();
                  // messages.Sort(new OrderBy[] { OrderBy.ReverseDate });

                  //++ Die Msg abrufen
                  var (imapClient, workingImapFolder, thisMsg, uniqueId) = ReadImapMailbox.Fetch_Folder_MessageSummaries
                     (
                      RuntimeConfig_MailServer.CfgImapServerRl
                      , RuntimeConfig_MailServer.CfgImapServerCredAktiv
                      , folder.oImapFolder
                      , msg.EnvelopeMessageId!);

                  //+ Den Msg Status aktualisieren
                  var msgChanged = false;

                  //++ Den Msg Header
                  BL_Mail_MsgHeader.Update_MailMsg_Headers(folder.oImapFolder, thisMsg)
                                   .IfTrue(() => msgChanged = true);

                  //++ Den Msg Body
                  // BL_Mail_Body.Update_MailMsg_Body(folder.oImapFolder, thisMsg)
                  //             .IfTrue(() => msgChanged = true);

                  // BL_Mail_Body.AddOrUpdate_MailMsgBody_TriageHashTags(folder.oImapFolder, thisMsg);

                  if (msgChanged) {
                     //+ Die Msg wieder speichern
                     ModifyImapEMail.SaveMessage
                        (imapClient
                         , workingImapFolder
                         , uniqueId
                         , thisMsg
                         , true);
                  }

                  var sto1pp5er = 2;
               }

               /*
               // Hat der Betreff eine RefNr?
               var matches1 = BL_Mail_Common.oRgxRefNr.Match(msg.SubjectNormalized);

               if (matches1.Success) {
                  // Die RefNr in der macthes-Liste erfassen
                  var refNr = matches1.Groups["RefNr"].Value;
                  matches.Add(refNr);
               }
               else {
                  //++ Wir haben keine RefNr
                  // Die RefNr in der macthes-Liste erfassen
                  if (msg.Subject.ØHasValue()) {
                     // Der Betreff hat einen Wert
                     if (msg.Subject.Contains("#")) {
                        // Der Betref hat ein #
                        // Wenn die noMatches-Liste den Betreff noch nicht kennt
                        if (!noMatches.Contains(msg.Subject)) {
                           // Die noMatches-Liste ergänzen
                           Debug.WriteLine(msg.Subject);
                           noMatches.Add(msg.Subject);
                        }
                     }
                  }
               }
               */

               var dsfdsfs = 1;
            }
         }
      }


      /// <summary>
      /// Liest eine IMAP E-Mail,
      /// verändert sie
      /// und ersetzt sie auf dem Server
      /// - Wie funktioniert es?
      /// - Wir die Message-ID verändert?
      /// <trinity-18e4db9c-7b45-4504-98e7-f10c67506060-1671880967016@ msvc-mesg-gmx104>
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void BtnModifyImapEmail_Click(object sender, RoutedEventArgs e) {
         // ToDo: 🟥 Löschen
      }


      private void BtnReadGoogleSheetData_Click(object sender, RoutedEventArgs e)
         => GoogleSheetApi.Test_DoIt();


      /// <summary>
      /// Liest das ganze Postfach
      /// Sucht eine gewünschte E-Mail
      /// Gibt alle Header-Infos aus
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void BtnReadMailboxMailHeader_Click(object sender, RoutedEventArgs e) {
         //+ Alle Mails lesen
         var rootNode =
            ReadImapMailbox.ReadMailbox_CreateHierarchy
               (RuntimeConfig_MailServer.CfgImapServerRl
                , RuntimeConfig_MailServer.CfgImapServerCredAktiv
                , true
                , _serilogViewer);

         //+ Alle Mail-Elemente sammeln
         var allMails = rootNode.DescendantNodes(TraversalType.DepthFirst)
                                .Where(x => x.Data.ImapMsgNde != null)
                                .Select(x => x.Data.ImapMsgNde);

         //+ Die gesuchte Mail suchen
         var srchBetreff = "Test 221228 150143";
         srchBetreff = "RE: Test 221228 150143";

         var myMail = allMails.Where(x => x.Subject.Equals(srchBetreff, StringComparison.OrdinalIgnoreCase))
                              .FirstOrDefault();

         //+ Den Header ausgeben
         if (myMail == null) {
            var error = 1;
         }
         else {
            foreach (var item in myMail.MailHeaders) {
               Debug.WriteLine($"{item.Field}: {item.Value}");
            }
         }
      }


      private void BtnSerilogTest_Click(object sender, RoutedEventArgs e)
         => _serilogViewer.log.Write(LogEventLevel.Error, "Tester");


      /// <summary>
      /// Liest die ganze Mailbox
      /// Erzeugt die statistischen Daten
      /// Überträgt die Daten zu GSheet
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void Btn_UpdateGSheet_Imap_Mailbox_Stats_Click(object sender, RoutedEventArgs e) {
         //+ Config

         // Die statistischen Daten berechnen und exportieren
         GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic.PublishStats
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , false
             , null
             , null);

         // todo Löschen:
         /*         
                  // Ab welchem Datum aktualsieren wir rückwirkend bestehende Daten in GSheets?
                  // Berechnung:
                  // +1 Woche zusätzlich minus 1 Tag, um die gewünschten KW rückwirkend sicher abzudecken
                  var rückwirkendeUpdatesStartDatum = DateTime.Now.AddDays((updateRückwirkendAnzWeeks + 1)*7 - 1);
         
                  //+ Die Mailbox-Daten lesen
                  var mbxStats = CalcImapStats.Calc_AntwortenVsNeueFragen_Stats(
                                                                               MailConfig.CfgImapServerRL_Neu, MailConfig.CfgImapServerCredAktiv, false
                                                                               , null);
         
                  
                  //+ Bestehende Daten aktualisieren
                  // Die Liste mbxStats wird verändert!
                  GoogleSheetMbxStatistics.UpdateMailboxStatistics(mbxStats, rückwirkendeUpdatesStartDatum);
         
                  //++ Neue Daten ergänzen
                  // Die erledigten Daten entfernen
                  var neueDaten = mbxStats.Where(x => x.IsExportedToGoogleSheets == false);
         
                  // Die Daten ergänzen
                  GoogleSheetMbxStatistics.AppendMailboxStatistics(neueDaten);
         */
      }


      private void ExportMailboxItemsToExcel_Click(object sender, RoutedEventArgs e) {
         //+ In Excel exportieren
         // MyExcelTools.ExportMailboxItemsToExcel(RootNode);
      }


      private void MainWindow_OnClosed(object? sender, EventArgs e) => _serilogViewer.Close();

      private void OpenLogView2_OnClick(object sender, RoutedEventArgs e) => _serilogViewer.Show();

      #endregion


      /// <summary>
      /// Liest die Mailbox-Statistik
      /// und exportiert die Informationen mitden Ordner-Stats 
      /// zu GSheets
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void Btn_GSheets_Export_MbxFldr_Stats_Click(object sender, RoutedEventArgs e) {
         //+ Die Mailbox Order Statistik erzeugen und in GSheets publizieren
         GSheets_MbxFldr_Stats_BusinessLogic.PublishStats
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , false
             , null
             , null);
      }


      /// <summary>
      /// Liest die Mailbox-Hierarchie und analysiert von im Sent-Ordner alle Absender-E-Mail-Adressen Domänen
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      /// <exception cref="NotImplementedException"></exception>
      private void BtnAnalyze_SentFldr_SenderDomain_Click(object sender, RoutedEventArgs e) {
         int zzz = 1;

         var res = CalcImapStats.Analyze_GesendetFolder_SenderDomain
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , false
             , _serilogViewer);

         int ddd = 1;
      }


      private void BtnGSheets_Export_MbxFragenUndAntworten_Stats_Click(
         object            sender
         , RoutedEventArgs e) {
         // Die statistischen Daten berechnen und exportieren
         GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic.PublishStats
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , false
             , null
             , null);
      }


      private void BtnGSheets_Export_MbxFragenProKW_Stats_Click(object sender, RoutedEventArgs e) {
         // Die statistischen Daten berechnen und exportieren
         GSheets_Mbx_OffeneFragenProWK_Stats_BusinessLogic.PublishStats
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , false
             , null
             , null);
      }


      private void Btn_MbxStats_ReadAllData_Click(object sender, RoutedEventArgs e) {
         // Alle Mailbox-Daten lesen
         var rootNode = ReadImapMailbox.ReadMailbox_CreateHierarchy
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , false
             , null);

         // Btn_GSheets_Export_All_MbxStats.IsEnabled = true;
      }


      private void Btn_MbxStats_ExportAllDataToGSheets_Click(object sender, RoutedEventArgs e) {
         //+ Die Mailbox Order Statistik erzeugen und in GSheets publizieren
         GSheets_MbxFldr_Stats_BusinessLogic.PublishStats
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , false
             , null
             , AppCfgRuntime.ImapMbxDataRootNode);

         // Die statistischen Daten berechnen und exportieren
         GSheets_Mbx_OffeneFragenProWK_Stats_BusinessLogic.PublishStats
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , false
             , null
             , AppCfgRuntime.ImapMbxDataRootNode);

         // Die statistischen Daten berechnen und exportieren
         GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic.PublishStats
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , false
             , null
             , AppCfgRuntime.ImapMbxDataRootNode);
      }


      private void Btn_GSheets_Export_All_MbxStats_Click(object sender, RoutedEventArgs e) {
         int wdsffs = 1;

         throw new NotImplementedException();
      }


      /// <summary>
      /// Liest die Mailbox-Hierarchie und exportiert die analysierten Daten ins Excel,
      /// damit Klassifizierungs- / Kategorie-Fehler besser analysiert werden können 
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      /// <exception cref="NotImplementedException"></exception>
      private void Btn_Export_Mailbox_Hierarchy_ToExcel_Click(object sender, RoutedEventArgs e) {
         //+ Config
         string excelFilename = @"c:\Temp\¦\MailboxHierarchyDetails.xls";

         // Die statistischen Daten berechnen und exportieren
         GSheets_MbxAntwortenVsNeueFragen_Stats_BusinessLogic.PublishStats
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , false
             , null
             , null
             , excelFilename);
      }


      private void Btn_Mbx_Analyze_MsgSent_SenderDomain_Click(object sender, RoutedEventArgs e) {
         throw new NotImplementedException();
      }


      private async void Btn_Test_ClickAsync(object sender, RoutedEventArgs e) {
         await GenerateAndExportMailboxStatsToGSheets();
         int aaa = 1;
      }


      private void HtmlAgilityPackTest_Click(object sender, RoutedEventArgs e) {
         // ParseCSS.Parse_CSS(teser);

         int sss = 1;

         Test_HtmlAgilityPack.TestAddNodeBeforeBodyEndTag();
         var stopper = 1;
      }


      /// <summary>
      /// Liest die ganze Mailbox una aktualisiert jede E-Mail mit Metadaten
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void Btn_Process_AllMails_Update_MetaData_Click(object sender, RoutedEventArgs e) {
         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Process_AllMails_Update_MetaData()
             , true
             , _serilogViewer);

         int stopper = 1;
      }


      private void Btn_Process_AllMails_AnonymizeMails_Click(object sender, RoutedEventArgs e) {
         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Process_AllMails_Anonymize_Mails()
             , true
             , _serilogViewer);

         int stopper = 1;
      }


      /// <summary>
      /// ‼ Löscht alle E-Mails einer Mailbox
      /// ‼ Die WarningException muss deshalb entfernt werden 
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      /// <exception cref="WarningException"></exception>
      private void Btn_Process_AllMails_Delete_AllMails_Click(object sender, RoutedEventArgs e) {
         int stopper = 1;

         //‼ Sicherstellen, dass diese Aktion authentifiziert ist!
         // throw new WarningException("Vorsicht!");
         if (ChkbxIsAuthenticated.IsChecked == null || !ChkbxIsAuthenticated.IsChecked.Value) {
            var b = TxtAuthenticator.Background;
            TxtAuthenticator.Background = new SolidColorBrush(Colors.Orange);
            TxtAuthenticator.Focus();

            return;
         }

         stopper = 2;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Process_AllMails_DELETE_AllMails()
             , true
             , _serilogViewer);

         stopper = 3;
      }


      private void Btn_Process_AllMails_Print_Mail_Subjects_Click(object sender, RoutedEventArgs e) {
         int stopper1 = 1;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Process_AllMails_Print_Mail_Subjects()
             , true
             , _serilogViewer);

         int stopper = 1;
      }


      /// <summary>
      /// Logik, damit der User ein Passwort abtippt
      /// und so eine gefährliche Funktion nicht dummerweise ausführt
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void TxtAuthenticator_KeyDown(object sender, System.Windows.Input.KeyEventArgs e) {
         if (e.Key == System.Windows.Input.Key.Return) {
            var validate = TxtAuthenticator.Text;
            ChkbxIsAuthenticated.IsChecked = validate != null && validate.Equals(LblAuthenticationPassword.Content);

            if (ChkbxIsAuthenticated.IsChecked.Value) {
               // Wenn authenticated, den Hintergrund setzen
               // TxtAuthenticator.Background = new SolidColorBrush(Color.FromArgb(0xff, 0xff, 0xff, 0xff));
               TxtAuthenticator.Background = new SolidColorBrush(Colors.LightGreen);
            }
         }
      }


      private void TxtAuthenticator_GotFocus(object sender, RoutedEventArgs e) {}


      private void Btn_Process_AllMails_Analyze_MessageThreader_Click(object sender, RoutedEventArgs e) {
         int stopper1 = 1;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Process_AllMails_Get_MessageThreads()
             , true
             , _serilogViewer);

         int stopper = 1;
      }


      private void Btn_Process_AllMails_Get_Mailbox_Statistics_Click(object sender, RoutedEventArgs e) {
         int stopper1 = 1;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Process_AllMails_Analyze_Mailbox_Statistics()
             , true
             , _serilogViewer);

         int stopper = 1;
      }


      private void Btn_Process_AllMails_UpdateRecievedDate_ForDocMails_Click(object sender, RoutedEventArgs e) {
         int stopper1 = 1;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Process_AllMails_UpdateRecievedDate_ForDocMails()
             , true
             , _serilogViewer);

         int stopper = 1;
      }


      /// <summary>
      /// Setzt bei allen Triagierten E-Mails den Status auf ungelesen
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      private void Btn_Process_AllMails__Mark_Mails_Triaged_AsUnread_Click(object sender, RoutedEventArgs e) {
         int stopper1 = 1;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Process_AllMails_Set_MsgRead_Flag()
             , true
             , _serilogViewer);

         int stopper = 1;
      }


      private void Btn_Process_AllMails_Cleanup_MailSubject_Click(object sender, RoutedEventArgs e) {
         int stopper1 = 1;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Process_AllMails_Cleanup_Subject()
             , true
             , _serilogViewer);

         int stopper = 1;
      }


      /// <summary>
      /// Die IMAP Server config wurde gewechselt
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
      /// <exception cref="NotImplementedException"></exception>
      private void AppCfg_ImapServer_OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
         Sensitive_AppSettings.CfgImapServerCred? newImapServerCred
            = ((sender as ComboBox)!).SelectedItem as Sensitive_AppSettings.CfgImapServerCred;

         RuntimeConfig_MailServer.CfgImapServerCredAktiv = newImapServerCred;

         int stopper = 1;
      }


      private void Btn_AnalyzeMbx_Collect_MailData_Click(object sender, RoutedEventArgs e) {
         int stopper1 = 1;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new AnalyzeMbx_Collect_MailData()
             , true
             , _serilogViewer);

         int stopper = 1;
      }


      private void Btn_Process_AllMails_Detect_AntwortAuthor_Click(object sender, RoutedEventArgs e) {
         int stopper1 = 1;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Process_AllMails_Detect_AntwortAuthor()
             , true
             , _serilogViewer);

         int stopper = 1;
      }


      private void Btn_Debug_Test_MailKit_SearchQuery_Click(object sender, RoutedEventArgs e) {
         int stopper1 = 1;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Debug_Test_MailKit_SearchQuery()
             , true
             , _serilogViewer);

         int stopper = 1;
      }


      private void Btn_Process_AllMails_EMail_Klassifizierung_Click(object sender, RoutedEventArgs e) {
         int stopper1 = 1;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Process_AllMails_EMail_Kategorisierung()
             , true
             , _serilogViewer);

         int stopper = 1;
      }


      private void Btn_Debug_Generic_Mailbox_Iterator_Click(object sender, RoutedEventArgs e) {
         int stopper1 = 1;

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , new Debug_Generic_Mailbox_Iterator()
             , true
             , _serilogViewer);

         int stopper = 1;
      }


      private void Btn_Mailbox_Iterator_AdHoc_Click(object sender, RoutedEventArgs e) {

         var selectedType = "Process_AllMails_Info_MbxFolders";

         var app_Types_ImplementingInterface = ExtensionMethods_Reflection.GetApp_Types_ImplementingInterface
            (typeof(IMailboxIteratorAdHoc));
         var myType = app_Types_ImplementingInterface.Where(x => x.Name.ØEqualsIgnoreCase(selectedType));

         if (myType.Count() > 1) {
            throw new RuntimeException("Btn_Mailbox_Iterator_AdHoc_Click() #1");
         }

         var mailboxIterator = Activator.CreateInstance(myType.First());

         if (mailboxIterator is not IMailboxIterator) {
            throw new RuntimeException("Btn_Mailbox_Iterator_AdHoc_Click() #2");
         }

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , (mailboxIterator as IMailboxIterator)!
             , true
             , _serilogViewer);

         int stopper = 1;
      }


      private void Btn_AdHoc_Code_Click(object sender, RoutedEventArgs e) {
         // Alle Ordner durchlaufen
         var folderFilter = new FolderFilter {

            MbxFolder_Whitelist = new
               (IMailbox_FolderFilter.FolderCompare.RgxMatch
                , new List<string> {
                   /*                   
                                      ExtensionMethods_RegEx.ØWildcardToRegex(@"99 Tests Thomas ★*".Replace('\\', '/'))
                                      , ExtensionMethods_RegEx.ØWildcardToRegex(@"Sent".Replace('\\', '/'))
                                      , ExtensionMethods_RegEx.ØWildcardToRegex(@"Gesendet".Replace('\\', '/'))
                                      , ExtensionMethods_RegEx.ØWildcardToRegex(@"Gesendete Elemente".Replace('\\', '/'))
                   */
                })
            , MbxFolder_Whitelist_Except_StopOn = new
               (IMailbox_FolderFilter.FolderCompare.RgxMatch
                , new List<string> {
                   ExtensionMethods_RegEx.ØWildcardToRegex(@"99 Tests Thomas ★\*".Replace('\\', '/'))
                })
         };

         //+ Von der Mailbox mit allen Mails die Threads bestimmen 
         Process_AllMails_Get_MessageThreads mailboxIterator = new Process_AllMails_Get_MessageThreads(folderFilter) {
            MailboxIterator_Dont_ProcessDone = true
         };

         Main_MailboxIterator.IterateMailbopxFolders
            (RuntimeConfig_MailServer.CfgImapServerRl
             , RuntimeConfig_MailServer.CfgImapServerCredAktiv
             , mailboxIterator
             , true
             , _serilogViewer);


         var msgMainId = "f8a913238432711c966d8d9405e77e6b@rogerliebi.ch";
         var msgMainId1 = "a038c6d9591e02b25a8753b57019483b@rogerliebi.ch";
         var msgMainId2 = "fe654c13af5ae03c9e0fac846f9cf1a8@rogerliebi.ch";


         var msgMainThread = MailKit_JigTools_MessageThread.GetMessageThread_ForMsg(mailboxIterator.AllMsgSummary, msgMainId);
         var msgMainThread1 = MailKit_JigTools_MessageThread.GetMessageThread_ForMsg(mailboxIterator.AllMsgSummary, msgMainId1);
         var msgMainThread2 = MailKit_JigTools_MessageThread.GetMessageThread_ForMsg(mailboxIterator.AllMsgSummary, msgMainId2);

         var mainMsgs = MailKit_JigTools_MessageThread.GetMessageThread_Messages(msgMainThread.First());
         var mainMsgs1 = MailKit_JigTools_MessageThread.GetMessageThread_Messages(msgMainThread1.First());
         var mainMsgs2 = MailKit_JigTools_MessageThread.GetMessageThread_Messages(msgMainThread2.First());
         
         int stopper = 1;

      }

   }

}
