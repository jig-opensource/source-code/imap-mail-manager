:toc:

== ToDo Beispiele

    // ToDo 🟥 Anonymisieren der E-Mail
    // ToDo     🟥 HtmlBody
    // ToDo     🟥 TextBody
    // ToDo     🟥 Headers
    

// ToDo Now 🟥 
    
// ToDo 🟥 E-Mails automatisch löschen / archivieren: Lesebestätigung:
// ToDo     🟥 … allenfalls nach z.B. 3 Tagen oder soabld sie von jemandem im Team gelesen wurde


// ToDo 🟥 MsgHeader: AppMailHeaderType.MailInhaltAnonymisiert
// ToDo 🟥 MsgHeader: AppMailHeaderType.SpracheMailInhalt

// ToDo 🟥 


    // ToDo 🚩 Debug test
    // 🚩 Debug


  - 🟥 Export GSheets
    -   🟥 Nur noch Delta-Updates für alle 3 Exporte
    -   🟥 … bzw. Full-Update per Knopfdruck

== Unicode Symbole
- https://www.compart.com/en/unicode/block/U+1F300
- https://www.compart.com/en/unicode/block/U+2700
- https://www.compart.com/en/unicode/category/So

🟥✅☑️
📌📍🚩
🔴🔵
🔶🔷
🔸🔹
🔺🔻
🛑
🟠🟡🟢🟣🟤
🟥🟦🟧🟨🟩🟪🟫
⭕
❌
❗
✔️


= ToDo Prjekt

 * 🟥 Übertragen der wichtigsen Msg Headers von der Frage auf die E-Mail mit der Antwort im Gesendet-Ordner
** 🟥 Mail-Threads lesen
*** 🟥 Alle Msg Summary aus allen Ordnern sammeln
*** 🟥 Die Msg Summary dem `MessageThreader` übergeben
*** 🟥 Die Msg Hdr übertragen:
**** 🟥 ConfigMail_MsgHeader.AppMailHeaderType: 
        Siehe: `ImapMailManager.Config.ConfigMail_MsgHeader.TranferHdrToOtherMailsInThread`



= Welche .Net Version

	.Net 7.x
		Infos siehe nachfolgend…
		
		!9 Problem
			.Net 7 hat breaking changes und ist nicht mehr rückwärtskompatibel!
			
			Das führt zu hässlichen FileNotFoundException,
				die aber eigentlich sagen, dass eine Library nicht kompatibel zu .Net 7 ist
				und deshalb kann die Lib nicht geladen werden
				und deshalb meldet M$ die FileNotFoundException

== .Net: Versionsnamen Albtraum

	Hilft vielleicht
	!Q https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/compiler-messages/feature-version-errors?f1url=%3FappId%3Droslyn%26k%3Dk(CS8370)

		Update your target framework
		The compiler determines a default based on these rules:

		Target framework	Version		C= language version default
		
		Ist vermutlich die neuste Namensgebung:
			.NET							7.x				C= 11
			.NET							6.x				C= 10
			.NET							5.x				C= 9.0
			
		Ist vermutlich die 4.x Reihe:
			.NET Framework		all				C= 7.3

		War wohl die Namensgebung dazwischen:
			.NET Core					3.x				C= 8.0
			.NET Core					2.x				C= 7.3
			.NET Standard			2.1				C= 8.0
			.NET Standard			2.0				C= 7.3
			.NET Standard			1.x				C= 7.3

== Visual Studio: .Net Framework Support

welches NS.Net unterstützt welches Framework?

	Visual Studio 2019
		!H https://learn.microsoft.com/en-us/visualstudio/releases/2019/compatibility
		
		Maximal .Net 5.x
		https://dotnet.microsoft.com/en-us/download/dotnet/5.0
		
		
	Visual Studio 2022
		Ab .Net 6.x
		https://dotnet.microsoft.com/en-us/download/dotnet/6.0

		https://dotnet.microsoft.com/en-us/download/dotnet/7.0


= !KH SWE
== !KH WPF & Console

=== No output to console from a WPF application?

- Gute Lösung:
  https://stackoverflow.com/a/160597/4795779

== Unit Tests

    <Reference Include="Microsoft.VisualStudio.TestPlatform.TestFramework, Version=14.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a, processorArchitecture=MSIL">
      <HintPath>..\..\imap Mail Manager\packages\MSTest.TestFramework.2.2.7\lib\net45\Microsoft.VisualStudio.TestPlatform.TestFramework.dll</HintPath>

Add new Project MSTest Test Project

	PrjName
		jig.Tools.Test

	Location
		c:\GitWork\GitLab.com\jig-Opensource\Source-Code\imap-Mail-Manager\Repo\Src\

			> Erzeugt das Unterverzeichnis jig.Tools.Test automatisch!

	Framework
		.Net 6



= NuGet
== String Interpolation, Auswertung

=== Fluid, template engine based on the Liquid template language

* Sehr gute Performance
* !H https://github.com/sebastienros/fluid#using-fluid-in-your-project
* !H https://www.nuget.org/packages/Fluid.Core
* `Install-Package Fluid.Core`




=== !8 System.Linq.Dynamic, os

Mühselig im Einsatz

* !H https://dynamic-linq.net/
* !H https://github.com/zzzprojects/System.Linq.Dynamic.Core
* !M https://dynamic-linq.net/overview
* !Ex https://dynamic-linq.net/online-examples
* Setup
** !M https://dynamic-linq.net/installation-nuget
** `Install-Package System.Linq.Dynamic.Core`

=== !0 C# Eval Expression, $500

* !H https://eval-expression.net/
* !H https://eval-expression.net/pricing
* !H https://www.nuget.org/packages/Z.Expressions.Eval/
* Install-Package Z.Expressions.Eval

== Regex
=== PCRE2 - Perl-compatible regular expressions

- Die RegEx scheinen mir logischer verständlich
- Funktioniert mit dem Debugger: https://regex101.com/

- !H https://www.pcre.org/
- !H https://www.pcre.org/pcre2.txt

=== PCRE.NET - Perl Compatible Regular Expressions for .NET

`Install-Package PCRE.NET`

-!H GitHub: https://github.com/ltrzesniewski/pcre-net
- Nuget: https://www.nuget.org/packages/PCRE.NET



==  CliFx, Class-first framework for building command-line interfaces

- !H https://github.com/Tyrrrz/CliFx
- !H https://www.nuget.org/packages/CliFx
   Install-Package CliFx


= Text: Erkennung der Sprache

== Catalyst

Catalyst is a C= Natural Language Processing library built for speed. 
Inspired by spaCy's design, it brings pre-trained models, 
out-of-the box support for training word and document embeddings, 
and flexible entity recognition models.

=== Getting started

1. Lib installieren
    Install-Package Catalyst

2. Sprach-Modell / -Paket(e) installieren
    !9 Die Sprach-Modell / -Pakete müssen für den  
       cld2LanguageDetector = await LanguageDetector
       vermutlich *nicht* installiert werden!,  
       er scheint auch ohne zu funktionieren.

    !9 Aber: Der
         fastTextLanguageDetector = await FastTextLanguageDetector.FromStoreAsync
       verlangt, dass
         Storage.Current = new DiskStorage(thisSubDirSprachmodelle);
       gesetzt wird, d.h. er braucht die Sprach-Modell 

       » Ich kriegte es aber nicht zum laufen
            https://github.com/curiosity-ai/catalyst/issues/88

3. Sprach-Modell / Pakete
   Install-Package Catalyst.Models.English
    > Wird hier installiert
      C:\Users\schittli\.nuget\packages\catalyst.models.english
    > Wird beim Build hierhin kopiert
      .\bin\Debug\net6.0-windows\Catalyst.Models.English.dll

- https://github.com/curiosity-ai/catalyst
- !Ex https://github.com/curiosity-ai/catalyst/blob/master/samples/LanguageDetection/Program.cs
- Sprachmodelle
  - https://www.nuget.org/packages?q=catalyst.models&sortBy=relevance

- Install-Package Catalyst



== NTextCat

- https://github.com/ivanakcheurov/ntextcat
- Install-Package NTextCat

== Detect Language API Client for .NET

- https://detectlanguage.com/
- !M https://detectlanguage.com/faq
- https://detectlanguage.com/plans
- https://github.com/detectlanguage/detectlanguage-dotnet
- Install-Package DetectLanguage

= Google API für Sheets

== !To GoogleSheetsWrapper

	!H GitHub
	https://github.com/SteveWinward/GoogleSheetsWrapper

	Nützliche Konverter wie:
		https://github.com/SteveWinward/GoogleSheetsWrapper/blob/main/src/GoogleSheetsWrapper/Utils/DateTimeUtils.cs

!Tut https://dottutorials.net/google-sheets-read-write-operations-dotnet-core-tutorial/=setting-up-google-sheet

== Cred: Credentials

developers.google.com@jig.ch

!H Cloud - Google Drive - Fragenbeantwortung
https://drive.google.com/drive/u/1/folders/1A9wMkTgdSPOdby5JDF04D2sNZh9fmWAv

Install-Package Google.Apis.Sheets

= Libs / NuGet Install-Package

== CRC, Hashing Algorithm

- https://github.com/brandondahler/Data.HashFunction/
- cityhash
  - https://github.com/google/cityhash
  - https://github.com/gmarz/city-hash-net
  - https://github.com/google/cityhash
- Tests
  - https://aras-p.info/blog/2016/08/09/More-Hash-Function-Tests/

=== xxHash

- !H https://cyan4973.github.io/xxHash/
- Performance: https://aras-p.info/blog/2016/08/09/More-Hash-Function-Tests/
- C
  - https://github.com/Cyan4973/xxHash
- C=
  - https://github.com/uranium62/xxHash
  - Install-Package Standart.Hash.xxHash    



== Configuration-Provider zum Speichern der Credentials in externen Files

    Install-Package Microsoft.Extensions.Configuration.Json

== Clipboard

    https://github.com/CopyText/TextCopy
    Install-Package TextCopy


== Office / Excel Lib

	NPOI
		!9 Vorsicht, ist nicht mit .Net 7 kompatibel!
			> https://github.com/nissl-lab/npoi/issues/982
	
		.Net 6 funktioniert
		
		Install-Package NPOI

Metalama
Postsharp Nachfolger
https://www.postsharp.net/metalama

https://github.com/jstedfast/MimeKit
http://www.mimekit.net/
Install-Package MimeKit

https://github.com/jstedfast/MailKit
MailKit is a cross-platform mail client library built on top of MimeKit.
Install-Package MailKit

https://github.com/jstedfast/EmailValidation
Install-Package EmailValidation

https://github.com/jstedfast/HtmlKit
Install-Package HtmlKit

= Log: Serilog

	Serilog
	https://serilog.net/
	Install-Package Serilog

== Serilog: Sinks

	https://github.com/umairsyed613/Serilog.Sinks.WPF
	Install-Package Serilog.Sinks.WPF

= WPF Custom Control Library vs User Control Library

UserControls are meant to
compose multiple WPF controls together,
in order to make a set of functionality built out of other controls

Deshalb heisst das Projekt so:
	jig.WPF.UserControlLib\

Update:
Weil jig.WPF nun auch Markup-Erweiterungen enthält, heisst es neu so:
	jig.WPF\



= WPF Tools

== HTML Editor, Parser

=== HTML Agility Pack

- https://html-agility-pack.net/
- Install-Package HtmlAgilityPack

=== AngleSharp.Css


== HTML CSS Parser

=== ExCSS
‼ Klappt *nicht* für html\head\<style> Tags
- https://github.com/TylerBrinks/ExCSS
- https://www.nuget.org/packages/excss/
- Install-Package ExCSS


=== AngleSharp

- Kann selber kein CSS Parsen
- https://www.nuget.org/packages/AngleSharp
- Install-Package AngleSharp
- https://anglesharp.github.io/
- !M https://anglesharp.github.io/docs/css/01-API
- !M https://github.com/AngleSharp/AngleSharp/blob/devel/docs/README.md
- https://anglesharp.github.io/docs/01-articles
- !M https://github.com/AngleSharp/AngleSharp/blob/main/docs/tutorials/01-API.md

!Tut https://zetcode.com/csharp/anglesharp-parse-html/

=== AngleSharp.Css

- Könnte CSS Parsen
- Der released Code hat aber Bugs, für die es noch keine Issues gibt
- https://github.com/AngleSharp/AngleSharp.Css

==== !Ex
    https://github.com/AngleSharp/AngleSharp.Css/blob/devel/src/AngleSharp.Css.Tests/Parsing/StyleSheet.cs





= WPF Tools

Meziantou.Framework.WPF
!Ex9 https://www.meziantou.net/thread-safe-observable-collection-in-dotnet.htm
!H        https://github.com/meziantou/Meziantou.Framework/tree/main/src/Meziantou.Framework.WPF

	Install-Package Meziantou.Framework.WPF

= Encoding.CodePages

Beim Decodieren einer Nachricht:
System.NotSupportedException
No data is available for encoding 10000.
For information on defining a custom encoding, see the documentation for the Encoding.RegisterProvider method.

	Install-Package System.Text.Encoding.CodePages

	Und dann:
		
		var codePages = CodePagesEncodingProvider.Instance;
		Encoding.RegisterProvider(codePages);

= Lib: Hierarchy

Install-Package Hierarchy


= Lib: Async, Threading

!M https://github.com/microsoft/vs-threading/blob/main/doc/cookbook_vs.md

	Install-Package Microsoft.VisualStudio.Threading
	
== Installing the analyzers
	Install-Package Microsoft.VisualStudio.Threading.Analyzers


It is highly recommended that you install the Microsoft.VisualStudio.Threading.Analyzers NuGet package which adds analyzers to your project to help catch many common violations of the threading rules, helping you prevent your code from deadlocking.


	
	



= Lib: SQLite und .Net

	!Tut9 https://zetcode.com/csharp/sqlite/

== Packages

	!M https://system.data.sqlite.org/index.html/doc/trunk/www/faq.wiki=q5
	
	System.Data.SQLite
		The official SQLite database engine for both x86 and x64 
		along with the ADO.NET provider.
		This package includes support for LINQ and Entity Framework 6.
		
	System.Data.SQLite.Core
		The official SQLite database engine for both x86 and x64 
		along with the ADO.NET provider.		
		
	-> Install-Package System.Data.SQLite

= !KH SWE

== MailKit, MimeKit

=== Message ID

- Envelope.MessageId
    - Die MailKit UniqueId ist vermutlich nur während der aktuellen
      MailKit Session Unique

    - Entspricht dem Mail Header Property
      Message-ID

    - Nicht alle E-Mails haben offenbar eine solche ID zwingend,
      aber in der Team-Mailbox haben sie alle

= Archiv

LogViewer2 for WPF
https://github.com/tkouba/LogViewer2

	> Liest ein File, zeigt es an und behält den FileStream
		als Datenquelle für das Log!
		Deshalb für mich nicht brauchbar

		Third Party
			ObjectListView
				Used for displaying the file lines
				!KH9^9 https://objectlistview.sourceforge.net/cs/gettingStarted.html
				https://objectlistview.sourceforge.net/cs/index.html
				
				Install-Package ObjectListView.Official -Version 2.9.2-alpha2
			
			
			Nett
				Used for the TOML configuration file reading/writing
				https://github.com/paiden/Nett
				Install-Package Nett
			
			
			icons8
			Icons used within the application
