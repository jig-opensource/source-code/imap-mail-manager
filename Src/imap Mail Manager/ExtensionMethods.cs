﻿using System.Linq;
using Hierarchy;


namespace ImapMailManager {

   public static class ExtensionMethods {

      #region Public Methods and Operators

      /// <summary>
      /// Der Hierarchy ein Element zufügen,
      /// den Parent setzen
      /// und die neu erzeugte Node zurückgeben
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="parent"></param>
      /// <param name="child"></param>
      /// <returns></returns>
      public static IHierarchyNode<T> ØAddChild<T>(this IHierarchyNode<T> parent, HierarchyNode<T> child) {
         parent.Children.Add(child);
         child.Parent = parent;

         return parent.Children.Last();
      }

      #endregion

   }

}
