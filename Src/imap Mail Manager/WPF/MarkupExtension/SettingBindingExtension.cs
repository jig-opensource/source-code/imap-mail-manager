using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;


namespace ImapMailManager.WPF.MarkupExtension;

/// <summary>
/// Erlaubt das direkte Binden von Properties in Xaml zu den Applikations-Settings
/// Dient zum Speichern der Fenster-Position
///
/// !9 Elegantere Lösung: https://stackoverflow.com/a/53817880/4795779
/// » Es ist mühsam, für jede Einstellung, für jedes Fenster ein App Settings Prop zu erzeugen
///   Sinnvoller ist, diese Settings dynamisch in ein config File zu schreiben  
/// 
/// 
/// !Q https://thomaslevesque.com/2008/11/18/wpf-binding-to-application-settings-using-a-markup-extension/
/// </summary>
public class SettingBindingExtension : Binding {

   public SettingBindingExtension() { Initialize(); }


   public SettingBindingExtension(string path)
      : base(path) {
      Initialize();
   }


   private void Initialize() {
      this.Source = ImapMailManager.Properties.Settings.Default;
      this.Mode   = BindingMode.TwoWay;
   }

}
