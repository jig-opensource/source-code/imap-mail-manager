using System.Windows;
using System.Windows.Controls;


namespace ImapMailManager.WPF.Behavior;


/// <summary>
/// Ein behavior für die WPF TextBox:
///  behaviors:TextBoxBehavior.SelectAllTextOnFocus="True"
/// 
/// !Q https://stackoverflow.com/a/17859206/4795779
/// !Q http://www.dutton.me.uk/2013-07-25/how-to-select-all-wpf-textbox-text-on-focus-using-an-attached-behavior/
/// </summary>
public class TextBoxBehavior {

   public static bool GetSelectAllTextOnFocus(TextBox textBox) {
      return (bool)textBox.GetValue(SelectAllTextOnFocusProperty);
   }


   public static void SetSelectAllTextOnFocus(TextBox textBox, bool value) {
      textBox.SetValue(SelectAllTextOnFocusProperty, value);
   }


   public static readonly DependencyProperty SelectAllTextOnFocusProperty =
      DependencyProperty.RegisterAttached
         (
          "SelectAllTextOnFocus"
          , typeof(bool)
          , typeof(TextBoxBehavior)
          , new UIPropertyMetadata(false, OnSelectAllTextOnFocusChanged));


   private static void OnSelectAllTextOnFocusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
      var textBox = d as TextBox;

      if (textBox == null) return;

      if (e.NewValue is bool == false) return;

      if ((bool)e.NewValue) {
         textBox.GotFocus         += SelectAll;
         textBox.PreviewMouseDown += IgnoreMouseButton;
      }
      else {
         textBox.GotFocus         -= SelectAll;
         textBox.PreviewMouseDown -= IgnoreMouseButton;
      }
   }


   private static void SelectAll(object sender, RoutedEventArgs e) {
      var textBox = e.OriginalSource as TextBox;

      if (textBox == null) return;
      textBox.SelectAll();
   }


   private static void IgnoreMouseButton(object sender, System.Windows.Input.MouseButtonEventArgs e) {
      var textBox = sender as TextBox;

      if (textBox == null || textBox.IsKeyboardFocusWithin) return;

      e.Handled = true;
      textBox.Focus();
   }

}
