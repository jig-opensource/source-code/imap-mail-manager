﻿using System;
using System.Media;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using jig.WPF.SerilogViewerCtrl;
using Serilog;
using Serilog.Events;


namespace jig.WPF.SerilogViewer {

	/// <summary>
	/// Interaction logic for SerilogViewer.xaml
	/// </summary>
	public partial class SerilogViewer : Window {

      #region Fields

      private CancellationTokenSource _cancelLogTask;

      private Task _logTask;

      #endregion


      #region Constructors and Destructors

      public SerilogViewer() {
         InitializeComponent();

         cbAutoScroll.IsChecked = true;

         log = new LoggerConfiguration()
               .MinimumLevel.Verbose()
               .WriteTo.SerilogViewerSink(logCtrl)
               .CreateLogger();

         logCtrl.ItemAdded += OnLogMessageItemAdded;
      }

      #endregion


      #region Public Properties

      public ILogger log { get; }

      #endregion


      #region Public Methods and Operators

      public void LogDebug(string message) => log.Write(LogEventLevel.Debug, message);

      public void LogError(string message) => log.Write(LogEventLevel.Error, message);

      public void LogFatal(string message) => log.Write(LogEventLevel.Fatal, message);

      public void LogInformation(string message) => log.Write(LogEventLevel.Information, message);

      public void LogVerbose(string message) => log.Write(LogEventLevel.Verbose, message);

      public void LogWarning(string message) => log.Write(LogEventLevel.Warning, message);

      #endregion


      #region Internal Methods

      private void AutoScroll_Checked(object   sender, RoutedEventArgs e) => logCtrl.AutoScrollToLast = true;
      private void AutoScroll_Unchecked(object sender, RoutedEventArgs e) => logCtrl.AutoScrollToLast = false;


      private void BackgroundSending_Checked(object sender, RoutedEventArgs e) {
         _cancelLogTask = new CancellationTokenSource();
         var token = _cancelLogTask.Token;
         _logTask = new Task(SendLogs, token, token);
         _logTask.Start();
      }


      private void BackgroundSending_Unchecked(object sender, RoutedEventArgs e) {
         if (_cancelLogTask != null)
            _cancelLogTask.Cancel();
      }


      private void BottomScroll_Click(object sender, RoutedEventArgs e) => logCtrl.ScrollToLast();

      private void Clear_Click(object sender, RoutedEventArgs e) => logCtrl.Clear();


      private void OnLogMessageItemAdded(object o, EventArgs Args) {
         // Do what you want :)
         LogEvent logEvent = (SerilogEvent)Args;

         if (logEvent.Level >= LogEventLevel.Error)
            SystemSounds.Beep.Play();
      }


      private void Send_Click(object sender, RoutedEventArgs e) {
         var level = LogEventLevel.Verbose;

         if (sender.Equals(btnDebug))
            level = LogEventLevel.Debug;

         if (sender.Equals(btnWarning))
            level = LogEventLevel.Warning;

         if (sender.Equals(btnError))
            level = LogEventLevel.Error;

         if ((bool)cbWithContext.IsChecked) { log.ForContext<SerilogViewer>().Write(level, tbLogText.Text); }
         else { log.Write(level, tbLogText.Text); }
      }


      private void SendLogs(object obj) {
         var ct = (CancellationToken)obj;

         var counter = 0;

         var backgroundLogger = log.ForContext("Type", "Backgroundtask");

         backgroundLogger.Debug("Backgroundtask started.");

         while (!ct.WaitHandle.WaitOne(2000)) {
            backgroundLogger.ForContext("Type", "Backgroundtask")
                            .Verbose(string.Format("Message number: {0} from backgroudtask.", counter++));
         }

         backgroundLogger.Debug("Backgroundtask stopped.");
      }


      private void SendWithContext_Click(object sender, RoutedEventArgs e) {
         var level = LogEventLevel.Verbose;

         if (sender.Equals(btnDebug))
            level = LogEventLevel.Debug;

         if (sender.Equals(btnWarning))
            level = LogEventLevel.Warning;

         if (sender.Equals(btnError))
            level = LogEventLevel.Error;

         log.ForContext<SerilogViewer>().Write(level, tbLogText.Text);
      }


      private void TopScroll_Click(object sender, RoutedEventArgs e) => logCtrl.ScrollToFirst();

      #endregion

   }

}
