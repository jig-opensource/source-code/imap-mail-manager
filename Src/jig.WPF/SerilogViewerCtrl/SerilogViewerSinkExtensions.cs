﻿using System;
using Serilog;
using Serilog.Configuration;


namespace jig.WPF.SerilogViewerCtrl {

   public static class SerilogViewerSinkExtensions {

      #region Public Methods and Operators

      public static LoggerConfiguration SerilogViewerSink(
         this LoggerSinkConfiguration loggerConfiguration,
         SerilogViewerCtrl            serilogViewerCtrl,
         IFormatProvider              formatProvider = null)
         => loggerConfiguration.Sink(new SerilogViewerSink(formatProvider, serilogViewerCtrl));

      #endregion

   }

}
