﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Serilog.Events;


namespace jig.WPF.SerilogViewerCtrl {

    /// <summary>
    /// Interaction logic for SerilogViewerCtrl.xaml
    /// </summary>
    public partial class SerilogViewerCtrl : UserControl {

      #region Constructors and Destructors

      public SerilogViewerCtrl() {
         IsTargetConfigured = false;
         LogEntries         = new ObservableCollection<LogEventViewModel>();

         InitializeComponent();

         if (!DesignerProperties.GetIsInDesignMode(this)) {
            //Serilog.Configuration.
            //target.LogReceived += LogReceived;
         }
      }

      #endregion


      #region Public Events

      public event EventHandler ItemAdded = delegate {};

      #endregion


      #region Public Properties

      [Description("Automatically scrolls to the last log item in the viewer. Default is true.")]
      [Category("Data")]
      [TypeConverter(typeof(BooleanConverter))]
      public bool AutoScrollToLast { get; set; } = true;

      [Description("Width of Context column in pixels, or auto if not specified")]
      [Category("Data")]
      [TypeConverter(typeof(LengthConverter))]
      public double ContextWidth { get; set; } = 50;

      [Description("Width of Exception column in pixels")]
      [Category("Data")]
      [TypeConverter(typeof(LengthConverter))]
      public double ExceptionWidth { get; set; } = 75;
      public bool IsTargetConfigured { get; }

      [Description("Width of Level column in pixels")]
      [Category("Data")]
      [TypeConverter(typeof(LengthConverter))]
      public double LevelWidth { get; set; } = 50;

      public ObservableCollection<LogEventViewModel> LogEntries { get; }
      public ListView                                LogView    => logView;

      [Description("The maximum number of row count. The oldest log gets deleted. Set to 0 for unlimited count.")]
      [Category("Data")]
      [TypeConverter(typeof(Int32Converter))]
      public int MaxRowCount { get; set; } = 50;

      [Description("Width of Message column in pixels")]
      [Category("Data")]
      [TypeConverter(typeof(LengthConverter))]
      public double MessageWidth { get; set; } = 200;

      [Description("Width of time column in pixels")]
      [Category("Data")]
      [TypeConverter(typeof(LengthConverter))]
      public double TimeWidth { get; set; } = 550;

      #endregion


      #region Public Methods and Operators

      public void Clear() => LogEntries.Clear();


      public void LogReceived(LogEvent log, IFormatProvider formatProvider) {
         var vm = new LogEventViewModel(log, formatProvider);

         Dispatcher.BeginInvoke(
            new Action(
               () => {
                  if (MaxRowCount > 0
                      && LogEntries.Count >= MaxRowCount)
                     LogEntries.RemoveAt(0);
                  LogEntries.Add(vm);

                  if (AutoScrollToLast)
                     ScrollToLast();

                  //ItemAdded(this, log);
               }));
      }


      public void ScrollToFirst() {
         if (LogView.Items.Count <= 0)
            return;
         LogView.SelectedIndex = 0;
         ScrollToItem(LogView.SelectedItem);
      }


      public void ScrollToLast() {
         if (LogView.Items.Count <= 0)
            return;
         LogView.SelectedIndex = LogView.Items.Count - 1;
         ScrollToItem(LogView.SelectedItem);
      }

      #endregion


      #region Internal Methods

      private void ScrollToItem(object item) => LogView.ScrollIntoView(item);

      #endregion

   }

}
