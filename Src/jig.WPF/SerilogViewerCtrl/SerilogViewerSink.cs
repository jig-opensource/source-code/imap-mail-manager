﻿using System;
using Serilog.Core;
using Serilog.Events;


namespace jig.WPF.SerilogViewerCtrl {

   public class SerilogViewerSink : ILogEventSink {

      #region Fields

      private readonly IFormatProvider _formatProvider;

      #endregion


      #region Constructors and Destructors

      public SerilogViewerSink(IFormatProvider formatProvider, SerilogViewerCtrl serilogViewerCtrl) {
         _formatProvider =  formatProvider;
         LogReceived     += serilogViewerCtrl.LogReceived;
      }

      #endregion


      #region Public Events

      public event Action<LogEvent, IFormatProvider> LogReceived;

      #endregion


      #region Public Methods and Operators

      public void Emit(LogEvent logEvent) => LogReceived?.Invoke(logEvent, _formatProvider);

      #endregion

   }

}
