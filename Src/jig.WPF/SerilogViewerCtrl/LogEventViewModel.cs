﻿using System;
using System.Globalization;
using System.Windows.Media;
using Serilog.Events;


namespace jig.WPF.SerilogViewerCtrl {

   public class LogEventViewModel {

      #region Fields

      private IFormatProvider formatProvider;

      private LogEvent logEvent;

      #endregion


      #region Constructors and Destructors

      // ReSharper: [NotNull]
      public LogEventViewModel(LogEvent logEvent, IFormatProvider formatProvider) {
         // TODO: Complete member initialization
         this.logEvent       = logEvent;
         this.formatProvider = formatProvider;

         ToolTip          = logEvent.RenderMessage();
         Level            = logEvent.Level.ToString();
         FormattedMessage = logEvent.RenderMessage(formatProvider);
         Exception        = logEvent.Exception;
         LogEventPropertyValue sourceContext;

         SourceContext = "";

         if (logEvent.Properties.TryGetValue("SourceContext", out sourceContext)) { SourceContext = sourceContext.ToString(); }

         Time = logEvent.Timestamp.ToString("G", CultureInfo.CurrentCulture);

         SetupColors(logEvent);
      }

      #endregion


      #region Public Properties

      public SolidColorBrush Background          { get; private set; }
      public SolidColorBrush BackgroundMouseOver { get; private set; }
      public Exception?      Exception           { get; }
      public SolidColorBrush Foreground          { get; private set; }
      public SolidColorBrush ForegroundMouseOver { get; private set; }
      public string          FormattedMessage    { get; }
      public string          Level               { get; }
      public string          SourceContext       { get; set; }

      public string Time    { get; }
      public string ToolTip { get; }

      #endregion


      #region Internal Methods

      private void SetupColors(LogEvent logEvent) {
         if (logEvent.Level == LogEventLevel.Warning) {
            Background          = Brushes.Yellow;
            BackgroundMouseOver = Brushes.GreenYellow;
         }
         else if (logEvent.Level == LogEventLevel.Error) {
            Background          = Brushes.Tomato;
            BackgroundMouseOver = Brushes.IndianRed;
         }
         else {
            Background          = Brushes.White;
            BackgroundMouseOver = Brushes.LightGray;
         }

         Foreground          = Brushes.Black;
         ForegroundMouseOver = Brushes.Black;
      }

      #endregion

   }

}
