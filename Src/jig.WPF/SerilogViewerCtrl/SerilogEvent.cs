﻿using System;
using Serilog.Events;


namespace jig.WPF.SerilogViewerCtrl {

   public class SerilogEvent : EventArgs {

      #region Fields

      public LogEvent EventInfo;

      #endregion


      #region Constructors and Destructors

      public SerilogEvent(LogEvent logEvent) =>

         // TODO: Complete member initialization
         EventInfo = logEvent;

      #endregion


      #region Public Methods and Operators

      public static implicit operator LogEvent(SerilogEvent e) => e.EventInfo;

      public static implicit operator SerilogEvent(LogEvent e) => new SerilogEvent(e);

      #endregion

   }

}
