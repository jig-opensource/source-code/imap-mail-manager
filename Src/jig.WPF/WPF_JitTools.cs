using System.Windows;
using System.Windows.Media;


namespace jig.WPF; 

public class WPF_JitTools {

   /// <summary>
   /// Sucht ein WPF-Element
   /// </summary>
   /// <param name="startNode"></param>
   /// <param name="name"></param>
   /// <returns></returns>
   public static FrameworkElement? FindChildByName(DependencyObject startNode, string name) {
      int count = VisualTreeHelper.GetChildrenCount(startNode);

      for (int i = 0; i < count; i++) {
         DependencyObject current = VisualTreeHelper.GetChild(startNode, i);

         if (current is FrameworkElement frameworkElement) {
            if (frameworkElement.Name == name)
               return frameworkElement;
         }

         var result = FindChildByName(current, name);

         if (result != null) {
            return result;
         }
      }

      return null;
   }

}
