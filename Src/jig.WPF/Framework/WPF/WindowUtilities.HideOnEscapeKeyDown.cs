﻿using System.Windows;
using System.Windows.Input;


namespace jig.WPF.Framework.WPF {

	/// <summary>
	/// TomTom
	/// WPF Erweiterung, die ein Fenster bei ESC versteckt
	/// Usage: im WPF Formular XML ergänzen
	/// xmlns:utilities="clr-namespace:jig.Tools.Framework.WPF;assembly=jig.Tools"
	/// utilities:WindowUtilities.HideOnEscapeKeyDown="True"
	/// !Idee
	/// https://github.com/meziantou/Meziantou.Framework/tree/main/src/Meziantou.Framework.WPF
	/// </summary>
	public static class WindowUtilities {

      #region Static Fields

      /// <summary>
      /// Usage, im WPF xml:
      /// xmlns:utilities="clr-namespace:Meziantou.Framework.WPF;assembly=Meziantou.Framework.WPF"
      /// xmlns:utilities="clr-namespace:ImapMailManager.Tools.WPF"
      /// utilities:WindowUtilities.HideOnEscapeKeyDown="True"
      /// </summary>
      public static readonly DependencyProperty HideOnEscapeProperty = DependencyProperty.RegisterAttached(
         "HideOnEscapeKeyDown",
         typeof(bool),
         typeof(WindowUtilities),
         new FrameworkPropertyMetadata(false, HideOnEscapeKeyDownChanged));

      #endregion


      #region Public Methods and Operators

      public static bool GetHideOnEscapeKeyDown(DependencyObject d) => (bool)d.GetValue(HideOnEscapeProperty);

      public static void SetHideOnEscapeKeyDown(DependencyObject d, bool value) => d.SetValue(HideOnEscapeProperty, value);

      #endregion


      #region Internal Methods

      private static void HideOnEscapeKeyDown_PreviewKeyDown(object sender, KeyEventArgs e) {
         if (sender is Window target) {
            if (e.Key == Key.Escape) { target.Hide(); }
         }
      }


      private static void HideOnEscapeKeyDownChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
         if (d is Window target) {
            if ((bool)e.NewValue) { target.PreviewKeyDown += HideOnEscapeKeyDown_PreviewKeyDown; }
            else { target.PreviewKeyDown                  -= HideOnEscapeKeyDown_PreviewKeyDown; }
         }
      }

      #endregion

   }

}
