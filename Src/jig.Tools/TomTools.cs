﻿using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;


namespace jig.Tools;

public class TomTools {

   #region Public Methods and Operators

   /// <summary>
   /// Stellt sicher, dass nullableReference nicht null ist
   /// </summary>
   /// <param name="nullableReference"></param>
   /// <exception cref="ArgumentNullException"></exception>
   public static void AssertIsNotNull([NotNull] object? nullableReference) {
      if (nullableReference == null) { throw new ArgumentNullException(); }
   }


   /// <summary>
   /// Gibt format / args aus, wenn print true ist
   /// </summary>
   /// <param name="print"></param>
   /// <param name="format"></param>
   /// <param name="args"></param>
   public static void DebugWriteline(bool print, string format, params object?[] args) {
      if (print) { Debug.WriteLine(format, args); }
   }

   #endregion

}
