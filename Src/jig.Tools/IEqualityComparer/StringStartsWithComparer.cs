namespace jig.Tools.IEqualityComparer;

/// <summary>
/// Der Comparer der prüft, ob die Strings der Haupt-Liste
/// gleich starten wie einer der Strings der Vergleichsliste
/// </summary>
public class StringStartsWithComparer : IEqualityComparer<string> {

   public bool Equals(string? str1, string? str2) {
      if (str2 == null && str1 == null) {
         // Debug.WriteLine("• str2 == null && str1 == null");
         return true;
      }

      if (str1 == null || str2 == null) {
         // Debug.WriteLine("• str1 == null || str2 == null");
         return false;
      }

      var res = str2.StartsWith(str1, StringComparison.OrdinalIgnoreCase);

      // Debug.WriteLine($"• Equals({str1}, {str2}): {res}");
      return res;
   }


   /// <summary>
   /// !KH9 TomTom
   /// 
   /// Damit der Vergleich immer Equals() aufruft,
   /// liefert der HashCode immer 0,
   /// so dass alle Elemente identisch scheinen und ein weiterer Vergleich nötig ist  
   /// </summary>
   /// <param name="obj"></param>
   /// <returns></returns>
   public int GetHashCode(string obj) {
      return 0;
   }

}
