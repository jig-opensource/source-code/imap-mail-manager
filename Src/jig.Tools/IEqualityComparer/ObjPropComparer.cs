namespace jig.Tools.IEqualityComparer;


// !KH9 Tom
// Der Comparer der prüft, ob der Wert der Props zweier Objs identisch sind
//
// !TT9 How to intersect two different IEnumerable collections
//   https://stackoverflow.com/a/5468803/4795779
//
//      HashSet<int> listProp1Items = new HashSet<int>(listB.Select(x => x.Prop1));
//      var filtered = listA.Where(x => listProp1Items.Contains(x.Prop2));
//
//   oder ein Join:
//      var query = from a in listA
//      join b in listB on a.z2 equals b.j6
//      select new { a, b };
// 
