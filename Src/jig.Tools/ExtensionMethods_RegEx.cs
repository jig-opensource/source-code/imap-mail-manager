using System.Text.RegularExpressions;
using jig.Tools.String;


namespace jig.Tools;

public static class ExtensionMethods_RegEx {

   /// <summary>
   /// Konvertiert einen Wilcard-String
   /// in einen RegEx
   /// </summary>
   /// <param name="wildcardStr"></param>
   /// <returns></returns>
   public static string ØWildcardToRegex(string wildcardStr) {
      if (!wildcardStr.ØHasValue()) { return ""; }
      
      // Wildcards: ? und *
      return "^" + Regex.Escape(wildcardStr).Replace("\\?", ".").Replace("\\*", ".*") + "$";

      // Wildcard: *
      // return "^" + Regex.Escape(value).Replace("\\*", ".*") + "$"; 

      // SQL % als Wildcard
      // return "^" + Regex.Escape(wildcardStr).Replace("_", ".").Replace("%", ".*") + "$";
   }


   /// <summary>
   /// Prüft, ob str im Text das Wildcard-Muster in  wildcardCompareStr dring hat
   /// </summary>
   /// <param name="str"></param>
   /// <param name="wildcardCompareStr"></param>
   /// <returns></returns>
   public static bool ØMatchWildcard(this string str, string wildcardCompareStr) {
      if (!str.ØHasValue()) { return false; }
      if (!wildcardCompareStr.ØHasValue()) { return false; }
      
      return Regex.IsMatch(str, ØWildcardToRegex(wildcardCompareStr));
   }
   
   
   /// <summary>
   /// Prüft, ob ein RegEx matcht, kommt mit null klar
   /// </summary>
   /// <param name="regex"></param>
   /// <param name="testStr"></param>
   /// <returns></returns>
   public static Match ØMatch(this Regex? regex, string? testStr) {
      if (regex != null && testStr != null) {
         return regex.Match(testStr);
      }

      return Match.Empty;
   }


   public static Match ØMatch(this string? testStr, string? regex) {
      if (regex != null && testStr != null) {
         return Regex.Match(testStr, regex);
      }

      return Match.Empty;
   }


   /// <summary>
   /// Sucht alle Matches der named Group und liefert die List<string>
   /// </summary>
   /// <param name="regex"></param>
   /// <param name="testStr"></param>
   /// <param name="groupName"></param>
   /// <returns></returns>
   public static List<string> ØGetNamedMatches(this Regex? regex, string? testStr, string groupName) {
      if (regex != null && testStr != null) {
         //+ Suchen
         var matches = regex.Matches(testStr).ØOrEmptyIfNull();

         //+ Suche verarbeiten
         var foundList = new List<string>();

         foreach (Match match in matches) {
            string thisHashTag = match.Groups[groupName].Value;
            foundList.ØAppendItem_IfMissing(thisHashTag);
         }

         return foundList;
      }

      return new List<string>();
   }

}
