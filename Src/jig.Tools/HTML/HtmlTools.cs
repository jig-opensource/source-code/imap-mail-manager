using System.Diagnostics;
using HtmlAgilityPack;
using jig.Tools.Exception;
using jig.Tools.String;


namespace jig.Tools.HTML;

public class HtmlTools {

   /// <summary>
   /// Versucht das die HtmlNode zu finden: <!DocType 
   /// </summary>
   /// <param name="htmlDoc"></param>
   /// <returns></returns>
   public static HtmlNode? Get_HtmlNode_DocType(HtmlDocument htmlDoc) {
      return htmlDoc.DocumentNode
                    .ChildNodes
                    .OfType<HtmlCommentNode>()
                    .FirstOrDefault
                        (x => x.Comment
                               .StartsWith
                                   ("<!DocType"
                                    , StringComparison.OrdinalIgnoreCase));
   }


   /// <summary>
   /// Manchmal haben E-Mails zwar einen
   /// Content-Type: text/html;
   /// aber der Inhalt beginnt grad mit einem <div> …
   /// und hat keinen HTML Header / Body
   ///
   /// Diese Funktion stellt sicher, dass wir ein ordentliches HTML-Dokument haben, mit:
   ///   <!doctype html>
   ///   <html>
   ///      <head></head>
   ///      <body></body>
   ///   </html>
   /// 
   /// </summary>
   public static HtmlDocument Ensure_Complete_HtmlDoc(
      string   defaultHtmlDocStruct
      , string newContentHtmlText) {
      //+ Das Resultat mit dem vollständigen Dokument erstellen
      HtmlDocument resHtmlDoc = new HtmlDocument();
      resHtmlDoc.LoadHtml(defaultHtmlDocStruct);

      //+ Wenn das neue Dokument leer ist, geben wir die standard-Strktur zurück
      if (!newContentHtmlText.ØHasValue()) {
         return resHtmlDoc;
      }

      //+ Aus dem Text ein HtmlDoc erzeugen
      HtmlDocument newContentHtmlDoc = new HtmlDocument();
      newContentHtmlDoc.LoadHtml(newContentHtmlText);

      //+ Das Dokument analysieren
      //++ Prüfen, welche Nodes wir haben

      //+++ Haben wir einen DocType?
      // ‼ !KH Einfacher wöre, ist aber Case-Sensitive :-(
      //    var newContentHtmlDocTypeNode = newContentHtmlDoc.DocumentNode.SelectSingleNode
      //    ("/comment()[starts-with(.,'<!DOCTYPE')]");

      HtmlNode? newContentHtmlDocTypeNode = Get_HtmlNode_DocType(newContentHtmlDoc);

      HtmlNode? newContentMainHtmlNode = newContentHtmlDoc.DocumentNode.SelectSingleNode("//html");
      HtmlNode? newContentHeadNode     = newContentHtmlDoc.DocumentNode.SelectSingleNode("//head");
      HtmlNode? newContentBodyNode     = newContentHtmlDoc.DocumentNode.SelectSingleNode("//body");

      /*
       * <!doctype html>
       * <html>
       *   <head></head>
       *   <body>
       *    » Hier hinein kommt <htmltext> 
       *   </body>
       * </html>
      */

      //+ Das Resultat berechnen indem die einzelnen Elemente vom neuen Dokumen ins Template übertragen werden 
      //++ Den Doctype ins Resultat übernehmen
      if (newContentHtmlDocTypeNode != null) {
         // Im Resultat / Template die DocTypeNode ersetzen
         /*
                  var resDocTypeNode = resHtmlDoc.DocumentNode.SelectSingleNode
                     ("/comment()[starts-with(.,'<!DOCTYPE')]");
         */
         var resDocTypeNode = Get_HtmlNode_DocType(resHtmlDoc);

         resDocTypeNode.ParentNode.ReplaceChild(newContentHtmlDocTypeNode, resDocTypeNode);
      }

      //++ Haben wir weder html-, head- noch body-Nodes?
      if (newContentMainHtmlNode == null
          && newContentHeadNode == null
          && newContentBodyNode == null) {
         //+++ Dann einfach den Inhalt im Body einfügen
         HtmlNode? tplBodyNode = resHtmlDoc.DocumentNode.SelectSingleNode("//body");
         tplBodyNode.AppendChildren(newContentHtmlDoc.DocumentNode.ChildNodes);

         // Fertig
         return resHtmlDoc;
      }

      //++ Haben wir eine main html node?
      if (newContentMainHtmlNode != null) {
         //+++ Wenn der neue Inhalt eine Head- und Body-Node hat
         if (newContentHeadNode != null && newContentBodyNode != null) {
            // dann übernehmen wir den ganzen main html block
            HtmlNode? tplHtmlNode = resHtmlDoc.DocumentNode.SelectSingleNode("//html");
            tplHtmlNode.ParentNode.ReplaceChild(newContentMainHtmlNode, tplHtmlNode);
         }

         else if (newContentHeadNode == null && newContentBodyNode == null) {
            //+++ Wenn der neue Inhalt weder eine Head- noch Body-Node
            // und nur <html><div> … </div></html> hat,
            // dann setzen wir im Template nur den Inhalt von Body 
            HtmlNode? tplBodyNode = resHtmlDoc.DocumentNode.SelectSingleNode("//body");
            tplBodyNode.AppendChildren(newContentMainHtmlNode.ChildNodes);
         }
         else if (newContentHeadNode != null) {
            //+++ Wenn der neue Inhalt nur <html><head></head></html> hat, dann haben wir ein Problem:
            // Es existiert kein Body
            throw new InvalidOperationException
               ("Der neue HTML-Inhalt hat nur eine Head-Node ohne Body");
         }
         else if (newContentBodyNode != null) {
            //+++ Wenn der neue Inhalt nur <html><body></body></html> hat, dann
            // dann übernehmen wir nur den Head vom Template
            HtmlNode? tplHeadNode = resHtmlDoc.DocumentNode.SelectSingleNode("//head");
            newContentBodyNode.ParentNode.InsertBefore(tplHeadNode, newContentBodyNode);

            //+++ und wir übergeben die main html node dem resultat
            newContentMainHtmlNode = newContentHtmlDoc.DocumentNode.SelectSingleNode("//html");
            HtmlNode? resHtmlNode = resHtmlDoc.DocumentNode.SelectSingleNode("//html");
            resHtmlNode.ParentNode.ReplaceChild(newContentMainHtmlNode, resHtmlNode);
         }

         // Fertig
         return resHtmlDoc;
      }

      //++ Haben wir eine head html node?
      if (newContentHeadNode != null) {
         //+++ Die Head Node übernehmen
         HtmlNode? tplHeadNode = resHtmlDoc.DocumentNode.SelectSingleNode("//head");
         tplHeadNode.ParentNode.ReplaceChild(newContentHeadNode, tplHeadNode);
      }

      //++ Haben wir eine body html node?
      if (newContentBodyNode != null) {
         //+++ Die Body Node übernehmen
         HtmlNode? tplBodyNode = resHtmlDoc.DocumentNode.SelectSingleNode("//body");
         tplBodyNode.ParentNode.ReplaceChild(newContentBodyNode, tplBodyNode);
      }

      return resHtmlDoc;
   }


   /// <summary>
   /// Prüft im htmlDoc in Head\Style
   /// und ergänzt oder aktualisiert die Styles,
   /// wenn unser Style fehlt oder veraltet ist 
   /// </summary>
   /// <param name="htmlDoc"></param>
   /// Das htmlDoc obj mit den Daten
   /// <param name="strNewStyle"></param>
   /// Der neue Style als String
   /// <param name="cssSelectorId">
   /// Der Name des CSS Selecors, der als ID für unser CSS RuleSet dient
   /// Definiert, welcher CssSelector den Style eindeutig identifiziert
   /// </param>
   /// <returns>
   /// var (htmlChanged, htmlDoc) = … 
   /// </returns>
   public static Tuple<bool, HtmlDocument> AddOrUpdate_HtmlHeadStyle(
      HtmlDocument htmlDoc
      , string     strNewStyle
      , string     cssSelectorId) {
      var htmlChanged = false;

      //+ newStyle parsen und in ein Obj wandeln, um es analysieren zu können
      var objCssNewStyle = new Lib.Jo_CSS_Parser.Parser.CssParser();
      objCssNewStyle.Css = strNewStyle;

      var htmlHead       = htmlDoc.DocumentNode.SelectSingleNode("//head");
      var headStyleNodes = htmlDoc.DocumentNode.SelectNodes("//head/style").ØOrEmptyIfNull();

      //+ Haben wir den newStyle bereits?
      // Jeden gefundenen Style prüfen
      var cssSelectorIDFound = false;

      foreach (var thisStyle in headStyleNodes) {
         //+ Haben wir den CSS Selecor, der unseren Style identifiziert?
         var objThisCssStyle = new Lib.Jo_CSS_Parser.Parser.CssParser();
         objThisCssStyle.Css = thisStyle.InnerHtml;

         if (objThisCssStyle.CssSelectorExist(cssSelectorId)) {
            //+ Unser CSS RuleSet gefunden
            cssSelectorIDFound = true;

            //++ Ist die Style-Definition identisch?
            if (!objCssNewStyle.Css.ØCompareStringWithouWhiteChars
                   (objThisCssStyle.Css
                    , StringComparison
                      .OrdinalIgnoreCase)) {
               htmlChanged = true;

               //+++ Der neue Style ist anders, deshalb im Html Doc ersetzen
               thisStyle.InnerHtml = objCssNewStyle.Css;
            }
         }
      }

      //+ Wenn wir den cssSelectorID nicht gefunden haben, dann ihn neu erzeugen
      if (!cssSelectorIDFound) {
         //+ Nein: Unser Style existiert nicht, also ergänzen
         HtmlNode newStyle = HtmlNode.CreateNode($"<Style>\n{objCssNewStyle.Css}</Style>");

         // Den Header ergänzen
         htmlChanged = true;
         htmlHead.AppendChild(newStyle);
      }

      return new Tuple<bool, HtmlDocument>(htmlChanged, htmlDoc);
   }


   /// <summary>
   /// Sucht im Html\Body Footer ein Element mit einer ID
   /// und ersetzt den Inhalt
   /// </summary>
   /// <param name="htmlDoc"></param>
   /// Das HTML Doc, das aktualisiert wird
   /// <param name="elementID"></param>
   /// Das HTML Element ID, das wir ersetzen
   /// <param name="newHtmlDocElement">
   /// Die HTML Doc Node mit den Element-Daten
   /// </param>
   /// <returns>
   /// var (isHtmlUpdated, newHtmlDoc) = … 
   /// bool          Wurde das HtmlDoc verändert?
   /// HtmlDocument  Das HtmlDoc
   /// </returns>
   public static Tuple<bool, HtmlDocument?> AddOrUpdate_HtmlElementByID(
      HtmlDocument htmlDoc
      , string     elementID
      , HtmlNode   newHtmlDocElement) {
      var htmlUpdated = false;

      //+ Die Body Node suchen 
      var htmlBody = htmlDoc.DocumentNode.SelectSingleNode("//body");

      //+ Existiert das Element?
      HtmlNode currElementData = htmlDoc.GetElementbyId(elementID);

      if (currElementData == null) {
         //+ Das Element einfügen
         htmlUpdated = true;
         htmlBody.AppendChild(newHtmlDocElement);
      }
      else {
         //+ Das Element existiert bereits
         // Ist das Element bereits aktuell / identisch?
         if (!currElementData.OuterHtml.ØCompareStringWithouWhiteChars
                (newHtmlDocElement.OuterHtml
                 , StringComparison.OrdinalIgnoreCase)) {
            //+ Das neue Element ist unterschiedlich - ersetzen
            htmlUpdated = true;
            htmlBody.ReplaceChild(newHtmlDocElement, currElementData);
         }
      }

      return new Tuple<bool, HtmlDocument?>(htmlUpdated, htmlDoc);
   }


   /// <summary>
   /// Ersetzt im htmlDoc die Body-Node
   /// </summary>
   /// <param name="htmlDoc"></param>
   /// <param name="newBodyNodeHtml">
   /// Muss das umfassende Body-HTML Tag beinhalten
   /// </param>
   public static void Replace_BodyNode(HtmlDocument htmlDoc, string newBodyNodeHtml) {
      HtmlNode bodyNode = htmlDoc.DocumentNode.SelectSingleNode("//body");
      if (bodyNode == null) {
         // ToDo 🟥 Warnung im Logfile, weil vermutlich m Programmierer unerwartetes Verhalten
         throw new RuntimeException("Replace_BodyNode(): Die //body Node wurde nicht gefunden!");
      }
      var newNode    = HtmlNode.CreateNode(newBodyNodeHtml);
      bodyNode.ParentNode.ReplaceChild(newNode, bodyNode);
   }


   /// <summary>
   /// Sucht im Html\Body Footer ein Element mit einer ID
   /// und ersetzt den Inhalt
   /// </summary>
   /// <param name="htmlDoc"></param>
   /// Das HTML Doc, das aktualisiert wird
   /// <param name="elementID">
   /// Das HTML Element ID, das wir ersetzen
   /// </param>
   /// <param name="newHtmlData">
   /// Der String der HTML Doc Node mit den Element-Daten
   /// </param>
   /// <returns>
   /// Tuple<bool, HtmlDocument>
   /// bool          Wurde das HtmlDoc verändert?
   /// HtmlDocument  Das HtmlDoc
   /// </returns>
   public static Tuple<bool, HtmlDocument?> AddOrUpdate_HtmlElementByID(
      HtmlDocument htmlDoc
      , string     elementID
      , string     newHtmlData) {
      //+ Config
      // Die neue Node erzeugen
      HtmlNode newChild = HtmlNode.CreateNode(newHtmlData);

      return AddOrUpdate_HtmlElementByID
         (htmlDoc
          , elementID
          , newChild);
   }


   #region Debug

   /// <summary>
   /// Gibt eine HtmlNode aus
   /// </summary>
   /// <param name="node"></param>
   public static void DisplayNodes(HtmlNodeCollection node) {
      Debug.WriteLine("---- DisplayNodes() ----");

      foreach (var n in node) {
         DisplayNode(n);
      }

      Debug.WriteLine("------------------------");
   }


   /// <summary>
   /// Gibt eine HtmlNode aus
   /// </summary>
   /// <param name="node"></param>
   public static void DisplayNode(HtmlNode node) {
      Debug.WriteLine("Node Name: " + node.Name);
      Debug.Write("  " + node.Name + " children:\n");
      DisplayChildNodes(node);
   }


   public static void DisplayChildNodes(HtmlNode nodeElement) {
      HtmlNodeCollection childNodes = nodeElement.ChildNodes;

      if (childNodes.Count == 0) {
         Debug.WriteLine("    " + nodeElement.Name + " has no children");
      }
      else {
         foreach (var node in childNodes) {
            if (node.NodeType == HtmlNodeType.Element) {
               Debug.WriteLine("    " + node.OuterHtml);
            }
         }
      }
   }

   #endregion Debug

}
