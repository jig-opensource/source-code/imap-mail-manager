using System.Diagnostics;
using System.Reflection;


namespace jig.Tools;

public static class ExtensionMethods_Object {

   public static Dictionary<string, object> DictionaryFromType(this object? aType) {
      if (aType == null) {
         return new Dictionary<string, object>();
      }

      Type           t            = aType.GetType();
      PropertyInfo[] propsDefault = t.GetProperties();

      PropertyInfo[] propsInstamce  = t.GetProperties(BindingFlags.Instance);
      PropertyInfo[] propsPublic    = t.GetProperties(BindingFlags.Public);
      PropertyInfo[] propsNonPublic = t.GetProperties(BindingFlags.NonPublic);

      PropertyInfo[] propsMany = t.GetProperties
         (BindingFlags.Instance
          | BindingFlags
            .Public
          | BindingFlags.NonPublic);

      FieldInfo[] fieldsDefault = t.GetFields();

      FieldInfo[] fieldsMany = t.GetFields
         (BindingFlags.Instance
          | BindingFlags
            .Public
          | BindingFlags.NonPublic);

      Dictionary<string, object> dict = new Dictionary<string, object>();

      foreach (PropertyInfo prp in propsDefault) {
         object value = prp.GetValue(aType, new object[] {});
         dict.Add(prp.Name, value);
      }

      return dict;
   }


   /// <summary>
   /// Liefert via Reflection den Wert eines benannten Properties vom Obj this
   /// </summary>
   /// <param name="obj"></param>
   /// <param name="propName"></param>
   /// <returns></returns>
   public static object? ØObjGetPropVal(this object? obj, string propName) {
      if (obj != null) {
         //+ Das Prop holen
         var thisProp = obj.GetType().GetProperty
            (propName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

         if (thisProp != null) {
            //+ Den Wert holen und zurückgeben
            return thisProp?.GetValue(obj, null);
         }

         //+ Noch die Fields prüfen
         var thisField = obj.GetType().GetField
            (propName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

         if (thisField != null) {
            //+ Den Wert holen und zurückgeben
            return thisField?.GetValue(obj);
         }

         // ToDo 🟥 Aktivieren:
         // throw new ArgumentException($"Das obj {obj.GetType()} hat kein Property mit Namen '{propName}'");
         Debug.WriteLine($"Das obj {obj.GetType()} hat weder ein Field noch Property mit Namen '{propName}'");
      }

      return null;
   }


   /// <summary>
   /// Setzt via Reflection den Wert eines benannten Properties vom Obj this
   /// </summary>
   /// <param name="obj"></param>
   /// <param name="propName"></param>
   /// <returns></returns>
   public static void ØObjSetPropVal(this object? obj, string propName, string propValue) {
      if (obj != null) {
         if (obj.ØObjHasProp(propName)) {
            obj.GetType().GetProperty(propName)?.SetValue
               (obj
                , propValue
                , null);
         }
         else if (obj.ØObjHasField(propName)) {
            obj.GetType().GetField(propName)?.SetValue(obj, propValue);
         }
         else {
            // ToDo 🟥 Aktivieren:
            // throw new ArgumentException($"Das obj {obj.GetType()} hat kein Property mit Namen '{propName}'");
            Debug.WriteLine($"Das obj {obj.GetType()} hat weder ein Field noch Property mit Namen '{propName}'");
         }

      }
   }


   /// <summary>
   /// Liefert true, wenn Obj das Property propName hat
   /// </summary>
   /// <param name="propName"></param>
   /// <returns></returns>
   public static bool ØObjHasProp(this object obj, string propName) {
      return obj.GetType().GetProperty(propName) != null;
   }


   /// <summary>
   /// Liefert True, wenn das Obj das Field hat
   /// </summary>
   /// <param name="obj"></param>
   /// <param name="propName"></param>
   /// <returns></returns>
   public static bool ØObjHasField(this object obj, string propName) {
      return obj.GetType().GetField(propName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
             != null;
   }


   /// <summary>
   /// Setzt Wert eines benannten Properties
   /// </summary>
   /// <param name="obj"></param>
   /// <param name="propName"></param>
   /// <param name="newVal"></param>
   /// <returns></returns>
   public static void ØObjSetPropVal(this object obj, string propName, object newVal)
      => obj.GetType().GetProperty(propName)?.SetValue
         (obj
          , newVal
          , null);

}
