namespace jig.Tools.Exception;

/// <summary>
/// Die fehlende .Net Framework Exception
/// </summary>
[Serializable]
public class RuntimeException : System.Exception {

   public RuntimeException()
      : base() {}


   public RuntimeException(string message)
      : base(message) {}


   public RuntimeException(System.Exception e)
      : base("", e) {}


   public RuntimeException(string exception, System.Exception ex)
      : base(exception, ex) {}
   

}
