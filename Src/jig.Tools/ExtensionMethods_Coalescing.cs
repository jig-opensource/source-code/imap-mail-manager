using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Linq.Expressions;


namespace jig.Tools;

/// <summary>
/// Coalescing, coalesce
/// Verschmelzen, zusammenwachsen
///
/// » IN Aufrufketten null erkennen und die Kette zum Resultat zusammenschmelzen lassen
///
///
/// !Q, Varianten
///     https://smellegantcode.wordpress.com/2008/12/11/the-maybe-monad-in-c/
///
///  
///  
/// </summary>
public static class ExtensionMethods_Coalescing {

   #region Stackoverflow 4795779

   /// <summary>
   /// !Q https://stackoverflow.com/a/854619/4795779
   ///
   /// 
   /// !Ex
   /// var one = new One();
   /// string fooIfNotNull = one.IfNotNull(x => x.Two).IfNotNull(x => x.Three);
   ///
   /// Nachteil: Die Kette muss von Hand codiert werden 
   /// 
   /// </summary>
   /// <param name="item"></param>
   /// <param name="lambda"></param>
   /// <typeparam name="T"></typeparam>
   /// <typeparam name="U"></typeparam>
   /// <returns></returns>
   public static T IfNotNull<T, U>(this U item, Func<U, T> lambda)
      where U : class {
      if (item == null) {
         return default(T);
      }

      return lambda(item);
   }

   #endregion Stackoverflow 4795779

   #region Stackoverflow 4795779

   /// <summary>
   /// !Q https://stackoverflow.com/a/2081942/4795779
   ///
   /// Vorteil: Vollautomatisch
   /// 
   /// !Ex
   /// var berries = cake.IfNotNull(c => c.Frosting.Berries);
   /// 
   /// </summary>
   /// <param name="arg"></param>
   /// <param name="expression"></param>
   /// <typeparam name="TArg"></typeparam>
   /// <typeparam name="TResult"></typeparam>
   /// <returns></returns>
   /// <exception cref="ArgumentNullException"></exception>
   /// <exception cref="ApplicationException"></exception>
   public static TResult IfNotNull<TArg, TResult>(this TArg arg, Expression<Func<TArg, TResult>> expression) {
      if (expression == null)
         throw new ArgumentNullException("expression");

      if (ReferenceEquals(arg, null))
         return default(TResult);

      var stack = new Stack<MemberExpression>();
      var expr  = expression.Body as MemberExpression;

      while (expr != null) {
         stack.Push(expr);
         expr = expr.Expression as MemberExpression;
      }

      if (stack.Count == 0 || !(stack.Peek().Expression is ParameterExpression))
         throw new ApplicationException($"The expression '{expression}' contains unsupported constructs.");

      object a = arg;

      while (stack.Count > 0) {
         expr = stack.Pop();
         var p = expr.Expression as ParameterExpression;

         if (p == null) {
            p    = Expression.Parameter(a.GetType(), "x");
            expr = expr.Update(p);
         }

         var      lambda = Expression.Lambda(expr, p);
         Delegate t      = lambda.Compile();
         a = t.DynamicInvoke(a);

         if (ReferenceEquals(a, null))
            return default(TResult);
      }

      return (TResult)a;
   }

   #endregion Stackoverflow 4795779

   #region Stackoverflow 4795779

   // !Q https://stackoverflow.com/a/4997173/4795779


   /// <summary>
   /// !Q https://stackoverflow.com/a/4997173/4795779
   ///
   /// !Ex
   ///  var converted = NullCoalesce((MethodInfo p) => p.DeclaringType.Assembly.Evidence.Locked);
   ///  var converted = NullCoalesce((string[] s) => s.Length);
   ///
   /// </summary>
   /// <param name="lambdaExpression"></param>
   /// <typeparam name="TSource"></typeparam>
   /// <typeparam name="TResult"></typeparam>
   /// <returns></returns>
   private static Expression<Func<TSource, TResult>> NullCoalesce<TSource, TResult>(
      Expression<Func<TSource, TResult>> lambdaExpression) {
      var test = GetTest(lambdaExpression.Body);

      if (test != null) {
         return Expression.Lambda<Func<TSource, TResult>>
            (
             Expression.Condition
                (
                 test
                 , lambdaExpression.Body
                 , Expression.Default
                    (
                     typeof(TResult)
                    )
                )
             , lambdaExpression.Parameters
            );
      }

      return lambdaExpression;
   }


   private static Expression GetTest(Expression expression) {
      Expression container;

      switch (expression.NodeType) {
         case ExpressionType.ArrayLength:
            container = ((UnaryExpression)expression).Operand;

            break;

         case ExpressionType.MemberAccess:
            if ((container = ((MemberExpression)expression).Expression) == null) {
               return null;
            }

            break;

         default:
            return null;
      }

      var baseTest = GetTest(container);

      if (!container.Type.IsValueType) {
         var containerNotNull = Expression.NotEqual
            (
             container
             , Expression.Default
                (
                 container.Type
                )
            );

         return (baseTest == null
                    ? containerNotNull
                    : Expression.AndAlso
                       (
                        baseTest
                        , containerNotNull
                       )
                );
      }

      return baseTest;
   }

   #endregion Stackoverflow 4795779

   #region Steve Wilkes, GetValueOrDefault

   
   /// <summary>
   /// !Q https://web.archive.org/web/20130116173708/http://geekswithblogs.net/mrsteve/archive/2013/01/10/getvalueordefault-expression-object-graph-arbitrary-length-depth-expression-tree.aspx
   ///
   /// !Ex
   ///   person.GetValueOrDefault(p => p.Address.Postcode.Value);
   ///   person.GetValueOrDefault(p => p.Address.Postcode.Value, "No postcode");
   ///
   /// If person, person.Address or person.Address.Postcode are null the default value is returned. 
   ///
   ///
   /// The ValueOrDefaultCache class contains a ConcurrentDictionary
   /// which caches functions which return a value or default for a given expression
   /// against the 'signature' of the expression itself.
   /// 
   /// Because ValueOrDefaultCache is static and generic,
   /// one dictionary and set of functions is cached per root
   /// and leaf object type, ensuring the functions are unique.
   ///
   ///
   /// 
   /// </summary>
   /// <param name="root"></param>
   /// <param name="property"></param>
   /// <param name="defaultValue"></param>
   /// <typeparam name="T"></typeparam>
   /// <typeparam name="TResponse"></typeparam>
   /// <returns></returns>
   public static TResponse GetValueOrDefault<T, TResponse>(
      this T                           root
      , Expression<Func<T, TResponse>> property
      , TResponse                      defaultValue = default(TResponse))
      where T : class {
      if (root == null)
         return defaultValue;

      return ValueOrDefaultCache<T, TResponse>.Cache
                                              .GetOrAdd(property.Body.ToString(), key => CreateValueOrDefaultLambda(property))
                                              .Invoke(root, defaultValue);
   }


   private static Func<T, TResponse, TResponse> CreateValueOrDefaultLambda<T, TResponse>(
      Expression<Func<T, TResponse>> property)
      where T : class {
      var rootParameter         = property.Parameters.First();
      var defaultValueParameter = Expression.Parameter(typeof(TResponse), "default");

      var        memberExpression         = (MemberExpression)property.Body;
      Expression valueOrDefaultExpression = null;

      while (memberExpression != null) {
         var memberDefaultValue = Expression.Default(memberExpression.Type);
         var memberIsNotDefault = Expression.NotEqual(memberExpression, memberDefaultValue);

         valueOrDefaultExpression = Expression.Condition
            (
             memberIsNotDefault
             , (valueOrDefaultExpression ?? memberExpression)
             , defaultValueParameter);

         memberExpression = memberExpression.Expression as MemberExpression;
      }

      var lambda = Expression.Lambda<Func<T, TResponse, TResponse>>
         (
          valueOrDefaultExpression
          , rootParameter
          , defaultValueParameter);

      return lambda.Compile();
   }


   private static class ValueOrDefaultCache<T, TResponse> {

      public static readonly ConcurrentDictionary<string, Func<T, TResponse, TResponse>> Cache =
         new ConcurrentDictionary<string, Func<T, TResponse, TResponse>>();

   }
   
   
   #endregion Steve Wilkes, GetValueOrDefault
   
   
}
