using jig.Tools.Exception;


namespace jig.Tools.DependencySupplierToClient;

/// <summary>
/// Bildet eine Dependency, also eine directed relationship, ab.
/// Die Abhängigkeit ist: supplier > client (Lieferant > Kunde)
/// Diese Klasse ist der Supplier
/// !Q https://www.uml-diagrams.org/dependency.html
///
/// Der Supplier merkt sich den Zeitstempel, zu dem der Wert gesetzt / geändert wurde 
/// </summary>
/// <typeparam name="T"></typeparam>
public record SupplierDataWithTimestamp<T> {

   private readonly object _lock = new();

   #region Props

   private volatile bool _isInitialized;

   public bool IsInitialized {
      get => _isInitialized;
      private set => _isInitialized = value;
   }

   /// <summary>
   /// Property des Objekts mit dem Zeitstempel, von dessen Gültigkeit wir abhängig sind
   /// </summary>
   public long TimestampDataValidSince { get; private set; }

   private T? _value;

   /// <summary>
   /// Die Daten
   /// </summary>
   public T? Value {
      get {
         lock (_lock) {
            return !IsInitialized ? default(T) : _value;
         }
      }
      set {
         lock (_lock) {
            _value                  = value;
            TimestampDataValidSince = DateTime.Now.Ticks;
            IsInitialized           = value != null;
         }
      }
   }

   #endregion Props

   #region Methoden

   /// <summary>
   /// Setzt das Element auf Modified
   /// </summary>
   public void Modified() {
      lock (_lock) {
         TimestampDataValidSince = DateTime.Now.Ticks;
      }
   }

   #endregion Methoden

   #region Konstruktoren

   /// <summary>
   /// Initialisiert das Obj,
   /// mit dem Status nicht initialisiert  
   /// </summary>
   public SupplierDataWithTimestamp() { IsInitialized = false; }


   /// <summary>
   /// Konstruktor
   /// Erzeugt das Element und speichert den Zeitstempel
   /// </summary>
   /// <param name="value"></param>
   public SupplierDataWithTimestamp(T? value) {
      TimestampDataValidSince = DateTime.Now.Ticks;
      Value                   = value;
      IsInitialized           = true;
   }

   #endregion Konstruktoren

}
