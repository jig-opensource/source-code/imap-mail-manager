using jig.Tools.ExpressionTree_Tools;


namespace jig.Tools.DependencySupplierToClient;

/// <summary>
/// Bildet eine Dependency, also eine directed relationship, ab.
/// Die Abhängigkeit ist: supplier > client (Lieferant > Kunde)
/// Diese Klasse ist der Client
/// 
/// !Q https://www.uml-diagrams.org/dependency.html
/// 
/// Ein einfacher Cache:
/// - Beim initialisieren der Daten erhalten wir den Initialisierungs-Zeitstempel
/// - Beim lesen der Daten erhalten wir einen Zeitstempel
///   Wenn dieser neuer als der Initialisierungs-Zeitstempel ist,
///   dann werden die Daten neu berechnet
///
/// Die Idee:
/// - Die Daten von CachedData sind von einem anderen Prop abhängig
/// - Wird das andere Prop verändert, dann merken wir uns den Änderungszeitstempel
/// - Wenn wir CachedData auslesen, wird geprüft,
///   ob der Prop Änderungszeitstempel neuer ist, als der CachedData Zeitstempel  
/// </summary>
/// <typeparam name="T"></typeparam>
public record CachedDependentClientData<T> {

   private readonly object _lock = new();

   #region Props

   private volatile bool _isInitialized;

   /// <summary>
   /// True, wenn die cached Daten initialisiert sind 
   /// </summary>
   public bool IsInitialized {
      get => _isInitialized;
      private init => _isInitialized = value;
   }

   /// <summary>
   /// True, wenn die cached Daten initialisiert und nicht veraltet sind
   /// </summary>
   public bool HasValidData {
      get {
         // Wenn die Daten initialisiert und nicht veraltet sind
         bool isValid;

         lock (_lock) {
            isValid = _isInitialized && DependentObj_TimestampProp_DataValidUntil.Get() <= _timestampDataValidUntil;
         }

         return isValid;
      }
   }

   /// <summary>
   /// Der Zeitstempel, bis zu diesem dieser Wert gültig ist
   /// Erhalten wir einen neueren Zeitstempel, dann wird _data neu berechnet   
   /// </summary>
   private long _timestampDataValidUntil;

   /// <summary>
   /// Die cached Daten
   /// </summary>
   private T? _value;

   /// <summary>
   /// Property mit dem Zeitstempel des Objekts, das die Gültigkeit unserer Daten definiert
   /// </summary>
   public Accessor<long> DependentObj_TimestampProp_DataValidUntil { get; set; }

   #endregion Props

   #region Konstruktoren

   public CachedDependentClientData(Accessor<long> dependentObjTimestampPropDataValidUntil) {
      IsInitialized                             = false;
      DependentObj_TimestampProp_DataValidUntil = dependentObjTimestampPropDataValidUntil;
   }


   public CachedDependentClientData(Accessor<long> dependentObjTimestampPropDataValidUntil, T? value)
      : this(dependentObjTimestampPropDataValidUntil, DateTime.Now.Ticks, value) {}


   public CachedDependentClientData(Accessor<long> dependentObjTimestampPropDataValidUntil, long timestamp, T? value) {
      _value                                    = value;
      _timestampDataValidUntil                  = timestamp;
      DependentObj_TimestampProp_DataValidUntil = dependentObjTimestampPropDataValidUntil;
      IsInitialized                             = true;
   }

   #endregion Konstruktoren


   /// <summary>
   /// Liest den Value,
   /// wenn validUntilTimestamp älter als der Timestamp des Objekts ist,
   /// sonst wird der Wert neu berechnet 
   /// </summary>
   /// <param name="validUntilTimestamp"></param>
   /// <param name="getterFunc"></param>
   /// <returns></returns>
   public T? GetCachedValue(Func<T?> getterFunc) {
      // Wenn unsere Daten veraltet sind, dann den Wert neu holen
      lock (_lock) {
         if (!_isInitialized || DependentObj_TimestampProp_DataValidUntil.Get() > _timestampDataValidUntil) {
            // Den neuen Zeitstempel merken
            _timestampDataValidUntil = DependentObj_TimestampProp_DataValidUntil.Get();

            // Den neuen Wert berechnen
            _value         = getterFunc();
            _isInitialized = true;
         }
         return _value;
      }
   }


   /// <summary>
   /// Setzt den Value und merkt sich den Zeitstempel
   /// </summary>
   /// <param name="value"></param>
   public void SetCachedValue(T? value) {
      lock (_lock) {
         _timestampDataValidUntil = DateTime.Now.Ticks;
         _value                   = value;
         _isInitialized           = true;
      }
   }

}
