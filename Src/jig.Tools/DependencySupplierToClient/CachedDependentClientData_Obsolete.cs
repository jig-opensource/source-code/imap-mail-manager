namespace jig.Tools.DependencySupplierToClient;

/// <summary>
/// Bildet eine Dependency, also eine directed relationship, ab.
/// Die Abhängigkeit ist: supplier > client (Lieferant > Kunde)
/// Diese Klasse ist der Client
/// !Q https://www.uml-diagrams.org/dependency.html
/// 
/// Ein einfacher Cache:
/// - Beim initialisieren der Daten erhalten wir den Initialisierungs-Zeitstempel
/// - Beim lesen der Daten erhalten wir einen Zeitstempel
///   Wenn dieser neuer als der Initialisierungs-Zeitstempel ist,
///   dann werden die Daten neu berechnet
///
/// Die Idee:
/// - Die Daten von CachedData sind von einem anderen Prop abhängig
/// - Wird das andere Prop verändert, dann merken wir uns den Änderungszeitstempel
/// - Wenn wir CachedData auslesen, wird geprüft,
///   ob der Prop Änderungszeitstempel neuer ist, als der CachedData Zeitstempel  
/// </summary>
/// <typeparam name="T"></typeparam>
[Obsolete("Dieser record ist obsolete » Siehe: record CachedDependentClientData<T>", false)]
public record CachedDependentClientData_Obsolete<T> {

   private readonly object _lock = new();

   #region Props

   private volatile bool _isInitialized;

   /// <summary>
   /// 
   /// </summary>
   public bool IsInitialized {
      get => _isInitialized;
      private set => _isInitialized = value;
   }

   /// <summary>
   /// Der Zeitstempel, bis zu diesem dieser Wert gültig ist
   /// Erhalten wir einen neueren Zeitstempel, dann wird _data neu berechnet   
   /// </summary>
   private long _timestampDataValidUntil;

   /// <summary>
   /// Die cached Daten
   /// </summary>
   private T? _value;

   #endregion Props

   #region Konstruktoren

   /// <summary>
   /// Initialisiert das Obj,
   /// mit dem Status nicht initialisiert  
   /// </summary>
   public CachedDependentClientData_Obsolete() {
      IsInitialized = false;
   }
   
   /// <summary>
   /// Konstruktor
   /// Erzeugt das Element mit dem jetztigen Zeitstempel
   /// </summary>
   /// <param name="value"></param>
   public CachedDependentClientData_Obsolete(T? value) : this(DateTime.Now.Ticks, value) {}
   
   /// <summary>
   /// Konstruktor
   /// Erzeugt das Element mit dem angegebenen Zeitstempel
   /// </summary>
   /// <param name="timestamp"></param>
   /// <param name="value"></param>
   public CachedDependentClientData_Obsolete(long timestamp, T? value) {
      IsInitialized           = true;
      _timestampDataValidUntil = timestamp;
      _value                   = value;
   }

   #endregion Konstruktoren


   /// <summary>
   /// Liest den Value,
   /// wenn validUntilTimestamp älter als der Timestamp des Objekts ist,
   /// sonst wird der Wert neu berechnet 
   /// </summary>
   /// <param name="validUntilTimestamp"></param>
   /// <param name="getterFunc"></param>
   /// <returns></returns>
   public T? GetCachedValue(long validUntilTimestamp, Func<T?> getterFunc) {
      // Wenn unsere Daten veraltet sind, dann den Wert neu holen
      if (!IsInitialized || validUntilTimestamp > _timestampDataValidUntil) {
         lock (_lock) {
            // Den neuen Zeitstempel merken
            _timestampDataValidUntil = validUntilTimestamp;

            // Den neuen Wert berechnen
            _value         = getterFunc();
            IsInitialized = true;
         }
      }

      lock (_lock) { return _value; }
   }


   /// <summary>
   /// Setzt den Value und merkt sich den Zeitstempel
   /// </summary>
   /// <param name="value"></param>
   public void SetCachedValue(T? value) {
      lock (_lock) {
         _timestampDataValidUntil = DateTime.Now.Ticks;
         _value                   = value;
         IsInitialized           = true;
      }
   }

}
