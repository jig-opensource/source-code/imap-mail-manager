using System.Text;
using System.Text.RegularExpressions;
using jig.Tools.String;


namespace jig.Tools;

/// <summary>
/// Klasse zum Konvertieren des E-Mail Empfnagsdatum
/// in / von rfc822 / rfc2822
///
/// RFC822
/// https: //www.rfc-editor.org/rfc/rfc822
///  ‼ Obsoleted by RFC2822
/// 
/// RFC 2822
/// https: //www.rfc-editor.org/rfc/rfc2822
/// </summary>
public static class ExtensionMethods_rfc822_rfc2822 {


   #region Regex RFC822 / RFC2822 DateTime: Erkennen von Fehlern in den Headern

   /// <summary>
   /// Testet, ob ein RFC822 / RFC2822 Zeitstempel die RFC verletzt
   /// und die Zeitzone als Stundenangabe +/-NNNN
   /// und gleichzeitig als Namenskürzel anzeigt 
   /// </summary>
   static readonly RegexOptions RgxOptions_RgxRFC822RFC2822_TestTimezone = RegexOptions.IgnoreCase
                                                                | RegexOptions.Multiline
                                                                | RegexOptions.ExplicitCapture
                                                                | RegexOptions.CultureInvariant
                                                                | RegexOptions
                                                                  .IgnorePatternWhitespace
                                                                | RegexOptions.Compiled;

   public static string SRgxRFC822RFC2822_TestTimezone = @"# RFC822, RFC2822 Date-Time Zeitstempel im Mail Header
                                       # Manche Mail-Clients und Server ignorieren den Standrd
                                       # und haben die Zeitzone als Stunden und gleichzeitig als Namen
                                       # Dieser Regex prüft, ob beide Elemente vorhanden sind

                                       # RFC 822
                                       # https://www.rfc-editor.org/rfc/rfc822
                                       # Obsoleted by RFC 2822
                                       # Date and Time Specification:
                                       # https://www.rfc-editor.org/rfc/rfc822#section-5

                                       # RFC 2822
                                       # https://www.rfc-editor.org/rfc/rfc2822
                                       # Date and Time Specification:
                                       # https://www.rfc-editor.org/rfc/rfc2822#section-3.3


                                       (?<Time>\d{2}:\d{2}:\d{2})[\t ]+
                                       (?:
                                         (?:
                                           (?<TimezoneHours>(?:\+|-)\d{4})
                                           |
                                           (?<TimezoneName>\(?(?:[a-zA-Z]{3}|UT|[a-zA-Z])\)?)
                                         )[\t ]?
                                       )*";
   public static readonly Regex ORgxRFC822RFC2822_TestTimezone = new Regex
      (SRgxRFC822RFC2822_TestTimezone, RgxOptions_RgxRFC822RFC2822_TestTimezone);

   #endregion Regex RFC822 / RFC2822 DateTime: Erkennen von Fehlern in den Headern


   /// <summary>
   /// Manche Mail-Clients und Server ignorieren den Standrd
   /// und haben die Zeitzone als Stunden und gleichzeitig als Namen
   /// Dieser Regex prüft, ob beide Elemente vorhanden sind
   ///
   /// Diese Funktion entfernt einen allenfalls vorhandenen (und im Standard nicht mehr gültigen) Zeitzonen-Namen,
   /// wenn die Zeitzone in Stunden angegeben wurde  
   /// 
   /// </summary>
   /// <param name="timestamp"></param>
   /// <returns></returns>
   public static string Extract_RFC2822_Timestamp(string timestamp) {

      // Verletzt unser Tiestamp die RFC-Regeln?
      var match = ORgxRFC822RFC2822_TestTimezone.Match(timestamp);

      // Kein Match - fertig
      if (!match.Success) { return timestamp; }

      var timezoneHours = match.Groups["TimezoneHours"];
      var timezoneName  = match.Groups["TimezoneName"];
      
      // Wenn beide gruppen matchen, dann haben wir einen Fehler
      if (timezoneHours.Success && timezoneName.Success) {
         // Den timezoneName entfernen
         StringBuilder newTimestamp = new StringBuilder();
         // Den Text vor dem Named Timestamp
         newTimestamp.Append( timestamp.Substring(0, timezoneName.Index));
         // Den Text nach dem Named Timestamp
         newTimestamp.Append( timestamp.ØSubstring(timezoneName.Index + timezoneName.Length));

         return newTimestamp.ToString().ØTrimAndReduce();
      }

      // Nicht beide Zeitzonen gleichzeitig angegeben - fertig
      return timestamp;
   }
  
   /// <summary>
   /// Konvertiert ein DateTime in einen Rfc822 Rfc2822 String 
   /// RFC 822
   /// https: //www.rfc-editor.org/rfc/rfc822
   ///  ‼ Obsoleted by RFC 2822
   /// 
   /// RFC 2822
   /// https: //www.rfc-editor.org/rfc/rfc2822
   /// </summary>
   /// <param name="datum"></param>
   /// <returns></returns>
   public static string ØTo_Rfc822Rfc2822_Str(this DateTimeOffset datum) {
      //+ String erzeugen
      string rfc822Rfc2822Time = datum.ToString("r");

      //+ GMT mit der TimeZone (+0100) ersetzen 
      TimeZoneInfo tzLocal = TimeZoneInfo.Local;
      string       tzOffsetStr;
      tzOffsetStr = tzLocal.BaseUtcOffset.Hours > 0
                 ? $"+{tzLocal.BaseUtcOffset.Hours:00}00"
                 : $"-{tzLocal.BaseUtcOffset.Hours:00}00";
      return rfc822Rfc2822Time.Replace("GMT", tzOffsetStr);
   }


   /// <summary>
   /// Liefert true, wenn der String ein Rfc822 / Rfc2822 Timestamp ist
   /// RFC 822
   /// https: //www.rfc-editor.org/rfc/rfc822
   ///  ‼ Obsoleted by RFC 2822
   /// 
   /// RFC 2822
   /// https: //www.rfc-editor.org/rfc/rfc2822
   /// </summary>
   /// <param name="datumRfc822Rfc2822"></param>
   /// <returns></returns>
   public static bool ØIs_Rfc822Rfc2822_Date(this string? datumRfc822Rfc2822) {
      if (!datumRfc822Rfc2822.ØHasValue()) { return false; }
      return DateTimeOffset.TryParse(datumRfc822Rfc2822, out _);
   }

   /// <summary>
   /// Konvertiert den Rfc822 / Rfc2822 Timestamp String in ein DateTimeOffset 
   /// RFC 822
   /// https: //www.rfc-editor.org/rfc/rfc822
   ///  ‼ Obsoleted by RFC 2822
   /// 
   /// RFC 2822
   /// https: //www.rfc-editor.org/rfc/rfc2822
   /// </summary>
   /// <param name="datumRfc822Rfc2822"></param>
   /// <returns></returns>
   public static DateTimeOffset? ØFrom_Rfc822Rfc2822(this string? datumRfc822Rfc2822) {
      if (!datumRfc822Rfc2822.ØHasValue()) { return null; }

      if (DateTimeOffset.TryParse(datumRfc822Rfc2822, out var res)) {
         return res;
      }

      return null;
   }

}
