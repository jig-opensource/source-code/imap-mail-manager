using System.Linq.Expressions;
using System.Reflection;
using jig.Tools.Exception;


namespace jig.Tools.ExpressionTree_Tools;

/// <summary>
/// Accessor<T> Dient zum Speichern einer Referenz auf ein Property
/// Der Wert des Props kann dann via Accessor.Get() abgerufen werden
/// 
/// !Ex Referenz auf das WorkPhone Prop:
///   var accessor = new Accessor<string>(() => myClient.WorkPhone);
///   accessor.Set("12345");
///   Assert.Equal(accessor.Get(), "12345");
///
/// !Q https://stackoverflow.com/a/43498938/4795779 
/// </summary>
/// <typeparam name="T"></typeparam>
public class Accessor<T> {

   /// <summary>
   /// TomTom: Nullable
   /// Das Objekt-Prop hat ev. keinen Setter
   /// </summary>
   private readonly Action<T>? _setter;

   /// <summary>
   /// TomTom: Nullable
   /// Das Objekt-Prop hat ev. keinen Getter
   /// </summary>
   private readonly Func<T>? _getter;


   public Accessor(Expression<Func<T>> expr) {
      var memberExpression   = (MemberExpression)expr.Body;
      var instanceExpression = memberExpression.Expression;
      var parameter          = Expression.Parameter(typeof(T));

      if (memberExpression.Member is PropertyInfo propertyInfo) {
         // TomTom
         bool hasSetter = propertyInfo.GetSetMethod() != null;
         bool hasGetter = propertyInfo.GetGetMethod() != null;

         if (hasSetter) {
            _setter = Expression.Lambda<Action<T>>
               (Expression.Call(instanceExpression, propertyInfo.GetSetMethod(), parameter), parameter).Compile();
         }

         if (hasGetter) {
            _getter = Expression.Lambda<Func<T>>(Expression.Call(instanceExpression, propertyInfo.GetGetMethod())).Compile();
         }
      }
      else if (memberExpression.Member is FieldInfo fieldInfo) {
         _setter = Expression.Lambda<Action<T>>(Expression.Assign(memberExpression, parameter), parameter).Compile();
         _getter = Expression.Lambda<Func<T>>(Expression.Field(instanceExpression, fieldInfo)).Compile();
      }
      else {
         throw new RuntimeException("class Accessor<T>, Konstruktor: Code für den Member Typ noch nicht implementiert");
      }

   }


   public void Set(T value) {
      if (_setter != null) {
        _setter.Invoke(value);
      }
      else {
         throw new RuntimeException("class Accessor<T>, Set(): Das zugeordnete Obj-Prop hat keinen Setter!");
      }
   }


   public T Get() {
      if (_getter != null) {
         return _getter();
      }
      else {
         throw new RuntimeException("class Accessor<T>, Set(): Das zugeordnete Obj-Prop hat keinen Getter!");
      }
   }

}
