using System.Text.RegularExpressions;
using jig.Tools.String;


namespace jig.Tools;

public static class ExtensionMethods_List {

   /// <summary>
   /// Liefert True, wenn liste Elemente hat
   /// </summary>
   /// <param name="liste"></param>
   /// <typeparam name="T"></typeparam>
   /// <returns></returns>
   public static bool ØHasItems<T>(this List<T>? liste) {
      if (liste != null) {
         return liste.Count > 0;
      }

      return false;
   }

   
   /// <summary>
   /// Ergänzt die Liste, wenn das Element noch fehlt
   /// </summary>
   /// <param name="liste"></param>
   /// <param name="item"></param>
   /// <typeparam name="T"></typeparam>
   /// <returns>
   /// True: Die Liste wurde ergänzt
   /// </returns>
   public static bool ØAppendItem_IfMissing<T>(this List<T>? liste, T? item) {
      if (liste != null) {
         if (!liste.Contains(item!)) {
            liste.Add(item!);
            return true;
         }
      }

      return false;
   }

   /// <summary>
   /// Ergänzt die Liste mit den neuen Elementen in additionalItems
   /// wenn neue Elemente noch nicht in Liste existieren 
   /// </summary>
   /// <param name="liste"></param>
   /// <param name="additionalItems"></param>
   /// <typeparam name="T"></typeparam>
   /// <returns></returns>
   public static void ØAppendItem_IfMissing<T>(this List<T>? liste, List<T>? additionalItems) {
      if (liste != null && additionalItems != null) {
         foreach (var additionalItem in additionalItems) {
            liste.ØAppendItem_IfMissing(additionalItem);
         }
      }
   }

   public static List<T>? ØMerge<T>(this List<T>? liste, List<T>? additionalItems) {
      if (liste != null && additionalItems != null) {
         foreach (var additionalItem in additionalItems) {
            liste.ØAppendItem_IfMissing(additionalItem);
         }
      }
      return liste;
   }


   /// <summary>
   /// Prüft, ob aTestItem mit einem Element aus der Liste beginnt
   /// </summary>
   /// <param name="liste"></param>
   /// <param name="aTestItem"></param>
   /// <param name="comparisonType"></param>
   /// <returns></returns>
   public static bool ØStartsWithAnyItemInList(this List<string>? liste, string? aTestItem
                                               , StringComparison comparisonType = StringComparison.OrdinalIgnoreCase) {
      if (liste == null) { return false; }

      if (!aTestItem.ØHasValue()) { return false; }

      foreach (var listenelement in liste) {
         if (aTestItem!.StartsWith(listenelement, comparisonType)) {
            return true;
         }
      }

      return false;
   }

   /// <summary>
   /// 
   /// </summary>
   /// <param name="liste"></param>
   /// <param name="aTestItem"></param>
   /// <param name="comparisonType"></param>
   /// <returns></returns>
   public static bool ØListItem_ContainsSubstring(this List<string>? liste, string? aTestItem, StringComparison comparisonType = StringComparison.OrdinalIgnoreCase) {
      if (liste == null) { return false; }
      if (!aTestItem.ØHasValue()) { return false; }

      foreach (var listenelement in liste) {
         if (
            listenelement.Contains(aTestItem!, comparisonType)) {
            return true;
         }
      }

      return false;
   }


   /// <summary>
   /// Prüft, ob rgxTestItem mit einem Element aus der Liste matched
   /// </summary>
   /// <param name="liste"></param>
   /// <param name="rgxTestItem"></param>
   /// <param name="rgxOptions"></param>
   /// <returns></returns>
   public static bool ØRgxMatchWithAnyItemInList(
      this List<string>? liste
      , string?          rgxTestItem
      , RegexOptions
         rgxOptions = RegexOptions.IgnoreCase) {
      if (liste == null) { return false; }

      if (!rgxTestItem.ØHasValue()) { return false; }

      foreach (var listenelement in liste) {
         if (Regex.Match
                (rgxTestItem!
                 , listenelement
                 , rgxOptions).Success) {
            return true;
         }
      }

      return false;
   }


   /// <summary>
   /// Prüft, ob aTestItem mit einem Element aus der Liste exakt übereinstimmt
   /// </summary>
   /// <param name="liste"></param>
   /// <param name="aTestItem"></param>
   /// <returns></returns>
   public static bool ØEqualsWithAnyItemInList(this List<string>? liste, string? aTestItem) {
      if (liste == null) { return false; }

      if (!aTestItem.ØHasValue()) { return false; }

      foreach (var listenelement in liste) {
         if (aTestItem!.Equals(listenelement, StringComparison.OrdinalIgnoreCase)) {
            return true;
         }
      }

      return false;
   }


   /// <summary>
   /// Löscht ein Element aus der Liste
   /// und verhält sich so wie erwartet, ohne Exceptions und sos
   /// </summary>
   /// <param name="liste"></param>
   /// <param name="position"></param>
   public static void ØRemoveAt(this List<string>? liste, int? position) {
      if (liste == null || position == null || position < 0) { return; }

      if (position >= liste.Count) { return; }

      liste.RemoveAt((int)position);
   }

}
