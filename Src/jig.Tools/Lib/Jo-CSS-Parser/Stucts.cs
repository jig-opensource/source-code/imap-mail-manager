﻿using System.Collections.Generic;


namespace jig.Tools.Lib.Jo_CSS_Parser {

   /// <summary>
   /// Property Info (Property Name and Property Value_.
   /// </summary>
   public struct Property {

      public string PropertyName;
      public string PropertyValue;

   };


   public struct TagWithCSS {

      // In fact, this is the CSS Selector of a CSS Rule Set  
      public string TagName;
      // all properties defines the Css Declaration Block
      public List<Property> Properties;

   };

}
