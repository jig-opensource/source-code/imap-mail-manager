using System.Globalization;
using jig.Tools.String;


namespace jig.Tools;

/// <summary>
/// Tools für das Handlung mit den Sprachcodes:
/// - ISO 639-1   2 Char Codes
/// - ISO 639-2   3 Char Codes
///
/// !M https://www.loc.gov/standards/iso639-2/php/code_list.php
/// </summary>
public static class ExtensionMethods_CultureInfo {

   /// <summary>
   /// Konvertiert einen 2 oder 3 Char Sprachcode
   /// in eine .Net CultureInfo
   /// 
   /// </summary>
   /// <param name="sprachCode">
   /// !M https://learn.microsoft.com/de-de/dotnet/api/system.globalization.cultureinfo.-ctor?view=net-7.0
   /// !Ex
   ///   3 char
   ///      DEU, ENG, FRA
   ///   2 char oder Präziser:
   ///      de, fr-FR
   /// </param>
   /// <returns></returns>
   public static CultureInfo ØGetLangCultureInfo(this string sprachCode) {
      if (!sprachCode.ØHasValue()) {
         return CultureInfo.GetCultureInfo("de");
      }

      sprachCode = sprachCode.Trim();

      return sprachCode.Length switch {
         // !M https://learn.microsoft.com/de-de/dotnet/api/system.globalization.cultureinfo.getcultures?view=net-7.0
         3 => CultureInfo
               .GetCultures(CultureTypes.NeutralCultures)
               .FirstOrDefault(c => c.ThreeLetterISOLanguageName.ØEqualsIgnoreCase(sprachCode))

         // !M https://learn.microsoft.com/de-de/dotnet/api/system.globalization.cultureinfo.-ctor?view=net-7.0
         , _ => CultureInfo.GetCultureInfo(sprachCode)
      } ?? CultureInfo.GetCultureInfo("de");
   }

}
