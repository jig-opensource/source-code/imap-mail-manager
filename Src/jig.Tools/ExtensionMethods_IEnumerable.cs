namespace jig.Tools;

public static class ExtensionMethods_IEnumerable {

   /// <summary>
   /// Erlaubt in foreach(… in nullable)
   /// mit Null Objekten zu arbeiten
   /// </summary>
   /// <param name="source"></param>
   /// <typeparam name="T"></typeparam>
   /// <returns></returns>
   public static IEnumerable<T> ØOrEmptyIfNull<T>(this IEnumerable<T>? source) { return source ?? Enumerable.Empty<T>(); }


   /// <summary>
   /// Konvertiert ein null-obj in eine Enumerable.Empty<T> 
   /// </summary>
   /// <param name="source"></param>
   /// <typeparam name="T"></typeparam>
   /// <returns></returns>
   public static object ØOrEmptyIfNull<T>(this object? source) { return source ?? Enumerable.Empty<T>(); }

}
