namespace jig.Tools; 

public static class ExtensionMethods_Dictionary {

   
   /// <summary>
   /// Ergänzt im Dict key + data, wenn der Eintrag noch nicht existiert
   /// Sonst wird key neu gesetzt
   ///
   /// Wenn dict null ist, wird dict initialisiert 
   /// </summary>
   /// <param name="dict"></param>
   /// <param name="key"></param>
   /// <param name="data"></param>
   /// <typeparam name="K"></typeparam>
   /// <typeparam name="D"></typeparam>
   public static void ØAddorSetData<K, D>(this Dictionary<K,D>? dict, K key, D data) {
      if (dict != null) {
         dict = new Dictionary<K, D>();
      }

      if (dict.ContainsKey(key)) {
         dict.Add(key, data);
      }
      else {
         dict[key] = data;
      }
   }


}
