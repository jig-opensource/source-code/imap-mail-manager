using System.Diagnostics;


namespace jig.Tools;

/// <summary>
/// ExtensionMethods für HashSets
/// </summary>
public static class ExtensionMethodsHashSet {

   /// <summary>
   /// Vergleicht zwei Listen und liefert true, wenn mind. 1 Element in beiden Listen vorhanden sind 
   /// </summary>
   /// <param name="list1"></param>
   /// <param name="list2"></param>
   /// <typeparam name="TSource"></typeparam>
   /// <returns></returns>
   public static bool ØHatSchnittmenge<TSource>(this IEnumerable<TSource>? list1, IEnumerable<TSource>? list2) {
      // Wenn beide null sind, sind sie vermutlich als identisch zu werten
      if (list1 == null && list2 == null) {
         return true;
      }

      // Wenn nur eines null ist, hat es keine Schnittmenge
      if (list1 == null || list2 == null) {
         return false;
      }

      // Enumerable.IntersectBy
      // !Tut  https://learn.microsoft.com/de-de/dotnet/csharp/programming-guide/concepts/linq/set-operations?branch=main#intersect-and-intersectby
      // !M    https://learn.microsoft.com/de-de/dotnet/api/system.linq.enumerable.intersectby?view=net-7.0
      var schnittmenge = list1.Intersect(list2);

      return schnittmenge.Any();
   }


   /// <summary>
   /// Vergleicht zwei Listen und liefert true, wenn mind. 1 Property in beiden Listen identisch sind 
   ///
   /// !Ex
   ///  objList.ØHatSchnittmengeBy(objList2, x => x)
   ///  objList.ØHatSchnittmengeBy(objList2, x => x.name)
   /// 
   /// </summary>
   /// <param name="list1"></param>
   /// <param name="list2"></param>
   /// <param name="classSelector"></param>
   /// <typeparam name="TSource"></typeparam>
   /// <typeparam name="TKey"></typeparam>
   /// <returns></returns>
   public static bool ØHatSchnittmengeBy<TSource, TKey>(
      this IEnumerable<TSource>? list1
      , IEnumerable<TKey>?       list2
      , Func<TSource, TKey>      classSelector) {
      // Wenn beide null sind, sind sie vermutlich als identisch zu werten
      if (list1 == null && list2 == null) {
         return true;
      }

      // Wenn nur eines null ist, hat es keine Schnittmenge
      if (list1 == null || list2 == null) {
         return false;
      }

      // Enumerable.IntersectBy
      // !Tut  https://learn.microsoft.com/de-de/dotnet/csharp/programming-guide/concepts/linq/set-operations?branch=main#intersect-and-intersectby
      // !M    https://learn.microsoft.com/de-de/dotnet/api/system.linq.enumerable.intersectby?view=net-7.0
      var schnittmenge = list1.IntersectBy(list2, classSelector);

      return schnittmenge.Any();
   }


   /// <summary>
   /// Vergleicht zwei Listen und liefert true, wenn mind. 1 Property in beiden Listen identisch sind
   /// Der User definiert einen eigenen Comparer:
   /// IEqualityComparer<TKey> 
   ///
   /// !Ex
   ///  » UnitTest_ExtensionMethodsHashSet.Test_ØHatSchnittmengeBy_StringArr_WithComparer
   ///  » UnitTest_ExtensionMethodsHashSet.Test_ØHatSchnittmengeBy
   /// 
   ///  objList.ØHatSchnittmengeBy(objList2, x => x)
   ///  objList.ØHatSchnittmengeBy(objList2, x => x.name)
   /// 
   /// </summary>
   ///
   /// <param name="list1"></param>
   /// <param name="list2"></param>
   /// <param name="keySelector"></param>
   /// <typeparam name="TSource"></typeparam>
   /// <typeparam name="TKey"></typeparam>
   /// <returns></returns>
   public static bool ØHatSchnittmengeBy<TSource, TKey>(
      this IEnumerable<TSource>? list1
      , IEnumerable<TKey>?       list2
      , Func<TSource, TKey>      keySelector
      , IEqualityComparer<TKey>  comparer) {
      // Wenn beide null sind, sind sie vermutlich als identisch zu werten
      if (list1 == null && list2 == null) {
         return true;
      }

      // Wenn nur eines null ist, hat es keine Schnittmenge
      if (list1 == null || list2 == null) {
         return false;
      }

      // Enumerable.IntersectBy
      // !Tut  https://learn.microsoft.com/de-de/dotnet/csharp/programming-guide/concepts/linq/set-operations?branch=main#intersect-and-intersectby
      // !M    https://learn.microsoft.com/de-de/dotnet/api/system.linq.enumerable.intersectby?view=net-7.0

      // !Tut https://learn.microsoft.com/de-de/dotnet/api/system.collections.generic.iequalitycomparer-1?view=net-7.0
      var schnittmenge = list1.IntersectBy(list2, keySelector, comparer);

      return schnittmenge.Any();
   }

}
