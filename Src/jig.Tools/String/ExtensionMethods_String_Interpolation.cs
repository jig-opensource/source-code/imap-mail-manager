using Fluid;


namespace jig.Tools.String;

public static class ExtensionMethods_String_Interpolation {

   /// <summary>
   /// Interpretiert expression, der auf obj Props zugreifen kann
   /// </summary>
   /// <param name="obj"></param>
   /// <param name="expression"></param>
   /// <param name="variableName"></param>
   /// <typeparam name="T"></typeparam>
   /// <returns></returns>
   public static string ØInterpolate<T>(this T? obj, string? expression, string? variableName) {
      if (obj == null) { return ""; }
      if (!expression.ØHasValue()) { return ""; }
      if (!variableName.ØHasValue()) { return ""; }
      
      // TemplateOptions.Default.MemberAccessStrategy.Register<T> (variableName);
      TemplateOptions options = new TemplateOptions();
      options.MemberAccessStrategy = new UnsafeMemberAccessStrategy();
      var parser   = new FluidParser();
      var template = parser.Parse(expression);
      var context = new TemplateContext(options);
      context.SetValue(variableName, obj);
      return template.Render(context);
   }

}
