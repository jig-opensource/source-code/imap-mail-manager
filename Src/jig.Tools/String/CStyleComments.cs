using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;


namespace jig.Tools.String;

public class CStyleComments {

   /// <summary>
   /// Löscht in str alle C Style Comments:
   /// /* … */
   /// </summary>
   /// <param name="str"></param>
   /// <returns></returns>
   public static string Remove_CStyleComments(string str) {
      var _myRegexOptions = RegexOptions.IgnoreCase
                            | RegexOptions.Multiline
                            | RegexOptions.ExplicitCapture
                            | RegexOptions.CultureInvariant
                            | RegexOptions.Compiled;

      // Diese Kommentare: /* .. */
      // !KH9 https://blog.ostermiller.org/finding-comments-in-source-code-using-regular-expressions/
      var sRgxComment = @"(?<Comment>/\*(?:[^*]|[\r\n]|(?:\*+(?:[^*/]|[\r\n])))*\*+/)";
      var oRgxCStyleComment   = new Regex(sRgxComment, _myRegexOptions);

      int              LastEndofMatchPos = 0;
      StringBuilder    res               = new StringBuilder();
      MatchCollection? matchCollection   = null;

      do {
         // Alle Comments suchen
         matchCollection = oRgxCStyleComment.Matches(str, LastEndofMatchPos);

         // Jeden gefundenen Comment aus dem ursprünglichen Str entfernen 
         foreach (Match match in matchCollection) {
            // var    grpVal = match.Groups["Comment"].Value;
            string startSubStr = str.ØSubstring(LastEndofMatchPos, match.Groups["Comment"].Index - LastEndofMatchPos);
            LastEndofMatchPos = match.Groups["Comment"].Index + match.Groups["Comment"].Length;
            res.Append(startSubStr);
         }
      }
      while (matchCollection.Count > 0);

      // Vom letzten Kommentar bis zum Ende von str die Zeichen übernehmen
      string lastSubStr = str.ØSubstring(LastEndofMatchPos, str.Length - LastEndofMatchPos);
      res.Append(lastSubStr);
      // Debug.WriteLine(res.ToString());

      return res.ToString();
   }

}
