using System.Globalization;
using System.Text.RegularExpressions;


namespace jig.Tools.String;

/// <summary>
/// String Extension Methods for less annoying .Net String handling
/// (c) Thomas Schittli 
/// </summary>
public static class ExtensionMethods_Strings {

   // de: neutrale Kultur für DE
   // !M https://learn.microsoft.com/en-us/dotnet/api/System.Globalization.CultureInfo?view=net-6.0#culture-names-and-identifiers
   public static TextInfo CultureInfoDE = new CultureInfo("de").TextInfo;

   public static bool ØHasValue(this string? str) => !string.IsNullOrWhiteSpace(str);


   /// <summary>
   /// Splittet einen CamelCase String
   /// !Q https://stackoverflow.com/a/5796793/4795779
   /// </summary>
   /// <param name="str"></param>
   /// <returns></returns>
   public static string ØSplitCamelCase(this string? str) {
      if (!str.ØHasValue()) { return ""; }
      
      return Regex.Replace
         (
          Regex.Replace
             (
              str
              , @"(\P{Ll})(\P{Ll}\p{Ll})"
              , "$1 $2"
             )
          , @"(\p{Ll})(\P{Ll})"
          , "$1 $2"
         );
   }


   /// <summary>
   /// Liefert True, wenn str mit test beginnt
   /// </summary>
   /// <param name="str"></param>
   /// <param name="test"></param>
   /// <param name="comparisonType"></param>
   /// <returns></returns>
   public static bool ØStartsWith(this string? str, string? test, StringComparison comparisonType) {
      if (str == null || test == null) return false;

      return str!.StartsWith(test, comparisonType);
   }


   /// <summary>
   /// Liefert True, wenn str test beinhaltet
   /// </summary>
   /// <param name="str"></param>
   /// <param name="test"></param>
   /// <param name="comparisonType"></param>
   /// <returns></returns>
   public static bool ØContains(this string? str, string? test, StringComparison comparisonType) {
      if (str == null || test == null) return false;

      return str!.Contains(test, comparisonType);
   }


   /// <summary>
   /// Wenn str mit trimStartStr beginnt,
   /// dann wird in str dieser Start-String entfernt
   /// </summary>
   /// <param name="str"></param>
   /// <param name="trimStartStr"></param>
   /// <param name="comparisonType"></param>
   /// <returns></returns>
   public static string ØTrimStringStart(
      this string? str
      , string?    trimStartStr
      , StringComparison
         comparisonType = StringComparison.Ordinal) {
      if (str.ØHasValue() && trimStartStr.ØHasValue()) {
         var pos = str!.IndexOf(trimStartStr!, comparisonType);

         //+ Gefunden am Anfang
         if (pos == 0) {
            // TrimStart 
            return str.ØSubstring(trimStartStr!.Length);
         }
      }

      return "";
   }


   /// <summary>
   /// Untersucht, welcher der möglichen Delimiter in str existieren
   /// </summary>
   /// <param name="str"></param>
   /// <param name="possibleDelimiters"></param>
   /// <returns>
   /// null    Keinen oder mehrere passende Delimiter gefunden
   /// char    der eindeutig gefundene Delimiter
   /// </returns>
   public static char? ØGetDelimimter(this string? str, char[] possibleDelimiters) {
      if (str.ØHasValue()) {
         List<char> foundDelimiters = new List<char>();

         //+ Alle Delimiter prüfen
         foreach (var possibleDelimiter in possibleDelimiters) {
            if (str!.Contains(possibleDelimiter)) {
               foundDelimiters.Add(possibleDelimiter);
            }
         }

         //+ Nur, wenn wir nur 1 Delimter gefundenhaben, ist alles i.O.
         switch (foundDelimiters.Count) {
            case 0: {
               return null;
            }

            case 1: {
               return foundDelimiters[0];
            }

            default: {
               return null;
            }
         }
      }

      return null;
   }


   /// <summary>
   /// Vergleicht die beiden Strings, indem alle white spaces entfernt werden,
   /// so dass z.B. CSS oder HTML Elemente verglichen werden können 
   /// </summary>
   /// <param name="str"></param>
   /// <param name="that"></param>
   /// <returns></returns>
   public static bool ØCompareStringWithouWhiteChars(
      this string? str
      , string?    other
      , StringComparison?
         comparisonType) {
      //+ Beide null oder leer
      if (!str.ØHasValue() && !other.ØHasValue()) {
         return true;
      }

      //+ Nur einer null oder leer
      if (!str.ØHasValue() || !other.ØHasValue()) {
         return false;
      }

      //+ Vergleichen
      // Die white spaces entfernen
      string left  = str!.ØRemoveWhiteSpaces();
      string right = other!.ØRemoveWhiteSpaces();

      if (comparisonType == null) {
         return left.Equals(right);
      }
      else {
         return left.Equals(right, (StringComparison)comparisonType!);
      }
   }


   /// <summary>
   /// Vergleicht zwei Strings ohne Gross-/Kleinschreibung
   /// </summary>
   /// <param name="str"></param>
   /// <param name="other"></param>
   /// <returns></returns>
   public static bool ØEqualsIgnoreCase(
      this string? str
      , string?    other) {
      //+ Beide null oder leer
      if (!str.ØHasValue() && !other.ØHasValue()) {
         return true;
      }

      //+ Nur einer null oder leer
      if (!str.ØHasValue() || !other.ØHasValue()) {
         return false;
      }

      //+ Vergleichen
      return str!.Equals(other, StringComparison.OrdinalIgnoreCase);
   }


   /// <summary>
   /// Der normale Vergleich mit nullable
   /// </summary>
   /// <param name="str"></param>
   /// <param name="other"></param>
   /// <returns></returns>
   public static bool ØEquals(
      this string? str
      , string?    other) {
      //+ Beide null oder leer
      if (!str.ØHasValue() && !other.ØHasValue()) { return true; }

      //+ Nur einer null oder leer
      if (!str.ØHasValue() || !other.ØHasValue()) { return false; }

      //+ Vergleichen
      return str!.Equals(other);
   }


   /// <summary>
   /// True, wenn EndWith ohne Gross-/Kleinschreibung stimmt
   /// </summary>
   /// <param name="str"></param>
   /// <param name="other"></param>
   /// <returns></returns>
   public static bool ØEndsWithIgnoreCase(
      this string? str
      , string?    other) {
      //+ Beide null oder leer
      if (!str.ØHasValue() && !other.ØHasValue()) {
         return true;
      }

      //+ Nur einer null oder leer
      if (!str.ØHasValue() || !other.ØHasValue()) {
         return false;
      }

      //+ Vergleichen
      // Die white spaces entfernen
      return str!.EndsWith(other!, StringComparison.OrdinalIgnoreCase);
   }


   /// <summary>
   /// Extension-Method for Strings
   /// 
   /// You can call "".ØSubstring() and you always get what you expect
   /// and you never get an exception as M$ .Net is doing if a parameter is 'invalid' :-(
   ///  
   /// </summary>
   /// <param name="str"></param>
   /// <param name="start"></param>
   /// <param name="length"></param>
   /// <returns></returns>
   public static string ØSubstring(this string str, int start, int? length = null) {
      if (ØHasValue(str)) {
         if (start < 0) { start = 0; }

         if (!length.HasValue) { length = str.Length - start; }

         var calcStart = Math.Min(str.Length,             start);
         var calcLen   = Math.Min(str.Length - calcStart, (int)length);

         return str.Substring(calcStart, calcLen);
      }

      return "";
   }


   /// <summary>
   /// Ergänzt das Prefix, wenn es nicht schon existiert
   /// </summary>
   /// <param name="str"></param>
   /// <param name="prefix"></param>
   /// <returns></returns>
   public static string ØAddPrefix(this string? str, string? prefix) {
      if (ØHasValue(str) && ØHasValue(prefix)) {
         return str!.StartsWith(prefix!) ? str : $"{prefix}{str}";
      }

      return "";
   }


   /// <summary>
   /// Ergänzt das Suffix, wenn es nicht schon existiert
   /// </summary>
   /// <param name="str"></param>
   /// <param name="suffix"></param>
   /// <returns></returns>
   public static string ØAddSuffix(this string? str, string? suffix) {
      if (ØHasValue(str) && ØHasValue(suffix)) {
         if (str!.EndsWith(suffix!)) {
            return str;
         }
         else {
            return $"{suffix}{str}";
         }
      }

      return "";
   }


   /// <summary>
   /// jesus christus » Jesus Christus
   /// </summary>
   /// <param name="str"></param>
   /// <returns></returns>
   public static string ØToTitleCase(this string str) {
      if (ØHasValue(str)) {
         // !M https://learn.microsoft.com/de-de/dotnet/api/system.globalization.textinfo
         // !M https://learn.microsoft.com/en-us/dotnet/api/System.Globalization.CultureInfo
         return CultureInfoDE.ToTitleCase(str);
      }

      return "";
   }


   /// <summary>
   /// Löscht leere Zeilen
   /// </summary>
   /// <param name="str"></param>
   /// <returns></returns>
   public static string ØRemoveEmptyLines(this string str) {
      if (ØHasValue(str)) {
         return Regex.Replace
            (str
             , @"^([\t ]*\r?\n)*"
             , string.Empty
             , RegexOptions.Multiline).Trim('\r', '\n');
      }

      return "";
   }


   /// <summary>
   /// Löscht alle white spaces
   /// </summary>
   /// <param name="str"></param>
   /// <returns></returns>
   public static string ØRemoveWhiteSpaces(this string str) {
      if (ØHasValue(str)) {
         return Regex.Replace
            (str
             , @"\s*"
             , string.Empty
             , RegexOptions.Multiline);
      }

      return "";
   }


   /// <summary>
   /// Ersetzt alle mehrfachen White Spaces mit einem " "
   /// </summary>
   /// <param name="str"></param>
   /// <param name="keepCrLf">
   /// Wenn true, dann bleiben CrLf erhalten,
   /// aber aufeinanderfolgende werden auf 1 CrLf reduziert  
   /// </param>
   /// <returns></returns>
   public static string ØReduceWhiteSpaces(this string str, bool keepCrLf = false) {
      if (ØHasValue(str)) {
         if (keepCrLf) {
            return str.ØRemoveEmptyLines().ØReduceSpaces();
         }
         else {
            return Regex.Replace
               (str
                , @"\s+"
                , " "
                , RegexOptions.Multiline).Trim();
         }
      }

      return "";
   }


   /// <summary>
   /// Ersetzt alle mehrfachen White Spaces (ohne CRLF) mit einem Leerzeichen
   /// </summary>
   /// <param name="str"></param>
   /// <returns></returns>
   public static string ØReduceSpaces(this string str) {
      if (ØHasValue(str)) {
         //+ Alle mehrfachen Leezeichen auf eines reduzieren
         var res1 = Regex.Replace
                          (str
                           , @"[\t ]+"
                           , " "
                           , RegexOptions.Multiline)
                         .Trim();

         //+ Alle Leezeichen am Start und Ende jeder Zeile löschen
         // ‼! !Q https://stackoverflow.com/a/31782094/4795779
         return Regex.Replace
                      (res1
                       , @"^[\t ]+|\ +(?=(\n|\r?$))"
                       , ""
                       , RegexOptions.Multiline)
                     .Trim();
      }

      return "";
   }


   /// <summary>
   /// float in B, KB, MB, ... formatieren
   /// </summary>
   /// <param name="bytes"></param>
   /// <param name="Nachkommastellen"></param>
   /// <param name="Delimiter"></param>
   /// <returns></returns>
   public static string ØToUnitStr(
      this float bytes
      , int      Nachkommastellen = 3
      , string   Delimiter        = " ") {
      string[] suffix = { "B", "KB", "MB", "GB", "TB" };
      int      i;
      double   doubleBytes = 0;

      for (i = 0; (int)(bytes/1024) > 0; i++, bytes /= 1024) {
         doubleBytes = bytes/1024.0;
      }

      switch (Nachkommastellen) {
         case 0:
            return string.Format
               ("{0:0}{1}{2}"
                , doubleBytes
                , Delimiter
                , suffix[i]);

         case 1:
            return string.Format
               ("{0:0.0}{1}{2}"
                , doubleBytes
                , Delimiter
                , suffix[i]);

         case 2:
            return string.Format
               ("{0:0.00}{1}{2}"
                , doubleBytes
                , Delimiter
                , suffix[i]);

         default:
         case 3:
            return string.Format
               ("{0:0.000}{1}{2}"
                , doubleBytes
                , Delimiter
                , suffix[i]);
      }
   }

   /// <summary>
   /// Null-Tolerantes Trim()
   /// </summary>
   /// <param name="str"></param>
   /// <returns></returns>
   public static string ØTrim(this string? str) {
      if (str.ØHasValue()) {
         return str!.Trim();
      }
      return "";
   }


   /// <summary>
   /// str.Trim()
   /// und dann alle mehrfachen Leerzeichen auf jeweils eines reduzieren
   /// </summary>
   /// <param name="str"></param>
   /// <returns></returns>
   public static string ØTrimAndReduce(this string str) => Regex.Replace
      (str
       , @"\s+"
       , " ").Trim();


   /// <summary>
   /// Zähle die Anz. Worte, die getrennt sind durch
   /// ' ', '.', '?'
   /// </summary>
   /// <param name="str"></param>
   /// <returns></returns>
   public static int ØWordCount(this string str) => str.Split
                                                        (
                                                         new[] { ' ', '.', '?' }
                                                         , StringSplitOptions.RemoveEmptyEntries)
                                                       .Length;


   /// <summary>
   /// Erzeugt aus einem String einen MemoryStream
   /// ab dem dann gelesen werden kann
   ///
   /// !Q https://stackoverflow.com/a/1879470/4795779
   /// !i Alternative mit Codireung
   ///   https://stackoverflow.com/a/5238289/4795779
   /// </summary>
   /// <param name="str"></param>
   /// <returns></returns>
   public static MemoryStream ØToStream(this string str) {
      var stream = new MemoryStream();
      var writer = new StreamWriter(stream);
      writer.Write(str);
      writer.Flush();
      stream.Position = 0;

      return stream;
   }

}
