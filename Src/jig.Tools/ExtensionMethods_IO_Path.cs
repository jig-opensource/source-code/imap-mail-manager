using System.Text;
using System.Text.RegularExpressions;
using jig.Tools.String;
using PCRE;


namespace jig.Tools;

/// <summary>
/// Berienigt Strings, so dass sie für Datei-Systeme gültig sind  
/// </summary>
public static class ExtensionMethods_IO_Path {

   #region Filesystem Sonderzeichen

   private static readonly char[] InvalidPathChars = Path.GetInvalidPathChars();
   private static readonly char[] InvalidFileChars = Path.GetInvalidFileNameChars();

   /// <summary>
   /// Mein eigenes Mapping
   /// </summary>
   private static readonly char[][] FilePathCharMapping = {
      new[] { '<', '❮' }
      , // ‹
      new[] { '>', '❯' }
      , // ›
      new[] { '|', '┃' }
      , new[] { '"', 'ʺ' }, // ¯  ˝
      new[] { '*', '✸' }
      , // ★ ✲ ✶ ✷  ✸  ✹
      new[] { '?', '¿' }
   };

   private static readonly char[][] FileNameCharMapping = {
      new[] { ':', '꞉' }, // ᎓
      new[] { '<', '❮' }
      , // ‹
      new[] { '>', '❯' }
      , // › 
      new[] { '|', '┃' }
      , new[] { '"', 'ʺ' }, // ¯  ˝
      new[] { '*', '✸' }
      , // ★ ✲ ✶ ✷  ✸  ✹
      new[] { '?', '¿' }
      , new[] { '\\', '╲' }, new[] { '/', '╱' } // ∕  ⁄  ╱
   };

   #endregion Filesystem Sonderzeichen

   #region RegEx c# String Composite formatting

   // !M https://learn.microsoft.com/en-us/dotnet/standard/base-types/composite-formatting

   private static readonly PcreOptions RgxOpt_CSharpStringCompositeFormat =
      PcreOptions.Compiled
      | PcreOptions.IgnoreCase
      | PcreOptions.Caseless
      | PcreOptions.IgnorePatternWhitespace
      | PcreOptions.ExplicitCapture

      // | PcreOptions.MultiLine
      // | PcreOptions.Singleline
      | PcreOptions.Unicode;

   private const string SRgx_CSharpStringCompositeFormat = @"
                        # Erkennt Elemente wie: {…..}
                        # c:\Temp\Datei {0:D2}.txt
                        # !M https://learn.microsoft.com/en-us/dotnet/standard/base-types/composite-formatting
                        (?<DotNetStrMarker>{[^}]+})";

   public static readonly PcreRegex ORgx_CSharpStringCompositeFormat = new PcreRegex
      (SRgx_CSharpStringCompositeFormat, RgxOpt_CSharpStringCompositeFormat);

   #endregion RegEx c# String Composite formatting


   /// <summary>
   /// Entfernt aus einem c# composite String den Platzhalter
   /// Erzeugt aus:
   ///   c:\Temp\Datei {0:D2}.txt
   /// » c:\Temp\Datei.txt
   /// 
   ///   c:\Temp\Datei {0:D2} Suffix.txt
   /// » c:\Temp\Datei Suffix.txt
   /// </summary>
   /// <param name="pattern"></param>
   /// <returns></returns>
   public static string Trim_CSharpCompositeString(string pattern) {
      if (!pattern.ØHasValue()) { return ""; }

      // Haben wir ein Muster?
      var match      = ORgx_CSharpStringCompositeFormat.Match(pattern);
      var matchGroup = match.Groups["DotNetStrMarker"];

      if (!matchGroup.Value.ØHasValue()) { return pattern; }

      //+ Ist vor und nach dem Match ein Space?
      var idxBefore = matchGroup.Index - 1;
      var idxAfter  = matchGroup.Index + matchGroup.Length;

      var hasSpaceBefore = idxBefore > 0
                           && pattern[idxBefore] == ' ';

      var hasSpaceAfter = idxAfter < pattern.Length
                          && pattern[idxAfter] == ' ';

      var hasDotBefore = idxBefore > 0
                           && pattern[idxBefore] == '.';

      var hasDotAfter = idxAfter < pattern.Length
                          && pattern[idxAfter] == '.';

      var hasSpaceBeforeAndAfter = hasSpaceBefore && hasSpaceAfter;

      // Den String bereinigen
      var vorher  = pattern.Substring(0, matchGroup.Index);
      var nachher = pattern.Substring(matchGroup.Index + matchGroup.Length);

      // In diesen Fällen ein Space entfernen
      if (matchGroup.Index == 0 && hasSpaceAfter) {
         // Das erste Char im nachher löschen
         nachher = nachher.Substring(1);
      } else if (matchGroup.Index + matchGroup.Length == pattern.Length && hasSpaceBefore) {
         // Das letzte Char im vorher löschen
         vorher = vorher.Substring(0, vorher.Length - 1);
      } else if (hasSpaceBefore && hasDotAfter) {
         // Das letzte Char im vorher löschen
         vorher = vorher.Substring(0, vorher.Length - 1);
      } else if (hasDotBefore && hasSpaceAfter) {
         // Das erste Char im nachher löschen
         nachher = nachher.Substring(1);
      } else if (hasSpaceBeforeAndAfter) {
         // Das letzte Char im vorher löschen
         vorher = vorher.Substring(0, vorher.Length - 1);
      }
      return vorher + nachher;
   }


   /// <summary>
   /// Prüft, ob ein Dateiname schon vorhanden ist.
   /// Wenn ja, wird mit einer fortlaufenden Nummer ein neer Dateiname berechnet
   ///
   /// Konzept:
   /// - Übergeben wird ein Muster, z.B.:
   ///   c:\Temp\Datei {0:D2}.txt
   /// - geprüft wird:
   ///   c:\Temp\Datei.txt
   ///   c:\Temp\Datei 02.txt
   ///   c:\Temp\Datei 02.txt
   ///   c:\Temp\Datei 03.txt
   /// 
   /// !Q https://stackoverflow.com/a/1078898/4795779
   /// </summary>
   /// <param name="fullFileName"></param>
   /// <returns></returns>
   public static string GetUniqueNumberedFilename(string fullFileName) {
      //+ Den Filenamen ohne CSharp Composite Muster
      var fullFileNameWithoutNumber = Trim_CSharpCompositeString(fullFileName);
      if (fullFileNameWithoutNumber == fullFileName) {
         throw new ArgumentException("The pattern must include the index place-holder: `{0`}", "fullFileName");
      }

      //+ Die Datei ohne Nummer existiert noch nicht
      if (!File.Exists(fullFileNameWithoutNumber)) { return fullFileNameWithoutNumber; }

      // Mit der ersten Datei prüfen, ob ein Pattern übergeben wurde "{0:D2}"
      // !KH: D2: zwei Dezimalstellen, kann auch mehr sein
      int    fileNo = 2;
      while (File.Exists(string.Format(fullFileName, fileNo))) {
         fileNo++;
      }

      return string.Format(fullFileName, fileNo);
   }


   /// <summary>
   /// Bereingt einen String,
   /// so dass er nur noch für Pfade und Dateinamen gültige Zeichen beinhaltet 
   /// </summary>
   /// <param name="path"></param>
   /// <returns></returns>
   public static string ØStringToFilename(this string? path) {
      if (!path.ØHasValue()) { return ""; }

      var Pfad     = Path.GetDirectoryName(path);
      var FileName = Path.GetFileName(path);

      /*
            foreach (char[] chars in filePathCharMapping) {
               Pfad     = Pfad.Replace(chars[0], chars[1]);
               FileName = FileName.Replace(chars[0], chars[1]);
            }
      */

      //+ Den Pfad bereinigen
      StringBuilder resPfad = new StringBuilder();

      for (int idxPfad = 0; idxPfad < Pfad.Length; idxPfad++) {
         var  thisPfadChar    = Pfad[idxPfad];
         bool charFoundInPath = false;

         for (var idxChrMap = 0; idxChrMap < FilePathCharMapping.Length; idxChrMap++) {
            if (thisPfadChar == FilePathCharMapping[idxChrMap][0]) {
               charFoundInPath = true;
               resPfad.Append(FilePathCharMapping[idxChrMap][1]);

               break;
            }
         }

         //+ Wenn nicht gefunden
         if (!charFoundInPath) {
            for (int idxInvalidPathChars = 0; idxInvalidPathChars < InvalidPathChars.Length; idxInvalidPathChars++) {
               if (thisPfadChar == InvalidPathChars[idxInvalidPathChars]) {
                  charFoundInPath = true;

                  // Space ergänzen, wenn das letzte Zeichen nicht schon ein Space ist
                  if (resPfad[^1] != ' ') {
                     resPfad.Append(' ');
                  }

                  break;
               }
            }
         }

         //+ Wenn immer noch nicht gefunden, dann ist das Zeichen i.O.
         if (!charFoundInPath) { resPfad.Append(thisPfadChar); }
      }

      //+ Den Dateinamen bereinigen
      StringBuilder resFileName = new StringBuilder();

      for (int idxFileName = 0; idxFileName < FileName.Length; idxFileName++) {
         var  thisFileNameChar    = FileName[idxFileName];
         bool charFoundInFileName = false;

         for (var idxChrMap = 0; idxChrMap < FileNameCharMapping.Length; idxChrMap++) {
            if (thisFileNameChar == FileNameCharMapping[idxChrMap][0]) {
               charFoundInFileName = true;
               resFileName.Append(FileNameCharMapping[idxChrMap][1]);

               break;
            }
         }

         //+ Wenn nicht gefunden
         if (!charFoundInFileName) {
            for (int idxInvalidPathChars = 0; idxInvalidPathChars < InvalidFileChars.Length; idxInvalidPathChars++) {
               if (thisFileNameChar == InvalidFileChars[idxInvalidPathChars]) {
                  charFoundInFileName = true;

                  // Space ergänzen, wenn das letzte Zeichen nicht schon ein Space ist
                  if (resFileName[^1] != ' ') {
                     resFileName.Append(' ');
                  }

                  break;
               }
            }
         }

         //+ Wenn immer noch nicht gefunden, dann ist das Zeichen i.O.
         if (!charFoundInFileName) { resFileName.Append(thisFileNameChar); }
      }

      var res = Path.Join(resPfad.ToString().Trim(), resFileName.ToString().Trim());

      return res;

   }


   /// <summary>
   /// Entfernt von einem Pfad das Root Dir
   /// Kommt mit \ und / zurecht
   /// </summary>
   /// <param name="path"></param>
   /// <returns></returns>
   public static string ØRemoveRootDir(this string? path) {
      if (!path.ØHasValue()) {
         return "";
      }

      //+ Haben wir einen Pfad Delimiter?
      var posDelim = path!.IndexOfAny(new[] { '\\', '/' });

      if (posDelim < 0) {
         //+ wir haben nur das Hauptverzeichnis, ohne SubDir
         return "";
      }
      else {
         //+ Wir haben mind. ein SubDir
         //++ Den Delimiter des Root-Dirs brauchen wir nicht
         return path.ØSubstring(++posDelim);
      }
   }


   /// <summary>
   /// Ersetzt das Root Dir
   /// </summary>
   /// <param name="path"></param>
   /// <returns></returns>
   public static string ØReplaceRootDir(this string? path, string? replaceName) {
      if (!path.ØHasValue()) {
         return "";
      }

      //+ Kein neuer Name definiert
      if (!replaceName.ØHasValue()) {
         // Das Root Dir löschen
         return path.ØRemoveRootDir();
      }

      //+ Haben wir einen Pfad Delimiter?
      var posDelim = path!.IndexOfAny(new[] { '\\', '/' });

      if (posDelim < 0) {
         //+ wir haben nur das Hauptverzeichnis, ohne SubDir
         return replaceName!;
      }
      else {
         //+ Wir haben mind. ein SubDir
         //++ Den Delimiter des Root Dirs brauchen wir nicht
         //+ Im neuen Namen das Delimiter entfernen
         var replaceNameClean = replaceName!.TrimEnd(new[] { '\\', '/' });

         return
            $"{replaceNameClean}{path.ØSubstring(posDelim)}";
      }
   }

}
