using System.Reflection;
using System.Xml.Schema;


namespace jig.Tools; 

/// <summary>
/// Erweiterungsmethoden für die Reflection
/// </summary>
public static class ExtensionMethods_Reflection {


   /// <summary>
   /// Scan all the types in an assembly
   /// !Q https://haacked.com/archive/2012/07/23/get-all-types-in-an-assembly.aspx/
   /// </summary>
   /// <param name="assembly"></param>
   /// <returns></returns>
   /// <exception cref="ArgumentNullException"></exception>
   public static IEnumerable<Type> GetLoadableTypes(this Assembly assembly) {
      if (assembly == null) throw new ArgumentNullException("assembly");

      try {
         return assembly.GetTypes();
      } catch (ReflectionTypeLoadException e) {
         return e.Types.Where(t => t != null);
      }
   }

   /// <summary>
   /// Sucht in asm alle Typen, die das Interface typeInterface implementieren
   /// </summary>
   /// <param name="asm"></param>
   /// <param name="typeInterface"></param>
   /// <returns></returns>
   public static IEnumerable<Type> GetTypesWithInterface(Assembly asm, Type typeInterface) {
      return asm.GetLoadableTypes().Where(typeInterface.IsAssignableFrom).ToList();
   }


   /// <summary>
   /// Sucht in der ganzen Applikaiton alle Typen, die das Interface typeInterface implementieren
   /// </summary>
   /// <param name="typeInterface"></param>
   /// <returns></returns>
   public static List<Type> GetApp_Types_ImplementingInterface(Type typeInterface) {
      List<Type> foundClasses = new List<Type>();
      var assemblies = AppDomain.CurrentDomain.GetAssemblies();
      foreach (var assembly in assemblies) {
         IEnumerable<Type> typesWithInterface = ExtensionMethods_Reflection.GetTypesWithInterface
            (assembly, typeInterface);
         var withInterface = typesWithInterface as Type[] ?? typesWithInterface.ToArray();

         if (withInterface.Any()) {
            foundClasses.AddRange(withInterface.Where(x => x.IsClass));
         }
      }
      return foundClasses;
   }

}
