using System.Collections;
using jig.Tools.String;


namespace jig.Tools;

/// <summary>
/// Extension Methos für Enums Flags
/// </summary>
public static class ExtensionMethods_Enum {

   /// <summary>
   /// Enums haben per default den zugrundeliegenden Wert Int32
   /// Diese Funktion konvertiert die Enum
   /// 
   /// !M https://learn.microsoft.com/de-de/dotnet/csharp/language-reference/builtin-types/enum
   /// </summary>
   /// <param name="val"></param>
   /// <returns></returns>
   /// <exception cref="ArgumentOutOfRangeException"></exception>
   public static Int32 ToNumber(this Enum val) {
      if (Convert.GetTypeCode(val) != TypeCode.Int32) {
         throw new ArgumentOutOfRangeException("val");
      }
      return Convert.ToInt32(val);
   }

   /// <summary>
   /// Generischer Funktion zum konvertoeren einer enum in eine UInt64
   /// 
   /// !M https://learn.microsoft.com/de-de/dotnet/csharp/language-reference/builtin-types/enum
   /// </summary>
   /// <param name="val"></param>
   /// <returns></returns>
   /// <exception cref="ArgumentOutOfRangeException"></exception>
   public static UInt64? ØTox64(this Enum? val) {
      if (val == null) { return null; }

      switch (Convert.GetTypeCode(val)) {
         case TypeCode.SByte:
         case TypeCode.Int16:
         case TypeCode.Int32:
         case TypeCode.Int64:
            return (ulong)Convert.ToInt64(val);

         case TypeCode.Byte:
         case TypeCode.UInt16:
         case TypeCode.UInt32:
         case TypeCode.UInt64:
            return Convert.ToUInt64(val);

         default:
            throw new ArgumentOutOfRangeException("val");
      }
   }


   /// <summary>
   /// Parst str und liefert null oder den parsed enum wert  
   /// </summary>
   /// <param name="str"></param>
   /// <param name="defaultEnumValue"></param>
   /// <typeparam name="T"></typeparam>
   /// <returns>
   /// liefert:
   /// - den parsed enum wert
   /// - oder null
   /// </returns>
   public static T? ØParseToEnum<T>(this string str)
      where T : struct, Enum {
      if (!str.ØHasValue()) { return default(T); }

      if (Enum.TryParse(str, out T parsed)) {
         return parsed;
      }

      return default(T);
   }


   /// <summary>
   /// Parst str und liefert den parsed enum wert oder defaultEnumValue  
   /// </summary>
   /// <param name="str"></param>
   /// <param name="defaultEnumValue"></param>
   /// <typeparam name="T"></typeparam>
   /// <returns>
   /// liefert:
   /// - den parsed enum wert
   /// - defaultEnumValue
   /// </returns>
   public static T ØParseToEnum<T>(this string str, T defaultEnumValue)
      where T : struct, Enum {
      if (!str.ØHasValue()) { return defaultEnumValue; }

      if (Enum.TryParse(str, out T parsed)) {
         return parsed;
      }

      return defaultEnumValue;
   }


   /// <summary>
   /// Püft, ob eine Enumeration die mind. erwartete Anzahl Elemente hat
   ///
   /// !Q https://stackoverflow.com/a/27180014/4795779
   /// </summary>
   /// <param name="source"></param>
   /// <param name="count"></param>
   /// <typeparam name="TSource"></typeparam>
   /// <returns></returns>
   public static bool ØHasCountOfAtLeast<TSource>(this IEnumerable<TSource>? source, int count) {
      if (source == null) { return false; }

      //+ Haben wir eine ICollection<>? Dann direkt prüfen 
      if (source is ICollection<TSource> iCollectionT) {
         return iCollectionT.Count >= count;
      }

      //+ Haben wir eine ICollection? Dann direkt prüfen 
      if (source is ICollection iCollection) {
         return iCollection.Count >= count;
      }

      // Können wir so viele Elemente holen, wie wir brauchen?
      return source.Take(count).Count() == count;
   }


   /// <summary>
   /// Liefert true, wenn in einer [Flags] Enum ein Bit gesetzt ist
   /// </summary>
   /// <param name="flags"></param>
   /// <param name="flag"></param>
   /// <typeparam name="T"></typeparam>
   /// <returns></returns>
   public static bool IsSet<T>(this T flags, T flag)
      where T : struct {
      int flagsValue = (int)(object)flags;
      int flagValue  = (int)(object)flag;

      return (flagsValue & flagValue) != 0;
   }


   /// <summary>
   /// Setzt in einer [Flags] Enum ein Bit
   /// </summary>
   /// <param name="flags"></param>
   /// <param name="flag"></param>
   /// <typeparam name="T"></typeparam>
   public static void Set<T>(this ref T flags, T flag)
      where T : struct {
      int flagsValue = (int)(object)flags;
      int flagValue  = (int)(object)flag;

      flags = (T)(object)(flagsValue | flagValue);
   }


   /// <summary>
   /// Löscht in einer [Flags] Enum ein Bit
   /// </summary>
   /// <param name="flags"></param>
   /// <param name="flag"></param>
   /// <typeparam name="T"></typeparam>
   public static void Unset<T>(this ref T flags, T flag)
      where T : struct {
      int flagsValue = (int)(object)flags;
      int flagValue  = (int)(object)flag;

      flags = (T)(object)(flagsValue & (~flagValue));
   }

}
