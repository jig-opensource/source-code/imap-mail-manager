namespace jig.Tools; 

public static class ExtensionMethods_DateTime {

   /// <summary>
   /// Erzeugt einen TimeStamp,
   /// default: yyMMdd HHmm
   /// </summary>
   /// <param name="format"></param>
   /// <returns></returns>
   public static string Get_ActualTimeStamp(string format = "yyMMdd HHmm") {
      return DateTime.Now.ToString(format);
   }   

   /// <summary>
   /// Erzeugt einen TimeStamp,
   /// default: yyMMdd HHmm
   /// </summary>
   /// <param name="dateTime"></param>
   /// <param name="format"></param>
   /// <returns></returns>
   public static string ØGet_TimeStamp(this DateTime? dateTime, string format = "yyMMdd HHmm") {
      if (dateTime == null) {
         return ØGet_TimeStamp(DateTime.Now);
      }
      return dateTime.Value.ToString(format);
   }   
   public static string ØGet_TimeStamp(this DateTime dateTime, string format = "yyMMdd HHmm") {
      return dateTime.ToString(format);
   }   

   /// <summary>
   /// Erzeugt einen TimeStamp,
   /// default: yyMMdd HHmm
   /// </summary>
   /// <param name="dateTimeOffset"></param>
   /// <param name="format"></param>
   /// <returns></returns>
   public static string ØGet_TimeStamp(this DateTimeOffset? dateTimeOffset, string format = "yyMMdd HHmm") {
      if (dateTimeOffset == null) {
         return ØGet_TimeStamp(DateTime.Now);
      }
      return dateTimeOffset.Value.ToString(format);
   }   

   public static string ØGet_TimeStamp(this DateTimeOffset dateTimeOffset, string format = "yyMMdd HHmm") {
      return dateTimeOffset.ToString(format);
   }   

}
