using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using jig.Tools.String;
using PCRE;
using PCRE.Dfa;


namespace jig.Tools;

public static class ExtensionMethods_PCRE2 {

   public static PcreMatch? ØMatch(this PcreRegex? pcreRegex, string? subject) {
      if (pcreRegex == null) { return null; }

      if (!subject.ØHasValue()) { return null; }

      return pcreRegex.Match(subject!);
   }


   /// <summary>
   /// Replace für PCRE / PCRE2
   /// </summary>
   /// <param name="pcreRegex"></param>
   /// <param name="subject"></param>
   /// <param name="replacement"></param>
   /// <returns></returns>
   public static string ØReplace(this PcreRegex? pcreRegex, string? subject, string? replacement) {
      if (pcreRegex == null) { return subject ?? ""; }

      if (!subject.ØHasValue()) { return ""; }

      if (!replacement.ØHasValue()) { return subject ?? ""; }

      return pcreRegex.Replace(subject!, replacement!);
   }


   /// <summary>
   /// Ersetzt alle gefundenen Gruppen im subject
   /// </summary>
   /// <param name="pcreRegex"></param>
   /// <param name="groupName"></param>
   /// <param name="subject"></param>
   /// <param name="replacement"></param>
   /// <param name="func"></param>
   /// <param name="predicate"></param>
   /// <returns>
   /// var( hasMatch, newString) = … 
   /// </returns>
   public static Tuple<bool, string> ØReplaceGroupName(
      this PcreRegex?       pcreRegex
      , string?             groupName
      , string?             subject
      , string?             replacement
      , Func<string, bool>? predicate = null) {

      // Expression<Func<string, bool>>? predicate = null

      if (pcreRegex == null) { return new Tuple<bool, string>(false, subject ?? ""); }

      if (!groupName.ØHasValue()) { return new Tuple<bool, string>(false, subject ?? ""); }

      if (!subject.ØHasValue()) { return new Tuple<bool, string>(false, ""); }

      if (!replacement.ØHasValue()) { return new Tuple<bool, string>(false, subject ?? ""); }

      //+ Haben wir mind. 1 TReffer?
      bool hasMatch = false;

      //+ Alle Named Groups ersetzen

      int           lastEndofMatchPos = 0;
      StringBuilder res               = new StringBuilder();
      PcreMatch     match;

      // Expression<Func<string, bool>>? predicate2 = (x => x.Count(char.IsDigit) > 6); 

      do {
         // Alle TelNr suchen und mi
         match = pcreRegex.Match(subject!, lastEndofMatchPos);
         if (match.Success) {
            // var grpVal  = match.Groups[groupName].Value;
            var matchgrp = match.Groups[groupName!];

            string strBeforeMatch = subject!.ØSubstring(lastEndofMatchPos, matchgrp.Index - lastEndofMatchPos);
            res.Append(strBeforeMatch);

            // Wenn wir kein predicate haben oder das predicate i.O. ist
            if (predicate == null || predicate(matchgrp.Value)) {
               hasMatch.IfFalse(() => hasMatch = true);
               res.Append(replacement);
            }
            else {
               // Das Predicate ergab false: die Fundstelle überspringen,
               // also debn Match übernehmen
               res.Append(matchgrp.Value);
            }

            lastEndofMatchPos = matchgrp.Index + matchgrp.Length;
         }
      }
      while (match.Success);

      // Von der letzten Fundstelle bis zum Ende die Zeichen übernehmen
      string lastSubStr = subject!.ØSubstring(lastEndofMatchPos, subject!.Length - lastEndofMatchPos);
      res.Append(lastSubStr);

      return new Tuple<bool, string>(hasMatch, res.ToString());
   }

}

