using System.Security.Cryptography;
using System.Text;
using jig.Tools.Exception;


namespace jig.Tools.Crypto;

/// <summary>
/// Tools zum einfachen Verschlüsseln und Entschlüsseln von Strings
/// </summary>
public class EncryptDecrypt {

   /// <summary>
   /// The Input String (UTF8) is sealed with a MD5-128-Bit-Hash and then Crypted to a Base64 String
   ///
   /// !Q https://www.c-sharpcorner.com/article/a-simple-crypto-safe-for-strings-in-c-sharp/
   /// </summary>
   /// <param name="originalText"></param>
   /// <returns></returns>
   public static string EncryptToBase64(string pw, string originalText) {
      var                userBytes = Encoding.UTF8.GetBytes(originalText); // UTF8 saves Space
      var                userHash  = MD5.Create().ComputeHash(userBytes);
      SymmetricAlgorithm crypt     = Aes.Create(); // (Default: AES-CCM (Counter with CBC-MAC))
      crypt.Key = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(pw)); // MD5: 128 Bit Hash
      crypt.IV  = new byte[16]; // by Default. IV[] to 0.. is OK simple crypt
      using var memoryStream = new MemoryStream();

      using var cryptoStream = new CryptoStream
         (memoryStream
          , crypt.CreateEncryptor()
          , CryptoStreamMode.Write);

      cryptoStream.Write
         (userBytes
          , 0
          , userBytes.Length); // User Data

      cryptoStream.Write
         (userHash
          , 0
          , userHash.Length); // Add HASH
      cryptoStream.FlushFinalBlock();
      var resultString = Convert.ToBase64String(memoryStream.ToArray());

      return resultString;
   }


   /// <summary>
   /// Try to get original (decrypted) String. Password (and Base64-format) must be correct
   ///
   /// !Q https://www.c-sharpcorner.com/article/a-simple-crypto-safe-for-strings-in-c-sharp/
   /// </summary>
   /// <param name="encryptedText"></param>
   /// <returns></returns>
   /// <exception cref="Exception"></exception>
   public static string DecryptFromBase64(string pw, string encryptedText) {
      var                encryptedBytes = Convert.FromBase64String(encryptedText);
      SymmetricAlgorithm crypt          = Aes.Create();
      crypt.Key = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(pw));
      crypt.IV  = new byte[16];
      using var memoryStream = new MemoryStream();

      using var cryptoStream = new CryptoStream
         (memoryStream
          , crypt.CreateDecryptor()
          , CryptoStreamMode.Write);

      cryptoStream.Write
         (encryptedBytes
          , 0
          , encryptedBytes.Length);
      cryptoStream.FlushFinalBlock();
      var allBytes = memoryStream.ToArray();
      var userLen  = allBytes.Length - 16;

      if (userLen < 0) throw new RuntimeException("Invalid Len"); // No Hash?
      var userHash = new byte[16];

      Array.Copy
         (allBytes
          , userLen
          , userHash
          , 0
          , 16); // Get the 2 Hashes

      var decryptHash = MD5.Create().ComputeHash
         (allBytes
          , 0
          , userLen);

      if (userHash.SequenceEqual(decryptHash) == false) throw new RuntimeException("Invalid Hash");

      var resultString = Encoding.UTF8.GetString
         (allBytes
          , 0
          , userLen);

      return resultString;
   }

}
