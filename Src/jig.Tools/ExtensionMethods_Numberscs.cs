using jig.Tools.String;


namespace jig.Tools; 

public static class ExtensionMethods_Numbers {

   /// <summary>
   /// int in B, KB, MB, ... formatieren
   /// </summary>
   /// <param name="bytes"></param>
   /// <param name="nachkommastellen"></param>
   /// <param name="delimiter"></param>
   /// <returns></returns>
   public static string ØToUnitStr(this int bytes, int nachkommastellen = 3, string delimiter = " ")
      => ((float)bytes).ØToUnitStr(nachkommastellen, delimiter);


   /// <summary>
   /// ulong in B, KB, MB, ... formatieren
   /// </summary>
   /// <param name="bytes"></param>
   /// <param name="nachkommastellen"></param>
   /// <param name="delimiter"></param>
   /// <returns></returns>
   public static string ØToUnitStr(this ulong bytes, int nachkommastellen = 3, string delimiter = " ")
      => ((float)bytes).ØToUnitStr(nachkommastellen, delimiter);

   /// <summary>
   /// ulong in B, KB, MB, ... formatieren
   /// </summary>
   /// <param name="bytes"></param>
   /// <param name="nachkommastellen"></param>
   /// <param name="delimiter"></param>
   /// <returns></returns>
   public static string ØToUnitStr(this long bytes, int nachkommastellen = 3, string delimiter = " ")
      => ((float)bytes).ØToUnitStr(nachkommastellen, delimiter);



}
