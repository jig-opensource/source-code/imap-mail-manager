using System.Web;
using HtmlAgilityPack;


namespace jig.Tools; 

public static class ExtensionMethods_HtmlAgilityPack {

   /// <summary>
   /// htmlNode.InnerText hat manchmal Html Codierte texte wie &auml;
   /// Desalb decodieren wir selber
   /// </summary>
   /// <param name="htmlNode"></param>
   /// <typeparam name="TSource"></typeparam>
   /// <returns></returns>
   public static string ØGetInnerText(this HtmlNode? htmlNode) {
      if (htmlNode == null) { return ""; }
      return HttpUtility.HtmlDecode(htmlNode.InnerHtml.Trim());
   }

}
