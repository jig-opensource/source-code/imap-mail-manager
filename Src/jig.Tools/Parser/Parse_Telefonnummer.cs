using System.Text;
using System.Text.RegularExpressions;
using jig.Tools.String;
using PCRE;


namespace jig.Tools.Parser; 

public class Parse_Telefonnummer {

   #region .Net RegEx Telefonnummer

   ///summary>
   /// Definition Kontaktformular:
   /// Vorname, Nachname
   /// </summary>
   static readonly RegexOptions RgxOptionssRgxEMailAddr = RegexOptions.IgnoreCase
                                                          | RegexOptions.Multiline
                                                          | RegexOptions.ExplicitCapture
                                                          | RegexOptions.CultureInvariant
                                                          | RegexOptions.IgnorePatternWhitespace
                                                          | RegexOptions.Compiled;

   /// <summary>
   /// Regex, um Tel-Nummern zu finden
   /// Annahme: Die TelNr hat > 6 Zahlen, um die Mail Refnr nicht als TelNr zu erkennen 
   /// </summary>
   private static readonly string SRgxTelNr = @"
                                       ## Vor der Nr. muss mind. 1 Leerzeichen oder der Zeilenanfang sein
                                       (?:[\p{Zs}\t]+|^)
                                       ## Die Telnr
                                       (?<TelNr>

                                       ## Die Vorwahl:
                                       # darf nicht mit einem # beginnen, und, wichtig: Exclude from capture
                                       # 0+       1 oder mehrere Nullen
                                       # \+[1-9]  Plus gefolgt von einer Zahl
                                       # ()0-9    1 oder mehrere Klammern oder Zahlen
                                       (?<Vorwahl>(?=[^#])(?:0+|\+[1-9]+|[()0-9]+)\p{Zs}*)

                                       ## Die Nummer mit Trennzeichen: Leerzeichen und -
                                       # 4 bis 20x :
                                       # - Leerzeichen
                                       # - Tab
                                       # - Digit
                                       # - Bindestrich
                                       (?<Nr>[)\p{Zs}\t\d\-/]{4,20}\d)
                                       )";
   public static readonly Regex ORgxTelNr = new Regex(SRgxTelNr, RgxOptionssRgxEMailAddr);

   #endregion .Net RegEx Telefonnummer


   #region PCRE2 RegEx Telefonnummer

   // !M https://github.com/ltrzesniewski/pcre-net#example-usage

   /// <summary>
   /// Regex, um Tel-Nummern zu finden
   /// Annahme: Die TelNr hat > 6 Zahlen, um die Mail Refnr nicht als TelNr zu erkennen 
   /// </summary>
   private static readonly string SRgxPcre2TelNr = @"
                                       ## Vor der Nr. muss mind. 1 Leerzeichen oder der Zeilenanfang sein
                                       (?:[\p{Zs}\t]+|^)
                                       ## Die Telnr
                                       (?<TelNr>

                                         ## Die Vorwahl:
                                         # darf nicht mit einem # beginnen, und, wichtig: Exclude from capture
                                         # 0+       1 oder mehrere Nullen
                                         # \+[1-9]  Plus gefolgt von einer Zahl
                                         # ()0-9    1 oder mehrere Klammern oder Zahlen
                                         (?<Vorwahl>(?=[^#])(?:0+|\+[1-9]+|[()0-9]+)\p{Zs}*)

                                         ## Die Nummer mit Trennzeichen: Leerzeichen und -
                                         # 4 bis 20x :
                                         # - Klammern
                                         # - Leerzeichen
                                         # - Tab
                                         # - Digit
                                         # - Bindestrich
                                         (?<Nr>[)\p{Zs}\t\d\-/()]{4,20}\d)(?!\+)
                                       )
                                       # Muss mit einem Leerzeichen, dem Start eines HTML Tags oder EOL enden
                                       (?:[\p{Zs}\t<]+|$)";

   static readonly PcreOptions RgxPcre2OptionssRgxEMailAddr = PcreOptions.Compiled
                                                              | PcreOptions.IgnoreCase
                                                         | PcreOptions.ExplicitCapture
                                                         | PcreOptions.Caseless
                                                         | PcreOptions.IgnorePatternWhitespace
                                                         | PcreOptions.Unicode;

   public static PcreRegex ORgxTelNrPcre2 = new PcreRegex(SRgxPcre2TelNr, RgxPcre2OptionssRgxEMailAddr);

   
   
   
   #endregion PCRE2 RegEx Telefonnummer
   
   /// <summary>
   /// Ersetzt alle Tel.-Nummern mit dem Ersatz-Zeichen
   /// </summary>
   /// <param name="text"></param>
   /// <param name="replacement"></param>
   /// <returns>
   /// var (hasMatch, newText) = … 
   /// </returns>
   [Obsolete("Diese Funktion ist obsolete » Parse_Telefonnummer.Replace_Telefonnummer_Pcre2", false)]
   public static Tuple<bool, string> Replace_Telefonnummer(string? text, string? replacement) {
      if (!text.ØHasValue()) {
         return new Tuple<bool, string>(false, "");
      }

      if (!replacement.ØHasValue()) {
         return new Tuple<bool, string>(false, text ?? "");
      }

      //+ Haben wir mind. 1 TelNr?
      var hasMatch = ORgxTelNr.Match(text!).Success;

      //+ Alle Named Groups ersetzen

      int              LastEndofMatchPos = 0;
      StringBuilder    res               = new StringBuilder();
      MatchCollection? matchCollection   = null;

      do {
         // Alle TelNr suchen und mi
         matchCollection = ORgxTelNr.Matches(text, LastEndofMatchPos);

         // Jede gefundenen Telnr ersetzen 
         foreach (Match match in matchCollection) {
            // var    grpVal = match.Groups["TelNr"].Value;
            string strBeforeMatch = text.ØSubstring(LastEndofMatchPos, match.Groups["TelNr"].Index - LastEndofMatchPos);
            LastEndofMatchPos = match.Groups["TelNr"].Index + match.Groups["TelNr"].Length;
            res.Append(strBeforeMatch);
            res.Append(replacement);
         }
      }
      while (matchCollection.Count > 0);

      // Von der letzten Fundstelle bis zum Ende die Zeichen übernehmen
      string lastSubStr = text.ØSubstring(LastEndofMatchPos, text.Length - LastEndofMatchPos);
      res.Append(lastSubStr);

      return new Tuple<bool, string>(hasMatch, res.ToString());
   }

   /// <summary>
   /// Ersetzt alle Tel.-Nummern mit dem Ersatz-Zeichen
   /// </summary>
   /// <param name="text"></param>
   /// <param name="replacement"></param>
   /// <returns>
   /// var (hasMatch, newText) = … 
   /// </returns>
   public static Tuple<bool, string> Replace_Telefonnummer_Pcre2(string?               text, string? replacement
                                                                 , Func<string, bool>? predicate = null) {
      return ORgxTelNrPcre2.ØReplaceGroupName("TelNr", text, replacement, predicate);
   }

}
