namespace jig.Tools;

public static class ExtensionMethods_Arrays {

   /// <summary>
   /// Join für zwei Arrays
   /// </summary>
   /// <param name="arr1"></param>
   /// <param name="arr2"></param>
   /// <typeparam name="T"></typeparam>
   /// <returns></returns>
   public static T[] ØJoin<T>(this T[]? arr1, T[]? arr2) {
      if (arr1 == null && arr2 == null) { return Array.Empty<T>(); }

      if (arr1 == null) { return arr2; }

      if (arr2 == null) { return arr1; }

      var joinedArr = new T[arr1.Length + arr2.Length];
      arr1.CopyTo(joinedArr, 0);
      arr2.CopyTo(joinedArr, arr1.Length);

      return joinedArr;
   }


   /// <summary>
   /// !Q https://techiesweb.net/2021/05/15/an_alternative_to_linq_intersect_any.html
   /// 
   /// Returns the collection as an array.
   /// If the collection is already an array, it will be returned,
   /// else creates a new array by copying the contents of the collection, which is an O(n) operation.
   /// </summary>
   /// <param name="source">The collection to return as a list</param>
   /// <typeparam name="T">The type of element in the collection.</typeparam>
   /// <returns>Array populated from the source collection or null.</returns>
   public static T[]? AsArray<T>(this ICollection<T>? source) {
      if (source == null) {
         return null;
      }

      if (source is T[] sourceConvertedAsArray) {
         return sourceConvertedAsArray;
      }

      T[] returnArray = new T[source.Count];
      source.CopyTo(returnArray, 0);

      return returnArray;
   }


   /// <summary>
   /// !Q https://techiesweb.net/2021/05/15/an_alternative_to_linq_intersect_any.html
   ///
   /// Prüft, ob zwei Arrays mind. ein identisches Element haben
   /// 
   /// Determines there is a common element present in the two collections passed in by doing a case insensitive check.
   /// This is a fast, less allocation alternative to calling "Any" on the result of "Intersect".
   /// Returns false, If any of the collections parameter passed in is null.
   /// </summary>
   /// <param name="first">First collection.</param>
   /// <param name="second">Second collection.</param>
   /// <returns>True if a common element is present, else False.</returns>
   public static bool IntersectAny(this ICollection<string>? first, ICollection<string>? second) {
      // Working with a for loop on an array is faster than working with
      // an Enumerator(foreach). So we will first convert the ICollection to an array.
      // If the parameters are already an array, this method allocates zero bytes.

      string[]? firstArray  = first.AsArray();
      string[]? secondArray = second.AsArray();

      if (firstArray == null || secondArray == null) {
         return false;
      }

      for (var i = 0; i < firstArray.Length; i++) {
         for (var j = 0; j < secondArray.Length; j++) {
            if (firstArray[i].Equals(secondArray[j], StringComparison.OrdinalIgnoreCase)) {
               return true;
            }
         }
      }

      return false;
   }

}
