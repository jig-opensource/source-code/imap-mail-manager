namespace jig.Tools.IComparer;


/// <summary>
/// ‼ Ist ein generischer IComparer für Nullable 
/// !KH9 https://stackoverflow.com/a/5308648/4795779
/// 
/// !Q https://stackoverflow.com/a/3294808/4795779
/// </summary>
/// <typeparam name="T"></typeparam>
public class NullableComparer<T> : IComparer<Nullable<T>>
   where T : struct, IComparable<T> {

   /// <summary>
   /// 
   /// </summary>
   /// <param name="x"></param>
   /// <param name="y"></param>
   /// <returns></returns>
   public int Compare(Nullable<T> x, Nullable<T> y) {
      // Compare nulls acording MSDN specification

      // Two nulls are equal
      if (!x.HasValue && !y.HasValue) { return 0; }

      // Any object is greater than null
      if (x.HasValue && !y.HasValue) { return 1; }

      if (y.HasValue && !x.HasValue) { return -1; }

      // Otherwise compare the two values
      return x.Value.CompareTo(y.Value);
   }

}
