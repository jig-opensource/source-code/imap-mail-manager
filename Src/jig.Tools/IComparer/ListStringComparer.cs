namespace jig.Tools.IComparer; 

public class ListStringComparer : IComparer <List<string>> {

   /// <summary>Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.</summary>
   /// <param name="x">The first object to compare.</param>
   /// <param name="y">The second object to compare.</param>
   /// <returns>A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in the following table.
   /// <list type="table"><listheader><term> Value</term><description> Meaning</description></listheader><item><term> Less than zero</term><description><paramref name="x" /> is less than <paramref name="y" />.</description></item><item><term> Zero</term><description><paramref name="x" /> equals <paramref name="y" />.</description></item><item><term> Greater than zero</term><description><paramref name="x" /> is greater than <paramref name="y" />.</description></item></list></returns>
   public int Compare(List<string>? x, List<string>? y) {
      if (x == null && y == null) { return 0; }
      if (x == null ) { return 1; }
      if (y == null ) { return -1; }
      var hashTagsHaveNotBeenChanged = new HashSet<string>(x).SetEquals(y);
      return hashTagsHaveNotBeenChanged ? 0 : -1;
   }

}
