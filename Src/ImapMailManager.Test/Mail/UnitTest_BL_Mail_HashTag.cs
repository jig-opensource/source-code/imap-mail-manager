using ImapMailManager.Mail;


namespace ImapMailManager.Test.Mail;

[TestClass]
public class UnitTest_BL_Mail_HashTag {

   /// <summary>
   /// Testet gültige HashTags
   /// </summary>
   /// <param name="testString"></param>
   /// <param name="expectedValue"></param>
   [TestMethod]
   [DataRow("#$",            true)]
   [DataRow("#3",            true)]
   [DataRow("#A",            true)]
   [DataRow("#a",            true)]
   [DataRow("#hallo",        true)]
   [DataRow("#Hallo",        true)]
   [DataRow("#halloErde",    true)]
   [DataRow("#Hallo_Erde",   true)]
   [DataRow("#Hallo.Erde",   true)]
   [DataRow("#Hallo-Erde",   true)]
   [DataRow("#Hallo-(Erde)", true)]
   [DataRow("#Hallo-[Erde]", true)]
   public void Test_IsValidHashtag_Valid(string testString, bool expectedValue) {
      Assert.AreEqual(BL_Mail_HashTag.IsValidHashtag(testString), expectedValue);
   }


   /// <summary>
   /// Testet ungültige HashTags
   /// </summary>
   [TestMethod]
   [DataRow("#",    false)]
   [DataRow("##",   false)]
   [DataRow("#a!b", false)]
   public void Test_IsValidHashtag_Invalid(string testString, bool expectedValue) {
      Assert.AreEqual(BL_Mail_HashTag.IsValidHashtag(testString), expectedValue);
   }

}
