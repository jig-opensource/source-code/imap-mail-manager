using jig.Tools;
using ImapMailManager.Mail;
using jig.Tools;


namespace ImapMailManager.Test.Mail;

[TestClass]
public class UnitTests_BOMail {

   /// <summary>
   /// Testet den BOMail.oRgxHashTag
   /// </summary>
   [TestMethod]
   public void Test_EncryptDecrypt_FullAddressRightPassword() {
      var testText = @"
#Tom;
         Auch gibt es niemanden, der den Schmerz an sich liebt, 
         sucht oder wünscht, nur, weil er Schmerz ist, #test
         es sei denn, #Tom2#test2 es kommt zu zufälligen Umständen, 
         in denen Mühen und Schmerz ihm große Freude bereiten können. 

         Aber wer hat irgend ein Recht, einen Menschen zu tadeln, 
         der die Entscheidung trifft, eine Freude zu genießen, 
         die keine unangenehmen #230113_181912 Folgen hat, oder einen, 
         der Schmerz vermeidet, welcher keine daraus resultierende Freude 
         nach sich zieht? #230113-181912
         Auch gibt es niemanden, der den Schmerz an sich liebt, 
         sucht oder wünscht, nur, weil er Schmerz ist, es sei denn, 
         es kommt zu zufälligen Umständen.

         Um einen Menschen #230113.181912 zu tadeln, der die Entscheidung trifft, 
         eine Freude zu genießen, die keine unangenehmen Folgen hat, 
         oder einen, der Schmerz vermeidet, welcher keine daraus resultierende Freude 
         nach sich zieht? Auch gibt es niemanden, der den Schmerz an sich liebt, 
         sucht oder wünscht, nur,";

      List<string> expectedHashTags = new List<string>() {
         "#Tom", "#test", "#Tom2", "#test2", "#230113_181912", "#230113-181912", "#230113.181912"
      };

      //+ Suchen
      var foundHashTags = BL_Mail_HashTag.ORgx_HashTag.ØGetNamedMatches(testText, "HashTag");

      //+ Test
      //++ Beginnen alle gefundenen Elemente mit #?
      var allWithHashPrefix = foundHashTags.Where(x => x.StartsWith("#")).ToList();

      //+++ Die Anzahl Elemente ist identisch
      Assert.AreEqual(foundHashTags.Count, allWithHashPrefix.Count);

      //++ Die erwartete Liste hat keine Elemente, die nicht auch die gefundene Liste hat
      Assert.AreEqual(expectedHashTags.Except(foundHashTags).ToList().Count, 0);

      //++ Die gefundene Liste hat keine Elemente, die nicht auch die erwartete Liste hat
      Assert.AreEqual(foundHashTags.Except(expectedHashTags).ToList().Count, 0);
   }

}
