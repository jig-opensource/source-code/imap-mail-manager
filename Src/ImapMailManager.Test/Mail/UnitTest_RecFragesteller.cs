using ImapMailManager.Mail;


namespace ImapMailManager.Test.Mail;


[TestClass]
public class UnitTest_RecFragesteller {

   /// <summary>
   /// Testet das Verschlüsseln und entschlüsseln
   /// mit einem Datensatz mit allen Properties (Namen, Vornamen, …)
   /// und mit dem richtigen Passwort zum Entschlüsseln
   /// </summary>
   [TestMethod]
   public void Test_EncryptDecrypt_FullAddressRightPassword() {
      var myPw = "Key";

      //+ Init
      var recOri = new RecFragestellerV01() {
         Kontaktformular_Nachname           = "Schittli", Kontaktformular_Vorname = "Thomas", Kontaktformular_EMailAddress = "tom@jig.ch"
         , Kontaktformular_EMailDisplayName = "Thomas Schittli"
      };

      //+ Den Record verschlüsseln
      var encryptedRecord    = recOri.EncryptRecord(myPw);

      //+ Den Record entschlüsseln
      var recDecrypted = RecFragestellerV01.DecryptRecord(myPw,         encryptedRecord);

      //+ Test
      Assert.AreEqual(recOri.ToString(), recDecrypted.ToString());
   }


   /// <summary>
   /// Testet das Verschlüsseln und entschlüsseln
   /// mit einem Datensatz mit allen Properties (Namen, Vornamen, …)
   /// und mit dem falschen Passwort zum Entschlüsseln
   /// </summary>
   [TestMethod]
   public void Test_EncryptDecrypt_FullAddressWrongPassword() {
      var myPw = "Key";

      //+ Init
      var recOri = new RecFragestellerV01() {
         Kontaktformular_Nachname           = "Schittli", Kontaktformular_Vorname = "Thomas", Kontaktformular_EMailAddress = "tom@jig.ch"
         , Kontaktformular_EMailDisplayName = "Thomas Schittli"
      };

      //+ Den Record verschlüsseln
      var encryptedRecord = recOri.EncryptRecord(myPw);

      //+ Den Record entschlüsseln
      var recDecrypted = RecFragestellerV01.DecryptRecord("falsches PW", encryptedRecord);

      //+ Test
      Assert.IsNull(recDecrypted);
   }


   /// <summary>
   /// Testet das Verschlüsseln und entschlüsseln
   /// mit einem Datensatz nur mit teilen der Properties (Namen, Vornamen, …)
   /// und mit dem richtigen Passwort zum Entschlüsseln
   /// </summary>
   [TestMethod]
   public void Test_EncryptDecrypt_PartAddressRightPassword() {
      var myPw = "Key";

      //+ Init
      var recOri = new RecFragestellerV01() {
         Kontaktformular_Nachname           = "Schittli", Kontaktformular_Vorname = "Thomas"
      };

      //+ Den Record verschlüsseln
      var encryptedRecord = recOri.EncryptRecord(myPw);

      //+ Den Record entschlüsseln
      var recDecrypted = RecFragestellerV01.DecryptRecord(myPw, encryptedRecord);

      //+ Test
      Assert.AreEqual(recOri.ToString(), recDecrypted.ToString());
   }

   /// <summary>
   /// Testet das Verschlüsseln und entschlüsseln
   /// mit einem Datensatz ohne Daten in den Properties (Namen, Vornamen, …)
   /// und mit dem richtigen Passwort zum Entschlüsseln
   /// </summary>
   [TestMethod]
   public void Test_EncryptDecrypt_NoAddressRightPassword() {
      var myPw = "Key";

      //+ Init
      var recOri = new RecFragestellerV01();

      //+ Den Record verschlüsseln
      var encryptedRecord = recOri.EncryptRecord(myPw);

      //+ Den Record entschlüsseln
      var recDecrypted = RecFragestellerV01.DecryptRecord(myPw, encryptedRecord);

      //+ Test
      Assert.AreEqual(recOri.ToString(), recDecrypted.ToString());
   }

}
