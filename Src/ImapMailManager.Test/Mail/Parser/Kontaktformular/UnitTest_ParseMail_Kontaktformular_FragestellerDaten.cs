using System.Reflection;
using System.Text;
using HtmlAgilityPack;
using ImapMailManager.Mail.Parser.Kontaktformular.Personendaten;
using jig.Tools;
using jig.Tools.String;


namespace ImapMailManager.Test.Mail.Parser.Kontaktformular;

[TestClass]
public class UnitTest_ParseMail_Kontaktformular_FragestellerDaten {

   #region Tools
   
   /// <summary>
   /// Berechnet von diesem Test das \bin-Dir, also:
   /// .\Src\ImapMailManager.Test\bin\Debug\net6.0-windows\HTML\HtmlAgilityPack\
   /// </summary>
   /// <returns></returns>
   public string GetThisTestBinDir() {
      //+ Das Verzeichnis Runtime Assembly bestimmen
      // » .\bin\Debug\net6.0-windows 
      var thisAssemblyDir = Path.GetDirectoryName
         (System.Reflection.Assembly.GetExecutingAssembly().Location);

      //+ Das SubDir dieses Tests suchen
      // Das SubDir ist: HTML\HtmlAgilityPack\

      //++ Der Assembly Titel:
      // ImapMailManager.Test
      string assemblyTitle
         = Assembly.GetExecutingAssembly().GetCustomAttribute<AssemblyTitleAttribute>()?.Title
           ?? "Kein AssemblyTitleAttribute";

      //++ Der Namespace spiegelt die Verzeichnisstruktur:
      // ImapMailManager.Test.HTML.HtmlAgilityPack
      var thisNamespace = this.GetType().Namespace;

      //++ Den Assembly Titel entfernen
      // » .HTML.HtmlAgilityPack
      thisNamespace = thisNamespace.ØTrimStringStart(assemblyTitle);

      //++ führenden '.' enfernen
      // » HTML.HtmlAgilityPack
      thisNamespace = thisNamespace.TrimStart('.');

      //++ Aus HTML.HtmlAgilityPack den Pfad erstellen
      // » HTML\HtmlAgilityPack
      var pathitems      = thisNamespace.Split('.');
      var thisTestSubDir = String.Join(@"\", pathitems);

      return Path.Combine(thisAssemblyDir!, thisTestSubDir);
   }

   #endregion Tools

   /// <summary>
   /// Test als HTML
   /// 
   /// Testet mit der E-Mail:
   /// .\Src\ImapMailManager.Test\HTML\HtmlAgilityPack\UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #1.html
   /// bzw:
   /// .\bin\Debug\net6.0-windows\HTML\HtmlAgilityPack\UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #1.html
   /// </summary>
   [TestMethod]
   public void Test_ParseMail__Ex1() {
      var thisTestBinDir = GetThisTestBinDir();

      //+ Prepare
      var thisTestMailHtmlFileName = @"UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #1.html";
      thisTestMailHtmlFileName = Path.Combine(thisTestBinDir, thisTestMailHtmlFileName);
      
      //++ Expected
      var expectedRefNr = @"#29-5858";
      var expectedRecData = @"Block|Toni|toni.block@web.de";
      
      //+ Html lesen
      string htmlMailText = System.IO.File.ReadAllText(thisTestMailHtmlFileName, Encoding.UTF8);
      HtmlDocument htmlDoc = new HtmlDocument();
      htmlDoc.LoadHtml(htmlMailText);

      //+ Parser testen
      var (subjectRefNr, bodyRefNr, recFragesteller) = Parse_HtmlStruct.Parse_MailHtmlBody(htmlDoc, null);

      Assert.AreEqual(expectedRefNr,   bodyRefNr);
      Assert.AreEqual(expectedRecData, recFragesteller.Get_DataAsString());
   }

   /// <summary>
   /// Test als Pure Text
   /// 
   /// Testet mit der E-Mail:
   /// .\Src\ImapMailManager.Test\HTML\HtmlAgilityPack\UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #2.html
   /// bzw:
   /// .\bin\Debug\net6.0-windows\HTML\HtmlAgilityPack\UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #2.html
   /// </summary>
   [TestMethod]
   public void Test_ParseMail__Ex2() {
      var thisTestBinDir = GetThisTestBinDir();

      //+ Prepare
      var thisTestMailHtmlFileName = @"UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #2.html";
      thisTestMailHtmlFileName = Path.Combine(thisTestBinDir, thisTestMailHtmlFileName);
      
      //++ Expected
      var expectedRecData = @"Bianca Kosanke|bkosanke@gmx.de";
      
      //+ Html lesen
      string htmlMailText = System.IO.File.ReadAllText(thisTestMailHtmlFileName, Encoding.UTF8);
      HtmlDocument htmlDoc = new HtmlDocument();
      htmlDoc.LoadHtml(htmlMailText);

      //+ Parser testen
      var recFragesteller = Parse_PureText.Parse_MailHtmlBody(htmlDoc);
      
      // Debug, um expected zu bestimmen
      var tester = recFragesteller.Get_DataAsString();

      // Assert.AreEqual(expectedRefNr,   bodyRefNr);
      Assert.AreEqual(expectedRecData, recFragesteller.Get_DataAsString());
   }


   /// <summary>
   /// Test als HTML
   /// 
   /// Testet mit der E-Mail:
   /// .\Src\ImapMailManager.Test\HTML\HtmlAgilityPack\UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #3.html
   /// bzw:
   /// .\bin\Debug\net6.0-windows\HTML\HtmlAgilityPack\UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #3.html
   /// </summary>
   [TestMethod]
   public void Test_ParseMail__Ex3() {
      var thisTestBinDir = GetThisTestBinDir();

      //+ Prepare
      var thisTestMailHtmlFileName
         = @"UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #3.html";
      thisTestMailHtmlFileName = Path.Combine(thisTestBinDir, thisTestMailHtmlFileName);

      //++ Expected
      var expectedRefNr   = @"#19-2603";
      var expectedRecDataAsString = @"neureiterm@gmx.at";

      //+ Html lesen
      string htmlMailText = System.IO.File.ReadAllText(thisTestMailHtmlFileName, Encoding.UTF8);
      HtmlDocument htmlDoc = new HtmlDocument();
      htmlDoc.LoadHtml(htmlMailText);

      //+ Parser testen
      var (subjectRefNr, bodyRefNr, recFragesteller) = Parse_HtmlStruct.Parse_MailHtmlBody
         (htmlDoc, null);

      //! Debug: Expected bestimmen
      var tester = recFragesteller.Get_DataAsString();

      //+ Testen
      Assert.AreEqual(expectedRefNr,   bodyRefNr);
      Assert.AreEqual(expectedRecDataAsString, recFragesteller.Get_DataAsString());
   }

   /// <summary>
   /// Test als HTML
   /// 
   /// Testet mit der E-Mail:
   /// .\Src\ImapMailManager.Test\HTML\HtmlAgilityPack\UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #4.html
   /// bzw:
   /// .\bin\Debug\net6.0-windows\HTML\HtmlAgilityPack\UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #4.html
   /// </summary>
   [TestMethod]
   public void Test_ParseMail__Ex4() {
      var thisTestBinDir = GetThisTestBinDir();

      //+ Prepare
      var thisTestMailHtmlFileName
         = @"UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #4.html";
      thisTestMailHtmlFileName = Path.Combine(thisTestBinDir, thisTestMailHtmlFileName);

      //++ Expected
      var expectedRefNr   = @"#6F-4422";
      var expectedRecData = @"Beate Weidner|zoepfeziehen@gmail.com";

      //+ Html lesen
      string htmlMailText = System.IO.File.ReadAllText(thisTestMailHtmlFileName, Encoding.UTF8);
      HtmlDocument htmlDoc = new HtmlDocument();
      htmlDoc.LoadHtml(htmlMailText);

      //+ Parser testen
//      var (subjectRefNr, bodyRefNr, recFragesteller) = Parse_HtmlStruct.Parse_MailHtmlBody
//         (htmlDoc, null);
      var recFragesteller = Parse_PureText.Parse_MailHtmlBody(htmlDoc);

      //! Debug: Expected bestimmen
      var tester = recFragesteller.Get_DataAsString();

      //+ Testen
      // Assert.AreEqual(expectedRefNr,   bodyRefNr);
      Assert.AreEqual(expectedRecData, recFragesteller.Get_DataAsString());
   }

   /// <summary>
   /// Test als HTML, mit Doppel-s ß 
   /// 
   /// Testet mit der E-Mail:
   /// .\Src\ImapMailManager.Test\HTML\HtmlAgilityPack\UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #5.html
   /// bzw:
   /// .\bin\Debug\net6.0-windows\HTML\HtmlAgilityPack\UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #5.html
   /// </summary>
   [TestMethod]
   public void Test_ParseMail__Ex5() {
      var thisTestBinDir = GetThisTestBinDir();

      //+ Prepare
      var thisTestMailHtmlFileName
         = @"UnitTest_ParseMail_Kontaktformular_FragestellerDaten - !Ex Mail #5.html";
      thisTestMailHtmlFileName = Path.Combine(thisTestBinDir, thisTestMailHtmlFileName);

      //++ Expected
      var expectedRefNr   = @"#6F-4422";
      var expectedRecData = @"Haßler|Beate|zoepfeziehen@gmail.com";

      //+ Html lesen
      string htmlMailText = System.IO.File.ReadAllText(thisTestMailHtmlFileName, Encoding.UTF8);
      HtmlDocument htmlDoc = new HtmlDocument();
      htmlDoc.LoadHtml(htmlMailText);

      //+ Parser testen
//      var (subjectRefNr, bodyRefNr, recFragesteller) = Parse_HtmlStruct.Parse_MailHtmlBody
//         (htmlDoc, null);
      var recFragesteller = Parse_PureText.Parse_MailHtmlBody(htmlDoc);

      //! Debug: Expected bestimmen
      var tester = recFragesteller.Get_DataAsString();

      //+ Testen
      // Assert.AreEqual(expectedRefNr,   bodyRefNr);
      Assert.AreEqual(expectedRecData, recFragesteller.Get_DataAsString());
   }
   
}
