using ImapMailManager.Config;


namespace ImapMailManager.Test.Config;

[TestClass]
public class UnitTest_ConfigMail_Basics {

   /// <summary>
   /// Testet ungültige HashTags
   /// </summary>
   [TestMethod]
   //        testString                      xpctSuccess    xpctMAKuerzel
   [DataRow (null,                           false,         "",        DisplayName = "01")]
   [DataRow ("",                             false,         "",        DisplayName = "02")]
   [DataRow ("JohnDoe@test.ch",              false,         "",        DisplayName = "03")]
   [DataRow ("TeamDE.MeLi@rogerliebi.ch",    true,          "MeLi",    DisplayName = "04")]
   [DataRow ("TeamDE.Thsc@rogerliebi.ch",    true,          "Thsc",    DisplayName = "05")]
   public void Test_Rgx_EMailAdress_PersönlicheTeamAntwortAdresse(string? testMail, bool xpctSuccess, string xpctMAKuerzel) {

      var res = ConfigMail_Basics.ORgx_EMailAdress_PersönlicheTeamAntwortAdresse.Match(testMail ?? "");

      // Debug
      var resSuccess   = res.Success;
      var test_1 = res.Groups["EMailAddress"];
      var test_2 = res.Groups["Prefix"];
      var test_3 = res.Groups["Sprache"];
      var test_4 = res.Groups["MAKuerzel"];
      var test_5 = res.Groups["Domain"];

      Assert.AreEqual(xpctSuccess,   res.Success, "Success");
      Assert.AreEqual(xpctMAKuerzel, res.Groups["MAKuerzel"], "Success");
   }
   

}
