using ImapMailManager.Properties;


namespace ImapMailManager.Test.Properties;

[TestClass]
public class UnitTest_Sensitive_Team_Mitglieder {

   /// <summary>
   /// Testen des Yaml-Parsers und der Abstimmung der Config-Props
   /// </summary>
   [TestMethod]
   public void Test_Sensitive_Team_Mitglieder() {
      var teamConfiguration = Sensitive_Team_Mitglieder.ReadYaml();
      Assert.IsTrue(teamConfiguration.TeamConfig.TeamMitglieder.Count > 0);
   }

}
