using jig.Tools;


namespace ImapMailManager.Test;

[TestClass]
public class UnitTest_CodeTests {

   #region List<Func<>>

   private string TestFunc1(bool arg1) { return ""; }
   private string TestFunc2(bool arg1) { return ""; }

   /// <summary>
   /// » !9 Funktioniert wie erhofft: 
   /// Testet, ob ØAppendItem_IfMissing auchmit List<Func<>> funktioniert
   /// </summary>
   [TestMethod]
   public void Test_List_Func() {
      List<Func<bool, string?>> funcs = new();
      funcs.ØAppendItem_IfMissing(TestFunc1);
      funcs.ØAppendItem_IfMissing(TestFunc2);
      funcs.ØAppendItem_IfMissing(TestFunc1);
      int stopper = 1;
   }

   #endregion List<Func<>>

   #region string.Join()

   /// <summary>
   /// » !9 Funktioniert wie erhofft: 
   ///   Ein String-Array wird mit " # " joined
   ///   und danach wieder mit " # " aufgetrennt
   /// </summary>
   [TestMethod]
   public void Test_SplitString_MultipleChars() {
      string   splitter = " # ";
      string[] strArr   = new[] { "Text 1", "Text 2" };

      string joined = string.Join(splitter, strArr);

      var newArr = joined.Split(splitter);

      int stopper = 1;

      // Assert.AreEqual(BL_Mail_HashTag.IsValidHashtag(testString), expectedValue);
   }

   #endregion string.Join()

   #region DateTime TimeSpamp

   /// <summary>
   /// » !9 Funktioniert wie erhofft:
   /// Erzeugt 230228 1120 
   /// </summary>
   [TestMethod]
   public void Test_Get_TimeStamp() {
      var now     = DateTime.Now.ToString("yyMMdd HHmm");
      int stopper = 1;

      // Assert.AreEqual(BL_Mail_HashTag.IsValidHashtag(testString), expectedValue);
   }

   #endregion DateTime TimeSpamp

   #region Mixed_CompositeString

   /// <summary>
   /// ‼ Klappt leider nicht
   /// Gemischte CompositeString:
   /// Platzhalter mit {variable} und {positions-idx} 
   /// </summary>
   [TestMethod]
   public void Test_Mixed_CompositeString() {
      var testVar               = "Hallo";
      var mixed_CompositeString = $"{testVar}: {0:D2}";

      int stopper = 1;
      Assert.AreEqual("Hallo: 00", mixed_CompositeString);
   }

   #endregion Mixed_CompositeString

}
