using ImapMailManager.BO;


namespace ImapMailManager.Test.BO;


[TestClass]
public class UnitTest_BL_MailSubject {

   /// <summary>
   /// Testet die Funktion Split_Subject_SprachCode_Präfixes_Subject,
   /// die ein Subject in diese Elemente auftrennt:
   /// - Sprachcode
   /// - Erstes Subject Präfix (RE, FWD, …)
   /// - Letztes Subject Präfix (RE, FWD, …)
   /// - Der rest-String mit dem Betreff
   /// 
   /// </summary>
   [TestMethod]
   [DataRow
      (    null
         , ""
         , BL_MailSubject.EmpfangenerEMailTyp.Unknown
         , BL_MailSubject.EmpfangenerEMailTyp.Unknown
         , ""
         , DisplayName = "01")]
   [DataRow
      (    ""
         , ""
         , BL_MailSubject.EmpfangenerEMailTyp.Unknown
         , BL_MailSubject.EmpfangenerEMailTyp.Unknown
         , ""
         , DisplayName = "02")]
   [DataRow
      (    "Re: Das Subject"
         , ""
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , BL_MailSubject.EmpfangenerEMailTyp.Unknown
         , "Das Subject"
         , DisplayName = "03")]
   [DataRow
      (    "⭕DE Re: Das Subject"
         , "DE"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , BL_MailSubject.EmpfangenerEMailTyp.Unknown
         , "Das Subject"
         , DisplayName = "04")]
   [DataRow
      (    "Re: ⭕DE Das Subject"
         , "DE"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , BL_MailSubject.EmpfangenerEMailTyp.Unknown
         , "Das Subject"
         , DisplayName = "05")]
   [DataRow
      (    "⭕DE Re: FW Das Subject"
         , "DE"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , "Das Subject"
         , DisplayName = "06")]
   [DataRow
      (    "Re: ⭕DE FW: Das Subject"
         , "DE"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , "Das Subject"
         , DisplayName = "07")]
   [DataRow
      (    "Re: FW ⭕DE Re Das Subject"
         , "DE"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , "Das Subject"
         , DisplayName = "08")]
   [DataRow
      (    "Re: FW: Re FW ⭕EN Das Subject"
         , "EN"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , "Das Subject"
         , DisplayName = "09")]
   [DataRow
      (    "Re: FW: Re FW ⭕FRA ⭕FRA ⭕FRA Das Subject"
         , "FRA"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , "Das Subject"
         , DisplayName = "10")]
   public void Test_Split_Subject_SprachCode_Präfixes_Subject__Param_Null(string           testSubject, 
                                                                          string                             expectedSprachCodeIso2,
                                                                          BL_MailSubject.EmpfangenerEMailTyp expectedEMailTypFirst, 
                                                                          BL_MailSubject.EmpfangenerEMailTyp expectedEMailTypLast,
                                                                          string expectedSubject) {
      var (resSpracheIso2, resEMailTypFirst, resEMailTypLast, resNewSubject) =  
         BL_MailSubject.Split_Subject_SprachCode_Präfixes_Subject(testSubject);

      Assert.AreEqual(expectedSprachCodeIso2, resSpracheIso2, "Sprach-Code");
      Assert.AreEqual(expectedEMailTypFirst,  resEMailTypFirst, "resEMailTypFirst");
      Assert.AreEqual(expectedEMailTypLast,   resEMailTypLast, "resEMailTypLast");
      Assert.AreEqual(expectedSubject,        resNewSubject, "resNewSubject");
   }
   

}
