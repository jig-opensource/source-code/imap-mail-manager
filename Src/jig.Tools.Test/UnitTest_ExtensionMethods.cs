using ImapMailManager.BO;
using jig.Tools.IEqualityComparer;


namespace jig.Tools.Test;

[TestClass]
public class UnitTest_ExtensionMethods {


   [TestMethod]
   public void Tester() {
      var VornameNachname = "Abc 123 def";
      var items           = VornameNachname.Split(' ');

      string? vorname  = items.FirstOrDefault();
      string? nachname = string.Join(' ', items.Skip(1)); 

      int stopper = 1;

   }
   
   #region Public Methods and Operators

   /// <summary>
   /// Testet die Extension-Metode jig.Tools. ØCleanEmailSubject
   /// </summary>
   /// <param name="testString"></param>
   /// <param name="expectedValue"></param>
   [TestMethod]
   [DataRow("Re: Hallo", "Hallo", DisplayName = "01")]
   [DataRow("[Fwd: ] Blonde Joke", "Blonde Joke", DisplayName = "02")]
   [DataRow("Re: Fwd", "Fwd", DisplayName = "03")]
   [DataRow("Fwd : Re : Re: Many", "Many", DisplayName = "04")]
   [DataRow("Re : Re: Many", "Many", DisplayName = "05")]
   [DataRow("Re  : : Re: Many", "Many", DisplayName = "06")]
   [DataRow("Re:: Many", "Many", DisplayName = "07")]
   [DataRow("Re; Many", "Many", DisplayName = "08")]
   [DataRow(": noah - should not match anything", ": noah - should not match anything", DisplayName = "09")]
   [DataRow("RE--", "RE--", DisplayName = "10")]
   [DataRow("RE: : Presidential Ballots for Florida", "Presidential Ballots for Florida", DisplayName = "11")]
   [DataRow("[RE: (no subject)]", "(no subject)", DisplayName = "12")]
   [DataRow("Request - should not match anything", "Request - should not match anything", DisplayName = "13")]
   [DataRow("this is the subject (fwd)", "this is the subject", DisplayName = "14")]
   [DataRow("Re: [Fwd: ] Blonde Joke", "Blonde Joke", DisplayName = "15")]
   [DataRow("Re: [Fwd: [Fwd: FW: Policy]]", "Policy", DisplayName = "16")]
   [DataRow("Re: Fwd: [Fwd: FW: \\\"Drink Plenty of Water\\\"]", "\\\"Drink Plenty of Water\\\"", DisplayName = "17")]
   [DataRow("FW: FW: (fwd) FW:  Warning from XYZ...", "Warning from XYZ...", DisplayName = "18")]
   [DataRow("FW: (Fwd) (Fwd)", "", DisplayName = "19")]
   [DataRow("Fwd: [Fwd: [Fwd: Big, Bad Surf Moving]]", "Big, Bad Surf Moving", DisplayName = "20")]
   [DataRow("FW: [Fwd: Fw: drawing by a school age child in PA (fwd)]", "drawing by a school age child in PA", DisplayName = "21")]
   [DataRow("Fwd: Re: fwd is an acronym (four-wheel drive)", "is an acronym (four-wheel drive)", DisplayName = "22")]
   public void Test_ExtensionMethod_CleanEmailSubject_NoPrefixes(string testString, string expectedValue) {
      Assert.AreEqual(expectedValue, testString.ØCleanEmailSubjectPrefix(false, false));
   }

   /// <summary>
   /// Testet die Extension-Metode jig.Tools. ØCleanEmailSubject
   /// </summary>
   /// <param name="testString"></param>
   /// <param name="expectedValue"></param>
   [TestMethod]
   [DataRow("Re: Hallo", "Re: Hallo", DisplayName = "01")]
   [DataRow("Re: Fwd", "Re: Fwd", DisplayName = "02")]
   [DataRow("Fwd : Re : Re: Many", "Fw: Many", DisplayName = "03")]
   [DataRow("Re : Re: Many", "Re: Many", DisplayName = "04")]
   [DataRow("Re  : : Re: Many", "Re: Many", DisplayName = "05")]
   [DataRow("Re:: Many", "Re: Many", DisplayName = "06")]
   [DataRow("Re; Many", "Re: Many", DisplayName = "07")]
   [DataRow(": noah - should not match anything", ": noah - should not match anything", DisplayName = "08")]
   [DataRow("RE--", "RE--", DisplayName = "09")]
   [DataRow("RE: : Presidential Ballots for Florida", "Re: Presidential Ballots for Florida", DisplayName = "10")]
   [DataRow("Request - should not match anything", "Request - should not match anything", DisplayName = "11")]
   [DataRow("this is the subject (fwd)", "this is the subject", DisplayName = "12")]
   [DataRow("Re: [Fwd: ] Blonde Joke", "Re: Blonde Joke", DisplayName = "13")]
   [DataRow("Re: [Fwd: [Fwd: FW: Policy]]", "Re: Policy", DisplayName = "14")]
   [DataRow("Re: Fwd: [Fwd: FW: \\\"Drink Plenty of Water\\\"]", "Re: \\\"Drink Plenty of Water\\\"", DisplayName = "15")]
   [DataRow("FW: FW: (fwd) FW:  Warning from XYZ...", "Fw: Warning from XYZ...", DisplayName = "16")]
   [DataRow("Fwd: [Fwd: [Fwd: Big, Bad Surf Moving]]", "Fw: Big, Bad Surf Moving", DisplayName = "17")]
   [DataRow("FW: [Fwd: Fw: drawing by a school age child in PA (fwd)]", "Fw: drawing by a school age child in PA", DisplayName = "18")]
   // ToDo 🟥 Nicht saubere Lösung
   [DataRow("[Fwd: ] Blonde Joke", "Blonde Joke", DisplayName = "19")]
   // ToDo 🟥 Nicht saubere Lösung
   [DataRow("[RE: (no subject)]", "(no subject)", DisplayName = "20")]
   // ToDo 🟥 Nicht saubere Lösung
   [DataRow("FW: (Fwd) (Fwd)", "", DisplayName = "21")]
   // ToDo 🟥 Nicht saubere Lösung
   [DataRow("Fwd: Re: fwd is an acronym (four-wheel drive)", "Re: is an acronym (four-wheel drive)", DisplayName = "22")]
   public void Test_ExtensionMethod_CleanEmailSubject_1stPrefixOnly(string testString, string expectedValue) {
      Assert.AreEqual(expectedValue, testString.ØCleanEmailSubjectPrefix(true, false));
   }

   /// <summary>
   /// Testet die Extension-Metode jig.Tools. ØCleanEmailSubject
   /// </summary>
   /// <param name="testString"></param>
   /// <param name="expectedValue"></param>
   [TestMethod]
   [DataRow("Re: Hallo", "Re: Hallo", DisplayName = "01")]
   [DataRow("Re: Fwd", "Re: Fwd", DisplayName = "02")]
   [DataRow("Re : Re: Many", "Re: Many", DisplayName = "03")]
   [DataRow("Re  : : Re: Many", "Re: Many", DisplayName = "04")]
   [DataRow("Re:: Many", "Re: Many", DisplayName = "05")]
   [DataRow("Re; Many", "Re: Many", DisplayName = "06")]
   [DataRow(": noah - should not match anything", ": noah - should not match anything", DisplayName = "07")]
   [DataRow("RE--", "RE--", DisplayName = "08")]
   [DataRow("RE: : Presidential Ballots for Florida", "Re: Presidential Ballots for Florida", DisplayName = "09")]
   [DataRow("Request - should not match anything", "Request - should not match anything", DisplayName = "10")]
   [DataRow("this is the subject (fwd)", "this is the subject", DisplayName = "11")]
   [DataRow("Re: [Fwd: ] Blonde Joke", "Re: Blonde Joke", DisplayName = "12")]
   [DataRow("Re: [Fwd: [Fwd: FW: Policy]]", "Re: Policy", DisplayName = "13")]
   [DataRow("Re: Fwd: [Fwd: FW: \\\"Drink Plenty of Water\\\"]", "Re: \\\"Drink Plenty of Water\\\"", DisplayName = "14")]
   [DataRow("FW: FW: (fwd) FW:  Warning from XYZ...", "Fw: Warning from XYZ...", DisplayName = "15")]
   [DataRow("Fwd: [Fwd: [Fwd: Big, Bad Surf Moving]]", "Fw: Big, Bad Surf Moving", DisplayName = "16")]
   [DataRow("FW: [Fwd: Fw: drawing by a school age child in PA (fwd)]", "Fw: drawing by a school age child in PA", DisplayName = "17")]
   [DataRow("Fwd: Re: fwd is an acronym (four-wheel drive)", "Re: Fw: is an acronym (four-wheel drive)", DisplayName = "18")]

   // ToDo 🟥 Nicht saubere Lösung
   [DataRow("Fwd : Re : Re: Many", "Fw: Many", DisplayName = "19")]
   // ToDo 🟥 Nicht saubere Lösung
   [DataRow("[Fwd: ] Blonde Joke", "Blonde Joke", DisplayName = "20")]
   // ToDo 🟥 Nicht saubere Lösung
   [DataRow("[RE: (no subject)]", "(no subject)", DisplayName = "21")]
   // ToDo 🟥 Nicht saubere Lösung
   [DataRow("FW: (Fwd) (Fwd)", "", DisplayName = "22")]
   public void Test_ExtensionMethod_CleanEmailSubject_BothPrefixes(string testString, string expectedValue) {
      Assert.AreEqual(expectedValue, testString.ØCleanEmailSubjectPrefix(true, true));
   }

   #endregion

}
