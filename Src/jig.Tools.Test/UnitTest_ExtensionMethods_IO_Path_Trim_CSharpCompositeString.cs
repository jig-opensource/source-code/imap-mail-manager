namespace jig.Tools.Test;

[TestClass]
public class UnitTest_ExtensionMethods_IO_Path_Trim_CSharpCompositeString {

   [TestMethod]
   //        testPfad                             xpctPfad
   [DataRow (null,                                "",                         DisplayName = "01")]
   [DataRow (@"",                                 "",                         DisplayName = "02")]
   [DataRow (@"Test.txt",                        @"Test.txt",                 DisplayName = "03")]
   [DataRow (@"{0:D2} Test.txt",                 @"Test.txt",                 DisplayName = "04")]
   [DataRow (@"{0:D2}-Test.txt",                 @"-Test.txt",                DisplayName = "05")]
   [DataRow (@"Test {0:D2}.txt",                 @"Test.txt",                 DisplayName = "06")]
   [DataRow (@"Test-{0:D2}.txt",                 @"Test-.txt",                DisplayName = "07")]
   [DataRow (@"Test.{0:D2}txt",                  @"Test.txt",                 DisplayName = "08")]
   [DataRow (@"Test.{0:D2} txt",                 @"Test.txt",                 DisplayName = "09")]
   [DataRow (@"Test.{0:D2}-txt",                 @"Test.-txt",                DisplayName = "10")]
   [DataRow (@"Test.txt{0:D2}",                  @"Test.txt",                 DisplayName = "11")]
   [DataRow (@"Test.txt-{0:D2}",                 @"Test.txt-",                DisplayName = "12")]
   [DataRow (@"Test.txt {0:D2}",                 @"Test.txt",                 DisplayName = "13")]
   [DataRow (@"c:\Temp\Datei {0:D2}.txt",        @"c:\Temp\Datei.txt",        DisplayName = "14")]
   [DataRow (@"c:\Temp\Datei {0:D2} Suffix.txt", @"c:\Temp\Datei Suffix.txt", DisplayName = "15")]
   public void Test_Trim_CSharpCompositeString(string? testPfad, string xpctPfad) {
      var res = ExtensionMethods_IO_Path.Trim_CSharpCompositeString(testPfad);
      
      int stopper = 1;
      Assert.AreEqual(xpctPfad, res);
   }

}
