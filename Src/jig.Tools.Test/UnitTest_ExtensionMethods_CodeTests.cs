using ImapMailManager.Mail.MimeKit.jig_Mailbox_Enumerator;


namespace jig.Tools.Test;

[TestClass]
public class UnitTest_ExtensionMethods_CodeTests {

   [TestMethod]
   public void Tester() {

      List<Type> foundClasses = new List<Type>();
      
      var assemblies = AppDomain.CurrentDomain.GetAssemblies();
      foreach (var assembly in assemblies) {
         IEnumerable<Type> typesWithInterface = ExtensionMethods_Reflection.GetTypesWithInterface(assembly, typeof(IMailboxIteratorAdHoc));
         var               withInterface      = typesWithInterface as Type[] ?? typesWithInterface.ToArray();
         if (withInterface.Any()) {
            foundClasses.AddRange(withInterface.Where(x => x.IsClass));
         }
      }

      int stopper3 = 1;
   }

}
