namespace jig.Tools.Test;

[TestClass]
public class UnitTest_ExtensionMethods_List {

   [TestMethod]
   public void Test_ØAppendItem_IfMissing() {
      List<string> testList = new List<string>() { "1", "2", "3" };

      testList.ØAppendItem_IfMissing("2");
      Assert.AreEqual(3, testList.Count);

      testList.ØAppendItem_IfMissing("100");
      Assert.AreEqual(4, testList.Count);

   }

}
