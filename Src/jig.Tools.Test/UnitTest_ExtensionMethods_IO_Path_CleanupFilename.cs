namespace jig.Tools.Test;

[TestClass]
public class UnitTest_ExtensionMethods_IO_Path_CleanupFilename {

   [TestMethod]
   //        testPfad                     xpctPfad
   [DataRow (null,                      @"",                       DisplayName = "01")]
   [DataRow (@"",                       @"",                       DisplayName = "02")]
   [DataRow (@"c:\Temp",                @"c:\Temp",                DisplayName = "03")]
   [DataRow (@"c:\Temp\",               @"c:\Temp",                DisplayName = "04")]
   [DataRow (@"c:\Te<mp\",              @"c:\Te❮mp",               DisplayName = "05")]
   [DataRow (@"c:\Te>mp\",              @"c:\Te❯mp",               DisplayName = "06")]
   [DataRow (@"c:\Te|mp\",              @"c:\Te┃mp",               DisplayName = "07")]
   [DataRow ("c:\\Te\"mp\\",            @"c:\Teʺmp",               DisplayName = "08")]
   [DataRow (@"c:\Te*mp\",              @"c:\Te✸mp",               DisplayName = "09")]
   [DataRow (@"c:\Te?mp\",              @"c:\Te¿mp",               DisplayName = "10")]
   [DataRow (@"Set.zip",                @"Set.zip",                DisplayName = "11")]
   [DataRow (@".\Set.zip",              @".\Set.zip",              DisplayName = "12")]
   [DataRow (@".\Set:ter.zip",          @".\Set꞉ter.zip",          DisplayName = "13")]
   [DataRow (@".\Set<ter.zip",          @".\Set❮ter.zip",          DisplayName = "14")]
   [DataRow (@".\Set>ter.zip",          @".\Set❯ter.zip",          DisplayName = "15")]
   [DataRow (@".\Set|ter.zip",          @".\Set┃ter.zip",          DisplayName = "16")]
   [DataRow (".\\Set\"ter.zip",         @".\Setʺter.zip",          DisplayName = "17")]
   [DataRow (@".\Set*ter.zip",          @".\Set✸ter.zip",          DisplayName = "18")]
   [DataRow (@".\Set?ter.zip",          @".\Set¿ter.zip",          DisplayName = "19")]
   [DataRow (@"c:\Temp\230202\Set.zip", @"c:\Temp\230202\Set.zip", DisplayName = "20")]
   [DataRow (@"c:\Te*p\230?02\S>t.zip", @"c:\Te✸p\230¿02\S❯t.zip", DisplayName = "21")]
   public void Test_ØReplaceRootDir(string? testPfad, string xpctPfad) {
      var res     = testPfad.ØStringToFilename();
      int stopper = 1;
      Assert.AreEqual(xpctPfad, res);
   }

   
}
