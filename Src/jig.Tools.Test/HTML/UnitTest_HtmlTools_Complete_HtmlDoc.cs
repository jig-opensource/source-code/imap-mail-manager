using System.Diagnostics;
using System.Text;
using System.Xml;
using AngleSharp;
using AngleSharp.Html;
using AngleSharp.Html.Dom;
using AngleSharp.Html.Parser;
using HtmlAgilityPack;
using jig.Tools.HTML;
using jig.Tools.String;
using TextCopy;


namespace jig.Tools.Test.HTML;

[TestClass]
public class UnitTest_HtmlTools_Complete_HtmlDoc {

   /// <summary>
   /// Pretty Print für HTML
   /// </summary>
   /// <param name="html"></param>
   /// <returns></returns>
   public static string PrettyPrint_Html(string html) {
      var context    = BrowsingContext.New(Configuration.Default);
      var htmlParser = context.GetService<IHtmlParser>();

      IHtmlDocument htmlDoc = htmlParser.ParseDocument(html);

      using (var writer = new StringWriter()) {
         htmlDoc.ToHtml(writer, new PrettyMarkupFormatter());

         return writer.ToString();
      }
   }


   readonly static public string DocTypeDeclaration = @"<!DocType HTML>";

   /// <summary>
   /// Definiert das standard Grundgerüst eines HTML Dokuments
   /// </summary>
   readonly static public string Default_HtmlDoc_Structure = @"
         <!doctype html>
         <html>
           <head>
           </head>
           <body>
           </body>
         </html>
         ";


   /// <summary>
   /// Erzeugt die Standard Struktur eines HTML-Dokuments
   /// </summary>
   /// <returns></returns>
   private HtmlDocument Get_Default_HtmlDoc_Structure() {
      HtmlDocument htmlDoc = new HtmlDocument();
      htmlDoc.LoadHtml(Default_HtmlDoc_Structure);

      return htmlDoc;
   }


   /// <summary>
   /// Prüft Ensure_Complete_HtmlDoc()
   /// mit leeren Daten
   ///
   /// Prüft auch das explizite setzen von <!DocType
   /// </summary>
   [TestMethod]
   public void Test_Ensure_Complete_HtmlDoc__EmptyNewDoc() {
      //+ Als Quelle: ein leeres Dok
      var newHtmlDoc = @"";
      var htmlDoc    = HtmlTools.Ensure_Complete_HtmlDoc(Default_HtmlDoc_Structure, newHtmlDoc);

      //+ Test
      Assert.AreEqual
         (htmlDoc.DocumentNode.OuterHtml.ØRemoveWhiteSpaces()
          , Default_HtmlDoc_Structure.ØRemoveWhiteSpaces());

      //++ DocType entspricht *nicht* DocTypeDeclaration
      Assert.IsFalse(htmlDoc.DocumentNode.OuterHtml.Contains(DocTypeDeclaration));

      //+ DocType: Testen mit der anderen Gross-/Klein-Schreibung DocTypeDeclaration
      newHtmlDoc = DocTypeDeclaration;

      // newHtmlDoc = @"<!DOCTYPE>";
      // newHtmlDoc = @"<!DocType>";
      htmlDoc = HtmlTools.Ensure_Complete_HtmlDoc(Default_HtmlDoc_Structure, newHtmlDoc);

      //++ Ist jetzt True
      Assert.IsTrue(htmlDoc.DocumentNode.OuterHtml.Contains(DocTypeDeclaration));

      //‼ 🚩 Debug: Ins ClipBoard kopieren
      // ClipboardService.SetText(PrettyPrint_Html(htmlDoc.DocumentNode.OuterHtml));
   }


   /// <summary>
   /// Prüft Ensure_Complete_HtmlDoc()
   /// mit nur div-Inhalt 
   /// </summary>
   [TestMethod]
   public void Test_Ensure_Complete_HtmlDoc__DivOnly() {
      //+ Prepare
      //++ Als Quelle: Nur <div>
      var newHtmlDoc = @"<div>...</div>";

      var expected = @"<!DOCTYPE html>
         <html>
	         <head>
	         </head>
	         <body>
		         <div>...</div>
	         </body>
         </html>";

      var htmlDoc = HtmlTools.Ensure_Complete_HtmlDoc(Default_HtmlDoc_Structure, newHtmlDoc);

      //‼ 🚩 Debug: Ins ClipBoard kopieren
      // ClipboardService.SetText(PrettyPrint(htmlDoc.DocumentNode.OuterHtml));
      // ClipboardService.SetText($"Expected:\n{expected.ØRemoveWhiteSpaces()}\n\nWe've got:\n{htmlDoc.DocumentNode.OuterHtml.ØRemoveWhiteSpaces()}");

      //+ Test
      Assert.AreEqual
         (htmlDoc.DocumentNode.OuterHtml.ØRemoveWhiteSpaces()
          , expected.ØRemoveWhiteSpaces()
          , true);
   }


   /// <summary>
   /// Prüft Ensure_Complete_HtmlDoc()
   /// mit nur div-Inhalt 
   /// </summary>
   [TestMethod]
   public void Test_Ensure_Complete_HtmlDoc__BodyAndData() {
      //+ Prepare
      //++ Als Quelle: Nur <div>
      var newHtmlDoc = @"<body><div>...</div></body>";

      var expected = @"<!DOCTYPE html>
         <html>
	         <head>
	         </head>
	         <body>
		         <div>...</div>
	         </body>
         </html>";

      var htmlDoc = HtmlTools.Ensure_Complete_HtmlDoc(Default_HtmlDoc_Structure, newHtmlDoc);

      //‼ 🚩 Debug: Ins ClipBoard kopieren
      // ClipboardService.SetText(PrettyPrint(htmlDoc.DocumentNode.OuterHtml));
      // ClipboardService.SetText($"Expected:\n{expected.ØRemoveWhiteSpaces()}\n\nWe've got:\n{htmlDoc.DocumentNode.OuterHtml.ØRemoveWhiteSpaces()}");

      //+ Test
      Assert.AreEqual
         (htmlDoc.DocumentNode.OuterHtml.ØRemoveWhiteSpaces()
          , expected.ØRemoveWhiteSpaces()
          , true);
   }


   /// <summary>
   /// Prüft Ensure_Complete_HtmlDoc()
   /// mit nur div-Inhalt 
   /// </summary>
   [TestMethod]
   public void Test_Ensure_Complete_HtmlDoc__HeadAndBodyAndData() {
      //+ Prepare
      //++ Als Quelle: Nur <div>
      var newHtmlDoc = @"<head></head><body><div>...</div></body>";

      var expected = @"<!DOCTYPE html>
         <html>
	         <head>
	         </head>
	         <body>
		         <div>...</div>
	         </body>
         </html>";

      var htmlDoc = HtmlTools.Ensure_Complete_HtmlDoc(Default_HtmlDoc_Structure, newHtmlDoc);

      //‼ 🚩 Debug: Ins ClipBoard kopieren
      // ClipboardService.SetText(PrettyPrint(htmlDoc.DocumentNode.OuterHtml));
      // ClipboardService.SetText($"Expected:\n{expected.ØRemoveWhiteSpaces()}\n\nWe've got:\n{htmlDoc.DocumentNode.OuterHtml.ØRemoveWhiteSpaces()}");

      //+ Test
      Assert.AreEqual
         (htmlDoc.DocumentNode.OuterHtml.ØRemoveWhiteSpaces()
          , expected.ØRemoveWhiteSpaces()
          , true);
   }


   /// <summary>
   /// Prüft Ensure_Complete_HtmlDoc()
   /// mit nur div-Inhalt 
   /// </summary>
   [TestMethod]
   public void Test_Ensure_Complete_HtmlDoc__MainAndBodyAndData() {
      //+ Prepare
      //++ Als Quelle: Nur <div>
      var newHtmlDoc = @"<html><body><div>...</div></body></html>";

      var expected = @"<!DOCTYPE html>
         <html>
	         <head>
	         </head>
	         <body>
		         <div>...</div>
	         </body>
         </html>";

      var htmlDoc = HtmlTools.Ensure_Complete_HtmlDoc(Default_HtmlDoc_Structure, newHtmlDoc);

      //‼ 🚩 Debug: Ins ClipBoard kopieren
      // ClipboardService.SetText(PrettyPrint(htmlDoc.DocumentNode.OuterHtml));
      // ClipboardService.SetText($"Expected:\n{expected.ØRemoveWhiteSpaces()}\n\nWe've got:\n{htmlDoc.DocumentNode.OuterHtml.ØRemoveWhiteSpaces()}");

      //+ Test
      Assert.AreEqual
         (htmlDoc.DocumentNode.OuterHtml.ØRemoveWhiteSpaces()
          , expected.ØRemoveWhiteSpaces()
          , true);
   }


   /// <summary>
   /// Prüft Ensure_Complete_HtmlDoc()
   /// mit nur div-Inhalt 
   /// </summary>
   [TestMethod]
   public void Test_Ensure_Complete_HtmlDoc__MainNoBodyButData() {
      //+ Prepare
      //++ Als Quelle: Nur <div>
      var newHtmlDoc = @"<html><div>...</div></html>";

      var expected = @"<!DOCTYPE html>
         <html>
	         <head>
	         </head>
	         <body>
		         <div>...</div>
	         </body>
         </html>";

      var htmlDoc = HtmlTools.Ensure_Complete_HtmlDoc(Default_HtmlDoc_Structure, newHtmlDoc);

      //‼ 🚩 Debug: Ins ClipBoard kopieren
      // ClipboardService.SetText(PrettyPrint(htmlDoc.DocumentNode.OuterHtml));
      // ClipboardService.SetText($"Expected:\n{expected.ØRemoveWhiteSpaces()}\n\nWe've got:\n{htmlDoc.DocumentNode.OuterHtml.ØRemoveWhiteSpaces()}");

      //+ Test
      Assert.AreEqual
         (htmlDoc.DocumentNode.OuterHtml.ØRemoveWhiteSpaces()
          , expected.ØRemoveWhiteSpaces()
          , true);
   }


   /// <summary>
   /// Prüft Ensure_Complete_HtmlDoc()
   /// mit nur div-Inhalt 
   /// </summary>
   [TestMethod]
   public void Test_Ensure_Complete_HtmlDoc__FullHtmlNoDocType() {
      //+ Prepare
      //++ Als Quelle: Nur <div>
      var newHtmlDoc = @"<html><head></head><body><div>...</div></body></html>";

      var expected = @"<!DocType html>
         <html>
	         <head>
	         </head>
	         <body>
		         <div>...</div>
	         </body>
         </html>";

      var htmlDoc = HtmlTools.Ensure_Complete_HtmlDoc(Default_HtmlDoc_Structure, newHtmlDoc);

      //‼ 🚩 Debug: Ins ClipBoard kopieren
      // ClipboardService.SetText(PrettyPrint(htmlDoc.DocumentNode.OuterHtml));

      // ClipboardService.SetText($"Expected:\n{expected.ØRemoveWhiteSpaces()}\n\nWe've got:\n{htmlDoc.DocumentNode.OuterHtml.ØRemoveWhiteSpaces()}");

      //+ Test
      Assert.AreEqual
         (htmlDoc.DocumentNode.OuterHtml.ØRemoveWhiteSpaces()
          , expected.ØRemoveWhiteSpaces()
          , true);
   }


   /// <summary>
   /// Prüft Ensure_Complete_HtmlDoc()
   /// 
   /// </summary>
   [TestMethod]
   public void Test_Ensure_Complete_HtmlDoc__WithDocType() {
      //‼ 🚩 Debug: Ins ClipBoard kopieren
      // ClipboardService.SetText(PrettyPrint(htmlDoc.DocumentNode.OuterHtml));
      int a = 1;
   }

}
