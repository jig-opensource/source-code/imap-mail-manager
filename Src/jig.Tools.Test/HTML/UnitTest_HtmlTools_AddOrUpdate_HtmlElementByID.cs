using System.Diagnostics;
using HtmlAgilityPack;
using jig.Tools.HTML;
using jig.Tools.Lib.Jo_CSS_Parser;


namespace jig.Tools.Test.HTML;

/// <summary>
/// Testet AddOrUpdate_HtmlElementByID()
/// </summary>
[TestClass]
public class UnitTest_HtmlTools_AddOrUpdate_HtmlElementByID {

   // Die HTML Element ID, die in den E-Mails genützt wird 
   public static readonly string HtmlElementID = "jigData";


   #region HTML strings

   /// <summary>
   /// Simuliert ein HTML ohne das gesuchte Element mit ID
   /// </summary>
   readonly static public string htmlOhneElementID =
      @"<!DOCTYPE html>
            <html>
            <head>
            </head>
            <body style=""text-align:center"">
               <h1>Get element by class</h1>
               <p class=""classDemo"">Demo for class selector</p>
             </body>
            </html>";

   /// <summary>
   /// Simuliert nur das HTML Element
   /// in einer neuen Version
   /// </summary>
   readonly static public string htmlElementIDNeu =
      @"<div id=""jigData"" class=""jigID"">
            Neuer Inhalt
			   <ul class=""jigTag"">
				   <li>#MbxFldrXx1</li>
				   <li>#MbxFldrXx2</li>
			   </ul>
			   <div class=""jigHidden"">
				   hallo2
			   </div>
			   <div class=""jigTag jigHidden"">
				   hallo3
			   </div>
		   </div>";

   /// <summary>
   /// Simuliert ein HTML
   /// mit dem gesuchten Element mit ID
   /// in einer alten Version
   /// </summary>
   readonly static public string htmlMitElementIDAlt =
      @"<!DOCTYPE html>
            <html>
            <head>
            </head>
            <body style=""text-align:center"">
               <h1>Get element by class</h1>
               <p class=""classDemo"">Demo for class selector</p>
             
		         <div id=""jigData"" class=""jigID"">
                  Alter Inhalt
			         <ul class=""jigTag"">
				         <li>#MbxFldrXx1</li>
				         <li>#MbxFldrXx2</li>
			         </ul>
			         <div class=""jigHidden"">
				         hallo2
			         </div>
			         <div class=""jigTag jigHidden"">
				         hallo3
			         </div>
		         </div>

            </body>
            </html>";

   #endregion HTML strings


   /// <summary>
   /// Prüft HtmlElementByID_HtmlElement()
   /// mit einem Html, das das Element nicht hat
   /// und ergänzt das HTML als Text
   /// </summary>
   [TestMethod]
   public void Test_AddOrUpdate_HtmlElementByID_HtmlElementMissing_Text() {
      //+ Das Html Dokument
      var oHtmlOhneStyles = new HtmlDocument();
      oHtmlOhneStyles.LoadHtml(htmlOhneElementID);

      //+ Das neue Element
      var oHtmlNewElement = new HtmlDocument();
      oHtmlNewElement.LoadHtml(htmlElementIDNeu);

      //+ Fehlenden Style ergänzen

      //+ Test
      HtmlNode htmlElementNodeV1 = oHtmlOhneStyles.GetElementbyId(HtmlElementID);

      //++ Das Html Element existiert nicht 
      Assert.IsTrue(htmlElementNodeV1 == null);

      //++ Das Element ergänzen
      var (htmlUpdated, resHtmlDoc) = HtmlTools.AddOrUpdate_HtmlElementByID
         (oHtmlOhneStyles
          , HtmlElementID
          , htmlElementIDNeu);

      //+ Test
      //++ Das HtmlDoc wurde aktualisiert
      Assert.IsTrue(htmlUpdated);

      //++ Das Html Element existiert jetzt 
      HtmlNode htmlElementNodeV2 = resHtmlDoc.GetElementbyId(HtmlElementID);
      Assert.IsTrue(htmlElementNodeV2 != null);

      // Debug
      Debug.WriteLine(resHtmlDoc.DocumentNode.OuterHtml);
   }


   /// <summary>
   /// Prüft HtmlElementByID_HtmlElement()
   /// mit einem Html, das das Element nicht hat
   /// und ergänzt das HTML als HTML Doc Node
   /// </summary>
   [TestMethod]
   public void Test_AddOrUpdate_HtmlElementByID_HtmlElementMissing_HtmlDoc() {
      //+ Das Html Dokument
      var oHtmlOhneStyles = new HtmlDocument();
      oHtmlOhneStyles.LoadHtml(htmlOhneElementID);

      //+ Das neue Element
      var oHtmlNewElement = new HtmlDocument();
      oHtmlNewElement.LoadHtml(htmlElementIDNeu);

      //+ Fehlenden Style ergänzen

      //+ Test
      HtmlNode htmlElementNodeV1 = oHtmlOhneStyles.GetElementbyId(HtmlElementID);

      //++ Das Html Element existiert nicht 
      Assert.IsTrue(htmlElementNodeV1 == null);

      //++ Das Element ergänzen
      var (htmlUpdated, resHtmlDoc) = HtmlTools.AddOrUpdate_HtmlElementByID
         (oHtmlOhneStyles
          , HtmlElementID
          , oHtmlNewElement.DocumentNode);

      //+ Test
      //++ Das HtmlDoc wurde aktualisiert
      Assert.IsTrue(htmlUpdated);

      //++ Das Html Element existiert jetzt 
      HtmlNode htmlElementNodeV2 = resHtmlDoc.GetElementbyId(HtmlElementID);
      Assert.IsTrue(htmlElementNodeV2 != null);

      // Debug
      Debug.WriteLine(resHtmlDoc.DocumentNode.OuterHtml);
   }


   /// <summary>
   /// Prüft HtmlElementByID_HtmlElement()
   /// mit einem Html, das bereits existiert, aber veraltet ist
   /// </summary>
   [TestMethod]
   public void Test_AddOrUpdate_HtmlElementByID_HtmlElementExisting() {
      //+ Das Html Dokument
      var oHtmlOhneStyles = new HtmlDocument();
      oHtmlOhneStyles.LoadHtml(htmlMitElementIDAlt);

      //+ Das neue Element
      var oHtmlNewElement = new HtmlDocument();
      oHtmlNewElement.LoadHtml(htmlElementIDNeu);

      //+ Fehlenden Style ergänzen

      //+ Test
      HtmlNode htmlElementNodeV1 = oHtmlOhneStyles.GetElementbyId(HtmlElementID);

      //++ Das Element existiert
      Assert.IsTrue(htmlElementNodeV1 != null);

      //++ Das Element hat den alten Text
      var containsOldText = htmlElementNodeV1.InnerText.Contains
         ("Alter Inhalt", StringComparison.OrdinalIgnoreCase);
      Assert.IsTrue(containsOldText == true);

      //+ Das Element ergänzen
      var (htmlUpdated, resHtmlDoc) = HtmlTools.AddOrUpdate_HtmlElementByID
         (oHtmlOhneStyles
          , HtmlElementID
          , oHtmlNewElement.DocumentNode);

      //+ Test
      //++ Das HtmlDoc wurde aktualisiert
      Assert.IsTrue(htmlUpdated);

      //++ Das Element existiert noch
      var htmlElementNodeV2 = resHtmlDoc.GetElementbyId(HtmlElementID);
      Assert.IsTrue(htmlElementNodeV2 != null);

      //++ Das Element hat den neuen Text
      var containsNewText = htmlElementNodeV2.InnerText.Contains
         ("Neuer Inhalt", StringComparison.OrdinalIgnoreCase);
      Assert.IsTrue(containsNewText == true);

      // Debug
      Debug.WriteLine(resHtmlDoc.DocumentNode.OuterHtml);
   }

}
