using System.Diagnostics;
using HtmlAgilityPack;
using jig.Tools.HTML;
using jig.Tools.Lib.Jo_CSS_Parser;


namespace jig.Tools.Test.HTML;


/// <summary>
/// Testet AddOrUpdate_HtmlHeadStyle()
/// </summary>
[TestClass]
public class UnitTest_HtmlTools_AddOrUpdate_HtmlHeadStyle {

   #region jig CSS Styles

   /// <summary>
   /// Simuliert den jig CSS Style Red
   /// </summary>
   readonly static public string mailCssRed =
      @"<style>
         /* CSS Klassen für die Mail-Formatierung */
         .jigTag { color: red; }
         .jigHidden { display:none; }
        </style>
      ";

   /// <summary>
   /// Simuliert den jig CSS Style Green
   /// </summary>
   readonly static public string mailCssGreen =
      @"<style>
         /* CSS Klassen für die Mail-Formatierung */
         .jigTag { color: green; }
         .jigHidden { display:none; }
        </style>
      ";

   #endregion jig CSS Styles


   #region HTML strings

   /// <summary>
   /// Simuliert ein HTML ohne Head\Style
   /// </summary>
   readonly static public string htmlOhneStyles =
      @"<!DOCTYPE html>
            <html>
            <head>
            <title>
            Class demo
            </title>
            </head>
            <body style=""text-align:center"">
               <h1>Get element by class</h1>
               <p class=""classDemo"">Demo for class selector</p>
             </body>
            </html>";

   /// <summary>
   /// Simuliert ein HTML mit fremden Head\Style
   /// </summary>
   readonly static public string htmlOtherStyles =
      @"<!DOCTYPE html>
            <html>
            <head>
            <title>
            Class demo
            </title>
            <style>
               .classDemo{
                  color:orange;
                  font-size:25px;
               }
            </style>
            </head>
            <body style=""text-align:center"">
               <h1>Get element by class</h1>
               <p class=""classDemo"">Demo for class selector</p>
             </body>
            </html>";

   /// <summary>
   /// Simuliert ein HTML mit fremden Head\Style
   /// und dem JIG CSS Style Yellow
   /// </summary>
   readonly static public string htmlOtherStylesAndJigYellow =
      @"<!DOCTYPE html>
            <html>
            <head>
            <title>
            Class demo
            </title>
            <style>
               .classDemo{
                  color:orange;
                  font-size:25px;
               }
            </style>
            <style>
               .jigTag { color: Yellow; }
               .jigHidden { display:none; }
            </style>
            </head>
            <body style=""text-align:center"">
               <h1>Get element by class</h1>
               <p class=""classDemo"">Demo for class selector</p>
             </body>
            </html>";

   #endregion HTML strings


   /// <summary>
   /// Prüft AddOrUpdateHtmlHeadStyle()
   /// wenn Html\Head keine Styles hat
   /// </summary>
   [TestMethod]
   public void Test_AddOrUpdateHtmlHeadStyle_HtmlOhneStyles() {
      var oHtmlOhneStyles = new HtmlDocument();
      oHtmlOhneStyles.LoadHtml(htmlOhneStyles);

      //+ Fehlenden Style ergänzen

      //++ Test
      var headStyleNodesVor = oHtmlOhneStyles.DocumentNode.SelectNodes
         ("//head/style").ØOrEmptyIfNull();
      Assert.IsTrue(headStyleNodesVor.Count() == 0);

      //++ Ergänzen
      var (htmlUpdated, resHtmlDoc) = HtmlTools.AddOrUpdate_HtmlHeadStyle
         (oHtmlOhneStyles
          , mailCssGreen
          , ".jigTag");

      //+ Test
      //++ Das HtmlDoc wurde aktualisiert
      Assert.IsTrue(htmlUpdated);

      //++ Das Html Element existiert jetzt 
      var headStyleNodesNach = resHtmlDoc.DocumentNode.SelectNodes
         ("//head/style").ØOrEmptyIfNull();
      Assert.IsTrue(headStyleNodesNach.Count() == 1);

      // Debug
      Debug.WriteLine(resHtmlDoc.DocumentNode.OuterHtml);
   }


   /// <summary>
   /// Prüft AddOrUpdateHtmlHeadStyle()
   /// wenn Html\Head einen fremden Style hat 
   /// </summary>
   [TestMethod]
   public void Test_AddOrUpdateHtmlHeadStyle_HtmlOtherStyles() {
      var oHtmlOhneStyles = new HtmlDocument();
      oHtmlOhneStyles.LoadHtml(htmlOtherStyles);

      //+ Den eigenen Style ergänzen

      //++ Test
      var headStyleNodesVor = oHtmlOhneStyles.DocumentNode.SelectNodes
         ("//head/style").ØOrEmptyIfNull();
      Assert.IsTrue(headStyleNodesVor.Count() == 1);

      //++ Ergänzen
      var (htmlUpdated, resHtmlDoc) = HtmlTools.AddOrUpdate_HtmlHeadStyle
         (oHtmlOhneStyles
          , mailCssGreen
          , ".jigTag");

      //+ Test
      //++ Das HtmlDoc wurde aktualisiert
      Assert.IsTrue(htmlUpdated);

      //++ Das Html Element existiert jetzt 
      var headStyleNodesNach = resHtmlDoc.DocumentNode.SelectNodes
         ("//head/style").ØOrEmptyIfNull();
      Assert.IsTrue(headStyleNodesNach.Count() == 2);

      // Debug
      Debug.WriteLine(resHtmlDoc.DocumentNode.OuterHtml);
   }


   /// <summary>
   /// Prüft AddOrUpdateHtmlHeadStyle()
   /// wenn Html\Head einen veralteten eigenen Style hat 
   /// </summary>
   [TestMethod]
   public void Test_AddOrUpdateHtmlHeadStyle_HtmlOldStyles() {
      var oHtmlOhneStyles = new HtmlDocument();
      oHtmlOhneStyles.LoadHtml(htmlOtherStyles);

      //+ Den eigenen Style ergänzen

      //++ Test
      var headStyleNodesV1 = oHtmlOhneStyles.DocumentNode.SelectNodes
         ("//head/style").ØOrEmptyIfNull();
      Assert.IsTrue(headStyleNodesV1.Count() == 1);

      //+ Den Style Green setzen
      var (htmlUpdatedV2, resHtmlDocV2) = HtmlTools.AddOrUpdate_HtmlHeadStyle
         (oHtmlOhneStyles
          , mailCssGreen
          , ".jigTag");

      //+ Test
      //++ Das HtmlDoc wurde aktualisiert
      Assert.IsTrue(htmlUpdatedV2);

      //++ Das Html Element existiert jetzt
      var headStyleNodesV2 = resHtmlDocV2.DocumentNode.SelectNodes
         ("//head/style").ØOrEmptyIfNull();
      Assert.IsTrue(headStyleNodesV2.Count() == 2);

      // Haben wir den Green Style?
      foreach (var thisStyle in headStyleNodesV2) {
         var objThisCssStyle = new Lib.Jo_CSS_Parser.Parser.CssParser();
         objThisCssStyle.Css = thisStyle.InnerHtml;

         // Haben wir den CSS Selector .jigTag?
         if (objThisCssStyle.CssSelectorExist(".jigTag")) {
            // Ja: Stimmt die Farbe?
            var colorProp     = objThisCssStyle.GetProperty(".jigTag", CssProperty.Color);
            Assert.IsTrue(colorProp.PropertyValue.Equals("green", StringComparison.OrdinalIgnoreCase));
         }
      }

      //+ Den Style auf Red ändern
      var (htmlUpdatedV3, resHtmlDocV3) = HtmlTools.AddOrUpdate_HtmlHeadStyle
         (oHtmlOhneStyles
          , mailCssRed
          , ".jigTag");

      //+ Test
      //++ Das HtmlDoc wurde aktualisiert
      Assert.IsTrue(htmlUpdatedV3);

      //++ Wir haben immer noch zwei Styles
      var headStyleNodesV3 = resHtmlDocV3.DocumentNode.SelectNodes
         ("//head/style").ØOrEmptyIfNull();
      // Immer noch 2 Styles
      Assert.IsTrue(headStyleNodesV3.Count() == 2);

      //++ Haben wir den Red Style?
      foreach (var thisStyle in headStyleNodesV3) {
         var objThisCssStyle = new Lib.Jo_CSS_Parser.Parser.CssParser();
         objThisCssStyle.Css = thisStyle.InnerHtml;

         // Haben wir den CSS Selector .jigTag?
         if (objThisCssStyle.CssSelectorExist(".jigTag")) {
            // Ja: Stimmt die Farbe?
            var colorProp = objThisCssStyle.GetProperty(".jigTag", CssProperty.Color);

            Assert.IsTrue
               (colorProp.PropertyValue.Equals("red", StringComparison.OrdinalIgnoreCase));
         }
      }

      // Debug
      Debug.WriteLine(resHtmlDocV3.DocumentNode.OuterHtml);
   }

}
