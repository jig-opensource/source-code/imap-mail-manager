using ImapMailManager.BO;


namespace jig.Tools.Test;

[TestClass]
public class UnitTest_ExtensionMethods_BusinessLogic_MailSubject {

   /// <summary>
   /// Testen von: Get_EmpfangenerEMailTyp()
   /// für: EmpfangenerEMailTyp.Unknown
   /// </summary>
   /// <param name="testString"></param>
   /// <param name="expectedType"></param>
   [TestMethod]
   [DataRow
      (    @"#10-0058 . RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Unknown
         , DisplayName = "01")]
   public void Test_Get_EmpfangenerEMailTyp_Unknown(
      string                               testString
      , BL_MailSubject.EmpfangenerEMailTyp expectedType) {
      var res = BL_MailSubject.Get_EmpfangenerEMailTyp(testString);
      Assert.AreEqual(expectedType, res);
   }


   #region Test von einzelnen Mail Subject Präfixes

   /// <summary>
   /// Testen von: Get_EmpfangenerEMailTyp()
   /// für: EmpfangenerEMailTyp.Antwort
   /// </summary>
   [TestMethod]
   [DataRow
      (    @"Antwort: #10-0058 . RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "01")]
   [DataRow
      (    @"AntWort:RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "02")]
   [DataRow
      (    @"Re: RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "03")]
   [DataRow
      (    @"Re;RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "04")]
   [DataRow
      (    @"RE:; RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "05")]
   [DataRow
      (    @"re:: RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "06")]
   [DataRow
      (    @"AW RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "07")]
   [DataRow
      (    @"Aw: RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "08")]
   [DataRow
      (    @"P. S. RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "09")]
   [DataRow
      (    @"P.S. RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "10")]
   [DataRow
      (    @"P.S.: RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "11")]
   [DataRow
      (    @"R: RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "12")]
   [DataRow
      (    @"R; RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "13")]
   public void Test_Get_EmpfangenerEMailTyp_Antwort(
      string                               testString
      , BL_MailSubject.EmpfangenerEMailTyp expectedType) {
      var res = BL_MailSubject.Get_EmpfangenerEMailTyp(testString);
      Assert.AreEqual(expectedType, res);
   }


   /// <summary>
   /// Testen von: Get_EmpfangenerEMailTyp()
   /// für: EmpfangenerEMailTyp.Weitergeleitet
   /// </summary>
   [TestMethod]
   [DataRow
      (    @"Wg: #10-0058 . RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , DisplayName = "01")]
   [DataRow
      (    @"WG: RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , DisplayName = "02")]
   [DataRow
      (    @"WG; RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , DisplayName = "03")]
   [DataRow
      (    @"WG;: RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , DisplayName = "04")]
   [DataRow
      (    @"FWD RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , DisplayName = "05")]
   [DataRow
      (    @"FWD: RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , DisplayName = "06")]
   [DataRow
      (    @"FwD RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , DisplayName = "07")]
   [DataRow
      (    @"Fw: RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , DisplayName = "08")]
   [DataRow
      (    @"FW RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , DisplayName = "09")]
   public void Test_Get_EmpfangenerEMailTyp_Weitergeleitet(
      string                               testString
      , BL_MailSubject.EmpfangenerEMailTyp expectedType) {
      var res = BL_MailSubject.Get_EmpfangenerEMailTyp(testString);
      Assert.AreEqual(expectedType, res);
   }

   #endregion Test von einzelnen Mail Subject Präfixes

   #region Test von mehreren Mail Subject Präfixes

   /// <summary>
   /// Testen von: Get_EmpfangenerEMailTyp()
   /// für: EmpfangenerEMailTyp.Antwort
   /// </summary>
   [TestMethod]
   [DataRow
      (    @"Antwort: FW #10-0058 . RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "01")]
   [DataRow
      (    @"AntWort: FWd RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "02")]
   [DataRow
      (    @"Re: FWd: RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "03")]
   [DataRow
      (    @"Re; WG: RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "04")]
   [DataRow
      (    @"RE:; WG RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Antwort
         , DisplayName = "05")]
   public void Test_Get_EmpfangenerEMailTyp_Antwort_MultipleSubjectPrefixes(
      string                               testString
      , BL_MailSubject.EmpfangenerEMailTyp expectedType) {
      var res = BL_MailSubject.Get_EmpfangenerEMailTyp(testString);
      Assert.AreEqual(expectedType, res);
   }


   /// <summary>
   /// Testen von: Get_EmpfangenerEMailTyp()
   /// für: EmpfangenerEMailTyp.Weitergeleitet
   /// </summary>
   [TestMethod]
   [DataRow
      (    @"Wg: Re #10-0058 . RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , DisplayName = "01")]
   [DataRow
      (    @"WG: Re: RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , DisplayName = "02")]
   [DataRow
      (    @"WG; Antwort:RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , DisplayName = "03")]
   [DataRow
      (    @"WG;: AW: Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , DisplayName = "04")]
   [DataRow
      (    @"FWD aw RL Formular: Biblische Frage"
         , BL_MailSubject.EmpfangenerEMailTyp.Weitergeleitet
         , DisplayName = "05")]
   public void Test_Get_EmpfangenerEMailTyp_Weitergeleitet_MultipleSubjectPrefixes(
      string                                                           testString
      , BL_MailSubject.EmpfangenerEMailTyp expectedType) {
      var res = BL_MailSubject.Get_EmpfangenerEMailTyp(testString);
      Assert.AreEqual(expectedType, res);
   }

   #endregion Test von mehreren Mail Subject Präfixes

   #region Bereinigen von Mail Subject Präfixes

   /// <summary>
   /// Testen von: ØCleanEmailSubjectPrefix()
   /// für: einzelne Präfixes für FWD:
   /// </summary>
   [TestMethod]
   [DataRow
      (    @"Wg: #10-0058 . RL Formular: Biblische Frage"
         , @"Fw: #10-0058 . RL Formular: Biblische Frage"
         , DisplayName = "01")]
   [DataRow
      (    @"Wg RL Formular:             Biblische Frage"
         , @"Fw: RL Formular: Biblische Frage"
         , DisplayName = "02")]
   [DataRow
      (    @"Fwd: RL Formular: Biblische Frage"
         , @"Fw: RL Formular: Biblische Frage"
         , DisplayName = "03")]
   [DataRow
      (    @"Fw RL Formular: Biblische Frage"
         , @"Fw: RL Formular: Biblische Frage"
         , DisplayName = "04")]
   [DataRow
      (    @"Fw: RL Formular: Biblische Frage"
         , @"Fw: RL Formular: Biblische Frage"
         , DisplayName = "05")]
   public void Test_ØCleanEmailSubjectPrefix_FWD_SingleSubjectPrefixes(
      string   testString
      , string expectedType) {
      var res = testString.ØCleanEmailSubjectPrefix();
      Assert.AreEqual(expectedType, res);
   }


   /// <summary>
   /// Testen von: ØCleanEmailSubjectPrefix()
   /// für: einzelne Präfixes for AW:
   /// </summary>
   [TestMethod]
   [DataRow
      (    @"Wg: #10-0058 . RL Formular: Biblische Frage"
         , @"Fw: #10-0058 . RL Formular: Biblische Frage"
         , DisplayName = "01")]
   [DataRow
      (    @"Wg RL Formular:             Biblische Frage"
         , @"Fw: RL Formular: Biblische Frage"
         , DisplayName = "02")]
   [DataRow
      (    @"Fwd: RL Formular: Biblische Frage"
         , @"Fw: RL Formular: Biblische Frage"
         , DisplayName = "03")]
   [DataRow
      (    @"Fw RL Formular: Biblische Frage"
         , @"Fw: RL Formular: Biblische Frage"
         , DisplayName = "04")]
   [DataRow
      (    @"Fw: RL Formular: Biblische Frage"
         , @"Fw: RL Formular: Biblische Frage"
         , DisplayName = "05")]
   public void Test_ØCleanEmailSubjectPrefix_AW_SingleSubjectPrefixes(
      string   testString
      , string expectedType) {
      var res = testString.ØCleanEmailSubjectPrefix();
      Assert.AreEqual(expectedType, res);
   }


   /// <summary>
   /// Testen von: ØCleanEmailSubjectPrefix()
   /// für: mehrere Präfixes, primary is AW:
   /// </summary>
   [TestMethod]

   // Mehrere unterschiedliche Präfixe
   [DataRow
      (@"Wg: AW Re: #10-0058: Biblische Frage"

       // Expected Subject ohne Präfixe
       , @"#10-0058: Biblische Frage"

       // Expected Subject mit dem ersten Präfix, standardisiert
       , @"Fw: #10-0058: Biblische Frage"

       // Expected Subject mit dem ersten + letzten Präfix, standardisiert
       , @"Fw: Re: #10-0058: Biblische Frage"
       , DisplayName = "01")]

   // Ein Präfix
   [DataRow
      (    @"Wg RL Formular:             Biblische Frage"
         , @"RL Formular: Biblische Frage"
         , @"Fw: RL Formular: Biblische Frage"
         , @"Fw: RL Formular: Biblische Frage"
         , DisplayName = "02")]

   // Kein Präfix
   [DataRow
      (    @"RL Formular: Biblische Frage"
         , @"RL Formular: Biblische Frage"
         , @"RL Formular: Biblische Frage"
         , @"RL Formular: Biblische Frage"
         , DisplayName = "03")]

   // Zwei identische Präfixe (Aliasse)
   [DataRow
      (    @"Fw WG: RL Formular: Biblische Frage"
         , @"RL Formular: Biblische Frage"
         , @"Fw: RL Formular: Biblische Frage"
         , @"Fw: RL Formular: Biblische Frage"
         , DisplayName = "04")]
   public void Test_ØCleanEmailSubjectPrefix_AW_ManySubjectPrefixes(
      string   testString
      , string expected_SubjectWithoutPrefixes
      , string expected_SubjectWithFirstPrefixes
      , string expected_SubjectWithFirstAndLastPrefixes) {
      var res_SubjectWithoutPrefixes          = testString.ØCleanEmailSubjectPrefix(false, false);
      var res_SubjectWithFirstPrefixes        = testString.ØCleanEmailSubjectPrefix(true,  false);
      var res_SubjectWithFirstAndLastPrefixes = testString.ØCleanEmailSubjectPrefix(true,  true);

      Assert.AreEqual
         (expected_SubjectWithoutPrefixes
          , res_SubjectWithoutPrefixes
          , "SubjectWithoutPrefixes");

      Assert.AreEqual
         (expected_SubjectWithFirstPrefixes
          , res_SubjectWithFirstPrefixes
          , "SubjectWithFirstPrefixes");

      Assert.AreEqual
         (expected_SubjectWithFirstAndLastPrefixes
          , res_SubjectWithFirstAndLastPrefixes
          , "SubjectWithFirstAndLastPrefixes");
   }

   #endregion Bereinigen von Mail Subject Präfixes

}
