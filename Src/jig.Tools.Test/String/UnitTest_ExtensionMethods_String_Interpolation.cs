using jig.Tools.String;


namespace jig.Tools.Test.String;

[TestClass]
public class UnitTest_ExtensionMethods_String_Interpolation {

   /// <summary>
   /// struct wird in Fluid nicht direkt unterstützt
   /// </summary>
   /// <param name="Vorname"></param>
   /// <param name="Nachname"></param>
   record struct TestRec(string Vorname, string Nachname);
   record class TestClass(string Vorname, string Nachname);


   [TestMethod]
   public void Test_Interpolate() {
      var objArgClass = new TestClass("John", "Doe");
      var expected = "Name: Doe";
      
      var res = objArgClass.ØInterpolate("Name: {{person.Nachname}}", "person");
      Assert.AreEqual(expected, res);
   }



}
