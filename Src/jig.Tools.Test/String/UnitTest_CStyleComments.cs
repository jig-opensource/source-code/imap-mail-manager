using jig.Tools.String;
using TextCopy;


namespace jig.Tools.Test.String;

[TestClass]
public class UnitTest_CStyleComments {

   /// <summary>
   /// Testet die Remove_CStyleComments Funktion
   /// </summary>
   [TestMethod]
   public void Test_Remove_CStyleComments() {
      string testStr = @"Hallo
/* 1 - This is multi-line
comment */
p1 {
  color: red; /* Hallo */
}
/**** 
* 2- multi-line comment 
*/
p2 {
  color: red; /* Hallo */
}";

      var expectedStr = @"Hallo

p1 {
  color: red; 
}

p2 {
  color: red; 
}";

      var res = CStyleComments.Remove_CStyleComments(testStr);
      // ClipboardService.SetText(res);
      Assert.IsTrue(expectedStr.Equals(res));
   }

}
