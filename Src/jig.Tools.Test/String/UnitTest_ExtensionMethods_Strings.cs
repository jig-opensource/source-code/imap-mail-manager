using jig.Tools.String;


namespace jig.Tools.Test.String;

[TestClass]
public class UnitTest_ExtensionMethods_Strings {

   [TestMethod]
   [DataRow(null, "", DisplayName = "01")]
   [DataRow("", "", DisplayName = "02")]
   [DataRow("IBMMakeStuffAndSellIt", "IBM Make Stuff And Sell It", DisplayName = "03")]
   [DataRow("HansKrampulz", "Hans Krampulz", DisplayName = "04")]
   public void Test_ØSplitCamelCase(string? test, string expexted) {
      var res = test.ØSplitCamelCase();
      Assert.AreEqual(expexted, res);
   }


   [TestMethod]
   public void Test_ØTrimStringStart() {
      //+ Prepare
      var full         = "ImapMailManager.Test.HTML.HtmlAgilityPack";
      var testStartStr = "ImapMailManager.Test";
      var expected     = ".HTML.HtmlAgilityPack";

      //+ Do it
      var res = full.ØTrimStringStart(testStartStr);

      //+ Test
      Assert.AreEqual(res, expected);
   }


   [TestMethod]

   //       Test, Str         , Start, Len, Expected
   [DataRow
      (    1
         , "0123456789"
         , 0
         , null
         , "0123456789"
         , DisplayName = "01")]
   [DataRow
      (    2
         , "0123456789"
         , 5
         , null
         , "56789"
         , DisplayName = "02")]
   [DataRow
      (    3
         , "0123456789"
         , 0
         , 2
         , "01"
         , DisplayName = "03")]
   [DataRow
      (    4
         , "0123456789"
         , 5
         , 2
         , "56"
         , DisplayName = "04")]
   [DataRow
      (    5
         , "0123456789"
         , 0
         , 10
         , "0123456789"
         , DisplayName = "05")]
   [DataRow
      (    6
         , "0123456789"
         , 0
         , 15
         , "0123456789"
         , DisplayName = "06")]
   [DataRow
      (    7
         , "0123456789"
         , 5
         , 5
         , "56789"
         , DisplayName = "07")]
   [DataRow
      (    8
         , "0123456789"
         , 5
         , 15
         , "56789"
         , DisplayName = "08")]
   public void Test_ØSubstring(
      int      testNo
      , string testString
      , int    start
      , int?   len
      , string expectedValue) {
      //+ Genereller Test mit den Parametern
      Assert.AreEqual(testString.ØSubstring(start, len), expectedValue);

      //+ Einzelner Test mit den berechneten erwarteten Werten
      switch (testNo) {
         case 1: {
            var ExpectedTest1 = "0123456789".Substring(0, 10);
            Assert.AreEqual(testString.ØSubstring(start, len), ExpectedTest1);

            break;
         }

         case 2: {
            var ExpectedTest2 = "0123456789".Substring(5, 5);
            Assert.AreEqual(testString.ØSubstring(start, len), ExpectedTest2);

            break;
         }

         case 3: {
            var ExpectedTest3 = "0123456789".Substring(0, 2);
            Assert.AreEqual(testString.ØSubstring(start, len), ExpectedTest3);

            break;
         }

         case 4: {
            var ExpectedTest4 = "0123456789".Substring(5, 2);
            Assert.AreEqual(testString.ØSubstring(start, len), ExpectedTest4);

            break;
         }

         case 5: {
            var ExpectedTest5 = "0123456789".Substring(0, 10);
            Assert.AreEqual(testString.ØSubstring(start, len), ExpectedTest5);

            break;
         }

         case 6: {
            var ExpectedTest6 = "0123456789".Substring(0, 10);
            Assert.AreEqual(testString.ØSubstring(start, len), ExpectedTest6);

            break;
         }

         case 7: {
            var ExpectedTest7 = "0123456789".Substring(5, 5);
            Assert.AreEqual(testString.ØSubstring(start, len), ExpectedTest7);

            break;
         }

         case 8: {
            var ExpectedTest8 = "0123456789".Substring(5, 5);
            Assert.AreEqual(testString.ØSubstring(start, len), ExpectedTest8);

            break;
         }
      }
   }


   /// <summary>
   /// Testet die Funktion ØReduceWhiteSpaces 
   /// Löscht die CrLf 
   /// </summary>
   [TestMethod]
   public void Test_ØReduceWhiteSpaces_DeleteCrLf() {
      //+ Prepare
      // Ein CRLF werden auch in " " konvertiert, deshalb das Leerzeichen vor dem !
      var expected = "Hallo Welt !";

      var test = @"

      Hallo 

Welt
!

";

      //+ Start…
      var res = test.ØReduceWhiteSpaces();

      //+ Test
      Assert.AreEqual(res, expected);
   }


   /// <summary>
   /// Testet die Funktion ØReduceWhiteSpaces
   /// Behält die CrLf 
   /// </summary>
   [TestMethod]
   public void Test_ØReduceWhiteSpaces_KeepCrLf() {
      //+ Prepare
      // Ein CRLF werden auch in " " konvertiert, deshalb das Leerzeichen vor dem !
      var expected = @"Hallo
Welt
!";

      var test = @"

      Hallo 

Welt
!

";

      //+ Start…
      var res = test.ØReduceWhiteSpaces(true);

      //+ Test
      Assert.AreEqual(res, expected);
   }


   [TestMethod]
   public void Test_ØReduceSpaces_Var1() {
      //+ Prepare
      // Ein CRLF werden auch in " " konvertiert, deshalb das Leerzeichen vor dem !
      var expected = @"Hallo

Welt
!";

      var test = @"   

      Hallo    

Welt   
!

";

      //+ Start…
      var res = test.ØReduceSpaces();

      //+ Test
      Assert.AreEqual(res, expected);
   }


   [TestMethod]
   public void Test_ØReduceSpaces_Var2() {
      //+ Prepare
      // Ein CRLF werden auch in " " konvertiert, deshalb das Leerzeichen vor dem !
      var expected = @"Line Nr. 1
Line Nr. 2
Line Nr. 3";

      var test = @"Line Nr.  1  
   Line Nr.  2
 Line Nr.  3  
";

      //+ Start…
      var res = test.ØReduceSpaces();

      //+ Test
      Assert.AreEqual(res, expected);
   }


   [TestMethod]
   public void Test_ØReduceSpaces_Var3() {
      //+ Prepare
      // Ein CRLF werden auch in " " konvertiert, deshalb das Leerzeichen vor dem !
      var expected = @"Line Nr. 1
Line Nr. 2";

      var test = @"
   Line Nr.  1  
   Line Nr.  2  
";

      //+ Start…
      var res = test.ØReduceSpaces();

      //+ Test
      Assert.AreEqual(res, expected);
   }

}
