namespace jig.Tools.Test;

[TestClass]
public class UnitTest_ExtensionMethods_IO_Path_RootDirHandling {

   [TestMethod]

   //       TestNr, Dir          , newRootDirName, Expected
   [DataRow(1,  @"abc",         @"",     @"",            DisplayName = "01")]
   [DataRow(2,  @"abc",         @"neu",  @"neu",         DisplayName = "02")]
   [DataRow(3,  @"abc\",        @"neu",  @"neu\",        DisplayName = "03")]
   [DataRow(4,  @"abc/",        @"neu",  @"neu/",        DisplayName = "04")]
   [DataRow(5,  @"abc\123",     @"neu",  @"neu\123",     DisplayName = "05")]
   [DataRow(6,  @"abc/123",     @"neu",  @"neu/123",     DisplayName = "06")]
   [DataRow(7,  @"abc\123\",    @"neu\", @"neu\123\",    DisplayName = "07")]
   [DataRow(8,  @"abc/123/",    @"neu/", @"neu/123/",    DisplayName = "08")]
   [DataRow(9,  @"abc\123/def", @"neu",  @"neu\123/def", DisplayName = "09")]
   [DataRow(10, @"abc/123\def", @"neu\", @"neu/123\def", DisplayName = "10")]
   [DataRow(11, null,           null,    "",             DisplayName = "11")]
   [DataRow(12, null,           @"",     "",             DisplayName = "12")]
   [DataRow(13, @"",            null,    @"",            DisplayName = "13")]
   [DataRow(14, @"abc",         null,    @"",            DisplayName = "14")]
   [DataRow(15, @"abc\123",     null,    @"123",         DisplayName = "15")]
   public void Test_ØReplaceRootDir(
      int       testNo
      , string? dir
      , string? newRootDirName
      , string  expectedValue) {

      //+ Debug
      var res1 = "".ØReplaceRootDir(newRootDirName);
      var res2 = "abc".ØReplaceRootDir(newRootDirName);
      var res3 = @"abc\".ØReplaceRootDir(newRootDirName);
      var res4 = @"abc\123".ØReplaceRootDir(newRootDirName);
      var res7 = @"abc\123\".ØReplaceRootDir(@"neu\");
      var res9 = @"abc\123/def".ØReplaceRootDir(@"neu\");
      
      //+ Testen 
      Assert.AreEqual(dir.ØReplaceRootDir(newRootDirName), expectedValue);
   }

   [TestMethod]

   //       TestNr, Dir          , Expected
   [DataRow(1, @"abc",         "",         DisplayName = "01")]
   [DataRow(2, @"abc\",        "",         DisplayName = "02")]
   [DataRow(3, @"abc/",        "",         DisplayName = "03")]
   [DataRow(4, @"abc\123",     @"123",     DisplayName = "04")]
   [DataRow(5, @"abc/123",     @"123",     DisplayName = "05")]
   [DataRow(6, @"abc\123\",    @"123\",    DisplayName = "06")]
   [DataRow(7, @"abc/123/",    @"123/",    DisplayName = "07")]
   [DataRow(8, @"abc\123/def", @"123/def", DisplayName = "08")]
   [DataRow(9, @"abc/123\def", @"123\def", DisplayName = "09")]
   [DataRow(10, null, @"", DisplayName = "10")]
   public void Test_ØRemoveRootDir(
      int       testNo
      , string? dir
      , string  expectedValue) {

      //+ Debug
      var res1 = "".ØRemoveRootDir();
      var res2 = "abc".ØRemoveRootDir();
      var res3 = @"abc\".ØRemoveRootDir();
      var res4 = @"abc\123".ØRemoveRootDir();
      var res5 = @"abc\123\".ØRemoveRootDir();
      
      //+ Testen 
      Assert.AreEqual(dir.ØRemoveRootDir(), expectedValue);
   }

}
