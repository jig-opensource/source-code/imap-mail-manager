using jig.Tools.IEqualityComparer;


namespace jig.Tools.Test;

[TestClass]
public class UnitTest_ExtensionMethodsHashSet {

   #region Public Methods and Operators

   [TestMethod]
   public void Test_ØHatSchnittmenge() {
      // !Tut https://learn.microsoft.com/de-de/dotnet/csharp/programming-guide/concepts/linq/set-operations?branch=main#intersect-and-intersectby
      string[] planets1 = { "Mercury", "Venus", "Earth", "Jupiter" };
      string[] planets2 = { "Mercury", "Earth", "Mars", "Jupiter" };

      Assert.IsTrue(planets1.ØHatSchnittmenge(planets2));
      Assert.IsTrue(planets2.ØHatSchnittmenge(planets1));
   }


   public record TestRec(
      string sStr
      , int  iInt
      , bool bBool) {}


   [TestMethod]
   public void Test_ØHatSchnittmengeBy() {
      // !Tut https://learn.microsoft.com/de-de/dotnet/csharp/programming-guide/concepts/linq/set-operations?branch=main#intersect-and-intersectby

      List<TestRec> testRecs1 = new List<TestRec>() {
         new TestRec("Mercury", 1, true), new TestRec("Venus", 2, true), new TestRec("Earth", 3, true),
      };

      List<TestRec> testRecs2 = new List<TestRec>() {
         new TestRec("Mercury", 1, true), new TestRec("Venus", 12, true), new TestRec("Jupiter", 13, true),
      };

      List<TestRec> testRecs3 = new List<TestRec>() {
         new TestRec("Mars", 21, true), new TestRec("Sonne", 22, true),
      };

      // Mercury, 1, true ist identisch:
      Assert.IsTrue(testRecs1.ØHatSchnittmengeBy(testRecs2, x => x));

      // Nichts ist identisch
      Assert.IsFalse(testRecs1.ØHatSchnittmengeBy(testRecs3, x => x));
   }


   /// <summary>
   /// Vergleicht String Listen
   /// Mit einem Comparer
   /// </summary>
   [TestMethod]
   public void Test_ØHatSchnittmengeBy_StringArr_WithComparer() {
      // !Ex als Handbuch
      Assert.IsTrue(
                    (new[] { "Mer", "Ven", "Ear", "Jup" }).ØHatSchnittmengeBy(
                                                                              new[] { "Mer", "Ear" }
                                                                              , x => x
                                                                              , new StringStartsWithComparer()));

      Assert.IsTrue((new[] { "Mercury", "Jupiter" }).ØHatSchnittmengeBy(new[] { "Mer", "Ear" }, x => x, new StringStartsWithComparer()));

      Assert.IsFalse((new[] { "Mars", "Jupiter" }).ØHatSchnittmengeBy(new[] { "Mer", "Ear" }, x => x, new StringStartsWithComparer()));

      string[] planets1 = { "Mer", "Ven", "Ear", "Jup" };
      string[] planets2 = { "Mercury", "Earth", "Mars", "Jupiter" };
      string[] planets3 = { "Mond", "Sonne" };

      // !Tut https://learn.microsoft.com/de-de/dotnet/csharp/programming-guide/concepts/linq/set-operations?branch=main#intersect-and-intersectby
      // !Tut https://learn.microsoft.com/de-de/dotnet/api/system.collections.generic.iequalitycomparer-1?view=net-7.0

      // Der Comparer der prüft, ob die Strings der Haupt-Liste
      // gleich starten wie einer der Strings der Vergleichsliste
      StringStartsWithComparer stringStartsWithComparer = new StringStartsWithComparer();

      Assert.IsFalse(planets1.ØHatSchnittmengeBy(planets2, x => x, stringStartsWithComparer));
      Assert.IsTrue(planets2.ØHatSchnittmengeBy(planets1, x => x, stringStartsWithComparer));
      Assert.IsFalse(planets1.ØHatSchnittmengeBy(planets3, x => x, stringStartsWithComparer));
   }


   /// <summary>
   /// Nur ein Code-Beispiel
   /// </summary>
   public bool Test_ØHatSchnittmengeBy_ObjArr_WithComparer() {
      List<TestRec> listZumTest = new List<TestRec>() {
         new TestRec("Mercury", 1, true), new TestRec("Venus", 2, true), new TestRec("Earth", 3, true),
      };

      List<string> listKürzel = new List<string>() {
         "Mer", "Jup"
      };

      // var list1Keys    = new HashSet<string>(listVergleich.Select(x => x.sStr));
      var listCompareKeys = new List<string>(listKürzel.Select(x => x));

      var matchingItems = listZumTest.Where(
                                            x =>
                                               listCompareKeys.ØStartsWithAnyItemInList(x.sStr));

      return matchingItems.Any();
   }


   #endregion

}
