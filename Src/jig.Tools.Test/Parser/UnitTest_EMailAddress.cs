using ImapMailManager.Mail.Parser;


namespace jig.Tools.Test.Parser;

[TestClass]
public class UnitTest_EMailAddress {

   [TestMethod]
   [DataRow (null,       "", "",  "",       "",         DisplayName = "01")]
   [DataRow ("",       "", "",  "",       "",         DisplayName = "02")]
   [DataRow( "t@jig.ch", "", "t", "jig.ch", "t@jig.ch", DisplayName = "03")]
   [DataRow( "Irgend ein Text", "", "", "", "", DisplayName = "04")]
   public void Test_EMailAddress_Constructor_1Arg(string? testEMail, string xpctDisplayName, 
                                 string  xpctLocalPart, string xpctDomain, 
                                 string  xpctEMailAddr) {
      // E-Mail mit Zusatztext
      // E-Mail mit falschem Format
      var  res   = new EMailAddress_WithWordsForAnonymize(testEMail);
      
      Assert.AreEqual(xpctDisplayName, res.DisplayName);
      Assert.AreEqual(xpctLocalPart, res.LocalPart);
      Assert.AreEqual(xpctDomain, res.Domain);
      Assert.AreEqual(xpctEMailAddr, res.EMailAddr);
   }

   [TestMethod]
   [DataRow (null,              null, "", "",  "",       "",         DisplayName = "01")]
   [DataRow ("",                "",   "", "",  "",       "",         DisplayName = "02")]
   [DataRow ("",                "NurDisplayName",   "NurDisplayName", "",  "",       "",         DisplayName = "03")]
   [DataRow( "t@jig.ch",        "",   "", "t", "jig.ch", "t@jig.ch", DisplayName = "04")]
   [DataRow( "t@jig.ch",        "MitDisplayName",   "MitDisplayName", "t", "jig.ch", "t@jig.ch", DisplayName = "05")]
   [DataRow( "Irgend ein Text", "", "",   "", "",  "",       DisplayName = "06")]
   [DataRow( "Irgend ein Text", "MitDisplayName", "MitDisplayName",   "", "",  "",       DisplayName = "05")]
   public void Test_EMailAddress_Constructor_2Args(string? testEMail, string? testDisplayName, string xpctDisplayName, 
                                 string  xpctLocalPart, string xpctDomain, 
                                 string  xpctEMailAddr) {
      // E-Mail mit Zusatztext
      // E-Mail mit falschem Format
      var res = new EMailAddress_WithWordsForAnonymize(testEMail, testDisplayName);
      
      Assert.AreEqual(xpctDisplayName, res.DisplayName);
      Assert.AreEqual(xpctLocalPart, res.LocalPart);
      Assert.AreEqual(xpctDomain, res.Domain);
      Assert.AreEqual(xpctEMailAddr, res.EMailAddr);
   }

}
