using ImapMailManager.Mail.Parser;


namespace jig.Tools.Test.Parser;

[TestClass]
public class UnitTest_EMailAddress_ExtensionMethods {

   [TestMethod]
   public void Test_ØGetForeignEMailAddresses_KurzerMailname() {

      List<string> addresses = new List<string>() {
         "team@rogerliebi.ch"
         , "team@rogerLiebi.ch"
         , "Urs@nospam.ch"
      };

      //+ Alle Mail Adressen parsen
      var allMailAddresses = EMailAddress_WithWordsForAnonymize.Create(addresses);

      //+ Alle fremden Adressen holen
      var foreignDomainAddresses = allMailAddresses.ØGetForeignEMailAddresses();
      Assert.AreEqual(1, foreignDomainAddresses.Count());

      //+ Die Worte zählen
      var words = ExtensionMethods_EMailAddress_WithWordsForAnonymize.ØGetForeignWords(foreignDomainAddresses);
      Assert.AreEqual(1, words.Count());
   }

   [TestMethod]
   public void Test_ØGetForeignEMailAddresses_LangerMailName() {

      List<string> addresses = new List<string>() {
         "team@rogerliebi.ch"
         , "team@rogerLiebi.ch"
         , "Hans.Muster@nospam.ch"
      };

      //+ Alle Mail Adressen parsen
      var allMailAddresses = EMailAddress_WithWordsForAnonymize.Create(addresses);

      //+ Alle fremden Adressen holen
      var foreignDomainAddresses = allMailAddresses.ØGetForeignEMailAddresses();
      Assert.AreEqual(1, foreignDomainAddresses.Count());

      //+ Die Worte zählen
      var words = ExtensionMethods_EMailAddress_WithWordsForAnonymize.ØGetForeignWords(foreignDomainAddresses);
      Assert.AreEqual(3, words.Count());
   }


   [TestMethod]
   public void Test_ØGetForeignEMailAddresses_MultipleLongAdresses() {

      List<string> addresses = new List<string>() {
         "team@rogerliebi.ch"
         , "team@rogerLiebi.ch"
         , "Muster@nospam.ch"
         , "Hans@mydomain.ch"
      };

      //+ Alle Mail Adressen parsen
      var allMailAddresses = EMailAddress_WithWordsForAnonymize.Create(addresses);

      //+ Alle fremden Adressen holen
      var foreignDomainAddresses = allMailAddresses.ØGetForeignEMailAddresses();
      Assert.AreEqual(2, foreignDomainAddresses.Count());

      //+ Die Worte zählen
      var words = ExtensionMethods_EMailAddress_WithWordsForAnonymize.ØGetForeignWords(foreignDomainAddresses);
      Assert.AreEqual(4, words.Count());
   }

}
