using System.ComponentModel;
using jig.Tools.Parser;


namespace jig.Tools.Test.Parser;

[TestClass]
public class UnitTest_Parse_Telefonnummer {

   static readonly string ReplacementStr = "⛔";


   [TestMethod]
   [DataRow(null,                                       false, "",                  DisplayName = "01")]
   [DataRow("",                                         false, "",                  DisplayName = "02")]
   [DataRow("Anfrage: #10-4558",                        false, "Anfrage: #10-4558", DisplayName = "03")]
   [DataRow("123 456",                                  true,  "⛔",                 DisplayName = "04")]
   [DataRow("123 456 7",                                  true,  "⛔",                 DisplayName = "05")]
   [DataRow("032 341 15 41",                            true,  "⛔",                 DisplayName = "06")]
   [DataRow("+41 79-9455276",                           true,  "⛔",                 DisplayName = "07")]
   [DataRow("(00)49123456789",                          true,  "⛔",                 DisplayName = "08")]
   [DataRow("Hallo (00)49123456789 Welt",               true,  "Hallo ⛔ Welt",      DisplayName = "09")]
   [DataRow("Hallo (00)49123456789 Welt 032 341 15 41", true,  "Hallo ⛔ Welt ⛔",    DisplayName = "10")]
   [DataRow("2023 22:44", true,  "⛔:44",    DisplayName = "11")]
   public void Test_Replace_Telefonnummer_DotNet(string testString, bool hasMatch, string expectedString) {
      var res = Parse_Telefonnummer.Replace_Telefonnummer(testString, ReplacementStr);
      Assert.AreEqual(expectedString, res.Item2);
   }


   [TestMethod]
   [DataRow(null,                                       false, "",                  DisplayName = "01")]
   [DataRow("",                                         false, "",                  DisplayName = "02")]
   [DataRow("Anfrage: #10-4558",                        false, "Anfrage: #10-4558", DisplayName = "03")]
   [DataRow("2023 22:44",                               false,  "2023 22:44",       DisplayName = "04")]
   [DataRow("123 456",                                  true,  "⛔",                DisplayName = "05")]
   [DataRow("123 456 7",                                true,  "⛔",                DisplayName = "06")]
   [DataRow("032 341 15 41",                            true,  "⛔",                DisplayName = "07")]
   [DataRow("+41 79-9455276",                           true,  "⛔",                DisplayName = "08")]
   [DataRow("(00)49123456789",                          true,  "⛔",                DisplayName = "09")]
   [DataRow("Hallo (00)49123456789 Welt",               true,  "Hallo ⛔ Welt",     DisplayName = "10")]
   [DataRow("Hallo (00)49123456789 Welt 032 341 15 41", true,  "Hallo ⛔ Welt ⛔",  DisplayName = "11")]
   public void Test_Replace_Telefonnummer_Pcre2(string testString, bool xpctHasMatch, string xpctString) {
      
      // var match = Parse_Telefonnummer.ORgxTelNrPcre2.ØMatch(testString);
      
      // var res = Parse_Telefonnummer.Replace_Telefonnummer_Pcre2(testString, ReplacementStr);
      // Assert.AreEqual(expectedString, res.Item2);

      var (hasMatch, newString) = Parse_Telefonnummer.ORgxTelNrPcre2.ØReplaceGroupName("TelNr", testString, ReplacementStr);
      Assert.AreEqual(xpctHasMatch, hasMatch);
      Assert.AreEqual(xpctString, newString);
   }

   [TestMethod]
   [DataRow(null,                                           false, "",                              DisplayName = "01")]
   [DataRow("",                                             false, "",                              DisplayName = "02")]
   [DataRow("Anfrage: #10-4558",                            false, "Anfrage: #10-4558",             DisplayName = "03")]
   [DataRow("2023 22:44",                                   false, "2023 22:44",                    DisplayName = "04")]
   [DataRow("123 456",                                      false, "123 456",                       DisplayName = "05")]
   [DataRow("123 456 7",                                    false, "123 456 7",                     DisplayName = "06")]
   [DataRow("032 341 15 41",                                true,  "⛔",                             DisplayName = "07")]
   [DataRow("+41 79-9455276",                               true,  "⛔",                             DisplayName = "08")]
   [DataRow("(00)49123456789",                              true,  "⛔",                             DisplayName = "09")]
   [DataRow("Hallo (00)49123456789 Welt",                   true,  "Hallo ⛔ Welt",                  DisplayName = "10")]
   [DataRow("Hallo (00)49123456789 Welt 032 341 15 41",     true,  "Hallo ⛔ Welt ⛔",                DisplayName = "11")]
   [DataRow("Liebe Grüße +49 511 7636509 +49 151 28359132", true,  "Liebe Grüße ⛔ ⛔",               DisplayName = "12")]
   [DataRow("Am 2021-10-06 03:42, schrieb:",                false, "Am 2021-10-06 03:42, schrieb:", DisplayName = "13")]
   [DataRow("Telefon 0043 650 950 50 50 Test",              true, "Telefon ⛔ Test", DisplayName = "14")]
   [DataRow("phone +41 (0)43 844 33 23 mobile +41 (0)77 476 28 49 Test", true, "phone ⛔ mobile ⛔ Test", DisplayName = "15")]
   [DataRow("Sanddornweg 1, CH-3613 Steffisburg Tel. +41 (0)33 437 63 43 Fax +41 (0)86 033 437 63 43 www.edition-nehemia.ch", true, "Sanddornweg 1, CH-3613 Steffisburg Tel. ⛔ Fax ⛔ www.edition-nehemia.ch", DisplayName = "16")]
   public void Test_Replace_Telefonnummer_Pcre2_MinLen7Digits(string testString, bool xpctHasMatch, string xpctString) {
      
      // var match = Parse_Telefonnummer.ORgxTelNrPcre2.ØMatch(testString);
      
      // var res = Parse_Telefonnummer.Replace_Telefonnummer_Pcre2(testString, ReplacementStr);
      // Assert.AreEqual(expectedString, res.Item2);

      // Siehe auch:
      // ImapMailManager.Config.ConfigTel_Basics.TelNrMatch_Predicate
      var (hasMatch, newString) = Parse_Telefonnummer.ORgxTelNrPcre2.ØReplaceGroupName("TelNr", testString, ReplacementStr
                                                                                       , test => test.Count(char.IsDigit) >= 9);
      Assert.AreEqual(xpctHasMatch, hasMatch, "hasMatch");
      Assert.AreEqual(xpctString, newString, "newStringIsOK");
   }

}
