using ImapMailManager.Mail.Parser;


namespace jig.Tools.Test.Parser;

[TestClass]
public class UnitTest_EMailAddress_Parser {

   /// <summary>
   /// Testet mit
   /// einer E-Mail Adresse in einer Zeile
   /// </summary>
   [TestMethod]
   public void Test_EMailAddress_Parse_SingleLine_SingleMail() {
      //+ Prepare
      var test     = "tom@jig.ch";
      var expectedMail = "tom@jig.ch";
      var expectedDomain = "jig.ch";

      //+ Do it
      var (res, restString) = EMailAddress.Parse_EMailAddresses(test);

      //‼ 🚩 Debug: Ins ClipBoard kopieren
      // ClipboardService.SetText(outerHtml);

      //+ Test
      Assert.AreEqual(res.Count,        1);
      Assert.AreEqual(res[0].EMailAddr, expectedMail);
      Assert.AreEqual(res[0].Domain,    expectedDomain);
   }


   /// <summary>
   /// Testet mit
   /// zwei E-Mail Adressen in einer Zeile
   /// </summary>
   [TestMethod]
   public void Test_ParseMail__Test_SingleLine_DoubleMails() {
      //+ Prepare
      var test           = "eMail1@test.com dslkf sfjf lkjf ls fla eMail2@test.com";
      var expected1      = "eMail1@test.com";
      var expected2      = "eMail2@test.com";
      var expectedDomain = "test.com";

      //+ Do it
      var (res, restString) = EMailAddress.Parse_EMailAddresses(test);

      //‼ 🚩 Debug: Ins ClipBoard kopieren
      // ClipboardService.SetText(outerHtml);

      //+ Test
      Assert.AreEqual(res.Count,        2);
      Assert.AreEqual(res[0].EMailAddr, expected1);
      Assert.AreEqual(res[1].EMailAddr, expected2);
      Assert.AreEqual(res[0].Domain, expectedDomain);
   }

   /// <summary>
   /// Testet mit
   /// drei E-Mail Adressen in zwein Zeile
   /// </summary>
   [TestMethod]
   public void Test_EMailAddress_Parse_DoubleLine_TrippeMails() {
      //+ Prepare
      var test      = @"eMail1@test.com dslkf sfjf lkjf ls fla eMail2@test.com

fla eMail3@test.com sfjf lkjf ls
 
";
      var expected1      = "eMail1@test.com";
      var expected2      = "eMail2@test.com";
      var expected3      = "eMail3@test.com";
      var expectedDomain = "test.com";


      //+ Do it
      var (res, restString) = EMailAddress.Parse_EMailAddresses(test);

      //‼ 🚩 Debug: Ins ClipBoard kopieren
      // ClipboardService.SetText(outerHtml);

      //+ Test
      Assert.AreEqual(res.Count,        3);
      Assert.AreEqual(res[0].EMailAddr, expected1);
      Assert.AreEqual(res[1].EMailAddr, expected2);
      Assert.AreEqual(res[2].EMailAddr, expected3);
      Assert.AreEqual(res[0].Domain, expectedDomain);
   }

}
