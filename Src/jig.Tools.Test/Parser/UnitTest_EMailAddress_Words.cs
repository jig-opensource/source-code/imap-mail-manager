using ImapMailManager.Mail.Parser;


namespace jig.Tools.Test.Parser;


/// <summary>
/// Testet, ob in E-Mail Adressen die Worte für die Anonymisierung richtig erkannt werden
/// </summary>
[TestClass]
public class UnitTest_EMailAddress_Words {

   [TestMethod]
   //       testEMail                testDisplayName    xpctWords    xpctDisplayName    xpctLocalPart    xpctDomain    xpctEMailAddr
   [DataRow (null,                    null,              0,           "",                "",              "",           "",                   DisplayName = "01")]
   [DataRow ("",                      "",                0,           "",                "",              "",           "",                   DisplayName = "02")]
   [DataRow ("",                      "NurDisplayName",  3,           "NurDisplayName",  "",              "",           "",                   DisplayName = "03")]
   // Zu kurze Mail Adresse
   [DataRow( "123@jig.ch",            "",                1,           "",                "123",           "jig.ch",     "123@jig.ch",         DisplayName = "04")]
   [DataRow( "lang@jig.ch",           "",                2,           "",                "lang",          "jig.ch",     "lang@jig.ch",        DisplayName = "05")]
   [DataRow( "Hans.Muster@jig.ch",    "",                3,           "",                "Hans.Muster",   "jig.ch",     "Hans.Muster@jig.ch", DisplayName = "06")]
   [DataRow( "Hans.Muster@jig.ch",    "Peter Müller",    10,         "Peter Müller",    "Hans.Muster",   "jig.ch",     "Hans.Muster@jig.ch", DisplayName = "07")]
   [DataRow( "",                      "Peter Müller",    7,           "Peter Müller",    "",              "",           "",                   DisplayName = "08")]
   [DataRow( "t@jig.ch",              "MitDisplayName",  4,           "MitDisplayName",  "t",             "jig.ch",     "t@jig.ch",           DisplayName = "09")]
   [DataRow( "Irgend ein Text",       "",                0,           "",                "",              "",           "",                   DisplayName = "10")]
   [DataRow( "Irgend ein Text",       "MitDisplayName",  3,           "MitDisplayName",  "",              "",           "",                   DisplayName = "11")]
   public void Test_EMailAddress_WithWordsForAnonymize_WordAnalyzer(string? testEMail,     string? testDisplayName, int
                                                                       xpctWords, string xpctDisplayName, 
                                                       string  xpctLocalPart, string  xpctDomain, 
                                                       string  xpctEMailAddr) {

      var res = new EMailAddress_WithWordsForAnonymize(testEMail, testDisplayName);

      Assert.AreEqual(xpctWords, res.Wortliste.Count);
      Assert.AreEqual(xpctDisplayName, res.DisplayName, "DisplayName");
      Assert.AreEqual(xpctLocalPart, res.LocalPart, "LocalPart");
      Assert.AreEqual(xpctDomain, res.Domain, "Domain");
      Assert.AreEqual(xpctEMailAddr, res.EMailAddr, "EMailAddr");
   }

   

}
