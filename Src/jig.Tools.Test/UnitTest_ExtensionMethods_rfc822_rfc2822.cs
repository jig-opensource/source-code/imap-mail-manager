using jig.Tools;

namespace jig.Tools.Test;

[TestClass]
public class UnitTest_ExtensionMethods_rfc822_rfc2822 {

   [TestMethod]
   public void Test_Extract_RFC2822_Timestamp() {
      //+ Prepare
      // im neuen RFC2822 darf die benannte Zeitzone (CET) nicht angegeben werden
      string testInvalidTimeStamp = "Fri,  3 Feb 2023 20:18:15 +0100 (CET)";
      string expected             = "Fri, 3 Feb 2023 20:18:15 +0100";

      //+ Do it
      var res = ExtensionMethods_rfc822_rfc2822.Extract_RFC2822_Timestamp(testInvalidTimeStamp);

      //+ Test
      Assert.AreEqual(res, expected);
   }


   [TestMethod]
   public void Test_ØTo_Rfc822Rfc2822_Str() {
      //+ Prepare
      var expected = "Fri, 31 Jan 2020 11:00:30 +0100";

      DateTimeOffset test = new DateTimeOffset
         (2020, 1, 31, 12, 00, 30, TimeZoneInfo.Local.BaseUtcOffset);

      //+ Konvertieren
      string res = test.ØTo_Rfc822Rfc2822_Str();

      //+ Test
      Assert.AreEqual(res, expected);
   }

   [TestMethod]
   public void Test_ØFrom_Rfc822Rfc2822_OK() {
      //+ Prepare
      var testStr         = "Sat, 11 Feb 2023 10:14:45 +0100";
      DateTimeOffset expected = new DateTimeOffset
         (2023, 2, 11, 10, 14, 45, TimeZoneInfo.Local.BaseUtcOffset);

      //+ Konvertieren
      DateTimeOffset? res = testStr.ØFrom_Rfc822Rfc2822();

      //+ Test
      Assert.AreEqual(res.Value, expected);
   }

   [TestMethod]
   public void Test_ØFrom_Rfc822Rfc2822_Failed() {
      //+ Prepare
      var testStr         = "Hallo";
      DateTimeOffset? expected = null;

      //+ Konvertieren
      DateTimeOffset? res = testStr.ØFrom_Rfc822Rfc2822();

      //+ Test
      Assert.AreEqual(res, expected);
   }

   [TestMethod]
   public void Test_ØIs_Rfc822Rfc2822_Date() {
      //+ Prepare
      var testStr         = "Hallo";
      //+ Konvertieren
      bool res = testStr.ØIs_Rfc822Rfc2822_Date();

      //+ Test
      Assert.AreEqual(res, false);
   }

}
